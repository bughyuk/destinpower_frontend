import {
  fetchLogisticsStockByProducts,
  fetchLogisticsSummary,
} from '@/api/statistics';

export type StatZone = {
  zoneId: string;
  zoneName: string;
  geomStr: string;
};

export type StatAccessInfo = {
  averageForPeriod: number;
  averageForPeriodComparison: number;
  totalEnterCountForPeriod: number;
  totalEnterCountForPeriodComparison: number;
  frequentAreaForPeriod: FrequentAreaForPeriod | null;
  rushHourForPeriod: RushHourForPeriod | null;
  rushHourForPeriodByDaily: RushHourForPeriod[];
  enterCountForPeriodByDaily: {
    manager: EnterCountForPeriodByDaily[];
    user: EnterCountForPeriodByDaily[];
  };
};

type FrequentAreaForPeriod = {
  areaName: string;
  totalPercent: number;
};

type RushHourForPeriod = {
  logHour: string;
  cnt: number;
  totalPercent: number;
};

type EnterCountForPeriodByDaily = {
  logDate: string;
  cnt: number;
};

export type StatCongestionArea = {
  lng: string;
  lat: string;
};

export type StatLogisticsSummary = {
  warehouseStockTotal: number;
  logisticsCounts: {
    warehousingCompleteCnt: number;
    releasingCompleteCnt: number;
    logisticsIncompleteCnt: number;
  };
};

export type StatLogisticsTrend = {
  date: string;
  totalQuantity: number;
};

export type StatLogisticsStockChange = {
  date: string;
  totalQuantity: number;
};

export type StatLogisticsStockByProduct = {
  totalQuantitySum: number;
  data: {
    productName: string;
    percent: number;
  }[];
};
