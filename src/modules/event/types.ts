export type ControlEvent = ControlEventImage & {
  eventId: string;
  eventTitle: string;
  eventSubject: string;
  eventContent: string;
  startDate: string;
  endDate: string;
  activeFlag: boolean;
  registDate: string;
  geom?: string;
  geomType?: string;
  lng?: number;
  lat?: number;
  radius?: number;
  areaName: string;
  areaId: string;
  areaGeom: string;
};

export type ControlEventImage = {
  imgId: string;
  originalFileName: string;
  fileUrl: string;
};
