export type InviteUser = {
  invitationSeq: number;
  userId: string;
  userName: string;
  latestAccessDate: string;
  accountBeingFlag: boolean;
  profileImgId: string;
  profileImgUrl: string;
  activeFlag: boolean;
};

export type InviteUserDetail = {
  seq: number;
  invitationSeq: number;
  userId: string;
  userName: string;
  latestAccessDate: string;
  profileImgId: string;
  profileImgUrl: string;
  activeFlag: boolean;
  companyName: string;
  department: string;
};

export type Group = {
  groupSeq: number;
  groupName: string;
  groupDesc: string;
  projectCnt: number;
  maxCount: number;
  userCnt: number;
  deleteFlag: boolean;
  seq: number;
};

export type GroupAddUser = {
  userId: string;
  invitationSeq: number;
};

export type GroupProjectRoleType = 'user' | 'admin';

export type GroupProject = {
  groupSeq: number;
  groupName: string;
  seq: number;
  projectId: string;
  projectName: string;
  roleType: GroupProjectRoleType;
  thumbnail: string;
};

export type GroupAddProject = {
  projectId: string;
  roleType: GroupProjectRoleType;
};

export type GroupChangeRoleProject = GroupAddProject & {
  seq: number;
};

export type GroupDetail = Group;
