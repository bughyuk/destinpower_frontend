import React, { createContext, Dispatch, ReactNode, useReducer } from 'react';
import {
  Action,
  CONTROL_MODE_TENSE_REALTIME,
  HistoryFilter,
  RIGHT_PANE_STATISTICS,
  SetupState,
} from '@/modules/setup/types';
import {
  MENU_IDX_ACCIDENT,
  MENU_IDX_EVENT,
  MENU_IDX_SPACE,
  MENU_IDX_PROJECT,
  MENU_IDX_SEARCH,
  MENU_IDX_CONNECTED_SPACE,
  MENU_IDX_USER,
  MENU_IDX_ASSETS,
  MENU_IDX_ENTRY,
  MENU_IDX_STATISTICS,
  MENU_IDX_PHYSICAL_DISTRIBUTION,
  MENU_IDX_PROCESS_DASHBOARD,
  MENU_IDX_PROCESS_MANAGEMENT,
  MENU_IDX_PROCESS_SETTINGS,
} from '@/utils/constants/common';
import { setupReducer } from '@/modules/setup/reducer';

export const SetupStateContext = createContext<SetupState | null>(null);
type SetupDispatch = Dispatch<Action>;
export const SetupDispatchContext = createContext<SetupDispatch | null>(null);

const historyFilterInitialState: HistoryFilter = {
  period: [],
  startTime: '',
  endTime: '',
  gender: '',
  cellphone: '',
  age: {
    from: 0,
    to: 0,
  },
  residenceTime: {
    from: 0,
    to: 0,
  },
};

export const initialState: SetupState = {
  data: {
    search: {
      division: 0,
      keyword: '',
      flag: false,
    },
    historyFilter: historyFilterInitialState,
    historyUserFilter: {
      accessKey: '',
      users: [],
      filter: historyFilterInitialState,
      tab: 'info',
    },
  },
  mode: {
    tense: CONTROL_MODE_TENSE_REALTIME,
    historyStandard: 'condition',
  },
  menus: [
    {
      menuIdx: MENU_IDX_SEARCH,
      iconName: '',
      titleKey: '',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_PROJECT,
      iconName: 'topic',
      titleKey: 'text_project',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_CONNECTED_SPACE,
      iconName: 'shop_2',
      titleKey: 'text_app',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_STATISTICS,
      iconName: 'bar_chart',
      titleKey: 'text_statistics',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_EVENT,
      iconName: 'local_mall',
      titleKey: 'text_event',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_ENTRY,
      iconName: 'camera_outdoor',
      titleKey: 'text_entry_management',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_ACCIDENT,
      iconName: 'error_outline',
      titleKey: 'text_accident',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_PHYSICAL_DISTRIBUTION,
      iconName: 'view_in_ar',
      titleKey: 'text_physical_distribution',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_ASSETS,
      iconName: 'category',
      titleKey: 'text_assets_management',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_USER,
      iconName: 'person_search',
      titleKey: 'text_user',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_SPACE,
      iconName: '',
      titleKey: '',
      active: false,
      display: false,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_PROCESS_DASHBOARD,
      iconName: 'dashboard',
      titleKey: '대시보드',
      active: true,
      display: true,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_PROCESS_MANAGEMENT,
      iconName: 'fact_check',
      titleKey: '공정관리',
      active: false,
      display: true,
      wide: false,
    },
    {
      menuIdx: MENU_IDX_PROCESS_SETTINGS,
      iconName: 'settings',
      titleKey: '설정',
      active: false,
      display: true,
      wide: false,
    },
  ],
  visibleLayer: [],
  eventsFlag: {
    click: true,
  },
  assetsReloadFlag: false,
  controlUser: {
    tab: 'info',
    flag: false,
  },
  pane: {
    left: {
      container: null,
    },
    float: {
      show: false,
      view: undefined,
      checkListId: '-1',
      container: null,
    },
    right: {
      show: false,
      view: RIGHT_PANE_STATISTICS,
    },
  },
};

export function SetupContextProvider({ children }: { children: ReactNode }) {
  const [setup, dispatch] = useReducer(setupReducer, initialState);

  return (
    <SetupDispatchContext.Provider value={dispatch}>
      <SetupStateContext.Provider value={setup}>
        {children}
      </SetupStateContext.Provider>
    </SetupDispatchContext.Provider>
  );
}
