import { useContext, useMemo } from 'react';
import {
  SetupDispatchContext,
  SetupStateContext,
} from '@/modules/setup/context';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  CellphoneType,
  ControlModeHistoryStandard,
  ControlModeTense,
  ControlUserTab,
  FloatPaneView,
  GenderType,
  HistoryFilter,
  HistoryUserFilter,
  LayerKind,
  Menu,
  RightPaneView,
  Search,
  SET_UP_ADD_VISIBLE_LAYER,
  SET_UP_CHANGE_ASSETS_RELOAD_FLAG,
  SET_UP_CHANGE_EVENT_FLAG_CLICK,
  SET_UP_CHANGE_FLOAT_PANE_SHOW,
  SET_UP_CHANGE_RIGHT_PANE_SHOW,
  SET_UP_DIRECT_MENU_ACTIVE,
  SET_UP_DIRECT_SHOW_USER_PANE,
  SET_UP_INIT,
  SET_UP_MENU_ACTIVE,
  SET_UP_MENU_RESET_ACTIVE,
  SET_UP_REMOVE_VISIBLE_LAYER,
  SET_UP_RESET_CONTROL_USER_TAB,
  SET_UP_SEARCH_ACTIVE,
  SET_UP_SET_CONTROL_MODE_TENSE,
  SET_UP_SET_DATA_HISTORY_FILTER,
  SET_UP_SET_DATA_HISTORY_USER_FILTER,
  SET_UP_SET_DATA_PERIOD_FILTER,
  SET_UP_SET_FLOAT_PANE_CONTAINER,
  SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID,
  SET_UP_SET_LEFT_PANE_CONTAINER,
  SET_UP_TOGGLE_EVENT_FLAG_CLICK,
} from '@/modules/setup/types';
import moment from 'moment';
import { useControlProject } from '@/modules/map/hook';

function useSetupState() {
  const state = useContext(SetupStateContext);
  if (!state) throw new Error('SetupProvider not found');
  return state;
}

function useSetupDispatch() {
  const dispatch = useContext(SetupDispatchContext);
  if (!dispatch) throw new Error('SetupProvider not found');
  return dispatch;
}

export function useLeftPaneContainer() {
  const { pane } = useSetupState();
  const dispatch = useSetupDispatch();

  const setContainer = (container: PerfectScrollbar) => {
    dispatch({
      type: SET_UP_SET_LEFT_PANE_CONTAINER,
      container: container,
    });
  };

  const updateScroll = () => {
    const leftPaneContainer = pane.left.container as any;
    if (leftPaneContainer && leftPaneContainer._container) {
      leftPaneContainer.updateScroll();
    }
  };

  return {
    setContainer,
    updateScroll,
  };
}

export function useFloatPane() {
  const { pane } = useSetupState();
  const dispatch = useSetupDispatch();
  const { show, view, checkListId, container } = pane.float;

  const handleSetContainer = (container: PerfectScrollbar) => {
    dispatch({
      type: SET_UP_SET_FLOAT_PANE_CONTAINER,
      container: container,
    });
  };

  const handleContainerUpdateScroll = () => {
    if (container) {
      container.updateScroll();
    }
  };

  const handleChangeShow = (show: boolean, view: FloatPaneView = undefined) => {
    dispatch({
      type: SET_UP_CHANGE_FLOAT_PANE_SHOW,
      show,
      view,
    });
  };

  const handleSetCheckListId = (checkListId: string) => {
    dispatch({
      type: SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID,
      checkListId,
    });
  };

  return {
    show,
    view,
    checkListId,
    handleSetContainer,
    handleContainerUpdateScroll,
    handleChangeShow,
    handleSetCheckListId,
  };
}

export function useRightPane() {
  const { pane } = useSetupState();
  const dispatch = useSetupDispatch();
  const { show, view } = pane.right;

  const handleChangeShow = (show: boolean, view?: RightPaneView) => {
    dispatch({
      type: SET_UP_CHANGE_RIGHT_PANE_SHOW,
      show,
      view,
    });
  };

  return {
    show,
    view,
    handleChangeShow,
  };
}

export function useMapMenu() {
  const { menus } = useSetupState();
  const { project } = useControlProject();
  const { spaceList } = project;

  const filterMenus: Menu[] = useMemo(() => {
    if (!spaceList.length) {
      return menus.filter((menu) => {
        // TODO: 층 없을 경우 예외상황 필요
        // if (menu.menuIdx !== MENU_IDX_STATISTICS) {
        return menu;
        // }
      });
    }

    return menus;
  }, [menus, spaceList]);

  return {
    menus: filterMenus,
  };
}

export function useActiveMenu() {
  const state = useSetupState();
  const dispatch = useSetupDispatch();
  const activeMenu = useMemo(
    () => state.menus.find((menuDatum) => menuDatum.active),
    [state.menus]
  );

  let menuIdx = -1;
  let show = false;
  let showWide = false;
  if (activeMenu) {
    menuIdx = activeMenu.menuIdx;
    if (activeMenu.wide) {
      showWide = true;
    } else {
      show = true;
    }
  }

  const handleMenuActive = (menuIdx: number) => {
    dispatch({
      type: SET_UP_MENU_ACTIVE,
      menuIdx: menuIdx,
    });
  };

  const handleMenuResetActive = () => {
    dispatch({
      type: SET_UP_MENU_RESET_ACTIVE,
    });
  };

  const handleDirectMenuActive = (menuIdx: number) => {
    dispatch({
      type: SET_UP_DIRECT_MENU_ACTIVE,
      menuIdx,
    });
  };

  const handleSearchActive = (search: Search) => {
    dispatch({
      type: SET_UP_SEARCH_ACTIVE,
      search,
    });
  };

  return {
    menuIdx,
    show,
    showWide,
    handleMenuActive,
    handleMenuResetActive,
    handleDirectMenuActive,
    handleSearchActive,
  };
}

export function useControlMode() {
  const state = useSetupState();
  const dispatch = useSetupDispatch();
  const mode = state.mode;
  const { tense, historyStandard } = mode;

  const setControlModeTense = (
    tense: ControlModeTense,
    historyStandard: ControlModeHistoryStandard = 'condition'
  ) => {
    dispatch({
      type: SET_UP_SET_CONTROL_MODE_TENSE,
      tense,
      historyStandard,
    });
  };

  return {
    tense,
    historyStandard,
    setControlModeTense,
  };
}

export function useControlUserPane() {
  const { controlUser } = useSetupState();
  const dispatch = useSetupDispatch();

  const handleDirectShowControlUserPane = (tab: ControlUserTab) => {
    dispatch({
      type: SET_UP_DIRECT_SHOW_USER_PANE,
      tab,
    });
  };

  const handleResetControlUserTab = () => {
    dispatch({
      type: SET_UP_RESET_CONTROL_USER_TAB,
    });
  };

  return {
    controlUserTab: controlUser.tab,
    controlUserFlag: controlUser.flag,
    handleDirectShowControlUserPane,
    handleResetControlUserTab,
  };
}

export function useSetupData() {
  const { data } = useSetupState();
  const dispatch = useSetupDispatch();
  const { search, historyFilter, historyUserFilter } = data;

  const handleSetHistoryFilter = (data: HistoryFilter) => {
    dispatch({
      type: SET_UP_SET_DATA_HISTORY_FILTER,
      data,
    });
  };

  const handleSetPeriodFilter = (period: string[]) => {
    dispatch({
      type: SET_UP_SET_DATA_PERIOD_FILTER,
      period,
    });
  };

  const handleSetHistoryUserFilter = (historyUserFilter: HistoryUserFilter) => {
    const accessKey = historyUserFilter.accessKey;
    const users = historyUserFilter.users;
    const user = users.find((user) => user.access_key === accessKey);

    if (user) {
      const format = 'YYYY-MM-DD';
      const today = moment().format(format);
      const ago = moment().subtract(1, 'days').format(format);
      const userDeviceType = user.device_type;

      let cellphone: CellphoneType = 'Android';
      if (userDeviceType.toLowerCase() === 'ios') {
        cellphone = 'Iphone';
      }

      historyUserFilter.filter = {
        period: [ago, today],
        age: {
          to: user.user_age,
          from: user.user_age,
        },
        gender: user.user_gender as GenderType,
        residenceTime: {
          from: 0,
          to: 10,
        },
        startTime: '',
        endTime: '',
        cellphone,
      };
    }

    dispatch({
      type: SET_UP_SET_DATA_HISTORY_USER_FILTER,
      historyUserFilter,
    });
  };

  return {
    search,
    historyFilter,
    historyUserFilter,
    handleSetHistoryFilter,
    handleSetHistoryUserFilter,
    handleSetPeriodFilter,
  };
}

export function useSetupEventsFlag() {
  const { eventsFlag } = useSetupState();
  const dispatch = useSetupDispatch();
  const { click } = eventsFlag;

  const handleChangeClick = (flag: boolean) => {
    dispatch({
      type: SET_UP_CHANGE_EVENT_FLAG_CLICK,
      flag,
    });
  };

  const handleToggleClick = () => {
    dispatch({
      type: SET_UP_TOGGLE_EVENT_FLAG_CLICK,
    });
  };

  return {
    click,
    handleChangeClick,
    handleToggleClick,
  };
}

export function useSetupLayer() {
  const { visibleLayer } = useSetupState();
  const dispatch = useSetupDispatch();

  const handleAddVisibleLayer = (layer: LayerKind) => {
    dispatch({
      type: SET_UP_ADD_VISIBLE_LAYER,
      layer,
    });
  };

  const handleRemoveVisibleLayer = (layer: LayerKind) => {
    dispatch({
      type: SET_UP_REMOVE_VISIBLE_LAYER,
      layer,
    });
  };

  return {
    visibleLayer,
    handleAddVisibleLayer,
    handleRemoveVisibleLayer,
  };
}

export function useSetupInit() {
  const dispatch = useSetupDispatch();

  const handleSetupInit = () => {
    dispatch({
      type: SET_UP_INIT,
    });
  };

  return {
    handleSetupInit,
  };
}

export function useSetupAssets() {
  const { assetsReloadFlag } = useSetupState();
  const dispatch = useSetupDispatch();

  const handleChangeReloadFlag = () => {
    dispatch({
      type: SET_UP_CHANGE_ASSETS_RELOAD_FLAG,
    });
  };

  return {
    assetsReloadFlag,
    handleChangeReloadFlag,
  };
}
