import PerfectScrollbar from 'react-perfect-scrollbar';
import { HistoryUser } from '@/modules/map/types';
import { LAYER_HEAT_MAP } from '@/utils/constants/common';

export const CONTROL_MODE_TENSE_HISTORY = 1 as const;
export const CONTROL_MODE_TENSE_REALTIME = 2 as const;

export type ControlModeTense =
  | typeof CONTROL_MODE_TENSE_HISTORY
  | typeof CONTROL_MODE_TENSE_REALTIME;

export type ControlModeHistoryStandard = 'condition' | 'user';

export type ControlMode = {
  tense: ControlModeTense;
  historyStandard: ControlModeHistoryStandard;
};

export type RangeValueType = {
  from: number;
  to: number;
};

export type GenderType = '' | 'M' | 'W';

export type CellphoneType = '' | 'Android' | 'Iphone';

export type HistoryFilter = {
  period: string[];
  startTime: string;
  endTime: string;
  gender: GenderType;
  cellphone: CellphoneType;
  age: RangeValueType;
  residenceTime: RangeValueType;
};

export type HistoryUserFilter = {
  accessKey: string;
  users: HistoryUser[];
  filter?: HistoryFilter;
  tab: ControlUserTab;
};

export type SearchDivision = 0 | 1 | 2;

export type Search = {
  division: SearchDivision;
  keyword: string;
  flag: boolean;
};

type Data = {
  search: Search;
  historyFilter: HistoryFilter;
  historyUserFilter: HistoryUserFilter;
};

export type Menu = {
  menuIdx: number;
  iconName: string;
  titleKey: string;
  active: boolean;
  display: boolean;
  wide: boolean;
};

export type ControlUserTab = 'info' | 'message' | 'trace';

export type LayerKind = typeof LAYER_HEAT_MAP;

export const RIGHT_PANE_STATISTICS = 0;
export const RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS = 1;
export const RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY = 2;

export type RightPaneView =
  | typeof RIGHT_PANE_STATISTICS
  | typeof RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS
  | typeof RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY;

export const FLOAT_PANE_PHYSICAL_DISTRIBUTION = 0;
export const FLOAT_PANE_STATISTICS = 1;
export const FLOAT_PANE_PROCESS_CHECK = 70;
export const FLOAT_PANE_COMPLETED_PROCESS = 71;
export const FLOAT_PANE_PROCESS_DASHBOARD = 72;
export const FLOAT_PANE_PROCESS_CHECK_LIST_MANAGEMENT = 73;

export type FloatPaneView =
  | undefined
  | typeof FLOAT_PANE_PHYSICAL_DISTRIBUTION
  | typeof FLOAT_PANE_STATISTICS
  | typeof FLOAT_PANE_PROCESS_CHECK
  | typeof FLOAT_PANE_COMPLETED_PROCESS
  | typeof FLOAT_PANE_PROCESS_DASHBOARD
  | typeof FLOAT_PANE_PROCESS_CHECK_LIST_MANAGEMENT;

export type Pane = {
  left: {
    container: PerfectScrollbar | null;
  };
  float: {
    show: boolean;
    view: FloatPaneView;
    checkListId: string;
    container: PerfectScrollbar | null;
  };
  right: {
    show: boolean;
    view: RightPaneView;
  };
};

export type SetupState = {
  data: Data;
  mode: ControlMode;
  menus: Menu[];
  visibleLayer: LayerKind[];
  eventsFlag: {
    click: boolean;
  };
  assetsReloadFlag: boolean;
  controlUser: {
    tab: ControlUserTab;
    flag: boolean;
  };
  pane: Pane;
};

export const SET_UP_SET_LEFT_PANE_CONTAINER = 'SET_UP/LEFT_PANE_SET_CONTAINER' as const;
export const SET_UP_MENU_ACTIVE = 'SET_UP/MENU_ACTIVE' as const;
export const SET_UP_MENU_RESET_ACTIVE = 'SET_UP/MENU_RESET_ACTIVE' as const;
export const SET_UP_SEARCH_ACTIVE = 'SET_UP/SEARCH_ACTIVE' as const;
export const SET_UP_SEARCH_INACTIVE = 'SET_UP/SEARCH_INACTIVE' as const;
export const SET_UP_SET_CONTROL_MODE_TENSE = 'SET_UP/SET_CONTROL_MODE_TENSE' as const;
export const SET_UP_DIRECT_MENU_ACTIVE = 'SET_UP/DIRECT_MENU_ACTIVE' as const;
export const SET_UP_DIRECT_SHOW_USER_PANE = 'SET_UP/DIRECT_SHOW_USER_PANE' as const;
export const SET_UP_RESET_CONTROL_USER_TAB = 'SET_UP/RESET_CONTROL_USER_TAB' as const;
export const SET_UP_SET_DATA_HISTORY_FILTER = 'SET_UP/SET_DATA_HISTORY_FILTER' as const;
export const SET_UP_SET_DATA_HISTORY_USER_FILTER = 'SET_UP/SET_DATA_HISTORY_USER_FILTER' as const;
export const SET_UP_SET_DATA_PERIOD_FILTER = 'SET_UP/SET_DATA_PERIOD_FILTER' as const;
export const SET_UP_TOGGLE_EVENT_FLAG_CLICK = 'SET_UP/TOGGLE_EVENT_FLAG_CLICK' as const;
export const SET_UP_CHANGE_EVENT_FLAG_CLICK = 'SET_UP/CHANGE_EVENT_FLAG_CLICK' as const;
export const SET_UP_ADD_VISIBLE_LAYER = 'SET_UP/ADD_VISIBLE_LAYER' as const;
export const SET_UP_REMOVE_VISIBLE_LAYER = 'SET_UP/REMOVE_VISIBLE_LAYER' as const;
export const SET_UP_INIT = 'SET_UP/INIT' as const;
export const SET_UP_CHANGE_FLOAT_PANE_SHOW = 'SET_UP/CHANGE_FLOAT_PANE_SHOW' as const;
export const SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID = 'SET_UP/SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID' as const;
export const SET_UP_SET_FLOAT_PANE_CONTAINER = 'SET_UP/SET_FLOAT_PANE_CONTAINER' as const;
export const SET_UP_CHANGE_RIGHT_PANE_SHOW = 'SET_UP/CHANGE_RIGHT_PANE_SHOW' as const;
export const SET_UP_CHANGE_ASSETS_RELOAD_FLAG = 'SET_UP/CHANGE_ASSETS_RELOAD_FLAG' as const;

export type Action =
  | {
      type: typeof SET_UP_SET_LEFT_PANE_CONTAINER;
      container: PerfectScrollbar;
    }
  | {
      type: typeof SET_UP_MENU_ACTIVE;
      menuIdx: number;
    }
  | {
      type: typeof SET_UP_MENU_RESET_ACTIVE;
    }
  | {
      type: typeof SET_UP_SEARCH_ACTIVE;
      search: Search;
    }
  | {
      type: typeof SET_UP_SEARCH_INACTIVE;
    }
  | {
      type: typeof SET_UP_SET_CONTROL_MODE_TENSE;
      tense: ControlModeTense;
      historyStandard: ControlModeHistoryStandard;
    }
  | {
      type: typeof SET_UP_DIRECT_MENU_ACTIVE;
      menuIdx: number;
    }
  | {
      type: typeof SET_UP_DIRECT_SHOW_USER_PANE;
      tab: ControlUserTab;
    }
  | {
      type: typeof SET_UP_RESET_CONTROL_USER_TAB;
    }
  | {
      type: typeof SET_UP_SET_DATA_HISTORY_FILTER;
      data: HistoryFilter;
    }
  | {
      type: typeof SET_UP_SET_DATA_HISTORY_USER_FILTER;
      historyUserFilter: HistoryUserFilter;
    }
  | {
      type: typeof SET_UP_SET_DATA_PERIOD_FILTER;
      period: string[];
    }
  | {
      type: typeof SET_UP_TOGGLE_EVENT_FLAG_CLICK;
    }
  | {
      type: typeof SET_UP_CHANGE_EVENT_FLAG_CLICK;
      flag: boolean;
    }
  | {
      type: typeof SET_UP_ADD_VISIBLE_LAYER;
      layer: LayerKind;
    }
  | {
      type: typeof SET_UP_REMOVE_VISIBLE_LAYER;
      layer: LayerKind;
    }
  | {
      type: typeof SET_UP_INIT;
    }
  | {
      type: typeof SET_UP_SET_FLOAT_PANE_CONTAINER;
      container: PerfectScrollbar;
    }
  | {
      type: typeof SET_UP_CHANGE_FLOAT_PANE_SHOW;
      show: boolean;
      view?: FloatPaneView;
    }
  | {
      type: typeof SET_UP_CHANGE_RIGHT_PANE_SHOW;
      show: boolean;
      view?: RightPaneView;
    }
  | {
      type: typeof SET_UP_CHANGE_ASSETS_RELOAD_FLAG;
    }
  | {
      type: typeof SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID;
      checkListId: string;
    };
