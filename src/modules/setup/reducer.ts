import {
  Action,
  ControlUserTab,
  LayerKind,
  SET_UP_ADD_VISIBLE_LAYER,
  SET_UP_CHANGE_ASSETS_RELOAD_FLAG,
  SET_UP_CHANGE_EVENT_FLAG_CLICK,
  SET_UP_CHANGE_FLOAT_PANE_SHOW,
  SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID,
  SET_UP_CHANGE_RIGHT_PANE_SHOW,
  SET_UP_DIRECT_MENU_ACTIVE,
  SET_UP_DIRECT_SHOW_USER_PANE,
  SET_UP_INIT,
  SET_UP_MENU_ACTIVE,
  SET_UP_MENU_RESET_ACTIVE,
  SET_UP_REMOVE_VISIBLE_LAYER,
  SET_UP_RESET_CONTROL_USER_TAB,
  SET_UP_SEARCH_ACTIVE,
  SET_UP_SEARCH_INACTIVE,
  SET_UP_SET_CONTROL_MODE_TENSE,
  SET_UP_SET_DATA_HISTORY_FILTER,
  SET_UP_SET_DATA_HISTORY_USER_FILTER,
  SET_UP_SET_DATA_PERIOD_FILTER,
  SET_UP_SET_FLOAT_PANE_CONTAINER,
  SET_UP_SET_LEFT_PANE_CONTAINER,
  SET_UP_TOGGLE_EVENT_FLAG_CLICK,
  SetupState,
} from '@/modules/setup/types';
import { MENU_IDX_SEARCH, MENU_IDX_USER } from '@/utils/constants/common';
import _ from 'lodash';

export function setupReducer(state: SetupState, action: Action) {
  switch (action.type) {
    case SET_UP_SET_LEFT_PANE_CONTAINER:
      return {
        ...state,
        pane: {
          ...state.pane,
          left: {
            container: action.container,
          },
        },
      };
    case SET_UP_MENU_ACTIVE:
      return {
        ...state,
        menus: state.menus.map((menu) =>
          menu.menuIdx === action.menuIdx
            ? { ...menu, active: true }
            : { ...menu, active: false }
        ),
      };
    case SET_UP_MENU_RESET_ACTIVE:
      return {
        ...state,
        menus: state.menus.map((menu) => ({
          ...menu,
          active: false,
        })),
      };
    case SET_UP_SEARCH_ACTIVE:
      return {
        ...state,
        data: {
          ...state.data,
          search: {
            ...action.search,
            flag: !state.data.search.flag,
          },
        },
        menus: state.menus.map((menu) =>
          menu.menuIdx === MENU_IDX_SEARCH
            ? { ...menu, active: true }
            : { ...menu, active: false }
        ),
      };
    case SET_UP_SEARCH_INACTIVE:
      return {
        ...state,
        menus: state.menus.map((menu) => ({ ...menu, active: false })),
      };
    case SET_UP_SET_CONTROL_MODE_TENSE:
      return {
        ...state,
        mode: {
          ...state.mode,
          tense: action.tense,
          historyStandard: action.historyStandard,
        },
      };
    case SET_UP_DIRECT_MENU_ACTIVE:
      return {
        ...state,
        menus: state.menus.map((menu) =>
          menu.menuIdx === action.menuIdx
            ? { ...menu, active: true }
            : { ...menu, active: false }
        ),
      };
    case SET_UP_DIRECT_SHOW_USER_PANE:
      return {
        ...state,
        controlUser: {
          tab: action.tab,
          flag: !state.controlUser.flag,
        },
        menus: state.menus.map((menu) =>
          menu.menuIdx === MENU_IDX_USER
            ? { ...menu, active: true }
            : { ...menu, active: false }
        ),
      };
    case SET_UP_RESET_CONTROL_USER_TAB:
      return {
        ...state,
        controlUser: {
          ...state.controlUser,
          tab: 'info' as ControlUserTab,
        },
      };
    case SET_UP_SET_DATA_HISTORY_FILTER:
      return {
        ...state,
        data: {
          ...state.data,
          historyFilter: action.data,
        },
      };
    case SET_UP_SET_DATA_HISTORY_USER_FILTER:
      return {
        ...state,
        data: {
          ...state.data,
          historyFilter:
            action.historyUserFilter.filter || state.data.historyFilter,
          historyUserFilter: action.historyUserFilter,
        },
      };
    case SET_UP_SET_DATA_PERIOD_FILTER:
      const newState = {
        ...state,
        data: {
          ...state.data,
          historyFilter: {
            ...state.data.historyFilter,
            period: action.period,
          },
          historyUserFilter: {
            ...state.data.historyUserFilter,
          },
        },
      };

      const filter = state.data.historyUserFilter.filter;
      if (filter) {
        newState.data.historyUserFilter.filter = {
          ...filter,
          period: action.period,
        };
      }

      return newState;
    case SET_UP_TOGGLE_EVENT_FLAG_CLICK:
      return {
        ...state,
        eventsFlag: {
          ...state.eventsFlag,
          click: !state.eventsFlag.click,
        },
      };
    case SET_UP_CHANGE_EVENT_FLAG_CLICK:
      return {
        ...state,
        eventsFlag: {
          ...state.eventsFlag,
          click: action.flag,
        },
      };
    case SET_UP_ADD_VISIBLE_LAYER:
      let addVisibleLayer = [...state.visibleLayer];
      addVisibleLayer.push(action.layer);
      addVisibleLayer = _.uniq(addVisibleLayer) as LayerKind[];

      return {
        ...state,
        visibleLayer: addVisibleLayer,
      };
    case SET_UP_REMOVE_VISIBLE_LAYER:
      let removeVisibleLayer = [...state.visibleLayer];
      removeVisibleLayer = removeVisibleLayer.filter(
        (layer) => layer != action.layer
      );
      return {
        ...state,
        visibleLayer: removeVisibleLayer,
      };
    case SET_UP_INIT:
      return {
        ...state,
        eventsFlag: {
          click: true,
        },
        visibleLayer: [],
      };
    case SET_UP_SET_FLOAT_PANE_CONTAINER:
      return {
        ...state,
        pane: {
          ...state.pane,
          float: {
            ...state.pane.float,
            container: action.container,
          },
        },
      };
    case SET_UP_CHANGE_FLOAT_PANE_SHOW:
      return {
        ...state,
        pane: {
          ...state.pane,
          float: {
            ...state.pane.float,
            show: action.show,
            view: typeof action.view !== 'undefined' ? action.view : undefined,
          },
        },
      };
    case SET_UP_CHANGE_FLOAT_PANE_CHECK_LIST_ID:
      return {
        ...state,
        pane: {
          ...state.pane,
          float: {
            ...state.pane.float,
            checkListId: action.checkListId,
          },
        },
      };
    case SET_UP_CHANGE_RIGHT_PANE_SHOW:
      return {
        ...state,
        pane: {
          ...state.pane,
          right: {
            ...state.pane.right,
            show: action.show,
            view:
              typeof action.view !== 'undefined'
                ? action.view
                : state.pane.right.view,
          },
        },
      };
    case SET_UP_CHANGE_ASSETS_RELOAD_FLAG:
      return {
        ...state,
        assetsReloadFlag: !state.assetsReloadFlag,
      };
  }
}
