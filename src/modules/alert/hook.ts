import { useReducer } from 'react';
import { alertReducer } from '@/modules/alert/reducer';
import {
  Alert,
  ALERT_CONFIRM,
  ALERT_SHOW,
  AlertState,
} from '@/modules/alert/types';

export function useAlert() {
  const initialState: AlertState = [];

  const [alerts, dispatch] = useReducer(alertReducer, initialState);

  const handleSetAlert = (alert: Alert) => {
    dispatch({
      type: ALERT_SHOW,
      alert,
    });
  };

  const handleAlertConfirm = (eventId: string) => {
    dispatch({
      type: ALERT_CONFIRM,
      eventId,
    });
  };

  return {
    alerts,
    handleSetAlert,
    handleAlertConfirm,
  };
}
