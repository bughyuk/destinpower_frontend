export type FcmNotification = {
  title: string;
  body: string;
};

export type FcmData = {
  eventId: string;
  mappingId: string;
  mapId: string;
};

export type Alert = {
  eventId: string;
  mappingId: string;
  mapId: string;
  title: string;
  body: string;
};

export type AlertState = Alert[];

export const ALERT_SHOW = 'ALERT/SHOW' as const;
export const ALERT_CONFIRM = 'ALERT/CONFIRM' as const;

export type Action =
  | {
      type: typeof ALERT_SHOW;
      alert: Alert;
    }
  | {
      type: typeof ALERT_CONFIRM;
      eventId: string;
    };
