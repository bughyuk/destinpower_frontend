import {
  Action,
  ALERT_CONFIRM,
  ALERT_SHOW,
  AlertState,
} from '@/modules/alert/types';

export function alertReducer(state: AlertState, action: Action) {
  switch (action.type) {
    case ALERT_SHOW:
      return [...state, action.alert];
    case ALERT_CONFIRM:
      return state.filter((alert) => alert.eventId !== action.eventId);
  }
}
