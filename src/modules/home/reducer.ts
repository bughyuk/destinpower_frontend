import {
  Action,
  HOME_DATA_CLEAR,
  HOME_SET_DATA_GROUP,
  HOME_SET_DATA_USER,
  HOME_SET_MENU_ACTIVE,
  HomeState,
} from '@/modules/home/types';

export function homeReducer(state: HomeState, action: Action) {
  switch (action.type) {
    case HOME_SET_MENU_ACTIVE:
      return {
        ...state,
        menus: state.menus.map((menu) => {
          let active = false;
          if (menu.menuIdx === action.menuIdx) {
            active = true;
          }

          menu.subMenus = menu.subMenus.map((subMenu) => {
            let active = false;
            if (subMenu.menuIdx === action.menuIdx) {
              active = true;
            }

            return {
              ...subMenu,
              active,
            };
          });

          return {
            ...menu,
            active,
          };
        }),
      };
    case HOME_SET_DATA_USER:
      return {
        ...state,
        data: {
          ...state.data,
          groupSettingUser: {
            userId: action.userId,
            directFlag: !state.data.groupSettingUser.directFlag,
          },
        },
      };
    case HOME_SET_DATA_GROUP:
      return {
        ...state,
        data: {
          ...state.data,
          groupSettingGroup: {
            groupSeq: action.groupSeq,
            directFlag: !state.data.groupSettingGroup.directFlag,
          },
        },
      };
    case HOME_DATA_CLEAR:
      return {
        ...state,
        data: {
          groupSettingUser: {
            ...state.data.groupSettingUser,
            userId: '',
          },
          groupSettingGroup: {
            ...state.data.groupSettingGroup,
            groupSeq: -1,
          },
        },
      };
  }
}
