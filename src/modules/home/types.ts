import { UserType } from '@/modules/user/types';

export const MENU_PROJECT = 0;
export const MENU_SPACE = 1;
export const MENU_SPACE_CUSTOM = 2;
export const MENU_SPACE_PUBLIC = 3;
export const MENU_DATA = 4;
export const MENU_GROUP_SETTINGS = 5;
export const MENU_GROUP_SETTINGS_USER = 6;
export const MENU_GROUP_SETTINGS_GROUP = 7;
export const MENU_ACCOUNT = 8;
export const MENU_ACCOUNT_PROFILE = 9;
export const MENU_ACCOUNT_SECURITY = 10;
export const MENU_LOGISTICS_MANAGEMENT = 11;

export type CollapseMenuIdx =
  | undefined
  | typeof MENU_SPACE
  | typeof MENU_GROUP_SETTINGS
  | typeof MENU_ACCOUNT;
export type MenuIdx =
  | -1
  | typeof MENU_PROJECT
  | typeof MENU_SPACE
  | typeof MENU_SPACE_CUSTOM
  | typeof MENU_SPACE_PUBLIC
  | typeof MENU_DATA
  | typeof MENU_GROUP_SETTINGS
  | typeof MENU_GROUP_SETTINGS_USER
  | typeof MENU_GROUP_SETTINGS_GROUP
  | typeof MENU_ACCOUNT
  | typeof MENU_ACCOUNT_PROFILE
  | typeof MENU_ACCOUNT_SECURITY
  | typeof MENU_LOGISTICS_MANAGEMENT;

export type SubMenu = {
  menuIdx: MenuIdx;
  titleKey: string;
  active: boolean;
  allowableUserType: UserType[];
};

export type Menu = {
  menuIdx: MenuIdx;
  iconName: string;
  titleKey: string;
  active: boolean;
  subMenus: SubMenu[];
  allowableUserType: UserType[];
};

export type GroupSettingUser = {
  userId: string;
  directFlag: boolean;
};

export type GroupSettingGroup = {
  groupSeq: number;
  directFlag: boolean;
};

export type HomeState = {
  menus: Menu[];
  data: {
    groupSettingUser: GroupSettingUser;
    groupSettingGroup: GroupSettingGroup;
  };
};

export const HOME_SET_MENU_ACTIVE = 'HOME/SET_MENU_ACTIVE' as const;
export const HOME_SET_DATA_USER = 'HOME/SET_DATA_USER' as const;
export const HOME_SET_DATA_GROUP = 'HOME/SET_DATA_GROUP' as const;
export const HOME_DATA_CLEAR = 'HOME/DATA_CLEAR' as const;

export type Action =
  | {
      type: typeof HOME_SET_MENU_ACTIVE;
      menuIdx: MenuIdx;
    }
  | {
      type: typeof HOME_SET_DATA_USER;
      userId: string;
    }
  | {
      type: typeof HOME_SET_DATA_GROUP;
      groupSeq: number;
    }
  | {
      type: typeof HOME_DATA_CLEAR;
    };
