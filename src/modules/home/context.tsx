import React, { createContext, Dispatch, ReactNode, useReducer } from 'react';
import {
  Action,
  HomeState,
  MENU_GROUP_SETTINGS,
  MENU_GROUP_SETTINGS_GROUP,
  MENU_GROUP_SETTINGS_USER,
  MENU_ACCOUNT,
  MENU_PROJECT,
  MENU_SPACE,
  MENU_SPACE_CUSTOM,
  MENU_ACCOUNT_PROFILE,
  MENU_ACCOUNT_SECURITY,
  MENU_LOGISTICS_MANAGEMENT,
} from '@/modules/home/types';
import { homeReducer } from '@/modules/home/reducer';

export const HomeStateContext = createContext<HomeState | null>(null);
type HomeDispatch = Dispatch<Action>;
export const HomeDispatchContext = createContext<HomeDispatch | null>(null);

export const initialState: HomeState = {
  menus: [
    // {
    //   menuIdx: MENU_PROJECT,
    //   titleKey: 'text_project',
    //   iconName: 'topic',
    //   active: true,
    //   subMenus: [],
    //   allowableUserType: [],
    // },
    // {
    //   menuIdx: MENU_SPACE,
    //   titleKey: 'text_space',
    //   iconName: 'map',
    //   active: false,
    //   subMenus: [
    //     {
    //       menuIdx: MENU_SPACE_CUSTOM,
    //       titleKey: 'text_custom_classification',
    //       active: false,
    //       allowableUserType: [],
    //     },
    //   ],
    //   allowableUserType: ['OWNER'],
    // },
    {
      menuIdx: MENU_LOGISTICS_MANAGEMENT,
      titleKey: '공정 관리',
      iconName: 'view_in_ar',
      active: false,
      subMenus: [],
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      menuIdx: MENU_GROUP_SETTINGS,
      titleKey: 'text_group_settings',
      iconName: 'groups',
      active: false,
      subMenus: [
        {
          menuIdx: MENU_GROUP_SETTINGS_USER,
          titleKey: 'text_user',
          active: false,
          allowableUserType: [],
        },
        {
          menuIdx: MENU_GROUP_SETTINGS_GROUP,
          titleKey: 'text_group',
          active: false,
          allowableUserType: [],
        },
      ],
      allowableUserType: ['OWNER'],
    },
    {
      menuIdx: MENU_ACCOUNT,
      titleKey: 'text_account',
      iconName: 'account_circle',
      active: false,
      subMenus: [
        {
          menuIdx: MENU_ACCOUNT_PROFILE,
          titleKey: 'text_profile',
          active: false,
          allowableUserType: [],
        },
        {
          menuIdx: MENU_ACCOUNT_SECURITY,
          titleKey: 'text_security',
          active: false,
          allowableUserType: [],
        },
      ],
      allowableUserType: [],
    },
  ],
  data: {
    groupSettingUser: {
      userId: '',
      directFlag: false,
    },
    groupSettingGroup: {
      groupSeq: -1,
      directFlag: false,
    },
  },
};

export function HomeContextProvider({ children }: { children: ReactNode }) {
  const [home, dispatch] = useReducer(homeReducer, initialState);

  return (
    <HomeDispatchContext.Provider value={dispatch}>
      <HomeStateContext.Provider value={home}>
        {children}
      </HomeStateContext.Provider>
    </HomeDispatchContext.Provider>
  );
}
