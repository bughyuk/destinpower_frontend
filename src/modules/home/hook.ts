import { useContext, useMemo } from 'react';
import { HomeDispatchContext, HomeStateContext } from '@/modules/home/context';
import {
  HOME_DATA_CLEAR,
  HOME_SET_DATA_GROUP,
  HOME_SET_DATA_USER,
  HOME_SET_MENU_ACTIVE,
  Menu,
  MenuIdx,
} from '@/modules/home/types';
import { useUser } from '@/modules/user/hook';

function useState() {
  const state = useContext(HomeStateContext);
  if (!state) throw new Error('HomeProvider not found');
  return state;
}

function useDispatch() {
  const dispatch = useContext(HomeDispatchContext);
  if (!dispatch) throw new Error('HomeProvider not found');
  return dispatch;
}

export function useHomeMenu() {
  const { user } = useUser();
  const { menus } = useState();
  const dispatch = useDispatch();

  const handleSetMenu = (menuIdx: MenuIdx) => {
    dispatch({
      type: HOME_SET_MENU_ACTIVE,
      menuIdx,
    });
  };

  const activeMenuIdx: MenuIdx = useMemo(() => {
    let menuIdx = -1;
    const activeMenu = menus.find((menu) => menu.active);
    if (activeMenu) {
      menuIdx = activeMenu.menuIdx;
    }

    if (menuIdx === -1) {
      menus.some((menu) => {
        const activeSubMenu = menu.subMenus.find((subMenu) => subMenu.active);
        if (activeSubMenu) {
          menuIdx = activeSubMenu.menuIdx;
          return true;
        }
      });
    }

    return menuIdx as MenuIdx;
  }, [menus]);

  const filterMenus: Menu[] = useMemo(() => {
    if (user.userType) {
      return menus.filter((menu) => {
        menu.subMenus = menu.subMenus.filter((subMenu) => {
          if (subMenu.allowableUserType.length) {
            return subMenu.allowableUserType.includes(user.userType);
          } else {
            return true;
          }
        });

        if (menu.allowableUserType.length) {
          return menu.allowableUserType.includes(user.userType);
        } else {
          return true;
        }
      });
    }
    return [];
  }, [menus, user.userId, user.userType]);

  return {
    menus: filterMenus,
    activeMenuIdx,
    handleSetMenu,
  };
}

export function useHomeData() {
  const { data } = useState();
  const dispatch = useDispatch();
  const { groupSettingUser, groupSettingGroup } = data;

  const handleSetDataUser = (userId: string) => {
    dispatch({
      type: HOME_SET_DATA_USER,
      userId,
    });
  };

  const handleSetDataGroup = (groupSeq: number) => {
    dispatch({
      type: HOME_SET_DATA_GROUP,
      groupSeq,
    });
  };

  const handleDataClear = () => {
    dispatch({
      type: HOME_DATA_CLEAR,
    });
  };

  return {
    groupSettingUser,
    groupSettingGroup,
    handleSetDataUser,
    handleSetDataGroup,
    handleDataClear,
  };
}
