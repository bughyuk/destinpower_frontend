import React, { createContext, Dispatch, ReactNode, useReducer } from 'react';
import { Action, SpaceState } from '@/modules/space/types';
import { spaceReducer } from '@/modules/space/reducer';
import {
  SPACE_FILE_REGISTER_MODE,
  PANE_STATUS_DETAIL,
} from '@/utils/constants/common';

export const SpaceStateContext = createContext<SpaceState | null>(null);
type SpaceDispatch = Dispatch<Action>;
export const SpaceDispatchContext = createContext<SpaceDispatch | null>(null);

const initialState: SpaceState = {
  paneStatus: PANE_STATUS_DETAIL,
  openLayers: {
    map: null,
    drawSource: null,
    interaction: null,
    snapGuides: null,
    geoImage: {
      layer: null,
      draw: () => {
        //
      },
      drawFromSquare: () => {
        //
      },
      remove: () => {
        //
      },
    },
    draw: {
      polygon: null,
      lineString: null,
      point: null,
      square: null,
    },
    geofencingLayer: null,
  },
  register: {
    connectProjectId: null,
    address: {
      showPane: false,
      selectedDo: '',
      selectedSubDistrict: '',
      selectedLngLat: null,
      detailedAddress: '',
    },
    file: {
      mode: SPACE_FILE_REGISTER_MODE,
    },
  },
  info: {
    space: {
      id: '',
      name: '',
      address: '',
      location: {
        lng: 0,
        lat: 0,
      },
    },
    floors: {
      id: '',
      name: '',
      value: 0,
      fileName: '',
      oriFileName: '',
      cx: 0,
      cy: 0,
      scalex: 0,
      scaley: 0,
      rotation: 0,
    },
  },
  additional: {
    customPaneContainer: null,
  },
};

export function SpaceContextProvider({ children }: { children: ReactNode }) {
  const [space, dispatch] = useReducer(spaceReducer, initialState);

  return (
    <SpaceDispatchContext.Provider value={dispatch}>
      <SpaceStateContext.Provider value={space}>
        {children}
      </SpaceStateContext.Provider>
    </SpaceDispatchContext.Provider>
  );
}
