import { useContext } from 'react';
import {
  SpaceDispatchContext,
  SpaceStateContext,
} from '@/modules/space/context';
import {
  SPACE_CHANGE_PANE_STATUS,
  SPACE_CHANGE_SHOW_ADDRESS_PANE,
  SPACE_REMOVE_OPEN_LAYERS_INTERACTION,
  SPACE_SET_ADDRESS,
  SPACE_SET_CUSTOM_PANE_CONTAINER,
  SPACE_SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL,
  SPACE_SET_SPACE_INFO,
  SPACE_SET_OPEN_LAYERS,
  SPACE_SET_OPEN_LAYERS_INTERACTION,
  SPACE_SET_FLOORS_INFO,
  DrawGeoImage,
  OpenLayers,
  SpaceInfo,
  FloorsInfo,
  SPACE_CHANGE_FILE_MODE,
  SPACE_SET_CONNECT_PROJECT_ID,
  RegisterSelectedAddress,
} from '@/modules/space/types';
import { SpaceFileMode, PaneStatus } from '@/utils/constants/common';
import { Map } from 'ol';
import { Vector as SourceVector } from 'ol/source';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Interaction } from 'ol/interaction';

function useState() {
  const state = useContext(SpaceStateContext);
  if (!state) throw new Error('SpaceProvider not found');
  return state;
}

function useDispatch() {
  const dispatch = useContext(SpaceDispatchContext);
  if (!dispatch) throw new Error('SpaceProvider not found');
  return dispatch;
}

export function useOpenLayers() {
  const state = useState();
  const dispatch = useDispatch();

  const openLayers = state.openLayers;
  const draw = openLayers.draw;
  const map = openLayers.map;
  const drawSource = openLayers.drawSource;
  const geoImage = openLayers.geoImage;
  const geofencingLayer = openLayers.geofencingLayer;

  const handleSetOpenLayers = (openLayers: OpenLayers) => {
    dispatch({
      type: SPACE_SET_OPEN_LAYERS,
      openLayers,
    });
  };

  const handleRemoveInteraction = () => {
    if (openLayers.interaction) {
      dispatch({
        type: SPACE_REMOVE_OPEN_LAYERS_INTERACTION,
      });
      map?.removeInteraction(openLayers.interaction);
      map?.removeInteraction(openLayers.snapGuides);
    }
  };

  const setInteraction = (interaction: Interaction) => {
    handleRemoveInteraction();
    dispatch({
      type: SPACE_SET_OPEN_LAYERS_INTERACTION,
      interaction,
    });
    map?.addInteraction(interaction);
  };

  const handleDrawPolygon = () => {
    const interaction = draw.polygon!;
    setInteraction(interaction);
  };

  const handleDrawLineString = () => {
    const interaction = draw.lineString!;
    setInteraction(interaction);
    map?.addInteraction(openLayers.snapGuides);
  };

  const handleDrawPoint = () => {
    const interaction = draw.point!;
    setInteraction(interaction);
  };

  const handleClearDrawSource = () => {
    drawSource?.clear();
  };

  const handleAllClear = () => {
    geoImage.remove();
  };

  return {
    map,
    drawSource,
    geoImage,
    geofencingLayer,
    draw,
    handleSetOpenLayers,
    handleDrawPolygon,
    handleDrawLineString,
    handleDrawPoint,
    handleRemoveInteraction,
    handleClearDrawSource,
    handleAllClear,
  };
}

export function useSpacePane() {
  const state = useState();
  const dispatch = useDispatch();

  const { paneStatus } = state;

  const handleChangePaneStatus = (paneStatus: PaneStatus) => {
    dispatch({
      type: SPACE_CHANGE_PANE_STATUS,
      paneStatus,
    });
  };

  return {
    paneStatus,
    handleChangePaneStatus,
  };
}

export function useSpace() {
  const state = useState();
  const dispatch = useDispatch();

  const info = state.info;

  const handleSetSpaceInfo = (info: SpaceInfo) => {
    dispatch({
      type: SPACE_SET_SPACE_INFO,
      info,
    });
  };

  const handleSetFloorsInfo = (info: FloorsInfo) => {
    dispatch({
      type: SPACE_SET_FLOORS_INFO,
      info,
    });
  };

  return {
    info,
    handleSetSpaceInfo,
    handleSetFloorsInfo,
  };
}

export function useSpaceRegister() {
  const state = useState();
  const dispatch = useDispatch();

  const handleChangeShowAddressPane = (show: boolean) => {
    dispatch({
      type: SPACE_CHANGE_SHOW_ADDRESS_PANE,
      show,
    });
  };

  const handleSetAddress = (selectedAddress: RegisterSelectedAddress) => {
    dispatch({
      type: SPACE_SET_ADDRESS,
      selectedAddress,
    });
  };

  const handleSetCustomPaneContainer = (container: PerfectScrollbar) => {
    dispatch({
      type: SPACE_SET_CUSTOM_PANE_CONTAINER,
      container,
    });
  };

  const handleCustomPaneUpdateScroll = () => {
    dispatch({
      type: SPACE_SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL,
    });
  };

  const handleChangeFileMode = (mode: SpaceFileMode) => {
    dispatch({
      type: SPACE_CHANGE_FILE_MODE,
      mode,
    });
  };

  const handleSetConnectProjectId = (connectProjectId: string | null) => {
    dispatch({
      type: SPACE_SET_CONNECT_PROJECT_ID,
      connectProjectId,
    });
  };

  const { connectProjectId, address, file } = state.register;
  const {
    selectedDo,
    selectedSubDistrict,
    selectedLngLat,
    detailedAddress,
    showPane,
  } = address;
  const { mode } = file;

  return {
    showPane,
    selectedDo,
    selectedSubDistrict,
    selectedLngLat,
    detailedAddress,
    mode,
    connectProjectId,
    handleSetConnectProjectId,
    handleChangeShowAddressPane,
    handleSetAddress,
    handleSetCustomPaneContainer,
    handleCustomPaneUpdateScroll,
    handleChangeFileMode,
  };
}
