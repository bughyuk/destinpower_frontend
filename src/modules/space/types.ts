import { SpaceFileMode, PaneStatus } from '@/utils/constants/common';
import Map from 'ol/Map';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Vector as VectorSource } from 'ol/source';
import { Interaction } from 'ol/interaction';
import { LngLat } from '@/modules/map/types';
import { Feature } from 'ol';
import { Image } from 'ol/layer';

export type OpenLayers = {
  map: Map;
  drawSource: VectorSource;
  drawPolygon: Interaction;
  drawLine: Interaction;
  drawPoint: Interaction;
  drawSquare: Interaction;
  geoImageLayer: any;
  drawGeoImage: (options: DrawGeoImage) => void;
  drawGeoImageFromSquare: (
    options: DrawGeoImage,
    squareFeature: Feature
  ) => void;
  removeGeoImage: () => void;
  snapGuides: any;
  geofencingLayer: Image;
};

export type JapanLngLat = {
  経度: number;
  緯度: number;
};

export type ZipCodeAddress = {
  xpos: number;
  ypos: number;
  pre: string;
  city: string;
  addr: string;
};

export type DrawGeoImage = {
  url: string;
  imageCenter: number[];
  imageScale: number[];
  imageRotate: number;
};

export const SPACE_COUNTRY_CODE_JP = 81;
export const SPACE_COUNTRY_CODE_KR = 82;

export type SpaceCountryCode =
  | typeof SPACE_COUNTRY_CODE_JP
  | typeof SPACE_COUNTRY_CODE_KR;

export type SpaceInfo = {
  id: string;
  name?: string;
  countryCode?: string;
  country?: string;
  city?: string;
  address?: string;
  location?: LngLat;
};

export const FloorsInfoInitialState: FloorsInfo = {
  id: '',
  name: '',
  value: 0,
  fileName: '',
  oriFileName: '',
  cx: 0,
  cy: 0,
  scalex: 0,
  scaley: 0,
  rotation: 0,
};

export type FloorsInfo = {
  id: string;
  name: string;
  value: number;
  fileName: string;
  oriFileName: string;
  cx: number;
  cy: number;
  scalex: number;
  scaley: number;
  rotation: number;
};

export type UploadFloorsInfo = {
  map_id: string;
  filename: string;
};

export type PoiInfo = {
  poi_category: string;
  poi_id: string;
  poi_name: string;
  geom_type: string;
  geomstr: string;
  filenm: string;
  xcoord: number;
  ycoord: number;
};

export type PoiSearchInfo = {
  id: string;
  poi_name?: string;
  xpos: number;
  ypos: number;
  area_geom?: string;
};

export type RegisterSelectedAddress = {
  selectedDo: string;
  selectedSubDistrict: string;
  selectedLngLat: LngLat | null;
  detailedAddress: string;
};

export type SpaceState = {
  paneStatus: PaneStatus;
  openLayers: {
    map: Map | null;
    drawSource: VectorSource | null;
    interaction: Interaction | null;
    snapGuides: any | null;
    geoImage: {
      layer: any | null;
      draw: (() => void) | ((options: DrawGeoImage, editing?: boolean) => void);
      drawFromSquare:
        | (() => void)
        | ((options: DrawGeoImage, squareFeature: Feature) => void);
      remove: () => void;
    };
    draw: {
      polygon: Interaction | null;
      lineString: Interaction | null;
      point: Interaction | null;
      square: Interaction | null;
    };
    geofencingLayer: Image | null;
  };
  register: {
    connectProjectId: string | null;
    address: RegisterSelectedAddress & {
      showPane: boolean;
    };
    file: {
      mode: SpaceFileMode;
    };
  };
  info: {
    space: SpaceInfo;
    floors: FloorsInfo;
  };
  additional: {
    customPaneContainer: PerfectScrollbar | null;
  };
};

export const SPACE_CHANGE_PANE_STATUS = 'SPACE/CHANGE_PANE_STATUS' as const;
export const SPACE_CHANGE_SHOW_ADDRESS_PANE = 'SPACE/CHANGE_SHOW_ADDRESS_PANE' as const;
export const SPACE_SET_CONNECT_PROJECT_ID = 'SPACE/SET_CONNECT_PROJECT_ID' as const;
export const SPACE_SET_ADDRESS = 'SPACE/SET_ADDRESS' as const;
export const SPACE_SET_OPEN_LAYERS = 'SPACE/SET_OPEN_LAYERS' as const;
export const SPACE_SET_CUSTOM_PANE_CONTAINER = 'SPACE/SET_CUSTOM_PANE_CONTAINER' as const;
export const SPACE_SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL = 'SPACE/SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL' as const;
export const SPACE_SET_OPEN_LAYERS_INTERACTION = 'SPACE/SET_OPEN_LAYERS_INTERACTION' as const;
export const SPACE_REMOVE_OPEN_LAYERS_INTERACTION = 'SPACE/REMOVE_OPEN_LAYERS_INTERACTION' as const;
export const SPACE_SET_SPACE_INFO = 'SPACE/SET_SPACE_INFO' as const;
export const SPACE_SET_FLOORS_INFO = 'SPACE/SET_FLOORS_INFO' as const;
export const SPACE_CHANGE_FILE_MODE = 'SPACE/CHANGE_FILE_MODE' as const;

export type Action =
  | {
      type: typeof SPACE_CHANGE_PANE_STATUS;
      paneStatus: PaneStatus;
    }
  | {
      type: typeof SPACE_CHANGE_SHOW_ADDRESS_PANE;
      show: boolean;
    }
  | {
      type: typeof SPACE_SET_CONNECT_PROJECT_ID;
      connectProjectId: string | null;
    }
  | {
      type: typeof SPACE_SET_ADDRESS;
      selectedAddress: RegisterSelectedAddress;
    }
  | {
      type: typeof SPACE_SET_OPEN_LAYERS;
      openLayers: OpenLayers;
    }
  | {
      type: typeof SPACE_SET_CUSTOM_PANE_CONTAINER;
      container: PerfectScrollbar;
    }
  | {
      type: typeof SPACE_SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL;
    }
  | {
      type: typeof SPACE_SET_OPEN_LAYERS_INTERACTION;
      interaction: Interaction;
    }
  | {
      type: typeof SPACE_REMOVE_OPEN_LAYERS_INTERACTION;
    }
  | {
      type: typeof SPACE_SET_SPACE_INFO;
      info: SpaceInfo;
    }
  | {
      type: typeof SPACE_SET_FLOORS_INFO;
      info: FloorsInfo;
    }
  | {
      type: typeof SPACE_CHANGE_FILE_MODE;
      mode: SpaceFileMode;
    };
