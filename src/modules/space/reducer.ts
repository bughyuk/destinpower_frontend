import {
  Action,
  SPACE_CHANGE_PANE_STATUS,
  SPACE_CHANGE_SHOW_ADDRESS_PANE,
  SPACE_REMOVE_OPEN_LAYERS_INTERACTION,
  SPACE_SET_ADDRESS,
  SPACE_SET_CUSTOM_PANE_CONTAINER,
  SPACE_SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL,
  SPACE_SET_SPACE_INFO,
  SPACE_SET_OPEN_LAYERS,
  SPACE_SET_OPEN_LAYERS_INTERACTION,
  SpaceState,
  SPACE_SET_FLOORS_INFO,
  SPACE_CHANGE_FILE_MODE,
  SPACE_SET_CONNECT_PROJECT_ID,
} from '@/modules/space/types';

export function spaceReducer(state: SpaceState, action: Action) {
  switch (action.type) {
    case SPACE_SET_CUSTOM_PANE_CONTAINER:
      return {
        ...state,
        additional: {
          ...state.additional,
          customPaneContainer: action.container,
        },
      };
    case SPACE_SET_CUSTOM_PANE_CONTAINER_UPDATE_SCROLL:
      const customPaneContainer = state.additional.customPaneContainer as any;
      if (customPaneContainer._container) {
        state.additional.customPaneContainer?.updateScroll();
      }
      return {
        ...state,
      };
    case SPACE_CHANGE_PANE_STATUS:
      return {
        ...state,
        paneStatus: action.paneStatus,
      };
    case SPACE_CHANGE_SHOW_ADDRESS_PANE:
      return {
        ...state,
        register: {
          ...state.register,
          address: {
            ...state.register.address,
            showPane: action.show,
          },
        },
      };
    case SPACE_SET_CONNECT_PROJECT_ID:
      return {
        ...state,
        register: {
          ...state.register,
          connectProjectId: action.connectProjectId,
        },
      };
    case SPACE_SET_ADDRESS:
      return {
        ...state,
        register: {
          ...state.register,
          address: {
            ...state.register.address,
            ...action.selectedAddress,
          },
        },
      };
    case SPACE_SET_OPEN_LAYERS:
      const openLayers = action.openLayers;
      return {
        ...state,
        openLayers: {
          ...state.openLayers,
          map: openLayers.map,
          drawSource: openLayers.drawSource,
          snapGuides: openLayers.snapGuides,
          geoImage: {
            layer: openLayers.geoImageLayer,
            draw: openLayers.drawGeoImage,
            drawFromSquare: openLayers.drawGeoImageFromSquare,
            remove: openLayers.removeGeoImage,
          },
          draw: {
            polygon: openLayers.drawPolygon,
            lineString: openLayers.drawLine,
            point: openLayers.drawPoint,
            square: openLayers.drawSquare,
          },
          geofencingLayer: openLayers.geofencingLayer,
        },
      };
    case SPACE_SET_OPEN_LAYERS_INTERACTION:
      return {
        ...state,
        openLayers: {
          ...state.openLayers,
          interaction: action.interaction,
        },
      };
    case SPACE_REMOVE_OPEN_LAYERS_INTERACTION:
      return {
        ...state,
        openLayers: {
          ...state.openLayers,
          interaction: null,
        },
      };
    case SPACE_SET_SPACE_INFO:
      return {
        ...state,
        info: {
          ...state.info,
          space: {
            ...state.info.space,
            ...action.info,
          },
        },
      };
    case SPACE_SET_FLOORS_INFO:
      return {
        ...state,
        info: {
          ...state.info,
          floors: {
            id: action.info.id,
            name: action.info.name,
            value: action.info.value,
            fileName: action.info.fileName,
            oriFileName: action.info.oriFileName,
            cx: action.info.cx,
            cy: action.info.cy,
            scalex: action.info.scalex,
            scaley: action.info.scaley,
            rotation: action.info.rotation,
          },
        },
      };
    case SPACE_CHANGE_FILE_MODE:
      return {
        ...state,
        register: {
          ...state.register,
          file: {
            mode: action.mode,
          },
        },
      };
  }
}
