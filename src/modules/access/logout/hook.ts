import { useEffect, useReducer } from 'react';
import {
  ASYNC_DEFAULT_STATE,
  COOKIE_KEY_WHETHER_TO_SIGN_IN,
  SESSION_STORAGE_KEY_USER_INFO,
} from '@/utils/constants/common';
import { useCookies } from 'react-cookie';
import {
  logoutReducer,
  removeAccessToken,
} from '@/modules/access/logout/reducer';
import axios from 'axios';
import { useUser } from '@/modules/user/hook';
import { useHomeMenu } from '@/modules/home/hook';
import { MENU_PROJECT } from '@/modules/home/types';
import { User } from '@/modules/user/types';

export function useLogout() {
  const initialState = ASYNC_DEFAULT_STATE;
  const [state, dispatch] = useReducer(logoutReducer, initialState);
  const { user, handleUserSet } = useUser();
  const { handleSetMenu } = useHomeMenu();
  const { loading, data: logoutStatusCode, error } = state;

  const [cookies, setCookie, removeCookie] = useCookies([
    COOKIE_KEY_WHETHER_TO_SIGN_IN,
  ]);

  const handleLogout = () => {
    removeAccessToken(dispatch, {
      userId: user?.userId,
    });
  };

  useEffect(() => {
    if (logoutStatusCode) {
      handleClearUser();
    }
  }, [logoutStatusCode]);

  const handleClearUser = () => {
    removeCookie(COOKIE_KEY_WHETHER_TO_SIGN_IN);
    sessionStorage.removeItem(SESSION_STORAGE_KEY_USER_INFO);
    delete axios.defaults.headers.common['Authorization'];
    handleUserSet({} as User);
    handleSetMenu(MENU_PROJECT);
  };

  return {
    loading,
    logoutStatusCode,
    error,
    handleLogout,
    handleClearUser,
  };
}
