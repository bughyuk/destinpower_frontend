import { ReactNode } from 'react';
import { AsyncState } from '@/modules/common';

export type SignUp = {
  userName: string;
  userId: string;
  password: string;
  companyName?: string;
};

export type SignUpState = {
  terms: {
    service: boolean;
    privacy: boolean;
    location: boolean;
  };
  inputs: {
    name: string;
    email: string;
    password: string;
    rePassword: string;
  };
  validInputs: {
    name: boolean;
    email: boolean;
    duplicate: boolean;
    password: boolean;
    rePassword: boolean;
  };
  duplicateCheckEmail: {
    checkedEmail: boolean;
    checkPossible: boolean;
    emailPossible: boolean;
    pass: boolean;
  };
  modal: {
    show: boolean;
    title: string;
    content: ReactNode;
  };
  api: AsyncState<number>;
};

export const ACCESS_SIGN_UP = 'ACCESS/SIGN_UP' as const;
export const ACCESS_SIGN_UP_SUCCESS = 'ACCESS/SIGN_UP_SUCCESS' as const;
export const ACCESS_SIGN_UP_ERROR = 'ACCESS/SIGN_UP_ERROR' as const;
export const ACCESS_SIGN_UP_VALIDATION_SET = 'ACCESS/SIGN_UP_VALIDATION_SET' as const;
export const ACCESS_SIGN_UP_INPUT_CHANGE = 'ACCESS/SIGN_UP_INPUT_CHANGE' as const;
export const ACCESS_SIGN_UP_TERM_CHECK_CHANGE = 'ACCESS/SIGN_UP_TERM_CHECK_CHANGE' as const;
export const ACCESS_SIGN_UP_ALL_TERM_CHECK_CHANGE = 'ACCESS/SIGN_UP_ALL_TERM_CHECK_CHANGE' as const;
export const ACCESS_SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL = 'ACCESS/SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL' as const;
export const ACCESS_SIGN_UP_POSSIBLE_EMAIL = 'ACCESS/ACCESS_SIGN_UP_POSSIBLE_EMAIL' as const;
export const ACCESS_SIGN_UP_MODAL_SET = 'ACCESS/SIGN_UP_MODAL_SET' as const;
export const ACCESS_SIGN_UP_MODAL_CLOSE = 'ACCESS/SIGN_UP_MODAL_CLOSE' as const;

export type Action =
  | {
      type: typeof ACCESS_SIGN_UP;
      data: SignUp;
    }
  | {
      type: typeof ACCESS_SIGN_UP_SUCCESS;
      data: number;
    }
  | {
      type: typeof ACCESS_SIGN_UP_ERROR;
      error: Error | null;
    }
  | {
      type: typeof ACCESS_SIGN_UP_VALIDATION_SET;
      valid: {
        name: boolean;
        email: boolean;
        duplicate: boolean;
        password: boolean;
        rePassword: boolean;
      };
    }
  | {
      type: typeof ACCESS_SIGN_UP_INPUT_CHANGE;
      key: string;
      value: string;
    }
  | {
      type: typeof ACCESS_SIGN_UP_TERM_CHECK_CHANGE;
      key: string;
      checked: boolean;
    }
  | {
      type: typeof ACCESS_SIGN_UP_ALL_TERM_CHECK_CHANGE;
      checked: boolean;
    }
  | {
      type: typeof ACCESS_SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL;
      checkPossible: boolean;
    }
  | {
      type: typeof ACCESS_SIGN_UP_POSSIBLE_EMAIL;
      emailPossible: boolean;
    }
  | {
      type: typeof ACCESS_SIGN_UP_MODAL_SET;
      data: {
        title: string;
        content: ReactNode;
      };
    }
  | {
      type: typeof ACCESS_SIGN_UP_MODAL_CLOSE;
    };
