import {
  ACCESS_SIGN_UP_ALL_TERM_CHECK_CHANGE,
  ACCESS_SIGN_UP_INPUT_CHANGE,
  ACCESS_SIGN_UP_MODAL_CLOSE,
  ACCESS_SIGN_UP_MODAL_SET,
  ACCESS_SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL,
  ACCESS_SIGN_UP_POSSIBLE_EMAIL,
  ACCESS_SIGN_UP_TERM_CHECK_CHANGE,
  ACCESS_SIGN_UP_VALIDATION_SET,
  SignUpState,
} from '@/modules/access/signup/types';
import { ASYNC_DEFAULT_STATE } from '@/utils/constants/common';
import React, { ReactNode, useEffect, useReducer } from 'react';
import { createUser, signUpReducer } from '@/modules/access/signup/reducer';
import { ValidUtils } from '@/utils';
import { fetchUserId } from '@/api/user';

export function useSignUp() {
  const initialState: SignUpState = {
    inputs: {
      email: '',
      name: '',
      password: '',
      rePassword: '',
    },
    validInputs: {
      email: true,
      duplicate: true,
      name: true,
      password: true,
      rePassword: true,
    },
    duplicateCheckEmail: {
      checkedEmail: false,
      checkPossible: false,
      emailPossible: false,
      pass: false,
    },
    terms: {
      privacy: false,
      service: false,
      location: false,
    },
    modal: {
      show: false,
      title: '',
      content: undefined,
    },
    api: ASYNC_DEFAULT_STATE,
  };
  const [state, dispatch] = useReducer(signUpReducer, initialState);

  const { name, email, password, rePassword } = state.inputs;
  const {
    name: validName,
    email: validEmail,
    duplicate: validDuplicate,
    password: validPassword,
    rePassword: validRePassword,
  } = state.validInputs;

  const {
    checkPossible,
    emailPossible,
    checkedEmail,
    pass,
  } = state.duplicateCheckEmail;

  const { privacy, service, location } = state.terms;
  const {
    title: modalTitle,
    content: modalContent,
    show: modalShow,
  } = state.modal;
  const { loading, data: signUpStatusCode, error } = state.api;

  const validForm = (): boolean => {
    let validName = true;
    let validEmail = true;
    let validDuplicate = true;
    let validPassword = true;
    let validRePassword = true;

    const { name, email, password, rePassword } = state.inputs;

    if (!name) {
      validName = false;
    }

    if (
      !email ||
      !(email.length >= 5 && email.length <= 45) ||
      !ValidUtils.validateEmail(email)
    ) {
      validEmail = false;
    }

    if (!password || !ValidUtils.validatePassword(password)) {
      validPassword = false;
    }

    if (!rePassword || password !== rePassword) {
      validRePassword = false;
    }

    if (!checkedEmail) {
      validDuplicate = false;
    }

    dispatch({
      type: ACCESS_SIGN_UP_VALIDATION_SET,
      valid: {
        name: validName,
        email: validEmail,
        duplicate: validDuplicate,
        password: validPassword,
        rePassword: validRePassword,
      },
    });

    return (
      validName &&
      validEmail &&
      validPassword &&
      validRePassword &&
      checkedEmail &&
      pass
    );
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    const isValid = validForm();

    if (isValid) {
      createUser(dispatch, {
        userName: name,
        userId: email,
        password: password,
      });
    }
  };

  const handleModal = (title: string, content: ReactNode) => {
    dispatch({
      type: ACCESS_SIGN_UP_MODAL_SET,
      data: {
        title,
        content,
      },
    });
  };

  const handleModalClose = () => {
    dispatch({
      type: ACCESS_SIGN_UP_MODAL_CLOSE,
    });
  };

  const handleInputsChange = (key: string, value: string) => {
    if (key === 'email') {
      let checkPossible = false;

      if (
        value.length >= 5 &&
        value.length <= 45 &&
        ValidUtils.validateEmail(value)
      ) {
        checkPossible = true;
      }

      dispatch({
        type: ACCESS_SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL,
        checkPossible,
      });
    }

    dispatch({
      type: ACCESS_SIGN_UP_INPUT_CHANGE,
      key,
      value,
    });
  };

  const handleTermChange = (key: string, checked: boolean) => {
    dispatch({
      type: ACCESS_SIGN_UP_TERM_CHECK_CHANGE,
      key,
      checked,
    });
  };

  const handleAllTermChange = (checked: boolean) => {
    dispatch({
      type: ACCESS_SIGN_UP_ALL_TERM_CHECK_CHANGE,
      checked,
    });
  };

  const handleCheckUserId = async () => {
    const emailPossible = await fetchUserId(email);
    dispatch({
      type: ACCESS_SIGN_UP_POSSIBLE_EMAIL,
      emailPossible: !emailPossible,
    });
  };

  return {
    name,
    email,
    password,
    rePassword,
    validName,
    validEmail,
    validDuplicate,
    validPassword,
    validRePassword,
    privacy,
    service,
    location,
    modalTitle,
    modalContent,
    modalShow,
    loading,
    signUpStatusCode,
    error,
    checkedEmail,
    checkPossible,
    pass,
    emailPossible,
    handleSubmit,
    handleModal,
    handleModalClose,
    handleInputsChange,
    handleTermChange,
    handleAllTermChange,
    handleCheckUserId,
  };
}
