import {
  ACCESS_SIGN_UP,
  ACCESS_SIGN_UP_ALL_TERM_CHECK_CHANGE,
  ACCESS_SIGN_UP_ERROR,
  ACCESS_SIGN_UP_INPUT_CHANGE,
  ACCESS_SIGN_UP_MODAL_CLOSE,
  ACCESS_SIGN_UP_MODAL_SET,
  ACCESS_SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL,
  ACCESS_SIGN_UP_POSSIBLE_EMAIL,
  ACCESS_SIGN_UP_SUCCESS,
  ACCESS_SIGN_UP_TERM_CHECK_CHANGE,
  ACCESS_SIGN_UP_VALIDATION_SET,
  Action,
  SignUpState,
} from '@/modules/access/signup/types';
import {
  ASYNC_ERROR_STATE,
  ASYNC_LOADING_STATE,
  ASYNC_SUCCESS_STATE,
} from '@/utils/constants/common';
import createAsyncDispatcher from '@/utils/asyncActionUtils';
import { postUser } from '@/api/user';

export function signUpReducer(state: SignUpState, action: Action) {
  switch (action.type) {
    case ACCESS_SIGN_UP:
      return {
        ...state,
        api: {
          ...ASYNC_LOADING_STATE,
        },
      };
    case ACCESS_SIGN_UP_SUCCESS:
      return {
        ...state,
        api: {
          ...ASYNC_SUCCESS_STATE(action.data),
        },
      };
    case ACCESS_SIGN_UP_ERROR:
      return {
        ...state,
        api: {
          ...ASYNC_ERROR_STATE(action.error),
        },
      };
    case ACCESS_SIGN_UP_VALIDATION_SET:
      return {
        ...state,
        validInputs: {
          ...action.valid,
        },
      };
    case ACCESS_SIGN_UP_INPUT_CHANGE:
      return {
        ...state,
        inputs: {
          ...state.inputs,
          [action.key]: action.value,
        },
      };
    case ACCESS_SIGN_UP_TERM_CHECK_CHANGE:
      return {
        ...state,
        terms: {
          ...state.terms,
          [action.key]: action.checked,
        },
      };
    case ACCESS_SIGN_UP_ALL_TERM_CHECK_CHANGE:
      return {
        ...state,
        terms: {
          service: action.checked,
          privacy: action.checked,
          location: action.checked,
        },
      };
    case ACCESS_SIGN_UP_POSSIBLE_DUPLICATE_CHECK_EMAIL:
      return {
        ...state,
        validInputs: {
          ...state.validInputs,
          duplicate: true,
        },
        duplicateCheckEmail: {
          checkedEmail: false,
          checkPossible: action.checkPossible,
          emailPossible: false,
          pass: false,
        },
      };
    case ACCESS_SIGN_UP_POSSIBLE_EMAIL:
      return {
        ...state,
        validInputs: {
          ...state.validInputs,
          duplicate: true,
        },
        duplicateCheckEmail: {
          ...state.duplicateCheckEmail,
          checkedEmail: true,
          emailPossible: action.emailPossible,
          pass: action.emailPossible,
        },
      };
    case ACCESS_SIGN_UP_MODAL_SET:
      return {
        ...state,
        modal: {
          ...action.data,
          show: true,
        },
      };
    case ACCESS_SIGN_UP_MODAL_CLOSE: {
      return {
        ...state,
        modal: {
          show: false,
          title: '',
          content: undefined,
        },
      };
    }
  }
}

export const createUser = createAsyncDispatcher(ACCESS_SIGN_UP, postUser);
