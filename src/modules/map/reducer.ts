import {
  Action,
  Location,
  MAP_ADDITIONAL_LOCATION_CHANGE,
  MAP_ADDITIONAL_LOCATION_RESET,
  MAP_ADDITIONAL_LOCATION_SET,
  MAP_CANCEL_CONTROL_USER,
  MAP_CANCEL_HISTORY_USER,
  MAP_OPEN_LAYERS_INSTANCE_SET,
  MAP_RELOAD_ACTIVATION_ACCIDENTS,
  MAP_RELOAD_ACTIVATION_EVENTS,
  MAP_RESET_CONTROL_USER,
  MAP_SET_AREA_LIST,
  MAP_SET_SPACE,
  MAP_SET_CONTROL_USER,
  MAP_SET_DIRECT_ACCIDENT_ID,
  MAP_SET_DIRECT_EVENT_ID,
  MAP_SET_DRAW_INTERACTION,
  MAP_SET_HISTORY_USER,
  MAP_SET_PROJECT,
  MAP_SET_REALTIME_USER,
  MAP_SET_SELECT_AREA,
  MAP_SET_SUMMARY,
  MapState,
} from '@/modules/map/types';

export function mapReducer(state: MapState, action: Action) {
  switch (action.type) {
    case MAP_ADDITIONAL_LOCATION_SET:
      return {
        ...state,
        additional: {
          ...state.additional,
          location: {
            feature: action.feature,
            data: action.data,
          },
        },
      };
    case MAP_ADDITIONAL_LOCATION_CHANGE:
      const feature = state.additional.location.feature;
      const data = {
        ...state.additional.location.data,
        [action.key]: action.value,
      };

      return {
        ...state,
        additional: {
          ...state.additional,
          location: {
            ...state.additional.location,
            data,
          },
        },
      };
    case MAP_ADDITIONAL_LOCATION_RESET:
      const map = state.openLayers.map;
      return {
        ...state,
        additional: {
          ...state.additional,
          location: {
            feature: null,
            data: {} as Location,
          },
        },
      };
    case MAP_OPEN_LAYERS_INSTANCE_SET:
      return {
        ...state,
        openLayers: action.openLayers,
      };
    case MAP_SET_PROJECT:
      let mataId = '';
      let mappingId = '';
      let mapId = '';
      const spaceList = action.project.spaceList;
      if (spaceList.length > 0) {
        const space = spaceList[0];
        mataId = space.id;
        mappingId = space.mappingId;
        const floorsList = space.floorsList;
        if (floorsList.length > 0) {
          const floors = floorsList[0];
          mapId = floors.id;
        }
      }

      return {
        ...state,
        space: {
          spaceMetaId: mataId,
          spaceMappingId: mappingId,
          floorsMapId: mapId,
        },
        project: action.project,
      };
    case MAP_SET_SPACE:
      const actionSpace = action.space;

      if (actionSpace.spaceMappingId && !actionSpace.spaceMetaId) {
        const spaceList = state.project.spaceList.find(
          (space) => space.mappingId === actionSpace.spaceMappingId
        );

        if (spaceList) {
          actionSpace.spaceMetaId = spaceList.id;
        }
      }

      return {
        ...state,
        space: actionSpace,
      };
    case MAP_SET_AREA_LIST:
      return {
        ...state,
        area: {
          ...state.area,
          list: action.areaList,
        },
      };
    case MAP_SET_SELECT_AREA:
      return {
        ...state,
        area: {
          ...state.area,
          selectedFeature: action.selectAreaFeature,
        },
      };
    case MAP_SET_REALTIME_USER:
      return {
        ...state,
        users: {
          ...state.users,
          realtime: action.userList,
        },
      };
    case MAP_SET_CONTROL_USER:
      const newControlUserList = [...state.users.control];
      action.controlUserList.forEach((controlUser) => {
        const exist = newControlUserList.some(
          (newControlUser) => newControlUser.accessKey === controlUser.accessKey
        );
        if (!exist) {
          newControlUserList.push(controlUser);
        }
      });

      return {
        ...state,
        users: {
          ...state.users,
          control: newControlUserList,
        },
      };
    case MAP_CANCEL_CONTROL_USER:
      return {
        ...state,
        users: {
          ...state.users,
          control: state.users.control.filter(
            (controlUser) => controlUser.accessKey !== action.accessKey
          ),
        },
      };
    case MAP_RESET_CONTROL_USER:
      return {
        ...state,
        users: {
          ...state.users,
          control: [],
        },
      };
    case MAP_SET_HISTORY_USER:
      return {
        ...state,
        users: {
          ...state.users,
          history: action.userList,
        },
      };
    case MAP_CANCEL_HISTORY_USER:
      return {
        ...state,
        users: {
          ...state.users,
          history: state.users.history.filter(
            (historyUser) => historyUser.access_key !== action.accessKey
          ),
        },
      };
    case MAP_SET_DRAW_INTERACTION:
      return {
        ...state,
        openLayers: {
          ...state.openLayers,
          draw: {
            ...state.openLayers.draw,
            interaction: action.interaction,
          },
        },
      };
    case MAP_SET_DIRECT_EVENT_ID:
      return {
        ...state,
        event: {
          ...state.event,
          directFlag: !state.event.directFlag,
          id: action.id,
        },
      };
    case MAP_RELOAD_ACTIVATION_EVENTS:
      return {
        ...state,
        event: {
          ...state.event,
          reloadFlag: !state.event.reloadFlag,
        },
      };
    case MAP_SET_DIRECT_ACCIDENT_ID:
      return {
        ...state,
        accident: {
          ...state.accident,
          directFlag: !state.accident.directFlag,
          id: action.id,
        },
      };
    case MAP_RELOAD_ACTIVATION_ACCIDENTS:
      return {
        ...state,
        accident: {
          ...state.accident,
          reloadFlag: !state.accident.reloadFlag,
        },
      };
    case MAP_SET_SUMMARY:
      return {
        ...state,
        summary: action.summary,
      };
  }
}
