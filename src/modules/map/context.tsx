import React, { createContext, Dispatch, ReactNode, useReducer } from 'react';
import { Action, Location, MapState } from '@/modules/map/types';
import { mapReducer } from '@/modules/map/reducer';

export const MapStateContext = createContext<MapState | null>(null);
type MapDispatch = Dispatch<Action>;
export const MapDispatchContext = createContext<MapDispatch | null>(null);

export const initialState: MapState = {
  space: {
    spaceMetaId: '',
    spaceMappingId: '',
    floorsMapId: '',
  },
  project: {
    id: '',
    name: '',
    note: '',
    solutionType: undefined,
    spaceList: [],
  },
  area: {
    list: [],
    selectedFeature: null,
  },
  users: {
    realtime: [],
    history: [],
    control: [],
  },
  event: {
    id: '',
    directFlag: false,
    reloadFlag: false,
  },
  accident: {
    id: '',
    directFlag: false,
    reloadFlag: false,
  },
  additional: {
    location: {
      feature: null,
      data: {} as Location,
    },
  },
  openLayers: {
    map: null,
    floorsLayer: null,
    realtimeLayer: null,
    areaSource: null,
    eventLayer: null,
    accidentEmergencyLayer: null,
    accidentFireLayer: null,
    accidentRescueLayer: null,
    accidentInfectiousDiseaseLayer: null,
    heatMapLayer: null,
    distributionLayer: null,
    apLayer: null,
    historySimulationLayer: null,
    historyTraceDistributionLayer: null,
    poiLayer: {
      icon: null,
      text: null,
    },
    zoneLayer: null,
    realtimeHeatMapLayer: null,
    draw: {
      source: null,
      layer: null,
      circle: null,
      polygon: null,
      accident: null,
    },
    floorPlan: {
      draw: undefined,
      remove: undefined,
    },
  },
  summary: {
    userCount: 0,
    accidentCount: 0,
  },
};

export function MapContextProvider({ children }: { children: ReactNode }) {
  const [map, dispatch] = useReducer(mapReducer, initialState);

  return (
    <MapDispatchContext.Provider value={dispatch}>
      <MapStateContext.Provider value={map}>
        {children}
      </MapStateContext.Provider>
    </MapDispatchContext.Provider>
  );
}
