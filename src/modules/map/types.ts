import { Feature, Map } from 'ol';
import VectorSource from 'ol/source/Vector';
import VectorLayer from 'ol/layer/Vector';
import { Interaction } from 'ol/interaction';
import { Heatmap, Image } from 'ol/layer';
import { DrawGeoImage } from '@/modules/space/types';
import { SolutionType } from '@/modules/project/types';

export type LngLat = {
  lng: number;
  lat: number;
};

export type Location = {
  areaId?: string;
  areaName?: string;
  lng?: number;
  lat?: number;
  radius?: number;
  geom?: string;
};

export type ControlProject = {
  id: string;
  name: string;
  note: string;
  solutionType: SolutionType;
  spaceList: Space[];
};

export type Floors = {
  id: string;
  name: string;
  value: number;
  cx?: number;
  cy?: number;
  filename?: string;
  rotation?: number;
  scalex?: number;
  scaley?: number;
};

export type Space = {
  mappingId: string;
  id: string;
  name: string;
  longitude: number;
  latitude: number;
  floorsList: Floors[];
  registDate: string;
};

export type ControlSpace = {
  spaceMetaId: string;
  spaceMappingId: string;
  floorsMapId: string;
};

export type ProjectRealtimeData = {
  accidentsByProject: {
    size: number;
  };
  activeUsersByFloor: {
    data: RealtimeUserData;
  };
  activeUsersByProject: {
    size: number;
  };
};

export type HistoryUser = {
  access_key: string;
  user_age: number;
  user_gender: string;
  user_id: string;
  device_type: string;
};

export type HistoryTrace = {
  date: string;
  trace: HistoryTraceArea[];
  dateLocationList: number[][];
};

export type HistoryTraceArea = {
  hour: string;
  area: string;
  sectionLocationList: number[][];
};

export type HistorySimulationDate = {
  [date: string]: number[][];
};

export type HistorySimulationUser = {
  [userId: string]: HistorySimulationDate;
};

export type RealtimeUserData = {
  [accessKey: string]: RealtimeUser;
};

export type RealtimeUser = {
  lng: number;
  lat: number;
  deviceType: string;
  userAge: number;
  userGender: string;
  accessKey: string;
  userId: string;
  userType: string;
  residenceTime: number;
};

export type UserTrace = {
  areaName: string;
  logDate: string;
  lng: number;
  lat: number;
  residenceTime: number;
  sectionPath?: number[][];
};

export type OpenLayers = {
  map: Map | null;
  floorsLayer: Image | null;
  realtimeLayer: VectorLayer | null;
  areaSource: VectorSource | null;
  eventLayer: VectorLayer | null;
  accidentEmergencyLayer: VectorLayer | null;
  accidentFireLayer: VectorLayer | null;
  accidentRescueLayer: VectorLayer | null;
  accidentInfectiousDiseaseLayer: VectorLayer | null;
  heatMapLayer: Image | null;
  distributionLayer: Image | null;
  apLayer: Image | null;
  historySimulationLayer: VectorLayer | null;
  historyTraceDistributionLayer: Image | null;
  poiLayer: {
    icon: VectorLayer | null;
    text: VectorLayer | null;
  };
  zoneLayer: Image | null;
  realtimeHeatMapLayer: Heatmap | null;
  draw: {
    interaction?: Interaction | null;
    source: VectorSource | null;
    layer: VectorLayer | null;
    polygon: Interaction | null;
    circle: Interaction | null;
    accident: Interaction | null;
  };
  floorPlan: {
    draw: ((options: DrawGeoImage) => void) | undefined;
    remove: (() => void) | undefined;
  };
};

export type Event = {
  id: string;
  directFlag: boolean;
  reloadFlag: boolean;
};

export type Accident = {
  id: string;
  directFlag: boolean;
  reloadFlag: boolean;
};

export type Users = {
  realtime: RealtimeUser[];
  history: HistoryUser[];
  control: RealtimeUser[];
};

export type Area = {
  area_category: string;
  area_id: string;
  area_name: string;
  geom_type: string;
  geomstr: string;
  areacolor: string;
  linecolor: string;
  feature: Feature;
};

export type ControlArea = {
  selectedFeature: Feature | null;
  list: Area[];
};

export type ControlSummary = {
  userCount: number;
  accidentCount: number;
};

export type MapState = {
  space: ControlSpace;
  project: ControlProject;
  area: ControlArea;
  users: Users;
  event: Event;
  accident: Accident;
  openLayers: OpenLayers;
  additional: {
    location: {
      feature: Feature | null;
      data: Location;
    };
  };
  summary: ControlSummary;
};

export const MAP_ADDITIONAL_LOCATION_SET = 'MAP/ADDITIONAL_LOCATION_SET' as const;
export const MAP_ADDITIONAL_LOCATION_CHANGE = 'MAP/ADDITIONAL_LOCATION_CHANGE' as const;
export const MAP_ADDITIONAL_LOCATION_RESET = 'MAP/ADDITIONAL_LOCATION_RESET' as const;
export const MAP_OPEN_LAYERS_INSTANCE_SET = 'MAP/OPEN_LAYERS_INSTANCE_SET' as const;
export const MAP_SET_PROJECT = 'MAP/SET_PROJECT' as const;
export const MAP_SET_SPACE = 'MAP/SET_SPACE' as const;
export const MAP_SET_AREA_LIST = 'MAP/SET_AREA_LIST' as const;
export const MAP_SET_SELECT_AREA = 'MAP/SET_SELECT_AREA' as const;
export const MAP_SET_REALTIME_USER = 'MAP/SET_REALTIME_USER' as const;
export const MAP_SET_CONTROL_USER = 'MAP/SET_CONTROL_USER' as const;
export const MAP_CANCEL_CONTROL_USER = 'MAP/CANCEL_CONTROL_USER' as const;
export const MAP_RESET_CONTROL_USER = 'MAP/RESET_CONTROL_USER' as const;
export const MAP_SET_HISTORY_USER = 'MAP/SET_HISTORY_USER' as const;
export const MAP_CANCEL_HISTORY_USER = 'MAP/CANCEL_HISTORY_USER' as const;
export const MAP_SET_DRAW_INTERACTION = 'MAP/SET_DRAW_INTERACTION' as const;
export const MAP_SET_DIRECT_EVENT_ID = 'MAP/SET_DIRECT_EVENT_ID' as const;
export const MAP_RELOAD_ACTIVATION_EVENTS = 'MAP/RELOAD_ACTIVATION_EVENTS' as const;
export const MAP_SET_DIRECT_ACCIDENT_ID = 'MAP/SET_DIRECT_ACCIDENT_ID' as const;
export const MAP_RELOAD_ACTIVATION_ACCIDENTS = 'MAP/RELOAD_ACTIVATION_ACCIDENTS' as const;
export const MAP_SET_SUMMARY = 'MAP/MAP_SET_SUMMARY' as const;

export type Action =
  | {
      type: typeof MAP_ADDITIONAL_LOCATION_SET;
      feature: Feature;
      data: Location;
    }
  | {
      type: typeof MAP_ADDITIONAL_LOCATION_CHANGE;
      key: string;
      value: number;
    }
  | {
      type: typeof MAP_ADDITIONAL_LOCATION_RESET;
    }
  | {
      type: typeof MAP_OPEN_LAYERS_INSTANCE_SET;
      openLayers: OpenLayers;
    }
  | {
      type: typeof MAP_SET_PROJECT;
      project: ControlProject;
    }
  | {
      type: typeof MAP_SET_SPACE;
      space: ControlSpace;
    }
  | {
      type: typeof MAP_SET_AREA_LIST;
      areaList: Area[];
    }
  | {
      type: typeof MAP_SET_SELECT_AREA;
      selectAreaFeature: Feature | null;
    }
  | {
      type: typeof MAP_SET_REALTIME_USER;
      userList: RealtimeUser[];
    }
  | {
      type: typeof MAP_SET_CONTROL_USER;
      controlUserList: RealtimeUser[];
    }
  | {
      type: typeof MAP_CANCEL_CONTROL_USER;
      accessKey: string;
    }
  | {
      type: typeof MAP_RESET_CONTROL_USER;
    }
  | {
      type: typeof MAP_SET_HISTORY_USER;
      userList: HistoryUser[];
    }
  | {
      type: typeof MAP_CANCEL_HISTORY_USER;
      accessKey: string;
    }
  | {
      type: typeof MAP_SET_DRAW_INTERACTION;
      interaction: Interaction | null;
    }
  | {
      type: typeof MAP_SET_DIRECT_EVENT_ID;
      id: string;
    }
  | {
      type: typeof MAP_RELOAD_ACTIVATION_EVENTS;
    }
  | {
      type: typeof MAP_SET_DIRECT_ACCIDENT_ID;
      id: string;
    }
  | {
      type: typeof MAP_RELOAD_ACTIVATION_ACCIDENTS;
    }
  | {
      type: typeof MAP_SET_SUMMARY;
      summary: ControlSummary;
    };
