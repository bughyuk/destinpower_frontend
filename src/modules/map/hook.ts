import { useContext, useRef, useState } from 'react';
import { MapDispatchContext, MapStateContext } from '@/modules/map/context';
import {
  Area,
  ControlProject,
  ControlSpace,
  ControlSummary,
  HistoryUser,
  Location,
  MAP_ADDITIONAL_LOCATION_CHANGE,
  MAP_ADDITIONAL_LOCATION_RESET,
  MAP_ADDITIONAL_LOCATION_SET,
  MAP_CANCEL_CONTROL_USER,
  MAP_CANCEL_HISTORY_USER,
  MAP_OPEN_LAYERS_INSTANCE_SET,
  MAP_RELOAD_ACTIVATION_ACCIDENTS,
  MAP_RELOAD_ACTIVATION_EVENTS,
  MAP_RESET_CONTROL_USER,
  MAP_SET_AREA_LIST,
  MAP_SET_CONTROL_USER,
  MAP_SET_DIRECT_ACCIDENT_ID,
  MAP_SET_DIRECT_EVENT_ID,
  MAP_SET_DRAW_INTERACTION,
  MAP_SET_HISTORY_USER,
  MAP_SET_PROJECT,
  MAP_SET_REALTIME_USER,
  MAP_SET_SELECT_AREA,
  MAP_SET_SPACE,
  MAP_SET_SUMMARY,
  OpenLayers,
  RealtimeUser,
} from '@/modules/map/types';
import { Interaction } from 'ol/interaction';
import { Circle, Point, Polygon } from 'ol/geom';
import { WKT } from 'ol/format';
import { useActiveMenu } from '@/modules/setup/hook';
import { MENU_IDX_ACCIDENT, MENU_IDX_EVENT } from '@/utils/constants/common';
import { Icon, Style } from 'ol/style';
import { Feature } from 'ol';
import { CommonUtils } from '@/utils';
import * as turf from '@turf/turf';
import { AccidentCategory } from '@/modules/accident/types';
import { EventsKey } from 'ol/events';
import { unByKey } from 'ol/Observable';
import VectorSource from 'ol/source/Vector';

function useMapState() {
  const state = useContext(MapStateContext);
  if (!state) throw new Error('MapProvider not found');
  return state;
}

function useMapDispatch() {
  const dispatch = useContext(MapDispatchContext);
  if (!dispatch) throw new Error('MapProvider not found');
  return dispatch;
}

export function useAdditionalLocation() {
  const state = useMapState();
  const dispatch = useMapDispatch();

  const handleSetLocation = (feature: any, data: Location) => {
    dispatch({
      type: MAP_ADDITIONAL_LOCATION_SET,
      feature,
      data,
    });
  };

  const location = state.additional.location;
  const locationFeature = location.feature;
  const locationData = location.data;

  const handleLocationChange = (key: string, value: string) => {
    if (key === 'radius') {
      if (locationFeature) {
        const circleGeometry = locationFeature.getGeometry() as Circle;
        circleGeometry.setRadius(Number(value));
      }
    }

    dispatch({
      type: MAP_ADDITIONAL_LOCATION_CHANGE,
      key,
      value: Number(value),
    });
  };

  const handleResetLocation = () => {
    dispatch({
      type: MAP_ADDITIONAL_LOCATION_RESET,
    });
  };

  return {
    locationFeature,
    locationData,
    handleSetLocation,
    handleLocationChange,
    handleResetLocation,
  };
}

export function useOpenLayers() {
  const state = useMapState();
  const dispatch = useMapDispatch();
  const { handleSetLocation } = useAdditionalLocation();

  const openLayers = state.openLayers;
  const map = openLayers.map;
  const floorsLayer = openLayers.floorsLayer;
  const realtimeLayer = openLayers.realtimeLayer;
  const realtimeSource = realtimeLayer?.getSource() as VectorSource;
  const areaSource = openLayers.areaSource;
  const eventLayer = openLayers.eventLayer;
  const accidentEmergencyLayer = openLayers.accidentEmergencyLayer;
  const accidentFireLayer = openLayers.accidentFireLayer;
  const accidentRescueLayer = openLayers.accidentRescueLayer;
  const accidentInfectiousDiseaseLayer =
    openLayers.accidentInfectiousDiseaseLayer;
  const heatMapLayer = openLayers.heatMapLayer;
  const distributionLayer = openLayers.distributionLayer;
  const apLayer = openLayers.apLayer;
  const historySimulationLayer = openLayers.historySimulationLayer;
  const historyTraceDistributionLayer =
    openLayers.historyTraceDistributionLayer;
  const poiLayer = openLayers.poiLayer;
  const zoneLayer = openLayers.zoneLayer;
  const realtimeHeatMapLayer = openLayers.realtimeHeatMapLayer;
  const draw = openLayers.draw;
  const floorPlan = openLayers.floorPlan;

  const handleSetOpenLayers = (openLayers: OpenLayers) => {
    dispatch({
      type: MAP_OPEN_LAYERS_INSTANCE_SET,
      openLayers,
    });

    const drawCircle = openLayers.draw.circle;
    if (drawCircle) {
      drawCircle.on('drawend', (e) => {
        const circle = e.feature.getGeometry() as Circle;
        const center = circle.getCenter();
        openLayers.map?.removeInteraction(drawCircle);
        dispatch({
          type: MAP_SET_DRAW_INTERACTION,
          interaction: null,
        });
        dispatch({
          type: MAP_ADDITIONAL_LOCATION_SET,
          feature: e.feature,
          data: {
            lng: center[0],
            lat: center[1],
            radius: circle.getRadius(),
          },
        });
      });
    }

    const drawAccident = openLayers.draw.accident;
    if (drawAccident) {
      drawAccident.on('drawend', (e) => {
        const circle = e.feature.getGeometry() as Circle;
        const center = circle.getCenter();
        openLayers.map?.removeInteraction(drawAccident);
        dispatch({
          type: MAP_SET_DRAW_INTERACTION,
          interaction: null,
        });
        dispatch({
          type: MAP_ADDITIONAL_LOCATION_SET,
          feature: e.feature,
          data: {
            lng: center[0],
            lat: center[1],
            radius: circle.getRadius(),
          },
        });
      });
    }

    const drawPolygon = openLayers.draw.polygon;
    const wkt = new WKT();
    if (drawPolygon) {
      drawPolygon.on('drawend', (e) => {
        const polygon = e.feature.getGeometry() as Polygon;

        openLayers.map?.removeInteraction(drawPolygon);
        dispatch({
          type: MAP_SET_DRAW_INTERACTION,
          interaction: null,
        });
        dispatch({
          type: MAP_ADDITIONAL_LOCATION_SET,
          feature: e.feature,
          data: {
            geom: wkt.writeGeometry(polygon),
          },
        });
      });
    }
  };

  const setDrawInteraction = (interaction: Interaction) => {
    if (draw.interaction) {
      map?.removeInteraction(draw.interaction);
    }
    map?.addInteraction(interaction);
    dispatch({
      type: MAP_SET_DRAW_INTERACTION,
      interaction,
    });
  };

  const [
    selectAreaEventKey,
    setSelectAreaEventKey,
  ] = useState<EventsKey | null>(null);
  const handleUnByKeySelectArea = () => {
    if (selectAreaEventKey) {
      unByKey(selectAreaEventKey);
    }
  };
  const handleSelectArea = () => {
    if (draw.interaction) {
      map?.removeInteraction(draw.interaction);
    }
    draw.source?.clear();
    handleUnByKeySelectArea();
    setSelectAreaEventKey(
      map?.once('click', (e) => {
        const coordinate = e.coordinate;
        state.area.list.some((area) => {
          const feature = area.feature;
          const geometry = feature.getGeometry();
          const polygonGeometry = geometry as Polygon;
          const polygon = turf.polygon(polygonGeometry.getCoordinates());
          const clickPoint = turf.point(coordinate);
          if (turf.inside(clickPoint, polygon)) {
            draw.source?.addFeature(feature);
            handleSetLocation(feature, {
              areaId: area.area_id,
              areaName: area.area_name,
            });

            return true;
          }
        });

        setSelectAreaEventKey(null);
      }) as EventsKey
    );
  };

  const handleDrawCircle = () => {
    draw.source?.clear();
    handleUnByKeySelectArea();
    const drawCircle = draw.circle;
    if (drawCircle) {
      setDrawInteraction(drawCircle);
    }
  };

  const handleDrawPolygon = () => {
    draw.source?.clear();
    handleUnByKeySelectArea();
    const drawPolygon = draw.polygon;
    if (drawPolygon) {
      setDrawInteraction(drawPolygon);
    }
  };

  const drawAccidentEventsKeyRef = useRef<EventsKey>();

  const handleDrawAccident = (category: AccidentCategory) => {
    draw.source?.clear();
    const drawAccident = draw.accident;

    if (drawAccident) {
      if (drawAccidentEventsKeyRef.current) {
        unByKey(drawAccidentEventsKeyRef.current);
      }

      drawAccidentEventsKeyRef.current = drawAccident.once('drawstart', (e) => {
        const accidentIconName = CommonUtils.getAccidentIconName(category);
        const circleGeometry = e.feature.getGeometry() as Circle;
        const center = circleGeometry.getCenter();
        const icon = new Feature(new Point(center));
        icon.setId('drawAccidentIconFeature');
        const iconStyle = new Style({
          image: new Icon({
            src: `/static/images/${accidentIconName}`,
            scale: 2,
          }),
        });
        icon.setStyle(iconStyle);
        draw.source?.addFeature(icon);
      }) as EventsKey;
      setDrawInteraction(drawAccident);
    }
  };

  return {
    map,
    floorsLayer,
    realtimeLayer,
    realtimeSource,
    areaSource,
    eventLayer,
    accidentEmergencyLayer,
    accidentFireLayer,
    accidentRescueLayer,
    accidentInfectiousDiseaseLayer,
    heatMapLayer,
    distributionLayer,
    apLayer,
    historySimulationLayer,
    historyTraceDistributionLayer,
    poiLayer,
    zoneLayer,
    realtimeHeatMapLayer,
    draw,
    floorPlan,
    handleSetOpenLayers,
    handleDrawCircle,
    handleDrawPolygon,
    handleDrawAccident,
    handleSelectArea,
  };
}

export function useControlSpace() {
  const { space } = useMapState();
  const dispatch = useMapDispatch();

  const handleSetSpace = (space: ControlSpace) => {
    dispatch({
      type: MAP_SET_SPACE,
      space,
    });
  };

  return {
    space,
    handleSetSpace,
  };
}

export function useControlProject() {
  const { project, summary } = useMapState();
  const dispatch = useMapDispatch();

  const handleSetProject = (project: ControlProject) => {
    dispatch({
      type: MAP_SET_PROJECT,
      project,
    });
  };

  const handleSetProjectSummary = (summary: ControlSummary) => {
    dispatch({
      type: MAP_SET_SUMMARY,
      summary,
    });
  };

  return {
    project,
    summary,
    handleSetProject,
    handleSetProjectSummary,
  };
}

export function useControlArea() {
  const { area } = useMapState();
  const dispatch = useMapDispatch();

  const handleSetAreaList = (areaList: Area[]) => {
    dispatch({
      type: MAP_SET_AREA_LIST,
      areaList,
    });
  };

  const handleSetSelectArea = (selectAreaFeature: Feature | null) => {
    dispatch({
      type: MAP_SET_SELECT_AREA,
      selectAreaFeature,
    });
  };

  return {
    areaList: area.list,
    selectedAreaFeature: area.selectedFeature,
    handleSetAreaList,
    handleSetSelectArea,
  };
}

export function useHistoryUser() {
  const { users } = useMapState();
  const dispatch = useMapDispatch();

  const handleSetHistoryUser = (userList: HistoryUser[]) => {
    dispatch({
      type: MAP_SET_HISTORY_USER,
      userList,
    });
  };

  const handleCancelHistoryUser = (accessKey: string) => {
    dispatch({
      type: MAP_CANCEL_HISTORY_USER,
      accessKey,
    });
  };

  return {
    users: users.history,
    handleSetHistoryUser,
    handleCancelHistoryUser,
  };
}

export function useRealtimeUser() {
  const { users } = useMapState();
  const dispatch = useMapDispatch();

  const handleSetRealtimeUser = (userList: RealtimeUser[]) => {
    dispatch({
      type: MAP_SET_REALTIME_USER,
      userList,
    });
  };

  return {
    users: users.realtime,
    handleSetRealtimeUser,
  };
}

export function useControlUser() {
  const { users } = useMapState();
  const dispatch = useMapDispatch();

  const handleSetControlUser = (controlUserList: RealtimeUser[]) => {
    dispatch({
      type: MAP_SET_CONTROL_USER,
      controlUserList,
    });
  };

  const handleCancelControlUser = (accessKey: string) => {
    dispatch({
      type: MAP_CANCEL_CONTROL_USER,
      accessKey,
    });
  };

  const handleResetControlUser = () => {
    dispatch({
      type: MAP_RESET_CONTROL_USER,
    });
  };

  return {
    users: users.control,
    handleSetControlUser,
    handleCancelControlUser,
    handleResetControlUser,
  };
}

export function useControlEvent() {
  const { event } = useMapState();
  const dispatch = useMapDispatch();
  const { handleDirectMenuActive } = useActiveMenu();

  const eventId = event.id;
  const directFlag = event.directFlag;
  const reloadFlag = event.reloadFlag;

  const handleSetDirectEventId = (id: string) => {
    handleDirectMenuActive(MENU_IDX_EVENT);
    dispatch({
      type: MAP_SET_DIRECT_EVENT_ID,
      id,
    });
  };

  const handleReloadActivationEvents = () => {
    dispatch({
      type: MAP_RELOAD_ACTIVATION_EVENTS,
    });
  };

  return {
    directEventId: eventId,
    directFlag,
    reloadFlag,
    handleSetDirectEventId,
    handleReloadActivationEvents,
  };
}

export function useControlAccident() {
  const { accident } = useMapState();
  const dispatch = useMapDispatch();
  const { handleDirectMenuActive } = useActiveMenu();

  const eventId = accident.id;
  const directFlag = accident.directFlag;
  const reloadFlag = accident.reloadFlag;

  const handleSetDirectAccidentId = (id: string) => {
    handleDirectMenuActive(MENU_IDX_ACCIDENT);
    dispatch({
      type: MAP_SET_DIRECT_ACCIDENT_ID,
      id,
    });
  };

  const handleReloadActivationAccidents = () => {
    dispatch({
      type: MAP_RELOAD_ACTIVATION_ACCIDENTS,
    });
  };

  return {
    directAccidentId: eventId,
    directFlag,
    reloadFlag,
    handleSetDirectAccidentId,
    handleReloadActivationAccidents,
  };
}
