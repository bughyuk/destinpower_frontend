export type AssetsSummary = {
  asset_cnt: string;
  sensor_cnt: string;
};

export const ASSET_SIGNAL_STATUS_GOOD = 0;
export const ASSET_SIGNAL_STATUS_USUALLY = 1;
export const ASSET_SIGNAL_STATUS_ERROR = 2;
export const ASSET_SIGNAL_STATUS_NO_SIGNAL = 3;
export const ASSET_SIGNAL_STATUS_STOP = 4;

export type AssetsSignalStatus =
  | typeof ASSET_SIGNAL_STATUS_GOOD
  | typeof ASSET_SIGNAL_STATUS_USUALLY
  | typeof ASSET_SIGNAL_STATUS_ERROR
  | typeof ASSET_SIGNAL_STATUS_NO_SIGNAL
  | typeof ASSET_SIGNAL_STATUS_STOP;

export type Assets = {
  asset_id: string;
  asset_category: AssetsCategory;
  asset_name: string;
  lat: number;
  lng: number;
  asset_status: AssetsSignalStatus;
  asset_tagging: string;
  util_name: string;
  util_serial_no: string;
  asset_sigdate: string;
  asset_position: string;
  asset_type: string;
  asset_uuid: string;
  asset_categorynm: string;
  asset_nowtemp: number;
  maxval: string;
  active_flag: boolean;
};

export const TEMPERATURE_AND_HUMIDITY_SIGNAL_GOOD = 0;
export const TEMPERATURE_AND_HUMIDITY_SIGNAL_ORDINARY = 1;
export const TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER = 2;
export const TEMPERATURE_AND_HUMIDITY_SIGNAL_NO_SIGNAL = 3;

export type TemperatureAndHumiditySignal =
  | typeof TEMPERATURE_AND_HUMIDITY_SIGNAL_GOOD
  | typeof TEMPERATURE_AND_HUMIDITY_SIGNAL_ORDINARY
  | typeof TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER
  | typeof TEMPERATURE_AND_HUMIDITY_SIGNAL_NO_SIGNAL;

export type TemperatureAndHumidity = {
  hourinfo: string;
  celsius: string;
  humidity: string;
  signal: TemperatureAndHumiditySignal;
};

export type TemperatureAndHumidityRealtime = {
  xpos: string;
  ypos: string;
  celsius: string;
  cellcius_max1: string;
  cellcius_min1: string;
  celsius_ok: 0 | 1;
  humidity: string;
  humidity_max2: string;
  humidity_min2: string;
  humidity_ok: 0 | 1;
};

export type TemperatureAndHumidityStatus = {
  asset_name: string;
  signal: TemperatureAndHumiditySignal;
};

export const ASSETS_DIV_TOTAL = 2;
export const ASSETS_DIV_SENSOR = 1;

export type AssetsDiv = typeof ASSETS_DIV_SENSOR | typeof ASSETS_DIV_TOTAL;

export const ASSETS_CATEGORY_TEMPERATURE = 2;
export const ASSETS_CATEGORY_HUMIDITY = 3;
export const ASSETS_CATEGORY_TEMPERATURE_AND_HUMIDITY = 4;

export type AssetsCategory =
  | typeof ASSETS_CATEGORY_TEMPERATURE
  | typeof ASSETS_CATEGORY_HUMIDITY
  | typeof ASSETS_CATEGORY_TEMPERATURE_AND_HUMIDITY;
