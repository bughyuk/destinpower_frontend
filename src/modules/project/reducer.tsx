import {
  Action,
  PROJECT_CHANGE_DEPTH_SIDEBAR_SHOW,
  PROJECT_CHANGE_PANE_STATUS,
  PROJECT_CHANGE_REGISTER_STEP,
  PROJECT_OPEN_LAYERS_INSTANCE_SET,
  PROJECT_RELOAD_SPACE_LOCATION_OVERLAY,
  PROJECT_ROUTE_EDIT,
  PROJECT_SET_EDIT_INITIAL_STATE,
  PROJECT_SET_REGISTER_CONNECTED_META_IDS,
  PROJECT_SET_REGISTER_INITIAL_STATE,
  PROJECT_SET_REGISTER_PRODUCE_STEP,
  PROJECT_SET_REGISTER_PROJECT_INFO,
  ProjectState,
} from '@/modules/project/types';
import { PANE_STATUS_EDIT } from '@/utils/constants/common';

export function projectReducer(state: ProjectState, action: Action) {
  switch (action.type) {
    case PROJECT_CHANGE_PANE_STATUS:
      return {
        ...state,
        paneStatus: action.paneStatus,
      };
    case PROJECT_CHANGE_REGISTER_STEP:
      return {
        ...state,
        register: {
          ...state.register,
          step: action.registerStep,
        },
      };
    case PROJECT_CHANGE_DEPTH_SIDEBAR_SHOW:
      return {
        ...state,
        depthSidebarShow: action.show,
      };
    case PROJECT_SET_REGISTER_PROJECT_INFO:
      return {
        ...state,
        register: {
          ...state.register,
          info: {
            ...action.projectInfo,
          },
        },
      };
    case PROJECT_SET_REGISTER_INITIAL_STATE:
      return {
        ...state,
        register: {
          step: 1,
          info: {
            projectName: '',
            note: '',
          },
          connectedMetaIds: [],
          projectProduceStep: null,
        },
      };
    case PROJECT_SET_REGISTER_CONNECTED_META_IDS:
      return {
        ...state,
        register: {
          ...state.register,
          connectedMetaIds: action.connectedMetaIds,
        },
      };
    case PROJECT_SET_REGISTER_PRODUCE_STEP:
      return {
        ...state,
        register: {
          ...state.register,
          projectProduceStep: action.projectProduceStep,
        },
      };
    case PROJECT_ROUTE_EDIT:
      return {
        ...state,
        paneStatus: PANE_STATUS_EDIT,
        edit: {
          projectId: action.projectId,
        },
      };
    case PROJECT_SET_EDIT_INITIAL_STATE:
      return {
        ...state,
        edit: {
          projectId: '',
        },
      };
    case PROJECT_RELOAD_SPACE_LOCATION_OVERLAY:
      return {
        ...state,
        locationOverlayReload: !state.locationOverlayReload,
      };
    case PROJECT_OPEN_LAYERS_INSTANCE_SET:
      return {
        ...state,
        openLayers: action.openLayers,
      };
  }
}
