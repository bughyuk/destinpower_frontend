import { useContext } from 'react';

import { PaneStatus } from '@/utils/constants/common';
import {
  ProjectDispatchContext,
  ProjectStateContext,
} from '@/modules/project/context';
import {
  OpenLayers,
  PROJECT_CHANGE_DEPTH_SIDEBAR_SHOW,
  PROJECT_CHANGE_PANE_STATUS,
  PROJECT_CHANGE_REGISTER_STEP,
  PROJECT_OPEN_LAYERS_INSTANCE_SET,
  PROJECT_RELOAD_SPACE_LOCATION_OVERLAY,
  PROJECT_ROUTE_EDIT,
  PROJECT_SET_EDIT_INITIAL_STATE,
  PROJECT_SET_REGISTER_CONNECTED_META_IDS,
  PROJECT_SET_REGISTER_INITIAL_STATE,
  PROJECT_SET_REGISTER_PRODUCE_STEP,
  PROJECT_SET_REGISTER_PROJECT_INFO,
  ProjectInfo,
  ProjectProduceStep,
} from '@/modules/project/types';

function useState() {
  const state = useContext(ProjectStateContext);
  if (!state) throw new Error('ProjectProvider not found');
  return state;
}

function useDispatch() {
  const dispatch = useContext(ProjectDispatchContext);
  if (!dispatch) throw new Error('ProjectProvider not found');
  return dispatch;
}

export function useProjectPane() {
  const state = useState();
  const dispatch = useDispatch();

  const { paneStatus, depthSidebarShow } = state;

  const handleChangePaneStatus = (paneStatus: PaneStatus) => {
    dispatch({
      type: PROJECT_CHANGE_PANE_STATUS,
      paneStatus,
    });
  };

  const handleChangeDepthSidebarShow = (show: boolean) => {
    dispatch({
      type: PROJECT_CHANGE_DEPTH_SIDEBAR_SHOW,
      show,
    });
  };

  return {
    paneStatus,
    depthSidebarShow,
    handleChangePaneStatus,
    handleChangeDepthSidebarShow,
  };
}

export function useOpenLayers() {
  const state = useState();
  const dispatch = useDispatch();

  const { map, drawSource, geofencingLayer, draw, geoImage } = state.openLayers;
  const locationOverlayReload = state.locationOverlayReload;

  const handleReloadSpaceLocationOverlay = () => {
    dispatch({
      type: PROJECT_RELOAD_SPACE_LOCATION_OVERLAY,
    });
  };

  const handleSetOpenLayers = (openLayers: OpenLayers) => {
    dispatch({
      type: PROJECT_OPEN_LAYERS_INSTANCE_SET,
      openLayers,
    });
  };

  const handleClearDrawSource = () => {
    drawSource?.clear();
  };

  const handleAllClear = () => {
    geoImage.remove();
  };

  return {
    map,
    drawSource,
    draw,
    geoImage,
    geofencingLayer,
    locationOverlayReload,
    handleReloadSpaceLocationOverlay,
    handleSetOpenLayers,
    handleClearDrawSource,
    handleAllClear,
  };
}

export function useProjectRegister() {
  const state = useState();
  const dispatch = useDispatch();

  const handleChangeRegisterStep = (registerStep: number) => {
    dispatch({
      type: PROJECT_CHANGE_REGISTER_STEP,
      registerStep,
    });
  };

  const handleSetProjectInfo = (projectInfo: ProjectInfo) => {
    dispatch({
      type: PROJECT_SET_REGISTER_PROJECT_INFO,
      projectInfo,
    });
  };

  const handleSetConnectedMetaIds = (connectedMetaIds: string[]) => {
    dispatch({
      type: PROJECT_SET_REGISTER_CONNECTED_META_IDS,
      connectedMetaIds,
    });
  };

  const handleSetProjectProduceStep = (
    projectProduceStep: ProjectProduceStep
  ) => {
    dispatch({
      type: PROJECT_SET_REGISTER_PRODUCE_STEP,
      projectProduceStep,
    });
  };

  const handleSetRegisterInitialState = () => {
    dispatch({
      type: PROJECT_SET_REGISTER_INITIAL_STATE,
    });
  };

  const {
    step: registerStep,
    info: projectInfo,
    connectedMetaIds,
    projectProduceStep,
  } = state.register;

  return {
    registerStep,
    projectInfo,
    connectedMetaIds,
    projectProduceStep,
    handleChangeRegisterStep,
    handleSetProjectInfo,
    handleSetRegisterInitialState,
    handleSetConnectedMetaIds,
    handleSetProjectProduceStep,
  };
}

export function useProjectEdit() {
  const state = useState();
  const dispatch = useDispatch();

  const projectId = state.edit.projectId;

  const handleRouteEdit = (projectId: string) => {
    dispatch({
      type: PROJECT_ROUTE_EDIT,
      projectId,
    });
  };

  const handleSetEditInitialState = () => {
    dispatch({
      type: PROJECT_SET_EDIT_INITIAL_STATE,
    });
  };

  return {
    projectId,
    handleRouteEdit,
    handleSetEditInitialState,
  };
}
