import { PaneStatus } from '@/utils/constants/common';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { Feature, Map } from 'ol';
import { Image } from 'ol/layer';
import { Vector as VectorSource } from 'ol/source';
import { Interaction } from 'ol/interaction';
import { DrawGeoImage } from '@/modules/space/types';

export type OpenLayers = {
  map: Map | null;
  drawSource: VectorSource | null;
  geofencingLayer: Image | null;
  geoImage: {
    layer: any | null;
    draw: (options: DrawGeoImage) => void;
    drawFromSquare: (options: DrawGeoImage, squareFeature: Feature) => void;
    remove: () => void;
  };
  draw: {
    square: Interaction | null;
  };
};

export type ProjectSpaceLocation = {
  projectId: string;
  projectName: string;
  lng: number;
  lat: number;
};

export type ProjectStepData = {
  produceStepData: string | null;
  solutionType?: SolutionType;
};

export type ProjectProduceStep = {
  step?: number;
  metaId?: string;
};

export type ProjectInfo = {
  projectId?: string;
  projectName?: string;
  note?: string;
  solutionType?: SolutionType;
  metaIds?: string[];
  produceStepData?: string;
};

export type ProjectSpaceFloors = {
  mapId: string;
  mapName: string;
  mapFloor: number;
  cx?: number;
  cy?: number;
  filename?: string;
  rotation?: number;
  scalex?: number;
  scaley?: number;
};

export type ProjectSpace = {
  mappingId: string;
  metaId: string;
  metaName: string;
  lng: number;
  lat: number;
  floors: ProjectSpaceFloors[];
  registDate: string;
};

export type ProjectDetail = {
  projectId: string;
  projectName: string;
  note: string;
  solutionType: SolutionType;
  buildings: ProjectSpace[];
  produceStep: ProjectProduceStep;
};

export type Project = {
  projectId: string;
  projectName: string;
  registDate: string;
  updateDate: string;
  solutionType: SolutionType;
  produceEndFlag: boolean;
};

export type Projects = Project[];

export type ProjectState = {
  paneStatus: PaneStatus;
  depthSidebarShow: boolean;
  locationOverlayReload: boolean;
  register: {
    step: number;
    info: ProjectInfo;
    connectedMetaIds: string[];
    projectProduceStep: ProjectProduceStep | null;
  };
  edit: {
    projectId: string;
  };
  openLayers: OpenLayers;
};

export const SOLUTION_TYPE_SMART_CONTROL = 'A';
export const SOLUTION_TYPE_SMART_OFFICE = 'B';
export const SOLUTION_TYPE_SMART_HOSPITAL = 'C';
export const SOLUTION_TYPE_SMART_FACTORY = 'D';
export const SOLUTION_TYPE_SMART_RETAIL = 'E';

export type Solution = {
  type: SolutionType;
  icon: IconProp;
  name: string;
  description: string;
};

export type Solutions = Solution[];

export type SolutionType =
  | undefined
  | typeof SOLUTION_TYPE_SMART_CONTROL
  | typeof SOLUTION_TYPE_SMART_OFFICE
  | typeof SOLUTION_TYPE_SMART_HOSPITAL
  | typeof SOLUTION_TYPE_SMART_FACTORY
  | typeof SOLUTION_TYPE_SMART_RETAIL;

export const PROJECT_CHANGE_PANE_STATUS = 'PROJECT/CHANGE_PANE_STATUS' as const;
export const PROJECT_CHANGE_REGISTER_STEP = 'PROJECT/CHANGE_REGISTER_STEP' as const;
export const PROJECT_CHANGE_DEPTH_SIDEBAR_SHOW = 'PROJECT/CHANGE_DEPTH_SIDEBAR_SHOW' as const;
export const PROJECT_SET_REGISTER_PROJECT_INFO = 'PROJECT/SET_REGISTER_PROJECT_INFO' as const;
export const PROJECT_SET_REGISTER_INITIAL_STATE = 'PROJECT/SET_REGISTER_INITIAL_STATE' as const;
export const PROJECT_SET_REGISTER_CONNECTED_META_IDS = 'PROJECT/SET_REGISTER_CONNECTED_META_IDS' as const;
export const PROJECT_SET_REGISTER_PRODUCE_STEP = 'PROJECT/SET_REGISTER_PRODUCE_STEP' as const;
export const PROJECT_ROUTE_EDIT = 'PROJECT/ROUTE_EDIT' as const;
export const PROJECT_SET_EDIT_INITIAL_STATE = 'PROJECT/SET_EDIT_INITIAL_STATE' as const;
export const PROJECT_RELOAD_SPACE_LOCATION_OVERLAY = 'PROJECT/RELOAD_SPACE_LOCATION_OVERLAY' as const;
export const PROJECT_OPEN_LAYERS_INSTANCE_SET = 'PROJECT/OPEN_LAYERS_INSTANCE_SET' as const;

export type Action =
  | {
      type: typeof PROJECT_CHANGE_PANE_STATUS;
      paneStatus: PaneStatus;
    }
  | {
      type: typeof PROJECT_CHANGE_REGISTER_STEP;
      registerStep: number;
    }
  | {
      type: typeof PROJECT_CHANGE_DEPTH_SIDEBAR_SHOW;
      show: boolean;
    }
  | {
      type: typeof PROJECT_SET_REGISTER_PROJECT_INFO;
      projectInfo: ProjectInfo;
    }
  | {
      type: typeof PROJECT_SET_REGISTER_INITIAL_STATE;
    }
  | {
      type: typeof PROJECT_SET_REGISTER_CONNECTED_META_IDS;
      connectedMetaIds: string[];
    }
  | {
      type: typeof PROJECT_SET_REGISTER_PRODUCE_STEP;
      projectProduceStep: ProjectProduceStep;
    }
  | {
      type: typeof PROJECT_ROUTE_EDIT;
      projectId: string;
    }
  | {
      type: typeof PROJECT_SET_EDIT_INITIAL_STATE;
    }
  | {
      type: typeof PROJECT_RELOAD_SPACE_LOCATION_OVERLAY;
    }
  | {
      type: typeof PROJECT_OPEN_LAYERS_INSTANCE_SET;
      openLayers: OpenLayers;
    };
