import { ReactNode } from 'react';
import { PaneStatus } from '@/utils/constants/common';

export type AsyncState<T = any> = {
  loading: boolean;
  error: Error | null;
  data: T | null;
};

type LoadingAction = {
  type: 'LOADING';
};

type SuccessAction<T> = {
  type: 'SUCCESS';
  data: T;
};

type ErrorAction = {
  type: 'ERROR';
  error: Error;
};

export type AsyncAction<T> = LoadingAction | SuccessAction<T> | ErrorAction;

export type ApiResult = {
  status: number;
  message: string;
  data: any | null;
};

export type ListResult<T> = {
  content: T;
  totalElements: number;
};

export type PromiseFn<T> = (...args: any) => Promise<T>;

export type ModalProps = {
  show: boolean;
  onHide: () => void;
  onReload?: () => void;
};

export type ModalContainerProps = ModalProps & {
  children: ReactNode;
  onConfirm: () => void;
};

export type ModalAlertProps = {
  show: boolean;
  children: ReactNode;
  onHide: () => void;
};

export type ReactSelectOption = {
  value: string;
  label: string | ReactNode;
  name: string;
};

export type PaneProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

// 백엔드 결과값
export type dp_ApiResult = {
  res: string;
  message: string;
  id: number;
};
