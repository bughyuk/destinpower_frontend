import {
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
  AccidentCategory,
} from '@/modules/accident/types';
import { useTranslation } from 'react-i18next';

export function useAccidentMessage() {
  const { t } = useTranslation();

  const handleGetAccidentMessage = (accidentCategory: AccidentCategory) => {
    let title = '';
    let content = '';

    switch (accidentCategory) {
      case ACCIDENT_EMERGENCY:
        title = t('msg_emergency_accident_message_title');
        content = t('msg_emergency_accident_message_content');
        break;
      case ACCIDENT_FIRE:
        title = t('msg_fire_accident_message_title');
        content = t('msg_fire_accident_message_content');
        break;
      case ACCIDENT_RESCUE:
        title = t('msg_rescue_accident_message_title');
        content = t('msg_rescue_accident_message_content');
        break;
      case ACCIDENT_INFECTIOUS_DISEASE:
        title = t('msg_infectious_disease_accident_message_title');
        content = t('msg_infectious_disease_accident_message_content');
        break;
    }

    return {
      title,
      content,
    };
  };

  return {
    handleGetAccidentMessage,
  };
}
