export type Accident = {
  eventId: string;
  eventDetailCategory: AccidentCategory;
  eventTitle: string;
  registDate: string;
  updateDate: string;
  activeFlag: boolean;
  geom?: string;
  geomType?: string;
  lng?: number;
  lat?: number;
  radius?: number;
  areaName: string;
  areaId: string;
  areaGeom: string;
};

export type Message = {
  title: string;
  content: string;
  linkUrl: string;
  imgId: string;
};

export type MessageHistory = {
  messageId: string;
  messageTitle: string;
  messageContent: string;
  registDate: string;
};

export const ACCIDENT_EMERGENCY = 'EMERGENCY';
export const ACCIDENT_FIRE = 'FIRE';
export const ACCIDENT_RESCUE = 'SOS';
export const ACCIDENT_INFECTIOUS_DISEASE = 'DISEASES';

export type AccidentCategory =
  | typeof ACCIDENT_EMERGENCY
  | typeof ACCIDENT_FIRE
  | typeof ACCIDENT_RESCUE
  | typeof ACCIDENT_INFECTIOUS_DISEASE;
