import React, { createContext, Dispatch, ReactNode, useReducer } from 'react';
import {
  Action,
  PhysicalDistributionState,
} from '@/modules/physical_distribution/types';
import { physicalDistributionReducer } from '@/modules/physical_distribution/reducer';
import { PANE_STATUS_LIST } from '@/utils/constants/common';

export const PhysicalDistributionStateContext = createContext<PhysicalDistributionState | null>(
  null
);
type PhysicalDistributionDispatch = Dispatch<Action>;
export const PhysicalDistributionDispatchContext = createContext<PhysicalDistributionDispatch | null>(
  null
);

export const initialDistributionStatusDataState = {
  todayBoxCount: {
    releasingCnt: 0,
    releasingCompleteCnt: 0,
    warehousingCnt: 0,
    warehousingCompleteCnt: 0,
  },
  todayCount: {
    releasingCnt: 0,
    releasingCompleteCnt: 0,
    totalCnt: 0,
    warehousingCnt: 0,
    warehousingCompleteCnt: 0,
  },
  warehouseStockTotal: 0,
  reloadFlag: false,
};

const initialState: PhysicalDistributionState = {
  categoryIdx: undefined,
  productData: {
    selectedProduct: null,
    paneStatus: PANE_STATUS_LIST,
    reloadFlag: false,
    directSelectedProduct: null,
  },
  warehouseData: {
    selectedWarehouse: null,
  },
  historyData: {
    selectedDistributionHistory: null,
    changedDistributionHistory: null,
    reloadFlag: false,
  },
  distributionStatusData: initialDistributionStatusDataState,
  statisticsData: {
    selectedWarehouseId: '',
  },
};

export function PhysicalDistributionContextProvider({
  children,
}: {
  children: ReactNode;
}) {
  const [state, dispatch] = useReducer(
    physicalDistributionReducer,
    initialState
  );

  return (
    <PhysicalDistributionDispatchContext.Provider value={dispatch}>
      <PhysicalDistributionStateContext.Provider value={state}>
        {children}
      </PhysicalDistributionStateContext.Provider>
    </PhysicalDistributionDispatchContext.Provider>
  );
}
