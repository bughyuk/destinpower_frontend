import {
  Action,
  PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG,
  PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG,
  PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_PANE_STATUS,
  PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_RELOAD_FLAG,
  PHYSICAL_DISTRIBUTION_SET_CATEGORY_IDX,
  PHYSICAL_DISTRIBUTION_SET_DIRECT_PRODUCT,
  PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_HISTORY,
  PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_STATUS,
  PHYSICAL_DISTRIBUTION_SET_PRODUCT,
  PHYSICAL_DISTRIBUTION_SET_STATISTICS_WAREHOUSE_ID,
  PHYSICAL_DISTRIBUTION_SET_WAREHOUSE,
  PhysicalDistributionState,
} from '@/modules/physical_distribution/types';

export function physicalDistributionReducer(
  state: PhysicalDistributionState,
  action: Action
) {
  switch (action.type) {
    case PHYSICAL_DISTRIBUTION_SET_CATEGORY_IDX:
      return {
        ...state,
        categoryIdx: action.categoryIdx,
      };
    case PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_PANE_STATUS:
      return {
        ...state,
        productData: {
          ...state.productData,
          paneStatus: action.paneStatus,
        },
      };
    case PHYSICAL_DISTRIBUTION_SET_PRODUCT:
      return {
        ...state,
        productData: {
          ...state.productData,
          selectedProduct: action.product,
        },
      };
    case PHYSICAL_DISTRIBUTION_SET_DIRECT_PRODUCT:
      return {
        ...state,
        productData: {
          ...state.productData,
          directSelectedProduct: action.product,
        },
      };
    case PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_RELOAD_FLAG:
      return {
        ...state,
        productData: {
          ...state.productData,
          reloadFlag: !state.productData.reloadFlag,
        },
      };
    case PHYSICAL_DISTRIBUTION_SET_WAREHOUSE:
      return {
        ...state,
        warehouseData: {
          ...state.warehouseData,
          selectedWarehouse: action.warehouse,
        },
      };
    case PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_STATUS:
      return {
        ...state,
        distributionStatusData: {
          ...state.distributionStatusData,
          ...action.distributionStatus,
        },
      };
    case PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG:
      return {
        ...state,
        distributionStatusData: {
          ...state.distributionStatusData,
          reloadFlag: !state.distributionStatusData.reloadFlag,
        },
      };
    case PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_HISTORY:
      return {
        ...state,
        historyData: {
          ...state.historyData,
          selectedDistributionHistory: action.distributionHistory,
        },
      };
    case PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG:
      return {
        ...state,
        historyData: {
          ...state.historyData,
          reloadFlag: !state.historyData.reloadFlag,
        },
      };
    case PHYSICAL_DISTRIBUTION_SET_STATISTICS_WAREHOUSE_ID:
      return {
        ...state,
        statisticsData: {
          ...state.statisticsData,
          selectedWarehouseId: action.warehouseId,
        },
      };
  }
}
