import { useContext, useEffect, useState } from 'react';
import {
  PhysicalDistributionDispatchContext,
  PhysicalDistributionStateContext,
} from '@/modules/physical_distribution/context';
import {
  Category,
  Client,
  DistributionHistory,
  DistributionStatus,
  Item,
  PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG,
  PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG,
  PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_PANE_STATUS,
  PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_RELOAD_FLAG,
  PHYSICAL_DISTRIBUTION_SET_CATEGORY_IDX,
  PHYSICAL_DISTRIBUTION_SET_DIRECT_PRODUCT,
  PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_HISTORY,
  PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_STATUS,
  PHYSICAL_DISTRIBUTION_SET_PRODUCT,
  PHYSICAL_DISTRIBUTION_SET_STATISTICS_WAREHOUSE_ID,
  PHYSICAL_DISTRIBUTION_SET_WAREHOUSE,
  Product,
  ProductCodeSize,
  ReleaseProduct,
  Warehouse,
} from '@/modules/physical_distribution/types';
import { PaneStatus } from '@/utils/constants/common';
import {
  fetchClients,
  fetchItems,
  fetchProductCodeSize,
  fetchWarehouses,
} from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';

function usePhysicalDistributionState() {
  const state = useContext(PhysicalDistributionStateContext);
  if (!state) throw new Error('PhysicalDistributionProvider not found');
  return state;
}

function usePhysicalDistributionDispatch() {
  const dispatch = useContext(PhysicalDistributionDispatchContext);
  if (!dispatch) throw new Error('PhysicalDistributionProvider not found');
  return dispatch;
}

export function usePhysicalDistributionCategory() {
  const { categoryIdx } = usePhysicalDistributionState();
  const dispatch = usePhysicalDistributionDispatch();

  const handleSetCategoryIdx = (categoryIdx: Category | undefined) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_CATEGORY_IDX,
      categoryIdx,
    });
  };

  return {
    categoryIdx,
    handleSetCategoryIdx,
  };
}

export function usePhysicalDistributionProduct() {
  const { productData } = usePhysicalDistributionState();
  const {
    paneStatus,
    selectedProduct,
    reloadFlag,
    directSelectedProduct,
  } = productData;
  const dispatch = usePhysicalDistributionDispatch();

  const handleChangePaneStatus = (status: PaneStatus) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_PANE_STATUS,
      paneStatus: status,
    });
  };

  const handleSetProduct = (product: Product | ReleaseProduct | null) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_PRODUCT,
      product,
    });
  };

  const handleSetDirectProduct = (product: Product | ReleaseProduct | null) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_DIRECT_PRODUCT,
      product,
    });
  };

  const handleChangeReloadFlag = () => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_RELOAD_FLAG,
    });
  };

  return {
    paneStatus,
    selectedProduct,
    directSelectedProduct,
    reloadFlag,
    handleChangePaneStatus,
    handleSetProduct,
    handleSetDirectProduct,
    handleChangeReloadFlag,
  };
}

export function usePhysicalDistributionProductCode() {
  const [load, setLoad] = useState(false);
  const [productCodeSizeList, setProductCodeSizeList] = useState<
    ProductCodeSize[]
  >([]);

  useEffect(() => {
    handleFetchProductCodeSize();
  }, []);

  const handleFetchProductCodeSize = async () => {
    setLoad(false);
    const data = await fetchProductCodeSize();
    setProductCodeSizeList(data);
    setLoad(true);
  };

  return {
    load,
    productCodeSizeList,
  };
}

export function usePhysicalDistributionWarehouse() {
  const { warehouseData } = usePhysicalDistributionState();
  const [load, setLoad] = useState(false);
  const [warehouseList, setWarehouseLis] = useState<Warehouse[]>([]);
  const { selectedWarehouse } = warehouseData;
  const { project } = useControlProject();
  const dispatch = usePhysicalDistributionDispatch();

  useEffect(() => {
    handleFetchWarehouseList();
  }, []);

  const handleFetchWarehouseList = async () => {
    setLoad(false);
    const data = await fetchWarehouses(project.id);
    setWarehouseLis(data.filter((datum) => !datum.deleteFlag));
    setLoad(true);
  };

  const handleSetWarehouse = (warehouse: Warehouse | null) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_WAREHOUSE,
      warehouse,
    });
  };

  return {
    load,
    warehouseList,
    selectedWarehouse,
    handleSetWarehouse,
  };
}

export function usePhysicalDistributionItem() {
  const [reload, setReload] = useState(false);
  const [load, setLoad] = useState(false);
  const [itemList, setItemList] = useState<Item[]>([]);
  const { project } = useControlProject();

  useEffect(() => {
    handleFetchItemList();
  }, [reload]);

  const handleFetchItemList = async () => {
    setLoad(false);
    const data = await fetchItems(project.id);
    setItemList(data.filter((datum) => !datum.deleteFlag));
    setLoad(true);
  };

  const handleReloadItemList = () => {
    setReload(!reload);
  };

  return {
    load,
    itemList,
    handleReloadItemList,
  };
}

export function usePhysicalDistributionClient() {
  const [load, setLoad] = useState(false);
  const [clientList, setClientList] = useState<Client[]>([]);
  const [releaseClientList, setReleaseClientList] = useState<Client[]>([]);
  const { project } = useControlProject();

  useEffect(() => {
    handleFetchClientList();
  }, []);

  const handleFetchClientList = async () => {
    setLoad(false);
    const data = await fetchClients(project.id);
    setClientList(data.filter((datum) => !datum.deleteFlag));
    setLoad(true);
  };

  useEffect(() => {
    setReleaseClientList(
      clientList.filter((client) => client.logisticsCategory === 'RELEASING')
    );
  }, [clientList]);

  return {
    load,
    clientList,
    releaseClientList,
  };
}

export function usePhysicalDistributionStatus() {
  const { distributionStatusData } = usePhysicalDistributionState();
  const {
    todayBoxCount,
    todayCount,
    warehouseStockTotal,
    reloadFlag,
  } = distributionStatusData;
  const dispatch = usePhysicalDistributionDispatch();

  const handleSetDistributionStatus = (
    distributionStatus: DistributionStatus
  ) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_STATUS,
      distributionStatus,
    });
  };

  const handleChangeReloadFlag = () => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG,
    });
  };

  return {
    todayBoxCount,
    todayCount,
    warehouseStockTotal,
    reloadFlag,
    handleSetDistributionStatus,
    handleChangeReloadFlag,
  };
}

export function usePhysicalDistributionHistory() {
  const { historyData } = usePhysicalDistributionState();
  const { selectedDistributionHistory, reloadFlag } = historyData;
  const dispatch = usePhysicalDistributionDispatch();

  const handleSetDistributionHistory = (
    distributionHistory: DistributionHistory | null
  ) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_HISTORY,
      distributionHistory,
    });
  };

  const handleChangeReloadFlag = () => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG,
    });
  };

  return {
    selectedDistributionHistory,
    reloadFlag,
    handleSetDistributionHistory,
    handleChangeReloadFlag,
  };
}

export function usePhysicalDistributionStatistics() {
  const { statisticsData } = usePhysicalDistributionState();
  const { selectedWarehouseId } = statisticsData;
  const dispatch = usePhysicalDistributionDispatch();

  const handleSetWarehouseId = (warehouseId: string) => {
    dispatch({
      type: PHYSICAL_DISTRIBUTION_SET_STATISTICS_WAREHOUSE_ID,
      warehouseId,
    });
  };

  return {
    selectedWarehouseId,
    handleSetWarehouseId,
  };
}
