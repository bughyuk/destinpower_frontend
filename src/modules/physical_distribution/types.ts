import { PaneStatus } from '@/utils/constants/common';

export const PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT = 'PRODUCT';
export const PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING = 'WAREHOUSING';
export const PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE = 'RELEASE';
export const PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT = 'ADJUSTMENT';
export const PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT = 'MOVEMENT';
export const PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY = 'HISTORY';
export const PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT =
  'WAREHOUSE_MANAGEMENT';
export const PHYSICAL_DISTRIBUTION_CATEGORY_ITEM_MANAGEMENT = 'ITEM_MANAGEMENT';
export const PHYSICAL_DISTRIBUTION_CATEGORY_CLIENT_MANAGEMENT =
  'CLIENT_MANAGEMENT';

export const PROCESS_CATEGORY_FACTORY_MANAGEMENT = 'FACTORY_MANAGEMENT';
export const PROCESS_CATEGORY_PRODUCT_MANAGEMENT = 'PRODUCT_MANAGEMENT';
export const PROCESS_CATEGORY_PRODUCT_LINE_MANAGEMENT =
  'PRODUCT_LINE_MANAGEMENT';
export const PROCESS_CATEGORY_CLIENT_MANAGEMENT = 'CLIENT_MANAGEMENT';
export const PROCESS_CATEGORY_PROCESS_CHECK = 'PROCESS_CHECK';
export const PROCESS_CATEGORY_COMPLETED_PROCESS = 'COMPLETED_PROCESS';
export const PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT = 'CHECK_LIST_MANAGEMENT';
export const PROCESS_CATEGORY_CHECK_LIST_FORMAT_MANAGEMENT =
  'CHECK_LIST_FORMAT_MANAGEMENT';

export type Category =
  | undefined
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_ITEM_MANAGEMENT
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_CLIENT_MANAGEMENT
  | typeof PROCESS_CATEGORY_FACTORY_MANAGEMENT
  | typeof PROCESS_CATEGORY_PRODUCT_MANAGEMENT
  | typeof PROCESS_CATEGORY_PRODUCT_LINE_MANAGEMENT
  | typeof PROCESS_CATEGORY_CLIENT_MANAGEMENT
  | typeof PROCESS_CATEGORY_PROCESS_CHECK
  | typeof PROCESS_CATEGORY_COMPLETED_PROCESS
  | typeof PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT
  | typeof PROCESS_CATEGORY_CHECK_LIST_FORMAT_MANAGEMENT;

export const PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING = 'WAREHOUSING';
export const PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING = 'RELEASING';

export type ClientKind =
  | typeof PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING
  | typeof PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING;

type DistributionStatusData = DistributionStatus & {
  reloadFlag: boolean;
};

type WarehouseData = {
  selectedWarehouse: Warehouse | null;
};

type ProductData = {
  selectedProduct: Product | ReleaseProduct | null;
  paneStatus: PaneStatus;
  reloadFlag: boolean;
  directSelectedProduct: Product | ReleaseProduct | null;
};

type HistoryData = {
  selectedDistributionHistory: DistributionHistory | null;
  changedDistributionHistory: DistributionHistory | null;
  reloadFlag: boolean;
};

type StatisticsData = {
  selectedWarehouseId: string;
};

export type PhysicalDistributionState = {
  categoryIdx: Category | undefined;
  productData: ProductData;
  warehouseData: WarehouseData;
  historyData: HistoryData;
  distributionStatusData: DistributionStatusData;
  statisticsData: StatisticsData;
};

export type ProductCodeSize = {
  code: number;
  text: string;
};

export type ProductStock = {
  warehouseId: string;
  warehouseName: string;
  totalQuantity: number;
};

export type Product = {
  productId: string;
  productCode: string;
  productName: string;
  boxQuantity: number;
  boxInsideQuantity: number;
  minQuantity: number;
  totalQuantity: number;
  productSize: string;
  productSizeName: string;
  productWeight: number;
  productCategoryId: string;
  productCategoryName: string;
  deleteFlag: boolean;
  imgId: string;
  imgUrl: string;
};

export type ProductDetail = {
  detail: Product;
  stock: ProductStock[];
};

export type WarehousingProduct = Product & {
  logisticsQuantity: number;
};

export type WarehousingProductBox = {
  productId: string;
};

export type WarehousingQRCodePrintingData = {
  image: string;
  totalLogisticsQuantity: number;
  warehousingDate: string;
  productList: WarehousingProduct[];
};

export type QRCodePrintConfirmData = {
  image: string;
  totalLogisticsQuantity: number;
  warehousingDate: string;
  productName: string;
  productStandard: string;
};

export type ReleaseProduct = Product & {
  boxCategoryId: string;
  warehouseId: string;
  warehouseName: string;
  customerId: string;
  usableQuantity: number;
  logisticsQuantity: number;
  logisticsExecutionQuantity: number;
};

export type MovementProduct = ReleaseProduct & {
  targetWarehouseName: string;
  targetWarehouseId: string;
};

export type Item = {
  productCategoryId: string;
  productCategoryName: string;
  maker: string;
  productCodePrefix: string;
  registDate: string;
  deleteFlag: boolean;
};

export type Client = {
  customerId: string;
  customerName: string;
  logisticsCategory: ClientKind;
  phoneNumber: string;
  mailId: string;
  note: string;
  deleteFlag: boolean;
};

// 공장
export type Factory = {
  id: string;
  name: string;
  memo: string;
  status: string;
};

// 제품군
export type ProductLine = {
  id: string;
  name: string;
  memo: string;
  status: string;
};

// 제품타입
export type ProductType = {
  id: string;
  name: string;
  product_family: ProductLine;
  image_path: string;
  status: string;
};

// 제품타입 관련데이타
export type ProductTypeRelData = {
  //
};

export type Process = {
  processId: string;
  processName: string;
  processPname: string;
  workingTime: string;
  endTime: string;
  serialNumber: string;
  deleteFlag: boolean;
};

export const PROGRESS_DEFAULT = 0;
export const PROGRESS_START = 50;
export const PROGRESS_COMPLETE = 100;

export type CheckList = {
  checkListId: number;
  checkListName: string;
  registerTime: string;
  updateTime: string;
  status: string;
  checkListStep: CheckListStep[];
};

export type CheckListStep = {
  stepId: number;
  stepName: string;
  stepOrder: number;
  group: StepGroup;
  stepItems: CheckListItem[];
};

export type StepGroup = {
  groupId: string;
  groupName: string;
};

export type CheckListItem = {
  itemId: number;
  itemName: string;
  itemOrder: number;
};

export type WarehouseStock = Product & {
  warehouseId: string;
  warehouseName: string;
};

export type Warehouse = {
  warehouseId: string;
  warehouseName: string;
  note: string;
  maxQuantity: number;
  totalQuantity: number;
  deleteFlag: boolean;
};

export type WarehouseDetail = {
  detail: Warehouse;
  stock: WarehouseStock[];
};

export type DistributionStatus = {
  todayBoxCount: {
    releasingCnt: number;
    releasingCompleteCnt: number;
    warehousingCnt: number;
    warehousingCompleteCnt: number;
  };
  todayCount: {
    releasingCnt: number;
    releasingCompleteCnt: number;
    totalCnt: number;
    warehousingCnt: number;
    warehousingCompleteCnt: number;
  };
  warehouseStockTotal: number;
};

export type DistributionHistoryLogisticsCategory =
  | 'WAREHOUSING'
  | 'RELEASING'
  | 'MOVE'
  | 'ADJUSTMENT';

export type DistributionHistoryStatus = 'READY' | 'COMPLETE';

export type DistributionHistory = ReleaseProduct & {
  logisticsId: string;
  logisticsCategory: DistributionHistoryLogisticsCategory;
  status: DistributionHistoryStatus;
  productName: string;
  updateDate: string;
  registId: string;
  updateId: string;
  dueDate: string;
  deleteFlag: boolean;
};

export type DistributionRevisedHistory = {
  logDate: string;
  dueDate: string;
  executionQuantity: number;
  warehouseName: string;
  customerId: string;
  workerId: string;
};

export const PHYSICAL_DISTRIBUTION_SET_CATEGORY_IDX = 'PHYSICAL_DISTRIBUTION/SET_CATEGORY_IDX' as const;
export const PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_PANE_STATUS = 'PHYSICAL_DISTRIBUTION/CHANGE_PRODUCT_PANE_STATUS' as const;
export const PHYSICAL_DISTRIBUTION_SET_PRODUCT = 'PHYSICAL_DISTRIBUTION/SET_PRODUCT' as const;
export const PHYSICAL_DISTRIBUTION_SET_DIRECT_PRODUCT = 'PHYSICAL_DISTRIBUTION/SET_DIRECT_PRODUCT' as const;
export const PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_RELOAD_FLAG = 'PHYSICAL_DISTRIBUTION/CHANGE_PRODUCT_RELOAD_FLAG' as const;
export const PHYSICAL_DISTRIBUTION_SET_WAREHOUSE = 'PHYSICAL_DISTRIBUTION/SET_WAREHOUSE' as const;
export const PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_STATUS = 'PHYSICAL_DISTRIBUTION/SET_DISTRIBUTION_STATUS' as const;
export const PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG = 'PHYSICAL_DISTRIBUTION/CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG' as const;
export const PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_HISTORY = 'PHYSICAL_DISTRIBUTION/SET_DISTRIBUTION_HISTORY' as const;
export const PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG = 'PHYSICAL_DISTRIBUTION/CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG' as const;
export const PHYSICAL_DISTRIBUTION_SET_STATISTICS_WAREHOUSE_ID = 'PHYSICAL_DISTRIBUTION/SET_STATISTICS_WAREHOUSE_ID' as const;

export type Action =
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_CATEGORY_IDX;
      categoryIdx: Category | undefined;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_PANE_STATUS;
      paneStatus: PaneStatus;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_PRODUCT;
      product: Product | ReleaseProduct | null;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_DIRECT_PRODUCT;
      product: Product | ReleaseProduct | null;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_CHANGE_PRODUCT_RELOAD_FLAG;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_WAREHOUSE;
      warehouse: Warehouse | null;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_STATUS;
      distributionStatus: DistributionStatus;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_STATUS_RELOAD_FLAG;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_DISTRIBUTION_HISTORY;
      distributionHistory: DistributionHistory | null;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_CHANGE_DISTRIBUTION_HISTORY_RELOAD_FLAG;
    }
  | {
      type: typeof PHYSICAL_DISTRIBUTION_SET_STATISTICS_WAREHOUSE_ID;
      warehouseId: string;
    };
