import React from 'react';
import ReactDOM from 'react-dom';
import './locales/i18n';
import './index.scss';
import './lib/fontawesome';
import App from './App';
import reportWebVitals from './reportWebVitals';

ReactDOM.render(<App />, document.getElementById('root'));

reportWebVitals();
