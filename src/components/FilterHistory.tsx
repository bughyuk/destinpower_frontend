import { useTranslation } from 'react-i18next';
import Flatpickr from 'react-flatpickr';

import React, {
  Children,
  cloneElement,
  CSSProperties,
  forwardRef,
  isValidElement,
  ReactNode,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { useDropdown } from '@/modules/common/hook';
import { Dropdown } from 'react-bootstrap';
import classNames from 'classnames';
import { CommonUtils } from '@/utils';
import MaterialIcon from '@/components/MaterialIcon';
import RangeSlider from '@/components/RangeSlider';
import moment from 'moment';
import {
  useControlArea,
  useControlSpace,
  useHistoryUser,
  useOpenLayers,
} from '@/modules/map/hook';
import { ImageWMS } from 'ol/source';
import { Area, HistorySimulationDate, HistoryUser } from '@/modules/map/types';
import { fetchHistoryAllUserTrace, fetchHistoryUser } from '@/api/map';
import _ from 'lodash';
import {
  CellphoneType,
  GenderType,
  HistoryFilter,
  RangeValueType,
} from '@/modules/setup/types';
import { useControlMode, useSetupData } from '@/modules/setup/hook';
import { useUser } from '@/modules/user/hook';
import VectorSource from 'ol/source/Vector';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { Icon, Style } from 'ol/style';

let simulationTimer: ReturnType<typeof setInterval> | null;
const simulationTraceIdx = {} as any;
const simulationTraceDateIdx = {} as any;
const simulationTraceData = new Map<string, HistorySimulationDate>();
function FilterHistory() {
  const [values, setValues] = useState<HistoryFilter>({
    period: [],
    startTime: '',
    endTime: '',
    gender: '',
    cellphone: '',
    age: {
      from: 0,
      to: 100,
    },
    residenceTime: {
      from: 0,
      to: 10,
    },
  });

  const [mounted, setMounted] = useState(false);
  const [isSetupClicked, setSetupClicked] = useState(false);
  const [loading, setLoading] = useState(false);
  const { historyStandard } = useControlMode();
  const { historyUserFilter, handleSetHistoryFilter } = useSetupData();
  const { handleSetHistoryUser } = useHistoryUser();
  const { space } = useControlSpace();
  const { selectedAreaFeature } = useControlArea();
  const {
    heatMapLayer,
    distributionLayer,
    historySimulationLayer,
  } = useOpenLayers();
  const { user } = useUser();

  useEffect(() => {
    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        setValues({
          ...values,
          period: filter.period,
        });
      }
    }
  }, [historyUserFilter.filter?.period]);

  useEffect(() => {
    setMounted(true);

    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        setValues(filter);
        handleSetLayerParams(filter);
      }
    }

    return () => {
      if (simulationTimer) {
        clearInterval(simulationTimer);
        simulationTimer = null;
      }
    };
  }, []);

  useEffect(() => {
    if (space.spaceMappingId && space.floorsMapId && mounted) {
      if (isSetupClicked) {
        handleSetup();
      }
    }
  }, [space]);

  const handlePeriod = (period: string[]) => {
    setValues({
      ...values,
      period,
    });
  };

  const handleTime = (startTime: string, endTime: string) => {
    setValues({
      ...values,
      startTime,
      endTime,
    });
  };

  const handleGender = (gender: GenderType) => {
    setValues({
      ...values,
      gender,
    });
  };

  const handleCellphone = (cellphone: CellphoneType) => {
    setValues({
      ...values,
      cellphone,
    });
  };

  const handleAge = (from: number, to: number) => {
    setValues({
      ...values,
      age: {
        from,
        to,
      },
    });
  };

  const handleResidenceTime = (from: number, to: number) => {
    setValues({
      ...values,
      residenceTime: {
        from,
        to,
      },
    });
  };

  const handleFetchHistoryUser = async () => {
    const today = moment().format('YYYY-MM-DD');
    const historyUserSummary = await fetchHistoryUser({
      mapid: space.floorsMapId,
      startDate: values.period[0] || today,
      endDate: values.period[1] || today,
      stime: values.startTime,
      etime: values.endTime,
      user_gender: values.gender,
      device_type: values.cellphone,
      ageTo: values.age.to,
      ageFrom: values.age.from,
      visitTo: values.residenceTime.to,
      visitFrom: values.residenceTime.from,
    });

    handleSetHistoryFilter(values);
    handleSetHistoryUser(_.uniqBy(historyUserSummary, 'access_key'));

    historySimulationLayer?.getSource().clear();

    if (simulationTimer) {
      clearInterval(simulationTimer);
      simulationTimer = null;
    }

    if (user.userId === 'shirakawa@skyland.jp') {
      simulationHistoryUser();
    }
  };

  const handleFilterHistoryUser = () => {
    handleSetHistoryFilter(values);
    const filterUser: HistoryUser[] = [];
    historyUserFilter.users.some((user) => {
      const userAge = user.user_age;
      if (values.age.from > userAge || values.age.to < userAge) {
        return true;
      }

      if (values.gender) {
        const userGender = user.user_gender;

        if (values.gender === 'W' && userGender === 'M') {
          return true;
        }

        if (values.gender === 'M' && userGender === 'W') {
          return true;
        }
      }

      if (values.cellphone) {
        const userDeviceType = user.device_type;

        if (values.cellphone === 'Android') {
          if (userDeviceType.toLowerCase() === 'ios') {
            return true;
          }
        } else if (values.cellphone === 'Iphone') {
          if (userDeviceType.toLowerCase() === 'android') {
            return true;
          }
        }
      }

      filterUser.push(user);
    });

    handleSetHistoryUser(filterUser);
  };

  const handleSetLayerParams = (filter: HistoryFilter) => {
    const startTime = filter.startTime ? filter.startTime : '00:00:00';
    const endTime = filter.endTime ? filter.endTime : '23:59:59';
    let params = `map_id:'${space.floorsMapId}';`;
    if (filter.period.length > 1) {
      params += `starttime:'${filter.period[0]} ${startTime}';endtime:'${filter.period[1]} ${endTime}';`;
    } else {
      const today = moment().format('YYYY-MM-DD');
      params += `starttime:'${today} ${startTime}';endtime:'${today} ${endTime}';`;
    }
    if (filter.gender) {
      params += `user_gender:'${filter.gender}';`;
    }
    if (filter.cellphone) {
      params += `device_type:'${filter.cellphone}';`;
    }
    params += `from_age:'${filter.age.from}';to_age:'${filter.age.to}';`;
    params += `fromtime:'${filter.residenceTime.from}';totime:'${filter.residenceTime.to}';`;

    const updateParams = {
      CQL_FILTER: `map_id = '${space.floorsMapId}'`,
      viewparams: params,
    };

    if (selectedAreaFeature) {
      const { area_name: areaName } = selectedAreaFeature.get(
        'area_info'
      ) as Area;

      updateParams[
        'CQL_FILTER'
      ] = `${updateParams['CQL_FILTER']} area_name = '${areaName}'`;
    }
    const heatMapSource = heatMapLayer?.getSource() as ImageWMS;
    heatMapSource.updateParams(updateParams);
    const distributionSource = distributionLayer?.getSource() as ImageWMS;
    distributionSource.updateParams(updateParams);
  };

  const handleSetup = () => {
    if (!isSetupClicked) {
      setSetupClicked(true);
    }
    setLoading(true);
    handleSetLayerParams(values);

    if (historyStandard === 'condition') {
      handleFetchHistoryUser();
    } else if (historyStandard === 'user') {
      handleFilterHistoryUser();
    }

    setLoading(false);
  };

  const simulationHistoryUser = async () => {
    const data = await fetchHistoryAllUserTrace({
      mapid: space.floorsMapId,
      startDate: values.period[0],
      endDate: values.period[1],
      stime: values.startTime,
      etime: values.endTime,
      device_type: values.cellphone,
      user_gender: values.gender,
      ageTo: values.age.to,
      ageFrom: values.age.from,
      visitTo: values.residenceTime.to,
      visitFrom: values.residenceTime.from,
    });

    for (const userId in data) {
      const historySimulationDate: HistorySimulationDate = data[userId];
      let coordinates: number[] = [];
      simulationTraceIdx[userId] = 1;
      simulationTraceDateIdx[userId] = 0;
      simulationTraceData.set(userId, historySimulationDate);
      const firstLocationList =
        historySimulationDate[Object.keys(historySimulationDate)[0]];
      if (firstLocationList && firstLocationList.length > 0) {
        coordinates = firstLocationList[0];
      }

      if (historySimulationLayer?.getSource()) {
        const source: VectorSource = historySimulationLayer?.getSource();
        const iconFeature = new Feature(new Point(coordinates));
        iconFeature.setId(userId);
        const iconStyle = new Style({
          image: new Icon({
            src: `/static/images/person_pin.svg`,
            scale: 2,
          }),
        });
        iconFeature.setStyle(iconStyle);
        source?.addFeature(iconFeature);
      }
    }
    simulationTimer = setInterval(handleSimulation, 100);
  };

  const handleSimulation = () => {
    if (historySimulationLayer?.getSource()) {
      const source: VectorSource = historySimulationLayer?.getSource();
      for (const userId in simulationTraceIdx) {
        const traceIdx = simulationTraceIdx[userId];
        const historySimulationDate = simulationTraceData.get(userId);
        if (historySimulationDate) {
          const dateIdx = simulationTraceDateIdx[userId];
          const date = Object.keys(historySimulationDate)[dateIdx];
          if (date) {
            const locationList = historySimulationDate[date];
            const featureById = source.getFeatureById(userId);

            if (featureById) {
              featureById.setGeometry(new Point(locationList[traceIdx]));
            }

            if (locationList.length === traceIdx + 1) {
              simulationTraceIdx[userId] = 0;
              if (Object.keys(historySimulationDate).length === dateIdx + 1) {
                simulationTraceDateIdx[userId] = 0;
              } else {
                simulationTraceDateIdx[userId] = dateIdx + 1;
              }
            } else {
              simulationTraceIdx[userId] = traceIdx + 1;
            }
          }
        }
      }
    }
  };

  return (
    <div className="tabBox mapStauts01 status-con active">
      <Period onConfirmClick={handlePeriod} />
      <Time onConfirmClick={handleTime} />
      <Age initialValue={values.age} onChange={handleAge} />
      <Gender initialValue={values.gender} onChange={handleGender} />
      <Cellphone initialValue={values.cellphone} onChange={handleCellphone} />
      <ResidenceTime
        initialValue={values.residenceTime}
        onChange={handleResidenceTime}
      />
      <Setup loading={loading} onClick={handleSetup} />
    </div>
  );
}

type ContainerProps = {
  textKey: string;
  displayTextKey: string;
  displayValue?: string;
  show?: boolean;
  onChangeShow?: (isShow: boolean) => void;
  onHide?: () => void;
  children?: ReactNode;
  style?: CSSProperties;
};

function Container({
  textKey,
  displayTextKey,
  displayValue,
  show,
  onChangeShow,
  onHide,
  children,
  style,
}: ContainerProps) {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle, handleClose, isShowDropdown } = useDropdown(dropdown);

  useEffect(() => {
    if (!show) {
      handleClose();
    }
  }, [show]);

  useEffect(() => {
    if (!isShowDropdown) {
      if (onHide) {
        onHide();
      }
    }
  }, [isShowDropdown]);

  return (
    <div className="filter-cell filter-list d-flex align-items-center">
      <Dropdown
        onToggle={(isOpen, event, metadata) => {
          handleToggle(isOpen, event, metadata);
          onChangeShow?.call(null, isOpen);
        }}
        show={show}
        style={style}
      >
        <Dropdown.Toggle
          as={'a'}
          data-caret="false"
          className={classNames('filter-dropdown-toggle filter-select', {
            active: isShowDropdown,
          })}
        >
          <strong>
            {displayValue && (
              <>
                <span>{t(displayTextKey)}</span> {displayValue}
              </>
            )}
            {!displayValue && t(textKey)}
          </strong>
        </Dropdown.Toggle>
        {Children.map(children, (child, index) => {
          if (index === 0) {
            if (isValidElement(child)) {
              return cloneElement(child, {
                ref: dropdown,
              });
            }
          }
          return child;
        })}
      </Dropdown>
    </div>
  );
}

type PeriodProps = {
  onConfirmClick: (period: string[]) => void;
};

const date = new Date();
let startTime = moment(date).format('YYYY-MM-DD'),
  endTime = moment(date).format('YYYY-MM-DD');
function Period({ onConfirmClick }: PeriodProps) {
  const { t } = useTranslation();
  const [isShow, setShow] = useState(false);
  const { historyStandard } = useControlMode();
  const { historyUserFilter } = useSetupData();
  const handleDropdownShow = (isShow: boolean) => {
    setShow(isShow);
  };
  const [displayValue, setDisplayValue] = useState('');

  useEffect(() => {
    startTime = moment(date).format('YYYY-MM-DD');
    endTime = moment(date).format('YYYY-MM-DD');
  }, []);

  useEffect(() => {
    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        const filterPeriod = filter.period;
        if (filterPeriod.length === 2) {
          startTime = filterPeriod[0];
          endTime = filterPeriod[1];
          setDisplayValue(
            `${moment(filterPeriod[0]).format('YYYY.MM.DD')}~${moment(
              filterPeriod[1]
            ).format('YYYY.MM.DD')}`
          );
        }
      }
    }
  }, [historyUserFilter.filter?.period]);

  const Content = forwardRef<HTMLDivElement>((props, ref) => {
    const [period, setPeriod] = useState<string[]>([startTime, endTime]);

    return (
      <Dropdown.Menu
        className="date-pick filter-item-dropdown no-arrow"
        style={{
          width: '350px',
        }}
        ref={ref}
      >
        <div className="dropdown-body pt-8pt">
          <div className="form-group">
            <Flatpickr
              className="form-control"
              options={{
                mode: 'range',
                inline: true,
              }}
              onChange={(dates) => {
                setPeriod(
                  dates.map((date) => moment(date).format('YYYY-MM-DD'))
                );
              }}
              value={period}
            />
          </div>
        </div>
        <div
          className="d-flex align-items-center justify-content-center mb-2"
          onClick={(e) => {
            e.stopPropagation();
            if (period.length === 2) {
              startTime = moment(period[0]).format('YYYY-MM-DD');
              endTime = moment(period[1]).format('YYYY-MM-DD');
              onConfirmClick(period);
              setDisplayValue(
                `${moment(period[0]).format('YYYY.MM.DD')}~${moment(
                  period[1]
                ).format('YYYY.MM.DD')}`
              );
              setShow(false);
            }
          }}
        >
          <a
            className={classNames('btn btn-outline-secondary', {
              disabled: period.length < 2,
            })}
          >
            {t('text_confirm')}
          </a>
        </div>
      </Dropdown.Menu>
    );
  });
  Content.displayName = 'Content';

  return (
    <Container
      textKey={'text_selection_period'}
      displayTextKey={'text_period'}
      displayValue={displayValue}
      show={isShow}
      onChangeShow={handleDropdownShow}
    >
      <Content />
    </Container>
  );
}

type TimeProps = {
  onConfirmClick: (startTime: string, endTime: string) => void;
};

type Time = {
  ampm: string;
  hour: number;
  minute: number;
};

let formatStartTime = moment(date).add(-30, 'minutes'),
  formatEndTime = moment(date);
function Time({ onConfirmClick }: TimeProps) {
  const { t } = useTranslation();
  const [isShow, setShow] = useState(false);
  const handleDropdownShow = (isShow: boolean) => {
    setShow(isShow);
  };
  const [displayValue, setDisplayValue] = useState('');

  useEffect(() => {
    formatStartTime = moment(date).add(-30, 'minutes');
    formatEndTime = moment(date);
  }, []);

  const hour = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  const minute = [0, 10, 20, 30, 40, 50];

  const Content = forwardRef<HTMLDivElement>((props, ref) => {
    const timePattern = 'A hh mm';
    const [startTime, setStartTime] = useState<Time>({
      ampm: formatStartTime.format('A'),
      hour: Number(formatStartTime.format('h')),
      minute: Number(`${formatStartTime.format('mm').slice(0, 1)}0`),
    });

    const [endTime, setEndTime] = useState<Time>({
      ampm: formatEndTime.format('A'),
      hour: Number(formatEndTime.format('h')),
      minute: Number(`${formatEndTime.format('mm').slice(0, 1)}0`),
    });

    const checkTime = useMemo(() => {
      const d1 = moment(
        `${startTime.ampm} ${startTime.hour} ${startTime.minute}`,
        timePattern
      );

      const d2 = moment(
        `${endTime.ampm} ${endTime.hour} ${endTime.minute}`,
        timePattern
      );

      const diff = d2.diff(d1, 'milliseconds');

      return diff >= 0;
    }, [startTime, endTime]);

    return (
      <Dropdown.Menu
        className="filter-item-dropdown no-arrow"
        style={{
          width: '250px',
        }}
        ref={ref}
      >
        <Dropdown.Header>
          <strong>{t('text_start_time')}</strong>
        </Dropdown.Header>
        <div className="dropdown-body mb-4">
          <div className="form-row">
            <div className="col-lg-4">
              <select
                className="form-line"
                value={startTime.ampm}
                onChange={(e) => {
                  setStartTime({
                    ...startTime,
                    ampm: e.target.value,
                  });
                }}
              >
                <option value="AM">AM</option>
                <option value="PM">PM</option>
              </select>
            </div>
            <div className="col-lg-4">
              <select
                className="form-line"
                value={startTime.hour}
                onChange={(e) => {
                  setStartTime({
                    ...startTime,
                    hour: Number(e.target.value),
                  });
                }}
              >
                {hour.map((value) => (
                  <option key={value} value={value}>
                    {CommonUtils.padZeroTime(value)}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-lg-4">
              <select
                className="form-line"
                value={startTime.minute}
                onChange={(e) => {
                  setStartTime({
                    ...startTime,
                    minute: Number(e.target.value),
                  });
                }}
              >
                {minute.map((value) => (
                  <option key={value} value={value}>
                    {CommonUtils.padZeroTime(value)}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <Dropdown.Header>
          <strong>{t('text_end_time')}</strong>
        </Dropdown.Header>
        <div className="dropdown-body mb-4">
          <div className="form-row">
            <div className="col-lg-4">
              <select
                className="form-line"
                value={endTime.ampm}
                onChange={(e) => {
                  setEndTime({
                    ...endTime,
                    ampm: e.target.value,
                  });
                }}
              >
                <option value="AM">AM</option>
                <option value="PM">PM</option>
              </select>
            </div>
            <div className="col-lg-4">
              <select
                className="form-line"
                value={endTime.hour}
                onChange={(e) => {
                  setEndTime({
                    ...endTime,
                    hour: Number(e.target.value),
                  });
                }}
              >
                {hour.map((value) => (
                  <option key={value} value={value}>
                    {CommonUtils.padZeroTime(value)}
                  </option>
                ))}
              </select>
            </div>
            <div className="col-lg-4">
              <select
                className="form-line"
                value={endTime.minute}
                onChange={(e) => {
                  setEndTime({
                    ...endTime,
                    minute: Number(e.target.value),
                  });
                }}
              >
                {minute.map((value) => (
                  <option key={value} value={value}>
                    {CommonUtils.padZeroTime(value)}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <div
          className="d-flex align-items-center justify-content-center mb-2"
          onClick={(e) => {
            e.stopPropagation();

            if (checkTime) {
              formatStartTime = moment(
                `${startTime.ampm} ${startTime.hour} ${startTime.minute}`,
                timePattern
              );
              formatEndTime = moment(
                `${endTime.ampm} ${endTime.hour} ${endTime.minute}`,
                timePattern
              );

              const startTimeValue = formatStartTime.format('HH:mm');
              const endTimeValue = formatEndTime.format('HH:mm');
              onConfirmClick(startTimeValue, endTimeValue);
              setDisplayValue(`${startTimeValue}~${endTimeValue}`);
              setShow(false);
            }
          }}
        >
          <a
            className={classNames('btn btn-outline-secondary', {
              disabled: !checkTime,
            })}
          >
            {t('text_confirm')}
          </a>
        </div>
      </Dropdown.Menu>
    );
  });
  Content.displayName = 'Content';

  return (
    <Container
      textKey={'text_selection_time'}
      displayTextKey={'text_time'}
      displayValue={displayValue}
      show={isShow}
      onChangeShow={handleDropdownShow}
    >
      <Content />
    </Container>
  );
}

type GenderProps = {
  initialValue: GenderType;
  onChange: (value: GenderType) => void;
};

function Gender({ initialValue, onChange }: GenderProps) {
  const { t } = useTranslation();
  const { historyStandard } = useControlMode();
  const { historyUserFilter } = useSetupData();
  let sendValue: GenderType = initialValue;
  const [displayValue, setDisplayValue] = useState('');

  useEffect(() => {
    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        const filterGender = filter.gender;
        if (filterGender === 'M') {
          setDisplayValue(t('text_male'));
        } else if (filterGender === 'W') {
          setDisplayValue(t('text_female'));
        }
      }
    }
  }, []);

  const Content = forwardRef<HTMLDivElement>((props, ref) => {
    const [gender, setGender] = useState<GenderType>(initialValue);

    return (
      <Dropdown.Menu className="filter-item-dropdown no-arrow" ref={ref}>
        <div className="dropdown-body my-2">
          <div className="form-check custom-checkbox">
            <input
              className="custom-control-input"
              type="radio"
              name="gender"
              value=""
              checked={gender === ''}
              onChange={(e) => {
                const value = e.target.value as GenderType;
                sendValue = value;
                setGender(value);
              }}
              id="genderAll"
            />
            <label className="custom-control-label" htmlFor="genderAll">
              {t('text_the_whole')}
            </label>
          </div>
          <div className="form-check custom-checkbox mt-12pt">
            <input
              className="custom-control-input"
              type="radio"
              name="gender"
              value="M"
              checked={gender === 'M'}
              onChange={(e) => {
                const value = e.target.value as GenderType;
                sendValue = value;
                setGender(value);
              }}
              id="genderMale"
            />
            <label className="custom-control-label" htmlFor="genderMale">
              {t('text_male')}
            </label>
          </div>
          <div className="form-check custom-checkbox mt-12pt">
            <input
              className="custom-control-input"
              type="radio"
              name="gender"
              value="W"
              checked={gender === 'W'}
              onChange={(e) => {
                const value = e.target.value as GenderType;
                sendValue = value;
                setGender(value);
              }}
              id="genderFemale"
            />
            <label className="custom-control-label" htmlFor="genderFemale">
              {t('text_female')}
            </label>
          </div>
        </div>
      </Dropdown.Menu>
    );
  });
  Content.displayName = 'Content';

  return (
    <Container
      textKey={'text_gender'}
      displayTextKey={'text_gender'}
      displayValue={displayValue}
      onHide={() => {
        onChange(sendValue);
        if (sendValue) {
          if (sendValue === 'M') {
            setDisplayValue(t('text_male'));
          } else {
            setDisplayValue(t('text_female'));
          }
        } else {
          setDisplayValue('');
        }
      }}
      style={{
        width: '70px',
      }}
    >
      <Content />
    </Container>
  );
}

type CellphoneProps = {
  initialValue: CellphoneType;
  onChange: (value: CellphoneType) => void;
};

function Cellphone({ initialValue, onChange }: CellphoneProps) {
  const { t } = useTranslation();
  const { historyStandard } = useControlMode();
  const { historyUserFilter } = useSetupData();
  let sendValue: CellphoneType = initialValue;
  const [displayValue, setDisplayValue] = useState('');

  useEffect(() => {
    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        const filterCellphone = filter.cellphone;

        if (filterCellphone === 'Iphone') {
          setDisplayValue('iOS');
        } else if (filterCellphone === 'Android') {
          setDisplayValue('Android');
        }
      }
    }
  }, []);

  const Content = forwardRef<HTMLDivElement>((props, ref) => {
    const [cellphone, setCellphone] = useState<CellphoneType>(initialValue);

    return (
      <Dropdown.Menu className="filter-item-dropdown no-arrow" ref={ref}>
        <div className="dropdown-body my-2">
          <div className="form-check custom-checkbox">
            <input
              className="custom-control-input"
              type="radio"
              name="deviceType"
              value=""
              checked={cellphone === ''}
              onChange={(e) => {
                const value = e.target.value as CellphoneType;
                sendValue = value;
                setCellphone(value);
              }}
              id="deviceTypeAll"
            />
            <label className="custom-control-label" htmlFor="deviceTypeAll">
              {t('text_the_whole')}
            </label>
          </div>

          <div className="form-check custom-checkbox mt-12pt">
            <input
              className="custom-control-input"
              type="radio"
              name="deviceType"
              value="Android"
              checked={cellphone === 'Android'}
              onChange={(e) => {
                const value = e.target.value as CellphoneType;
                sendValue = value;
                setCellphone(value);
              }}
              id="deviceTypeAndroid"
            />
            <label className="custom-control-label" htmlFor="deviceTypeAndroid">
              Android
            </label>
          </div>
          <div className="form-check custom-checkbox mt-12pt">
            <input
              className="custom-control-input"
              type="radio"
              name="deviceType"
              value="Iphone"
              checked={cellphone === 'Iphone'}
              onChange={(e) => {
                const value = e.target.value as CellphoneType;
                sendValue = value;
                setCellphone(value);
              }}
              id="deviceTypeIOS"
            />
            <label className="custom-control-label" htmlFor="deviceTypeIOS">
              iOS
            </label>
          </div>
        </div>
      </Dropdown.Menu>
    );
  });
  Content.displayName = 'Content';

  return (
    <Container
      textKey={'text_kind_of_cell_phone'}
      displayTextKey={'text_cell_phone'}
      displayValue={displayValue}
      onHide={() => {
        onChange(sendValue);
        if (sendValue === 'Iphone') {
          setDisplayValue('iOS');
        } else {
          setDisplayValue(sendValue);
        }
      }}
    >
      <Content />
    </Container>
  );
}

type AgeProps = {
  initialValue: RangeValueType;
  onChange: (from: number, to: number) => void;
};

function Age({ initialValue, onChange }: AgeProps) {
  let sendValue: RangeValueType | null = null;
  const [displayValue, setDisplayValue] = useState('');

  const { historyStandard } = useControlMode();
  const { historyUserFilter } = useSetupData();
  useEffect(() => {
    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        const filterAge = filter.age;
        setDisplayValue(`${filterAge.from}~${filterAge.to}`);
      }
    }
  }, []);

  const Content = forwardRef<HTMLDivElement>((props, ref) => {
    const [age, setAge] = useState<RangeValueType>(initialValue);

    return (
      <Dropdown.Menu
        className="filter-item-dropdown no-arrow"
        style={{
          width: '300px',
        }}
        ref={ref}
      >
        <div className="dropdown-body my-2">
          <div className="flex" style={{ maxWidth: '100%' }}>
            <RangeSlider
              type={'double'}
              min={0}
              max={100}
              from={age.from}
              to={age.to}
              grid={true}
              onChange={(data) => {
                const result = {
                  from: data.from,
                  to: data.to,
                };
                setAge(result);
                sendValue = result;
              }}
            />
          </div>
        </div>
      </Dropdown.Menu>
    );
  });
  Content.displayName = 'Content';

  return (
    <Container
      textKey={'text_range_of_age'}
      displayTextKey={'text_range_of_age'}
      displayValue={displayValue}
      onHide={() => {
        if (sendValue) {
          onChange(sendValue.from, sendValue.to);
          setDisplayValue(`${sendValue.from}~${sendValue.to}`);
        }
      }}
    >
      <Content />
    </Container>
  );
}

type ResidenceTimeProps = {
  initialValue: RangeValueType;
  onChange: (from: number, to: number) => void;
};

function ResidenceTime({ initialValue, onChange }: ResidenceTimeProps) {
  let sendValue: RangeValueType | null = null;
  const [displayValue, setDisplayValue] = useState('');

  const { historyStandard } = useControlMode();
  const { historyUserFilter } = useSetupData();
  useEffect(() => {
    if (historyStandard === 'user') {
      const filter = historyUserFilter.filter;
      if (filter) {
        const filterResidenceTime = filter.residenceTime;
        setDisplayValue(
          `${filterResidenceTime.from}~${filterResidenceTime.to}`
        );
      }
    }
  }, []);

  const Content = forwardRef<HTMLDivElement>((props, ref) => {
    const [residenceTime, setResidenceTime] = useState<RangeValueType>(
      initialValue
    );

    return (
      <Dropdown.Menu
        className="filter-item-dropdown no-arrow"
        style={{
          width: '300px',
        }}
        ref={ref}
      >
        <div className="dropdown-body my-2">
          <div className="flex" style={{ maxWidth: '100%' }}>
            <RangeSlider
              type={'double'}
              min={0}
              max={100}
              from={residenceTime.from}
              to={residenceTime.to}
              grid={true}
              onChange={(data) => {
                const result = {
                  from: data.from,
                  to: data.to,
                };
                setResidenceTime(result);
                sendValue = result;
              }}
            />
          </div>
        </div>
      </Dropdown.Menu>
    );
  });
  Content.displayName = 'Content';

  return (
    <Container
      textKey={'text_residence_time_minute'}
      displayTextKey={'text_residence_time'}
      displayValue={displayValue}
      onHide={() => {
        if (sendValue) {
          onChange(sendValue.from, sendValue.to);
          setDisplayValue(`${sendValue.from}~${sendValue.to}`);
        }
      }}
    >
      <Content />
    </Container>
  );
}

type SetupProps = {
  loading: boolean;
  onClick: () => void;
};

function Setup({ loading, onClick }: SetupProps) {
  const { t } = useTranslation();

  return (
    <div
      className="filter-btn-cell d-flex flex justify-content-end align-items-center"
      onClick={onClick}
    >
      {loading && <a className={'is-loading is-loading-sm'}>Loading</a>}
      {!loading && (
        <a className="">
          <MaterialIcon name={'done'} className={'mr-1'} /> {t('text_set_up')}
        </a>
      )}
    </div>
  );
}

export default FilterHistory;
