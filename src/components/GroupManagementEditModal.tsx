import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Modal, ModalBody, ModalFooter } from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/ModalHeader';
import MaterialIcon from '@/components/MaterialIcon';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import { ModalProps } from '@/modules/common';
import { Group } from '@/modules/group/types';
import { putGroup } from '@/api/group';

type GroupManagementEditModalProps = ModalProps & {
  group: Group | null;
};

function GroupManagementEditModal({
  group,
  show,
  onHide,
  onReload,
}: GroupManagementEditModalProps) {
  const { t } = useTranslation();
  const [inputs, setInputs] = useState<{
    groupSeq: number;
    groupName: string;
    groupDescription: string;
  }>({
    groupSeq: -1,
    groupName: '',
    groupDescription: '',
  });

  useEffect(() => {
    if (group) {
      setInputs({
        groupSeq: group.groupSeq,
        groupName: group.groupName,
        groupDescription: group.groupDesc,
      });
    }
  }, [group]);

  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (inputs.groupName && inputs.groupDescription) {
      setShowInvalidMessage(false);
    }
  }, [inputs]);

  const handleSubmit = async () => {
    if (!inputs.groupName || !inputs.groupDescription) {
      setShowInvalidMessage(true);
      return;
    }

    const result = await putGroup(inputs.groupSeq, {
      groupName: inputs.groupName,
      groupDesc: inputs.groupDescription,
    });

    if (result) {
      onReload?.call(null);
      onHide();
    }
  };

  return (
    <Modal
      show={show}
      onExited={() => {
        setInputs({
          groupSeq: -1,
          groupName: '',
          groupDescription: '',
        });
      }}
      onHide={() => {
        //
      }}
      centered={true}
      dialogClassName={'modal-custom'}
    >
      <ModalHeader>
        <button type="button" className="close custom-close" onClick={onHide}>
          <span>
            <MaterialIcon name={'clear'} />
          </span>
        </button>
      </ModalHeader>
      <ModalBody>
        <div className="title-group mb-5">
          <h5 className="modal-title">{t('text_edit_a_group')}</h5>
          <p className="m-0">{t('msg_group_edit_description')}</p>
        </div>

        <div className="form-group mb-4">
          <FormLabel
            textKey={'text_group_name'}
            essential={true}
            htmlFor={'groupName'}
          />
          <input
            id={'groupName'}
            type="text"
            className="form-line pr-6"
            placeholder={t('msg_enter_group_name')}
            value={inputs.groupName}
            onChange={(e) => {
              setInputs({
                ...inputs,
                [e.target.id]: e.target.value,
              });
            }}
            autoComplete={'off'}
          />
        </div>
        <div className="form-group mb-4">
          <FormLabel
            textKey={'text_group_description'}
            essential={true}
            htmlFor={'groupDescription'}
          />
          <input
            id={'groupDescription'}
            type="text"
            className="form-line pr-6"
            placeholder={t('msg_enter_description')}
            value={inputs.groupDescription}
            onChange={(e) => {
              setInputs({
                ...inputs,
                [e.target.id]: e.target.value,
              });
            }}
            autoComplete={'off'}
          />
        </div>
      </ModalBody>
      {showInvalidMessage && <InvalidAlert />}
      <ModalFooter>
        <button type="button" className="btn btn-link" onClick={onHide}>
          {t('text_cancel')}
        </button>
        <button type="button" className="btn btn-accent" onClick={handleSubmit}>
          {t('text_edit')}
        </button>
      </ModalFooter>
    </Modal>
  );
}

export default GroupManagementEditModal;
