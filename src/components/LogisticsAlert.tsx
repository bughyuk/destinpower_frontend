import React, { useEffect } from 'react';
import firebase from 'firebase/app';
import Toastr from 'toastr';
import {
  usePhysicalDistributionHistory,
  usePhysicalDistributionStatus,
} from '@/modules/physical_distribution/hook';
import { useControlProject } from '@/modules/map/hook';

function LogisticsAlert() {
  const { project } = useControlProject();
  const {
    handleChangeReloadFlag: handleChangeStatusReloadFlag,
  } = usePhysicalDistributionStatus();
  const {
    handleChangeReloadFlag: handleChangeHistoryReloadFlag,
  } = usePhysicalDistributionHistory();

  useEffect(() => {
    if (firebase.apps.length) {
      const firebaseApp = firebase.apps.find(
        (app) => app.name === 'WATA_CONTROL_PLATFORM_APP'
      );

      if (firebaseApp && firebase.messaging.isSupported()) {
        const messaging = firebaseApp.messaging();
        if (messaging) {
          messaging.onMessage((payload) => {
            const data = payload.data;
            const projectId = data.projectId;
            if (projectId === project.id) {
              Toastr.clear();
              if (data.type === 'logistics') {
                Toastr.info(data.body, '', {
                  positionClass: 'toast-bottom-right',
                });
                handleChangeStatusReloadFlag();
                handleChangeHistoryReloadFlag();
              } else if (data.type === 'sensor') {
                Toastr.warning(data.body, '', {
                  positionClass: 'toast-bottom-right',
                });
              }
            }
          });
        }
      }
    }
  }, [firebase.apps.length]);

  return <></>;
}

export default LogisticsAlert;
