import React, { ReactElement, useEffect } from 'react';
import { PANE_STATUS_CATEGORY, PaneStatus } from '@/utils/constants/common';
import {
  Category,
  PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_CLIENT_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY,
  PHYSICAL_DISTRIBUTION_CATEGORY_ITEM_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
  PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
} from '@/modules/physical_distribution/types';
import { useTranslation } from 'react-i18next';
import { useRightPane } from '@/modules/setup/hook';
import { RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY } from '@/modules/setup/types';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import PhysicalDistributionProduct from '@/components/PhysicalDistributionProduct';
import PhysicalDistributionItem from '@/components/PhysicalDistributionItem';
import PhysicalDistributionClient from '@/components/PhysicalDistributionClient';
import PhysicalDistributionWarehouse from '@/components/PhysicalDistributionWarehouse';
import PhysicalDistributionRelease from '@/components/PhysicalDistributionRelease';
import PhysicalDistributionHistory from '@/components/PhysicalDistributionHistory';

export type PhysicalDistributionListContentProps = {
  categoryIdx?: Category;
  title?: string;
  onClickBack?: () => void;
};

type PhysicalDistributionListProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function PhysicalDistributionList({
  onChangeStatus,
}: PhysicalDistributionListProps) {
  const { t } = useTranslation();
  const { categoryIdx } = usePhysicalDistributionCategory();
  const { handleChangeShow } = useRightPane();

  useEffect(() => {
    handleChangeShow(true, RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY);
  }, []);

  let title = '';
  let content: ReactElement = <PhysicalDistributionProduct />;
  switch (categoryIdx) {
    case PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT:
      title = t('text_product_list');
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING:
      title = t('text_warehousing');
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE:
      title = t('text_release');
      content = <PhysicalDistributionRelease />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT:
      title = t('text_adjustment');
      content = <PhysicalDistributionRelease />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT:
      title = t('text_movement');
      content = <PhysicalDistributionRelease />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY:
      title = t('text_history');
      content = <PhysicalDistributionHistory />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT:
      title = t('text_warehouse_management');
      content = <PhysicalDistributionWarehouse />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_ITEM_MANAGEMENT:
      title = t('text_item_management');
      content = <PhysicalDistributionItem />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_CLIENT_MANAGEMENT:
      title = t('text_client_management');
      content = <PhysicalDistributionClient />;
      break;
  }

  const handleClickBack = () => {
    onChangeStatus(PANE_STATUS_CATEGORY);
  };

  return (
    <>
      {React.cloneElement<PhysicalDistributionListContentProps>(content, {
        categoryIdx,
        title,
        onClickBack: handleClickBack,
      })}
    </>
  );
}

export default PhysicalDistributionList;
