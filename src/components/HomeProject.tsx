import React, { useEffect, useState } from 'react';
import HomeHeader from '@/components/HomeHeader';
import { useTranslation } from 'react-i18next';
import {
  Project,
  Projects,
  SOLUTION_TYPE_SMART_CONTROL,
  SOLUTION_TYPE_SMART_FACTORY,
  SOLUTION_TYPE_SMART_HOSPITAL,
  SOLUTION_TYPE_SMART_OFFICE,
  SOLUTION_TYPE_SMART_RETAIL,
} from '@/modules/project/types';
import { useHistory } from 'react-router-dom';
import { useControlProject } from '@/modules/map/hook';
import { fetchProject, fetchRecentProjects } from '@/api/project';
import { Floors, Space } from '@/modules/map/types';
import { Card } from 'react-bootstrap';
import moment from 'moment';
import Preloader from '@/components/Preloader';
import EmptyList from '@/components/EmptyList';
import { useProjectPane } from '@/modules/project/hook';
import { PANE_STATUS_REGISTER } from '@/utils/constants/common';
import ProjectListTable from '@/components/ProjectListTable';
import { useUser } from '@/modules/user/hook';

function HomeProject() {
  const history = useHistory();
  const { user } = useUser();
  const { handleChangePaneStatus } = useProjectPane();
  const [count, setCount] = useState(0);
  const [searchKeyword, setSearchKeyword] = useState('');
  const [reloadFlag, setReloadFlag] = useState(false);

  const handleSubmitSearch = (searchKeyword: string) => {
    setSearchKeyword(searchKeyword);
  };

  const handleClickRegisterButton = () => {
    handleChangePaneStatus(PANE_STATUS_REGISTER);
    history.push('/project');
  };

  const handleReload = () => {
    setReloadFlag(!reloadFlag);
  };

  return (
    <>
      <div className="contents-section border-bottom-1">
        <div className="cell">
          <RecentProject reloadFlag={reloadFlag} />
        </div>
      </div>
      <div className="contents-section">
        <div className="cell">
          <HomeHeader
            titleKey={'text_project_list'}
            registerButtonTextKey={'text_project_new_registration'}
            count={count}
            descriptionTextKey={'msg_there_are_number_project_registered'}
            onSubmitSearch={handleSubmitSearch}
            onClickRegisterButton={handleClickRegisterButton}
          />
          <ProjectListTable
            showCheckbox={user.userType === 'OWNER'}
            showOptions={user.userType === 'OWNER'}
            searchKeyword={searchKeyword}
            onChangeCount={setCount}
            onReload={handleReload}
          />
        </div>
      </div>
    </>
  );
}

type RecentProjectProps = {
  reloadFlag: boolean;
};

function RecentProject({ reloadFlag }: RecentProjectProps) {
  const { t } = useTranslation();
  const [load, setLoad] = useState(false);
  const [recentProjectList, setRecentProjectList] = useState<Projects>([]);

  const handleFetchRecentProjects = async () => {
    const data = await fetchRecentProjects();
    if (data) {
      setRecentProjectList(data);
    }
    setLoad(true);
  };

  useEffect(() => {
    handleFetchRecentProjects();
  }, [reloadFlag]);

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_list_of_recent_projects')}
          </h3>
        </div>
      </div>
      {!load && <Preloader />}
      {load && recentProjectList.length === 0 && (
        <EmptyList listType={'project'} />
      )}
      {load && recentProjectList.length > 0 && (
        <div className="row card-group-row">
          {recentProjectList.map((recentProject, idx) => (
            <RecentProjectCard
              key={recentProject.projectId}
              idx={idx}
              {...recentProject}
            />
          ))}
        </div>
      )}
    </>
  );
}

type RecentProjectCardProps = Project & {
  idx: number;
};

function RecentProjectCard({
  projectId,
  projectName,
  solutionType,
  registDate,
  idx,
}: RecentProjectCardProps) {
  const { t } = useTranslation();
  const history = useHistory();
  const { handleSetProject } = useControlProject();

  const handleRouteProject = async () => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((projectSpace) => {
        const projectFloors: Floors[] = [];
        projectSpace.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: projectSpace.mappingId,
          id: projectSpace.metaId,
          name: projectSpace.metaName,
          longitude: projectSpace.lng,
          latitude: projectSpace.lat,
          floorsList: projectFloors,
          registDate: projectSpace.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.push('/control');
    }
  };

  let solutionText = '';

  switch (solutionType) {
    case SOLUTION_TYPE_SMART_CONTROL:
      solutionText = t('text_smart_control');
      break;
    case SOLUTION_TYPE_SMART_OFFICE:
      solutionText = t('text_smart_office');
      break;
    case SOLUTION_TYPE_SMART_HOSPITAL:
      solutionText = t('text_smart_hospital');
      break;
    case SOLUTION_TYPE_SMART_FACTORY:
      solutionText = t('text_smart_factory');
      break;
    case SOLUTION_TYPE_SMART_RETAIL:
      solutionText = t('text_smart_retail');
      break;
  }

  return (
    <div className="col-lg-3 card-group-row__col">
      <Card className="o-hidden mb-0">
        <a
          onClick={(e) => {
            e.stopPropagation();
            handleRouteProject();
          }}
          className="card-img-top js-image"
        >
          <span className="pj-type">{solutionText}</span>
          <Card.Img
            bsPrefix={' '}
            style={{ height: '250px' }}
            src={`/static/images/slider0${idx + 1}.jpg`}
            width="368"
            height="250"
          />
        </a>
        <Card.Body className="flex">
          <div className="d-flex">
            <div className="flex">
              <span className="card-title">
                <a
                  onClick={(e) => {
                    e.stopPropagation();
                    handleRouteProject();
                  }}
                >
                  {projectName}
                </a>
              </span>
              <small className="text-50 font-weight-bold mb-4pt">
                {moment(registDate).format('YYYY.MM.DD')}
              </small>
            </div>
          </div>
        </Card.Body>
      </Card>
    </div>
  );
}

export default HomeProject;
