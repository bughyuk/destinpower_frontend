import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Project, Projects } from '@/modules/project/types';
import EmptyList from '@/components/EmptyList';
import {
  deleteProject,
  fetchProject,
  fetchProjects,
  putProjectStepData,
} from '@/api/project';
import { ListResult } from '@/modules/common';
import { fetchSpaceInfo } from '@/api/space';
import { CommonUtils } from '@/utils';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import Pagination from '@/components/Pagination';
import {
  useProjectEdit,
  useProjectPane,
  useProjectRegister,
} from '@/modules/project/hook';
import { useHistory } from 'react-router-dom';
import DateBadge from '@/components/DateBadge';
import { Badge } from 'react-bootstrap';
import moment from 'moment';
import ProjectListDropdown from '@/components/ProjectListDropdown';
import { Floors, Space } from '@/modules/map/types';
import { useControlProject } from '@/modules/map/hook';
import { useSpace } from '@/modules/space/hook';

type ProjectListTableProps = {
  showCheckbox?: boolean;
  showOptions?: boolean;
  searchKeyword?: string;
  onChangeCount?: (count: number) => void;
  onChangeCheck?: (checked: boolean, projectId: string) => void;
  onChangeCheckAll?: (checked: boolean, projectIds: string[]) => void;
  onReload?: () => void;
};

function ProjectListTable({
  showCheckbox = true,
  showOptions = true,
  searchKeyword = '',
  onChangeCount,
  onChangeCheck,
  onChangeCheckAll,
  onReload,
}: ProjectListTableProps) {
  const { t } = useTranslation();
  const [projectList, setProjectList] = useState<Projects>([]);
  const [load, setLoad] = useState(false);
  const [checkList, setCheckList] = useState<string[]>([]);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    handleFetchProject();
  }, [searchKeyword, page]);

  const handleFetchProject = async () => {
    setLoad(false);
    setProjectList([]);
    setTotalCount(0);
    onChangeCount?.call(null, 0);
    const result = await fetchProjects(page, searchKeyword);
    const listResult = result as ListResult<Projects>;
    setProjectList(listResult.content);
    setTotalCount(listResult.totalElements);
    onChangeCount?.call(null, listResult.totalElements);
    setLoad(true);
  };

  const handleReloadProject = () => {
    onReload?.call(null);
    handleFetchProject();
  };

  const handleCheckChange = (checked: boolean, checkedProjectId: string) => {
    onChangeCheck?.call(null, checked, checkedProjectId);
    if (checked) {
      setCheckList([...checkList, checkedProjectId]);
    } else {
      setCheckList(
        checkList.filter((projectId) => projectId !== checkedProjectId)
      );
    }
  };

  const handleCheckAllChange = (checked: boolean) => {
    const idList: string[] = [];
    projectList.forEach((project) => idList.push(project.projectId));
    onChangeCheckAll?.call(null, checked, idList);
    if (checked) {
      setCheckList(idList);
    } else {
      setCheckList([]);
    }
  };

  const handleClickSelectionDelete = async () => {
    if (checkList.length) {
      const promise: Promise<any>[] = [];
      checkList.forEach((checkedProjectId) => {
        promise.push(deleteProject(checkedProjectId));
      });
      await Promise.all(promise);
      handleReloadProject();
    }
  };

  let allCheck = false;
  projectList.some((project) => {
    if (!checkList.includes(project.projectId)) {
      allCheck = false;
      return true;
    } else {
      allCheck = true;
    }
  });

  if (load && projectList.length === 0) {
    return <EmptyList listType={'project'} />;
  }

  return (
    <>
      <table className="table mb-4 table-nowrap">
        <colgroup>
          {showCheckbox && <col width="5%" />}
          <col width="*" />
          <col width="13%" />
          <col width="13%" />
          {showOptions && <col width="5%" />}
        </colgroup>
        <thead>
          <TableHeader
            check={projectList.length !== 0 && allCheck}
            showCheckbox={showCheckbox}
            showOptions={showOptions}
            onChange={handleCheckAllChange}
          />
        </thead>
        <tbody className="list home-list">
          {projectList.map((project, i) => {
            return (
              <TableItem
                key={project.projectId}
                {...project}
                randomIdx={(i % 4) + 1}
                checkList={checkList}
                showCheckbox={showCheckbox}
                showOptions={showOptions}
                onChange={handleCheckChange}
                onReload={handleReloadProject}
              />
            );
          })}
        </tbody>
      </table>
      {showCheckbox && (
        <div className="flex mb-4">
          <a
            className="btn btn-outline-secondary btn-rounded"
            onClick={handleClickSelectionDelete}
          >
            {t('text_selection_delete')}
          </a>
        </div>
      )}
      <Pagination
        curPage={page}
        totalCount={totalCount}
        onPageChange={setPage}
      />
    </>
  );
}

type TableHeaderProps = {
  check: boolean;
  showCheckbox: boolean;
  showOptions: boolean;
  onChange: (check: boolean) => void;
};

function TableHeader({
  check,
  showCheckbox,
  showOptions,
  onChange,
}: TableHeaderProps) {
  const { t } = useTranslation();

  return (
    <tr>
      {showCheckbox && (
        <th>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input js-toggle-check-all"
              id="checkAll"
              checked={check}
              onChange={(e) => onChange(e.target.checked)}
            />
            <label className="custom-control-label" htmlFor="checkAll">
              <span className="text-hide">Check All</span>
            </label>
          </div>
        </th>
      )}
      <th>{t('text_project_name')}</th>
      <th>{t('text_last_update_date')}</th>
      <th>{t('text_first_creation_date')}</th>
      {showOptions && <th></th>}
    </tr>
  );
}

type TableItemProps = Project & {
  randomIdx: number;
  checkList: string[];
  showCheckbox: boolean;
  showOptions: boolean;
  onChange: (checked: boolean, projectId: string) => void;
  onReload: () => void;
};

function TableItem({
  projectId,
  projectName,
  produceEndFlag,
  registDate,
  updateDate,
  randomIdx,
  checkList,
  showCheckbox,
  showOptions,
  onChange,
  onReload,
}: TableItemProps) {
  const history = useHistory();
  const { t } = useTranslation();
  const { handleChangePaneStatus } = useProjectPane();
  const {
    handleChangeRegisterStep,
    handleSetConnectedMetaIds,
    handleSetProjectProduceStep,
    handleSetProjectInfo,
    handleSetRegisterInitialState,
  } = useProjectRegister();
  const { handleSetSpaceInfo } = useSpace();
  const { handleSetProject } = useControlProject();
  const { handleRouteEdit } = useProjectEdit();

  const handleFetchProject = async () => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      if (!produceEndFlag) {
        handleSetRegisterInitialState();
        const produceStep = projectDetail.produceStep;
        const step = produceStep.step;

        handleSetProjectInfo({
          projectId: projectDetail.projectId,
          projectName: projectDetail.projectName,
          note: projectDetail.note,
          metaIds: projectDetail.buildings.map(
            (connectSpace) => connectSpace.metaId
          ),
        });

        handleSetProjectProduceStep(projectDetail.produceStep);
        handleSetConnectedMetaIds(
          projectDetail.buildings.map((space) => space.metaId)
        );

        if (step) {
          handleChangeRegisterStep(step);

          if (step === 4 || step === 5) {
            const metaId = produceStep.metaId;

            if (metaId) {
              const spaceMetaInfo = await fetchSpaceInfo(metaId);

              if (spaceMetaInfo.spaceId) {
                handleSetSpaceInfo({
                  id: metaId,
                  location: {
                    lng: spaceMetaInfo.lng,
                    lat: spaceMetaInfo.lat,
                  },
                });
              } else {
                await putProjectStepData(projectId, {
                  produceStepData: JSON.stringify({
                    metaId: '',
                    step: 2,
                  }),
                });
                handleChangeRegisterStep(2);
              }
            }
          }

          handleChangePaneStatus(PANE_STATUS_REGISTER);
          history.push('/project');
        }
        return;
      }

      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((space) => {
        const projectFloors: Floors[] = [];
        space.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: space.mappingId,
          id: space.metaId,
          name: space.metaName,
          longitude: space.lng,
          latitude: space.lat,
          floorsList: projectFloors,
          registDate: space.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.push('/control');
    }
  };

  const handleDeleteProject = async () => {
    const result = await deleteProject(projectId);
    if (result) {
      onReload();
    }
  };

  const handleSelectOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        handleRouteEdit(projectId);
        history.push('/project');
        break;
      case FUNCTION_DELETE:
        handleDeleteProject();
        break;
    }
  };

  return (
    <tr>
      {showCheckbox && (
        <td>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input js-check-selected-row"
              id={`check_${projectId}`}
              checked={checkList.includes(projectId)}
              onChange={(e) => onChange(e.target.checked, projectId)}
            />
            <label
              className="custom-control-label"
              htmlFor={`check_${projectId}`}
            >
              <span className="text-hide">Check</span>
            </label>
          </div>
        </td>
      )}
      <td>
        <div
          className="flex d-flex align-items-center mr-16pt"
          onClick={handleFetchProject}
        >
          <a className="avatar mr-12pt">
            <img
              src={`/static/images/slider0${randomIdx}.jpg`}
              className="avatar-img rounded"
            />
          </a>
          <div className="flex list-els">
            <a className="card-title">{projectName}</a>
            <div className="card-subtitle text-50">
              {produceEndFlag && <DateBadge date={registDate} />}
              {!produceEndFlag && (
                <>
                  <Badge variant={'secondary'}>
                    {t('text_project_editing_status')}
                  </Badge>
                  {moment(registDate).format('YYYY.MM.DD')}
                </>
              )}
            </div>
          </div>
        </div>
      </td>
      <td>{CommonUtils.convertDateFormat(updateDate, 'YYYY.MM.DD')}</td>
      <td>{CommonUtils.convertDateFormat(registDate, 'YYYY.MM.DD')}</td>
      {showOptions && (
        <td className="text-right">
          <ProjectListDropdown
            onSelect={handleSelectOptions}
            showEdit={produceEndFlag}
          />
        </td>
      )}
    </tr>
  );
}

export default ProjectListTable;
