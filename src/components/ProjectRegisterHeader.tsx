import React from 'react';
import { useProjectPane, useProjectRegister } from '@/modules/project/hook';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

function ProjectRegisterHeader() {
  const { t } = useTranslation();
  const { registerStep } = useProjectRegister();

  let text = '';
  if (registerStep === 1) {
    text = t('text_information_input');
  } else if (registerStep === 2) {
    text = t('text_space_connect_to_project');
  } else if (registerStep === 6) {
    text = t('text_selection_solution');
  }

  return (
    <>
      <div className="container-fluid d-flex align-items-center py-4">
        <div className="flex d-flex">
          <div className="mr-24pt">
            <h3 className="mb-0">{text}</h3>
            {/*<p className="text-70 mb-0">{t('msg_enter_description')}</p>*/}
          </div>
        </div>
      </div>
    </>
  );
}

export default ProjectRegisterHeader;
