import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Collapse } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import RightPhysicalDistributionStockHistoryAccordion from '@/components/RightPhysicalDistributionStockHistoryAccordion';
import { usePhysicalDistributionStatus } from '@/modules/physical_distribution/hook';
import { fetchMessages } from '@/api/message';
import { useControlProject, useControlSpace } from '@/modules/map/hook';
import moment from 'moment';
import { MessageHistory } from '@/modules/accident/types';
import { fetchTemperatureAndHumidityStatuses } from '@/api/assets';
import {
  TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER,
  TEMPERATURE_AND_HUMIDITY_SIGNAL_GOOD,
  TEMPERATURE_AND_HUMIDITY_SIGNAL_NO_SIGNAL,
  TEMPERATURE_AND_HUMIDITY_SIGNAL_ORDINARY,
  TemperatureAndHumidityStatus,
} from '@/modules/assets/types';
import { ASSETS_INTERVAL } from '@/utils/constants/common';

function RightPhysicalDistributionStatistics() {
  const perfectScrollbarRef = useRef<PerfectScrollbar>(null);

  useEffect(() => {
    const perfectScrollbar = perfectScrollbarRef.current;
    if (perfectScrollbar) {
      const container = (perfectScrollbar as any)._container;
      container.scrollTop = 0;
    }
  }, []);

  return (
    <>
      <div className="accordion js-accordion accordion--boxed list-group-flush">
        <RightPhysicalDistributionStockHistoryAccordion />
        <WarehouseMonitoringTable />
        <NoticeTable />
      </div>
    </>
  );
}

let monitoringTimer: ReturnType<typeof setInterval> | null;
function WarehouseMonitoringTable() {
  const { t } = useTranslation();
  const { space } = useControlSpace();
  const [isOpen, setOpen] = useState(true);
  const [load, setLoad] = useState(false);
  const [
    temperatureAndHumidityStatusList,
    setTemperatureAndHumidityStatusList,
  ] = useState<TemperatureAndHumidityStatus[]>([]);

  useEffect(() => {
    handleFetchTemperatureAndHumidityStatusList();
    monitoringTimer = setInterval(async () => {
      handleFetchTemperatureAndHumidityStatusList();
    }, ASSETS_INTERVAL);

    return () => {
      if (monitoringTimer) {
        clearInterval(monitoringTimer);
        monitoringTimer = null;
      }
    };
  }, []);

  const handleFetchTemperatureAndHumidityStatusList = async () => {
    setLoad(false);
    const data = await fetchTemperatureAndHumidityStatuses(space.floorsMapId);
    setTemperatureAndHumidityStatusList(data);
    setLoad(true);
  };

  return (
    <div
      className={classNames('accordion__item', {
        open: isOpen,
      })}
    >
      <a
        className="accordion__toggle collapsed"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="flex d-flex align-items-center">
          {t('text_monitoring_warehouse_environment')}
        </span>
        <span className="accordion__toggle-icon material-icons">
          keyboard_arrow_down
        </span>
      </a>
      <Collapse in={isOpen}>
        <div
          className={classNames('accordion__menu ac-item02', {
            show: isOpen,
          })}
        >
          <div className="inner">
            {!load && <></>}
            {load && temperatureAndHumidityStatusList.length === 0 && (
              <div
                className="empty-state"
                style={{
                  padding: '2rem 0',
                }}
              >
                <em className="empty-txt">{t('msg_assets_empty')}</em>
              </div>
            )}
            {load &&
              temperatureAndHumidityStatusList.map(
                ({ asset_name, signal }, i) => (
                  <div key={i} className="accordion__menu-link">
                    <a className="d-flex align-items-center w-100">
                      <p className="d-flex m-0">{asset_name}</p>
                      <span
                        className={classNames('d-flex badge ml-auto', {
                          'badge-accent':
                            signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER ||
                            signal ===
                              TEMPERATURE_AND_HUMIDITY_SIGNAL_NO_SIGNAL,
                          'badge-success':
                            signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_GOOD ||
                            signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_ORDINARY,
                        })}
                      >
                        {signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_GOOD &&
                          t('text_normal')}
                        {signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_ORDINARY &&
                          t('text_normal')}
                        {signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER &&
                          t('text_abnormality')}
                        {signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_NO_SIGNAL &&
                          t('text_no_signal')}
                      </span>
                    </a>
                  </div>
                )
              )}
          </div>
        </div>
      </Collapse>
    </div>
  );
}

function NoticeTable() {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(true);
  const [load, setLoad] = useState(false);
  const { project } = useControlProject();
  const { reloadFlag } = usePhysicalDistributionStatus();
  const [messageHistoryList, setMessageHistoryList] = useState<
    MessageHistory[]
  >([]);

  useEffect(() => {
    handleFetchNoticeList();
  }, [reloadFlag]);

  const handleFetchNoticeList = async () => {
    setLoad(false);
    let data = await fetchMessages({
      projectId: project.id,
      parentType: 'logistics',
      searchStartDates: moment().add(-2, 'days').format('YYYY-MM-DD'),
      searchEndDates: moment().format('YYYY-MM-DD'),
    });
    if (data.length > 3) {
      data = data.slice(0, 3);
    }
    setMessageHistoryList(data);
    setLoad(true);
  };

  return (
    <div
      className={classNames('accordion__item', {
        open: isOpen,
      })}
    >
      <a
        className="accordion__toggle collapsed"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="flex d-flex align-items-center">
          {t('text_notice')}
        </span>
        <span className="accordion__toggle-icon material-icons">
          keyboard_arrow_down
        </span>
      </a>
      <Collapse in={isOpen}>
        <div
          className={classNames('accordion__menu more-cover ac-item01', {
            show: isOpen,
          })}
        >
          {!load && <></>}
          {load && messageHistoryList.length === 0 && (
            <div
              className="empty-state"
              style={{
                padding: '2rem 0',
              }}
            >
              <em className="empty-txt">{t('msg_notice_not_exist')}</em>
            </div>
          )}
          {load &&
            messageHistoryList.map(
              ({ messageId, messageContent, registDate }) => (
                <div key={messageId} className="accordion__menu-link state-up">
                  <a className="d-flex align-items-center flex-row w-100">
                    <span className="material-icons-outlined text-accent mr-1 font-size-16pt">
                      info
                    </span>
                    <p className="d-flex m-0 text-accent pr-4">
                      {messageContent}
                    </p>
                    <span className="d-flex text-50 ml-auto">
                      {moment(registDate).format('YYYY-MM-DD')}
                    </span>
                  </a>
                </div>
              )
            )}
        </div>
      </Collapse>
    </div>
  );
}

export default RightPhysicalDistributionStatistics;
