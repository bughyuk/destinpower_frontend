import React, { useEffect, useRef, useState } from 'react';
import { useHomeMenu } from '@/modules/home/hook';
import {
  Menu,
  MENU_LOGISTICS_MANAGEMENT,
  MenuIdx,
  SubMenu,
} from '@/modules/home/types';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { useOutsideClick } from '@/modules/common';

function HomeLocationbar() {
  const { t } = useTranslation();
  const { menus, activeMenuIdx, handleSetMenu } = useHomeMenu();
  const [activeMenu, setActiveMenu] = useState<Menu | null>(null);
  const [activeSubMenu, setActiveSubMenu] = useState<SubMenu | null>(null);
  const [showMenu, setShowMenu] = useState(false);
  const [showSubMenu, setShowSubMenu] = useState(false);
  const menuFilterCellRef = useRef<HTMLDivElement>(null);
  const subMenuFilterCellRef = useRef<HTMLDivElement>(null);

  useOutsideClick(menuFilterCellRef, () => {
    setShowMenu(false);
  });

  useOutsideClick(subMenuFilterCellRef, () => {
    setShowSubMenu(false);
  });

  useEffect(() => {
    setActiveMenu(menus[0]);
  }, [menus]);

  useEffect(() => {
    const findMenu = menus.find((menu) => menu.menuIdx === activeMenuIdx);
    if (findMenu) {
      setActiveMenu(findMenu);
    } else {
      menus.some((menu) => {
        const findSubMenu = menu.subMenus.find(
          (subMenu) => subMenu.menuIdx === activeMenuIdx
        );
        if (findSubMenu) {
          setActiveMenu(menu);
          setActiveSubMenu(findSubMenu);
          return true;
        }
      });
    }
  }, [menus, activeMenuIdx]);

  const handleClickMenu = (menu: Menu) => {
    if (menu.subMenus.length) {
      const subMenu = menu.subMenus[0];
      handleSetMenu(subMenu.menuIdx);
    } else {
      handleSetMenu(menu.menuIdx);
    }

    setShowMenu(false);
  };

  const handleClickSubMenu = (menuIdx: MenuIdx) => {
    handleSetMenu(menuIdx);
    setShowSubMenu(false);
  };

  if (activeMenuIdx === -1 || activeMenuIdx === MENU_LOGISTICS_MANAGEMENT) {
    return <></>;
  }

  return (
    <div className="current-location">
      <a
        className="btn-location-home"
        onClick={() => handleSetMenu(MENU_LOGISTICS_MANAGEMENT)}
      >
        <span className="material-icons">home</span>
      </a>
      {activeMenu && (
        <>
          <div className="filter-cell location-depth" ref={menuFilterCellRef}>
            <>
              <a
                className={classNames('filter-dropdown-toggle location-list', {
                  active: showMenu,
                })}
                onClick={() => setShowMenu(!showMenu)}
              >
                <strong>{t(activeMenu.titleKey)}</strong>
              </a>
              <div
                className="filter-item-dropdown"
                style={{
                  display: showMenu ? 'block' : 'none',
                }}
              >
                {menus.map((menu) => (
                  <a
                    key={menu.menuIdx}
                    className={classNames('dropdown-item', {
                      active: menu.menuIdx === activeMenu.menuIdx,
                    })}
                    onClick={() => handleClickMenu(menu)}
                  >
                    {t(menu.titleKey)}
                  </a>
                ))}
              </div>
            </>
          </div>
          {activeMenu.subMenus.length > 0 && (
            <div
              className="filter-cell location-depth"
              ref={subMenuFilterCellRef}
            >
              {activeSubMenu && (
                <>
                  <a
                    className={classNames(
                      'filter-dropdown-toggle location-list',
                      {
                        active: showSubMenu,
                      }
                    )}
                    onClick={() => setShowSubMenu(!showSubMenu)}
                  >
                    <strong>{t(activeSubMenu.titleKey)}</strong>
                  </a>
                  <div
                    className="filter-item-dropdown"
                    style={{
                      display: showSubMenu ? 'block' : 'none',
                    }}
                  >
                    {activeMenu.subMenus.map((subMenu) => (
                      <a
                        key={subMenu.menuIdx}
                        className={classNames('dropdown-item', {
                          active: subMenu.menuIdx === activeSubMenu.menuIdx,
                        })}
                        onClick={() => handleClickSubMenu(subMenu.menuIdx)}
                      >
                        {t(subMenu.titleKey)}
                      </a>
                    ))}
                  </div>
                </>
              )}
            </div>
          )}
        </>
      )}
    </div>
  );
}

export default HomeLocationbar;
