import React, { useEffect } from 'react';
import { useSpace, useSpacePane } from '@/modules/space/hook';
import { useUser } from '@/modules/user/hook';
import { PANE_STATUS_SPACE_FILE_REGISTER } from '@/utils/constants/common';
import { useHistory } from 'react-router-dom';
import { ValidUtils } from '@/utils';
import { useLogout } from '@/modules/access/logout/hook';
import { Config } from '@/config';

type IframeMessage = {
  type: 'back' | 'next' | 'logout' | 'home';
};

function SpaceIndoorEditContainer() {
  const { user } = useUser();
  const { info } = useSpace();
  const { handleChangePaneStatus } = useSpacePane();
  const history = useHistory();
  const { handleLogout, logoutStatusCode } = useLogout();

  useEffect(() => {
    if (logoutStatusCode) {
      history.replace('/signin');
    }
  }, [logoutStatusCode]);

  const statusName = ValidUtils.validateHangul(user?.userName || '')
    ? user?.userName.length > 0
      ? user?.userName.substring(0, 1)
      : ''
    : user?.userName.length > 1
    ? user?.userName.substring(0, 1)
    : '';

  const lang = localStorage.getItem('language') || 'ko';

  const handleCommunicationMessage = (e: MessageEvent<IframeMessage>) => {
    e.stopPropagation();
    if (e.origin.indexOf(Config.map_server.indoor_edit_origin) > 0) {
      const data = e.data;
      if (data) {
        switch (data.type) {
          case 'back':
            handleChangePaneStatus(PANE_STATUS_SPACE_FILE_REGISTER);
            break;
          case 'next':
            history.replace('/home');
            break;
          case 'logout':
            handleLogout();
            break;
          case 'home':
            history.replace('/home');
        }
      }
    }
  };

  useEffect(() => {
    window.removeEventListener('message', handleCommunicationMessage, true);
    window.addEventListener('message', handleCommunicationMessage, {
      once: true,
    });

    return () =>
      window.removeEventListener('message', handleCommunicationMessage);
  }, []);

  return (
    <>
      <iframe
        style={{
          height: '100vh',
        }}
        frameBorder={0}
        src={`${Config.map_server.indoor_edit_uri}/drawMap.jsp?userid=${user.userId}&metaid=${info.space.id}&lang=${lang}&surname=${statusName}&div=0`}
      />
    </>
  );
}

export default SpaceIndoorEditContainer;
