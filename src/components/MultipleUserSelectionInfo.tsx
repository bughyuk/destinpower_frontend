import React, { useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

function MultipleUserSelectionInfo() {
  const { t } = useTranslation();

  const rollingBoxElementRef = useRef<HTMLUListElement>(null);
  const firstElementRef = useRef<HTMLLIElement>(null);
  const secondElementRef = useRef<HTMLLIElement>(null);
  const thirdElementRef = useRef<HTMLLIElement>(null);

  useEffect(() => {
    let move = 2;
    const rollingBoxElement = rollingBoxElementRef.current;
    const firstElement = firstElementRef.current;
    const secondElement = secondElementRef.current;
    const thirdElement = thirdElementRef.current;

    let modifierKey = 'Ctrl';

    if (navigator.appVersion.indexOf('Mac') > -1) {
      modifierKey = 'Command';
    }

    const rollingData: string[] = [
      `<strong>${modifierKey}</strong>${t('msg_multiple_selection_user')}`,
      `<strong>Shift</strong>${t('msg_multiple_cancel_user')}`,
    ];

    let dataCnt = 1;
    let listCnt = 1;
    if (rollingBoxElement && firstElement && secondElement && thirdElement) {
      firstElement.innerHTML = rollingData[0];

      setInterval(() => {
        if (move === 2) {
          firstElement.classList.remove('card-sliding');
          firstElement.classList.add('card-sliding-after');

          secondElement.classList.remove('card-sliding-after');
          secondElement.classList.add('card-sliding');

          thirdElement.classList.remove('card-sliding-after');
          thirdElement.classList.remove('card-sliding');

          move = 0;
        } else if (move == 1) {
          firstElement.classList.remove('card-sliding-after');
          firstElement.classList.add('card-sliding');

          secondElement.classList.remove('card-sliding-after');
          secondElement.classList.remove('card-sliding');

          thirdElement.classList.remove('card-sliding');
          thirdElement.classList.add('card-sliding-after');

          move = 2;
        } else if (move == 0) {
          firstElement.classList.remove('card-sliding-after');
          firstElement.classList.remove('card-sliding');

          secondElement.classList.remove('card-sliding');
          secondElement.classList.add('card-sliding-after');

          thirdElement.classList.remove('card-sliding-after');
          thirdElement.classList.add('card-sliding');

          move = 1;
        }

        const child = rollingBoxElement.children[listCnt];

        if (dataCnt < rollingData.length - 1) {
          child.innerHTML = rollingData[dataCnt];
          dataCnt++;
        } else if (dataCnt == rollingData.length - 1) {
          child.innerHTML = rollingData[dataCnt];
          dataCnt = 0;
        }

        if (listCnt < 2) {
          listCnt++;
        } else if (listCnt == 2) {
          listCnt = 0;
        }
      }, 2000);
    }
  }, []);

  return (
    <div className="inner rolling-box">
      <ul ref={rollingBoxElementRef}>
        <li className="card-sliding" ref={firstElementRef}></li>
        <li ref={secondElementRef}></li>
        <li ref={thirdElementRef}></li>
      </ul>
    </div>
  );
}

export default MultipleUserSelectionInfo;
