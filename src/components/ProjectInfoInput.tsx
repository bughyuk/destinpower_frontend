import { useTranslation } from 'react-i18next';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import React from 'react';

type ProjectInfoInput = {
  projectName: string;
  note: string;
  onChangeProjectName: (projectName: string) => void;
  onChangeNote: (note: string) => void;
};

function ProjectInfoInput({
  projectName,
  note,
  onChangeProjectName,
  onChangeNote,
}: ProjectInfoInput) {
  const { t } = useTranslation();
  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel
          textKey={'text_project_name'}
          essential={true}
          htmlFor={'projectName'}
        />
        <input
          type="text"
          className="form-line"
          id="projectName"
          placeholder={t('place_holder_project_name')}
          value={projectName}
          onChange={(e) => {
            onChangeProjectName(e.target.value);
          }}
          autoComplete={'off'}
        />
      </FormGroup>
      <div className="form-group mb-4">
        <FormLabel textKey={'text_content'} essential={true} htmlFor={'note'} />
        <input
          type="text"
          className="form-line"
          id="note"
          placeholder={t('place_holder_content')}
          value={note}
          onChange={(e) => {
            onChangeNote(e.target.value);
          }}
          autoComplete={'off'}
        />
      </div>
    </div>
  );
}

export default ProjectInfoInput;
