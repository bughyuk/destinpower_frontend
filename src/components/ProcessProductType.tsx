import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
// import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import { ProductType } from '@/modules/physical_distribution/types';
// import ProcessProductLineRegister from '@/components/ProcessProductLineRegister';
import ProcessProductTypeList from '@/components/ProcessProductTypeList';
import ProcessProductTypeRegister from '@/components/ProcessProductTypeRegister';

type ProcessProductTypeContentProps = {
  title?: string;
  onClickBack?: () => void;
};

function ProcessProductType({
  title,
  onClickBack,
}: ProcessProductTypeContentProps) {
  const { t } = useTranslation();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_LIST);
  const [productType, setProductType] = useState<ProductType>(
    {} as ProductType
  );

  let content: ReactElement = <></>;
  if (status === PANE_STATUS_LIST) {
    content = (
      <ProcessProductTypeList
        onChangeProductType={setProductType}
        onChangeStatus={setStatus}
      />
    );
  } else if (status === PANE_STATUS_REGISTER) {
    content = (
      <ProcessProductTypeRegister
        productType={undefined}
        onChangeStatus={setStatus}
      />
    );
  } else if (status === PANE_STATUS_EDIT) {
    content = (
      <ProcessProductTypeRegister
        productType={productType}
        onChangeStatus={setStatus}
      />
    );
  }

  let displayTitle = title;
  if (status === PANE_STATUS_REGISTER) {
    displayTitle = '제품타입 추가';
  } else if (status === PANE_STATUS_EDIT) {
    displayTitle = '제품타입 수정';
  }

  const handleClickBack = () => {
    if (status === PANE_STATUS_LIST) {
      onClickBack?.call(null);
    } else {
      setStatus(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={handleClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{displayTitle}</h3>
          </div>
          {status === PANE_STATUS_LIST && (
            <a
              className="btn btn-outline-dark ml-auto"
              onClick={() => setStatus(PANE_STATUS_REGISTER)}
            >
              추가
            </a>
          )}
        </div>
      </div>
      {content}
    </>
  );
}

export default ProcessProductType;
