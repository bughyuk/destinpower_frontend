import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ValidUtils } from '@/utils';
import { fetchUserId, postAuthNumberMail } from '@/api/user';
import classNames from 'classnames';

type SignUpEmailAuthenticationProps = {
  onChangeSendMail: () => void;
  onChangeSignUpEmail: (email: string) => void;
};

function SignUpEmailAuthentication({
  onChangeSendMail,
  onChangeSignUpEmail,
}: SignUpEmailAuthenticationProps) {
  const { i18n, t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState('');
  const [feedbackMessage, setFeedbackMessage] = useState('');
  const [duplicateCheck, setDuplicateCheck] = useState<{
    checkedEmail: boolean;
    emailPossible: boolean;
  }>({
    checkedEmail: false,
    emailPossible: false,
  });

  useEffect(() => {
    setDuplicateCheck({
      checkedEmail: false,
      emailPossible: false,
    });
  }, [email]);

  const handleClickDuplicateCheck = async () => {
    if (!email) {
      setFeedbackMessage(t('msg_valid_enter_email_value'));
    } else {
      const isEmail =
        email.length >= 5 &&
        email.length <= 45 &&
        ValidUtils.validateEmail(email);
      if (!isEmail) {
        setFeedbackMessage(t('msg_valid_format_email'));
      } else {
        setFeedbackMessage('');
        const duplicateUserId = await fetchUserId(email);
        const emailPossible = duplicateUserId.result;

        if (duplicateUserId.detail) {
          setFeedbackMessage(t('msg_valid_invite_email'));
        } else if (!emailPossible) {
          setFeedbackMessage(t('msg_valid_available_email'));
        } else {
          setFeedbackMessage(t('msg_valid_duplicate_email'));
        }

        setDuplicateCheck({
          checkedEmail: true,
          emailPossible: !emailPossible,
        });
      }
    }
  };

  const handleClickSend = async () => {
    if (!loading) {
      if (!email) {
        setFeedbackMessage(t('msg_valid_enter_email_value'));
        return;
      }

      const isEmail =
        email.length >= 5 &&
        email.length <= 45 &&
        ValidUtils.validateEmail(email);
      if (!isEmail) {
        setFeedbackMessage(t('msg_valid_format_email'));
        return;
      }

      if (!duplicateCheck.checkedEmail) {
        setFeedbackMessage(t('msg_check_for_duplicate'));
        return;
      }

      if (!duplicateCheck.emailPossible) {
        setFeedbackMessage(t('msg_try_another_email'));
        return;
      }

      setFeedbackMessage('');
      setLoading(true);
      const result = await postAuthNumberMail(
        email,
        i18n.language.length > 2 ? i18n.language.slice(0, 2) : i18n.language
      );

      if (result) {
        onChangeSendMail();
        onChangeSignUpEmail(email);
      }
    }
  };

  return (
    <>
      <div className="title-group mb-5">
        <h3 className="m-0">{t('text_email_authentication')}</h3>
      </div>
      <div className="form-group mb-5 over-txt">
        <div className="over-txt">
          <label className="form-label text-50 essential" htmlFor="email">
            {t('text_email_authentication')}
          </label>
          <input
            id="email"
            type="email"
            className="form-line pr-6"
            placeholder={t('msg_valid_empty_email')}
            autoComplete={'off'}
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
          <a
            className="btn btn-outline-secondary btn-sm btn-rounded"
            onClick={handleClickDuplicateCheck}
          >
            {t('text_duplicate_check')}
          </a>
        </div>
        {feedbackMessage && (
          <div className="invalid-feedback">{feedbackMessage}</div>
        )}
      </div>
      <div className="form-group text-center mb-32pt">
        <button
          className={classNames('btn btn-block btn-lg btn-accent', {
            'is-loading': loading,
          })}
          type="button"
          onClick={handleClickSend}
        >
          {t('text_authentication_number_transfer')}
        </button>
      </div>
    </>
  );
}

export default SignUpEmailAuthentication;
