import React from 'react';
import { useHomeMenu } from '@/modules/home/hook';
import AccountProfile from '@/components/AccountProfile';
import {
  MENU_ACCOUNT_PROFILE,
  MENU_ACCOUNT_SECURITY,
} from '@/modules/home/types';
import AccountSecurity from '@/components/AccountSecurity';

function HomeAccount() {
  const { activeMenuIdx } = useHomeMenu();

  return (
    <>
      {activeMenuIdx === MENU_ACCOUNT_PROFILE && <AccountProfile />}
      {activeMenuIdx === MENU_ACCOUNT_SECURITY && <AccountSecurity />}
    </>
  );
}

export default HomeAccount;
