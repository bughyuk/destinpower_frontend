import React, { useEffect, useRef, useState } from 'react';
import ListDropdown from '@/components/ListDropdown';
import DateBadge from '@/components/DateBadge';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import { useSpace, useSpacePane, useOpenLayers } from '@/modules/space/hook';
import {
  FUNCTION_DELETE,
  PANE_STATUS_SPACE_FILE_REGISTER,
} from '@/utils/constants/common';
import {
  fetchFloors,
  postDeleteFloorPlan,
  ResponseFloorsInfo,
} from '@/api/space';
import { useUser } from '@/modules/user/hook';
import classNames from 'classnames';
import { Config } from '@/config';
import { CommonUtils } from '@/utils';

function SpaceFloorsDetail() {
  const dragula = useRef<HTMLDivElement>(null);
  const [floorsList, setFloorsList] = useState<ResponseFloorsInfo[]>([]);
  const { handleAllClear } = useOpenLayers();
  const [selectedFloors, setSelectFloors] = useState('');
  const { user } = useUser();
  const { info } = useSpace();
  const { t } = useTranslation();

  useEffect(() => {
    /*
    if (dragula.current) {
      const drake = reactDragula([dragula.current], {});
      drake.on('drop', (el, target) => {
        // TODO: API;
      });
    }
     */

    handleFetchFloorsList();
  }, []);

  const handleFetchFloorsList = async () => {
    setFloorsList([]);
    const data = await fetchFloors(user.userId, info.space.id);
    if (data) {
      setFloorsList(data);
    }
  };

  const handleReloadFloorsList = (floorsId: string) => {
    if (selectedFloors === floorsId) {
      handleAllClear();
    }

    setFloorsList(floorsList.filter((floors) => floors.map_id !== floorsId));
  };

  return (
    <>
      {floorsList.length === 0 && (
        <em
          className="none-list"
          style={{
            borderTop: 'none',
            borderBottom: 'none',
          }}
        >
          {t('msg_floor_plan_suggest_to_add')}
        </em>
      )}
      {floorsList.length > 0 && (
        <div className="list-group block-list dragula-list mb-4" ref={dragula}>
          {floorsList.map((floors) => (
            <FloorsItem
              key={floors.map_id}
              {...floors}
              selectedFloors={selectedFloors}
              onSelectFloors={setSelectFloors}
              onReload={handleReloadFloorsList}
            />
          ))}
        </div>
      )}

      <RegisterButton />
    </>
  );
}

type FloorsItemProps = ResponseFloorsInfo & {
  selectedFloors: string;
  onSelectFloors: (floorsId: string) => void;
  onReload: (floorsId: string) => void;
};

function FloorsItem({
  map_id,
  map_name,
  map_floor,
  filename,
  rotation,
  scalex,
  scaley,
  cx,
  cy,
  selectedFloors,
  onSelectFloors,
  onReload,
}: FloorsItemProps) {
  const { t } = useTranslation();
  const { user } = useUser();
  const { geoImage } = useOpenLayers();

  const handleSelectFloors = () => {
    onSelectFloors(map_id);

    if (filename) {
      geoImage.draw(
        {
          url: `${Config.space_api.uri}/bprint/${filename}`,
          imageCenter: [Number(cx), Number(cy)],
          imageScale: [Number(scalex), Number(scaley)],
          imageRotate: Number(rotation),
        },
        false
      );
    } else {
      geoImage.remove();
    }
  };

  const handleDeleteFloorsPlan = async () => {
    const result = await postDeleteFloorPlan({
      userid: user.userId,
      del: true,
      mapid: map_id,
    });

    if (result) {
      onReload(map_id);
    }
  };

  const handleOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_DELETE:
        handleDeleteFloorsPlan();
        break;
    }
  };

  return (
    <div
      className={classNames('list-group-item d-flex', {
        active: selectedFloors === map_id,
      })}
      onClick={handleSelectFloors}
    >
      <div className="flex d-flex align-items-center mr-16pt">
        <div className="avatar avatar-sm mr-2">
          <span className="avatar-title rounded-circle">
            {CommonUtils.convertFloorsFormat(map_floor)}
          </span>
        </div>
        <div className="flex">
          <a className="card-title">{map_name}</a>
          <dl className="space-id">
            <dt>{t('text_floors_id')} :</dt>
            <dd>{map_id}</dd>
          </dl>
        </div>
      </div>
      <ListDropdown onSelect={handleOptions} showEdit={false} />
    </div>
  );
}

function RegisterButton() {
  const { t } = useTranslation();
  const { handleAllClear } = useOpenLayers();
  const { handleChangePaneStatus } = useSpacePane();

  const handleRouteSpaceRegister = () => {
    handleAllClear();
    handleChangePaneStatus(PANE_STATUS_SPACE_FILE_REGISTER);
  };

  return (
    <div className="d-flex align-items-center justify-content-center">
      <a className="btn-row-add" onClick={handleRouteSpaceRegister}>
        <MaterialIcon name={'dynamic_feed'} /> {t('text_floor_plan_management')}
      </a>
    </div>
  );
}

export default SpaceFloorsDetail;
