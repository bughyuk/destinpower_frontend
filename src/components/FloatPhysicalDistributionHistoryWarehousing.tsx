import React from 'react';
import { useTranslation } from 'react-i18next';
import { FloatPhysicalDistributionHistoryCommonProps } from '@/components/FloatPhysicalDistributionHistory';
import moment from 'moment';
import { CommonUtils } from '@/utils';
import InputNumber from '@/components/InputNumber';
import {
  DistributionHistory,
  Warehouse,
} from '@/modules/physical_distribution/types';
import PhysicalDistributionFlatpickr from '@/components/PhysicalDistributionFlatpickr';
import classNames from 'classnames';

type FloatPhysicalDistributionHistoryWarehousingProps = FloatPhysicalDistributionHistoryCommonProps & {
  warehouseList: Warehouse[];
  editMode?: boolean;
  onChangeDistributionHistory?: (
    distributionHistory: DistributionHistory
  ) => void;
};

function FloatPhysicalDistributionHistoryWarehousing({
  distributionHistory,
  editMode = false,
  warehouseList,
  onChangeDistributionHistory,
}: FloatPhysicalDistributionHistoryWarehousingProps) {
  const { t } = useTranslation();

  return (
    <>
      <div className={classNames('page-separator mt-5')}>
        <div className="page-separator__text">
          {t('text_warehousing_information')}
        </div>
      </div>
      <table className="table mb-4 thead-bg-light">
        <thead>
          <tr>
            <th>{t('text_warehousing_date')}</th>
            <th>{t('text_warehouse')}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              {editMode && (
                <PhysicalDistributionFlatpickr
                  value={moment(distributionHistory.dueDate).format(
                    'YYYY-MM-DD'
                  )}
                  disabled={false}
                  onChange={(value) =>
                    onChangeDistributionHistory?.call(null, {
                      ...distributionHistory,
                      dueDate: value,
                    })
                  }
                />
              )}
              {!editMode && (
                <PhysicalDistributionFlatpickr
                  value={moment(distributionHistory.dueDate).format(
                    'YYYY-MM-DD'
                  )}
                  disabled={true}
                />
              )}
            </td>
            <td>
              <select
                className="form-control custom-select font-weight-bolder w-auto"
                value={distributionHistory.warehouseId}
                disabled={!editMode}
                onChange={(e) => {
                  onChangeDistributionHistory?.call(null, {
                    ...distributionHistory,
                    warehouseId: e.target.value,
                  });
                }}
              >
                {warehouseList.map(({ warehouseId, warehouseName }) => (
                  <option key={warehouseId} value={warehouseId}>
                    {warehouseName}
                  </option>
                ))}
              </select>
            </td>
          </tr>
        </tbody>
      </table>
      <div className="page-separator mt-5">
        <div className="page-separator__text">
          {t('text_product_information')}
        </div>
      </div>
      <table className="table mb-4 thead-bg-light">
        <colgroup>
          <col width="*" />
          <col width="20%" />
        </colgroup>
        <thead>
          <tr>
            <th>{t('text_product_information')}</th>
            <th>{t('text_box_quantity')}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div className="flex d-flex align-items-center">
                <a className="avatar mr-12pt blank-img">
                  <img
                    src={distributionHistory.imgUrl}
                    className="avatar-img rounded"
                  />
                </a>
                <div className="flex list-els">
                  <h6 className="m-0">{distributionHistory.productName}</h6>
                  <div className="card-subtitle text-50">
                    <small className="mr-2">
                      {distributionHistory.productSizeName}
                    </small>
                    <small className="mr-2">
                      {CommonUtils.getProductStandard(
                        distributionHistory.productWeight,
                        distributionHistory.boxQuantity
                      )}
                    </small>
                  </div>
                </div>
              </div>
            </td>
            <td>
              <div className="d-flex align-items-center">
                <span className="d-flex font-weight-bolder mr-1">
                  {distributionHistory.logisticsExecutionQuantity} /{' '}
                </span>
                <span className="d-flex align-items-center">
                  <InputNumber
                    className="form-control font-weight-bolder"
                    style={{
                      width: '6rem',
                    }}
                    value={distributionHistory.logisticsQuantity}
                    disabled={!editMode}
                    onChange={(value) =>
                      onChangeDistributionHistory?.call(null, {
                        ...distributionHistory,
                        logisticsQuantity: value,
                      })
                    }
                  />
                  <span className="ml-2 text-50">{t('text_box_en')}</span>
                </span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}

export default FloatPhysicalDistributionHistoryWarehousing;
