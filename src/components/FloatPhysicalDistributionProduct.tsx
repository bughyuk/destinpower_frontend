import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { usePhysicalDistributionProduct } from '@/modules/physical_distribution/hook';
import MaterialIcon from '@/components/MaterialIcon';
import FormLabel from '@/components/FormLabel';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import { useDropdown } from '@/modules/common';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
} from '@/utils/constants/common';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import { ProductDetail } from '@/modules/physical_distribution/types';
import { deleteProduct, fetchProduct } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import ImageGalleryModal from '@/components/ImageGalleryModal';
import ConfirmModal from '@/components/ConfirmModal';
import classNames from 'classnames';

function FloatPhysicalDistributionProduct() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const {
    paneStatus,
    selectedProduct,
    handleSetProduct,
    handleChangePaneStatus,
    handleChangeReloadFlag,
  } = usePhysicalDistributionProduct();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const [productDetail, setProductDetail] = useState<ProductDetail | null>(
    null
  );
  const [showImageGalleryModal, setShowImageGalleryModal] = useState(false);
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  useEffect(() => {
    if (paneStatus === PANE_STATUS_LIST && selectedProduct) {
      handleFetchProduct();
    } else {
      setProductDetail(null);
    }
  }, [paneStatus, selectedProduct]);

  const handleFetchProduct = async () => {
    const productId = selectedProduct?.productId;

    if (productId) {
      const result = await fetchProduct(project.id, productId);
      if (result) {
        setProductDetail(result);
      }
    }
  };

  const handleDeleteProduct = async () => {
    const productId = productDetail?.detail.productId;
    if (productId) {
      const result = await deleteProduct(project.id, productId);
      if (result) {
        handleSetProduct(null);
        handleChangeReloadFlag();
      }
    }
  };

  const handleSelectFunction = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        handleChangePaneStatus(PANE_STATUS_EDIT);
        break;
      case FUNCTION_DELETE:
        setShowConfirmModal(true);
        break;
    }
  };

  if (paneStatus !== PANE_STATUS_LIST) {
    return <></>;
  }

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_view_product_detail')}
          </h3>
        </div>
        {productDetail?.detail.productId && !productDetail?.detail.deleteFlag && (
          <ButtonGroup>
            <Dropdown onToggle={handleToggle} onSelect={handleSelectFunction}>
              <DropdownToggle
                as={'a'}
                className="btn btn-rounded btn-outline-dark"
              >
                {t('text_see_more_details')}
              </DropdownToggle>
              <DropdownMenu align={'right'} ref={dropdown}>
                <Dropdown.Item
                  className="text-primary"
                  eventKey={FUNCTION_EDIT}
                >
                  {t('text_edit')}
                </Dropdown.Item>
                <Dropdown.Item
                  className="text-danger"
                  eventKey={FUNCTION_DELETE}
                >
                  {t('text_delete')}
                </Dropdown.Item>
              </DropdownMenu>
            </Dropdown>
          </ButtonGroup>
        )}
      </div>
      <div className="contents-view">
        <div className="page-separator mt-5">
          <div className="page-separator__text">
            {t('text_product_information')}
          </div>
        </div>
        {!productDetail?.detail.productId && (
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>

            <em className="empty-txt m-0">
              {t('msg_selection_product_from_the_list_on_the_left')}
            </em>
          </div>
        )}
        {productDetail?.detail.productId && (
          <>
            <div className="form-group">
              <FormLabel textKey={'text_item'} className={'mb-0'} />
              <p className="form-text mb-0">
                {productDetail.detail.productCategoryName}
              </p>
            </div>
            <div className="form-group">
              <FormLabel textKey={'text_product_code'} className={'mb-0'} />
              <p className="form-text mb-0">
                {productDetail.detail.productCode || '-'}
              </p>
            </div>
            <div className="form-group">
              <FormLabel textKey={'text_product_title'} className={'mb-0'} />
              <p className="form-text mb-0">
                {productDetail.detail.productName}
              </p>
            </div>
            <div className="form-group">
              <FormLabel textKey={'text_size'} className={'mb-0'} />
              <p className="form-text mb-0">
                {productDetail.detail.productSizeName}
              </p>
            </div>
            <div className="form-group">
              <FormLabel textKey={'text_standard'} className={'mb-0'} />
              <p className="form-text mb-0">{`${productDetail.detail.productWeight}g x ${productDetail.detail.boxQuantity}`}</p>
            </div>
            <div className="form-group">
              <FormLabel
                textKey={'text_each_number_of_product'}
                className={'mb-0'}
              />
              <p className="form-text mb-0">
                {productDetail.detail.boxQuantity}
              </p>
            </div>
            <div className="form-group">
              <FormLabel
                textKey={'text_stock_minimum_quantity'}
                className={'mb-0'}
              />
              <p className="form-text mb-0">
                {productDetail.detail.minQuantity}
              </p>
            </div>
            {productDetail.detail.imgUrl && (
              <div className="form-group border-bottom-0">
                <div className="d-flex align-items-center">
                  <FormLabel textKey={'text_product_image'} />
                </div>
                <div className="d-flex align-items-center">
                  <a
                    className="btn-img"
                    onClick={() => setShowImageGalleryModal(true)}
                  >
                    <img
                      src={productDetail.detail.imgUrl}
                      style={{
                        maxWidth: '150px',
                      }}
                    />
                  </a>
                </div>
              </div>
            )}
            <div className="page-separator mt-5">
              <div className="page-separator__text">
                {t('text_stock_by_location')}
              </div>
            </div>
            <div className="form-group border-bottom-0">
              {productDetail.stock.length === 0 && (
                <div className="empty-state border-top-0">
                  <p className="empty-icon m-0">
                    <span className="material-icons-outlined">
                      error_outline
                    </span>
                  </p>
                  <em className="empty-txt m-0">{t('msg_stock_not_exist')}</em>
                </div>
              )}
              {productDetail.stock.map(
                ({ warehouseId, warehouseName, totalQuantity }) => (
                  <div key={warehouseId} className="card shadow-none mb-12pt">
                    <a className="card-body d-flex align-items-center">
                      <div className="icon-box">
                        <MaterialIcon name={'house_siding'} />
                      </div>
                      <div className="flex list-els">
                        <h4 className="card-title">{warehouseName}</h4>
                      </div>
                      <div className="pd-count">{totalQuantity}</div>
                    </a>
                  </div>
                )
              )}
            </div>
          </>
        )}
      </div>
      <ImageGalleryModal
        show={showImageGalleryModal}
        onHide={() => setShowImageGalleryModal(false)}
        imageUrl={productDetail?.detail.imgUrl || ''}
      />
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={handleDeleteProduct}
      >
        <div className="py-4">
          <div className="text-center">
            <h3>{t('msg_really_sure_want_to_delete')}</h3>
          </div>
          <div className="d-flex justify-content-center">
            <div className="media flex-nowrap align-items-center border-1 p-3">
              <span className="avatar avatar-sm mr-2 blank-img">
                <img
                  src={productDetail?.detail.imgUrl}
                  className={classNames('avatar-img', {
                    'rounded-circle': productDetail?.detail.imgUrl,
                  })}
                />
              </span>
              <div className="media-body">
                <strong className="text-dark">
                  {productDetail?.detail.productName}
                </strong>
                <div className="text-muted small">
                  {productDetail?.detail.productCode}
                </div>
              </div>
            </div>
          </div>
        </div>
      </ConfirmModal>
    </>
  );
}

export default FloatPhysicalDistributionProduct;
