import React, { useState } from 'react';
import SignUpComplete from '@/components/SignUpComplete';
import SignUpCompanyInfo from '@/components/SignUpCompanyInfo';
import SignUpMemberInvite from '@/components/SignUpMemberInvite';
import SignUpInviteComplete from '@/components/SignUpInviteComplete';

export type SignUpMemberProps = {
  onChangeMemberStep: (step: number) => void;
};

function SignUpMember() {
  const [memberStep, setMemberStep] = useState(1);

  return (
    <>
      {memberStep === 1 && <SignUpComplete />}
      {memberStep === 2 && (
        <SignUpCompanyInfo onChangeMemberStep={setMemberStep} />
      )}
      {memberStep === 3 && (
        <SignUpMemberInvite onChangeMemberStep={setMemberStep} />
      )}
      {memberStep === 4 && (
        <SignUpInviteComplete onChangeMemberStep={setMemberStep} />
      )}
    </>
  );
}

export default SignUpMember;
