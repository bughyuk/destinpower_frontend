import React, { useEffect, useRef, useState } from 'react';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import { useTranslation } from 'react-i18next';
import {
  usePhysicalDistributionItem,
  usePhysicalDistributionProduct,
  usePhysicalDistributionProductCode,
} from '@/modules/physical_distribution/hook';
import { postImage } from '@/api/common';
import MaterialIcon from '@/components/MaterialIcon';
import InvalidAlert from '@/components/InvalidAlert';
import FormRow from '@/components/FormRow';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER,
} from '@/utils/constants/common';
import InputNumber from '@/components/InputNumber';
import { fetchProduct, putProduct } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import { Product } from '@/modules/physical_distribution/types';
import classNames from 'classnames';
import PhysicalDistributionItemRegister from '@/components/PhysicalDistributionItemRegister';
import { CommonUtils } from '@/utils';

function PhysicalDistributionProductEdit() {
  const { t } = useTranslation();
  const fileInput = useRef<HTMLInputElement>(null);
  const { handleChangePaneStatus } = usePhysicalDistributionProduct();
  const {
    load: loadSizeCode,
    productCodeSizeList,
  } = usePhysicalDistributionProductCode();
  const {
    load: loadItemList,
    itemList,
    handleReloadItemList,
  } = usePhysicalDistributionItem();
  const [productCodePrefix, setProductCodePrefix] = useState('');
  const { project } = useControlProject();
  const { selectedProduct, paneStatus } = usePhysicalDistributionProduct();
  const [load, setLoad] = useState(false);
  const [inputs, setInputs] = useState<{
    productCode: string;
    productTitle: string;
    size: string;
    weight: number;
    eachNumberOfProduct: number;
    itemId: string;
    minimumQuantity: number;
    imgId: string;
  }>({
    productCode: '',
    productTitle: '',
    size: '1',
    weight: 0,
    eachNumberOfProduct: 0,
    itemId: '',
    minimumQuantity: 0,
    imgId: '',
  });
  const [product, setProduct] = useState<Product>({} as Product);
  const [originalImg, setOriginalImg] = useState<{
    imgId: string;
    imgUrl: string;
  }>({
    imgId: '',
    imgUrl: '',
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (selectedProduct) {
      handleFetchProduct();
    }
  }, [selectedProduct]);

  useEffect(() => {
    if (!CommonUtils.isEmptyObject(product)) {
      if (
        product?.productCategoryId &&
        inputs.itemId === product?.productCategoryId
      ) {
        setInputs({
          ...inputs,
          productCode: product?.productCode,
        });
        return;
      }

      if (productCodePrefix) {
        setInputs({
          ...inputs,
          productCode: `${productCodePrefix}`,
        });
      }
    }
  }, [productCodePrefix, inputs.itemId, product]);

  useEffect(() => {
    let productCode = product.productCode;
    let itemId = product.productCategoryId;

    if (itemList.length) {
      if (!product.productCategoryId) {
        const firstItem = itemList[0];
        productCode = firstItem.productCodePrefix;
        itemId = firstItem.productCategoryId;
      }
    } else {
      productCode = '';
    }

    setInputs({
      productCode,
      productTitle: product.productName,
      size: product.productSize,
      weight: product.productWeight,
      itemId,
      imgId: '',
      eachNumberOfProduct: product.boxQuantity,
      minimumQuantity: product.minQuantity,
    });
  }, [itemList, product]);

  const handleFetchProduct = async () => {
    setLoad(false);
    const productId = selectedProduct?.productId;
    if (productId) {
      const result = await fetchProduct(project.id, productId);
      if (result) {
        const detail = result.detail;
        setProduct(detail);
        const imgId = detail.imgId;
        if (imgId) {
          setOriginalImg({
            imgId,
            imgUrl: detail.imgUrl,
          });
        }
      } else {
        handleChangePaneStatus(PANE_STATUS_LIST);
      }
    }
    setLoad(true);
  };

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handlePostImage = async (file: File) => {
    const imgId = await postImage(file);

    setInputs({
      ...inputs,
      imgId,
    });
  };

  const handleCancelImage = () => {
    const current = fileInput.current;
    if (current) {
      current.value = '';
    }

    setInputs({
      ...inputs,
      imgId: '',
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);

    const isValid = inputs.productTitle && inputs.itemId;

    if (isValid) {
      setLoading(true);

      let imgId = '';
      if (originalImg.imgId) {
        imgId = originalImg.imgId;
      } else {
        imgId = inputs.imgId;
      }

      const result = await putProduct(product.productId, {
        productCode: inputs.productCode,
        productName: inputs.productTitle,
        productCategoryId: inputs.itemId,
        productSize: inputs.size,
        productWeight: inputs.weight,
        boxQuantity: inputs.eachNumberOfProduct,
        minQuantity: inputs.minimumQuantity,
        imgId,
      });

      if (result) {
        handleChangePaneStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  const handleChangePaneStatusProductEdit = () => {
    handleChangePaneStatus(PANE_STATUS_EDIT);
  };

  if (!load || !loadItemList || !loadSizeCode) {
    return <></>;
  }

  if (
    paneStatus ===
    PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER
  ) {
    return (
      <PhysicalDistributionItemRegister
        onFinish={() => {
          handleReloadItemList();
          handleChangePaneStatusProductEdit();
        }}
        onClickCancel={handleChangePaneStatusProductEdit}
      />
    );
  }

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'text_item'} essential={true} />
        {itemList.length === 0 && (
          <a
            className="btn-add-cat"
            onClick={() =>
              handleChangePaneStatus(
                PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER
              )
            }
          >
            <span className="material-icons-outlined font-size-24pt mr-2">
              add
            </span>
            {t('text_add_item')}
          </a>
        )}
        {itemList.length > 0 && (
          <select
            className="form-line"
            id="itemId"
            value={inputs.itemId ? inputs.itemId : ''}
            onChange={(e) => {
              const value = e.target.value;
              const item = itemList.find(
                (item) => item.productCategoryId === value
              );
              if (item) {
                setProductCodePrefix(item.productCodePrefix);
                setInputs({
                  ...inputs,
                  [e.target.id]: item.productCategoryId,
                });
              }
            }}
          >
            {itemList.map((item) => (
              <option
                key={item.productCategoryId}
                value={item.productCategoryId}
              >
                {item.productCategoryName}
              </option>
            ))}
          </select>
        )}
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_product_code'} className={'mb-0'} />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_product_code')}
          autoComplete={'off'}
          id="productCode"
          value={inputs.productCode}
          onChange={handleChangeInputsValue}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_product_title'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_product_title')}
          id="productTitle"
          value={inputs.productTitle}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_size'} className={'mb-0'} essential={true} />
        <select
          className="form-line"
          id="size"
          value={inputs.size}
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        >
          {productCodeSizeList.map(({ code, text }) => (
            <option key={code} value={code}>
              {text}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_weight_unit_gram'}
          className={'mb-0'}
          essential={true}
        />
        <InputNumber
          className="form-line"
          value={inputs.weight}
          onChange={(value) => {
            setInputs({
              ...inputs,
              weight: value,
            });
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_each_number_of_product'}
          className={'mb-0'}
          essential={true}
        />
        <InputNumber
          className="form-line"
          value={inputs.eachNumberOfProduct}
          onChange={(value) => {
            setInputs({
              ...inputs,
              eachNumberOfProduct: value,
            });
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_stock_minimum_quantity'}
          className={'mb-0'}
          essential={true}
        />
        <InputNumber
          className="form-line"
          value={inputs.minimumQuantity}
          onChange={(value) => {
            setInputs({
              ...inputs,
              minimumQuantity: value,
            });
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_product_image'} />
        <small className="text-50 ml-1">
          {t('text_product_image_recommended_size')}
        </small>
        {!originalImg.imgId && (
          <>
            <input
              className="form-control"
              type="file"
              accept={'image/*'}
              onChange={(e) => {
                if (e.target.files && e.target.files.length) {
                  handlePostImage(e.target.files[0]);
                } else {
                  setInputs({
                    ...inputs,
                    imgId: '',
                  });
                }
              }}
              ref={fileInput}
            />
            {inputs.imgId && (
              <a className="file-del" onClick={handleCancelImage}>
                <MaterialIcon name={'close'} />
              </a>
            )}
          </>
        )}
        {originalImg.imgId && (
          <FormRow className="align-items-center">
            <div className="col-auto">
              <div className="avatar">
                <img src={originalImg.imgUrl} className="avatar-img rounded" />
              </div>
            </div>
            <div className="col">
              <div className="font-weight-bold"></div>
            </div>
            <div className="col-auto">
              <a
                className="text-muted-light"
                onClick={() =>
                  setOriginalImg({
                    imgId: '',
                    imgUrl: '',
                  })
                }
              >
                <MaterialIcon name={'close'} />
              </a>
            </div>
          </FormRow>
        )}
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => handleChangePaneStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_do_edit')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default PhysicalDistributionProductEdit;
