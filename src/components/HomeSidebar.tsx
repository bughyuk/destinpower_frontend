import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHomeMenu } from '@/modules/home/hook';
import classNames from 'classnames';
import {
  CollapseMenuIdx,
  Menu,
  MENU_LOGISTICS_MANAGEMENT,
} from '@/modules/home/types';
import { Collapse } from 'react-bootstrap';
import { fetchProject, fetchProjects } from '@/api/project';
import { ListResult } from '@/modules/common';
import { Projects } from '@/modules/project/types';
import { Floors, Space } from '@/modules/map/types';
import { useHistory } from 'react-router-dom';
import { useControlProject } from '@/modules/map/hook';
import AlertModal from '@/components/AlertModal';

function HomeSidebar() {
  const history = useHistory();
  const { t } = useTranslation();
  const { menus, activeMenuIdx, handleSetMenu } = useHomeMenu();
  const { handleSetProject } = useControlProject();
  const [showAlertModal, setShowAlertModal] = useState(false);

  const [collapseMenuIdx, setCollapseMenuIdx] = useState<CollapseMenuIdx>(
    undefined
  );

  const handleClickCollapseMenu = (menuIdx: CollapseMenuIdx) => {
    if (collapseMenuIdx !== menuIdx) {
      setCollapseMenuIdx(menuIdx);
    } else {
      setCollapseMenuIdx(undefined);
    }
  };

  const handleFetchProject = async () => {
    const result = await fetchProjects(1, '');
    const listResult = result as ListResult<Projects>;
    if (listResult.content.length) {
      handleFetchProjectDetail(listResult.content[0].projectId);
    } else {
      setShowAlertModal(true);
      handleSetMenu(-1);
    }
  };

  const handleFetchProjectDetail = async (projectId: string) => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((space) => {
        const projectFloors: Floors[] = [];
        space.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: space.mappingId,
          id: space.metaId,
          name: space.metaName,
          longitude: space.lng,
          latitude: space.lat,
          floorsList: projectFloors,
          registDate: space.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.push('/control');
    }
  };

  useEffect(() => {
    const findMenu = menus.find((menu) => menu.menuIdx === activeMenuIdx);
    if (!findMenu) {
      menus.some((menu) => {
        const findSubMenu = menu.subMenus.find(
          (subMenu) => subMenu.menuIdx === activeMenuIdx
        );
        if (findSubMenu) {
          setCollapseMenuIdx(menu.menuIdx as CollapseMenuIdx);
          return true;
        }
      });
    } else {
      setCollapseMenuIdx(undefined);
      if (findMenu.menuIdx === MENU_LOGISTICS_MANAGEMENT) {
        handleFetchProject();
      }
    }
  }, [activeMenuIdx]);

  return (
    <>
      <aside className="side-nav sidebar sidebar-light sidebar-light-dodger-blue sidebar-left">
        <ul className="sidebar-menu">
          {menus.map((menu) => {
            if (menu.subMenus.length) {
              return (
                <MultiMenu
                  key={menu.menuIdx}
                  {...menu}
                  collapseMenuIdx={collapseMenuIdx}
                  onClickCollapseMenu={handleClickCollapseMenu}
                />
              );
            } else {
              return (
                <SingleMenu
                  key={menu.menuIdx}
                  {...menu}
                  onClickCollapseMenu={handleClickCollapseMenu}
                />
              );
            }
          })}
        </ul>
      </aside>
      <AlertModal show={showAlertModal} onHide={() => setShowAlertModal(false)}>
        공정 관리를 할 수 없습니다.
      </AlertModal>
    </>
  );
}

type MenuProps = Menu & {
  onClickCollapseMenu: (menuIdx: CollapseMenuIdx) => void;
};

function SingleMenu({
  menuIdx,
  titleKey,
  iconName,
  active,
  onClickCollapseMenu,
}: MenuProps) {
  const { t } = useTranslation();
  const { handleSetMenu } = useHomeMenu();

  const handleClickMenu = () => {
    onClickCollapseMenu(undefined);
    handleSetMenu(menuIdx);
  };

  return (
    <li
      key={menuIdx}
      className={classNames('sidebar-menu-item', {
        active: active,
      })}
    >
      <a className="sidebar-menu-button" onClick={handleClickMenu}>
        <span className="material-icons-outlined sidebar-menu-icon sidebar-menu-icon--left">
          {iconName}
        </span>
        <span className="sidebar-menu-text">{t(titleKey)}</span>
      </a>
    </li>
  );
}

type MultiMenuProps = MenuProps & {
  collapseMenuIdx: CollapseMenuIdx;
};

function MultiMenu({
  menuIdx,
  titleKey,
  iconName,
  subMenus,
  collapseMenuIdx,
  onClickCollapseMenu,
}: MultiMenuProps) {
  const { t } = useTranslation();
  const { activeMenuIdx, handleSetMenu } = useHomeMenu();

  const subMenusIdxList = subMenus.map((subMenus) => subMenus.menuIdx);

  return (
    <li
      className={classNames('sidebar-menu-item', {
        active: subMenusIdxList.includes(activeMenuIdx),
        open: collapseMenuIdx === menuIdx,
      })}
    >
      <a
        className="sidebar-menu-button"
        onClick={() => onClickCollapseMenu(menuIdx as CollapseMenuIdx)}
      >
        <span className="material-icons-outlined sidebar-menu-icon sidebar-menu-icon--left">
          {iconName}
        </span>
        {t(titleKey)}
        <span className="ml-auto sidebar-menu-toggle-icon"></span>
      </a>
      <Collapse in={collapseMenuIdx === menuIdx}>
        <ul className="sidebar-submenu sm-indent">
          {subMenus.map((subMenu) => (
            <li
              key={subMenu.menuIdx}
              className={classNames('sidebar-menu-item', {
                active: activeMenuIdx === subMenu.menuIdx,
              })}
            >
              <a
                className="sidebar-menu-button"
                onClick={() => handleSetMenu(subMenu.menuIdx)}
              >
                <span className="sidebar-menu-text">{t(subMenu.titleKey)}</span>
              </a>
            </li>
          ))}
        </ul>
      </Collapse>
    </li>
  );
}

export default HomeSidebar;
