import React, { useState } from 'react';
import { SignUpMemberProps } from '@/components/SignUpMember';
import { useTranslation } from 'react-i18next';
import FormLabel from '@/components/FormLabel';
import _ from 'lodash';
import MaterialIcon from '@/components/MaterialIcon';
import { Alert } from 'react-bootstrap';
import { ValidUtils } from '@/utils';

type SignUpMemberInviteProps = SignUpMemberProps;

function SignUpMemberInvite({ onChangeMemberStep }: SignUpMemberInviteProps) {
  const { t } = useTranslation();
  const [emailList, setEmailList] = useState<string[]>(['']);
  const [feedbackMessage, setFeedbackMessage] = useState('');

  const handleAddInviteEmail = () => {
    setEmailList([...emailList, '']);
  };

  const handleRemoveInviteEmail = (i: number) => {
    const removeEmailList = [...emailList];
    removeEmailList.splice(i, 1);
    setEmailList(removeEmailList);
  };

  const handleChangeEmail = (email: string, i: number) => {
    const changeEmailList = [...emailList];
    changeEmailList.splice(i, 1, email);
    setEmailList(changeEmailList);
  };

  const handleClickSend = () => {
    let sendEmailList = emailList.filter((email) => email !== '');

    if (!sendEmailList.length) {
      setFeedbackMessage(t('msg_valid_enter_at_least_one_email'));
    } else {
      sendEmailList = _.uniq(sendEmailList);
      let isPass = true;
      sendEmailList.some((email) => {
        const isEmail = ValidUtils.validateEmail(email);
        if (!isEmail) {
          isPass = false;
          return true;
        }
      });

      if (!isPass) {
        setFeedbackMessage(t('msg_invalid_email_format'));
        return;
      }

      setFeedbackMessage('');

      // TODO: API
      onChangeMemberStep(4);
    }
  };

  return (
    <>
      <div className="title-group mb-5">
        <h3 className="m-0">{t('text_invite_user')}</h3>
        <p className="mt-2 text-50 font-size-16pt">
          {t('msg_invite_user_to_collaborate')}
        </p>
      </div>
      <div className="form-group mb-5">
        <FormLabel textKey={'text_email'} essential={true} />
        {emailList.map((email, i) => (
          <InviteEmail
            key={i}
            email={email}
            deleteFlag={i !== emailList.length - 1}
            onChangeEmail={(email) => handleChangeEmail(email, i)}
            onClickAdd={handleAddInviteEmail}
            onClickRemove={() => handleRemoveInviteEmail(i)}
          />
        ))}
      </div>
      {feedbackMessage && (
        <Alert className="alert-soft-accent">
          <div className="d-flex flex-wrap align-items-center">
            <div className="mr-8pt">
              <MaterialIcon name={'error_outline'} />
            </div>
            <div className="flex" style={{ minWidth: '180px' }}>
              <small className="text-black-100">{feedbackMessage}</small>
            </div>
          </div>
        </Alert>
      )}
      <div className="form-group text-center mb-32pt">
        <button
          className="btn btn-block btn-lg btn-accent"
          type="button"
          onClick={handleClickSend}
        >
          {t('text_send_invitation')}
        </button>
      </div>
    </>
  );
}

type InviteEmailProps = {
  email: string;
  deleteFlag: boolean;
  onChangeEmail: (email: string) => void;
  onClickAdd: () => void;
  onClickRemove: () => void;
};

function InviteEmail({
  email,
  deleteFlag,
  onChangeEmail,
  onClickAdd,
  onClickRemove,
}: InviteEmailProps) {
  const { t } = useTranslation();
  return (
    <div className="file-controls">
      <div className="multi-file d-flex flex mb-4">
        <input
          type="email"
          className="form-line"
          placeholder={t('place_holder_email_of_invite_user')}
          value={email}
          onChange={(e) => onChangeEmail(e.target.value)}
        />
        <a
          className="btn-file-add align-items-center circle-pin"
          onClick={() => (deleteFlag ? onClickRemove() : onClickAdd())}
        >
          <span className="material-icons">
            {deleteFlag ? 'delete_outline' : 'add'}
          </span>
        </a>
      </div>
    </div>
  );
}

export default SignUpMemberInvite;
