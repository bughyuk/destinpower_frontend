import React, {
  CSSProperties,
  ReactNode,
  useEffect,
  useRef,
  useState,
} from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import classNames from 'classnames';
import { useFloatPane } from '@/modules/setup/hook';

type LeftSidebarDepthProps = {
  show: boolean;
  className?: string;
  wrapStyle?: CSSProperties;
  children?: ReactNode;
};

function LeftSidebarDepth({
  show,
  className,
  wrapStyle,
  children,
}: LeftSidebarDepthProps) {
  const perfectScrollbarRef = useRef<PerfectScrollbar>(null);
  const [style, setStyle] = useState<CSSProperties>({
    display: 'none',
  });
  const { handleSetContainer } = useFloatPane();

  useEffect(() => {
    const current = perfectScrollbarRef.current;

    if (current) {
      handleSetContainer(current);
    }
  }, []);

  useEffect(() => {
    if (show) {
      setStyle({
        position: 'absolute',
        transform: 'translateZ(0)',
      });
    } else {
      setStyle({
        position: 'absolute',
      });
    }
  }, [show]);

  return (
    <>
      <PerfectScrollbar
        className={classNames('sidebar-depth', className)}
        style={style}
        ref={perfectScrollbarRef}
      >
        <div className="sidebar-depth-wrap" style={wrapStyle}>
          {children}
        </div>
      </PerfectScrollbar>
    </>
  );
}

export default LeftSidebarDepth;
