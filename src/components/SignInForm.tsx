import React, { useEffect } from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import FormLabel from '@/components/FormLabel';
import FormGroup from '@/components/FormGroup';
import { Alert } from 'react-bootstrap';
import { useSignIn } from '@/modules/access/signin/hook';
import { PREFIX_FRONTEND_PATH } from '@/utils/constants/common';

function SignInForm() {
  const { t } = useTranslation();
  const history = useHistory();
  const {
    email,
    password,
    validEmail,
    validPassword,
    remember,
    loading,
    user,
    error,
    handleInputsChange,
    handleRememberChange,
    handleSubmit,
    handleSignInUserInfoGet,
  } = useSignIn();

  useEffect(() => {
    if (user) {
      handleSetStorageItem();
    }
  }, [user]);

  const handleSetStorageItem = async () => {
    await handleSignInUserInfoGet();
    history.replace('/home');
  };

  return (
    <>
      <img src="/static/images/logo.svg" className="id-logo" alt="WATA" />
      <h5
        className={
          location.pathname === `${PREFIX_FRONTEND_PATH}/signin`
            ? 'mb-5'
            : 'mb-4'
        }
      >
        AI Cloud Spatial Awareness platform
      </h5>
      <form onSubmit={handleSubmit} noValidate={true}>
        <div className="form-group mb-4">
          <FormLabel textKey={'text_email'} htmlFor={'email'} />
          <input
            id="email"
            type="email"
            className="form-line"
            value={email}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              handleInputsChange(e.target.id, e.target.value);
            }}
            placeholder={t('place_holder_email')}
            autoComplete={'off'}
          />
          {!validEmail && (
            <div className="invalid-feedback">{t('msg_valid_empty_email')}</div>
          )}
        </div>
        <div className="form-group">
          <FormLabel textKey={'text_password'} htmlFor={'password'} />
          <input
            id="password"
            type="password"
            className="form-line"
            value={password}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              handleInputsChange(e.target.id, e.target.value);
            }}
            placeholder={t('place_holder_password')}
          />
          {!validPassword && (
            <div className="invalid-feedback">
              {t('msg_valid_empty_password')}
            </div>
          )}
        </div>
        <FormGroup>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="remember"
              checked={remember}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                handleRememberChange(e.target.checked);
              }}
            />
            <label className="custom-control-label" htmlFor="remember">
              {t('text_remember_email')}
            </label>
          </div>
        </FormGroup>
        {error && (
          <Alert className="alert-soft-accent">
            <div className="d-flex flex-wrap align-items-center">
              <div className="mr-8pt">
                <MaterialIcon name={'error_outline'} />
              </div>
              <div className="flex" style={{ minWidth: '180px' }}>
                <small className="text-black-100">
                  {t('msg_sign_in_failure')}
                </small>
              </div>
            </div>
          </Alert>
        )}

        <div className="form-group text-center mb-32pt">
          <button
            className="btn btn-block btn-lg btn-accent"
            disabled={loading}
            type="submit"
          >
            <MaterialIcon name="power_settings_new" align="left" />
            {t('text_sign_in')}
          </button>
          <br />
        </div>
        <ul className="signin-help">
          <li>
            {t('msg_forget_password')}
            <Link className="text-body text-underline" to={'/find/password'}>
              {t('text_find_password')}
            </Link>
            <br />
          </li>
          <li>
            {t('msg_do_not_have_account')}
            <Link className="text-body text-underline" to={'/signup'}>
              {t('text_gen_account')}
            </Link>
          </li>
        </ul>
      </form>
    </>
  );
}

export default SignInForm;
