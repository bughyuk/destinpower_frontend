import React, { useEffect, useRef, useState } from 'react';
import { useFloatPane } from '@/modules/setup/hook';
import {
  FLOAT_PANE_COMPLETED_PROCESS,
  FLOAT_PANE_PROCESS_CHECK,
  FLOAT_PANE_PROCESS_CHECK_LIST_MANAGEMENT,
  FLOAT_PANE_PROCESS_DASHBOARD,
} from '@/modules/setup/types';
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Collapse, Modal, ModalBody, ModalFooter } from 'react-bootstrap';
import moment from 'moment';
import ModalHeader from 'react-bootstrap/ModalHeader';
import MaterialIcon from '@/components/MaterialIcon';
import FloatProcessDashboard from '@/components/FloatProcessDashboard';
import FloatProcessCompleted from '@/components/FloatProcessCompleted';
import FloatProcessCheckListManagement from '@/components/FloatProcessCheckListManagement';
import {
  fetchProcessWorkingInfo,
  fetchProcessWorkingHistory,
  postProcessWorkingStepProgress,
  postProcessWorkingItemProgress,
} from '@/api/process';
import {
  PROGRESS_DEFAULT,
  PROGRESS_START,
  PROGRESS_COMPLETE,
} from '@/modules/physical_distribution/types';

function PageCover() {
  const { show: floatPaneShow, view } = useFloatPane();

  if (!floatPaneShow) {
    return <></>;
  }

  return (
    <PerfectScrollbar>
      <div className="page-cover responsive-item">
        {view === FLOAT_PANE_PROCESS_CHECK && <FloatProcessCheck />}
        {view === FLOAT_PANE_COMPLETED_PROCESS && <FloatProcessCompleted />}
        {view === FLOAT_PANE_PROCESS_DASHBOARD && <FloatProcessDashboard />}
        {view === FLOAT_PANE_PROCESS_CHECK_LIST_MANAGEMENT && (
          <FloatProcessCheckListManagement />
        )}
      </div>
    </PerfectScrollbar>
  );
}

function FloatProcessCheck() {
  const [load, setLoad] = useState(false);
  const { checkListId } = useFloatPane();

  useEffect(() => {
    if (checkListId !== '-1') {
      handleFetchProcessCheck();
    }
  }, [checkListId]);

  const [curCheckStep, setCurCheckStep] = useState(0);
  const [
    totalCompletedProgressCount,
    setTotalCompletedProgressCount,
  ] = useState(0);
  const [
    totalCompletedProgressCountMap,
    setTotalCompletedProgressCountMap,
  ] = useState<Map<string, number>>(new Map());
  useEffect(() => {
    let sum = 0;
    totalCompletedProgressCountMap.forEach((value) => {
      sum += value;
    });

    setTotalCompletedProgressCount(sum);
  }, [totalCompletedProgressCountMap]);

  const [totalProgressTime, setTotalProgressTime] = useState(0);
  const [totalProgressTimeMap, setTotalProgressTimeMap] = useState<
    Map<string, number>
  >(new Map());
  useEffect(() => {
    let sum = 0;
    totalProgressTimeMap.forEach((value) => {
      sum += value;
    });

    setTotalProgressTime(sum);
  }, [totalProgressTimeMap]);

  const handleFetchProcessCheck = async () => {
    setLoad(false);

    if (checkListId.length === 0) return;

    try {
      console.log(checkListId);
      const info = await fetchProcessWorkingInfo(checkListId);

      setSummary({
        image: info.product_type.image_path,
        projectName: info.name,
        processPer: info.process_per,
        totalLT: info.total_lt,
      });
      setDetailProcessInfo({
        sn: info.sn,
        pName: info.pname,
        workingTime: info.working_time,
        endTime: info.expected_time,
        buyer: info.buyer.name,
        product: info.product_type.name,
      });

      const setSteps = info.steps.map((step: any) => {
        return {
          checkId: step.id, //step.grp.id,
          checkStep: step.ord,
          checkName: step.name,
          startTime: step.start_time,
          endTime: step.end_time,
          processCheckDetailList: step.items.map((item: any) => {
            return {
              checkDetailId: item.id,
              checkDetailName: item.name,
              checkDetailStep: item.ord,
              startTime: item.start_time,
              endTime: item.end_time,
            };
          }),
        };
      });

      setProcessCheckList(setSteps);
    } catch (err) {
      console.log(err);
      return;
    }

    // setLoad(true);
  };

  // if (!load) {
  //   return <></>;
  // }

  const [summary, setSummary] = useState({
    image: '',
    projectName: '',
    processPer: '',
    totalLT: '',
  });
  const [detailProcessInfo, setDetailProcessInfo] = useState({
    sn: '',
    pName: '',
    workingTime: '',
    endTime: '',
    buyer: '',
    product: '',
  });
  const [processCheckList, setProcessCheckList] = useState<any[]>([]);

  return (
    <>
      {true && (
        <div>
          <div className="d-flex flex-row mb-5 border-1">
            <div className="flex title-row p-4 border-right-1">
              {/* <div>
                <img src={summary.image} className="w-100" />
              </div> */}
              <p className="mb-0 font-weight-bolder font-size-16pt">
                프로젝트 명
              </p>
              <h2 className="d-flex align-items-center mb-0 ">
                {summary.projectName}
              </h2>
            </div>
            <div className="flex title-row p-4 border-right-1">
              <p className="mb-0 font-weight-bolder font-size-16pt">
                전체 공정률
              </p>
              <h2 className="d-flex align-items-center mb-0 ">
                <span className="text-primary">{`${Math.round(
                  (totalCompletedProgressCount /
                    processCheckList.reduce(
                      (a, b) => a + b.processCheckDetailList.length,
                      0
                    )) *
                    100
                )}%`}</span>
              </h2>
            </div>
            <div className="flex title-row p-4">
              <p className="mb-0 font-weight-bolder font-size-16pt">전체 L/T</p>
              <h2 className="d-flex align-items-center mb-0 ">
                <span className="text-primary">
                  {moment.utc(totalProgressTime).format('HH:mm:ss')}
                </span>
              </h2>
            </div>
          </div>
          <div className="contents-view">
            <h4>세부 공정 사항</h4>
            <table className="table lg-table mb-4 table-nowrap thead-bg-light">
              <colgroup>
                <col width="15%" />
                <col width="35%" />
                <col width="15%" />
                <col width="35%" />
              </colgroup>
              <tbody>
                <tr>
                  <th>시리얼 번호</th>
                  <td>{detailProcessInfo.sn}</td>
                  <th>제품명</th>
                  <td>{detailProcessInfo.pName}</td>
                </tr>
                <tr>
                  <th>작업 시작일</th>
                  <td>{detailProcessInfo.workingTime}</td>
                  <th>작업 종료일</th>
                  <td>{detailProcessInfo.endTime}</td>
                </tr>
                <tr>
                  <th>거래처</th>
                  <td>{detailProcessInfo.buyer}</td>
                  <th>제품</th>
                  <td>{detailProcessInfo.product}</td>
                </tr>
              </tbody>
            </table>
            <div className="page-separator mt-5">
              <div className="page-separator__text">체크리스트</div>
            </div>
            <div className="accordion js-accordion accordion--boxed list-group-flush list-bg-light">
              {processCheckList.map((processCheck) => (
                <ProcessCheckItem
                  key={processCheck.checkId}
                  processCheck={processCheck}
                  curCheckStep={curCheckStep} // 초기값이 99
                  onChangeCheckStep={() => {
                    setCurCheckStep(99); // 전체 완료 처리되면, curCheckStep은 99
                  }}
                  onChangeTotalCompletedProgressCount={(
                    checkId,
                    completedProgressCount
                  ) => {
                    totalCompletedProgressCountMap.set(
                      checkId,
                      completedProgressCount
                    );
                    setTotalCompletedProgressCountMap(
                      new Map(totalCompletedProgressCountMap)
                    );
                  }}
                  onChangeTotalProgressTime={(checkId, progressTime) => {
                    totalProgressTimeMap.set(checkId, progressTime);
                    setTotalProgressTimeMap(new Map(totalProgressTimeMap));
                  }}
                />
              ))}
            </div>
          </div>
        </div>
      )}
    </>
  );
}

type ProcessCheckItemProps = {
  processCheck: any;
  curCheckStep: number;
  onChangeCheckStep: () => void;
  onChangeTotalCompletedProgressCount: (
    checkId: string,
    completedProgressCount: number
  ) => void;
  onChangeTotalProgressTime: (checkId: string, progressTime: number) => void;
};

function ProcessCheckItem({
  processCheck,
  curCheckStep,
  onChangeCheckStep,
  onChangeTotalCompletedProgressCount,
  onChangeTotalProgressTime,
}: ProcessCheckItemProps) {
  const [isOpen, setOpen] = useState(false);
  const [isStart, setStart] = useState(false);
  const [isComplete, setComplete] = useState(false);
  const [progressTime, setProgressTime] = useState(0);
  const [completedDetailItemCount, setCompletedDetailItemCount] = useState(0);
  const [completedPercent, setCompletedPercent] = useState(0);
  const timeRef = useRef(0);
  const timerRef = useRef<ReturnType<typeof setInterval> | null>(null);

  const handleClickProcessStart = async () => {
    setOpen(true);

    try {
      const result = await postProcessWorkingStepProgress(
        processCheck.checkId,
        PROGRESS_START
      );
    } catch (err) {
      alert(err);
    }

    setStart(true);
  };

  // 체크 아이템 모두 체크 되어 (completed == length)
  // 사용자가 완료 클릭할 경우, post 하고
  // 시간 세기 멈춤 (clear interval)
  const handleClickProcessComplete = async () => {
    try {
      const result = await postProcessWorkingStepProgress(
        processCheck.checkId,
        PROGRESS_COMPLETE
      );
      console.log(result);
    } catch (err) {
      alert(err);
    }

    setComplete(true);
    setCompletedDetailItemCount(processCheck.processCheckDetailList.length);
    handleClearInterval();
    setCompletedPercent(100);
    onChangeTotalCompletedProgressCount(
      processCheck.checkId,
      processCheck.processCheckDetailList.length
    );
  };

  const handleClearInterval = () => {
    if (timerRef.current) {
      clearInterval(timerRef.current);
    }
  };

  const handleStartInterval = () => {
    handleClearInterval();
    let time = timeRef.current || 0;
    timerRef.current = setInterval(() => {
      time = time + 1000;
      timeRef.current = time;
      setProgressTime(time);
      onChangeTotalProgressTime(processCheck.checkId, time);
    }, 1000);
  };

  useEffect(() => {
    if (processCheck.startTime) {
      setStart(true);
    }
    if (processCheck.startTime && processCheck.endTime) {
      setCompletedDetailItemCount(processCheck.processCheckDetailList.length);
    }

    return () => {
      handleClearInterval();
    };
  }, []);

  // 아이템 체크될 때마다 호출
  useEffect(() => {
    // // 아이템 전체 완료
    // if (
    //   completedDetailItemCount === processCheck.processCheckDetailList.length
    // ) {
    //   setComplete(false);
    //   onChangeCheckStep();
    //   handleClearInterval();
    // } else {
    //   // 하나라도 완료가 아닐 경우
    //   setComplete(true);
    //   if (isStart) {
    //     // 초 세기
    //     handleStartInterval();
    //   }
    // }

    if (completedDetailItemCount === 0) {
      // 공정 시작 버튼 활성, L/T 회색 처리, 초 세기 없음
      // 공정 완료 버튼 비활성
    } else if (completedDetailItemCount < processCheck.processCheckDetailList.length) {
      // 공정 시작 버튼 비활성
      // 공정 완료 버튼 비활성
    } else if (completedDetailItemCount === processCheck.processCheckDetailList.length) {
      // 공정 완료 버튼 비활성
      // 공정 완료 버튼 활성
    }

    setCompletedPercent(
      Math.round(
        (completedDetailItemCount /
          processCheck.processCheckDetailList.length) *
          100
      )
    );

    onChangeTotalCompletedProgressCount(
      processCheck.checkId,
      completedDetailItemCount
    );
  }, [completedDetailItemCount]);

  useEffect(() => {
    if (isStart) {
      handleStartInterval();
    }
  }, [isStart]);

  return (
    <>
      <div
        className={classNames('accordion__item', {
          open: isOpen,
        })}
      >
        <div className="accordion__toggle">
          <ul className="ac-table-item">
            <li className="ac-col-01">
              <h6 className="flex m-0">{processCheck.checkName}</h6>
            </li>
            <li className="ac-col-02">
              {!isStart && processCheck.checkStep > curCheckStep && (
                <>공정 시작1</>
              )}
              {!isStart && processCheck.checkStep <= curCheckStep && (
                <div className="duration-time-holder holder-half state-play">
                  <a
                    className="d-flex text-light"
                    onClick={handleClickProcessStart}
                  >
                    <span className="material-icons-outlined mb-0 mr-3">
                      play_circle_filled
                    </span>
                  </a>
                  <strong>공정 시작2</strong>
                </div>
              )}
              {isStart && <>공정 시작3</>}
            </li>
            <li className="ac-col-03">
              {/* 시작 안함, 완료 안됨 (회색) */}
              {!isStart && !isComplete && <>공정 완료1</>}

              {/* 시작 상태, 아이템 전체 체크 상태 */}
              {isStart && !isComplete && (
                <div className="duration-time-holder holder-half state-comp">
                  <a
                    className="d-flex text-light"
                    onClick={handleClickProcessComplete}
                  >
                    <span className="material-icons mb-0 mr-3">
                      check_circle
                    </span>
                  </a>
                  <strong>공정 완료2</strong>
                </div>
              )}

              {/* 시작 상태, 완료 상태 (완료 공정) */}
              {isStart && isComplete && <>공정 완료3</>}
            </li>
            <li className="ac-col-04">
              <div
                className={classNames('duration-time-holder', {
                  'state-standby':
                    !isStart && processCheck.checkStep >= curCheckStep,
                  'state-play': isStart && !isComplete,
                  'state-comp': isStart && isComplete,
                })}
              >
                <span className="text-light mb-0 mr-3">L/T</span>
                <strong>{moment.utc(progressTime).format('HH:mm:ss')}</strong>
              </div>
            </li>
            <li className="ac-col-05">
              <div className="flex ml-auto px-4">
                <div
                  className="progress"
                  style={{
                    height: '18px',
                  }}
                >
                  <div
                    className={classNames('progress-bar', {
                      'pg-bar-ing': isStart && !isComplete,
                      'pg-bar-success': isStart && isComplete,
                    })}
                    role="progressbar"
                    style={{
                      width: `${completedPercent}%`,
                    }}
                    aria-valuenow={100}
                    aria-valuemin={0}
                    aria-valuemax={100}
                  >
                    {`${completedPercent}%`}
                  </div>
                </div>
              </div>
            </li>
            <li className="ac-col-06">
              <a
                className="d-flex h-100 align-items-center px-3 circle-pin"
                onClick={() => setOpen(!isOpen)}
              >
                <span className="accordion__toggle-icon material-icons">
                  keyboard_arrow_down
                </span>
              </a>
            </li>
          </ul>
        </div>
        <Collapse in={isOpen}>
          <div
            className={classNames('accordion__menu', {
              show: isOpen,
            })}
          >
            <div className="list-group list-group-flush border-top-1">
              {processCheck.processCheckDetailList.map(
                (processCheckDetail: any) => (
                  <ProcessCheckDetailItem
                    key={processCheckDetail.checkDetailId}
                    processCheckId={processCheck.checkId}
                    processCheckDetail={processCheckDetail}
                    isStart={isStart}
                    isComplete={isComplete}
                    onChangeCheck={(checked) => {
                      if (checked) {
                        if (
                          completedDetailItemCount <
                          processCheck.processCheckDetailList.length
                        ) {
                          setCompletedDetailItemCount(
                            completedDetailItemCount + 1
                          );
                        }
                      } else {
                        if (completedDetailItemCount > 0) {
                          setCompletedDetailItemCount(
                            completedDetailItemCount - 1
                          );
                        }
                      }
                    }}
                  />
                )
              )}
            </div>
          </div>
        </Collapse>
      </div>
    </>
  );
}

type ProcessCheckDetailItemProps = {
  processCheckId: string;
  processCheckDetail: any;
  isStart: boolean;
  isComplete: boolean;
  onChangeCheck: (checked: boolean) => void;
};

function ProcessCheckDetailItem({
  processCheckId,
  processCheckDetail,
  isStart,
  isComplete,
  onChangeCheck,
}: ProcessCheckDetailItemProps) {
  const [startDateTime, setStartDateTime] = useState('');
  const [completedDateTime, setCompletedDateTime] = useState('');
  const [isCheck, setCheck] = useState(false);
  const [isShowModal, setShowModal] = useState(false);

  useEffect(() => {
    if (isStart) {
      setStartDateTime(moment().format('YY-MM-DD, HH:mm'));
    }
  }, [isStart]);

  useEffect(() => {
    if (isComplete) {
      if (!completedDateTime) {
        setCheck(true);
      }
    }
  }, [isComplete]);

  useEffect(() => {
    onChangeCheck(isCheck);
    if (isCheck) {
      setCompletedDateTime(moment().format('YY-MM-DD, HH:mm'));
    } else {
      setCompletedDateTime('');
    }
  }, [isCheck]);

  const handleCheckDetailCheckHandle = async (e: any) => {
    const checked = e.target.checked;

    try {
      if (checked) {
        const result = await postProcessWorkingItemProgress(
          processCheckId,
          processCheckDetail.checkDetailId,
          PROGRESS_COMPLETE
        );
      } else {
        const result = await postProcessWorkingItemProgress(
          processCheckId,
          processCheckDetail.checkDetailId,
          PROGRESS_DEFAULT
        );
      }
    } catch (err) {
      alert(err);
    }

    setCheck(checked);
  };

  const handleProcessCheckHistory = async () => {
    try {
      const result = await fetchProcessWorkingHistory(
        processCheckDetail.checkDetailId
      );
      // TODO: history ui
      console.log(result);
    } catch (err) {
      alert(err);
    }

    setShowModal(true);
  };

  return (
    <>
      <div className="list-group-item">
        <ul className="ac-table-item">
          <li className="ac-col-01">
            <div className="d-flex align-items-center">
              {isStart && (
                <div className="custom-control custom-checkbox checkbox-lg checkbox-rounded">
                  <input
                    id={processCheckDetail.checkDetailId}
                    type="checkbox"
                    className="custom-control-input"
                    checked={isCheck}
                    onChange={handleCheckDetailCheckHandle}
                  />
                  <label
                    htmlFor={processCheckDetail.checkDetailId}
                    className="custom-control-label"
                  >
                    {processCheckDetail.checkDetailName}
                  </label>
                </div>
              )}
              {!isStart && (
                <div
                  style={{
                    paddingLeft: '1.5rem',
                    position: 'relative',
                    zIndex: 1,
                    display: 'block',
                    minHeight: '1.21875rem',
                  }}
                >
                  <label
                    style={{
                      fontSize: '1rem',
                      paddingLeft: '0.5rem',
                      width: '100%',
                      color: '#333',
                      marginBottom: 0,
                      position: 'relative',
                      verticalAlign: 'top',
                    }}
                  >
                    {processCheckDetail.checkDetailName}
                  </label>
                </div>
              )}
            </div>
          </li>
          <li className="ac-col-02">
            {startDateTime && (
              <>
                <span className="material-icons-outlined font-size-16pt text-30 mr-1">
                  play_circle_filled
                </span>
                {startDateTime}
              </>
            )}
          </li>
          <li className="ac-col-03">
            {completedDateTime && (
              <>
                <span className="material-icons-outlined font-size-16pt text-30 mr-1">
                  pause_circle_filled
                </span>
                {completedDateTime}
              </>
            )}
          </li>
          <li className="ac-col-04"></li>
          {isStart && (
            <>
              <li className="ac-col-05">
                <span className="badge-pich ml-3 font-weight-bolder">
                  {processCheckDetail.workerName}
                </span>
              </li>
              <li className="ac-col-06">
                <a
                  className="d-flex h-100 align-items-center px-3"
                  onClick={handleProcessCheckHistory}
                >
                  <span className="material-icons-outlined">history</span>
                </a>
              </li>
            </>
          )}
        </ul>
      </div>
      <Modal
        show={isShowModal}
        onHide={() => {
          //
        }}
        dialogClassName={'modal-custom'}
        centered={true}
      >
        <ModalHeader>
          <button
            type="button"
            className="close custom-close"
            onClick={() => setShowModal(false)}
          >
            <span>
              <MaterialIcon name={'clear'} />
            </span>
          </button>
        </ModalHeader>
        <ModalBody>
          <div className="title-group mb-4">
            <h5 className="modal-title" id="projectAddLabel">
              <td>{processCheckDetail.checkDetailName}</td>
            </h5>
          </div>
          <div className="table-cover">
            <table className="table mb-4 thead-bg-light">
              <thead>
                <tr>
                  <th>상태</th>
                  <th>날짜</th>
                  <th>작업자</th>
                </tr>
              </thead>
              <tbody>
                <tr className="text-50">
                  <td>완료</td>
                  <td>2021-09-06 16:20:12</td>
                  <td>{processCheckDetail.workerName}</td>
                </tr>
                <tr className="text-50">
                  <td>
                    <span className="text-accent">취소</span>
                  </td>
                  <td>2021-09-05 17:55:12</td>
                  <td>{processCheckDetail.workerName}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ModalBody>
        <ModalFooter>
          <button
            type="button"
            className="btn btn-accent"
            onClick={() => setShowModal(false)}
          >
            확인
          </button>
        </ModalFooter>
      </Modal>
    </>
  );
}

function FloatProcessCompletedOld() {
  const [processCheckList, setProcessCheckList] = useState<any[]>([
    {
      checkId: '0',
      checkStep: 1,
      checkName: '조립 1공정 (준비공정)',
      processCheckDetailList: [
        {
          checkDetailId: '0_0',
          checkDetailName: 'Production check sheet 및 S/N 확인',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_1',
          checkDetailName: '생산출고자재 품목/수량 출고전표와 일치',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_2',
          checkDetailName: '작업표준에 따른 라벨 부착 확인',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_3',
          checkDetailName: '덕트 및 딘레일 절단 치수 확인',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_4',
          checkDetailName: '공정별 자재 투입 확인',
          workerName: '공정 1 작업자',
        },
      ],
    },
    {
      checkId: '1',
      checkStep: 2,
      checkName: '조립 2공정 (기초공정)',
      processCheckDetailList: [
        {
          checkDetailId: '1_0',
          checkDetailName: '작업표준서에 따른 작업 확인',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_1',
          checkDetailName: 'Bolt 체결 상태 확인 (Torque, 아이마킹)',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_2',
          checkDetailName: '케이블 체결상태 확인',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_3',
          checkDetailName: 'FUSE 부착 위치 확인 (전면에 라벨이 보이게 부착)',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_4',
          checkDetailName: 'AC Capacitor whdla 전용 토크렌치사용 (MAX 10 N/M)',
          workerName: '공정 2 작업자',
        },
      ],
    },
    {
      checkId: '2',
      checkStep: 3,
      checkName: '조립 3공정 (전장품부착공정)',
      processCheckDetailList: [
        {
          checkDetailId: '2_0',
          checkDetailName: '작업표준에 따른 작업 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_1',
          checkDetailName: '덕트 및 딘레일 부착 상태 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_2',
          checkDetailName: '전장품 부착상태 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_3',
          checkDetailName: '애자 부착상태 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_4',
          checkDetailName: '도어 및 커버 보관 상태 확인',
          workerName: '공정 3 작업자',
        },
      ],
    },
    {
      checkId: '3',
      checkStep: 4,
      checkName: '조립 4공정 (부스바공정)',
      processCheckDetailList: [
        {
          checkDetailId: '3_0',
          checkDetailName: '작업표준서에 따른 작업 확인',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_1',
          checkDetailName: '부스바 체결상태 확인',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_2',
          checkDetailName: '작업표준서에 따른 작업 확인',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_3',
          checkDetailName: 'Bolt 체결 상태 확인 (Torque, 아이마킹)',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_4',
          checkDetailName: '풀림방지너트와 일반너트 사용 확인',
          workerName: '공정 4 작업자',
        },
      ],
    },
    {
      checkId: '4',
      checkStep: 5,
      checkName: '조립 5공정 (하네스공정)',
      processCheckDetailList: [
        {
          checkDetailId: '4_0',
          checkDetailName: '스마트와이어링키트 사용 작업 확인',
          workerName: '공정 5 작업자',
        },
        {
          checkDetailId: '4_1',
          checkDetailName: '케이블 체결상태 확인',
          workerName: '공정 5 작업자',
        },
        {
          checkDetailId: '4_2',
          checkDetailName: '페라이트코어 부착상태 확인',
          workerName: '공정 5 작업자',
        },
        {
          checkDetailId: '4_3',
          checkDetailName: '통신케이블 배선 확인',
          workerName: '공정 5 작업자',
        },
      ],
    },
    {
      checkId: '5',
      checkStep: 6,
      checkName: '조립 6공정 (하네스공정)',
      processCheckDetailList: [
        {
          checkDetailId: '5_0',
          checkDetailName: '제품 내외부 청결 상태 확인',
          workerName: '공정 6 작업자',
        },
        {
          checkDetailId: '5_1',
          checkDetailName: '세이프티커버 부착 상태 확인',
          workerName: '공정 6 작업자',
        },
        {
          checkDetailId: '5_2',
          checkDetailName: '라벨류(주의, 심볼등등) 부착상태 확인',
          workerName: '공정 6 작업자',
        },
        {
          checkDetailId: '5_3',
          checkDetailName: '첨부품 확인',
          workerName: '공정 6 작업자',
        },
      ],
    },
    {
      checkId: '6',
      checkStep: 7,
      checkName: '조립 검사',
      processCheckDetailList: [
        {
          checkDetailId: '6_0',
          checkDetailName: '하네스 케이블 결선상태',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_1',
          checkDetailName: '조립 C/S(조하시스템 자체 C/S)',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_2',
          checkDetailName: 'Label 및 볼트, Fuse 상태 확인',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_3',
          checkDetailName: '내부 최종 조립 상태',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_4',
          checkDetailName: '단선상태 확인',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_5',
          checkDetailName: '자재/도면 변경, 문제 Point 확인',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_6',
          checkDetailName: 'LF1(Reactor) 확인',
          workerName: '공정 7 작업자',
        },
      ],
    },
    {
      checkId: '7',
      checkStep: 8,
      checkName: '공정품질 검사',
      processCheckDetailList: [
        {
          checkDetailId: '7_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 8 작업자',
        },
      ],
    },
    {
      checkId: '8',
      checkStep: 9,
      checkName: '양산 검사',
      processCheckDetailList: [
        {
          checkDetailId: '8_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 9 작업자',
        },
      ],
    },
    {
      checkId: '9',
      checkStep: 10,
      checkName: '출하 검사',
      processCheckDetailList: [
        {
          checkDetailId: '9_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 10 작업자',
        },
      ],
    },
    {
      checkId: '10',
      checkStep: 11,
      checkName: '출하',
      processCheckDetailList: [
        {
          checkDetailId: '10_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 11 작업자',
        },
      ],
    },
  ]);

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-3">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0 ">DOOSAN ESM LPG</h3>
        </div>
      </div>
      <div className="contents-view">
        <table className="table lg-table mb-4 table-nowrap thead-bg-light">
          <colgroup>
            <col width="15%" />
            <col width="35%" />
            <col width="15%" />
            <col width="35%" />
          </colgroup>
          <tr>
            <th>프로젝트 명</th>
            <td>DOOSAN ESM LPG</td>
            <th>작업 시작일</th>
            <td>2021-09-17</td>
          </tr>
          <tr>
            <th>거래처</th>
            <td>삼성</td>
            <th>제품</th>
            <td>제품01</td>
          </tr>
          <tr>
            <th>제품 S/N</th>
            <td>5N2218-00027VP</td>
            <th></th>
            <td></td>
          </tr>
        </table>
        <div className="page-separator mt-5">
          <div className="page-separator__text">체크리스트</div>
        </div>
        <div className="accordion js-accordion accordion--boxed list-group-flush list-bg-light">
          {processCheckList.map((processCheck) => (
            <ProcessCompletedItem
              key={processCheck.checkId}
              processCheck={processCheck}
            />
          ))}
        </div>
      </div>
    </>
  );
}

type ProcessCompletedItemProps = {
  processCheck: any;
};

function ProcessCompletedItem({ processCheck }: ProcessCompletedItemProps) {
  const [isOpen, setOpen] = useState(false);

  return (
    <>
      <div
        className={classNames('accordion__item', {
          open: isOpen,
        })}
      >
        <a className="accordion__toggle px-4" onClick={() => setOpen(!isOpen)}>
          <h6 className="flex m-0">{processCheck.checkName}</h6>
          <div
            className="flex ml-auto px-4"
            style={{
              maxWidth: '200px',
            }}
          >
            <div
              className="progress"
              style={{
                height: '18px',
              }}
            >
              <div
                className={classNames('progress-bar', {
                  'pg-bar-success': true,
                })}
                role="progressbar"
                style={{
                  width: `100%`,
                }}
                aria-valuenow={100}
                aria-valuemin={0}
                aria-valuemax={100}
              >
                100%
              </div>
            </div>
          </div>
          <span className="accordion__toggle-icon material-icons">
            keyboard_arrow_down
          </span>
        </a>
        <Collapse in={isOpen}>
          <div
            className={classNames('accordion__menu', {
              show: isOpen,
            })}
          >
            <div className="list-group list-group-flush border-top-1">
              {processCheck.processCheckDetailList.map(
                (processCheckDetail: any) => (
                  <ProcessCompletedDetailItem
                    key={processCheckDetail.checkDetailId}
                    processCheckDetail={processCheckDetail}
                  />
                )
              )}
            </div>
          </div>
        </Collapse>
      </div>
    </>
  );
}

type ProcessCompletedDetailItemProps = {
  processCheckDetail: any;
};

function ProcessCompletedDetailItem({
  processCheckDetail,
}: ProcessCompletedDetailItemProps) {
  const [isShowModal, setShowModal] = useState(false);
  return (
    <>
      <div
        className="list-group-item px-4 d-flex"
        key={processCheckDetail.checkDetailId}
      >
        <div className="flex d-flex align-items-center mr-16pt">
          <div className="custom-control custom-checkbox checkbox-lg checkbox-rounded">
            <input
              type="checkbox"
              className="custom-control-input"
              checked={true}
            />
            <label className="custom-control-label">
              {processCheckDetail.checkDetailName}
            </label>
          </div>
        </div>
        <div className="media-right ml-auto d-flex flex-row align-items-center">
          <span className="ml-3">2021.09.23 14:00</span>
          <span className="badge-pich ml-3 font-weight-bolder">
            {processCheckDetail.workerName}
          </span>
          <a
            className="d-flex ml-3"
            data-toggle="modal"
            onClick={() => setShowModal(true)}
          >
            <span className="material-icons-outlined">history</span>
          </a>
        </div>
      </div>
      <Modal
        show={isShowModal}
        onHide={() => {
          //
        }}
        dialogClassName={'modal-custom'}
        centered={true}
      >
        <ModalHeader>
          <button
            type="button"
            className="close custom-close"
            onClick={() => setShowModal(false)}
          >
            <span>
              <MaterialIcon name={'clear'} />
            </span>
          </button>
        </ModalHeader>
        <ModalBody>
          <div className="title-group mb-4">
            <h5 className="modal-title" id="projectAddLabel">
              <td>{processCheckDetail.checkDetailName}</td>
            </h5>
          </div>
          <div className="table-cover">
            <table className="table mb-4 thead-bg-light">
              <thead>
                <tr>
                  <th>상태</th>
                  <th>날짜</th>
                  <th>작업자</th>
                </tr>
              </thead>
              <tbody>
                <tr className="text-50">
                  <td>완료</td>
                  <td>2021-09-06 16:20:12</td>
                  <td>{processCheckDetail.workerName}</td>
                </tr>
                <tr className="text-50">
                  <td>
                    <span className="text-accent">취소</span>
                  </td>
                  <td>2021-09-05 17:55:12</td>
                  <td>{processCheckDetail.workerName}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </ModalBody>
        <ModalFooter>
          <button
            type="button"
            className="btn btn-accent"
            onClick={() => setShowModal(false)}
          >
            확인
          </button>
        </ModalFooter>
      </Modal>
    </>
  );
}

export default PageCover;
