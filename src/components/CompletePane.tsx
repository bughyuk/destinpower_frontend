import React from 'react';
import { useTranslation } from 'react-i18next';

type CompletePaneProps = {
  completeTextKey?: string;
  backBtnTextKey?: string;
  onBackBtnClick?: () => void;
};

function CompletePane({
  completeTextKey,
  backBtnTextKey,
  onBackBtnClick,
}: CompletePaneProps) {
  const { t } = useTranslation();
  return (
    <div className="tit-intro">
      <em>{t(completeTextKey || 'msg_registration_complete')}</em>
      {backBtnTextKey && (
        <a onClick={onBackBtnClick} className="btn btn-outline-accent">
          {t(backBtnTextKey)}
        </a>
      )}
    </div>
  );
}

export default CompletePane;
