import React, { useEffect, useState } from 'react';
import ProjectRegisterHeader from '@/components/ProjectRegisterHeader';
import { useTranslation } from 'react-i18next';
import { useOpenLayers, useProjectRegister } from '@/modules/project/hook';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Solution,
  SOLUTION_TYPE_SMART_CONTROL,
  SOLUTION_TYPE_SMART_FACTORY,
  SOLUTION_TYPE_SMART_HOSPITAL,
  SOLUTION_TYPE_SMART_OFFICE,
  SOLUTION_TYPE_SMART_RETAIL,
  SolutionType,
  Solutions,
} from '@/modules/project/types';
import classNames from 'classnames';
import CompletePane from '@/components/CompletePane';
import { useAsync } from '@/modules/common/hook';
import { putProjectStepData } from '@/api/project';
import { useSpace } from '@/modules/space/hook';
import { useHistory } from 'react-router-dom';

function ProjectSolutionRegister() {
  const history = useHistory();
  const { handleReloadSpaceLocationOverlay } = useOpenLayers();
  const { info } = useSpace();
  const { map } = useOpenLayers();
  const [selected, setSelected] = useState<SolutionType>(
    SOLUTION_TYPE_SMART_CONTROL
  );
  const [isDone, setDone] = useState(false);
  const [state, api] = useAsync(putProjectStepData);
  const { loading, data, error } = state;
  const {
    projectInfo,
    projectProduceStep,
    handleChangeRegisterStep,
  } = useProjectRegister();

  const handleClickPrev = () => {
    if (projectProduceStep?.metaId) {
      handleChangeRegisterStep(5);
    } else {
      handleChangeRegisterStep(2);
    }
  };

  const handleSubmit = () => {
    const projectId = projectInfo.projectId;
    if (projectId) {
      api(projectId, {
        produceStepData: null,
        solutionType: selected,
      });
    }
  };

  useEffect(() => {
    const location = info.space.location;
    if (info && location && location.lng && location.lat) {
      if (map) {
        const view = map.getView();
        if (view) {
          view.setZoom(13);
          view.setCenter([location.lng, location.lat]);
        }
      }
    }
  }, [map]);

  useEffect(() => {
    if (data) {
      handleChangeRegisterStep(7);
      setDone(true);
      handleReloadSpaceLocationOverlay();
    }
  }, [data]);

  if (isDone) {
    return (
      <CompletePane
        backBtnTextKey={'text_project_list'}
        onBackBtnClick={() => history.replace('/home')}
      />
    );
  }

  return (
    <>
      <ProjectRegisterHeader />
      <Body selected={selected} onSelect={setSelected} />
      <Footer
        loading={loading}
        onClickPrev={handleClickPrev}
        onSubmit={handleSubmit}
      />
    </>
  );
}

type BodyProps = {
  selected: SolutionType;
  onSelect: (item: SolutionType) => void;
};

function Body({ selected, onSelect }: BodyProps) {
  const { t } = useTranslation();

  const items: Solutions = [
    {
      type: SOLUTION_TYPE_SMART_CONTROL,
      name: t('text_smart_control'),
      icon: ['fas', 'server'],
      description: t('msg_description_smart_control'),
    },
    {
      type: SOLUTION_TYPE_SMART_OFFICE,
      name: t('text_smart_office'),
      icon: ['fas', 'building'],
      description: t('msg_description_smart_office'),
    },
    {
      type: SOLUTION_TYPE_SMART_HOSPITAL,
      name: t('text_smart_hospital'),
      icon: ['fas', 'hospital-alt'],
      description: t('msg_description_smart_hospital'),
    },
    {
      type: SOLUTION_TYPE_SMART_FACTORY,
      name: t('text_smart_factory'),
      icon: ['fas', 'city'],
      description: t('msg_description_smart_factory'),
    },
    {
      type: SOLUTION_TYPE_SMART_RETAIL,
      name: t('text_smart_retail'),
      icon: ['fas', 'store'],
      description: t('msg_description_smart_retail'),
    },
  ];

  return (
    <div className="container-fluid">
      <div className="solution-list">
        {items.map((item) => (
          <SolutionItem
            key={item.type}
            {...item}
            selected={selected}
            onSelect={onSelect}
          />
        ))}
      </div>
    </div>
  );
}

type SolutionItemProps = Solution & BodyProps;

function SolutionItem({
  type,
  icon,
  name,
  description,
  selected,
  onSelect,
}: SolutionItemProps) {
  return (
    <div
      className={classNames('solution-card method', {
        active: type === selected,
      })}
    >
      <label className="card-body d-flex flex-column m-0">
        <div className="d-flex">
          <div className="rounded-circle w-64 h-64 d-inline-flex align-items-center justify-content-center mr-16pt">
            <FontAwesomeIcon
              icon={icon}
              style={{ fontSize: '24px', color: '#fff' }}
            />
          </div>
          <div className="flex">
            <div className="card-title mb-8pt">{name}</div>
            <p className="m-0 text-70">{description}</p>
          </div>
        </div>
        <input
          id={name}
          name="radio-stacked"
          type="radio"
          className="custom-control-input"
          value={type}
          onChange={(e) => {
            onSelect(e.target.value as SolutionType);
          }}
        />
        <div className="custom-control custom-radio d-none">
          <label htmlFor={name} className="custom-control-label"></label>
        </div>
      </label>
    </div>
  );
}

type Footer = {
  loading: boolean;
  onClickPrev: () => void;
  onSubmit: () => void;
};

function Footer({ loading, onClickPrev, onSubmit }: Footer) {
  const { t } = useTranslation();

  return (
    <div className="my-32pt">
      <div className="d-flex align-items-center justify-content-center">
        <a className="btn btn-outline-secondary mr-8pt" onClick={onClickPrev}>
          {t('text_to_the_prev')}
        </a>
        <a
          className={classNames('btn btn-outline-accent ml-0', {
            disabled: loading,
          })}
          onClick={() => {
            if (!loading) {
              onSubmit();
            }
          }}
        >
          {t('text_registration')}
        </a>
      </div>
    </div>
  );
}

export default ProjectSolutionRegister;
