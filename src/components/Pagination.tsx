import React, { useEffect } from 'react';
import classNames from 'classnames';
import MaterialIcon from '@/components/MaterialIcon';
import { CommonUtils } from '@/utils';

type PaginationProps = {
  curPage: number;
  totalCount: number;
  onPageChange?: (page: number) => void;
};

function Pagination({ curPage, totalCount, onPageChange }: PaginationProps) {
  const { pages, totalPage } = CommonUtils.calcPages(curPage, totalCount);

  const handleDirection = (weight: number) => {
    onPageChange?.call(null, curPage + weight);
  };

  useEffect(() => {
    if (totalPage) {
      if (curPage > totalPage) {
        onPageChange?.call(null, curPage - 1);
      }
    }
  }, [totalPage]);

  return (
    <ul className="pagination justify-content-center pagination-xsm m-0">
      <Prev disabled={curPage === 1} onClick={handleDirection} />
      {pages.map((page) => (
        <PageItem
          key={page}
          page={page}
          active={curPage === page}
          onPageChange={onPageChange}
        />
      ))}
      <Next disabled={curPage === totalPage} onClick={handleDirection} />
    </ul>
  );
}

type PagingDirectionProps = {
  disabled: boolean;
  onClick?: (weight: number) => void;
};

function Prev({ disabled, onClick }: PagingDirectionProps) {
  return (
    <li
      className={classNames('page-item', {
        disabled: disabled,
      })}
      onClick={() => {
        if (!disabled) {
          onClick?.call(null, -1);
        }
      }}
    >
      <a className="page-link">
        <MaterialIcon name={'chevron_left'} />
        <span>Prev</span>
      </a>
    </li>
  );
}

function Next({ disabled, onClick }: PagingDirectionProps) {
  return (
    <li
      className={classNames('page-item', {
        disabled: disabled,
      })}
      onClick={() => {
        if (!disabled) {
          onClick?.call(null, 1);
        }
      }}
    >
      <a className="page-link">
        <span>Next</span>
        <MaterialIcon name={'chevron_right'} />
      </a>
    </li>
  );
}

type PageItemProps = {
  page: number;
  active?: boolean;
  onPageChange?: (page: number) => void;
};

function PageItem({ page, active, onPageChange }: PageItemProps) {
  return (
    <li
      className={classNames('page-item', {
        active: active,
      })}
    >
      <a
        className="page-link"
        onClick={() => {
          onPageChange?.call(null, page);
        }}
      >
        <span>{page}</span>
      </a>
    </li>
  );
}

export default Pagination;
