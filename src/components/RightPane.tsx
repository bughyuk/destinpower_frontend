import React, { useEffect, useRef, useState } from 'react';
import { useRightPane } from '@/modules/setup/hook';
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY,
  RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS,
  RIGHT_PANE_STATISTICS,
} from '@/modules/setup/types';
import RightStatistics from '@/components/RightStatistics';
import RightPhysicalDistributionHistory from '@/components/RightPhysicalDistributionHistory';
import { useTranslation } from 'react-i18next';
import moment from 'moment';
import RightPhysicalDistributionStatistics from '@/components/RightPhysicalDistributionStatistics';

function RightPane() {
  const { t } = useTranslation();
  const [referenceDate, setReferenceDate] = useState(
    moment().format('YYYY-MM-DD HH:mm:ss')
  );
  const { show, view, handleChangeShow } = useRightPane();
  const perfectScrollbarRef = useRef<PerfectScrollbar>(null);

  useEffect(() => {
    return () => {
      handleChangeShow(false, undefined);
    };
  }, []);

  useEffect(() => {
    if (show) {
      setReferenceDate(moment().format('YYYY-MM-DD HH:mm:ss'));
      const perfectScrollbar = perfectScrollbarRef.current;
      if (perfectScrollbar) {
        const container = (perfectScrollbar as any)._container;
        container.scrollTop = 0;
      }
    }
  }, [show, view]);

  let title = '';
  if (view === RIGHT_PANE_STATISTICS) {
    title = t('text_factory');
  } else if (view === RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS) {
    title = t('text_realtime_monitoring');
  } else if (view === RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY) {
    title = t('text_history');
  }

  return (
    <div
      className={classNames('cd-panel cd-panel--from-right js-cd-panel-main', {
        'cd-panel--is-visible ': show,
      })}
    >
      <a
        className={classNames({
          'cd-panel__close js-cd-close': show,
          'cd-panel__show js-cd-panel-trigger': !show,
        })}
        onClick={() => handleChangeShow(!show)}
      ></a>
      <div className="cd-panel__container">
        <PerfectScrollbar
          className="cd-panel__content p-4"
          ref={perfectScrollbarRef}
        >
          {view !== undefined && (
            <div className="d-flex flex-row align-items-center mb-0 pb-4">
              <div className="flex">
                <h3 className="d-flex align-items-center mb-0">{title}</h3>
                <p className="text-50 m-0">
                  {t('text_as_of_reference_date', {
                    date: referenceDate,
                  })}
                </p>
              </div>
            </div>
          )}
          {view === RIGHT_PANE_STATISTICS && <RightStatistics />}
          {view === RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS && (
            <RightPhysicalDistributionStatistics />
          )}
          {view === RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY && (
            <RightPhysicalDistributionHistory />
          )}
        </PerfectScrollbar>
      </div>
    </div>
  );
}

export default RightPane;
