import React from 'react';
import { useTranslation } from 'react-i18next';

type EmptyPaneProps = {
  textKey: string;
  descriptionKey: string;
  btnTextKey?: string;
  onClick?: () => void;
};

function EmptyPane({
  textKey,
  descriptionKey,
  btnTextKey,
  onClick,
}: EmptyPaneProps) {
  const { t } = useTranslation();

  return (
    <div className="tit-intro">
      <em>{t(textKey)}</em>
      <p>{t(descriptionKey)}</p>
      {btnTextKey && (
        <a onClick={onClick} className="btn btn-accent">
          {t(btnTextKey)}
        </a>
      )}
    </div>
  );
}

export default EmptyPane;
