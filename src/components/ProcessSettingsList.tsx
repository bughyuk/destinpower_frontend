import React, { ReactElement, useEffect } from 'react';
import { PANE_STATUS_CATEGORY, PaneStatus } from '@/utils/constants/common';
import {
  Category,
  PROCESS_CATEGORY_CLIENT_MANAGEMENT,
  PROCESS_CATEGORY_FACTORY_MANAGEMENT,
  PROCESS_CATEGORY_PRODUCT_LINE_MANAGEMENT,
  PROCESS_CATEGORY_PRODUCT_MANAGEMENT,
} from '@/modules/physical_distribution/types';
import { useTranslation } from 'react-i18next';
import { useRightPane } from '@/modules/setup/hook';
import { RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY } from '@/modules/setup/types';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import PhysicalDistributionClient from '@/components/PhysicalDistributionClient';
import ProcessProductLine from '@/components/ProcessProductLine';
import ProcessFactory from '@/components/ProcessFactory';
import ProcessProductType from '@/components/ProcessProductType';

export type PhysicalDistributionListContentProps = {
  categoryIdx?: Category;
  title?: string;
  onClickBack?: () => void;
};

type PhysicalDistributionListProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function ProcessSettingsList({
  onChangeStatus,
}: PhysicalDistributionListProps) {
  const { t } = useTranslation();
  const { categoryIdx } = usePhysicalDistributionCategory();
  const { handleChangeShow } = useRightPane();

  useEffect(() => {
    handleChangeShow(true, RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY);
  }, []);

  let title = '';
  let content: ReactElement = <></>;
  switch (categoryIdx) {
    case PROCESS_CATEGORY_FACTORY_MANAGEMENT:
      title = '공장 관리';
      content = <ProcessFactory />;
      break;
    case PROCESS_CATEGORY_PRODUCT_MANAGEMENT:
      title = '제품타입 관리';
      content = <ProcessProductType />;
      break;
    case PROCESS_CATEGORY_PRODUCT_LINE_MANAGEMENT:
      title = '제품군 관리';
      content = <ProcessProductLine />;
      break;
    case PROCESS_CATEGORY_CLIENT_MANAGEMENT:
      title = t('text_client_management');
      content = <PhysicalDistributionClient />;
      break;
  }

  const handleClickBack = () => {
    onChangeStatus(PANE_STATUS_CATEGORY);
  };

  return (
    <>
      {React.cloneElement<PhysicalDistributionListContentProps>(content, {
        categoryIdx,
        title,
        onClickBack: handleClickBack,
      })}
    </>
  );
}

export default ProcessSettingsList;
