import React, {
  CSSProperties,
  useEffect,
  useLayoutEffect,
  useState,
} from 'react';
import MapContainer from '@/components/MapContainer';
import BottomToolbar from '@/components/BottomToolbar';
import {
  useControlProject,
  useControlSpace,
  useOpenLayers,
} from '@/modules/map/hook';
import { Config } from '@/config';
import LeftWidePane from '@/components/LeftWidePane';
import MapLegend from '@/components/MapLegend';
import FloatPane from '@/components/FloatPane';
import RightPane from '@/components/RightPane';
import LogisticsAlert from '@/components/LogisticsAlert';
import SearchNavbar from '@/components/SearchNavbar';
import ProcessFilterNavbar from '@/components/ProcessFilterNavbar';
import StatusNavbar from '@/components/StatusNavbar';
import TopNavbar from '@/components/TopNavbar';
import FilterNavbar from '@/components/FilterNavbar';
import PageCover from '@/components/PageCover';
import { useActiveMenu } from '@/modules/setup/hook';
import { MENU_IDX_PROCESS_DASHBOARD } from '@/utils/constants/common';

function ContentContainer() {
  const { map, floorPlan } = useOpenLayers();
  const { project: controlProject } = useControlProject();
  const { space: controlSpace } = useControlSpace();
  const [activeVisibleFloorPlan, setActiveVisibleFloorPlan] = useState(false);
  const { menuIdx } = useActiveMenu();

  const handleClickLocation = () => {
    if (controlSpace.spaceMappingId) {
      const findSpace = controlProject.spaceList.find(
        (space) => space.mappingId === controlSpace.spaceMappingId
      );
      if (findSpace) {
        map?.getView().animate({
          center: [findSpace.longitude, findSpace.latitude],
          zoom: 13,
        });
      }
    }
  };

  const handleClickFloorPlan = () => {
    setActiveVisibleFloorPlan(!activeVisibleFloorPlan);
  };

  const handleDrawFloorPlan = () => {
    if (typeof floorPlan.remove !== 'undefined') {
      floorPlan.remove();
    }

    if (controlSpace.spaceMappingId) {
      const findSpace = controlProject.spaceList.find(
        (space) => space.mappingId === controlSpace.spaceMappingId
      );

      if (findSpace) {
        const fineFloors = findSpace.floorsList.find(
          (floors) => floors.id === controlSpace.floorsMapId
        );

        if (
          fineFloors &&
          fineFloors.filename &&
          floorPlan.draw &&
          activeVisibleFloorPlan
        ) {
          floorPlan.draw({
            url: `${Config.space_api.uri}/bprint/${fineFloors.filename}`,
            imageCenter: [Number(fineFloors.cx), Number(fineFloors.cy)],
            imageScale: [Number(fineFloors.scalex), Number(fineFloors.scaley)],
            imageRotate: Number(fineFloors.rotation),
          });
        }
      }
    }
  };

  useEffect(() => {
    handleDrawFloorPlan();
  }, [controlProject, controlSpace, activeVisibleFloorPlan]);

  const handleClickNavbarToggler = () => {
    const leftSidebarElement = document.getElementById('left-sidebar');
    if (leftSidebarElement) {
      leftSidebarElement.dataset['opened'] = '';
    }
    const scrimElement = document.getElementById('scrim');
    if (scrimElement) {
      scrimElement.classList.add('mdk-drawer__scrim');
    }
  };

  const [contentStyle, setContentStyle] = useState<CSSProperties>({});

  useEffect(() => {
    if (menuIdx === MENU_IDX_PROCESS_DASHBOARD) {
      setContentStyle({
        position: 'absolute',
        left: window.innerWidth <= 1441 ? '0px' : '64px',
        width: window.innerWidth <= 1441 ? '100%' : 'calc(100% - 64px)',
      });
    } else {
      setContentStyle({});
    }
  }, [menuIdx]);

  return (
    <div
      className="mdk-drawer-layout__content page-content"
      style={{
        ...contentStyle,
        transform: 'translate3d(0px, 0px, 0px)',
      }}
    >
      <TopNavbar>
        <button
          className="navbar-toggler w-auto mr-16pt d-block d-xxl-none rounded-0"
          type="button"
          data-toggle="sidebar"
          onClick={handleClickNavbarToggler}
        >
          <span className="material-icons text-iden">menu</span>
        </button>
        <FilterNavbar />
        <StatusNavbar addOns={true} />
      </TopNavbar>
      <PageCover />
      <LogisticsAlert />
      {/*<LeftWidePane />*/}
      {/*<FloatPane />*/}
    </div>
  );
}

export default ContentContainer;
