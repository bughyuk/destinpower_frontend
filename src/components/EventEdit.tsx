import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDropdown } from '@/modules/common';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import MaterialIcon from '@/components/MaterialIcon';
import { Dropdown } from 'react-bootstrap';
import {
  FUNCTION_SCOPE_AREA,
  FUNCTION_SCOPE_CIRCLE,
  FUNCTION_SCOPE_POLYGON,
  GEOMETRY_TARGET_TYPE_INNER,
  GEOMETRY_TARGET_TYPE_OUTER,
  GEOMETRY_TYPE_CIRCLE,
  GEOMETRY_TYPE_POLYGON,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import {
  useAdditionalLocation,
  useControlEvent,
  useOpenLayers,
} from '@/modules/map/hook';
import { CommonUtils } from '@/utils';
import FormLabel from '@/components/FormLabel';
import FormGroup from '@/components/FormGroup';
import Flatpickr from 'react-flatpickr';
import ReactQuill from 'react-quill';
import moment from 'moment';
import CompletePane from '@/components/CompletePane';
import { useLeftPaneContainer, useSetupEventsFlag } from '@/modules/setup/hook';
import { fetchEvent, putEvent, RequestPutEvent } from '@/api/event';
import { postImage } from '@/api/common';
import { ControlEventImage } from '@/modules/event/types';
import { Feature } from 'ol';
import { Circle } from 'ol/geom';
import { WKT } from 'ol/format';
import FormRow from '@/components/FormRow';
import InvalidAlert from '@/components/InvalidAlert';

type EventEditProps = {
  eventId: string;
  onStatusChange: (status: PaneStatus) => void;
};

type EventEditData = {
  title: string;
  period: string[];
  subject: string;
  content: string;
  imgId: string;
};

const wkt = new WKT();
function EventEdit({ eventId, onStatusChange }: EventEditProps) {
  const { handleReloadActivationEvents } = useControlEvent();
  const { map, draw } = useOpenLayers();
  const {
    locationData,
    handleSetLocation,
    handleResetLocation,
  } = useAdditionalLocation();
  const { updateScroll } = useLeftPaneContainer();
  const { handleChangeClick } = useSetupEventsFlag();
  const [done, setDone] = useState(false);
  const [isValid, setValid] = useState(false);
  const [isEditPossible, setEditPossible] = useState(false);
  const [inputs, setInputs] = useState<EventEditData>({
    title: '',
    period: [],
    subject: '',
    content: '',
    imgId: '',
  });

  const [image, setImage] = useState<ControlEventImage>({
    imgId: '',
    originalFileName: '',
    fileUrl: '',
  });

  useEffect(() => {
    if (!CommonUtils.isEmptyObject(locationData)) {
      setEditPossible(true);
    } else {
      setEditPossible(false);
    }
  }, [locationData]);

  useEffect(() => {
    handleChangeClick(false);
    return () => {
      handleChangeClick(true);
      draw.source?.clear();
      handleResetLocation();
    };
  }, []);

  useEffect(() => {
    return () => {
      if (draw.interaction) {
        map?.removeInteraction(draw.interaction);
      }
    };
  }, [draw.interaction]);

  const handleFetchEvent = async () => {
    const data = await fetchEvent(eventId);

    if (data) {
      setInputs({
        title: data.eventTitle,
        period: [data.startDate, data.endDate],
        subject: data.eventSubject,
        content: data.eventContent,
        imgId: '',
      });

      if (data.imgId) {
        setImage({
          imgId: data.imgId,
          originalFileName: data.originalFileName,
          fileUrl: data.fileUrl,
        });
      }

      let feature: Feature | null = null;
      if (data.geomType === GEOMETRY_TYPE_POLYGON) {
        if (data.geom) {
          feature = wkt.readFeature(data.geom);
          handleSetLocation(feature, {
            geom: data.geom,
          });
        }
      } else if (data.geomType === GEOMETRY_TYPE_CIRCLE) {
        if (data.lng && data.lat && data.radius) {
          feature = new Feature(new Circle([data.lng, data.lat], data.radius));
          handleSetLocation(feature, {
            lng: data.lng,
            lat: data.lat,
            radius: data.radius,
          });
        }
      } else if (data.areaId) {
        if (data.areaGeom) {
          feature = wkt.readFeature(data.areaGeom);
          handleSetLocation(feature, {
            areaId: data.areaId,
            areaName: data.areaName,
          });
        }
      }

      if (feature) {
        draw.source?.addFeature(feature);
      }
    }
  };

  useEffect(() => {
    handleFetchEvent();
  }, [eventId]);

  const handleEditDone = async () => {
    let imgId = '';

    if (image.imgId) {
      imgId = image.imgId;
    } else {
      imgId = inputs.imgId;
    }

    const requestPutEvent: RequestPutEvent = {
      startDate: moment(inputs.period[0]).format('yyyy-MM-DDTHH:mm:ss'),
      endDate: moment(inputs.period[1]).format('yyyy-MM-DDTHH:mm:ss'),
      eventTitle: inputs.title,
      eventSubject: inputs.subject,
      eventContent: inputs.content,
      imgId,
    };

    if (locationData.geom) {
      requestPutEvent.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPutEvent.geomType = GEOMETRY_TYPE_POLYGON;
      requestPutEvent.geom = locationData.geom;
    }

    if (locationData.lng && locationData.lat && locationData.radius) {
      requestPutEvent.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPutEvent.geomType = GEOMETRY_TYPE_CIRCLE;
      requestPutEvent.lng = locationData.lng;
      requestPutEvent.lat = locationData.lat;
      requestPutEvent.radius = locationData.radius;
    }

    if (locationData.areaId) {
      requestPutEvent.targetGeomType = GEOMETRY_TARGET_TYPE_OUTER;
      requestPutEvent.outerKey = locationData.areaId;
    }

    const data = await putEvent(eventId, requestPutEvent);
    if (data) {
      draw.source?.clear();
      handleReloadActivationEvents();
      setDone(true);
    }
  };

  useEffect(() => {
    if (
      inputs.title &&
      inputs.period.length > 1 &&
      inputs.subject &&
      inputs.content
    ) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [inputs]);

  useEffect(() => {
    updateScroll();
  }, [done]);

  if (done) {
    return (
      <CompletePane
        completeTextKey={'msg_edit_complete'}
        backBtnTextKey={'text_event_list'}
        onBackBtnClick={() => onStatusChange(PANE_STATUS_LIST)}
      />
    );
  }

  let content: ReactElement;
  if (!CommonUtils.isEmptyObject(locationData)) {
    content = (
      <Body
        data={inputs}
        image={image}
        onChangeData={setInputs}
        onChangeImage={setImage}
      />
    );
  } else {
    content = <Guide />;
  }

  return (
    <>
      <Header />
      <div className="container-fluid">{content}</div>
      <Footer
        editPossible={isEditPossible}
        onConfirmBtnClick={handleEditDone}
        onCancelBtnClick={onStatusChange}
        valid={isValid}
      />
    </>
  );
}

function Header() {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const { handleResetLocation } = useAdditionalLocation();
  const {
    handleSelectArea,
    handleDrawCircle,
    handleDrawPolygon,
  } = useOpenLayers();

  const handleFunctionSelect = (eventKey: string | null) => {
    handleResetLocation();
    switch (eventKey) {
      case FUNCTION_SCOPE_AREA:
        handleSelectArea();
        break;
      case FUNCTION_SCOPE_CIRCLE:
        handleDrawCircle();
        break;
      case FUNCTION_SCOPE_POLYGON:
        handleDrawPolygon();
        break;
    }
  };

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{t('text_event_edit')}</h3>
        </div>
      </div>
      <div className="btn-group">
        <Dropdown onToggle={handleToggle} onSelect={handleFunctionSelect}>
          <DropdownToggle as={'a'} className="btn btn-outline-accent">
            {t('text_selection_function')}
          </DropdownToggle>
          <DropdownMenu align={'right'} ref={dropdown}>
            <Dropdown.Header>
              <strong>{t('text_specify_range')}</strong>
            </Dropdown.Header>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_AREA}>
              <MaterialIcon name={'highlight_alt'} />{' '}
              {t('text_selection_structure')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_CIRCLE}>
              <MaterialIcon name={'adjust'} /> {t('text_selection_range')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_POLYGON}>
              <MaterialIcon name={'timeline'} /> {t('text_draw_line')}
            </Dropdown.Item>
          </DropdownMenu>
        </Dropdown>
      </div>
    </div>
  );
}

function Guide() {
  const { t } = useTranslation();
  return <em className="none-list">{t('msg_selection_function')}</em>;
}

type BodyProps = {
  data: EventEditData;
  image: ControlEventImage;
  onChangeData: (data: EventEditData) => void;
  onChangeImage: (image: ControlEventImage) => void;
};

function Body({ data, image, onChangeData, onChangeImage }: BodyProps) {
  const { t } = useTranslation();
  const fileInput = useRef<HTMLInputElement>(null);

  useEffect(() => {
    return () =>
      onChangeData({
        ...data,
        imgId: '',
      });
  }, []);

  const handlePostImage = async (file: File) => {
    const imgId = await postImage(file);

    onChangeData({
      ...data,
      imgId,
    });
  };

  const handleCancelImage = () => {
    const current = fileInput.current;
    if (current) {
      current.value = '';
    }

    onChangeData({
      ...data,
      imgId: '',
    });
  };

  return (
    <>
      <FormGroup>
        <FormLabel
          textKey={'text_event_name'}
          className={'mb-0'}
          htmlFor={'title'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="title"
          value={data.title}
          onChange={(e) =>
            onChangeData({
              ...data,
              [e.target.id]: e.target.value,
            })
          }
          placeholder={t('place_holder_event_name')}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_period'}
          className={'mb-0'}
          htmlFor={'period'}
          essential={true}
        />
        <Flatpickr
          className="form-control"
          options={{
            mode: 'range',
          }}
          value={data.period}
          onChange={(dates) =>
            onChangeData({
              ...data,
              period: dates.map((date) => moment(date).format('YYYY-MM-DD')),
            })
          }
          id="period"
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_title'}
          className={'mb-0'}
          essential={true}
          htmlFor={'subject'}
        />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_title')}
          id={'subject'}
          value={data.subject}
          onChange={(e) =>
            onChangeData({
              ...data,
              [e.target.id]: e.target.value,
            })
          }
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_content'}
          essential={true}
          htmlFor={'content'}
        />
        <div style={{ height: '192px' }}>
          <ReactQuill
            id="content"
            style={{ height: '150px' }}
            placeholder={t('place_holder_content')}
            modules={{
              toolbar: [
                ['bold', 'italic'],
                ['link', 'blockquote', 'code', 'image'],
                [{ list: 'ordered' }, { list: 'bullet' }],
              ],
            }}
            value={data.content}
            onChange={(content, delta, source, editor) => {
              if (!editor.getText().trim().length) {
                content = '';
              }

              onChangeData({
                ...data,
                content,
              });
            }}
          />
        </div>
      </FormGroup>
      {!image.imgId && (
        <FormGroup>
          <FormLabel textKey={'text_file_registration'} />
          <input
            className="form-control"
            type="file"
            accept={'image/*'}
            onChange={(e) => {
              if (e.target.files && e.target.files.length) {
                handlePostImage(e.target.files[0]);
              } else {
                onChangeData({
                  ...data,
                  imgId: '',
                });
              }
            }}
            ref={fileInput}
          />
          {data.imgId && (
            <a className="file-del" onClick={handleCancelImage}>
              <MaterialIcon name={'close'} />
            </a>
          )}
        </FormGroup>
      )}
      {image.imgId && (
        <FormGroup>
          <FormLabel textKey={'text_attached_file'} />
          <FormRow className="align-items-center">
            <div className="col-auto">
              <div className="avatar">
                <img src={image.fileUrl} className="avatar-img rounded" />
              </div>
            </div>
            <div className="col">
              <div className="font-weight-bold">{image.originalFileName}</div>
            </div>
            <div className="col-auto">
              <a
                className="text-muted-light"
                onClick={() => {
                  onChangeImage({
                    imgId: '',
                    originalFileName: '',
                    fileUrl: '',
                  });
                }}
              >
                <MaterialIcon name={'close'} />
              </a>
            </div>
          </FormRow>
        </FormGroup>
      )}
    </>
  );
}

type FooterProps = {
  onCancelBtnClick?: (status: PaneStatus) => void;
  onConfirmBtnClick?: () => void;
  editPossible?: boolean;
  valid: boolean;
};

function Footer({
  onConfirmBtnClick,
  onCancelBtnClick,
  editPossible = false,
  valid,
}: FooterProps) {
  const { t } = useTranslation();
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (!editPossible || valid) {
      setShowInvalidMessage(false);
    }
  }, [editPossible, valid]);

  return (
    <>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            onClick={() => {
              onCancelBtnClick?.call(null, PANE_STATUS_LIST);
            }}
            className="btn btn-outline-secondary mr-8pt"
          >
            {t('text_to_cancel')}
          </a>
          {editPossible && (
            <a
              className={'btn btn-outline-accent ml-0'}
              onClick={() => {
                if (valid) {
                  setShowInvalidMessage(false);
                  onConfirmBtnClick?.call(null);
                } else {
                  setShowInvalidMessage(true);
                }
              }}
            >
              {t('text_do_edit')}
            </a>
          )}
        </div>
      </div>
    </>
  );
}

export default EventEdit;
