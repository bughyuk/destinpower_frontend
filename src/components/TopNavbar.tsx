import React, { ReactNode } from 'react';

type TopNavbarProps = {
  children?: ReactNode;
};

function TopNavbar({ children }: TopNavbarProps) {
  return <div className="navbar navbar-expand navbar-light">{children}</div>;
}

export default TopNavbar;
