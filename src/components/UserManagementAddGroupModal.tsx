import React, { useEffect, useState } from 'react';
import { ModalProps, ReactSelectOption } from '@/modules/common';
import { Modal, ModalBody, ModalFooter, ModalTitle } from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/ModalHeader';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import SelectMulti from '@/components/SelectMulti';
import { Group } from '@/modules/group/types';

export type UserManagementAddGroupData = {
  userId: string;
  invitationSeq: number;
};

type UserManagementAddGroupModalProps = ModalProps & {
  groupList: Group[];
  onSubmit: (selectedValue: number[]) => void;
};

function UserManagementAddGroupModal({
  show,
  groupList,
  onHide,
  onSubmit,
}: UserManagementAddGroupModalProps) {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [options, setOptions] = useState<ReactSelectOption[]>([]);
  const [selectedValue, setSelectedValue] = useState<number[]>([]);

  const handleConfirm = async () => {
    if (selectedValue.length > 0) {
      onSubmit(selectedValue);
      onHide();
    }
  };

  useEffect(() => {
    const newOptions: ReactSelectOption[] = [];
    groupList.forEach((group) => {
      if (!group.deleteFlag) {
        newOptions.push({
          value: group.groupSeq.toString(),
          label: group.groupName,
          name: group.groupName,
        });
      }
    });
    setOptions(newOptions);
  }, [groupList]);

  return (
    <Modal
      show={show}
      onExited={() => {
        setLoading(false);
        setSelectedValue([]);
      }}
      onHide={() => {
        //
      }}
      centered={true}
      dialogClassName={'modal-custom'}
    >
      <ModalHeader>
        <button type="button" className="close custom-close" onClick={onHide}>
          <span>
            <MaterialIcon name={'clear'} />
          </span>
        </button>
      </ModalHeader>
      <ModalBody>
        <div className="title-group mb-4">
          <ModalTitle as={'h5'}>{t('text_add_to_group')}</ModalTitle>
          <p className="m-0">{t('msg_user_add_group_description')}</p>
        </div>
        <div className="form-group mb-5">
          <label className="form-label">{t('text_group_search')}</label>
          <SelectMulti
            options={options}
            onChangeValue={(changeValue) => {
              setSelectedValue(changeValue.map((value) => Number(value)));
            }}
          />
        </div>
      </ModalBody>
      <ModalFooter>
        <button type="button" className="btn btn-link" onClick={onHide}>
          {t('text_cancel')}
        </button>
        <button
          type="button"
          className={classNames('btn', {
            'is-loading is-loading-sm': loading,
            'btn-light': selectedValue.length === 0,
            'btn-accent': selectedValue.length > 0,
          })}
          disabled={selectedValue.length === 0}
          onClick={handleConfirm}
        >
          {t('text_confirm')}
        </button>
      </ModalFooter>
    </Modal>
  );
}

export default UserManagementAddGroupModal;
