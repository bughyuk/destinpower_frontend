import React, { useEffect, useState } from 'react';
import SpaceListHeader from '@/components/SpaceListHeader';
import SpaceListTable from '@/components/SpaceListTable';
import { useTranslation } from 'react-i18next';
import { useProjectRegister } from '@/modules/project/hook';

function ProjectCustomSpaceTab() {
  const [count, setCount] = useState(0);
  const [searchKeyword, setSearchKeyword] = useState('');
  const [metaIds, setMetaIds] = useState<string[]>([]);
  const {
    projectInfo,
    handleSetProjectInfo,
    connectedMetaIds,
  } = useProjectRegister();

  const handleSubmit = () => {
    const ids = projectInfo.metaIds;
    let saveMetaIds: string[] = [];
    if (ids) {
      saveMetaIds = ids;
    }
    metaIds.forEach((metaId) => {
      if (!ids?.includes(metaId)) {
        saveMetaIds.push(metaId);
      }
    });
    handleSetProjectInfo({
      ...projectInfo,
      metaIds: saveMetaIds,
    });
  };

  useEffect(() => {
    const ids = projectInfo.metaIds;
    const saveMetaIds: string[] = [];

    metaIds.forEach((metaId) => {
      if (ids?.includes(metaId)) {
        saveMetaIds.push(metaId);
      }
    });

    setMetaIds(saveMetaIds);
  }, [projectInfo]);

  useEffect(() => {
    if (connectedMetaIds.length) {
      setMetaIds(connectedMetaIds);
    }
  }, [connectedMetaIds]);

  const handleChangeCheck = (checked: boolean, spaceId: string) => {
    if (checked) {
      if (!metaIds.includes(spaceId)) {
        setMetaIds(metaIds.concat(spaceId));
      }
    } else {
      setMetaIds(metaIds.filter((metaId) => metaId !== spaceId));
    }
  };

  const handleChangeCheckAll = (checked: boolean, spaceIds: string[]) => {
    let processMetaIds = [...metaIds];
    spaceIds.forEach((spaceId) => {
      if (checked) {
        if (!processMetaIds.includes(spaceId)) {
          processMetaIds.push(spaceId);
        }
      } else {
        processMetaIds = processMetaIds.filter((metaId) => {
          return metaId !== spaceId;
        });
      }
    });

    setMetaIds(processMetaIds);
  };

  return (
    <>
      <SpaceListHeader
        count={count}
        possibleRegister={false}
        onSubmit={setSearchKeyword}
      />
      <SpaceListTable
        showCheckbox={true}
        showOptions={false}
        showPagination={true}
        routeDetail={false}
        searchKeyword={searchKeyword}
        onChangeCount={setCount}
        initCheckList={metaIds}
        onChangeCheck={handleChangeCheck}
        onChangeCheckAll={handleChangeCheckAll}
      />
      <Footer onSubmit={handleSubmit} />
    </>
  );
}

type FooterProps = {
  onSubmit: () => void;
};

function Footer({ onSubmit }: FooterProps) {
  const { t } = useTranslation();
  return (
    <div className="my-32pt">
      <div className="d-flex align-items-center justify-content-center">
        <a className="btn btn-outline-accent" onClick={onSubmit}>
          {t('text_selection_space_connect_to_project')}
        </a>
      </div>
    </div>
  );
}

export default ProjectCustomSpaceTab;
