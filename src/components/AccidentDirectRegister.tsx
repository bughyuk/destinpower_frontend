import React, { useEffect, useState } from 'react';
import {
  AccidentFunction,
  FUNCTION_LOCATION_EMERGENCY,
  FUNCTION_LOCATION_FIRE,
  FUNCTION_LOCATION_INFECTIOUS_DISEASE,
  FUNCTION_LOCATION_RESCUE,
  FUNCTION_SCOPE_AREA,
  FUNCTION_SCOPE_CIRCLE,
  FUNCTION_SCOPE_POLYGON,
  GEOMETRY_TARGET_TYPE_INNER,
  GEOMETRY_TYPE_CIRCLE,
  PANE_STATUS_DETAIL,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import ListHeader from '@/components/ListHeader';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import {
  useAdditionalLocation,
  useControlAccident,
  useControlSpace,
  useControlProject,
  useOpenLayers,
  useRealtimeUser,
} from '@/modules/map/hook';
import { CommonUtils } from '@/utils';
import classNames from 'classnames';
import {
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
  AccidentCategory,
} from '@/modules/accident/types';
import { postAccident, RequestPostAccident } from '@/api/accident';
import { Circle } from 'ol/geom';
import * as turf from '@turf/turf';
import { postMessages } from '@/api/message';
import { useAccidentMessage } from '@/modules/accident/hook';
import Preloader from '@/components/Preloader';
import EmptyPane from '@/components/EmptyPane';
import { useSetupEventsFlag } from '@/modules/setup/hook';

type AccidentDirectRegisterProps = {
  onStatusChange: (status: PaneStatus) => void;
  onChangeEventId: (eventId: string) => void;
};

function AccidentDirectRegister({
  onStatusChange,
  onChangeEventId,
}: AccidentDirectRegisterProps) {
  const { t } = useTranslation();
  const {
    map,
    draw,
    handleDrawAccident,
    handleDrawPolygon,
    handleDrawCircle,
    handleSelectArea,
  } = useOpenLayers();
  const {
    locationFeature,
    locationData,
    handleResetLocation,
  } = useAdditionalLocation();
  const [activeAccidentFunction, setActiveAccidentFunction] = useState<
    AccidentFunction | ''
  >('');
  const { handleReloadActivationAccidents } = useControlAccident();
  const { handleGetAccidentMessage } = useAccidentMessage();
  const { space } = useControlSpace();
  const { project } = useControlProject();
  const { users } = useRealtimeUser();
  const { handleChangeClick } = useSetupEventsFlag();
  const [loading, setLoading] = useState(false);

  const handleRouteAccidentList = () => {
    onStatusChange(PANE_STATUS_LIST);
  };

  useEffect(() => {
    draw.source?.clear();
    handleResetLocation();
    if (draw.interaction) {
      map?.removeInteraction(draw.interaction);
    }
    setActiveAccidentFunction('');
  }, [space]);

  useEffect(() => {
    if (draw.interaction) {
      handleChangeClick(false);
    } else {
      handleChangeClick(true);
    }
    return () => {
      handleChangeClick(true);
      if (draw.interaction) {
        map?.removeInteraction(draw.interaction);
      }
    };
  }, [draw.interaction]);

  useEffect(() => {
    if (!CommonUtils.isEmptyObject(locationData)) {
      switch (activeAccidentFunction) {
        case FUNCTION_SCOPE_AREA:
        case FUNCTION_SCOPE_CIRCLE:
        case FUNCTION_SCOPE_POLYGON:
          onStatusChange(PANE_STATUS_REGISTER);
          break;
        case FUNCTION_LOCATION_EMERGENCY:
        case FUNCTION_LOCATION_FIRE:
        case FUNCTION_LOCATION_RESCUE:
        case FUNCTION_LOCATION_INFECTIOUS_DISEASE:
          handleAccidentRegister();
          break;
      }
    }
  }, [locationData]);

  const handleAccidentRegister = async () => {
    setLoading(true);
    let eventTitle = '';
    let eventContent = '';
    let category: AccidentCategory | undefined;
    if (activeAccidentFunction === FUNCTION_LOCATION_EMERGENCY) {
      eventTitle = t('text_emergency');
      eventContent = t('text_emergency');
      category = ACCIDENT_EMERGENCY;
    } else if (activeAccidentFunction === FUNCTION_LOCATION_FIRE) {
      eventTitle = t('text_fire');
      eventContent = t('text_fire');
      category = ACCIDENT_FIRE;
    } else if (activeAccidentFunction === FUNCTION_LOCATION_RESCUE) {
      eventTitle = t('text_rescue');
      eventContent = t('text_rescue');
      category = ACCIDENT_RESCUE;
    } else if (
      activeAccidentFunction === FUNCTION_LOCATION_INFECTIOUS_DISEASE
    ) {
      eventTitle = t('text_infectious_disease');
      eventContent = t('text_infectious_disease');
      category = ACCIDENT_INFECTIOUS_DISEASE;
    }

    if (category) {
      const requestPostAccident: RequestPostAccident = {
        activeFlag: true,
        projectId: project.id,
        eventTitle,
        eventContent,
        targetMappingId: space.spaceMappingId,
        targetMapId: space.floorsMapId,
        eventDetailCategory: category,
      };

      requestPostAccident.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPostAccident.geomType = GEOMETRY_TYPE_CIRCLE;
      requestPostAccident.lng = locationData.lng;
      requestPostAccident.lat = locationData.lat;
      requestPostAccident.radius = locationData.radius;

      const eventId = await postAccident(requestPostAccident);
      if (eventId) {
        const geometry = locationFeature?.getGeometry();
        if (geometry) {
          const targetIds: string[] = [];
          const circleGeometry = geometry as Circle;
          const center = circleGeometry.getCenter();
          const radius = circleGeometry.getRadius();

          const accidentPoint = turf.toWgs84(turf.point(center));

          users.forEach((user) => {
            const userPoint = turf.toWgs84(turf.point([user.lng, user.lat]));

            const buffer = turf.buffer(accidentPoint, radius, {
              units: 'meters',
            });
            const contain = turf.booleanContains(buffer, userPoint);

            if (contain) {
              targetIds.push(user.accessKey);
            }
          });

          if (targetIds.length > 0) {
            const { title, content } = handleGetAccidentMessage(category);

            await postMessages({
              projectId: project.id,
              parentId: eventId,
              parentType: 'message',
              messageTitle: title,
              messageContent: content,
              imgId: '',
              targetIds,
            });
          }
        }

        handleResetLocation();
        handleReloadActivationAccidents();
        onChangeEventId(eventId);
        onStatusChange(PANE_STATUS_DETAIL);
      }
    }
  };

  const handleActiveFunction = (
    accidentFunction: AccidentFunction,
    accidentCategory?: AccidentCategory
  ) => {
    if (accidentFunction === activeAccidentFunction) {
      if (draw.interaction) {
        map?.removeInteraction(draw.interaction);
      }
      setActiveAccidentFunction('');
    } else {
      handleResetLocation();
      setActiveAccidentFunction(accidentFunction);

      switch (accidentFunction) {
        case FUNCTION_LOCATION_EMERGENCY:
        case FUNCTION_LOCATION_FIRE:
        case FUNCTION_LOCATION_RESCUE:
        case FUNCTION_LOCATION_INFECTIOUS_DISEASE:
          if (accidentCategory) {
            handleDrawAccident(accidentCategory);
          }
          break;
        case FUNCTION_SCOPE_AREA:
          handleSelectArea();
          break;
        case FUNCTION_SCOPE_CIRCLE:
          handleDrawCircle();
          break;
        case FUNCTION_SCOPE_POLYGON:
          handleDrawPolygon();
          break;
      }
    }
  };

  if (!space.floorsMapId) {
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
  }

  return (
    <>
      {loading && <Preloader />}
      {!loading && (
        <>
          <ListHeader
            titleKey={'text_accident_registration'}
            createBtnTextKey={'text_registration_history'}
            onCreateBtnClick={handleRouteAccidentList}
          />
          <div className="container-fluid">
            <h5 className="mb-1">{t('text_specify_position')}</h5>
            <p className="text-50">
              {t('msg_accident_suggest_to_register_spot')}
            </p>
            <ul className="info-panel accident-panel info-link">
              <li
                className={classNames({
                  active:
                    activeAccidentFunction === FUNCTION_LOCATION_EMERGENCY,
                })}
                onClick={() =>
                  handleActiveFunction(
                    FUNCTION_LOCATION_EMERGENCY,
                    ACCIDENT_EMERGENCY
                  )
                }
              >
                <a>
                  <p>{t('text_emergency')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'warning'} />
                    </em>
                  </div>
                </a>
              </li>
              <li
                className={classNames({
                  active: activeAccidentFunction === FUNCTION_LOCATION_FIRE,
                })}
                onClick={() =>
                  handleActiveFunction(FUNCTION_LOCATION_FIRE, ACCIDENT_FIRE)
                }
              >
                <a>
                  <p>{t('text_fire')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'local_fire_department'} />
                    </em>
                  </div>
                </a>
              </li>
              <li
                className={classNames({
                  active: activeAccidentFunction === FUNCTION_LOCATION_RESCUE,
                })}
                onClick={() =>
                  handleActiveFunction(
                    FUNCTION_LOCATION_RESCUE,
                    ACCIDENT_RESCUE
                  )
                }
              >
                <a>
                  <p>{t('text_rescue')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'local_hospital'} />
                    </em>
                  </div>
                </a>
              </li>
              <li
                className={classNames({
                  active:
                    activeAccidentFunction ===
                    FUNCTION_LOCATION_INFECTIOUS_DISEASE,
                })}
                onClick={() =>
                  handleActiveFunction(
                    FUNCTION_LOCATION_INFECTIOUS_DISEASE,
                    ACCIDENT_INFECTIOUS_DISEASE
                  )
                }
              >
                <a>
                  <p>{t('text_infectious_disease')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'coronavirus'} />
                    </em>
                  </div>
                </a>
              </li>
            </ul>

            <h5 className="mb-1">{t('text_specify_range')}</h5>
            <p className="text-50">
              {t('msg_accident_suggest_to_register_range')}
            </p>
            <ul className="info-panel accident-panel info-link">
              <li
                className={classNames({
                  active: activeAccidentFunction === FUNCTION_SCOPE_AREA,
                })}
                onClick={() => handleActiveFunction(FUNCTION_SCOPE_AREA)}
              >
                <a>
                  <p>{t('text_selection_structure')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'highlight_alt'} />
                    </em>
                  </div>
                </a>
              </li>
              <li
                className={classNames({
                  active: activeAccidentFunction === FUNCTION_SCOPE_CIRCLE,
                })}
                onClick={() => handleActiveFunction(FUNCTION_SCOPE_CIRCLE)}
              >
                <a>
                  <p>{t('text_selection_range')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'adjust'} />
                    </em>
                  </div>
                </a>
              </li>
              <li
                className={classNames({
                  active: activeAccidentFunction === FUNCTION_SCOPE_POLYGON,
                })}
                onClick={() => handleActiveFunction(FUNCTION_SCOPE_POLYGON)}
              >
                <a>
                  <p>{t('text_draw_line')}</p>
                  <div className="cell">
                    <em>
                      <MaterialIcon name={'timeline'} />
                    </em>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </>
      )}
    </>
  );
}

export default AccidentDirectRegister;
