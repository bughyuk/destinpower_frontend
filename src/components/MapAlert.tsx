import React, { useEffect, useState } from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { Alert as BootstrapAlert } from 'react-bootstrap';
import { useAlert } from '@/modules/alert/hook';
import { useTranslation } from 'react-i18next';
import firebase from 'firebase/app';
import { Alert, FcmData, FcmNotification } from '@/modules/alert/types';
import {
  useControlAccident,
  useControlProject,
  useControlSpace,
} from '@/modules/map/hook';
import { useControlMode } from '@/modules/setup/hook';
import { CONTROL_MODE_TENSE_REALTIME } from '@/modules/setup/types';

function MapAlert() {
  const { alerts, handleSetAlert } = useAlert();
  const { project } = useControlProject();

  useEffect(() => {
    if (firebase.apps.length) {
      const firebaseApp = firebase.apps.find(
        (app) => app.name === 'WATA_CONTROL_PLATFORM_APP'
      );

      if (firebaseApp && firebase.messaging.isSupported()) {
        const messaging = firebaseApp.messaging();
        if (messaging) {
          messaging.onMessage((payload) => {
            const notification = payload.notification as FcmNotification;
            const data = payload.data as FcmData;

            const space = project.spaceList.find(
              (space) => space.mappingId === data.mappingId
            );

            if (space) {
              handleSetAlert({
                ...notification,
                ...data,
              });
            }
          });
        }
      }
    }
  }, [firebase.apps.length]);

  return (
    <div className="map-alert-container">
      {alerts.map((alert) => (
        <MapAlertItem key={alert.eventId} {...alert} />
      ))}
    </div>
  );
}

type MapAlertItemProps = Alert;

function MapAlertItem({ eventId, mappingId, mapId, body }: MapAlertItemProps) {
  const { t } = useTranslation();
  const [isShow, setShow] = useState(true);
  const { handleAlertConfirm } = useAlert();
  const { handleSetSpace } = useControlSpace();
  const { setControlModeTense } = useControlMode();
  const { handleSetDirectAccidentId } = useControlAccident();

  const handleLocationCheck = () => {
    handleSetSpace({
      spaceMappingId: mappingId,
      floorsMapId: mapId,
      spaceMetaId: '',
    });
    setControlModeTense(CONTROL_MODE_TENSE_REALTIME);
    handleSetDirectAccidentId(eventId);
    handleAlertConfirm(eventId);
    setShow(false);
  };

  return (
    <BootstrapAlert
      className="map-alert d-flex bg-accent text-white mb-0"
      show={isShow}
      transition={true}
    >
      <div className="d-flex align-items-center">
        <div className="mr-8pt">
          <MaterialIcon name={'local_fire_department'} />
        </div>
        <div className="map-alert-title">
          <small>
            <strong>{body}</strong>
          </small>
        </div>
      </div>
      <div className="d-flex flex justify-content-end align-items-center">
        <a className="btn btn-link btn-sm" onClick={handleLocationCheck}>
          {t('text_confirm_of_location')}
        </a>
        <button
          type="button"
          className="close"
          onClick={() => {
            setShow(false);
          }}
        >
          <span>
            <MaterialIcon name={'close'} />
          </span>
        </button>
      </div>
    </BootstrapAlert>
  );
}

export default MapAlert;
