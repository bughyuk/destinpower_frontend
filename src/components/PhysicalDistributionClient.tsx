import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import PhysicalDistributionClientList from '@/components/PhysicalDistributionClientList';
import PhysicalDistributionClientRegister from '@/components/PhysicalDistributionClientRegister';
import PhysicalDistributionClientEdit from '@/components/PhysicalDistributionClientEdit';
import { Client } from '@/modules/physical_distribution/types';

function PhysicalDistributionClient({
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  const { t } = useTranslation();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_LIST);
  const [client, setClient] = useState<Client>({} as Client);

  let content: ReactElement = <></>;
  if (status === PANE_STATUS_LIST) {
    content = (
      <PhysicalDistributionClientList
        onChangeClient={setClient}
        onChangeStatus={setStatus}
      />
    );
  } else if (status === PANE_STATUS_REGISTER) {
    content = <PhysicalDistributionClientRegister onChangeStatus={setStatus} />;
  } else if (status === PANE_STATUS_EDIT) {
    content = (
      <PhysicalDistributionClientEdit
        client={client}
        onChangeStatus={setStatus}
      />
    );
  }

  let displayTitle = title;
  if (status === PANE_STATUS_REGISTER) {
    displayTitle = t('text_add_client');
  } else if (status === PANE_STATUS_EDIT) {
    displayTitle = t('text_edit_client');
  }

  const handleClickBack = () => {
    if (status === PANE_STATUS_LIST) {
      onClickBack?.call(null);
    } else {
      setStatus(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={handleClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{displayTitle}</h3>
          </div>
          {status === PANE_STATUS_LIST && (
            <a
              className="btn btn-outline-dark ml-auto"
              onClick={() => setStatus(PANE_STATUS_REGISTER)}
            >
              {t('text_add_client')}
            </a>
          )}
        </div>
      </div>
      {content}
    </>
  );
}

export default PhysicalDistributionClient;
