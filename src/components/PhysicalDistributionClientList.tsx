import React, { useEffect, useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_EDIT } from '@/utils/constants/common';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import {
  // deleteClient,
  // fetchClients,
  dp_fetchClients,
  dp_deleteClient,
} from '@/api/physical_distribution';
// import { useControlProject } from '@/modules/map/hook';
import {
  Client,
  PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING,
  PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING,
} from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';

type PhysicalDistributionClientListProps = PaneProps & {
  onChangeClient: (client: Client) => void;
};

function PhysicalDistributionClientList({
  onChangeStatus,
  onChangeClient,
}: PhysicalDistributionClientListProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [load, setLoad] = useState(false);
  const [clientList, setClientList] = useState<Client[]>([]);
  const [filterList, setFilterList] = useState<Client[]>([]);
  const [activeId, setActiveId] = useState('');

  useEffect(() => {
    handleFetchClientList();
  }, []);

  useEffect(() => {
    setActiveId('');
    setFilterList(
      clientList.filter(
        (client) =>
          client.customerName.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, clientList]);

  const handleFetchClientList = async () => {
    setLoad(false);
    const data = await dp_fetchClients(''); //검색어없는
    setClientList(data);
    setLoad(true);
  };

  const handleClickEdit = (client: Client) => {
    onChangeClient(client);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteClient = async (clientId: string) => {
    const result = await dp_deleteClient(clientId);

    if (result) {
      handleFetchClientList();
    }
  };

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'place_holder_search_client'}
          onSubmit={setSearchKeyword}
        />
      </div>
      <div className="list-wide-cover">
        <div className="mb-4">
          {!load && <></>}
          {load && filterList.length === 0 && (
            <em className="none-list mb-4">{t('msg_client_empty')}</em>
          )}
          {load && filterList.length > 0 && (
            <ul className="sidebar-menu">
              {filterList.map((client) => (
                <ClientItem
                  key={client.customerId}
                  {...client}
                  activeId={activeId}
                  onClick={(clientId) => {
                    let value = '';
                    if (clientId !== activeId) {
                      value = clientId;
                    }
                    setActiveId(value);
                  }}
                  onClickEdit={() => {
                    handleClickEdit(client);
                  }}
                  onClickDelete={handleDeleteClient}
                />
              ))}
            </ul>
          )}
        </div>
      </div>
    </>
  );
}

type ClientItemProps = Client & {
  activeId: string;
  onClick: (clientId: string) => void;
  onClickEdit: () => void;
  onClickDelete: (clientId: string) => void;
};

function ClientItem({
  customerId,
  customerName,
  logisticsCategory,
  phoneNumber,
  mailId,
  note,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
  deleteFlag,
}: ClientItemProps) {
  const { t } = useTranslation();
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  return (
    <>
      <li
        className={classNames('sidebar-menu-item', {
          open: customerId === activeId,
        })}
      >
        <div className="d-flex flex-row">
          <a
            className={classNames('sidebar-menu-button', {
              'text-30': deleteFlag,
            })}
            onClick={() => onClick(customerId)}
          >
            {deleteFlag && `[${t('text_delete')}] `} {customerName}
            <span className="sidebar-menu-badge ml-auto mr-2">
              {logisticsCategory === PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING &&
                t('text_warehousing_client')}
              {logisticsCategory ===
                PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING &&
                t('text_release_client')}
            </span>
            <span className="sidebar-menu-toggle-icon"></span>
          </a>
        </div>
        <Collapse in={customerId === activeId}>
          <div className="sidebar-submenu sm-indent">
            <div className="cover">
              <div className="col-12 p-2">
                <span className="text-50">{t('text_phone_number')}</span>
                <p className="m-0">{phoneNumber}</p>
              </div>
              <div className="col-12 p-2">
                <span className="text-50">{t('text_email')}</span>
                <p className="m-0">{mailId}</p>
              </div>
              <div className="col-12 p-2">
                <span className="text-50">{t('text_memo')}</span>
                <p
                  className="m-0"
                  dangerouslySetInnerHTML={{
                    __html: note || '-',
                  }}
                ></p>
              </div>
              <div className="btn-cover mt-2">
                {!deleteFlag && (
                  <>
                    <a
                      className="btn"
                      onClick={() => setShowConfirmModal(true)}
                    >
                      {t('text_delete')}
                    </a>

                    <a className="btn" onClick={() => onClickEdit()}>
                      {t('text_edit')}
                    </a>
                  </>
                )}
              </div>
            </div>
          </div>
        </Collapse>
      </li>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(customerId)}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

export default PhysicalDistributionClientList;
