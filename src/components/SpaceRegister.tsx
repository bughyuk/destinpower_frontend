import React, { useEffect } from 'react';
import SpaceAddressRegister from '@/components/SpaceAddressRegister';
import { useSpaceRegister } from '@/modules/space/hook';

function SpaceRegister() {
  const { handleSetConnectProjectId } = useSpaceRegister();

  useEffect(() => {
    return () => {
      handleSetConnectProjectId(null);
    };
  }, []);

  return <SpaceAddressRegister />;
}

export default SpaceRegister;
