import React, { ReactElement, useEffect } from 'react';
import { PANE_STATUS_CATEGORY, PaneStatus } from '@/utils/constants/common';
import {
  Category,
  PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT,
  PROCESS_CATEGORY_COMPLETED_PROCESS,
  PROCESS_CATEGORY_PROCESS_CHECK,
} from '@/modules/physical_distribution/types';
import { useTranslation } from 'react-i18next';
import { useRightPane } from '@/modules/setup/hook';
import { RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY } from '@/modules/setup/types';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import ProcessCheckList from '@/components/ProcessCheckList';
import ProcessCheck from '@/components/ProcessCheck';

export type PhysicalDistributionListContentProps = {
  categoryIdx?: Category;
  title?: string;
  onClickBack?: () => void;
};

type PhysicalDistributionListProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function ProcessManagementList({
  onChangeStatus,
}: PhysicalDistributionListProps) {
  const { t } = useTranslation();
  const { categoryIdx } = usePhysicalDistributionCategory();
  const { handleChangeShow } = useRightPane();

  useEffect(() => {
    handleChangeShow(true, RIGHT_PANE_PHYSICAL_DISTRIBUTION_HISTORY);
  }, []);

  let title = '';
  let content: ReactElement = <></>;
  switch (categoryIdx) {
    case PROCESS_CATEGORY_PROCESS_CHECK:
    case PROCESS_CATEGORY_COMPLETED_PROCESS:
      title = '공정 체크리스트';
      content = <ProcessCheck />;
      break;
    case PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT:
      title = '공정 체크리스트 관리';
      content = <ProcessCheck />;
      break;
  }

  const handleClickBack = () => {
    onChangeStatus(PANE_STATUS_CATEGORY);
  };

  return (
    <>
      {React.cloneElement<PhysicalDistributionListContentProps>(content, {
        categoryIdx,
        title,
        onClickBack: handleClickBack,
      })}
    </>
  );
}

export default ProcessManagementList;
