import React from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import { usePhysicalDistributionStatus } from '@/modules/physical_distribution/hook';

function ProcessFilterNavbar() {
  const { t } = useTranslation();
  const {
    todayCount,
    handleChangeReloadFlag,
  } = usePhysicalDistributionStatus();

  return (
    <>
      <div className="tabBox mapStauts02 status-con active">
        <span className="d-flex align-items-center mr-4">
          <span className="avatar mr-12pt">
            <span className="avatar-title navbar-avatar bg-success border-0">
              <i className="material-icons text-white">check_circle</i>
            </span>
          </span>
          <small className="flex d-flex flex-column">
            <span className="navbar-text-50">생산완료</span>
            <strong className="navbar-text-100">3건</strong>
          </small>
        </span>
        <span className="d-flex align-items-center mr-4">
          <span className="avatar mr-12pt">
            <span className="avatar-title navbar-avatar bg-primary border-0">
              <i className="material-icons-outlined text-white">
                all_inclusive
              </i>
            </span>
          </span>
          <small className="flex d-flex flex-column">
            <span className="navbar-text-50">생산중</span>
            <strong className="navbar-text-100">4건</strong>
          </small>
        </span>
      </div>
    </>
  );
}

export default ProcessFilterNavbar;
