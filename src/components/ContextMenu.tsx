import React, { ReactNode } from 'react';

type ContextMenuProps = {
  top: number;
  left: number;
  children: ReactNode;
};

function ContextMenu({ top, left, children }: ContextMenuProps) {
  return (
    <div
      className="context-wrap row"
      style={{
        top,
        left,
      }}
    >
      <div className="context-menu type-row">{children}</div>
    </div>
  );
}

export default ContextMenu;
