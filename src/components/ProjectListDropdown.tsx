import React, { useRef } from 'react';
import { Dropdown } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import {
  FUNCTION_COPY,
  FUNCTION_DELETE,
  FUNCTION_EDIT,
} from '@/utils/constants/common';
import { useTranslation } from 'react-i18next';
import { useDropdown } from '@/modules/common';

type ProjectListDropdownProps = {
  onSelect: (eventKey: string | null) => void;
  showCopy?: boolean;
  showEdit?: boolean;
};

function ProjectListDropdown({
  onSelect,
  showCopy = false,
  showEdit = false,
}: ProjectListDropdownProps) {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  return (
    <Dropdown onToggle={handleToggle} onSelect={onSelect}>
      <Dropdown.Toggle as={'a'} className="text-muted" data-caret="false">
        <MaterialIcon name={'more_vert'} />
      </Dropdown.Toggle>
      <Dropdown.Menu ref={dropdown} align={'right'}>
        {showCopy && (
          <>
            <Dropdown.Item eventKey={FUNCTION_COPY}>
              {t('text_copy')}
            </Dropdown.Item>
            <Dropdown.Divider />
          </>
        )}
        {showEdit && (
          <Dropdown.Item eventKey={FUNCTION_EDIT} className={'text-primary'}>
            {t('text_edit')}
          </Dropdown.Item>
        )}
        <Dropdown.Item eventKey={FUNCTION_DELETE} className={'text-danger'}>
          {t('text_delete')}
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
}

export default ProjectListDropdown;
