import React, { useState } from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';

type ListSearchProps = {
  onSubmit: (searchKeyword: string) => void;
};

function ListSearch({ onSubmit }: ListSearchProps) {
  const { t } = useTranslation();
  const [searchKeyword, setSearchKeyword] = useState('');

  const handleSubmit = () => {
    onSubmit(searchKeyword);
  };

  return (
    <form
      className="form-inline ml-4"
      onSubmit={(e) => {
        e.preventDefault();
        handleSubmit();
      }}
    >
      <input
        type="text"
        className="form-control search"
        placeholder={t('text_search_en')}
        value={searchKeyword}
        onChange={(e) => setSearchKeyword(e.target.value)}
      />
      <a className="btn btn-sm btn-light" onClick={handleSubmit}>
        <MaterialIcon name={'search'} />
      </a>
    </form>
  );
}

export default ListSearch;
