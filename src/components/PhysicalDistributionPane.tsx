import React, { useEffect, useState } from 'react';
import {
  PANE_STATUS_CATEGORY,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import {
  useFloatPane,
  useLeftPaneContainer,
  useRightPane,
} from '@/modules/setup/hook';
import PhysicalDistributionCategory from '@/components/PhysicalDistributionCategory';
import PhysicalDistributionList from '@/components/PhysicalDistributionList';
import { useControlProject, useControlSpace } from '@/modules/map/hook';
import {
  FLOAT_PANE_PHYSICAL_DISTRIBUTION,
  RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS,
} from '@/modules/setup/types';
import { fetchDistributionStatus } from '@/api/physical_distribution';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionStatus,
} from '@/modules/physical_distribution/hook';
import { initialDistributionStatusDataState } from '@/modules/physical_distribution/context';

function PhysicalDistributionPane() {
  return (
    <div className="tab-pane sm-distribution active">
      <div className="distribution-inner">
        <PhysicalDistributionContent />
      </div>
    </div>
  );
}

function PhysicalDistributionContent() {
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_CATEGORY);
  const { project } = useControlProject();
  const { space } = useControlSpace();
  const { updateScroll } = useLeftPaneContainer();
  useEffect(() => {
    setStatus(PANE_STATUS_CATEGORY);
  }, [space]);

  const { handleChangeShow: handleChangeFloatPaneShow } = useFloatPane();
  const { handleChangeShow: handleChangeRightPaneShow } = useRightPane();
  const {
    categoryIdx,
    handleSetCategoryIdx,
  } = usePhysicalDistributionCategory();
  useEffect(() => {
    updateScroll();
    if (status !== PANE_STATUS_CATEGORY) {
      handleChangeFloatPaneShow(true, FLOAT_PANE_PHYSICAL_DISTRIBUTION);
    } else {
      handleChangeFloatPaneShow(false);
    }
  }, [status, categoryIdx]);
  const {
    handleSetDistributionStatus,
    reloadFlag,
  } = usePhysicalDistributionStatus();

  useEffect(() => {
    handleChangeRightPaneShow(
      true,
      RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS
    );

    return () => {
      handleChangeFloatPaneShow(false);
      handleChangeRightPaneShow(false);
      handleSetCategoryIdx(undefined);
    };
  }, []);

  useEffect(() => {
    if (typeof categoryIdx !== 'undefined') {
      setStatus(PANE_STATUS_LIST);
    }
  }, [categoryIdx]);

  useEffect(() => {
    handleFetchDistributionStatus();
  }, [reloadFlag]);

  const handleFetchDistributionStatus = async () => {
    const data = await fetchDistributionStatus(project.id);
    if (data) {
      handleSetDistributionStatus(data);
    }
  };

  return (
    <>
      {status === PANE_STATUS_CATEGORY && <PhysicalDistributionCategory />}
      {status === PANE_STATUS_LIST && (
        <PhysicalDistributionList onChangeStatus={setStatus} />
      )}
    </>
  );
}

export default PhysicalDistributionPane;
