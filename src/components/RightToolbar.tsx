import React from 'react';
import { useControlProject } from '@/modules/map/hook';
import { useRightPane } from '@/modules/setup/hook';
import { RIGHT_PANE_STATISTICS } from '@/modules/setup/types';

function RightToolbar() {
  const { project: controlProject } = useControlProject();
  const { handleChangeShow } = useRightPane();

  return (
    <>
      {controlProject.spaceList.length > 0 && (
        <div
          className="cd-panel__open"
          onClick={() => handleChangeShow(true, RIGHT_PANE_STATISTICS)}
        >
          <a className="js-cd-panel-trigger">
            <span className="material-icons-outlined">query_stats</span>
            <small>Chart</small>
          </a>
        </div>
      )}
    </>
  );
}

export default RightToolbar;
