import { useUser } from '@/modules/user/hook';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import React, { useEffect, useRef } from 'react';
import { useDropdown } from '@/modules/common/hook';
import { languages, Languages } from '@/locales/i18n';
import i18n from 'i18next';
import { Dropdown } from 'react-bootstrap';
import { useLogout } from '@/modules/access/logout/hook';
import { useHomeMenu } from '@/modules/home/hook';
import { MENU_ACCOUNT_PROFILE } from '@/modules/home/types';
import ProfileAvatar from '@/components/ProfileAvatar';
import { saveAs } from 'file-saver';

type StatusNavbarProps = {
  addOns?: boolean;
};

function StatusNavbar({ addOns = false }: StatusNavbarProps) {
  const { handleSetMenu } = useHomeMenu();
  const { user } = useUser();
  const { handleLogout, logoutStatusCode } = useLogout();
  const history = useHistory();
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  const handleClickMyPage = () => {
    handleSetMenu(MENU_ACCOUNT_PROFILE);
    if (location.pathname.indexOf('home') < 0) {
      history.push('/home');
    }
  };

  const handleClickLogout = () => {
    handleLogout();
  };

  useEffect(() => {
    if (logoutStatusCode) {
      history.replace('/signin');
    }
  }, [logoutStatusCode]);

  const handleChangeLanguage = (language: Languages) => {
    i18n.changeLanguage(language);
    localStorage.setItem('language', language);
  };

  const handleClickAppDownload = () => {
    saveAs('/static/apk/SmartFactory_1.0.5.apk', 'SmartFactory_1.0.5.apk');
  };

  return (
    <div className="d-flex flex justify-content-end align-items-center">
      {addOns && (
        <>
          {/*
          <div className="d-flex">
            <a
              className="btn btn-sm btn-outline-accent btn-rounded d-flex align-items-center text-capitalize px-3"
              onClick={handleClickAppDownload}
            >
              <span className="material-icons font-size-16pt mr-1">
                file_download
              </span>
              App v.1.0.5
            </a>
          </div>
          */}
          <div className="d-flex ml-4">
            <span className="font-weight-bolder">{user.companyName}</span>
          </div>
        </>
      )}
      {/*
      <div className="form-group m-0 ml-4 form-lang">
        <select
          className="form-line custom-line"
          defaultValue={localStorage.getItem('language') || 'ko'}
          onChange={(e) => handleChangeLanguage(e.target.value as Languages)}
        >
          {languages.map((language) => (
            <option key={language} value={language}>
              {t(`language_code_${language}`)}
            </option>
          ))}
        </select>
      </div>
      */}
      <Dropdown className={'nav-item ml-4'} onToggle={handleToggle}>
        <Dropdown.Toggle
          as={'a'}
          data-caret="false"
          className={'d-flex align-items-center'}
        >
          <span className="avatar avatar-sm">
            <ProfileAvatar
              profileImgUrl={user.profileImgUrl || ''}
              userName={user?.userName}
            />
          </span>
        </Dropdown.Toggle>
        <Dropdown.Menu ref={dropdown} align={'right'}>
          <Dropdown.Header>
            <strong>{t('text_account_en')}</strong>
          </Dropdown.Header>
          <Dropdown.Item onClick={handleClickMyPage}>
            {t('text_profile')}
          </Dropdown.Item>
          <Dropdown.Item onClick={handleClickLogout}>
            {t('text_logout')}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

export default StatusNavbar;
