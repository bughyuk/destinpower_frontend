import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

function ProjectPublicSpaceTab() {
  return (
    <>
      <Category />
    </>
  );
}

function Category() {
  const { t } = useTranslation();

  const [selectedCategory, setSelectCategory] = useState(0);

  const categories = [
    {
      idx: 0,
      name: t('text_subway'),
    },
    {
      idx: 1,
      name: t('text_department_store'),
    },
    {
      idx: 2,
      name: t('text_airport'),
    },
    {
      idx: 3,
      name: t('text_hospital'),
    },
  ];

  return (
    <>
      <h5>{t('text_category')}</h5>
      <div className="btn-group mb-3 category-group">
        {categories.map((category) => (
          <CategoryButton
            key={category.idx}
            {...category}
            selectedCategory={selectedCategory}
            onSelect={setSelectCategory}
          />
        ))}
      </div>
    </>
  );
}

type CategoryButtonProps = {
  idx: number;
  name: string;
  selectedCategory: number;
  onSelect: (idx: number) => void;
};

function CategoryButton({
  idx,
  name,
  selectedCategory,
  onSelect,
}: CategoryButtonProps) {
  return (
    <button
      type="button"
      className={classNames('btn btn-outline-secondary', {
        active: idx === selectedCategory,
      })}
      onClick={() => onSelect(idx)}
    >
      {name}
    </button>
  );
}

export default ProjectPublicSpaceTab;
