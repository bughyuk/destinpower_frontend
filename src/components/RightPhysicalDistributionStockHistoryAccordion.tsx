import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import { fetchDashboardDistributionHistory } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionHistory,
  usePhysicalDistributionStatus,
} from '@/modules/physical_distribution/hook';
import { CommonUtils } from '@/utils';
import {
  DistributionHistory,
  PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY,
} from '@/modules/physical_distribution/types';
import moment from 'moment';
import { useRightPane } from '@/modules/setup/hook';

function RightPhysicalDistributionStockHistoryAccordion() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const { show, handleChangeShow: handleChangeRightPaneShow } = useRightPane();
  const { handleSetCategoryIdx } = usePhysicalDistributionCategory();
  const {
    warehouseStockTotal,
    todayCount,
    reloadFlag,
  } = usePhysicalDistributionStatus();
  const { handleSetDistributionHistory } = usePhysicalDistributionHistory();
  const [isOpen, setOpen] = useState(true);
  const [distributionHistories, setDistributionHistories] = useState<
    DistributionHistory[]
  >([]);

  useEffect(() => {
    if (show) {
      handleFetchDashboardDistributionHistory();
    }
  }, [reloadFlag, show]);

  const handleFetchDashboardDistributionHistory = async () => {
    const data = await fetchDashboardDistributionHistory(project.id);
    setDistributionHistories(data);
  };

  return (
    <>
      <div
        className={classNames('accordion__item', {
          open: isOpen,
        })}
      >
        <a className="accordion__toggle" onClick={() => setOpen(!isOpen)}>
          <span className="flex d-flex align-items-center">
            {t('text_current_stock_and_history')}
          </span>
          <span className="accordion__toggle-icon material-icons">
            keyboard_arrow_down
          </span>
        </a>
        <Collapse in={isOpen}>
          <div className="accordion__menu">
            <div className="accordion__menu-link align-items-center">
              <em className="flex text-dark font-size-24pt">
                {CommonUtils.numberWithCommas(warehouseStockTotal)}
              </em>
              <div className="visitor-group">
                <ul className="mb-0">
                  <li>
                    {t('text_warehousing')}
                    <span className="text-accent">
                      {CommonUtils.numberWithCommas(
                        todayCount.warehousingCompleteCnt
                      )}
                    </span>
                  </li>
                  <li>
                    {t('text_release')}
                    <span className="text-primary">
                      {CommonUtils.numberWithCommas(
                        todayCount.releasingCompleteCnt
                      )}
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            {distributionHistories.map((distributionHistory) => (
              <StockHistoryItem
                key={distributionHistory.logisticsId}
                {...distributionHistory}
                onClick={() => {
                  handleChangeRightPaneShow(false);
                  handleSetCategoryIdx(PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY);
                  handleSetDistributionHistory(distributionHistory);
                }}
              />
            ))}
          </div>
        </Collapse>
      </div>
    </>
  );
}

type StockHistoryItemProps = DistributionHistory & {
  onClick: () => void;
};

function StockHistoryItem({
  updateDate,
  productName,
  warehouseName,
  logisticsCategory,
  status,
  logisticsQuantity,
  updateId,
  registId,
  deleteFlag,
  onClick,
}: StockHistoryItemProps) {
  const { t } = useTranslation();

  const distributionHistoryTextKey = CommonUtils.getDistributionHistoryTextKey(
    logisticsCategory,
    status
  );

  return (
    <div
      className={classNames('accordion__menu-link', {
        'state-up': logisticsQuantity > 0,
        'state-down': logisticsQuantity < 0,
      })}
    >
      <div className="panel-list">
        <a className="d-flex align-items-start" onClick={onClick}>
          <div className="mr-3">
            <h6
              className={classNames({
                'text-30': deleteFlag,
              })}
            >
              <span className="material-icons font-size-16pt icon--left">
                {distributionHistoryTextKey.icon}
              </span>
              {deleteFlag && `[${t('text_delete')}] `}
              {t(distributionHistoryTextKey.textKey)}
              {updateId && <span className="badge-pich">{t('text_edit')}</span>}
            </h6>
            <div className="text-els">{productName}</div>
            <ul className="mb-2">
              <li>{moment(updateDate).format('YYYY-MM-DD HH:mm:ss')}</li>
              <li>{warehouseName}</li>
            </ul>
            <span className="writer">{updateId || registId}</span>
          </div>
          <div className="quantity">
            <h6>
              {logisticsQuantity > 0
                ? `+${logisticsQuantity}`
                : `${logisticsQuantity}`}
            </h6>
          </div>
        </a>
      </div>
    </div>
  );
}

export default RightPhysicalDistributionStockHistoryAccordion;
