import React, { useEffect, useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_EDIT } from '@/utils/constants/common';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import {
  dp_deleteProductType,
  dp_fetchProductTypes,
} from '@/api/physical_distribution';
// import { useControlProject } from '@/modules/map/hook';
import { ProductType } from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';
import { Config } from '@/config';

type ProcessProductTypeListProps = PaneProps & {
  onChangeProductType: (productType: ProductType) => void;
};

function ProcessProductTypeList({
  onChangeStatus,
  onChangeProductType,
}: ProcessProductTypeListProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [load, setLoad] = useState(false);
  const [productTypeList, setProductTypeList] = useState<ProductType[]>([]);
  const [filterList, setFilterList] = useState<ProductType[]>([]);
  const [activeId, setActiveId] = useState('');

  useEffect(() => {
    handleFetchProductTypeList();
  }, []);

  useEffect(() => {
    setActiveId('');
    setFilterList(
      productTypeList.filter(
        (type) => type.name.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, productTypeList]);

  const handleFetchProductTypeList = async () => {
    setLoad(false);
    const data = await dp_fetchProductTypes(''); //검색어없는
    setProductTypeList(data);
    setLoad(true);
  };

  const handleClickEdit = (type: ProductType) => {
    onChangeProductType(type);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteProductType = async (typeId: string) => {
    const result = await dp_deleteProductType(typeId);

    if (result) {
      handleFetchProductTypeList();
    }
  };

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'검색어를 입력해 주세요.'}
          onSubmit={setSearchKeyword}
        />
      </div>
      <div className="list-wide-cover">
        <div className="mb-4">
          {!load && <></>}
          {load && filterList.length === 0 && (
            <em className="none-list mb-4">{t('제품타입이 없습니다.')}</em>
          )}
          {load && filterList.length > 0 && (
            <ul className="sidebar-menu">
              {filterList.map((type) => (
                <ProductTypeItem
                  key={type.id}
                  {...type}
                  activeId={activeId}
                  onClick={(typeId) => {
                    let value = '';
                    if (typeId !== activeId) {
                      value = typeId;
                    }
                    setActiveId(value);
                  }}
                  onClickEdit={() => {
                    handleClickEdit(type);
                  }}
                  onClickDelete={handleDeleteProductType}
                />
              ))}
            </ul>
          )}
        </div>
      </div>
    </>
  );
}

type ProductTypeItemProps = ProductType & {
  activeId: string;
  onClick: (typeId: string) => void;
  onClickEdit: () => void;
  onClickDelete: (typeId: string) => void;
};

function ProductTypeItem({
  id,
  name,
  product_family,
  image_path,
  status,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
}: ProductTypeItemProps) {
  const { t } = useTranslation();
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  return (
    <>
      <li
        className={classNames('sidebar-menu-item', {
          open: id === activeId,
        })}
      >
        <div className="d-flex flex-row">
          <a
            className={classNames('sidebar-menu-button', {
              'text-30': status === 'd',
            })}
            onClick={() => onClick(id)}
          >
            {status === 'd' && `[${t('text_delete')}] `} {name}
            <span className="sidebar-menu-badge ml-auto mr-2"></span>
            <span className="sidebar-menu-toggle-icon"></span>
          </a>
        </div>
        <Collapse in={id === activeId}>
          <div className="sidebar-submenu sm-indent">
            <div className="cover">
              <div className="col-12 p-2">
                <span className="text-50">제품군</span>
                <p className="m-0">{product_family?.name}</p>
              </div>
              <div className="col-12 p-2">
                <span className="text-50">제품타입</span>
                <p className="m-0">{name}</p>
              </div>
              {image_path && (
                <div className="col-12 p-2">
                  <span className="text-50">이미지</span>
                  <div className="blank-img">
                    <img
                      src={
                        Config.backend_api.uri +
                        image_path.replace(/^\.\//, '/')
                      }
                      className="avatar-img"
                    />
                  </div>
                </div>
              )}
              <div className="btn-cover mt-2">
                {status === 'n' && (
                  <>
                    <a
                      className="btn"
                      onClick={() => setShowConfirmModal(true)}
                    >
                      {t('text_delete')}
                    </a>
                    <a className="btn" onClick={() => onClickEdit()}>
                      {t('text_edit')}
                    </a>
                  </>
                )}
              </div>
            </div>
          </div>
        </Collapse>
      </li>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(id)}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

export default ProcessProductTypeList;
