import React, { ReactNode, useEffect, useRef } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useSpaceRegister } from '@/modules/space/hook';

type LeftCustomPaneProps = {
  show: boolean;
  children?: ReactNode;
};

function JapanAddressPane({ show, children }: LeftCustomPaneProps) {
  const {
    handleChangeShowAddressPane,
    handleSetCustomPaneContainer,
  } = useSpaceRegister();
  const scrollbar = useRef<PerfectScrollbar>(null);

  useEffect(() => {
    const current = scrollbar.current;
    if (current) {
      handleSetCustomPaneContainer(current);
    }

    return () => {
      handleChangeShowAddressPane(false);
    };
  }, []);

  useEffect(() => {
    const current = scrollbar.current;

    if (current) {
      const container = (current as any)._container;
      if (show) {
        container.style.visibility = 'visible';
        container.style.transform = 'translateZ(0)';
        container.style.boxShadow =
          '0 3px 3px -2px rgba(39, 44, 51, .1), 0 3px 4px 0 rgba(39, 44, 51, .04), 0 1px 8px 0 rgba(39, 44, 51, .02)';
      } else {
        container.style.transform = null;
        container.style.boxShadow = null;
      }
    }
  }, [show]);

  return (
    <PerfectScrollbar
      className="sidebar sidebar-left flex sidebar-secondary"
      ref={scrollbar}
    >
      <div className="tab-content">{children}</div>
    </PerfectScrollbar>
  );
}

export default JapanAddressPane;
