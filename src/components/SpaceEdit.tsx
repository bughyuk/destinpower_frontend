import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import classNames from 'classnames';
import {
  useOpenLayers,
  useSpace,
  useSpaceRegister,
} from '@/modules/space/hook';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import { useUser } from '@/modules/user/hook';
import { LngLat } from '@/modules/map/types';
import { useAsync } from '@/modules/common';
import { fetchJapanAddress } from '@/api/address';
import { postUpdateSpace, RequestUpdateSpace } from '@/api/space';
import { Collection, Feature } from 'ol';
import { Icon, Style } from 'ol/style';
import { Translate } from 'ol/interaction';
import {
  JapanLngLat,
  SPACE_COUNTRY_CODE_JP,
  SPACE_COUNTRY_CODE_KR,
  SpaceCountryCode,
} from '@/modules/space/types';
import { OpenLayersUtils } from '@/utils/OpenLayersUtils';
import { Point } from 'ol/geom';
import InvalidAlert from '@/components/InvalidAlert';
import { GOOGLE_API_KEY } from '@/utils/constants/common';
import { StatusCodes } from 'http-status-codes';
import DaumPostcodeModal from '@/components/DaumPostcodeModal';
import { fetchGoogleLocation } from '@/api/common';

const iconFeature = new Feature();
iconFeature.setId('address_marker');
const iconStyle = new Style({
  image: new Icon({
    src: '/static/images/ic-marker-24-px.svg',
  }),
});
iconFeature.setStyle(iconStyle);
const collection = new Collection<Feature>();
collection.push(iconFeature);
const translate = new Translate({
  features: collection,
});
function SpaceEdit() {
  const history = useHistory();
  const isSetDataFinish = useRef<boolean>(false);
  const { info } = useSpace();
  const { user } = useUser();
  const [spaceName, setSpaceName] = useState('');
  const [countryCode, setCountryCode] = useState<SpaceCountryCode>(
    Number(info.space.countryCode) as SpaceCountryCode
  );
  const [address, setAddress] = useState('');
  const [lngLat, setLngLat] = useState<LngLat | null>(null);
  const [isValid, setValid] = useState(false);
  const {
    selectedDo,
    selectedSubDistrict,
    selectedLngLat,
    detailedAddress,
    handleSetAddress,
  } = useSpaceRegister();
  const [fetchJapanAddressState, fetchJapanAddressApi] = useAsync(
    fetchJapanAddress
  );

  const { data: fetchJapanAddressData } = fetchJapanAddressState;
  const { map, drawSource } = useOpenLayers();

  const handleFetchAddress = async () => {
    const lngLat = await fetchGoogleLocation(
      selectedDo + selectedSubDistrict + address
    );
    if (lngLat) {
      setLngLat({
        lng: lngLat.lng,
        lat: lngLat.lat,
      });

      const lngLatArray = Object.values(lngLat);

      iconFeature.setGeometry(new Point(lngLatArray));
      drawSource?.addFeature(iconFeature);
      map?.getView().setZoom(13);
      map?.getView().setCenter(lngLatArray);
    }
  };

  useEffect(() => {
    if (fetchJapanAddressData) {
      if ((fetchJapanAddressData as any)['地理座標']) {
        const featureById = drawSource?.getFeatureById('address_marker');
        if (featureById) {
          drawSource?.removeFeature(featureById);
        }

        const japanLatLng: JapanLngLat = (fetchJapanAddressData as any)[
          '地理座標'
        ];

        const latLng: LngLat = {
          lng: japanLatLng.経度,
          lat: japanLatLng.緯度,
        };

        const convertLngLat = OpenLayersUtils.convertLngLat([
          latLng.lng,
          latLng.lat,
        ]);

        setLngLat({
          lng: convertLngLat[0],
          lat: convertLngLat[1],
        });

        iconFeature.setGeometry(new Point(convertLngLat));
        drawSource?.addFeature(iconFeature);

        map?.getView().setZoom(13);
        map?.getView().setCenter(convertLngLat);
      }
    }
  }, [fetchJapanAddressData]);

  useEffect(() => {
    if (spaceName && address && lngLat) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [spaceName, address, lngLat]);

  const handleSubmit = async () => {
    if (lngLat) {
      const id = info.space.id;
      const submitData: RequestUpdateSpace = {
        metaid: id,
        metaname: spaceName,
        countrycode: countryCode, // 일본
        addres1: address,
        country: selectedDo,
        city: selectedSubDistrict,
        lng: lngLat.lng,
        lat: lngLat.lat,
        userid: user.userId,
      };
      const result = await postUpdateSpace(submitData);

      if (result) {
        history.replace('/home');
      }
    }
  };

  useEffect(() => {
    return () => {
      handleSetAddress({
        selectedDo: '',
        selectedSubDistrict: '',
        selectedLngLat: null,
        detailedAddress: '',
      });
    };
  }, []);

  useEffect(() => {
    handleSetAddress({
      selectedDo: '',
      selectedSubDistrict: '',
      selectedLngLat: null,
      detailedAddress: '',
    });
    const view = map?.getView();
    if (view) {
      let center: number[] = [];
      if (countryCode === SPACE_COUNTRY_CODE_JP) {
        center = [15558454.49842193, 4257326.124240982];
      } else if (countryCode === SPACE_COUNTRY_CODE_KR) {
        center = [14134326.456645714, 4516772.805272909];
      }
      view.setZoom(7);
      view.setCenter(center);
    }
  }, [countryCode]);

  useEffect(() => {
    if (map) {
      translate.on('translateend', (e) => {
        const coordinate = e.coordinate;
        setLngLat({
          lng: coordinate[0],
          lat: coordinate[1],
        });
      });
      map?.addInteraction(translate);

      const id = info.space.id;
      if (id) {
        const location = info.space.location;
        const lng = location?.lng;
        const lat = location?.lat;
        if (lng && lat) {
          const lngLat = [Number(lng), Number(lat)];
          const view = map?.getView();
          if (view) {
            view.setCenter(lngLat);
            view.setZoom(14);
          }
          setLngLat({
            lng,
            lat,
          });
        }
        handleSetAddress({
          selectedDo: info.space.country || '',
          selectedSubDistrict: info.space.city || '',
          selectedLngLat: null,
          detailedAddress: info.space.address || '',
        });
        setSpaceName(info.space.name || '');
      }
    }
  }, [map]);

  useEffect(() => {
    setAddress('');
    if (selectedDo && selectedSubDistrict) {
      if (isSetDataFinish.current) {
        drawSource?.clear();
        if (detailedAddress) {
          setAddress(detailedAddress);
        }
      } else {
        const id = info.space.id;
        if (id) {
          const location = info.space.location;
          const lng = location?.lng;
          const lat = location?.lat;
          if (lng && lat) {
            const lngLat = [Number(lng), Number(lat)];
            iconFeature.setGeometry(new Point(lngLat));
            drawSource?.addFeature(iconFeature);
            setAddress(info.space.address || '');
          }
        }
        isSetDataFinish.current = true;
      }
    }

    if (selectedLngLat) {
      const coordinate = [selectedLngLat.lng, selectedLngLat.lat];
      const featureById = drawSource?.getFeatureById('address_marker');
      if (featureById) {
        drawSource?.removeFeature(featureById);
      }

      iconFeature.setGeometry(new Point(coordinate));
      drawSource?.addFeature(iconFeature);
      map?.getView().setCenter(coordinate);
      map?.getView().setZoom(10);
      setLngLat(selectedLngLat);
    }
  }, [selectedDo, selectedSubDistrict, selectedLngLat, detailedAddress]);

  const handleChangeAddress = (address: string) => {
    setAddress(address);
  };

  return (
    <>
      <Header />
      <Body
        spaceName={spaceName}
        countryCode={countryCode}
        address={address}
        onChangeSpaceName={setSpaceName}
        onChangeCountryCode={setCountryCode}
        onChangeAddress={handleChangeAddress}
        onSubmitAddress={handleFetchAddress}
      />
      <Footer valid={isValid} onSubmit={handleSubmit} />
    </>
  );
}

function Header() {
  const { t } = useTranslation();

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{t('text_space_edit')}</h3>
        </div>
      </div>
    </div>
  );
}

type BodyProps = {
  spaceName: string;
  countryCode: SpaceCountryCode;
  address: string;
  onChangeSpaceName: (spaceName: string) => void;
  onChangeCountryCode: (countryCode: SpaceCountryCode) => void;
  onChangeAddress: (address: string) => void;
  onSubmitAddress: () => void;
};

function Body({
  spaceName,
  countryCode,
  address,
  onChangeSpaceName,
  onChangeCountryCode,
  onChangeAddress,
  onSubmitAddress,
}: BodyProps) {
  const { t } = useTranslation();
  const [showKRPostCodeModal, setShowKRPostCodeModal] = useState(false);
  const {
    selectedDo,
    selectedSubDistrict,
    handleSetAddress,
    handleChangeShowAddressPane,
  } = useSpaceRegister();

  const handleClickAddressSearch = () => {
    if (countryCode === SPACE_COUNTRY_CODE_JP) {
      handleChangeShowAddressPane(true);
    } else if (countryCode === SPACE_COUNTRY_CODE_KR) {
      setShowKRPostCodeModal(true);
    }
  };

  return (
    <>
      <DaumPostcodeModal
        show={showKRPostCodeModal}
        onHide={() => setShowKRPostCodeModal(false)}
        onComplete={(data) => {
          handleSetAddress({
            selectedDo: data.address,
            selectedSubDistrict: data.extra,
            selectedLngLat: data.lngLat,
            detailedAddress: '',
          });
        }}
      />
      <div className="container-fluid">
        <FormGroup>
          <FormLabel textKey={'text_space_name'} essential={true} />
          <input
            type="text"
            className="form-line"
            placeholder={t('place_holder_space_name')}
            value={spaceName}
            onChange={(e) => {
              onChangeSpaceName(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup>
          <FormLabel textKey={'text_country'} />
          <select
            className="form-line"
            value={countryCode}
            onChange={(e) => {
              onChangeCountryCode(Number(e.target.value) as SpaceCountryCode);
            }}
          >
            <option value={SPACE_COUNTRY_CODE_KR}>{t('country_kr')}</option>
            <option value={SPACE_COUNTRY_CODE_JP}>{t('country_jp')}</option>
          </select>
        </FormGroup>
        <FormGroup className={'over-txt'}>
          <FormLabel textKey={'text_address'} essential={true} />
          <input
            type="text"
            className="form-line"
            style={{
              paddingRight: '75px',
            }}
            readOnly={true}
            value={selectedDo + selectedSubDistrict}
          />
          <a
            className="btn btn-secondary btn-sm"
            onClick={handleClickAddressSearch}
          >
            {t('text_address_search')}
          </a>
        </FormGroup>
        <FormGroup>
          <FormLabel textKey={'text_detailed_address'} essential={true} />
          <input
            type="text"
            className="form-line"
            style={{
              paddingRight: '100px',
            }}
            value={address}
            disabled={!selectedDo && !selectedSubDistrict}
            onChange={(e) => {
              onChangeAddress(e.target.value);
            }}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                if (address) {
                  onSubmitAddress();
                }
              }
            }}
          />
          <a
            className={classNames('btn btn-secondary btn-sm', {
              disabled: !address,
            })}
            onClick={() => {
              if (address) {
                onSubmitAddress();
              }
            }}
          >
            {t('text_detailed_location_search')}
          </a>
        </FormGroup>
      </div>
    </>
  );
}

type FooterProps = {
  valid: boolean;
  onSubmit: () => void;
};

function Footer({ valid, onSubmit }: FooterProps) {
  const { t } = useTranslation();
  const history = useHistory();

  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (valid) {
      setShowInvalidMessage(false);
    }
  }, [valid]);

  return (
    <>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => history.replace('/home')}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0')}
            onClick={() => {
              if (valid) {
                setShowInvalidMessage(false);
                onSubmit();
              } else {
                setShowInvalidMessage(true);
              }
            }}
          >
            {t('text_edit')}
          </a>
        </div>
      </div>
    </>
  );
}

export default SpaceEdit;
