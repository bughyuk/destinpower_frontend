import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import React from 'react';
import classNames from 'classnames';
import { useActiveMenu } from '@/modules/setup/hook';

type LeftMenuProps = {
  menuIdx: number;
  titleKey: string;
  iconName: string;
  active: boolean;
};

function LeftMenu({ menuIdx, titleKey, iconName, active }: LeftMenuProps) {
  const { t } = useTranslation();
  const { handleMenuActive } = useActiveMenu();

  return (
    <li className="sidebar-menu-item">
      <a
        onClick={() => handleMenuActive(menuIdx)}
        className={classNames({
          'sidebar-menu-button': true,
          active: active,
        })}
      >
        <MaterialIcon
          name={iconName}
          outlined={true}
          className="sidebar-menu-icon sidebar-menu-icon--left"
        />
        <span className="sidebar-menu-text">{t(titleKey)}</span>
      </a>
    </li>
  );
}

export default LeftMenu;
