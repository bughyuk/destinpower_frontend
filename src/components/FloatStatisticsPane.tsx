import React, { ReactElement } from 'react';
import { useControlProject } from '@/modules/map/hook';
import { SOLUTION_TYPE_SMART_FACTORY } from '@/modules/project/types';
import FloatStatisticsSmartOffice from '@/components/FloatStatisticsSmartOffice';
import FloatStatisticsSmartFactory from '@/components/FloatStatisticsSmartFactory';

function FloatStatisticsPane() {
  const { project } = useControlProject();

  let content: ReactElement;
  switch (project.solutionType) {
    case SOLUTION_TYPE_SMART_FACTORY:
      content = <FloatStatisticsSmartFactory />;
      break;
    default:
      content = <FloatStatisticsSmartOffice />;
      break;
  }

  return <>{content}</>;
}

export default FloatStatisticsPane;
