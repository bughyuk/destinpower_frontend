import React, { ReactElement, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import PhysicalDistributionClientList from '@/components/PhysicalDistributionClientList';
import PhysicalDistributionClientRegister from '@/components/PhysicalDistributionClientRegister';
import PhysicalDistributionClientEdit from '@/components/PhysicalDistributionClientEdit';
import {
  Client,
  PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT,
  PROCESS_CATEGORY_COMPLETED_PROCESS,
} from '@/modules/physical_distribution/types';
import ProcessProductLineList from '@/components/ProcessProductLineList';
import ProcessProductLineRegister from '@/components/ProcessProductLineRegister';
import ProcessCheckList from '@/components/ProcessCheckList';
import ProcessWorkingListRegister from './ProcessWorkingListRegister';
import { Process } from '@/modules/physical_distribution/types';

function ProcessCheck({
  categoryIdx,
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  const { t } = useTranslation();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_LIST);
  const [process, setProcess] = useState<Process>({
    processId: '',
    processName: '',
    processPname: '',
    workingTime: '',
    endTime: '',
    serialNumber: '',
    deleteFlag: false,
  });

  let content: ReactElement = <></>;
  if (status === PANE_STATUS_LIST) {
    content = (
      <ProcessCheckList
        categoryIdx={categoryIdx}
        onChangeStatus={setStatus}
        onChangeProcess={setProcess}
      />
    );
  } else if (status === PANE_STATUS_REGISTER || status === PANE_STATUS_EDIT) {
    content = (
      <ProcessWorkingListRegister
        onChangeStatus={setStatus}
        process={process}
      />
    );
  }

  let displayTitle = title;
  if (status === PANE_STATUS_REGISTER) {
    displayTitle = '공정 체크리스트 추가';
  } else if (status === PANE_STATUS_EDIT) {
    displayTitle = '공정 체크리스트 수정';
  }

  const handleClickBack = () => {
    if (status === PANE_STATUS_LIST) {
      onClickBack?.call(null);
    } else {
      setStatus(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={handleClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{displayTitle}</h3>
          </div>
          {status === PANE_STATUS_LIST && (
            <a
              className="btn btn-outline-dark ml-auto"
              onClick={() => {
                setProcess({
                  processId: '',
                  processName: '',
                  processPname: '',
                  workingTime: '',
                  endTime: '',
                  serialNumber: '',
                  deleteFlag: false,
                });
                setStatus(PANE_STATUS_REGISTER);
              }}
            >
              추가
            </a>
          )}
        </div>
      </div>
      {content}
    </>
  );
}

export default ProcessCheck;
