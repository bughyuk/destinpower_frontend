import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { MENU_LOGISTICS_MANAGEMENT } from '@/modules/home/types';
import { useHomeMenu } from '@/modules/home/hook';
import { useLogout } from '@/modules/access/logout/hook';

function HomeNavbar() {
  const { t } = useTranslation();
  const history = useHistory();
  const { handleSetMenu } = useHomeMenu();
  const { handleLogout, logoutStatusCode } = useLogout();

  useEffect(() => {
    if (logoutStatusCode) {
      history.replace('/signin');
    }
  }, [logoutStatusCode]);

  return (
    <>
      <nav className="dashboard-nav">
        <a
          className="navbar-brand mr-16pt"
          onClick={() => handleSetMenu(MENU_LOGISTICS_MANAGEMENT)}
        >
          <img src="/static/images/logo.svg" alt="logo" className="img-fluid" />
        </a>
        <div className="top-menu-cover">
          <div className="top-menu">
            <a onClick={handleLogout} className="menu-item text-primary">
              {t('text_logout')}
            </a>
            {/*
            <a className="menu-item">
              <span className="material-icons icon--left">code_off</span>
              Developers
            </a>
            */}
          </div>
        </div>
      </nav>
    </>
  );
}

export default HomeNavbar;
