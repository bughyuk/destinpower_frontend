import React, { useEffect, useState } from 'react';
import { ModalProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { Modal, ModalBody, ModalFooter, ModalTitle } from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/ModalHeader';
import { WarehousingQRCodePrintingData } from '@/modules/physical_distribution/types';
import classNames from 'classnames';

type QRCodePrintingModalProps = ModalProps & {
  qrCodePrintingData: WarehousingQRCodePrintingData;
  qrCodePrintingCompleteFlag: boolean;
};

function QRCodePrintingModal({
  show,
  qrCodePrintingData,
  qrCodePrintingCompleteFlag,
  onHide,
}: QRCodePrintingModalProps) {
  const { t } = useTranslation();
  const [printingCount, setPrintingCount] = useState(0);

  useEffect(() => {
    if (show) {
      if (printingCount + 1 > qrCodePrintingData.totalLogisticsQuantity) {
        return;
      }
      setPrintingCount(printingCount + 1);
    }
  }, [show, qrCodePrintingCompleteFlag]);

  const handleClickConfirm = () => {
    if (printingCount === qrCodePrintingData.totalLogisticsQuantity) {
      setPrintingCount(0);
      onHide();
    }
  };

  return (
    <Modal
      show={show}
      onHide={() => {
        //
      }}
      onExited={() => {
        setPrintingCount(0);
      }}
      centered={true}
    >
      <ModalHeader>
        <ModalTitle as={'h5'}>{t('text_printing_qr_code')}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="qr-cover">
          <img src={qrCodePrintingData.image} className="img-qr-code" />
          <div className="d-flex flex-row align-items-center justify-content-center py-2 font-size-24pt font-weight-bolder">
            <span className="text-accent mr-2">{printingCount}</span> /{' '}
            {qrCodePrintingData.totalLogisticsQuantity}
          </div>
        </div>
        <table className="table mb-4 thead-bg-light">
          <thead>
            <tr>
              <th>{t('text_product_title')}</th>
              <th>{t('text_warehousing_date')}</th>
              <th>{t('text_box_quantity')}</th>
            </tr>
          </thead>
          <tbody>
            {qrCodePrintingData.productList.map((product, i) => (
              <tr key={i}>
                <td>
                  {`${product.productName} (${product.productSizeName} / ${t(
                    'text_standard_each_number_of_product',
                    {
                      number: product.boxQuantity,
                    }
                  )})`}
                </td>
                <td>{qrCodePrintingData.warehousingDate}</td>
                <td>{product.logisticsQuantity}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </ModalBody>
      <ModalFooter>
        <button
          type="button"
          className={classNames('btn btn-accent', {
            disabled:
              printingCount !== qrCodePrintingData.totalLogisticsQuantity,
          })}
          onClick={handleClickConfirm}
        >
          {t('text_confirm')}
        </button>
      </ModalFooter>
    </Modal>
  );
}

export default QRCodePrintingModal;
