import React from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import PhysicalDistributionReleaseProductList from '@/components/PhysicalDistributionReleaseProductList';

function PhysicalDistributionRelease({
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={onClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{title}</h3>
          </div>
        </div>
      </div>
      <PhysicalDistributionReleaseProductList />
    </>
  );
}

export default PhysicalDistributionRelease;
