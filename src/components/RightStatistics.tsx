import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { Collapse } from 'react-bootstrap';
import { useActiveMenu, useRightPane } from '@/modules/setup/hook';
import { MENU_IDX_STATISTICS } from '@/utils/constants/common';
import { useTranslation } from 'react-i18next';

function RightStatistics() {
  const { t } = useTranslation();
  const { handleChangeShow } = useRightPane();
  const { handleMenuActive } = useActiveMenu();
  const perfectScrollbarRef = useRef<PerfectScrollbar>(null);

  useEffect(() => {
    const perfectScrollbar = perfectScrollbarRef.current;
    if (perfectScrollbar) {
      const container = (perfectScrollbar as any)._container;
      container.scrollTop = 0;
    }
  }, []);

  const handleClickViewStatistics = () => {
    handleChangeShow(false);
    handleMenuActive(MENU_IDX_STATISTICS);
  };

  return (
    <>
      <div className="accordion js-accordion accordion--boxed list-group-flush">
        <VisitorTable />
        <CongestionAreaTable />
        <NoticeTable />
      </div>
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-accent ml-0"
            onClick={handleClickViewStatistics}
          >
            {t('text_see_more_statistics')}
          </a>
        </div>
      </div>
    </>
  );
}

function VisitorTable() {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(true);
  const [isMore, setMore] = useState(false);

  return (
    <div
      className={classNames('accordion__item', {
        open: isOpen,
      })}
    >
      <a
        className="accordion__toggle collapsed"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="flex d-flex align-items-center">
          {t('text_number_of_people_residence_in_real_time')}{' '}
          <small className="text-muted ml-1">
            ({t('text_unit_of_person')})
          </small>
        </span>
        <span className="accordion__toggle-icon material-icons">
          keyboard_arrow_down
        </span>
      </a>
      <Collapse in={isOpen}>
        <div
          className={classNames('accordion__menu more-cover ac-item01', {
            show: isOpen,
            clicked: isMore,
          })}
        >
          <div className="inner">
            <div className="accordion__menu-link px-0 state-up">
              <div className="visitor-group">
                <ul className="visitor-list">
                  <li>
                    {t('text_total_en')}
                    <span>36</span>
                  </li>
                  <li>
                    {t('text_authorized_visitor')}
                    <span>35</span>
                  </li>
                  <li>
                    {t('text_temporary_visitor')}
                    <span>1</span>
                  </li>
                </ul>
              </div>
            </div>
            <div className="accordion__menu-link state-up active">
              <div className="panel-list">
                <a className="w-100">
                  <h6>
                    {`${t('text_development_department')} ${t('text_area')}`}{' '}
                    <span className="d-flex badge badge-accent ml-auto">
                      {t('text_confusion')}
                    </span>
                  </h6>
                  <ul>
                    <li>
                      {t('text_authorized_visitor')} <span>13</span>
                    </li>
                    <li>
                      {t('text_temporary_visitor')} <span>0</span>
                    </li>
                  </ul>
                </a>
              </div>
            </div>
            <div className="accordion__menu-link state-up">
              <div className="panel-list">
                <a className="w-100">
                  <h6>{`${t('text_design_department')} ${t('text_area')}`}</h6>
                  <ul>
                    <li>
                      {t('text_authorized_visitor')} <span>7</span>
                    </li>
                    <li>
                      {t('text_temporary_visitor')} <span>0</span>
                    </li>
                  </ul>
                </a>
              </div>
            </div>
          </div>
          {/*
          <div className="d-flex justify-content-center more-btn-cover">
            <button
              type="button"
              className="btn btn-link text-secondary btn-more"
              onClick={() => setMore(!isMore)}
            ></button>
          </div>
          */}
        </div>
      </Collapse>
    </div>
  );
}

function CongestionAreaTable() {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(true);
  const [isMore, setMore] = useState(false);

  return (
    <div
      className={classNames('accordion__item', {
        open: isOpen,
      })}
    >
      <a
        className="accordion__toggle collapsed"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="flex d-flex align-items-center">
          {t('text_real_time_congestion_zone')}
        </span>
        <span className="accordion__toggle-icon material-icons">
          keyboard_arrow_down
        </span>
      </a>
      <Collapse in={isOpen}>
        <div
          className={classNames('accordion__menu more-cover ac-item02', {
            show: isOpen,
            clicked: isMore,
          })}
        >
          <div className="inner">
            <div className="accordion__menu-link">
              <a className="d-flex align-items-center w-100">
                <p className="d-flex m-0">{`${t(
                  'text_development_department'
                )} ${t('text_area')}`}</p>
                <span className="d-flex badge badge-accent ml-auto">
                  {t('text_confusion')}
                </span>
              </a>
            </div>
            <div className="accordion__menu-link">
              <a className="d-flex align-items-center w-100">
                <p className="d-flex m-0">{`${t('text_design_department')} ${t(
                  'text_area'
                )}`}</p>
                <span className="d-flex badge badge-success ml-auto">
                  {t('text_smoothly')}
                </span>
              </a>
            </div>
          </div>
          {/*
          <div className="d-flex justify-content-center more-btn-cover">
            <button
              type="button"
              className="btn btn-link text-secondary btn-more"
              onClick={() => setMore(!isMore)}
            ></button>
          </div>
          */}
        </div>
      </Collapse>
    </div>
  );
}

function NoticeTable() {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(true);

  return (
    <div
      className={classNames('accordion__item', {
        open: isOpen,
      })}
    >
      <a
        className="accordion__toggle collapsed"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="flex d-flex align-items-center">
          {t('text_notice')}
        </span>
        <span className="accordion__toggle-icon material-icons">
          keyboard_arrow_down
        </span>
      </a>
      <Collapse in={isOpen}>
        <div
          className={classNames('accordion__menu more-cover ac-item01', {
            show: isOpen,
          })}
        >
          <div className="accordion__menu-link state-up">
            <a className="d-flex align-items-center flex-row w-100">
              <span className="material-icons-outlined text-accent mr-1 font-size-16pt">
                info
              </span>
              <p className="d-flex m-0 text-accent pr-4">
                {t('msg_notice_sample')}
              </p>
              <span className="d-flex text-50 ml-auto">2021.07.05</span>
            </a>
          </div>
        </div>
      </Collapse>
    </div>
  );
}

export default RightStatistics;
