import React, { useEffect, useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_EDIT } from '@/utils/constants/common';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import {
  dp_deleteFactory,
  dp_fetchFactorys,
} from '@/api/physical_distribution';
// import { useControlProject } from '@/modules/map/hook';
import { Factory } from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';

type PhysicalDistributionFactoryListProps = PaneProps & {
  onChangeFactory: (factory: Factory) => void;
};

function ProcessFactoryList({
  onChangeStatus,
  onChangeFactory,
}: PhysicalDistributionFactoryListProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [load, setLoad] = useState(false);
  const [factoryList, setFactoryList] = useState<Factory[]>([]);
  const [filterList, setFilterList] = useState<Factory[]>([]);
  const [activeId, setActiveId] = useState('');

  useEffect(() => {
    handleFetchFactoryList();
  }, []);

  useEffect(() => {
    setActiveId('');
    setFilterList(
      factoryList.filter(
        (factory) => factory.name.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, factoryList]);

  const handleFetchFactoryList = async () => {
    setLoad(false);
    const data = await dp_fetchFactorys(''); //검색어없는
    setFactoryList(data);
    setLoad(true);
  };

  const handleClickEdit = (factory: Factory) => {
    onChangeFactory(factory);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteFactory = async (factoryId: string) => {
    const result = await dp_deleteFactory(factoryId);

    if (result) {
      handleFetchFactoryList();
    }
  };

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'검색어를 입력해 주세요.'}
          onSubmit={setSearchKeyword}
        />
      </div>
      <div className="list-wide-cover">
        <div className="mb-4">
          {!load && <></>}
          {load && filterList.length === 0 && (
            <em className="none-list mb-4">{t('공장이 없습니다.')}</em>
          )}
          {load && filterList.length > 0 && (
            <ul className="sidebar-menu">
              {filterList.map((factory) => (
                <FactoryItem
                  key={factory.id}
                  {...factory}
                  activeId={activeId}
                  onClick={(factoryId) => {
                    let value = '';
                    if (factoryId !== activeId) {
                      value = factoryId;
                    }
                    setActiveId(value);
                  }}
                  onClickEdit={() => {
                    handleClickEdit(factory);
                  }}
                  onClickDelete={handleDeleteFactory}
                />
              ))}
            </ul>
          )}
        </div>
      </div>
    </>
  );
}

type FactoryItemProps = Factory & {
  activeId: string;
  onClick: (factoryId: string) => void;
  onClickEdit: () => void;
  onClickDelete: (factoryId: string) => void;
};

function FactoryItem({
  id,
  name,
  memo,
  status,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
}: FactoryItemProps) {
  const { t } = useTranslation();
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  return (
    <>
      <li
        className={classNames('sidebar-menu-item', {
          open: id === activeId,
        })}
      >
        <div className="d-flex flex-row">
          <a
            className={classNames('sidebar-menu-button', {
              'text-30': status === 'd',
            })}
            onClick={() => onClick(id)}
          >
            {status === 'd' && `[${t('text_delete')}] `} {name}
            <span className="sidebar-menu-badge ml-auto mr-2"></span>
            <span className="sidebar-menu-toggle-icon"></span>
          </a>
        </div>
        <Collapse in={id === activeId}>
          <div className="sidebar-submenu sm-indent">
            <div className="cover">
              <div className="col-12 p-2">
                <span className="text-50">공장명</span>
                <p className="m-0">{name}</p>
              </div>
              <div className="col-12 p-2">
                <span className="text-50">{t('text_memo')}</span>
                <p
                  className="m-0"
                  dangerouslySetInnerHTML={{
                    __html: memo || '-',
                  }}
                ></p>
              </div>
              <div className="btn-cover mt-2">
                {status === 'n' && (
                  <>
                    <a
                      className="btn"
                      onClick={() => setShowConfirmModal(true)}
                    >
                      {t('text_delete')}
                    </a>
                    <a className="btn" onClick={() => onClickEdit()}>
                      {t('text_edit')}
                    </a>
                  </>
                )}
              </div>
            </div>
          </div>
        </Collapse>
      </li>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(id)}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

export default ProcessFactoryList;
