import React from 'react';
import classNames from 'classnames';

type ToggleButtonProps = {
  id?: string;
  className?: string;
  checked?: boolean;
  onChange?: (id: string, checked: boolean) => void;
};

function ToggleButton({ id, className, checked, onChange }: ToggleButtonProps) {
  return (
    <div
      className={classNames('custom-control custom-checkbox-toggle', className)}
    >
      <input
        id={id}
        type="checkbox"
        checked={checked}
        onChange={(e) => {
          onChange?.call(null, e.target.id, e.target.checked);
        }}
        className="custom-control-input"
      />
      <label className="custom-control-label" htmlFor={id}></label>
    </div>
  );
}

export default ToggleButton;
