import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import InputNumber from '@/components/InputNumber';
import moment from 'moment';
import {
  usePhysicalDistributionClient,
  usePhysicalDistributionProduct,
  usePhysicalDistributionStatus,
} from '@/modules/physical_distribution/hook';
import { ReleaseProduct } from '@/modules/physical_distribution/types';
import AlertModal from '@/components/AlertModal';
import NewlineText from '@/components/NewlineText';
import { CommonUtils } from '@/utils';
import ConfirmModal from '@/components/ConfirmModal';
import { useControlProject } from '@/modules/map/hook';
import { postRelease } from '@/api/physical_distribution';
import PhysicalDistributionFlatpickr from '@/components/PhysicalDistributionFlatpickr';
import classNames from 'classnames';

const today = moment().format('YYYY-MM-DD');
function FloatPhysicalDistributionRelease() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const {
    load: loadClientList,
    releaseClientList,
  } = usePhysicalDistributionClient();
  const {
    selectedProduct,
    handleSetProduct,
    handleChangeReloadFlag: handleChangeProductReloadFlag,
  } = usePhysicalDistributionProduct();
  const {
    handleChangeReloadFlag: handleChangeStatusReloadFlag,
  } = usePhysicalDistributionStatus();
  const [inputs, setInputs] = useState<{
    date: string;
    clientId: string;
    boxCategoryId: string;
    warehouseId: string;
    warehouseName: string;
  }>({
    date: today,
    clientId: '',
    boxCategoryId: '',
    warehouseId: '',
    warehouseName: '',
  });
  const [productList, setProductList] = useState<ReleaseProduct[]>([]);
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [showAlertModal, setShowAlertModal] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  useEffect(() => {
    if (selectedProduct) {
      const selectedReleaseProduct = selectedProduct as ReleaseProduct;
      handleSetProduct(null);
      let message = '';
      if (
        productList.find(
          (product) =>
            product.warehouseId === selectedReleaseProduct.warehouseId &&
            product.boxCategoryId === selectedReleaseProduct.boxCategoryId
        )
      ) {
        message = t('msg_already_been_added_product');
      } else if (productList.length) {
        message = t('msg_can_be_one_product_release');
      }

      if (message) {
        handleShowAlertModal(message);
        return;
      }

      setInputs({
        ...inputs,
        boxCategoryId: selectedReleaseProduct.boxCategoryId,
        warehouseId: selectedReleaseProduct.warehouseId,
        warehouseName: selectedReleaseProduct.warehouseName,
      });

      setProductList([
        ...productList,
        {
          ...selectedReleaseProduct,
        },
      ]);
    }
  }, [selectedProduct]);

  useEffect(() => {
    if (releaseClientList.length) {
      const firstClient = releaseClientList[0];
      setInputs({
        ...inputs,
        clientId: firstClient.customerId,
      });
    }
  }, [releaseClientList]);

  const handleShowAlertModal = (message: string) => {
    setAlertMessage(message);
    setShowAlertModal(true);
  };

  const handleChangeValue = (
    changeIndex: number,
    field: 'logisticsQuantity',
    value: string | number
  ) => {
    setProductList(
      productList.map((product, index) => {
        if (changeIndex === index) {
          switch (field) {
            case 'logisticsQuantity':
              product[field] = Number(value);
              break;
          }
        }

        return product;
      })
    );
  };

  const handleClickRemove = (clickedIndex: number) => {
    setProductList(
      productList.filter((product, index) => index !== clickedIndex)
    );
  };

  const handleClickInstruction = () => {
    if (productList.length) {
      let totalLogisticsQuantity = 0;
      productList.forEach(({ productId, logisticsQuantity }) => {
        totalLogisticsQuantity += logisticsQuantity;
      });
      if (!totalLogisticsQuantity) {
        handleShowAlertModal(
          t('msg_release_instruction_with_more_than_one_box')
        );
      } else {
        setShowConfirmModal(true);
      }
    } else {
      handleShowAlertModal(t('msg_add_product_before_proceeding'));
    }
  };

  const handleSubmit = async () => {
    let totalLogisticsQuantity = 0;
    productList.forEach(({ logisticsQuantity }) => {
      totalLogisticsQuantity += logisticsQuantity;
    });

    if (!totalLogisticsQuantity) {
      handleShowAlertModal(t('msg_release_instruction_with_more_than_one_box'));
      return;
    }

    const result = await postRelease({
      projectId: project.id,
      logisticsQuantity: totalLogisticsQuantity,
      warehouseId: inputs.warehouseId,
      boxCategoryId: inputs.boxCategoryId,
      customerId: inputs.clientId,
      dueDate: `${moment(inputs.date).format('YYYY-MM-DD')}T00:00:00`,
    });

    if (result) {
      handleChangeStatusReloadFlag();
      handleChangeProductReloadFlag();
      handleInitialInputs();
    }
  };

  const handleInitialInputs = () => {
    let clientId = '';
    if (releaseClientList.length) {
      const firstClient = releaseClientList[0];
      clientId = firstClient.customerId;
    }

    setInputs({
      ...inputs,
      date: today,
      clientId,
      warehouseId: '',
      warehouseName: '',
      boxCategoryId: '',
    });

    setProductList([]);
  };

  if (!loadClientList) {
    return <></>;
  }

  if (loadClientList && releaseClientList.length === 0) {
    return (
      <>
        <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
          <div className="flex title-row">
            <h3 className="d-flex align-items-center mb-0">
              {t('text_release_instruction')}
            </h3>
          </div>
        </div>
        <div className="contents-view">
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>
            <em className="empty-txt m-0">
              {t('msg_add_release_client_before_proceeding')}
            </em>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_release_instruction')}
          </h3>
          <span className="text-muted text-headings text-uppercase">
            {t('msg_release_description')}
          </span>
        </div>
        <a
          className="btn btn-rounded btn-outline-dark ml-2"
          onClick={handleClickInstruction}
        >
          <MaterialIcon name={'arrow_downward'} align={'left'} />
          {t('text_release_instruction')}
        </a>
      </div>
      <div className="contents-view">
        <div className="page-separator mt-5">
          <div className="page-separator__text">
            {t('text_release_information')}
          </div>
        </div>
        {productList.length === 0 && (
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>

            <em className="empty-txt m-0">
              {t('msg_selection_product_from_the_list_on_the_left')}
            </em>
          </div>
        )}
        {productList.length > 0 && (
          <>
            <table className="table mb-4 thead-bg-light">
              <thead>
                <tr>
                  <th>{t('text_release_date')}</th>
                  <th>{t('text_client')}</th>
                  <th>{t('text_warehouse')}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <PhysicalDistributionFlatpickr
                      value={inputs.date}
                      onChange={(value) =>
                        setInputs({
                          ...inputs,
                          date: value,
                        })
                      }
                    />
                  </td>
                  <td>
                    <select
                      className="form-control custom-select font-weight-bolder w-auto"
                      onChange={(e) =>
                        setInputs({
                          ...inputs,
                          clientId: e.target.value,
                        })
                      }
                      value={inputs.clientId}
                    >
                      {releaseClientList.map(({ customerId, customerName }) => (
                        <option key={customerId} value={customerId}>
                          {customerName}
                        </option>
                      ))}
                    </select>
                  </td>
                  <td>{inputs.warehouseName}</td>
                </tr>
              </tbody>
            </table>
            <div className="page-separator mt-5">
              <div className="page-separator__text">
                {t('text_product_information')}
              </div>
            </div>
            <table className="table mb-4 thead-bg-light">
              <colgroup>
                <col width="*" />
                <col width="15%" />
                <col width="5%" />
              </colgroup>
              <thead>
                <tr>
                  <th>{t('text_product_information')}</th>
                  <th>{t('text_box_quantity')}</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                {productList.map((product, i) => (
                  <ReleaseProductItem
                    key={i}
                    {...product}
                    onChangeValue={(field, value) => {
                      handleChangeValue(i, field, value);
                    }}
                    onClickRemove={() => handleClickRemove(i)}
                  />
                ))}
              </tbody>
            </table>
          </>
        )}
      </div>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={handleSubmit}
      >
        <div className="py-4">
          <div className="text-center">
            <h3>
              {t('msg_sure_want_to_release_instruction_in_quantity_boxes', {
                quantity: productList.reduce(
                  (a, b) => +a + b.logisticsQuantity,
                  0
                ),
              })}
            </h3>
          </div>
          <div className="d-flex justify-content-center">
            {productList.map(
              ({ productName, imgUrl, productWeight, boxQuantity }, i) => (
                <div
                  key={i}
                  className="media flex-nowrap align-items-center border-1 p-3"
                >
                  <span className="avatar avatar-sm mr-2 blank-img">
                    <img
                      src={imgUrl}
                      className={classNames('avatar-img', {
                        'rounded-circle': imgUrl,
                      })}
                    />
                  </span>
                  <div className="media-body">
                    <strong className="text-dark">{productName}</strong>
                    <div className="text-muted small">
                      {CommonUtils.getProductStandard(
                        productWeight,
                        boxQuantity
                      )}
                    </div>
                  </div>
                </div>
              )
            )}
          </div>
        </div>
      </ConfirmModal>
      <AlertModal show={showAlertModal} onHide={() => setShowAlertModal(false)}>
        <NewlineText text={alertMessage} />
      </AlertModal>
    </>
  );
}

type ReleaseProductItemProps = ReleaseProduct & {
  onChangeValue: (field: 'logisticsQuantity', value: string | number) => void;
  onClickRemove: () => void;
};

function ReleaseProductItem({
  productName,
  imgUrl,
  productSizeName,
  productWeight,
  boxQuantity,
  usableQuantity,
  logisticsQuantity,
  onChangeValue,
  onClickRemove,
}: ReleaseProductItemProps) {
  const { t } = useTranslation();

  return (
    <tr>
      <td>
        <div className="flex d-flex align-items-center">
          <a className="avatar mr-12pt blank-img">
            <img src={imgUrl} className="avatar-img rounded" />
          </a>
          <div className="flex list-els">
            <h6 className="m-0">{productName}</h6>
            <div className="card-subtitle text-50">
              <small className="mr-2">{productSizeName}</small>
              <small className="mr-2">
                {CommonUtils.getProductStandard(productWeight, boxQuantity)}
              </small>
            </div>
          </div>
        </div>
      </td>
      <td>
        <div className="d-inline-flex align-items-center">
          <InputNumber
            className="form-control font-weight-bolder"
            style={{
              width: '6rem',
            }}
            value={logisticsQuantity}
            max={usableQuantity}
            onChange={(value) => onChangeValue('logisticsQuantity', value)}
          />
          <span className="ml-2 text-50">{t('text_box_en')}</span>
        </div>
      </td>
      <td>
        <a className="circle-pin-sm p-2" onClick={onClickRemove}>
          <span className="material-icons font-size-16pt">close</span>
        </a>
      </td>
    </tr>
  );
}

export default FloatPhysicalDistributionRelease;
