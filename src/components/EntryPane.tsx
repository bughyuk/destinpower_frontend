import React, { useEffect, useState } from 'react';
import {
  PANE_STATUS_DETAIL,
  PANE_STATUS_STATUS_BOARD,
  PaneStatus,
} from '@/utils/constants/common';
import { useLeftPaneContainer } from '@/modules/setup/hook';
import EntryStatus from '@/components/EntryStatus';
import EntryDetail from '@/components/EntryDetail';

function EntryPane() {
  return (
    <div className="tab-pane sm-entry active">
      <div className="entry-inner">
        <EntryContent />
      </div>
    </div>
  );
}

function EntryContent() {
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_STATUS_BOARD);
  const { updateScroll } = useLeftPaneContainer();
  useEffect(() => {
    updateScroll();
  }, [status]);

  return (
    <>
      {status === PANE_STATUS_STATUS_BOARD && (
        <EntryStatus onChangeStatus={setStatus} />
      )}
      {status === PANE_STATUS_DETAIL && (
        <EntryDetail onChangeStatus={setStatus} />
      )}
    </>
  );
}

export default EntryPane;
