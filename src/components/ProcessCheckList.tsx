import React, { useEffect, useState, useRef } from 'react';
import { PaneProps, ApiResult } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import { deleteClient, fetchClients } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import {
  CheckList,
  Client,
  PROCESS_CATEGORY_COMPLETED_PROCESS,
  PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT,
  Category,
  PROCESS_CATEGORY_PROCESS_CHECK,
  Process,
} from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';

import { useDropdown } from '@/modules/common';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
} from '@/utils/constants/common';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import { useFloatPane } from '@/modules/setup/hook';
import {
  fetchProcessCheckList,
  fetchProcessWorkingList,
  postProcessWorkingDelete,
  postProcessCheckListDelete,
} from '@/api/process';

type PhysicalDistributionClientListProps = PaneProps & {
  categoryIdx?: Category;
  onChangeProcess?: (process: Process) => void;
};

function ProcessCheckList({
  categoryIdx,
  onChangeStatus,
  onChangeProcess,
}: PhysicalDistributionClientListProps) {
  let content = <></>;
  switch (categoryIdx) {
    case PROCESS_CATEGORY_PROCESS_CHECK:
      content = (
        <ProcessWorkingList
          onChangeStatus={onChangeStatus}
          onChangeProcess={onChangeProcess}
        />
      );
      break;
    case PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT:
      content = <ProcessCheckListManagement onChangeStatus={onChangeStatus} />;
      break;
  }

  return <>{content}</>;
}

function ProcessWorkingList({
  onChangeStatus,
  onChangeProcess,
}: PhysicalDistributionClientListProps) {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [load, setLoad] = useState(false);
  const { handleSetCheckListId } = useFloatPane();

  // 공정 리스트
  const [processList, setProcessList] = useState<Process[]>([]);
  const [filterList, setFilterList] = useState<Process[]>([]);
  const [activeId, setActiveId] = useState('');

  useEffect(() => {
    handleFetchProcessList();
  }, []);

  useEffect(() => {
    setActiveId('');
    setFilterList(
      processList.filter(
        (process) =>
          process.processName.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, processList]);

  const handleFetchProcessList = async () => {
    setLoad(false);

    try {
      const processes = await fetchProcessWorkingList(searchKeyword);
      setProcessList(processes);
      setLoad(true);
    } catch (error) {
      alert(error);
    }
  };

  const handleClickEdit = (process: Process) => {
    //onChangeClient(process); // change to onchangeprocess
    if (onChangeProcess) onChangeProcess(process);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteProcess = async (processId: string) => {
    try {
      const result = await postProcessWorkingDelete(processId);

      if (result) {
        handleFetchProcessList();
      }
    } catch (err) {
      alert(err);
    }
  };

  return (
    <>
      <div className="list-opt-box">
        <div className="cell serach-box">
          <input
            type="text"
            placeholder="제품명, ID검색"
            value={searchKeyword}
            onChange={(e) => setSearchKeyword(e.target.value)}
          />
          <a
            className="btn btn-secondary btn-sm ml-auto"
            onClick={handleFetchProcessList}
          >
            {t('text_search')}
          </a>
        </div>
      </div>
      <div className="list-wide-cover">
        <div className="mb-4">
          {!load && <></>}
          {load && filterList.length === 0 && (
            <em className="none-list mb-4">
              {t('공정 체크리스트가 없습니다.')}
            </em>
          )}
          {load && filterList.length > 0 && (
            <div className="list-group list-group-flush mb-4">
              {filterList.map((process) => (
                <ProcessItem
                  key={process.processId}
                  {...process}
                  activeId={activeId}
                  onClick={(processId: string) => {
                    let value = '';
                    if (processId !== activeId) {
                      value = processId;
                    }

                    setActiveId(value);
                    console.log('setchecklist: ' + value);
                    handleSetCheckListId(value);
                  }}
                  onClickEdit={() => {
                    handleClickEdit(process);
                  }}
                  onClickDelete={handleDeleteProcess}
                />
              ))}
            </div>
          )}
        </div>
      </div>
    </>
  );
}

function ProcessCheckListManagement({
  onChangeStatus,
}: PhysicalDistributionClientListProps) {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [load, setLoad] = useState(false);
  const { handleSetCheckListId } = useFloatPane();

  const [checklistList, setChecklistList] = useState<CheckList[]>([]);
  const [filterList, setFilterList] = useState<CheckList[]>([]);
  const [activeId, setActiveId] = useState('');

  useEffect(() => {
    handleFetchCheckList();
  }, []);

  useEffect(() => {
    setActiveId('');
    setFilterList(
      checklistList.filter(
        (c) => c.checkListName.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, checklistList]);

  const handleFetchCheckList = async () => {
    setLoad(false);

    try {
      const checklist: CheckList[] = await fetchProcessCheckList(searchKeyword);
      setChecklistList(checklist);
      setLoad(true);
    } catch (error) {
      alert(error);
    }
  };

  const handleClickEdit = (checklist: CheckList) => {
    //onChangeCheckList(checklist);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteCheckList = async (checkListId: string) => {
    try {
      const result = await postProcessCheckListDelete(checkListId);

      if (result) {
        handleFetchCheckList();
      }
    } catch (err) {
      alert(err);
    }
  };

  return (
    <>
      <div className="list-opt-box">
        <div className="cell serach-box">
          <input
            type="text"
            placeholder="검색어를 입력해 주세요."
            value={searchKeyword}
            onChange={(e) => setSearchKeyword(e.target.value)}
          />
          <a
            className="btn btn-secondary btn-sm ml-auto"
            onClick={handleFetchCheckList}
          >
            {t('text_search')}
          </a>
        </div>
      </div>
      <div className="list-wide-cover">
        <div className="mb-4">
          {!load && <></>}
          {load && filterList.length === 0 && (
            <em className="none-list mb-4">
              {t('공정 체크리스트가 없습니다.')}
            </em>
          )}
          {load && filterList.length > 0 && (
            <div className="list-group list-group-flush mb-4">
              {filterList.map((checklist) => (
                <CheckListItem
                  key={checklist.checkListId}
                  {...checklist}
                  activeId={activeId}
                  onClick={(checklistId) => {
                    let value = '';
                    if (checklistId !== activeId) {
                      value = checklistId;
                    }

                    setActiveId(value);
                    handleSetCheckListId(value);
                  }}
                  onClickEdit={() => {
                    handleClickEdit(checklist);
                  }}
                  onClickDelete={handleDeleteCheckList}
                />
              ))}
            </div>
          )}
        </div>
      </div>
    </>
  );
}

type CheckListItemProps = CheckList & {
  activeId: string;
  onClick: (checklistId: string) => void;
  onClickEdit: () => void;
  onClickDelete: (checklistId: string) => void;
};

function CheckListItem({
  checkListId,
  checkListName,
  registerTime,
  updateTime,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
  status,
}: CheckListItemProps) {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  const handleSelectFunction = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        //handleChangePaneStatus(PANE_STATUS_EDIT);

        break;
      case FUNCTION_DELETE:
        setShowConfirmModal(true);
        break;
    }
  };

  return (
    <>
      <a onClick={() => onClick(checkListId.toString())}>
        <div
          className={classNames('list-group-item d-flex', {
            active: checkListId.toString() === activeId,
          })}
        >
          <div className="flex d-flex align-items-center py-2">
            <div className="flex">
              <h4 className="card-title">{checkListName}</h4>
              <div className="card-subtitle text-50 d-flex align-items-center">
                <small className="mr-2">{registerTime}</small>
                <small className="mr-2">{updateTime}</small>
              </div>
            </div>
            <ButtonGroup>
              <Dropdown onToggle={handleToggle} onSelect={handleSelectFunction}>
                <DropdownToggle
                  as={'a'}
                  data-caret="false"
                  className="text-muted"
                >
                  <MaterialIcon name={'more_vert'} />
                </DropdownToggle>
                <DropdownMenu align={'right'} ref={dropdown}>
                  <Dropdown.Item
                    className="text-primary"
                    eventKey={FUNCTION_EDIT}
                  >
                    {t('text_edit')}
                  </Dropdown.Item>
                  <Dropdown.Item
                    className="text-danger"
                    eventKey={FUNCTION_DELETE}
                  >
                    {t('text_delete')}
                  </Dropdown.Item>
                </DropdownMenu>
              </Dropdown>
            </ButtonGroup>
          </div>
        </div>
      </a>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(checkListId.toString())}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

type ProcessItemProps = Process & {
  activeId: string;
  onClick: (processId: string) => void;
  onClickEdit: () => void;
  onClickDelete: (processId: string) => void;
};

function ProcessItem({
  processId,
  processName,
  serialNumber,
  processPname,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
  deleteFlag,
}: ProcessItemProps) {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  const handleSelectFunction = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        onClickEdit();
        break;
      case FUNCTION_DELETE:
        setShowConfirmModal(true);
        break;
    }
  };

  return (
    <>
      <a onClick={() => onClick(processId)}>
        <div
          className={classNames('list-group-item d-flex', {
            active: processId === activeId,
          })}
        >
          <div className="flex d-flex align-items-center py-2">
            <div className="flex">
              <h4 className="card-title">{processName}</h4>
              <div className="card-subtitle text-50 d-flex align-items-center">
                <small className="mr-2">{processPname}</small>
                {/* TODO: back 요청 */}
                {/* <small className="mr-2">{registerTime}</small> */}
              </div>
            </div>
            <ButtonGroup>
              <Dropdown onToggle={handleToggle} onSelect={handleSelectFunction}>
                <DropdownToggle
                  as={'a'}
                  data-caret="false"
                  className="text-muted"
                >
                  <MaterialIcon name={'more_vert'} />
                </DropdownToggle>
                <DropdownMenu align={'right'} ref={dropdown}>
                  <Dropdown.Item
                    className="text-primary"
                    eventKey={FUNCTION_EDIT}
                  >
                    {t('text_edit')}
                  </Dropdown.Item>
                  <Dropdown.Item
                    className="text-danger"
                    eventKey={FUNCTION_DELETE}
                  >
                    {t('text_delete')}
                  </Dropdown.Item>
                </DropdownMenu>
              </Dropdown>
            </ButtonGroup>
          </div>
        </div>
      </a>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(processId)}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

export default ProcessCheckList;
