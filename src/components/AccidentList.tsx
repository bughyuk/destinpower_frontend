import React, { useEffect, useState } from 'react';
import ListHeader from '@/components/ListHeader';
import Pagination from '@/components/Pagination';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import {
  FUNCTION_COPY,
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_ACCIDENT_DIRECT_REGISTER,
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import {
  Accident,
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
  AccidentCategory,
} from '@/modules/accident/types';
import DateBadge from '@/components/DateBadge';
import ListDropdown from '@/components/ListDropdown';
import {
  useControlAccident,
  useControlSpace,
  useControlProject,
} from '@/modules/map/hook';
import { deleteAccident, fetchAccidents } from '@/api/accident';
import EmptyPane from '@/components/EmptyPane';
import classNames from 'classnames';
import { Badge } from 'react-bootstrap';
import moment from 'moment';

type AccidentListProps = {
  onStatusChange: (status: PaneStatus) => void;
  onChangeEventId: (eventId: string) => void;
};

function AccidentList({ onStatusChange, onChangeEventId }: AccidentListProps) {
  const { t } = useTranslation();
  const { space } = useControlSpace();
  const { project } = useControlProject();
  const [load, setLoad] = useState(false);
  const [totalCount, setTotalCount] = useState(0);
  const [accidentList, setAccidentList] = useState<Accident[]>([]);
  const [page, setPage] = useState(1);

  const handleFetchAccident = async () => {
    const data = await fetchAccidents({
      page,
      projectId: project.id,
      mappingId: space.spaceMappingId,
      mapId: space.floorsMapId,
    });

    if (data) {
      setLoad(true);
      setAccidentList(data.content);
      setTotalCount(data.totalElements);
    }
  };

  useEffect(() => {
    if (space.spaceMappingId && space.floorsMapId) {
      setLoad(false);
      setAccidentList([]);
      setPage(1);
      handleFetchAccident();
    }
  }, [space]);

  useEffect(() => {
    handleFetchAccident();
  }, [page]);

  const handleReload = () => {
    handleFetchAccident();
  };

  if (!space.floorsMapId) {
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
  }

  return (
    <>
      {!load && <></>}
      {load && accidentList.length === 0 && (
        <EmptyPane
          textKey={'msg_accident_empty'}
          descriptionKey={'msg_accident_suggest_to_register'}
          btnTextKey={'text_accident_event_registration'}
          onClick={() => {
            onStatusChange(PANE_STATUS_REGISTER);
          }}
        />
      )}
      {load && accidentList.length > 0 && (
        <>
          <div className="container-fluid py-4">
            <div className="flex d-flex align-items-center">
              <a
                className="circle-pin pr-2"
                onClick={() =>
                  onStatusChange(PANE_STATUS_ACCIDENT_DIRECT_REGISTER)
                }
              >
                <MaterialIcon name={'arrow_back'} />
              </a>
              <div className="mr-24pt">
                <h3 className="mb-0">{t('text_accident_list')}</h3>
              </div>
              <a
                className="btn btn-outline-dark ml-auto"
                onClick={() => onStatusChange(PANE_STATUS_REGISTER)}
              >
                {t('text_new_registration')}
              </a>
            </div>
          </div>
          <div className="list-group row-list list-group-flush mb-4">
            {accidentList.map((accident) => (
              <AccidentItem
                key={accident.eventId}
                {...accident}
                onClick={() => {
                  onChangeEventId(accident.eventId);
                  onStatusChange(PANE_STATUS_DETAIL);
                }}
                onEditClick={() => {
                  onChangeEventId(accident.eventId);
                  onStatusChange(PANE_STATUS_EDIT);
                }}
                onReload={handleReload}
              />
            ))}
          </div>
          <Pagination
            curPage={page}
            totalCount={totalCount}
            onPageChange={(page) => {
              setPage(page);
            }}
          />
        </>
      )}
    </>
  );
}

type AccidentIconProps = {
  category: AccidentCategory;
  activeFlag: boolean;
};

function AccidentIcon({ category, activeFlag }: AccidentIconProps) {
  return (
    <a className="avatar">
      <span
        className={classNames('lead text-headings', {
          'text-iden': activeFlag,
          'text-secondary': !activeFlag,
        })}
      >
        {category === ACCIDENT_FIRE && (
          <MaterialIcon name={'local_fire_department'} />
        )}
        {category === ACCIDENT_EMERGENCY && <MaterialIcon name={'warning'} />}
        {category === ACCIDENT_INFECTIOUS_DISEASE && (
          <MaterialIcon name={'coronavirus'} />
        )}
        {category === ACCIDENT_RESCUE && (
          <MaterialIcon name={'local_hospital'} />
        )}
      </span>
    </a>
  );
}

type AccidentItemProps = Accident & {
  onClick: () => void;
  onEditClick: (eventId: string) => void;
  onReload: () => void;
};

function AccidentItem({
  eventId,
  eventDetailCategory,
  eventTitle,
  registDate,
  activeFlag,
  onClick,
  onEditClick,
  onReload,
}: AccidentItemProps) {
  const { t } = useTranslation();
  const { handleReloadActivationAccidents } = useControlAccident();

  const handleDeleteAccident = async () => {
    const data = await deleteAccident(eventId);
    if (data) {
      handleReloadActivationAccidents();
      onReload();
    }
  };

  const handleOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        onEditClick(eventId);
        break;
      case FUNCTION_COPY:
        break;
      case FUNCTION_DELETE:
        handleDeleteAccident();
        break;
    }
  };

  return (
    <div className="list-group-item d-flex" onClick={onClick}>
      <AccidentIcon category={eventDetailCategory} activeFlag={activeFlag} />
      <div
        className={classNames('flex d-flex align-items-center mr-16pt', {
          'item-disabled': !activeFlag,
        })}
      >
        <div className="flex list-els">
          <a
            className={classNames('card-title', {
              'text-50': !activeFlag,
            })}
          >
            {eventTitle}
          </a>
          <div className="card-subtitle text-50">
            {activeFlag && <DateBadge date={registDate} />}
            {!activeFlag && (
              <>
                <Badge variant={'secondary'}>
                  {t('text_end_of_situation')}
                </Badge>
                {` ${moment(registDate).format('YYYY.MM.DD')}`}
              </>
            )}
          </div>
        </div>
      </div>
      <ListDropdown showEdit={activeFlag} onSelect={handleOptions} />
    </div>
  );
}

export default AccidentList;
