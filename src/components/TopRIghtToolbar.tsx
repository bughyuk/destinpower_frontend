import React, { useEffect, useRef, useState } from 'react';
import {
  useControlProject,
  useControlSpace,
  useOpenLayers,
} from '@/modules/map/hook';
import MaterialIcon from '@/components/MaterialIcon';
import { Collapse, Dropdown } from 'react-bootstrap';
import { Space } from '@/modules/map/types';
import classNames from 'classnames';
import { CommonUtils } from '@/utils';
import { useTranslation } from 'react-i18next';
import { useControlMode, useSetupLayer } from '@/modules/setup/hook';
import { useDropdown } from '@/modules/common';
import {
  LAYER_AP,
  LAYER_DISTRIBUTION_MAP,
  LAYER_EMERGENCY,
  LAYER_FIRE,
  LAYER_HEAT_MAP,
  LAYER_INDIVIDUAL_LOCATION,
  LAYER_INFECTIOUS_DISEASE,
  LAYER_POI,
  LAYER_REALTIME,
  LAYER_RESCUE,
  LAYER_ZONE,
} from '@/utils/constants/common';
import {
  CONTROL_MODE_TENSE_HISTORY,
  CONTROL_MODE_TENSE_REALTIME,
  ControlModeTense,
} from '@/modules/setup/types';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import RangeSlider from '@/components/RangeSlider';

function TopRightToolbar() {
  return (
    <div className="btn-top-holder">
      <FloorsTransparencyButton />
      <LayerToolbar />
      <SpaceToolbar />
    </div>
  );
}

function SpaceToolbar() {
  const { project } = useControlProject();
  const { space } = useControlSpace();
  const [isOpen, setOpen] = useState(true);

  useEffect(() => {
    setOpen(true);
  }, [space.spaceMappingId]);

  return (
    <div className="map-location ml-2">
      <div className="location-header d-flex align-items-center">
        <strong>{project.name}</strong>
        <a className="btn btn-accent ml-auto" onClick={() => setOpen(!isOpen)}>
          <MaterialIcon
            name={'keyboard_arrow_down'}
            className={'accordion__toggle-icon'}
            style={{
              lineHeight: '34px',
              fontSize: '1rem',
            }}
          />
        </a>
      </div>
      <Collapse in={isOpen}>
        <div className="location-body">
          <div className="sidebar">
            <ul className="sidebar-menu">
              {project.spaceList.map((space) => (
                <SpaceItem key={space.id} {...space} />
              ))}
            </ul>
          </div>
        </div>
      </Collapse>
    </div>
  );
}

type SpaceItemProps = Space;

function SpaceItem({ id, mappingId, name, floorsList }: SpaceItemProps) {
  const { space, handleSetSpace } = useControlSpace();
  const [isOpen, setOpen] = useState(space.spaceMappingId === mappingId);
  const [collapse, setCollapse] = useState(false);

  const handleChangeSpace = () => {
    handleSetSpace({
      spaceMetaId: id,
      spaceMappingId: mappingId,
      floorsMapId: '',
    });
  };

  const handleChangeFloors = (floorsMapId: string) => {
    handleSetSpace({
      ...space,
      floorsMapId,
    });
  };

  useEffect(() => {
    let flag: boolean;
    if (space.spaceMappingId === mappingId) {
      flag = true;
    } else {
      flag = false;
    }

    setOpen(flag);
    setCollapse(flag);
  }, [space.spaceMappingId]);

  return (
    <li
      className={classNames('sidebar-menu-item', {
        open: isOpen,
      })}
    >
      <a className="sidebar-menu-button" onClick={handleChangeSpace}>
        {name}
        <span className="ml-auto sidebar-menu-toggle-icon"></span>
      </a>
      <Collapse in={collapse}>
        <ul className="sidebar-submenu sm-indent">
          {floorsList.map((floors) => (
            <li
              key={floors.id}
              className={classNames('sidebar-menu-item', {
                active: floors.id === space.floorsMapId,
              })}
              onClick={() => {
                handleChangeFloors(floors.id);
              }}
            >
              <a className="sidebar-menu-button">
                <span className="sidebar-menu-text">
                  {`${floors.name}_${CommonUtils.convertFloorsFormat(
                    floors.value
                  )}`}
                </span>
              </a>
            </li>
          ))}
        </ul>
      </Collapse>
    </li>
  );
}

type Layers = {
  map: Layer[];
  accident: Layer[];
};

type LayerCategory = 'map' | 'accident';

type Layer = {
  idx: number;
  icon: string;
  nameKey: string;
  active: boolean;
  tense: ControlModeTense[];
};

function LayerToolbar() {
  const { t } = useTranslation();
  const { tense } = useControlMode();
  const {
    map,
    realtimeLayer,
    eventLayer,
    accidentEmergencyLayer,
    accidentFireLayer,
    accidentRescueLayer,
    accidentInfectiousDiseaseLayer,
    heatMapLayer,
    distributionLayer,
    apLayer,
    poiLayer,
    zoneLayer,
    realtimeHeatMapLayer,
  } = useOpenLayers();
  const { handleAddVisibleLayer, handleRemoveVisibleLayer } = useSetupLayer();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  const [layers, setLayers] = useState<Layers>({
    map: [
      {
        idx: LAYER_INDIVIDUAL_LOCATION,
        icon: 'person_pin_circle',
        nameKey: 'text_individual_location',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME],
      },
      {
        idx: LAYER_HEAT_MAP,
        icon: 'blur_on',
        nameKey: 'text_heat_map',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME, CONTROL_MODE_TENSE_HISTORY],
      },
      {
        idx: LAYER_DISTRIBUTION_MAP,
        icon: 'scatter_plot',
        nameKey: 'text_distribution_map',
        active: false,
        tense: [CONTROL_MODE_TENSE_HISTORY],
      },
      {
        idx: LAYER_AP,
        icon: 'sensors',
        nameKey: 'text_ap',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME, CONTROL_MODE_TENSE_HISTORY],
      },
      {
        idx: LAYER_POI,
        icon: 'place',
        nameKey: 'text_poi',
        active: true,
        tense: [CONTROL_MODE_TENSE_REALTIME, CONTROL_MODE_TENSE_HISTORY],
      },
      {
        idx: LAYER_REALTIME,
        icon: 'edit_location_alt',
        nameKey: 'text_user',
        active: true,
        tense: [CONTROL_MODE_TENSE_REALTIME],
      },
      {
        idx: LAYER_ZONE,
        icon: 'space_dashboard',
        nameKey: 'text_area',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME, CONTROL_MODE_TENSE_HISTORY],
      },
    ],
    accident: [
      {
        idx: LAYER_EMERGENCY,
        icon: 'warning',
        nameKey: 'text_emergency',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME],
      },
      {
        idx: LAYER_FIRE,
        icon: 'local_fire_department',
        nameKey: 'text_fire',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME],
      },
      {
        idx: LAYER_RESCUE,
        icon: 'local_hospital',
        nameKey: 'text_rescue',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME],
      },
      {
        idx: LAYER_INFECTIOUS_DISEASE,
        icon: 'coronavirus',
        nameKey: 'text_infectious_disease',
        active: false,
        tense: [CONTROL_MODE_TENSE_REALTIME],
      },
    ],
  });

  useEffect(() => {
    if (map) {
      const layer = layers.map.find((layer) => layer.idx === LAYER_POI);
      const { text } = poiLayer;
      map.on('moveend', () => {
        const view = map.getView();
        if (view && layer?.active) {
          const zoom = view.getZoom() || 0;
          if (zoom < 11) {
            text?.setVisible(false);
          } else {
            text?.setVisible(true);
          }
        }
      });
    }
  }, [map]);

  useEffect(() => {
    let active: boolean;
    if (tense === CONTROL_MODE_TENSE_REALTIME) {
      heatMapLayer?.setVisible(false);
      distributionLayer?.setVisible(false);
      layers.map.forEach((layer) => {
        active = layer.active;
        switch (layer.idx) {
          case LAYER_INDIVIDUAL_LOCATION:
            eventLayer?.setVisible(active);
            break;
          case LAYER_HEAT_MAP:
            realtimeHeatMapLayer?.setVisible(active);
            break;
        }
      });

      layers.accident.forEach((layer) => {
        active = layer.active;
        switch (layer.idx) {
          case LAYER_EMERGENCY:
            accidentEmergencyLayer?.setVisible(active);
            break;
          case LAYER_FIRE:
            accidentFireLayer?.setVisible(active);
            break;
          case LAYER_RESCUE:
            accidentRescueLayer?.setVisible(active);
            break;
          case LAYER_INFECTIOUS_DISEASE:
            accidentInfectiousDiseaseLayer?.setVisible(active);
            break;
        }
      });
    } else if (tense === CONTROL_MODE_TENSE_HISTORY) {
      eventLayer?.setVisible(false);
      accidentEmergencyLayer?.setVisible(false);
      accidentFireLayer?.setVisible(false);
      accidentRescueLayer?.setVisible(false);
      accidentInfectiousDiseaseLayer?.setVisible(false);

      layers.map.forEach((layer) => {
        active = layer.active;
        switch (layer.idx) {
          case LAYER_HEAT_MAP:
            heatMapLayer?.setVisible(active);
            break;
          case LAYER_DISTRIBUTION_MAP:
            distributionLayer?.setVisible(active);
            break;
        }
      });
    }
  }, [tense]);

  useEffect(() => {
    for (const layersKey in layers) {
      const kind = layersKey as LayerCategory;
      layers[kind].forEach((layer) => {
        const layerIdx = layer.idx;
        if (layerIdx === LAYER_HEAT_MAP)
          if (layer.active) {
            handleAddVisibleLayer(layerIdx);
          } else {
            handleRemoveVisibleLayer(layerIdx);
          }
      });
    }
  }, [layers]);

  const handleLayer = (idx: number) => {
    for (const layersKey in layers) {
      const kind = layersKey as LayerCategory;
      setLayers({
        ...layers,
        [kind]: layers[kind].map((layer) => {
          if (layer.idx === idx) {
            layer.active = !layer.active;
          }

          if (idx === LAYER_HEAT_MAP && layer.idx === LAYER_DISTRIBUTION_MAP) {
            layer.active = false;
          } else if (
            idx === LAYER_DISTRIBUTION_MAP &&
            layer.idx === LAYER_HEAT_MAP
          ) {
            layer.active = false;
          }

          return layer;
        }),
      });
    }

    // 레이어 아이디에 맞게 분기
    let layer: Layer | undefined;
    switch (idx) {
      case LAYER_INDIVIDUAL_LOCATION:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          eventLayer?.setVisible(layer.active);
        }
        break;
      case LAYER_AP:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          apLayer?.setVisible(layer.active);
        }
        break;
      case LAYER_POI:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          poiLayer.icon?.setVisible(layer.active);
          poiLayer.text?.setVisible(layer.active);
        }
        break;
      case LAYER_ZONE:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          zoneLayer?.setVisible(layer.active);
        }
        break;
      case LAYER_REALTIME:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          realtimeLayer?.setVisible(layer.active);
        }
        break;
      case LAYER_EMERGENCY:
      case LAYER_FIRE:
      case LAYER_RESCUE:
      case LAYER_INFECTIOUS_DISEASE:
        layer = layers.accident.find((layer) => layer.idx === idx);
        if (layer) {
          if (idx === LAYER_EMERGENCY) {
            accidentEmergencyLayer?.setVisible(layer.active);
          } else if (idx === LAYER_FIRE) {
            accidentFireLayer?.setVisible(layer.active);
          } else if (idx === LAYER_RESCUE) {
            accidentRescueLayer?.setVisible(layer.active);
          } else if (idx === LAYER_INFECTIOUS_DISEASE) {
            accidentInfectiousDiseaseLayer?.setVisible(layer.active);
          }
        }
        break;
      case LAYER_HEAT_MAP:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          if (tense === CONTROL_MODE_TENSE_HISTORY) {
            heatMapLayer?.setVisible(layer.active);
            distributionLayer?.setVisible(false);
          } else {
            realtimeHeatMapLayer?.setVisible(layer.active);
          }
        }
        break;
      case LAYER_DISTRIBUTION_MAP:
        layer = layers.map.find((layer) => layer.idx === idx);
        if (layer) {
          heatMapLayer?.setVisible(false);
          distributionLayer?.setVisible(layer.active);
        }
        break;
    }
  };

  return (
    <div className="btn-group ml-2">
      <div className="btn-group">
        <Dropdown onToggle={handleToggle}>
          <Dropdown.Toggle className="btn btn-white" as={'button'}>
            <MaterialIcon name={'layers'} />
          </Dropdown.Toggle>
          <Dropdown.Menu align={'right'} ref={dropdown}>
            <>
              {layers.map.filter((layer) => layer.tense.includes(tense))
                .length > 0 && (
                <Dropdown.Header>
                  <strong>{t('text_map_settings')}</strong>
                </Dropdown.Header>
              )}
              <ul className="item-list mb-2">
                {layers.map.map((layer) => {
                  if (layer.tense.includes(tense)) {
                    return (
                      <LayerButton
                        key={layer.idx}
                        {...layer}
                        onClick={handleLayer}
                      />
                    );
                  }
                })}
              </ul>
              {layers.accident.filter((layer) => layer.tense.includes(tense))
                .length > 0 && (
                <Dropdown.Header>
                  <strong>{t('text_alert_set_up')}</strong>
                </Dropdown.Header>
              )}
              <ul className="item-list">
                {layers.accident.map((layer) => {
                  if (layer.tense.includes(tense)) {
                    return (
                      <LayerButton
                        key={layer.idx}
                        {...layer}
                        onClick={handleLayer}
                      />
                    );
                  }
                })}
              </ul>
            </>
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </div>
  );
}

type LayerButtonProps = Layer & {
  onClick?: (idx: number) => void;
};

function LayerButton(props: LayerButtonProps) {
  const { t } = useTranslation();
  return (
    <li onClick={() => props.onClick?.call(null, props.idx)}>
      <a
        className={classNames({
          active: props.active,
        })}
      >
        <MaterialIcon name={props.icon} />
        <span>{t(props.nameKey)}</span>
      </a>
    </li>
  );
}

function FloorsTransparencyButton() {
  const { t } = useTranslation();
  const { floorsLayer } = useOpenLayers();
  const [transparency, setTransparency] = useState(100);
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  useEffect(() => {
    const opacity = transparency / 100;
    floorsLayer?.setOpacity(opacity);
  }, [transparency]);

  return (
    <div className="btn-group ml-2">
      <Dropdown onToggle={handleToggle}>
        <DropdownToggle
          as={'button'}
          className="btn btn-white dropdown-toggle map-e"
        >
          {t('text_indoor_map_transparency')}
        </DropdownToggle>
        <Dropdown.Menu align={'right'} ref={dropdown}>
          <Dropdown.Header>
            <strong>{t('text_transparency_settings')}</strong>
          </Dropdown.Header>
          <div className="p-3">
            <RangeSlider
              type={'single'}
              min={0}
              max={100}
              from={transparency}
              grid={false}
              onChange={(data) => {
                setTransparency(data.from);
              }}
            />
          </div>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

export default TopRightToolbar;
