import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import MaterialIcon from '@/components/MaterialIcon';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import { Dropdown } from 'react-bootstrap';
import { useDropdown } from '@/modules/common/hook';
import {
  FUNCTION_LOCATION_EMERGENCY,
  FUNCTION_LOCATION_FIRE,
  FUNCTION_LOCATION_INFECTIOUS_DISEASE,
  FUNCTION_LOCATION_RESCUE,
  FUNCTION_SCOPE_AREA,
  FUNCTION_SCOPE_CIRCLE,
  FUNCTION_SCOPE_POLYGON,
  GEOMETRY_TARGET_TYPE_INNER,
  GEOMETRY_TARGET_TYPE_OUTER,
  GEOMETRY_TYPE_CIRCLE,
  GEOMETRY_TYPE_POLYGON,
  PANE_STATUS_DETAIL,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import FormLabel from '@/components/FormLabel';
import classNames from 'classnames';
import { CommonUtils } from '@/utils';
import {
  useAdditionalLocation,
  useControlAccident,
  useControlSpace,
  useControlProject,
  useOpenLayers,
  useRealtimeUser,
} from '@/modules/map/hook';
import FormGroup from '@/components/FormGroup';
import FormRow from '@/components/FormRow';
import { postAccident, RequestPostAccident } from '@/api/accident';
import { Icon, Style } from 'ol/style';
import { Circle, Polygon } from 'ol/geom';
import {
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
  AccidentCategory,
} from '@/modules/accident/types';
import GeometryType from 'ol/geom/GeometryType';
import * as turf from '@turf/turf';
import { postMessages } from '@/api/message';
import { useAccidentMessage } from '@/modules/accident/hook';
import { useSetupEventsFlag } from '@/modules/setup/hook';

type AccidentRegisterProps = {
  onStatusChange: (status: PaneStatus) => void;
  onChangeEventId: (eventId: string) => void;
};

function AccidentRegister({
  onStatusChange,
  onChangeEventId,
}: AccidentRegisterProps) {
  const { project } = useControlProject();
  const { users } = useRealtimeUser();
  const { handleReloadActivationAccidents } = useControlAccident();
  const { space } = useControlSpace();
  const {
    locationFeature,
    locationData,
    handleResetLocation,
  } = useAdditionalLocation();
  const { map, draw } = useOpenLayers();
  const { handleChangeClick } = useSetupEventsFlag();
  const [isRegisterPossible, setRegisterPossible] = useState(false);
  const [category, setCategory] = useState<AccidentCategory>(
    ACCIDENT_EMERGENCY
  );
  const { handleGetAccidentMessage } = useAccidentMessage();
  const { t } = useTranslation();

  useEffect(() => {
    if (!CommonUtils.isEmptyObject(locationData)) {
      setRegisterPossible(true);
    } else {
      setRegisterPossible(false);
    }
  }, [locationData]);

  useEffect(() => {
    handleChangeClick(false);
    return () => {
      handleChangeClick(true);
      draw.source?.clear();
      handleResetLocation();
    };
  }, []);

  useEffect(() => {
    return () => {
      if (draw.interaction) {
        map?.removeInteraction(draw.interaction);
      }
    };
  }, [draw.interaction]);

  useEffect(() => {
    const featureById = draw.source?.getFeatureById('drawAccidentIconFeature');
    if (featureById) {
      const style = featureById.getStyle() as Style;
      style.setImage(
        new Icon({
          src: `/static/images/${CommonUtils.getAccidentIconName(category)}`,
          scale: 2,
        })
      );
      featureById.setStyle(style);
    }
  }, [category]);

  const handleRegisterDone = async () => {
    let eventTitle = '';
    let eventContent = '';
    switch (category) {
      case ACCIDENT_FIRE:
        eventTitle = t('text_fire');
        eventContent = t('text_fire');
        break;
      case ACCIDENT_EMERGENCY:
        eventTitle = t('text_emergency');
        eventContent = t('text_emergency');
        break;
      case ACCIDENT_RESCUE:
        eventTitle = t('text_rescue');
        eventContent = t('text_rescue');
        break;
      case ACCIDENT_INFECTIOUS_DISEASE:
        eventTitle = t('text_infectious_disease');
        eventContent = t('text_infectious_disease');
        break;
    }

    const requestPostAccident: RequestPostAccident = {
      activeFlag: true,
      projectId: project.id,
      eventTitle,
      eventContent,
      targetMappingId: space.spaceMappingId,
      targetMapId: space.floorsMapId,
      eventDetailCategory: category,
    };

    if (locationData.geom) {
      requestPostAccident.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPostAccident.geomType = GEOMETRY_TYPE_POLYGON;
      requestPostAccident.geom = locationData.geom;
    }

    if (locationData.lng && locationData.lat && locationData.radius) {
      requestPostAccident.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPostAccident.geomType = GEOMETRY_TYPE_CIRCLE;
      requestPostAccident.lng = locationData.lng;
      requestPostAccident.lat = locationData.lat;
      requestPostAccident.radius = locationData.radius;
    }

    if (locationData.areaId) {
      requestPostAccident.targetGeomType = GEOMETRY_TARGET_TYPE_OUTER;
      requestPostAccident.outerKey = locationData.areaId;
    }

    const eventId = await postAccident(requestPostAccident);
    if (eventId) {
      const geometry = locationFeature?.getGeometry();
      if (geometry) {
        const targetIds: string[] = [];
        switch (geometry.getType()) {
          case GeometryType.POLYGON:
            const polygonGeometry = geometry as Polygon;
            const polygon = turf.polygon(polygonGeometry.getCoordinates());

            users.forEach((user) => {
              const userPoint = turf.point([user.lng, user.lat]);
              if (turf.inside(userPoint, polygon)) {
                targetIds.push(user.accessKey);
              }
            });

            break;
          case GeometryType.CIRCLE:
            const circleGeometry = geometry as Circle;
            const center = circleGeometry.getCenter();
            const radius = circleGeometry.getRadius();

            const accidentPoint = turf.toWgs84(turf.point(center));

            users.forEach((user) => {
              const userPoint = turf.toWgs84(turf.point([user.lng, user.lat]));

              const buffer = turf.buffer(accidentPoint, radius, {
                units: 'meters',
              });
              const contain = turf.booleanContains(buffer, userPoint);

              if (contain) {
                targetIds.push(user.accessKey);
              }
            });
            break;
        }

        if (targetIds.length > 0) {
          const { title, content } = handleGetAccidentMessage(category);

          await postMessages({
            projectId: project.id,
            parentId: eventId,
            parentType: 'message',
            messageTitle: title,
            messageContent: content,
            imgId: '',
            targetIds,
          });
        }
      }

      handleReloadActivationAccidents();
      onChangeEventId(eventId);
      onStatusChange(PANE_STATUS_DETAIL);
    }
  };

  let content: ReactElement;
  if (!CommonUtils.isEmptyObject(locationData)) {
    content = <Body category={category} onCategoryChange={setCategory} />;
  } else {
    content = <Guide />;
  }

  return (
    <>
      <Header onChangeCategory={setCategory} />
      <div className="container-fluid">
        {content}
        <Footer
          registerPossible={isRegisterPossible}
          onConfirmBtnClick={handleRegisterDone}
          onCancelBtnClick={onStatusChange}
        />
      </div>
    </>
  );
}

type HeaderProps = {
  onChangeCategory: (category: AccidentCategory) => void;
};

function Header({ onChangeCategory }: HeaderProps) {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const { handleResetLocation } = useAdditionalLocation();
  const {
    handleSelectArea,
    handleDrawCircle,
    handleDrawPolygon,
    handleDrawAccident,
  } = useOpenLayers();

  const handleFunctionSelect = (eventKey: string | null) => {
    handleResetLocation();
    switch (eventKey) {
      case FUNCTION_SCOPE_AREA:
        handleSelectArea();
        onChangeCategory(ACCIDENT_EMERGENCY);
        break;
      case FUNCTION_SCOPE_CIRCLE:
        handleDrawCircle();
        onChangeCategory(ACCIDENT_EMERGENCY);
        break;
      case FUNCTION_SCOPE_POLYGON:
        handleDrawPolygon();
        onChangeCategory(ACCIDENT_EMERGENCY);
        break;
      case FUNCTION_LOCATION_EMERGENCY:
        handleDrawAccident(ACCIDENT_EMERGENCY);
        onChangeCategory(ACCIDENT_EMERGENCY);
        break;
      case FUNCTION_LOCATION_FIRE:
        handleDrawAccident(ACCIDENT_FIRE);
        onChangeCategory(ACCIDENT_FIRE);
        break;
      case FUNCTION_LOCATION_RESCUE:
        handleDrawAccident(ACCIDENT_RESCUE);
        onChangeCategory(ACCIDENT_RESCUE);
        break;
      case FUNCTION_LOCATION_INFECTIOUS_DISEASE:
        handleDrawAccident(ACCIDENT_INFECTIOUS_DISEASE);
        onChangeCategory(ACCIDENT_INFECTIOUS_DISEASE);
        break;
    }
  };

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{t('text_accident_registration')}</h3>
        </div>
      </div>
      <div className="btn-group">
        <Dropdown onToggle={handleToggle} onSelect={handleFunctionSelect}>
          <DropdownToggle as={'a'} className="btn btn-outline-accent">
            <MaterialIcon name={'timeline'} align={'left'} />
            {t('text_selection_function')}
          </DropdownToggle>
          <DropdownMenu align={'right'} ref={dropdown}>
            <Dropdown.Header>
              <strong>{t('text_specify_range')}</strong>
            </Dropdown.Header>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_AREA}>
              <MaterialIcon name={'highlight_alt'} />{' '}
              {t('text_selection_structure')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_CIRCLE}>
              <MaterialIcon name={'adjust'} /> {t('text_selection_range')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_POLYGON}>
              <MaterialIcon name={'timeline'} /> {t('text_draw_line')}
            </Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Header>
              <strong>{t('text_specify_position')}</strong>
            </Dropdown.Header>
            <Dropdown.Item eventKey={FUNCTION_LOCATION_EMERGENCY}>
              <MaterialIcon name={'warning'} /> {t('text_emergency')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_LOCATION_FIRE}>
              <MaterialIcon name={'local_fire_department'} /> {t('text_fire')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_LOCATION_RESCUE}>
              <MaterialIcon name={'local_hospital'} /> {t('text_rescue')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_LOCATION_INFECTIOUS_DISEASE}>
              <MaterialIcon name={'coronavirus'} />{' '}
              {t('text_infectious_disease')}
            </Dropdown.Item>
          </DropdownMenu>
        </Dropdown>
      </div>
    </div>
  );
}

function Guide() {
  const { t } = useTranslation();
  return <em className="none-list">{t('msg_selection_function')}</em>;
}

type BodyProps = {
  category: AccidentCategory;
  onCategoryChange: (category: AccidentCategory) => void;
};

function Body({ category, onCategoryChange }: BodyProps) {
  const { t } = useTranslation();
  const { locationData, handleLocationChange } = useAdditionalLocation();

  return (
    <>
      <FormGroup>
        <FormLabel textKey={'text_kind_of_accident'} essential />
        <select
          name="category"
          className="form-line"
          value={category}
          onChange={(e) => onCategoryChange(e.target.value as AccidentCategory)}
        >
          <option value={ACCIDENT_EMERGENCY}>{t('text_emergency')}</option>
          <option value={ACCIDENT_FIRE}>{t('text_fire')}</option>
          <option value={ACCIDENT_RESCUE}>{t('text_rescue')}</option>
          <option value={ACCIDENT_INFECTIOUS_DISEASE}>
            {t('text_infectious_disease')}
          </option>
        </select>
      </FormGroup>
      {locationData.areaName && (
        <FormGroup>
          <FormLabel textKey={'text_location_of_accident'} essential />
          <input
            type="text"
            className="form-line"
            placeholder=""
            value={locationData.areaName}
            readOnly={true}
          />
        </FormGroup>
      )}
      {locationData.lat && locationData.lng && (
        <></>
        /*
        <FormRow>
          <div className="col-md-6">
            <FormLabel textKey={'text_latitude'} htmlFor={'latitude'} />
            <input
              type="number"
              className="form-line"
              id="latitude"
              readOnly={true}
              value={locationData.latitude}
              onChange={(e) => {
                handleLocationChange(e.target.id, e.target.value);
              }}
            />
          </div>
          <div className="col-md-6">
            <FormLabel textKey={'text_longitude'} htmlFor={'longitude'} />
            <input
              type="number"
              className="form-line"
              id="longitude"
              readOnly={true}
              value={locationData.longitude}
              onChange={(e) => {
                handleLocationChange(e.target.id, e.target.value);
              }}
            />
          </div>
        </FormRow>
        
         */
      )}
      {locationData.hasOwnProperty('radius') && (
        <FormRow>
          <div className="col-md-6 over-txt">
            <FormLabel textKey={'text_radius'} htmlFor={'radius'} />
            <input
              type="number"
              className="form-line pr-5"
              id="radius"
              value={locationData.radius}
              onChange={(e) => {
                handleLocationChange(e.target.id, e.target.value);
              }}
            />
            <em className="text-50">meters</em>
          </div>
        </FormRow>
      )}
    </>
  );
}

type FooterProps = {
  onCancelBtnClick?: (status: PaneStatus) => void;
  onConfirmBtnClick?: () => void;
  registerPossible?: boolean;
};

function Footer({
  onConfirmBtnClick,
  onCancelBtnClick,
  registerPossible = false,
}: FooterProps) {
  const { t } = useTranslation();

  return (
    <div
      className={classNames({
        'mt-64pt': registerPossible,
        'my-32pt': !registerPossible,
      })}
    >
      <div className="d-flex align-items-center justify-content-center">
        <a
          onClick={() => {
            onCancelBtnClick?.call(null, PANE_STATUS_LIST);
          }}
          className="btn btn-outline-secondary mr-8pt"
        >
          {t('text_to_cancel')}
        </a>
        {registerPossible && (
          <a
            className="btn btn-outline-accent ml-0"
            onClick={onConfirmBtnClick}
          >
            {t('text_registration')}
          </a>
        )}
      </div>
    </div>
  );
}

export default AccidentRegister;
