import React, { useEffect, useState } from 'react';
import BottomInfo from '@/components/BottomInfo';
import MultipleUserSelectionInfo from '@/components/MultipleUserSelectionInfo';
import { useControlMode } from '@/modules/setup/hook';
import { useControlUser } from '@/modules/map/hook';
import {
  CONTROL_MODE_TENSE_HISTORY,
  CONTROL_MODE_TENSE_REALTIME,
  ControlModeTense,
} from '@/modules/setup/types';
import KeypressUserSelectionInfo from '@/components/KeypressUserSelectionInfo';

type InfoKind = '' | 'multiple' | 'selection' | 'cancel';

let fireKeydown = false;
let checkModeTense: ControlModeTense = CONTROL_MODE_TENSE_REALTIME;
function MapBottomInfo() {
  const { tense } = useControlMode();
  const { users, handleSetControlUser } = useControlUser();
  const [displayInfo, setDisplayInfo] = useState<InfoKind>('');

  const handleCheckKeydown = (e: KeyboardEvent) => {
    e.stopPropagation();

    if (checkModeTense === CONTROL_MODE_TENSE_REALTIME) {
      const key = e.key;
      let selectionKey = 'Control';
      if (navigator.appVersion.indexOf('Mac') > -1) {
        selectionKey = 'Meta';
      }

      if (key === selectionKey || key === 'Shift') {
        if (!fireKeydown) {
          fireKeydown = true;
          if (key === selectionKey) {
            setDisplayInfo('selection');
          } else if (key === 'Shift') {
            setDisplayInfo('cancel');
          }
        } else {
          setDisplayInfo('');
        }
      }
    }
  };

  const handleCheckKeyup = (e: KeyboardEvent) => {
    e.stopPropagation();
    fireKeydown = false;
    setDisplayInfo('');
    handleSetControlUser([]);
  };

  useEffect(() => {
    window.addEventListener('keydown', handleCheckKeydown);
    window.addEventListener('keyup', handleCheckKeyup);

    return () => {
      window.removeEventListener('keydown', handleCheckKeydown);
      window.removeEventListener('keyup', handleCheckKeyup);
    };
  }, []);

  const handleMultipleInfo = () => {
    if (!fireKeydown) {
      setDisplayInfo('');
      if (tense === CONTROL_MODE_TENSE_REALTIME && users.length > 0) {
        setDisplayInfo('multiple');
      }
    }
  };

  useEffect(() => {
    checkModeTense = tense;
    if (tense === CONTROL_MODE_TENSE_HISTORY) {
      setDisplayInfo('');
    }
    handleMultipleInfo();
  }, [tense, users]);

  return (
    <BottomInfo>
      {displayInfo === 'multiple' && <MultipleUserSelectionInfo />}
      {displayInfo === 'selection' && (
        <KeypressUserSelectionInfo eventKey={'selection'} />
      )}
      {displayInfo === 'cancel' && (
        <KeypressUserSelectionInfo eventKey={'cancel'} />
      )}
    </BottomInfo>
  );
}

export default MapBottomInfo;
