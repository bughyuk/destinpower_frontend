import React, { useState } from 'react';
import HomeHeader from '@/components/HomeHeader';
import SpaceListTable from '@/components/SpaceListTable';
import { useHistory } from 'react-router-dom';
import { useSpacePane } from '@/modules/space/hook';
import { PANE_STATUS_REGISTER } from '@/utils/constants/common';

function HomeSpace() {
  const history = useHistory();
  const { handleChangePaneStatus } = useSpacePane();
  const [count, setCount] = useState(0);
  const [searchKeyword, setSearchKeyword] = useState('');

  const handleSubmitSearch = (searchKeyword: string) => {
    setSearchKeyword(searchKeyword);
  };

  const handleClickRegisterButton = () => {
    handleChangePaneStatus(PANE_STATUS_REGISTER);
    history.push('/space');
  };

  return (
    <div className="contents-section">
      <div className="cell">
        <HomeHeader
          titleKey={'text_space_list'}
          registerButtonTextKey={'text_space_new_registration'}
          count={count}
          descriptionTextKey={'msg_there_are_number_space_registered'}
          onSubmitSearch={handleSubmitSearch}
          onClickRegisterButton={handleClickRegisterButton}
        />
        <SpaceListTable
          searchKeyword={searchKeyword}
          onChangeCount={setCount}
          showPagination={true}
          showCheckbox={true}
          showSelectionDelete={true}
        />
      </div>
    </div>
  );
}

export default HomeSpace;
