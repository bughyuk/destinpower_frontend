import React, { useEffect, useState } from 'react';
import { useLeftPaneContainer } from '@/modules/setup/hook';
import {
  PANE_STATUS_ACCIDENT_DIRECT_REGISTER,
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import AccidentList from '@/components/AccidentList';
import AccidentRegister from '@/components/AccidentRegister';
import AccidentDetail from '@/components/AccidentDetail';
import AccidentEdit from '@/components/AccidentEdit';
import { useControlAccident, useControlSpace } from '@/modules/map/hook';
import AccidentDirectRegister from '@/components/AccidentDirectRegister';

function AccidentPane() {
  return (
    <div className="tab-pane sm-accident active">
      <div className="accident-inner">
        <AccidentContent />
      </div>
    </div>
  );
}

function AccidentContent() {
  const { space } = useControlSpace();
  const {
    directFlag,
    directAccidentId,
    handleSetDirectAccidentId,
  } = useControlAccident();
  const [status, setStatus] = useState<PaneStatus>(
    PANE_STATUS_ACCIDENT_DIRECT_REGISTER
  );
  const { updateScroll } = useLeftPaneContainer();
  const [eventId, setEventId] = useState('');

  useEffect(() => {
    updateScroll();
  }, [status]);

  useEffect(() => {
    setStatus(PANE_STATUS_ACCIDENT_DIRECT_REGISTER);
  }, [space]);

  useEffect(() => {
    if (directAccidentId) {
      setEventId(directAccidentId);
      setStatus(PANE_STATUS_DETAIL);
      handleSetDirectAccidentId('');
    }
  }, [directFlag, directAccidentId]);

  return (
    <>
      {status === PANE_STATUS_ACCIDENT_DIRECT_REGISTER && (
        <AccidentDirectRegister
          onStatusChange={setStatus}
          onChangeEventId={setEventId}
        />
      )}
      {status === PANE_STATUS_LIST && (
        <AccidentList onStatusChange={setStatus} onChangeEventId={setEventId} />
      )}
      {status === PANE_STATUS_REGISTER && (
        <AccidentRegister
          onStatusChange={setStatus}
          onChangeEventId={setEventId}
        />
      )}
      {status === PANE_STATUS_EDIT && (
        <AccidentEdit onStatusChange={setStatus} eventId={eventId} />
      )}
      {status === PANE_STATUS_DETAIL && (
        <AccidentDetail onStatusChange={setStatus} eventId={eventId} />
      )}
    </>
  );
}

export default AccidentPane;
