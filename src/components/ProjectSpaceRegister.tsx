import React, { useEffect, useRef, useState } from 'react';
import ProjectRegisterHeader from '@/components/ProjectRegisterHeader';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MaterialIcon from '@/components/MaterialIcon';
import { Collapse, Dropdown, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useProjectPane, useProjectRegister } from '@/modules/project/hook';
import { fetchSpaceListInfo, ProcessSpaceInfo } from '@/api/space';
import { useDropdown } from '@/modules/common';
import { FUNCTION_DISCONNECT_SPACE } from '@/utils/constants/common';
import {
  deleteProjectDisconnectSpace,
  fetchProject,
  postProjectConnectSpace,
  putProjectStepData,
} from '@/api/project';

function ProjectSpaceRegister() {
  const {
    projectInfo,
    projectProduceStep,
    handleChangeRegisterStep,
    handleSetConnectedMetaIds,
  } = useProjectRegister();
  const { handleChangeDepthSidebarShow } = useProjectPane();

  const handleSubmit = async () => {
    const projectId = projectInfo.projectId;
    if (projectId) {
      const projectDetail = await fetchProject(projectId);

      if (projectDetail) {
        const resultSpaceIdList = projectInfo.metaIds || [];
        const spaceList = projectDetail.buildings;
        const curSpaceIdList = spaceList.map((space) => space.metaId);

        const disconnectSpace = spaceList.filter(
          (space) => !resultSpaceIdList.includes(space.metaId)
        );

        const promise: Promise<any>[] = [];
        const newConnectionSpace = resultSpaceIdList.filter(
          (metaId) => !curSpaceIdList.includes(metaId)
        );

        disconnectSpace.forEach((space) => {
          promise.push(
            deleteProjectDisconnectSpace(projectId, space.mappingId)
          );
        });

        newConnectionSpace.forEach((metaId) => {
          promise.push(postProjectConnectSpace(projectId, metaId));
        });

        promise.push(
          putProjectStepData(projectId, {
            produceStepData: JSON.stringify({
              ...projectProduceStep,
              step: 6,
            }),
          })
        );

        handleSetConnectedMetaIds(resultSpaceIdList);

        await Promise.all(promise);
      }
    }

    handleChangeRegisterStep(6);
  };

  useEffect(() => {
    return () => handleChangeDepthSidebarShow(false);
  }, []);

  return (
    <>
      <ProjectRegisterHeader />
      <Body />
      <Footer onSubmit={handleSubmit} />
    </>
  );
}

function Body() {
  const { t } = useTranslation();
  const { projectInfo, connectedMetaIds } = useProjectRegister();
  const [connectSpaceList, setConnectSpaceList] = useState<ProcessSpaceInfo[]>(
    []
  );

  useEffect(() => {
    if (connectedMetaIds.length) {
      handleFetchSpaceInfo(connectedMetaIds);
    }
  }, []);

  const handleFetchSpaceInfo = async (metaIds: string[]) => {
    setConnectSpaceList(await fetchSpaceListInfo(metaIds));
  };

  useEffect(() => {
    if (projectInfo.metaIds) {
      handleFetchSpaceInfo(projectInfo.metaIds!);
    }
  }, [projectInfo]);

  return (
    <div className="container-fluid">
      <div className="accordion js-accordion accordion--boxed mb-4">
        {connectSpaceList.length === 0 && (
          <em className="none-list mb-4">{t('msg_connect_space')}</em>
        )}
        {connectSpaceList.map((connectSpace) => (
          <SpaceItem key={connectSpace.spaceId} {...connectSpace} />
        ))}
      </div>
      <RegisterButton />
    </div>
  );
}

function SpaceItem({ spaceId, spaceName, floorsList }: ProcessSpaceInfo) {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const { projectInfo, handleSetProjectInfo } = useProjectRegister();
  const { metaIds = [] } = projectInfo;

  const handleOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_DISCONNECT_SPACE:
        const ids = [...metaIds];
        handleSetProjectInfo({
          ...projectInfo,
          metaIds: ids.filter((id) => id !== spaceId),
        });
        break;
    }
  };

  return (
    <div className="accordion__item">
      <a className="accordion__toggle" onClick={() => setOpen(!isOpen)}>
        <span className="flex">
          {spaceName}{' '}
          <small className="text-danger">({floorsList.length})</small>
        </span>
        {floorsList.length > 0 && (
          <MaterialIcon
            name={'keyboard_arrow_down'}
            className={'accordion__toggle-icon'}
          />
        )}
      </a>
      <Dropdown
        onSelect={handleOptions}
        onToggle={handleToggle}
        className="acc-del"
      >
        <Dropdown.Toggle as={'a'} data-caret="false" className="text-100">
          <MaterialIcon name={'more_vert'} />
        </Dropdown.Toggle>
        <Dropdown.Menu align={'right'} ref={dropdown}>
          <Dropdown.Item
            className="text-primary"
            eventKey={FUNCTION_DISCONNECT_SPACE}
          >
            {t('text_space_disconnect_to_project')}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      {floorsList.length > 0 && (
        <Collapse in={isOpen}>
          <div className="accordion__menu">
            {floorsList.map((floors) => (
              <FloorItem
                key={`${floors.floorsValue}/${floors.floorsName}`}
                {...floors}
              />
            ))}
          </div>
        </Collapse>
      )}
    </div>
  );
}

type FloorItemProps = {
  floorsName: string;
};

function FloorItem({ floorsName }: FloorItemProps) {
  return (
    <div className="accordion__menu-link">
      <span className="icon-holder icon-holder--small icon-holder--accent rounded-circle d-inline-flex icon--left">
        <FontAwesomeIcon icon={['far', 'map']} />
      </span>
      <p className="flex m-0">{floorsName}</p>
    </div>
  );
}

function RegisterButton() {
  const { t } = useTranslation();
  const { depthSidebarShow, handleChangeDepthSidebarShow } = useProjectPane();
  const {
    projectInfo,
    projectProduceStep,
    handleChangeRegisterStep,
  } = useProjectRegister();

  const handleChangeStepNewSpace = async () => {
    const projectId = projectInfo.projectId;
    if (projectId) {
      await putProjectStepData(projectId, {
        produceStepData: JSON.stringify({
          ...projectProduceStep,
          step: 3,
        }),
      });
    }
    handleChangeRegisterStep(3);
  };

  return (
    <div className="d-flex align-items-center justify-content-center">
      <OverlayTrigger
        placement={'top'}
        overlay={
          <Tooltip id={'doNotHaveSpace'}>{t('msg_do_not_have_space')}</Tooltip>
        }
      >
        <a className="btn-row-add" onClick={handleChangeStepNewSpace}>
          {' '}
          <MaterialIcon name={'add'} /> {t('text_space_new_registration')}
        </a>
      </OverlayTrigger>
      <OverlayTrigger
        placement={'top'}
        overlay={
          <Tooltip id={'connectExistingSpace'}>
            {t('msg_connect_existing_space')}
          </Tooltip>
        }
      >
        <a
          className="btn-row-add ml-2"
          onClick={() => handleChangeDepthSidebarShow(!depthSidebarShow)}
        >
          {' '}
          <MaterialIcon name={'insert_link'} />{' '}
          {t('text_space_connect_to_project')}
        </a>
      </OverlayTrigger>
    </div>
  );
}

type Footer = {
  onSubmit: () => void;
};

function Footer({ onSubmit }: Footer) {
  const { t } = useTranslation();
  const { handleChangeRegisterStep } = useProjectRegister();

  return (
    <div className="my-32pt">
      <div className="d-flex align-items-center justify-content-center">
        <a
          className="btn btn-outline-secondary mr-8pt"
          onClick={() => handleChangeRegisterStep(1)}
        >
          {t('text_to_the_prev')}
        </a>
        <a className="btn btn-outline-accent ml-0" onClick={onSubmit}>
          {t('text_save_and_to_the_next')}
        </a>
      </div>
    </div>
  );
}

export default ProjectSpaceRegister;
