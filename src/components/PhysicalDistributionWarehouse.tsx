import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import { Warehouse } from '@/modules/physical_distribution/types';
import PhysicalDistributionWarehouseList from '@/components/PhysicalDistributionWarehouseList';
import PhysicalDistributionWarehouseRegister from '@/components/PhysicalDistributionWarehouseRegister';
import PhysicalDistributionWarehouseEdit from '@/components/PhysicalDistributionWarehouseEdit';

function PhysicalDistributionWarehouse({
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  const { t } = useTranslation();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_LIST);
  const [warehouse, setWarehouse] = useState<Warehouse>({} as Warehouse);

  let content: ReactElement = <></>;
  if (status === PANE_STATUS_LIST) {
    content = (
      <PhysicalDistributionWarehouseList
        onChangeWarehouse={setWarehouse}
        onChangeStatus={setStatus}
      />
    );
  } else if (status === PANE_STATUS_REGISTER) {
    content = (
      <PhysicalDistributionWarehouseRegister onChangeStatus={setStatus} />
    );
  } else if (status === PANE_STATUS_EDIT) {
    content = (
      <PhysicalDistributionWarehouseEdit
        warehouse={warehouse}
        onChangeStatus={setStatus}
      />
    );
  }

  let displayTitle = title;
  if (status === PANE_STATUS_REGISTER) {
    displayTitle = t('text_add_warehouse');
  } else if (status === PANE_STATUS_EDIT) {
    displayTitle = t('text_edit_warehouse');
  }

  const handleClickBack = () => {
    if (status === PANE_STATUS_LIST) {
      onClickBack?.call(null);
    } else {
      setStatus(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={handleClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{displayTitle}</h3>
          </div>
          {status === PANE_STATUS_LIST && (
            <a
              className="btn btn-outline-dark ml-auto"
              onClick={() => setStatus(PANE_STATUS_REGISTER)}
            >
              {t('text_add_warehouse')}
            </a>
          )}
        </div>
      </div>
      {content}
    </>
  );
}

export default PhysicalDistributionWarehouse;
