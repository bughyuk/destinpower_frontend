import React, { ReactElement, useState } from 'react';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import PhysicalDistributionItemList from '@/components/PhysicalDistributionItemList';
import PhysicalDistributionItemRegister from '@/components/PhysicalDistributionItemRegister';
import PhysicalDistributionItemEdit from '@/components/PhysicalDistributionItemEdit';
import { Item } from '@/modules/physical_distribution/types';

function PhysicalDistributionItem({
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  const { t } = useTranslation();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_LIST);
  const [item, setItem] = useState<Item>({} as Item);

  const handleChangePaneStatusList = () => {
    setStatus(PANE_STATUS_LIST);
  };

  let content: ReactElement = <></>;
  if (status === PANE_STATUS_LIST) {
    content = (
      <PhysicalDistributionItemList
        onChangeItem={setItem}
        onChangeStatus={setStatus}
      />
    );
  } else if (status === PANE_STATUS_REGISTER) {
    content = (
      <PhysicalDistributionItemRegister
        onFinish={handleChangePaneStatusList}
        onClickCancel={handleChangePaneStatusList}
      />
    );
  } else if (status === PANE_STATUS_EDIT) {
    content = (
      <PhysicalDistributionItemEdit item={item} onChangeStatus={setStatus} />
    );
  }

  let displayTitle = title;
  if (status === PANE_STATUS_REGISTER) {
    displayTitle = t('text_add_item');
  } else if (status === PANE_STATUS_EDIT) {
    displayTitle = t('text_edit_item');
  }

  const handleClickBack = () => {
    if (status === PANE_STATUS_LIST) {
      onClickBack?.call(null);
    } else {
      setStatus(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={handleClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{displayTitle}</h3>
          </div>
          {status === PANE_STATUS_LIST && (
            <a
              className="btn btn-outline-dark ml-auto"
              onClick={() => setStatus(PANE_STATUS_REGISTER)}
            >
              {t('text_add_item')}
            </a>
          )}
        </div>
      </div>
      {content}
    </>
  );
}

export default PhysicalDistributionItem;
