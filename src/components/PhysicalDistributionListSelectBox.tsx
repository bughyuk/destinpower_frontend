import React from 'react';
import { useTranslation } from 'react-i18next';

type SelectBoxOptions = {
  value: string | number;
  text: string;
};

type PhysicalDistributionListSelectBoxProps = {
  all?: boolean;
  options: SelectBoxOptions[];
  emptyTextKey?: string;
  onChange?: (value: string) => void;
};

function PhysicalDistributionListSelectBox({
  all = false,
  options,
  emptyTextKey = 'text_data_not_exist',
  onChange,
}: PhysicalDistributionListSelectBoxProps) {
  const { t } = useTranslation();
  return (
    <div className="cell select-box">
      <select
        name="category"
        className="form-control custom-select"
        onChange={(e) => {
          onChange?.call(null, e.target.value);
        }}
      >
        {all && <option value={''}>{t('text_the_whole')}</option>}
        {options.length === 0 && <option value={''}>{t(emptyTextKey)}</option>}
        {options.map(({ value, text }, i) => (
          <option key={i} value={value}>
            {text}
          </option>
        ))}
      </select>
    </div>
  );
}

export default PhysicalDistributionListSelectBox;
