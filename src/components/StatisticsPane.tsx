import React, { useEffect } from 'react';
import { useControlProject } from '@/modules/map/hook';
import { SOLUTION_TYPE_SMART_FACTORY } from '@/modules/project/types';
import StatisticsSmartFactory from '@/components/StatisticsSmartFactory';
import { useFloatPane, useRightPane } from '@/modules/setup/hook';
import { FLOAT_PANE_STATISTICS } from '@/modules/setup/types';

function StatisticsPane() {
  return (
    <div className="tab-pane sm-distribution active">
      <div className="distribution-inner">
        <StatisticsContent />
      </div>
    </div>
  );
}

function StatisticsContent() {
  const { project } = useControlProject();
  const { handleChangeShow: handleChangeFloatPaneShow } = useFloatPane();
  const { handleChangeShow: handleChangeRightPaneShow } = useRightPane();

  useEffect(() => {
    handleChangeFloatPaneShow(true, FLOAT_PANE_STATISTICS);
    handleChangeRightPaneShow(false);

    return () => {
      handleChangeFloatPaneShow(false);
    };
  }, []);

  return (
    <>
      {project.solutionType === SOLUTION_TYPE_SMART_FACTORY && (
        <StatisticsSmartFactory />
      )}
    </>
  );
}

export default StatisticsPane;
