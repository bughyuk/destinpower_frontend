import React from 'react';
import FilterArea from '@/components/FilterArea';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import { usePhysicalDistributionStatus } from '@/modules/physical_distribution/hook';

function PhysicalDistributionFilterNavbar() {
  const { t } = useTranslation();
  const {
    todayCount,
    handleChangeReloadFlag,
  } = usePhysicalDistributionStatus();

  return (
    <>
      {/*<FilterArea />*/}
      <div id="filter-bar">
        <div className="status-map align-items-center justify-content-center">
          <div className="tabBox status-con active">
            <span className="d-flex align-items-center mr-4 pl-3">
              <span className="avatar mr-12pt">
                <span className="avatar-title navbar-avatar">
                  <MaterialIcon name={'all_inclusive'} />
                </span>
              </span>
              <small className="flex d-flex flex-column">
                <span className="navbar-text-50">{t('text_the_whole')}</span>
                <strong className="navbar-text-100">
                  {t('text_number_of_case', {
                    number:
                      todayCount.warehousingCompleteCnt +
                      todayCount.releasingCompleteCnt,
                  })}
                </strong>
              </small>
            </span>
            <span className="d-flex align-items-center mr-4">
              <span className="avatar mr-12pt">
                <span className="avatar-title navbar-avatar">
                  <MaterialIcon name={'arrow_downward'} />
                </span>
              </span>
              <small className="flex d-flex flex-column">
                <span className="navbar-text-50">{t('text_warehousing')}</span>
                <strong className="navbar-text-100">
                  {t('text_number_of_case', {
                    number: todayCount.warehousingCompleteCnt,
                  })}
                </strong>
              </small>
            </span>
            <span className="d-flex align-items-center mr-4">
              <span className="avatar mr-12pt">
                <span className="avatar-title navbar-avatar">
                  <MaterialIcon name={'arrow_upward'} />
                </span>
              </span>
              <small className="flex d-flex flex-column">
                <span className="navbar-text-50">{t('text_release')}</span>
                <strong className="navbar-text-100">
                  {t('text_number_of_case', {
                    number: todayCount.releasingCompleteCnt,
                  })}
                </strong>
              </small>
            </span>
            <div className="filter-btn-cell d-flex flex justify-content-end align-items-center">
              <a className="" onClick={handleChangeReloadFlag}>
                <MaterialIcon name={'autorenew'} className="mr-1" />{' '}
                {t('text_refresh')}
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default PhysicalDistributionFilterNavbar;
