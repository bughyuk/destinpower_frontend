import React, { useEffect, useState } from 'react';
import {
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';
import { useLeftPaneContainer } from '@/modules/setup/hook';
import EventRegister from '@/components/EventRegister';
import EventList from '@/components/EventList';
import EventDetail from '@/components/EventDetail';
import EventEdit from '@/components/EventEdit';
import { useControlEvent, useControlSpace } from '@/modules/map/hook';

function EventPane() {
  return (
    <div className="tab-pane sm-event active">
      <div className="event-inner">
        <EventContent />
      </div>
    </div>
  );
}

function EventContent() {
  const { space } = useControlSpace();
  const {
    directFlag,
    directEventId,
    handleSetDirectEventId,
  } = useControlEvent();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_LIST);
  const [eventId, setEventId] = useState('');

  const { updateScroll } = useLeftPaneContainer();

  useEffect(() => {
    updateScroll();
  }, [status]);

  useEffect(() => {
    setStatus(PANE_STATUS_LIST);
  }, [space]);

  useEffect(() => {
    if (directEventId) {
      setEventId(directEventId);
      setStatus(PANE_STATUS_DETAIL);
      handleSetDirectEventId('');
    }
  }, [directFlag, directEventId]);

  return (
    <>
      {status === PANE_STATUS_LIST && (
        <EventList onStatusChange={setStatus} onChangeEventId={setEventId} />
      )}
      {status === PANE_STATUS_REGISTER && (
        <EventRegister onStatusChange={setStatus} />
      )}
      {status === PANE_STATUS_EDIT && (
        <EventEdit onStatusChange={setStatus} eventId={eventId} />
      )}
      {status === PANE_STATUS_DETAIL && (
        <EventDetail onStatusChange={setStatus} eventId={eventId} />
      )}
    </>
  );
}

export default EventPane;
