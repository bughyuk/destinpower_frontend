import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { postResetPasswordMail } from '@/api/user';
import { ValidUtils } from '@/utils';
import { Alert } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import classNames from 'classnames';
import { Link, useHistory } from 'react-router-dom';

function FindPasswordForm() {
  const { i18n, t } = useTranslation();
  const history = useHistory();
  const [email, setEmail] = useState('');
  const [emailValid, setEmailValid] = useState(false);
  const [loading, setLoading] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    if (emailValid && !loading) {
      setLoading(true);
      const result = await postResetPasswordMail(
        email,
        i18n.language.length > 2 ? i18n.language.slice(0, 2) : i18n.language
      );
      if (result) {
        setSuccess(true);
        setTimeout(() => {
          history.replace('/signin');
        }, 1000);
      }
    }
  };

  useEffect(() => {
    setEmailValid(ValidUtils.validateEmail(email));
  }, [email]);

  return (
    <>
      <h2 className="mb-2">{t('text_find_password')}</h2>
      <p className="mb-5 text-50">{t('msg_find_password_send_reset_link')}</p>
      <form onSubmit={handleSubmit} noValidate={true}>
        <div className="form-group mb-4">
          <label className="form-label essential text-50" htmlFor="email">
            {t('text_email')}
          </label>
          <input
            id="email"
            type="email"
            className="form-line"
            autoComplete={'off'}
            placeholder={t('place_holder_email')}
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          />
        </div>
        {error && (
          <Alert className="alert-soft-accent">
            <div className="d-flex flex-wrap align-items-center">
              <div className="mr-8pt">
                <MaterialIcon name={'error_outline'} />
              </div>
              <div className="flex" style={{ minWidth: '180px' }}>
                <small className="text-black-100">
                  {t('msg_email_not_exist')}
                </small>
              </div>
            </div>
          </Alert>
        )}
        {success && (
          <Alert className="alert-soft-success">
            <div className="d-flex flex-wrap align-items-center">
              <div className="mr-8pt">
                <MaterialIcon name={'done'} />
              </div>
              <div className="flex" style={{ minWidth: '180px' }}>
                <small className="text-black-100">
                  {t('msg_mail_sent_successfully')}
                </small>
              </div>
            </div>
          </Alert>
        )}
        <div className="form-group text-center mb-32pt">
          <button
            className={classNames('btn btn-block btn-lg btn-accent', {
              disabled: !emailValid,
              'is-loading': loading,
            })}
            style={{
              cursor: !emailValid ? 'default' : 'pointer',
            }}
            type="submit"
          >
            <MaterialIcon name={'send'} align={'left'} /> {t('text_send_mail')}
          </button>
        </div>
        {/*
        <ul className="signin-help">
          <li>
            {t('msg_do_not_have_account')}
            <Link className="text-body text-underline" to={'/signup'}>
              {t('text_gen_account')}
            </Link>
          </li>
        </ul>
        */}
      </form>
    </>
  );
}

export default FindPasswordForm;
