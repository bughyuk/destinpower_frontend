import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import TermModal from '@/components/TermModal';
import { Alert } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';

type SignUpTermsProps = {
  onClickAgree: () => void;
};

function SignUpTerms({ onClickAgree }: SignUpTermsProps) {
  const { t } = useTranslation();

  const [isShowAlert, setShowAlert] = useState(false);

  const [terms, setTerms] = useState<{
    privacy: boolean;
    location: boolean;
  }>({
    privacy: false,
    location: false,
  });

  useEffect(() => {
    if (terms.privacy && terms.location) {
      setShowAlert(false);
    }
  }, [terms]);

  const [modalData, setModalData] = useState<{
    show: boolean;
    title: string;
    content: string;
  }>({
    show: false,
    title: '',
    content: '',
  });

  const handleChangeAllTerm = (e: React.ChangeEvent<HTMLInputElement>) => {
    const checked = e.target.checked;
    setTerms({
      privacy: checked,
      location: checked,
    });
  };

  const handleChangeTerm = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTerms({
      ...terms,
      [e.target.id]: e.target.checked,
    });
  };

  const handleShowModal = (title: string, content: string) => {
    setModalData({
      show: true,
      title,
      content,
    });
  };

  const handleCloseModal = () => {
    setModalData({
      show: false,
      title: '',
      content: '',
    });
  };

  const handleClickAgree = () => {
    if (terms.privacy && terms.location) {
      onClickAgree();
    } else {
      setShowAlert(true);
    }
  };

  return (
    <>
      <div className="title-group mb-5">
        <h3 className="m-0">{t('text_agree_to_terms_of_service')}</h3>
        <p className="mt-2 text-50 font-size-16pt">
          {t('msg_agree_to_wata_platform_term_of_service')}
        </p>
      </div>
      <div className="form-group mb-4 border-bottom-1">
        <div className="custom-control custom-checkbox checkbox-lg checkbox-rounded">
          <input
            id="all"
            type="checkbox"
            className="custom-control-input"
            checked={terms.privacy && terms.location}
            onChange={handleChangeAllTerm}
          />
          <label
            htmlFor="all"
            className="custom-control-label font-weight-bolder"
          >
            {t('msg_all_agree')}
          </label>
        </div>
        <p className="desc_check_all text-50">
          {t('msg_all_agree_description')}
        </p>
      </div>
      <div className="form-group mb-4 d-flex align-items-center">
        <div className="custom-control custom-checkbox checkbox-lg checkbox-rounded">
          <input
            id="location"
            type="checkbox"
            className="custom-control-input"
            checked={terms.location}
            onChange={handleChangeTerm}
          />
          <label htmlFor="location" className="custom-control-label">
            {t('text_terms_of_location_agree')}
          </label>
        </div>
        <a
          className="d-flex ml-auto"
          onClick={() =>
            handleShowModal(
              t('text_terms_of_location'),
              t('content_term_of_location')
            )
          }
        >
          <i className="material-icons font-size-16pt">arrow_forward_ios</i>
        </a>
      </div>
      <div className="form-group mb-4 d-flex align-items-center">
        <div className="custom-control custom-checkbox checkbox-lg checkbox-rounded">
          <input
            id="privacy"
            type="checkbox"
            className="custom-control-input"
            checked={terms.privacy}
            onChange={handleChangeTerm}
          />
          <label htmlFor="privacy" className="custom-control-label">
            {t('text_terms_of_privacy_agree')}
          </label>
        </div>
        <a
          className="d-flex ml-auto"
          onClick={() =>
            handleShowModal(
              t('text_terms_of_privacy'),
              t('content_term_of_privacy')
            )
          }
        >
          <i className="material-icons font-size-16pt">arrow_forward_ios</i>
        </a>
      </div>
      <div className="form-group mb-5 d-flex align-items-center"></div>
      {isShowAlert && (
        <Alert className="alert-soft-accent">
          <div className="d-flex flex-wrap align-items-center">
            <div className="mr-8pt">
              <MaterialIcon name={'error_outline'} />
            </div>
            <div className="flex" style={{ minWidth: '180px' }}>
              <small className="text-black-100">
                {t('msg_valid_all_agree_to_terms_of_service')}
              </small>
            </div>
          </div>
        </Alert>
      )}
      <div className="form-group text-center mb-32pt">
        <button
          className="btn btn-block btn-lg btn-accent"
          type="button"
          onClick={handleClickAgree}
        >
          {t('text_agree')}
        </button>
      </div>
      <TermModal
        show={modalData.show}
        title={modalData.title}
        content={modalData.content}
        onHide={handleCloseModal}
        positiveButton={true}
        onPositiveButtonClick={handleCloseModal}
      />
    </>
  );
}

export default SignUpTerms;
