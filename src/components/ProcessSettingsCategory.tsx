import React, { useEffect, useRef } from 'react';

import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import { useControlSpace } from '@/modules/map/hook';
import { UserType } from '@/modules/user/types';
import { useUser } from '@/modules/user/hook';
import {
  Category,
  PROCESS_CATEGORY_CLIENT_MANAGEMENT,
  PROCESS_CATEGORY_FACTORY_MANAGEMENT,
  PROCESS_CATEGORY_PRODUCT_LINE_MANAGEMENT,
  PROCESS_CATEGORY_PRODUCT_MANAGEMENT,
} from '@/modules/physical_distribution/types';

function ProcessSettingsCategory() {
  const { space } = useControlSpace();
  const { user } = useUser();
  const { t } = useTranslation();
  const { handleSetCategoryIdx } = usePhysicalDistributionCategory();

  useEffect(() => {
    handleSetCategoryIdx(undefined);
  }, []);

  const categoryListRef = useRef<
    {
      categoryIdx: Category;
      nameKey: string;
      icon: string;
      allowableUserType: UserType[];
    }[]
  >([
    {
      categoryIdx: PROCESS_CATEGORY_FACTORY_MANAGEMENT,
      icon: 'precision_manufacturing',
      nameKey: '공장 관리',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PROCESS_CATEGORY_PRODUCT_MANAGEMENT,
      icon: 'inventory_2',
      nameKey: '제품타입 관리',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PROCESS_CATEGORY_PRODUCT_LINE_MANAGEMENT,
      icon: 'category',
      nameKey: '제품군 관리',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PROCESS_CATEGORY_CLIENT_MANAGEMENT,
      icon: 'person_search',
      nameKey: '거래처 관리',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
  ]);

  const handleClickCategory = (categoryIdx: Category) => {
    handleSetCategoryIdx(categoryIdx);
  };

  if (!space.floorsMapId) {
    /*
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
     */
  }

  return (
    <>
      <div className="container-fluid d-flex flex-md-row align-items-center py-4">
        <div className="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">
          <div className="mb-24pt mb-sm-0 mr-sm-24pt">
            <h3 className="mb-0">설정</h3>
            <p className="text-70 mb-0">설정 페이지입니다.</p>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <ul className="info-panel accident-panel info-link">
          {categoryListRef.current.map(
            ({ categoryIdx, nameKey, icon, allowableUserType }) => {
              if (allowableUserType.includes(user.userType)) {
                return (
                  <li key={categoryIdx}>
                    <a onClick={() => handleClickCategory(categoryIdx)}>
                      <p>{t(nameKey)}</p>
                      <div className="cell">
                        <em>
                          <MaterialIcon name={icon} />
                        </em>
                      </div>
                    </a>
                  </li>
                );
              }
            }
          )}
        </ul>
      </div>
    </>
  );
}

export default ProcessSettingsCategory;
