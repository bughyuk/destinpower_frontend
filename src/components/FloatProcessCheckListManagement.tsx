import React, { useEffect, useState } from 'react';
import {
  Collapse,
  Nav,
  OverlayTrigger,
  TabContainer,
  TabContent,
  TabPane,
  Tooltip,
} from 'react-bootstrap';
import { Korean } from 'flatpickr/dist/l10n/ko';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import classNames from 'classnames';

import axios from 'axios';
import { fetchProcessCheckListInfo } from '@/api/process';
import { useFloatPane } from '@/modules/setup/hook';
import {
  DragDropContext,
  Droppable,
  Draggable,
  DropResult,
} from 'react-beautiful-dnd';

function FloatProcessCheckListManagement() {
  const [load, setLoad] = useState(false);
  const { checkListId } = useFloatPane();
  const [processCheckTitle, setProcessCheckTitle] = useState('');
  const [processCheckGrps, setProcessCheckGrps] = useState<any[]>([
    {
      id: 'g1',
      name: '조립 1공정',
    },
    {
      id: 'g2',
      name: '조립 2공정',
    },
    {
      id: 'g3',
      name: '조립 3공정',
    },
    {
      id: 'g4',
      name: '조립 4공정',
    },
    {
      id: 'g5',
      name: '조립 5공정',
    },
    {
      id: 'g6',
      name: '조립 6공정',
    },
    {
      id: 'g7',
      name: '조립검사',
    },
    {
      id: 'g8',
      name: '공정품질 검사',
    },
    {
      id: 'g9',
      name: '양산 검사',
    },
    {
      id: 'g10',
      name: '출하 검사',
    },
    {
      id: 'g11',
      name: '출하',
    },
  ]);
  const [processCheckList, setProcessCheckList] = useState<any[]>([
    {
      checkId: '0',
      checkStep: 1,
      checkName: '조립 1공정 (준비공정)',
      processCheckDetailList: [
        {
          checkDetailId: '0_0',
          checkDetailName: 'Production check sheet 및 S/N 확인',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_1',
          checkDetailName: '생산출고자재 품목/수량 출고전표와 일치',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_2',
          checkDetailName: '작업표준에 따른 라벨 부착 확인',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_3',
          checkDetailName: '덕트 및 딘레일 절단 치수 확인',
          workerName: '공정 1 작업자',
        },
        {
          checkDetailId: '0_4',
          checkDetailName: '공정별 자재 투입 확인',
          workerName: '공정 1 작업자',
        },
      ],
    },
    {
      checkId: '1',
      checkStep: 2,
      checkName: '조립 2공정 (기초공정)',
      processCheckDetailList: [
        {
          checkDetailId: '1_0',
          checkDetailName: '작업표준서에 따른 작업 확인',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_1',
          checkDetailName: 'Bolt 체결 상태 확인 (Torque, 아이마킹)',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_2',
          checkDetailName: '케이블 체결상태 확인',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_3',
          checkDetailName: 'FUSE 부착 위치 확인 (전면에 라벨이 보이게 부착)',
          workerName: '공정 2 작업자',
        },
        {
          checkDetailId: '1_4',
          checkDetailName: 'AC Capacitor whdla 전용 토크렌치사용 (MAX 10 N/M)',
          workerName: '공정 2 작업자',
        },
      ],
    },
    {
      checkId: '2',
      checkStep: 3,
      checkName: '조립 3공정 (전장품부착공정)',
      processCheckDetailList: [
        {
          checkDetailId: '2_0',
          checkDetailName: '작업표준에 따른 작업 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_1',
          checkDetailName: '덕트 및 딘레일 부착 상태 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_2',
          checkDetailName: '전장품 부착상태 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_3',
          checkDetailName: '애자 부착상태 확인',
          workerName: '공정 3 작업자',
        },
        {
          checkDetailId: '2_4',
          checkDetailName: '도어 및 커버 보관 상태 확인',
          workerName: '공정 3 작업자',
        },
      ],
    },
    {
      checkId: '3',
      checkStep: 4,
      checkName: '조립 4공정 (부스바공정)',
      processCheckDetailList: [
        {
          checkDetailId: '3_0',
          checkDetailName: '작업표준서에 따른 작업 확인',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_1',
          checkDetailName: '부스바 체결상태 확인',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_2',
          checkDetailName: '작업표준서에 따른 작업 확인',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_3',
          checkDetailName: 'Bolt 체결 상태 확인 (Torque, 아이마킹)',
          workerName: '공정 4 작업자',
        },
        {
          checkDetailId: '3_4',
          checkDetailName: '풀림방지너트와 일반너트 사용 확인',
          workerName: '공정 4 작업자',
        },
      ],
    },
    {
      checkId: '4',
      checkStep: 5,
      checkName: '조립 5공정 (하네스공정)',
      processCheckDetailList: [
        {
          checkDetailId: '4_0',
          checkDetailName: '스마트와이어링키트 사용 작업 확인',
          workerName: '공정 5 작업자',
        },
        {
          checkDetailId: '4_1',
          checkDetailName: '케이블 체결상태 확인',
          workerName: '공정 5 작업자',
        },
        {
          checkDetailId: '4_2',
          checkDetailName: '페라이트코어 부착상태 확인',
          workerName: '공정 5 작업자',
        },
        {
          checkDetailId: '4_3',
          checkDetailName: '통신케이블 배선 확인',
          workerName: '공정 5 작업자',
        },
      ],
    },
    {
      checkId: '5',
      checkStep: 6,
      checkName: '조립 6공정 (하네스공정)',
      processCheckDetailList: [
        {
          checkDetailId: '5_0',
          checkDetailName: '제품 내외부 청결 상태 확인',
          workerName: '공정 6 작업자',
        },
        {
          checkDetailId: '5_1',
          checkDetailName: '세이프티커버 부착 상태 확인',
          workerName: '공정 6 작업자',
        },
        {
          checkDetailId: '5_2',
          checkDetailName: '라벨류(주의, 심볼등등) 부착상태 확인',
          workerName: '공정 6 작업자',
        },
        {
          checkDetailId: '5_3',
          checkDetailName: '첨부품 확인',
          workerName: '공정 6 작업자',
        },
      ],
    },
    {
      checkId: '6',
      checkStep: 7,
      checkName: '조립 검사',
      processCheckDetailList: [
        {
          checkDetailId: '6_0',
          checkDetailName: '하네스 케이블 결선상태',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_1',
          checkDetailName: '조립 C/S(조하시스템 자체 C/S)',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_2',
          checkDetailName: 'Label 및 볼트, Fuse 상태 확인',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_3',
          checkDetailName: '내부 최종 조립 상태',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_4',
          checkDetailName: '단선상태 확인',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_5',
          checkDetailName: '자재/도면 변경, 문제 Point 확인',
          workerName: '공정 7 작업자',
        },
        {
          checkDetailId: '6_6',
          checkDetailName: 'LF1(Reactor) 확인',
          workerName: '공정 7 작업자',
        },
      ],
    },
    {
      checkId: '7',
      checkStep: 8,
      checkName: '공정품질 검사',
      processCheckDetailList: [
        {
          checkDetailId: '7_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 8 작업자',
        },
      ],
    },
    {
      checkId: '8',
      checkStep: 9,
      checkName: '양산 검사',
      processCheckDetailList: [
        {
          checkDetailId: '8_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 9 작업자',
        },
      ],
    },
    {
      checkId: '9',
      checkStep: 10,
      checkName: '출하 검사',
      processCheckDetailList: [
        {
          checkDetailId: '9_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 10 작업자',
        },
      ],
    },
    {
      checkId: '10',
      checkStep: 11,
      checkName: '출하',
      processCheckDetailList: [
        {
          checkDetailId: '10_0',
          checkDetailName: '진척 여부 확인',
          workerName: '공정 11 작업자',
        },
      ],
    },
  ]);

  useEffect(() => {
    // 체크리스트에서 아이템 클릭할 때 정보를 가져옴
    if (checkListId !== '-1') {
      handleFetchCheckList();
    }
  }, [checkListId]);

  const handleProcessCheckListAdd = () => {
    setProcessCheckList([
      ...processCheckList,
      { checkId: '-1', processCheckDetailList: [] },
    ]);
  };

  const handleFetchCheckList = async () => {
    setLoad(false);

    if (checkListId.length === 0) return;

    try {
      const { name, steps, grps } = await fetchProcessCheckListInfo(
        checkListId
      );

      if (name) {
        setProcessCheckTitle(name);
      }

      if (grps) {
        setProcessCheckGrps(grps);
      }

      // empty check list
      if (!steps) {
        setProcessCheckList([{ checkId: '-1', processCheckDetailList: [] }]);
      } else {
        const newSteps = steps.map((step: any) => {
          return {
            checkId: step.grp.id,
            checkStep: step.ord,
            checkName: step.name,
            processCheckDetailList: step.items.map((item: any) => {
              return {
                checkDetailId: item.id,
                checkDetailName: item.name,
                checkDetailStep: item.ord,
              };
            }),
          };
        });

        setProcessCheckList(newSteps);
      }

      setLoad(true);
    } catch (error) {
      alert(error);
    }
  };

  if (!load) {
    return <></>;
  }

  const handleProcessCheckListUpdate = async () => {
    const data = {
      name: processCheckTitle,
      steps: processCheckList.map((x) => ({
        name: x.checkName,
        grp: x.checkId,
        ord: x.checkStep,
        items: x.processCheckDetailList.map((item: any) => ({
          name: item.checkDetailName,
          ord: item.checkDetailStep,
        })),
      })),
    };

    const axiosInstance = axios.create({
      withCredentials: false,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      },
    });

    try {
      const response = await axiosInstance.post(
        `http://testserver:48080/dpserv/process-checklist/${checkListId}/update`,
        data
      );

      alert('success update');
    } catch (error) {
      alert(error);
    }
  };

  const reorder = (list: any, startIndex: number, endIndex: number) => {
    let result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    result = result.map((x: any, index) => ({ ...x, checkStep: index }));

    return result;
  };

  const onDragEnd = (result: DropResult) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    let list = [...processCheckList];

    list = reorder(list, result.source.index, result.destination.index);

    setProcessCheckList(list);
  };

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-3">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0 ">
            {processCheckTitle}
          </h3>
        </div>
      </div>
      <div>
        <div className="page-separator mt-5">
          <div className="page-separator__text">체크리스트</div>
        </div>
        <div className="accordion js-accordion accordion--boxed list-group-flush list-bg-light mb-4">
          {load && (
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="droppable">
                {(provided: any) => (
                  <div {...provided.droppableProps} ref={provided.innerRef}>
                    {processCheckList.map((processCheck, index) => (
                      <div key={index} className="mb-2">
                        <Draggable
                          key={index}
                          draggableId={`${index}`}
                          index={index}
                        >
                          {(provided: any) => (
                            <div
                              key={index}
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              <ProcessCheckListManagementItem
                                key={index}
                                index={index}
                                processCheck={processCheck}
                                processCheckGrps={processCheckGrps}
                                processCheckList={processCheckList}
                                setProcessCheckList={setProcessCheckList}
                              />
                            </div>
                          )}
                        </Draggable>
                      </div>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          )}
          <div className="d-flex justify-content-center p-3">
            <a
              className="btn btn-sm btn-rounded btn-accent"
              onClick={handleProcessCheckListAdd}
            >
              <i className="material-icons">add</i>공정 추가
            </a>
          </div>
        </div>
        <div className="d-flex justify-content-center p-3">
          <a className="btn btn-rounded btn-secondary mx-1">취소하기</a>
          <a
            className="btn btn-rounded btn-dark mx-1"
            onClick={handleProcessCheckListUpdate}
          >
            적용하기
          </a>
        </div>
      </div>
    </>
  );
}

type ProcessCheckListManagementItemProps = {
  index: number;
  processCheck: any;
  processCheckList: any;
  processCheckGrps: any;
  setProcessCheckList: any;
};

function ProcessCheckListManagementItem({
  index,
  processCheck,
  processCheckGrps,
  processCheckList,
  setProcessCheckList,
}: ProcessCheckListManagementItemProps) {
  // 그룹 변경
  const onChangeCheckId = (e: any) => {
    const targetIdx = e.target.value;
    const newList = [...processCheckList];

    if (newList.filter((x) => x.checkId === targetIdx).length > 0) {
      alert('체크리스트 중복');
      return;
    }

    const idx = newList.findIndex((x) => x.checkId === processCheck.checkId);
    newList[idx].checkId = targetIdx;

    setProcessCheckList(newList);
  };

  // 공정 항목 삭제
  const handleProcessCheckDelete = (e: any) => {
    const newList = [...processCheckList];
    newList.splice(index, 1);

    setProcessCheckList(newList);
  };

  // 체크 항목 추가
  const handleCheckDetailAdd = () => {
    const list = [...processCheckList];
    const idx = list.findIndex((x) => x.checkId === processCheck.checkId);

    list[idx].processCheckDetailList.push({
      checkDetailId: list[idx].processCheckDetailList.length,
      checkStep: list[idx].processCheckDetailList.length,
    });

    setProcessCheckList(list);
  };

  // 체크 항목 삭제
  const handleCheckDetailDelete = (e: any, id: string) => {
    const newList = [...processCheckList];
    const idx = newList.findIndex((x) => x.checkId === processCheck.checkId);

    const newDetailList = newList[idx].processCheckDetailList.filter(
      (x: any) => x.checkDetailId !== id
    );

    newList[idx].processCheckDetailList = newDetailList;

    setProcessCheckList(newList);
  };

  // 공정 항목 입력
  const handleCheckNameChange = (e: any) => {
    const list = [...processCheckList];
    const idx = list.findIndex((x) => x.checkId === processCheck.checkId);

    list[idx].checkName = e.target.value;

    setProcessCheckList(list);
  };

  // 체크 항목 입력
  const handleCheckDetailNameChange = (e: any) => {
    const list = [...processCheckList];
    const idx = list.findIndex((x) => x.checkId === processCheck.checkId);
    const detailIdx = list[idx].processCheckDetailList.findIndex(
      (x: any) => x.checkDetailId == e.target.id
    );

    list[idx].processCheckDetailList[detailIdx].checkDetailName =
      e.target.value;

    setProcessCheckList(list);
  };

  const reorder = (list: any, startIndex: number, endIndex: number) => {
    let result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    result = result.map((x: any, index) => ({ ...x, checkDetailStep: index }));

    return result;
  };

  const onDragEnd = (result: DropResult) => {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const list = [...processCheckList];
    const idx = list.findIndex((x) => x.checkId === processCheck.checkId);

    list[idx].processCheckDetailList = reorder(
      list[idx].processCheckDetailList,
      result.source.index,
      result.destination.index
    );

    setProcessCheckList(list);
  };

  return (
    <>
      <div className="accordion__item open">
        <div className="d-flex align-items-center px-4 py-3">
          <span className="flex mr-2">
            <input
              type="text"
              className="form-control"
              onChange={handleCheckNameChange}
              value={processCheck.checkName}
            />
          </span>
          <span>
            <select
              name="category"
              className="form-control custom-select"
              value={processCheck.checkId}
              onChange={onChangeCheckId}
            >
              <option value="-1" selected disabled hidden>
                그룹 선택
              </option>
              {processCheckGrps.map((group: any, index: number) => (
                <option key={index} value={group.id}>
                  {group.name}
                </option>
              ))}
            </select>
          </span>
          <div className="mx-3">
            <a className="btn-list-close" onClick={handleProcessCheckDelete}>
              <span className="material-icons">close</span>
            </a>
          </div>
          <div className="media-right ml-auto">
            <span
              className="js-dragula-handle material-icons"
              style={{
                cursor: 'move',
              }}
            >
              drag_handle
            </span>
          </div>
        </div>
        <div className="accordion__menu collapse show">
          <div className="list-group list-group-flush border-top-1">
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="droppable">
                {(provided: any) => (
                  <div {...provided.droppableProps} ref={provided.innerRef}>
                    {processCheck.processCheckDetailList.map(
                      (processCheckDetail: any, index: number) => (
                        <Draggable
                          key={index}
                          draggableId={`${index}`}
                          index={index}
                        >
                          {(provided: any) => (
                            <div
                              key={processCheckDetail.checkDetailId}
                              className="list-group-item px-4 py-3 h-auto d-flex"
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              <span className="flex">
                                <input
                                  type="text"
                                  className="form-control font-size-16pt"
                                  id={processCheckDetail.checkDetailId}
                                  onChange={handleCheckDetailNameChange}
                                  value={processCheckDetail.checkDetailName}
                                />
                              </span>
                              <div className="mx-3">
                                <a
                                  className="btn-list-close"
                                  onClick={(e) =>
                                    handleCheckDetailDelete(
                                      e,
                                      processCheckDetail.checkDetailId
                                    )
                                  }
                                >
                                  <span className="material-icons">close</span>
                                </a>
                              </div>
                              <div className="ml-auto">
                                <span
                                  className="js-dragula-handle material-icons"
                                  style={{
                                    cursor: 'move',
                                  }}
                                >
                                  drag_handle
                                </span>
                              </div>
                            </div>
                          )}
                        </Draggable>
                      )
                    )}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
          <div className="d-flex justify-content-center border-top-1 p-3">
            <a className="btn text-danger" onClick={handleCheckDetailAdd}>
              <i className="material-icons">add</i>항목 추가
            </a>
          </div>
        </div>
      </div>
    </>
  );
}

export default FloatProcessCheckListManagement;
