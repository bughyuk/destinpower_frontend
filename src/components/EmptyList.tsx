import React, { ReactNode } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useTranslation } from 'react-i18next';

type EmptyListProps = {
  listType: 'project' | 'space';
};

function EmptyList({ listType }: EmptyListProps) {
  const { t } = useTranslation();
  let icon: ReactNode;
  let text = '';

  if (listType === 'project') {
    icon = <FontAwesomeIcon icon={['far', 'folder']} />;
    text = t('msg_project_empty');
  } else if (listType === 'space') {
    icon = <FontAwesomeIcon icon={['far', 'map']} />;
    text = t('msg_space_empty');
  }

  return (
    <div className="empty-state">
      <p className="empty-icon m-0">{icon}</p>
      <em className="empty-txt mb-4">{text}</em>
    </div>
  );
}

export default EmptyList;
