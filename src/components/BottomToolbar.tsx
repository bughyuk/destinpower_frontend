import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Map } from 'ol';
import MaterialIcon from '@/components/MaterialIcon';
import classNames from 'classnames';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

type BottomToolbarProps = {
  map: Map | null;
  visibleLocationButton?: boolean;
  visibleFloorPlanButton?: boolean;
  visibleMousePositionInfoBar?: boolean;
  activeVisibleFloorPlanButton?: boolean;
  onClickLocationButton?: () => void;
  onClickFloorPlanButton?: () => void;
};

function BottomToolbar({
  map,
  visibleLocationButton = true,
  visibleFloorPlanButton = false,
  visibleMousePositionInfoBar = false,
  activeVisibleFloorPlanButton = false,
  onClickLocationButton,
  onClickFloorPlanButton,
}: BottomToolbarProps) {
  const { t } = useTranslation();

  return (
    <>
      <div className="btn-widget-holder z-index-0">
        <ul>
          {visibleFloorPlanButton && (
            <OverlayTrigger
              placement={'left'}
              overlay={
                <Tooltip id={'visibleFloorPlan'}>
                  {activeVisibleFloorPlanButton
                    ? t('text_hide_floor_plan')
                    : t('text_view_floor_plan')}
                </Tooltip>
              }
            >
              <li>
                <button
                  type="button"
                  className={classNames('btn btn-white mb-3', {
                    active: activeVisibleFloorPlanButton,
                  })}
                  style={{
                    position: 'relative',
                  }}
                  onClick={onClickFloorPlanButton}
                >
                  <MaterialIcon
                    name={
                      activeVisibleFloorPlanButton
                        ? 'visibility'
                        : 'visibility_off'
                    }
                    style={{
                      position: 'absolute',
                      top: '50%',
                      left: '50%',
                      transform: 'translate(-50%, -50%)',
                    }}
                  />
                </button>
              </li>
            </OverlayTrigger>
          )}
          {visibleLocationButton && (
            <li>
              <button
                type="button"
                className="btn btn-white mb-3"
                onClick={() => onClickLocationButton?.call(null)}
              >
                <FontAwesomeIcon icon={['fas', 'crosshairs']} />
              </button>
            </li>
          )}
          <li>
            <div className="btn-group-vertical">
              <button
                type="button"
                className="btn btn-white"
                onClick={() =>
                  map?.getView().animate({
                    zoom: (map?.getView().getZoom() || 0) + 0.5,
                    duration: 350,
                  })
                }
              >
                <FontAwesomeIcon icon={['fas', 'plus']} />
              </button>
              <button
                type="button"
                className="btn btn-white"
                onClick={() =>
                  map?.getView().animate({
                    zoom: (map?.getView().getZoom() || 0) + -0.5,
                    duration: 350,
                  })
                }
              >
                <FontAwesomeIcon icon={['fas', 'minus']} />
              </button>
            </div>
          </li>
        </ul>
      </div>
      {visibleMousePositionInfoBar && (
        <div className="info-bar">
          <span id={'mousePositionInfoBar'}></span>
        </div>
      )}
    </>
  );
}

export default BottomToolbar;
