import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionProduct,
  usePhysicalDistributionWarehouse,
} from '@/modules/physical_distribution/hook';
import { fetchWarehouse } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import {
  PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
  WarehouseDetail,
  WarehouseStock,
} from '@/modules/physical_distribution/types';
import { CommonUtils } from '@/utils';

function FloatPhysicalDistributionWarehouse() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const { handleSetCategoryIdx } = usePhysicalDistributionCategory();
  const { handleSetDirectProduct } = usePhysicalDistributionProduct();
  const { selectedWarehouse } = usePhysicalDistributionWarehouse();
  const [
    warehouseDetail,
    setWarehouseDetail,
  ] = useState<WarehouseDetail | null>(null);

  useEffect(() => {
    if (selectedWarehouse) {
      handleFetchWarehouse();
    } else {
      setWarehouseDetail(null);
    }
  }, [selectedWarehouse]);

  const handleFetchWarehouse = async () => {
    const warehouseId = selectedWarehouse?.warehouseId;
    if (warehouseId) {
      const result = await fetchWarehouse(project.id, warehouseId);
      if (result) {
        setWarehouseDetail(result);
      }
    }
  };

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_product_list')}
          </h3>
        </div>
      </div>
      <div className="contents-view">
        {!selectedWarehouse?.warehouseId && (
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>

            <em className="empty-txt m-0">
              {t('msg_selection_warehouse_from_the_list_on_the_left')}
            </em>
          </div>
        )}
        {selectedWarehouse?.warehouseId && (
          <>
            <div className="page-separator mt-5">
              <div className="page-separator__text">
                {t('text_storage_information_of_warehouse', {
                  warehouseName: selectedWarehouse.warehouseName,
                })}
              </div>
            </div>
            {warehouseDetail?.stock.length === 0 && (
              <div className="empty-state border-top-0">
                <p className="empty-icon m-0">
                  <span className="material-icons-outlined">error_outline</span>
                </p>

                <em className="empty-txt m-0">{t('msg_stock_not_exist')}</em>
              </div>
            )}
            {warehouseDetail && warehouseDetail.stock.length > 0 && (
              <table className="table mb-4 table-nowrap thead-bg-light">
                <thead>
                  <tr>
                    <th>{t('text_product_information')}</th>
                    <th>{t('text_stock_quantity')}</th>
                  </tr>
                </thead>
                <tbody>
                  {warehouseDetail.stock.map((stock) => (
                    <WarehouseStockItem
                      key={stock.productId}
                      {...stock}
                      onClick={() => {
                        handleSetDirectProduct(stock);
                        handleSetCategoryIdx(
                          PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT
                        );
                      }}
                    />
                  ))}
                </tbody>
              </table>
            )}
          </>
        )}
      </div>
    </>
  );
}

type WarehouseStockItemProps = WarehouseStock & {
  onClick: () => void;
};

function WarehouseStockItem({
  productName,
  imgUrl,
  productSizeName,
  productWeight,
  boxQuantity,
  totalQuantity,
  onClick,
}: WarehouseStockItemProps) {
  return (
    <tr onClick={onClick}>
      <td>
        <a>
          <div className="flex d-flex align-items-center mr-16pt">
            <span className="avatar mr-12pt blank-img">
              <img src={imgUrl} className="avatar-img rounded" />
            </span>
            <div className="flex">
              <h6 className="m-0">{productName}</h6>
              <div className="card-subtitle text-50">
                <small className="mr-2">{productSizeName}</small>
                <small className="mr-2">
                  {CommonUtils.getProductStandard(productWeight, boxQuantity)}
                </small>
              </div>
            </div>
          </div>
        </a>
      </td>
      <td>{CommonUtils.numberWithCommas(totalQuantity)}</td>
    </tr>
  );
}

export default FloatPhysicalDistributionWarehouse;
