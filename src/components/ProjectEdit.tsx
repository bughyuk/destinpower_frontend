import React, { useEffect, useState } from 'react';
import { useProjectEdit } from '@/modules/project/hook';
import { useAsync } from '@/modules/common';
import { fetchProject, putProject } from '@/api/project';
import { ProjectDetail } from '@/modules/project/types';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import ProjectInfoInput from '@/components/ProjectInfoInput';
import InvalidAlert from '@/components/InvalidAlert';
import { useHistory } from 'react-router-dom';

function ProjectEdit() {
  const [fetchState, fetchApi] = useAsync(fetchProject);
  const { data: fetchData } = fetchState;
  const [projectName, setProjectName] = useState('');
  const [note, setNote] = useState('');
  const [isValid, setValid] = useState(false);
  const { projectId, handleSetEditInitialState } = useProjectEdit();
  const [putState, putApi] = useAsync(putProject);
  const { data: putData } = putState;
  const history = useHistory();

  useEffect(() => {
    fetchApi(projectId);

    return () => {
      handleSetEditInitialState();
    };
  }, []);

  useEffect(() => {
    if (projectName && note) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [projectName, note]);

  useEffect(() => {
    if (fetchData) {
      const projectInfo = fetchData as ProjectDetail;
      setProjectName(projectInfo.projectName);
      setNote(projectInfo.note);
      setValid(true);
    }
  }, [fetchData]);

  const handleSubmit = () => {
    putApi(projectId, {
      projectName,
      note,
    });
  };

  useEffect(() => {
    if (putData) {
      history.replace('/home');
    }
  }, [putData]);

  return (
    <>
      <Header />
      <ProjectInfoInput
        projectName={projectName}
        note={note}
        onChangeProjectName={setProjectName}
        onChangeNote={setNote}
      />
      <Footer valid={isValid} onSubmit={handleSubmit} />
    </>
  );
}

function Header() {
  const { t } = useTranslation();

  return (
    <>
      <div className="container-fluid d-flex align-items-center py-4">
        <div className="flex d-flex">
          <div className="mr-24pt">
            <h3 className="mb-0">{t('text_changing_information')}</h3>
          </div>
        </div>
      </div>
    </>
  );
}

type FooterProps = {
  valid: boolean;
  onSubmit: () => void;
};

function Footer({ valid, onSubmit }: FooterProps) {
  const history = useHistory();
  const { t } = useTranslation();
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (valid) {
      setShowInvalidMessage(false);
    }
  }, [valid]);

  return (
    <>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => history.replace('/home')}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0')}
            onClick={() => {
              if (valid) {
                setShowInvalidMessage(false);
                onSubmit();
              } else {
                setShowInvalidMessage(true);
              }
            }}
          >
            {t('text_do_edit')}
          </a>
        </div>
      </div>
    </>
  );
}

export default ProjectEdit;
