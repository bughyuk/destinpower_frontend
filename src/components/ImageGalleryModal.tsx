import React from 'react';
import { Modal, ModalDialog } from 'react-bootstrap';
import { ModalProps } from '@/modules/common';
import MaterialIcon from '@/components/MaterialIcon';

type ImageGalleryModalProps = ModalProps & {
  imageUrl: string;
};

function ImageGalleryModal({ show, imageUrl, onHide }: ImageGalleryModalProps) {
  return (
    <Modal
      show={show}
      onHide={() => {
        onHide();
      }}
      className={'img-gallery'}
      dialogClassName={'modal-gallery-container'}
      contentClassName={'img-gallery-modal-content'}
    >
      <button type="button" className="close custom-close" onClick={onHide}>
        <span>
          <MaterialIcon name={'clear'} />
        </span>
      </button>
      <div className="modal-img-container">
        <img src={imageUrl} />
      </div>
    </Modal>
  );
}

export default ImageGalleryModal;
