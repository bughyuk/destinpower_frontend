import React, { useEffect, useRef, useState } from 'react';
import { PANE_STATUS_LIST, PaneStatus } from '@/utils/constants/common';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import FormLabel from '@/components/FormLabel';
import {
  Assets,
  ASSETS_CATEGORY_HUMIDITY,
  ASSETS_CATEGORY_TEMPERATURE,
  ASSETS_CATEGORY_TEMPERATURE_AND_HUMIDITY,
  TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER,
  TemperatureAndHumidity,
} from '@/modules/assets/types';
import { useControlProject } from '@/modules/map/hook';
import { Nav, TabContainer, TabContent, TabPane } from 'react-bootstrap';
import classNames from 'classnames';
import moment from 'moment';
import {
  fetchAssetsTemperatureAndHumidity,
  postUpdateAssetsActiveFlag,
} from '@/api/assets';
import { useSetupAssets } from '@/modules/setup/hook';

const assetsTimer: ReturnType<typeof setInterval> | null = null;

type AssetsDetailProps = {
  assets: Assets;
  onChangeStatus: (status: PaneStatus) => void;
};

function AssetsDetail({ assets, onChangeStatus }: AssetsDetailProps) {
  return (
    <>
      <Header
        assetsName={assets?.asset_name || ''}
        onChangeStatus={onChangeStatus}
      />
      <Body assets={assets} />
    </>
  );
}

type HeaderProps = {
  assetsName: string;
  onChangeStatus: (status: PaneStatus) => void;
};

function Header({ assetsName, onChangeStatus }: HeaderProps) {
  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <a className="pr-2" onClick={() => onChangeStatus(PANE_STATUS_LIST)}>
          <MaterialIcon name={'arrow_back'} />
        </a>
        <div className="mr-24pt">
          <h3 className="mb-0">{assetsName}</h3>
        </div>
      </div>
    </div>
  );
}

type AssetsTab = 'info' | 'temperatureAndHumidity';
type BodyProps = {
  assets: Assets;
};

function Body({ assets }: BodyProps) {
  const { t } = useTranslation();
  const [activeKey, setActiveKey] = useState<AssetsTab>('info');

  let isTemperatureAndHumidity = false;
  const numberAssetCategory = Number(assets.asset_category);
  if (
    numberAssetCategory === ASSETS_CATEGORY_TEMPERATURE ||
    numberAssetCategory === ASSETS_CATEGORY_HUMIDITY ||
    numberAssetCategory === ASSETS_CATEGORY_TEMPERATURE_AND_HUMIDITY
  ) {
    isTemperatureAndHumidity = true;
  }

  return (
    <div className="container-fluid contents-view">
      <TabContainer
        defaultActiveKey={'info'}
        transition={false}
        onSelect={(eventKey) => setActiveKey(eventKey as AssetsTab)}
      >
        {isTemperatureAndHumidity && (
          <Nav className="contents-tab the-num02 mb-4">
            <Nav.Link eventKey={'info'} bsPrefix={' '}>
              <span
                className={classNames({
                  'menu-active': activeKey === 'info',
                  'menu-cover': activeKey !== 'info',
                })}
              >
                <span>{t('text_info')}</span>
              </span>
            </Nav.Link>
            <Nav.Link eventKey={'temperatureAndHumidity'} bsPrefix={' '}>
              <span
                className={classNames({
                  'menu-active': activeKey === 'temperatureAndHumidity',
                  'menu-cover': activeKey !== 'temperatureAndHumidity',
                })}
              >
                <span>{t('text_temperature_humidity_status')}</span>
              </span>
            </Nav.Link>
          </Nav>
        )}
        <TabContent>
          <TabPane eventKey={'info'}>
            <InfoPane {...assets} />
          </TabPane>
          <TabPane eventKey={'temperatureAndHumidity'}>
            <TemperatureAndHumidityPane uuid={assets.asset_uuid} />
          </TabPane>
        </TabContent>
      </TabContainer>
    </div>
  );
}

type InfoPaneProps = Assets;

function InfoPane({
  asset_name,
  asset_position,
  util_name,
  asset_type,
  asset_uuid,
  asset_categorynm,
  active_flag,
}: InfoPaneProps) {
  const [activeFlag, setActiveFlag] = useState(active_flag);
  const { handleChangeReloadFlag } = useSetupAssets();

  const handleChangeActiveFlag = async (checked: boolean) => {
    const result = await postUpdateAssetsActiveFlag({
      asset_uuid,
      onoff: checked,
    });

    if (result) {
      handleChangeReloadFlag();
      setActiveFlag(checked);
    }
  };

  return (
    <>
      <div className="form-group">
        <FormLabel textKey={'text_assets_name'} className={'mb-0'} />
        <p className="form-text">{asset_name}</p>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_classification'} className={'mb-0'} />
        <p className="form-text">{asset_categorynm}</p>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_location'} className={'mb-0'} />
        <p className="form-text">{asset_position}</p>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_tag_connection'} className={'mb-0'} />
        <p className="form-text">{util_name}</p>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_type'} className={'mb-0'} />
        <p className="form-text">{asset_type}</p>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_uuid'} className={'mb-0'} />
        <p className="form-text">{asset_uuid}</p>
      </div>
      <div className="form-group">
        <div className="set-list">
          <FormLabel textKey={'text_whether_of_display'} className={'mb-0'} />
          <div className="control-cover">
            <div className="custom-control custom-checkbox-toggle ml-8pt">
              <input
                type="checkbox"
                id="whetherOfDisplay"
                className="custom-control-input"
                checked={activeFlag}
                onChange={(e) => handleChangeActiveFlag(e.target.checked)}
              />
              <label
                className="custom-control-label"
                htmlFor="whetherOfDisplay"
              >
                active
              </label>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

type TemperatureAndHumidityPaneProps = {
  uuid: string;
};

function TemperatureAndHumidityPane({ uuid }: TemperatureAndHumidityPaneProps) {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const todayRef = useRef(moment().format('YYYY-MM-DD'));
  const [load, setLoad] = useState(false);
  const [temperatureAndHumidityList, setTemperatureAndHumidityList] = useState<
    TemperatureAndHumidity[]
  >([]);

  useEffect(() => {
    handleFetchTemperatureAndHumidity();
  }, [uuid]);

  const handleFetchTemperatureAndHumidity = async () => {
    setLoad(false);
    const data = await fetchAssetsTemperatureAndHumidity({
      projectid: project.id,
      sensorid: uuid,
      dateinfo: todayRef.current,
    });
    setTemperatureAndHumidityList(data);
    setLoad(true);
  };

  return (
    <>
      <div className="d-flex align-items-center mb-2">
        <span className="ml-auto">{todayRef.current}</span>
      </div>
      {!load && <></>}
      {load && temperatureAndHumidityList.length === 0 && (
        <div className="empty-state">
          <em className="empty-txt">{t('msg_data_not_exist')}</em>
        </div>
      )}
      {load && temperatureAndHumidityList.length > 0 && (
        <table className="table mb-4 table-nowrap">
          <colgroup>
            <col width="25%" />
            <col width="25%" />
            <col width="25%" />
            <col width="25%" />
          </colgroup>
          <thead>
            <tr>
              <th>{t('text_time')}</th>
              <th>{t('text_temperature')}</th>
              <th>{t('text_humidity')}</th>
              <th>{t('text_status')}</th>
            </tr>
          </thead>
          <tbody>
            {temperatureAndHumidityList.map((temperatureAndHumidity, i) => (
              <TemperatureAndHumidityItem key={i} {...temperatureAndHumidity} />
            ))}
          </tbody>
        </table>
      )}
    </>
  );
}

type TemperatureAndHumidityItemProps = TemperatureAndHumidity;

function TemperatureAndHumidityItem({
  hourinfo,
  celsius,
  humidity,
  signal,
}: TemperatureAndHumidityItemProps) {
  return (
    <tr>
      <td>{hourinfo}</td>
      <td>
        <span
          className={classNames({
            'text-danger': signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER,
          })}
        >
          {celsius}&#8451;
        </span>
      </td>
      <td>{`${humidity}%`}</td>
      <td>
        <span
          className={classNames('material-icons-outlined', {
            'text-danger': signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER,
            'text-success': signal !== TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER,
          })}
        >
          {signal === TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER && 'mood_bad'}
          {signal !== TEMPERATURE_AND_HUMIDITY_SIGNAL_DANGER && 'mood'}
        </span>
      </td>
    </tr>
  );
}

export default AssetsDetail;
