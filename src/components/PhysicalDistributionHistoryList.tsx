import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { DistributionHistory } from '@/modules/physical_distribution/types';
import { fetchDistributionHistories } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import moment from 'moment';
import { CommonUtils } from '@/utils';
import Preloader from '@/components/Preloader';
import { usePhysicalDistributionHistory } from '@/modules/physical_distribution/hook';

function PhysicalDistributionHistoryList() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const {
    selectedDistributionHistory,
    reloadFlag,
    handleSetDistributionHistory,
  } = usePhysicalDistributionHistory();
  const [load, setLoad] = useState(false);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPages] = useState(0);
  const [distributionHistories, setDistributionHistories] = useState<
    DistributionHistory[]
  >([]);

  useEffect(() => {
    return () => {
      handleSetDistributionHistory(null);
    };
  }, []);

  useEffect(() => {
    handleFetchDistributionHistory();
  }, [page]);

  const handleFetchDistributionHistory = async () => {
    setLoading(true);
    if (page === 1) {
      setLoad(false);
    }
    const data = await fetchDistributionHistories({
      projectId: project.id,
      page,
    });
    setDistributionHistories([...distributionHistories, ...data.content]);
    setTotalPages(data.totalPage);
    if (page === 1) {
      setLoad(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    handleFetchReload();
  }, [reloadFlag]);

  const handleFetchReload = async () => {
    setDistributionHistories([]);
    setTotalPages(0);
    let reloadTotalPage = 0;
    let reloadDistributionHistories: DistributionHistory[] = [];
    for (let i = 1; i <= page; i++) {
      setLoading(true);
      if (i === 1) {
        setLoad(false);
      }
      const data = await fetchDistributionHistories({
        projectId: project.id,
        page: i,
      });

      reloadTotalPage = data.totalPage;
      reloadDistributionHistories = [
        ...reloadDistributionHistories,
        ...data.content,
      ];

      if (i === 1) {
        setLoad(true);
      }
      setLoading(false);
    }

    setDistributionHistories(reloadDistributionHistories);
    setTotalPages(reloadTotalPage);
  };

  const handleClickHistory = (clickHistory: DistributionHistory) => {
    let history: DistributionHistory | null = null;
    if (selectedDistributionHistory?.logisticsId !== clickHistory.logisticsId) {
      history = clickHistory;
    }
    handleSetDistributionHistory(history);
  };

  return (
    <div className="cd-panel__content">
      {!load && <Preloader />}
      {load && distributionHistories.length === 0 && (
        <em className="none-list mb-4">{t('msg_not_exist_histories')}</em>
      )}
      {load && distributionHistories.length > 0 && (
        <>
          <div className="accordion list-group-flush accordion-line">
            <div className={classNames('accordion__item')}>
              <Collapse in={true}>
                <div className="accordion__menu">
                  {distributionHistories.map((distributionHistory) => (
                    <HistoryItem
                      key={distributionHistory.logisticsId}
                      {...distributionHistory}
                      activeId={selectedDistributionHistory?.logisticsId || ''}
                      onClick={() => handleClickHistory(distributionHistory)}
                    />
                  ))}
                </div>
              </Collapse>
            </div>
          </div>
          {page !== totalPage && (
            <div className="container-fluid text-center">
              <a
                className={classNames('btn btn-rounded btn-outline-dark mb-3', {
                  disabled: loading,
                  'is-loading': loading,
                })}
                onClick={() => {
                  if (!loading) {
                    setPage(page + 1);
                  }
                }}
              >
                {t('text_see_more_details')}
              </a>
            </div>
          )}
        </>
      )}
    </div>
  );
}

type HistoryItemProps = DistributionHistory & {
  activeId: string;
  onClick: () => void;
};

function HistoryItem({
  logisticsId,
  logisticsCategory,
  status,
  productName,
  warehouseName,
  logisticsQuantity,
  updateDate,
  updateId,
  registId,
  activeId,
  deleteFlag,
  onClick,
}: HistoryItemProps) {
  const { t } = useTranslation();
  const distributionHistoryTextKey = CommonUtils.getDistributionHistoryTextKey(
    logisticsCategory,
    status
  );
  return (
    <div
      className={classNames('accordion__menu-link', {
        'state-up': logisticsQuantity > 0,
        'state-down': logisticsQuantity < 0,
        active: logisticsId === activeId,
      })}
      onClick={() => onClick()}
    >
      <div className="panel-list">
        <a className="d-flex align-items-start">
          <div className="mr-3">
            <h6
              className={classNames({
                'text-30': deleteFlag,
              })}
            >
              <span className="material-icons font-size-16pt icon--left">
                {distributionHistoryTextKey.icon}
              </span>
              {deleteFlag && `[${t('text_delete')}] `}
              {t(distributionHistoryTextKey.textKey)}
              {updateId && <span className="badge-pich">{t('text_edit')}</span>}
            </h6>
            <div className="text-els">{productName}</div>
            <ul className="mb-2">
              <li>{moment(updateDate).format('YYYY-MM-DD HH:mm:ss')}</li>
              <li>{warehouseName}</li>
            </ul>
            <span className="writer">{updateId || registId}</span>
          </div>
          <div className="quantity">
            <h6>
              {logisticsQuantity > 0
                ? `+${logisticsQuantity}`
                : `${logisticsQuantity}`}
            </h6>
          </div>
        </a>
      </div>
    </div>
  );
}

export default PhysicalDistributionHistoryList;
