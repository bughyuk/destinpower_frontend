import React from 'react';
import { useTranslation } from 'react-i18next';
import { SignUpMemberProps } from '@/components/SignUpMember';
import { useHistory } from 'react-router-dom';

type SignUpInviteCompleteProps = SignUpMemberProps;

function SignUpInviteComplete({
  onChangeMemberStep,
}: SignUpInviteCompleteProps) {
  const { t } = useTranslation();
  const history = useHistory();

  return (
    <>
      <div className="msg-board">
        <h2>{t('text_sent_invitation_complete')}</h2>
        <img src="/static/images/icon_invitation.svg" alt="" />
        <em>
          {t('msg_sent_invitation')}
          <br />
          {t('msg_gen_project_with_user')}
        </em>
      </div>
      <div className="form-group text-center mb-32pt">
        <button
          className="btn btn-block btn-lg btn-accent"
          type="button"
          onClick={() => history.replace('/signin')}
        >
          {t('text_complete')}
        </button>
      </div>
      <div className="text-50 text-center">
        <a
          className="text-body text-underline"
          onClick={() => onChangeMemberStep(3)}
        >
          {t('text_send_more')}
        </a>
      </div>
    </>
  );
}

export default SignUpInviteComplete;
