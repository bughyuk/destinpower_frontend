import React from 'react';
import { useTranslation } from 'react-i18next';
import { FloatPhysicalDistributionHistoryCommonProps } from '@/components/FloatPhysicalDistributionHistory';
import moment from 'moment';
import { CommonUtils } from '@/utils';
import InputNumber from '@/components/InputNumber';
import { Warehouse } from '@/modules/physical_distribution/types';
import PhysicalDistributionFlatpickr from '@/components/PhysicalDistributionFlatpickr';

type FloatPhysicalDistributionHistoryMovementProps = FloatPhysicalDistributionHistoryCommonProps;

function FloatPhysicalDistributionHistoryMovement({
  distributionHistory,
}: FloatPhysicalDistributionHistoryMovementProps) {
  const { t } = useTranslation();

  return (
    <>
      <div className="page-separator mt-5">
        <div className="page-separator__text">
          {t('text_movement_information')}
        </div>
      </div>
      <table className="table mb-4 thead-bg-light">
        <colgroup>
          <col width="*" />
          <col width="15%" />
        </colgroup>
        <thead>
          <tr>
            <th>{t('text_product_information')}</th>
            <th>{t('text_movement_box_quantity')}</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div className="flex d-flex align-items-center">
                <a className="avatar mr-12pt blank-img">
                  <img
                    src={distributionHistory.imgUrl}
                    className="avatar-img rounded"
                  />
                </a>
                <div className="flex list-els">
                  <h6 className="m-0">{distributionHistory.productName}</h6>
                  <div className="card-subtitle text-50">
                    <small className="mr-2">
                      {distributionHistory.productSizeName}
                    </small>
                    <small className="mr-2">
                      {CommonUtils.getProductStandard(
                        distributionHistory.productWeight,
                        distributionHistory.boxQuantity
                      )}
                    </small>
                  </div>
                </div>
              </div>
            </td>
            <td>
              <div className="d-inline-flex align-items-center">
                <span className="font-weight-bolder">
                  {distributionHistory.logisticsQuantity}
                </span>
                <span className="ml-2 text-50">{t('text_box_en')}</span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}

export default FloatPhysicalDistributionHistoryMovement;
