import React from 'react';
import ModalHeader from 'react-bootstrap/ModalHeader';
import { Modal, ModalBody, ModalFooter, ModalTitle } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import { ModalAlertProps } from '@/modules/common';

function AlertModal({ show, children, onHide }: ModalAlertProps) {
  const { t } = useTranslation();

  return (
    <Modal
      show={show}
      onHide={() => {
        //
      }}
      centered={true}
    >
      <ModalHeader>
        <ModalTitle as={'h5'}>{t('text_notice')}</ModalTitle>
        <button type="button" className="close custom-close" onClick={onHide}>
          <span>
            <MaterialIcon name={'clear'} />
          </span>
        </button>
      </ModalHeader>
      <ModalBody>{children}</ModalBody>
      <ModalFooter>
        <button type="button" className="btn btn-accent" onClick={onHide}>
          {t('text_confirm')}
        </button>
      </ModalFooter>
    </Modal>
  );
}

export default AlertModal;
