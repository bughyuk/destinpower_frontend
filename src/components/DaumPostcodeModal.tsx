import ModalHeader from 'react-bootstrap/ModalHeader';
import MaterialIcon from '@/components/MaterialIcon';
import DaumPostcode, { AddressData } from 'react-daum-postcode';
import { Modal } from 'react-bootstrap';
import React from 'react';
import { LngLat } from '@/modules/map/types';
import { fetchGoogleLocation } from '@/api/common';

type DaumPostcodeModalCompleteData = {
  address: string;
  extra: string;
  lngLat: LngLat | null;
};

type DaumPostcodeModalProps = {
  show: boolean;
  onHide: () => void;
  onComplete: (data: DaumPostcodeModalCompleteData) => void;
};

function DaumPostcodeModal({
  show,
  onHide,
  onComplete,
}: DaumPostcodeModalProps) {
  const handleCompletePostcode = async (data: AddressData) => {
    let fullAddress = data.address;
    let extraAddress = '';

    if (data.addressType === 'R') {
      if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
        extraAddress += data.bname;
      }
      if (data.buildingName !== '') {
        extraAddress +=
          extraAddress !== '' ? `, ${data.buildingName}` : data.buildingName;
      }
      extraAddress = extraAddress !== '' ? `(${extraAddress})` : '';
      fullAddress += ` ${extraAddress}`;
    }

    onHide();
    const lngLat = await fetchGoogleLocation(fullAddress);
    onComplete({
      address: data.address,
      extra: extraAddress,
      lngLat,
    });
  };

  return (
    <Modal
      show={show}
      onHide={() => {
        //
      }}
      centered={true}
    >
      <ModalHeader>
        <button type="button" className="close custom-close" onClick={onHide}>
          <span>
            <MaterialIcon name={'clear'} />
          </span>
        </button>
      </ModalHeader>
      <DaumPostcode onComplete={handleCompletePostcode} />
    </Modal>
  );
}

export default DaumPostcodeModal;
