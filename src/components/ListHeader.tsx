import React from 'react';
import { useTranslation } from 'react-i18next';

type ListHeaderProps = {
  titleKey: string;
  descriptionKey?: string;
  createBtnTextKey?: string;
  onCreateBtnClick?: () => void;
};

function ListHeader(props: ListHeaderProps) {
  const { t } = useTranslation();

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{t(props.titleKey)}</h3>
          {props.descriptionKey && (
            <p className="text-70 mb-0">{t(props.descriptionKey)}</p>
          )}
        </div>
      </div>
      {props.createBtnTextKey && (
        <a
          onClick={() => {
            props.onCreateBtnClick?.call(null);
          }}
          className="btn btn-outline-dark"
        >
          {t(props.createBtnTextKey)}
        </a>
      )}
    </div>
  );
}

export default ListHeader;
