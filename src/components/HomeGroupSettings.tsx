import React from 'react';
import { useHomeMenu } from '@/modules/home/hook';
import {
  MENU_GROUP_SETTINGS_GROUP,
  MENU_GROUP_SETTINGS_USER,
} from '@/modules/home/types';
import GroupManagement from '@/components/GroupManagement';
import UserManagement from '@/components/UserManagement';

function HomeGroupSettings() {
  const { activeMenuIdx } = useHomeMenu();

  return (
    <>
      {activeMenuIdx === MENU_GROUP_SETTINGS_USER && <UserManagement />}
      {activeMenuIdx === MENU_GROUP_SETTINGS_GROUP && <GroupManagement />}
    </>
  );
}

export default HomeGroupSettings;
