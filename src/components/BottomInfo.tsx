import React, { ReactNode } from 'react';

type BottomInfoProps = {
  children?: ReactNode;
};

function BottomInfo({ children }: BottomInfoProps) {
  return <div className="info-holder">{children}</div>;
}

export default BottomInfo;
