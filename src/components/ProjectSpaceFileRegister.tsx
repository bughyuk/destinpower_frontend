import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormRow from '@/components/FormRow';
import FormLabel from '@/components/FormLabel';
import MaterialIcon from '@/components/MaterialIcon';
import ListDropdown from '@/components/ListDropdown';
import { useSpace, useSpaceRegister } from '@/modules/space/hook';
import {
  fetchFloors,
  postDeleteFloorPlan,
  postFloorsFile,
  postFloorsInfo,
  postUpdateFloorPlan,
  postUpdateFloors,
  ResponseFloorsInfo,
} from '@/api/space';
import { useUser } from '@/modules/user/hook';
import { CommonUtils } from '@/utils';
import classNames from 'classnames';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  SPACE_FILE_EDIT_MODE,
  SPACE_FILE_REGISTER_MODE,
} from '@/utils/constants/common';
import { Config } from '@/config';
import FormGroup from '@/components/FormGroup';
import {
  FloorsInfoInitialState,
  UploadFloorsInfo,
} from '@/modules/space/types';
import { Feature } from 'ol';
import { useOpenLayers, useProjectRegister } from '@/modules/project/hook';
import { putProjectStepData } from '@/api/project';
import { Coordinate } from 'ol/coordinate';
import InvalidAlert from '@/components/InvalidAlert';

let squareFeature: Feature | null;
function ProjectSpaceFileRegister() {
  const { handleChangeFileMode } = useSpaceRegister();
  const { user } = useUser();
  const { info, handleSetFloorsInfo } = useSpace();
  const { map, geoImage, draw, drawSource, handleAllClear } = useOpenLayers();
  const [floorsList, setFloorsList] = useState<ResponseFloorsInfo[]>([]);

  const handleFetchFloorsList = async () => {
    const data = await fetchFloors(user.userId, info.space.id);
    if (data) {
      setFloorsList(data);
    }
  };

  useEffect(() => {
    handleFetchFloorsList();

    return () => {
      drawSource?.clear();
      const drawSquare = draw.square;
      if (drawSquare) {
        map?.removeInteraction(drawSquare);
      }
      handleChangeFileMode(SPACE_FILE_REGISTER_MODE);
      handleSetFloorsInfo(FloorsInfoInitialState);
      handleAllClear();
    };
  }, []);

  useEffect(() => {
    if (map) {
      const view = map.getView();
      if (view) {
        const location = info.space.location;
        if (location && location.lng && location.lat) {
          view.setZoom(13);
          view.setCenter([location.lng, location.lat]);
        }
      }
    }
  }, [map]);

  const handleSubmit = async () => {
    const layer = geoImage.layer;
    if (layer) {
      const source = layer.getSource();
      const center = source.getCenter();
      const rotation = source.getRotation();
      const scale = source.getScale();

      await postUpdateFloorPlan({
        userid: user.userId,
        mapid: info.floors.id,
        mapname: info.floors.name,
        floor: info.floors.value,
        cx: center[0],
        cy: center[1],
        imgRotate: rotation,
        imgScalex: scale[0],
        imgScaley: scale[1],
      });

      handleFetchFloorsList();
    }
  };

  return (
    <>
      <Header />
      <Body floorsList={floorsList} onFetchFloorsList={handleFetchFloorsList} />
      <Footer onSubmit={handleSubmit} floorsLength={floorsList.length} />
    </>
  );
}

function Header() {
  const { t } = useTranslation();
  const { mode } = useSpaceRegister();

  let title = '';
  if (mode === SPACE_FILE_REGISTER_MODE) {
    title = t('text_floor_plan_registration');
  } else if (mode === SPACE_FILE_EDIT_MODE) {
    title = t('text_floor_plan_edit');
  }

  return (
    <div className="container-fluid d-flex py-4 mb-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{title}</h3>
        </div>
      </div>
    </div>
  );
}

type BodyProps = {
  floorsList: ResponseFloorsInfo[];
  onFetchFloorsList: () => void;
};

function Body({ floorsList, onFetchFloorsList }: BodyProps) {
  return (
    <>
      <Input onRefreshFloorsList={onFetchFloorsList} />
      <List onRefreshFloorsList={onFetchFloorsList} floorsList={floorsList} />
    </>
  );
}

type InputProps = {
  onRefreshFloorsList: () => void;
};

function Input({ onRefreshFloorsList }: InputProps) {
  const { t } = useTranslation();
  const { mode, handleChangeFileMode } = useSpaceRegister();
  const { info, handleSetFloorsInfo } = useSpace();
  const { user } = useUser();
  const { map, draw, drawSource, geoImage } = useOpenLayers();
  const fileInput = useRef<HTMLInputElement>(null);
  const [showFileUpload, setShowFileUpload] = useState(true);
  const [fileDeleteFlag, setFileDeleteFlag] = useState(false);

  const [floorPlanInfo, setFloorPlanInfo] = useState<{
    name: string;
    floors: number;
    file: File | null;
  }>({
    name: '',
    floors: 1,
    file: null,
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [isDrawSquare, setDrawSquare] = useState(true);

  useEffect(() => {
    if (floorPlanInfo.name && floorPlanInfo.floors) {
      setShowInvalidMessage(false);
    }
  }, [floorPlanInfo]);

  useEffect(() => {
    if (fileInput.current) {
      fileInput.current.value = '';
    }
    setShowFileUpload(true);
    setFileDeleteFlag(false);
    const floors = info.floors;

    if (mode === SPACE_FILE_EDIT_MODE && floors.id) {
      setFloorPlanInfo({
        name: floors.name,
        floors: floors.value,
        file: null,
      });

      if (floors.oriFileName) {
        setShowFileUpload(false);
      }
    } else {
      setFloorPlanInfo({
        name: '',
        floors: 1,
        file: null,
      });
    }
  }, [info]);

  const handleSubmit = async () => {
    if (floorPlanInfo.name && floorPlanInfo.floors) {
      const floors = info.floors;
      const center = map?.getView().getCenter();
      if (mode === SPACE_FILE_EDIT_MODE && floors.id) {
        if (floorPlanInfo.name && floorPlanInfo.floors && !floorPlanInfo.file) {
          await postUpdateFloors({
            metaid: info.space.id,
            floor: floorPlanInfo.floors,
            mapname: floorPlanInfo.name,
            userid: user.userId,
            mapid: floors.id,
            cx: info.floors.cx,
            cy: info.floors.cy,
            imgRotate: info.floors.rotation,
            imgScalex: info.floors.scalex,
            imgScaley: info.floors.scaley,
            filename: fileDeleteFlag ? '' : info.floors.fileName,
            originalname: fileDeleteFlag ? '' : info.floors.oriFileName,
          });
        } else {
          if (center) {
            if (!squareFeature) {
              setDrawSquare(false);
              setShowInvalidMessage(true);
              return;
            }

            const uploadFloorsInfo = await postFloorsFile({
              metaid: info.space.id,
              floor: floorPlanInfo.floors,
              mapid: floors.id,
              mapname: floorPlanInfo.name,
              userid: user.userId,
              file: floorPlanInfo.file,
              cx: center[0],
              cy: center[1],
              imgRotate: 0,
              imgScalex: 1,
              imgScaley: 1,
              div: 'update',
            });

            handleFitFloorPlanFromSquare(uploadFloorsInfo, center);
          }
        }
      } else {
        if (floorPlanInfo.name && floorPlanInfo.floors && !floorPlanInfo.file) {
          await postFloorsInfo({
            metaid: info.space.id,
            floor: floorPlanInfo.floors,
            mapname: floorPlanInfo.name,
            userid: user.userId,
          });
        } else if (
          floorPlanInfo.name &&
          floorPlanInfo.floors &&
          floorPlanInfo.file
        ) {
          if (!squareFeature) {
            setDrawSquare(false);
            setShowInvalidMessage(true);
            return;
          }

          if (center) {
            const uploadFloorsInfo = await postFloorsFile({
              metaid: info.space.id,
              floor: floorPlanInfo.floors,
              mapname: floorPlanInfo.name,
              userid: user.userId,
              file: floorPlanInfo.file,
              cx: center[0],
              cy: center[1],
              imgRotate: 0,
              imgScalex: 1,
              imgScaley: 1,
              div: 'insert',
            });

            handleFitFloorPlanFromSquare(uploadFloorsInfo, center);
          }
        }
      }

      onRefreshFloorsList();
      setFloorPlanInfo({
        name: '',
        floors: 1,
        file: null,
      });
      handleSetFloorsInfo(FloorsInfoInitialState);
      handleChangeFileMode(SPACE_FILE_REGISTER_MODE);

      if (fileInput.current) {
        fileInput.current.value = '';
      }

      if (squareFeature) {
        setDrawSquare(true);
        drawSource?.removeFeature(squareFeature);
        squareFeature = null;
      }
    } else {
      setShowInvalidMessage(true);
    }
  };

  let showFileDel = false;
  if (
    fileInput.current &&
    fileInput.current.files &&
    fileInput.current.files.length
  ) {
    showFileDel = true;
  }

  const handleFitFloorPlanFromSquare = (
    uploadFloorsInfo: UploadFloorsInfo | null,
    center: Coordinate
  ) => {
    if (uploadFloorsInfo && squareFeature) {
      geoImage.drawFromSquare(
        {
          url: `${Config.space_api.uri}/bprint/${uploadFloorsInfo.filename}`,
          imageCenter: [center[0], center[1]],
          imageScale: [1, 1],
          imageRotate: 0,
        },
        squareFeature
      );

      setTimeout(() => {
        geoImage.remove();
        const layer = geoImage.layer;
        if (layer && uploadFloorsInfo) {
          const source = layer.getSource();
          const center = source.getCenter();
          const rotation = source.getRotation();
          const scale = source.getScale();

          postUpdateFloorPlan({
            userid: user.userId,
            mapid: uploadFloorsInfo.map_id,
            mapname: floorPlanInfo.name,
            floor: floorPlanInfo.floors,
            cx: center[0],
            cy: center[1],
            imgRotate: rotation,
            imgScalex: scale[0],
            imgScaley: scale[1],
          }).then(() => {
            onRefreshFloorsList();
          });
        }
      }, 150);
    }
  };

  return (
    <div className="container-fluid">
      <FormRow>
        <div className="col-md-9">
          <FormLabel textKey={'text_floor_plan_name'} essential={true} />
          <input
            type="text"
            className="form-line"
            placeholder={t('place_holder_floor_plan_name')}
            value={floorPlanInfo.name}
            onChange={(e) => {
              setFloorPlanInfo({
                ...floorPlanInfo,
                name: e.target.value,
              });
            }}
          />
        </div>
        <div className="col-md-3">
          <FormLabel textKey={'text_floor'} essential={true} />
          <input
            type="number"
            className="form-line"
            value={floorPlanInfo.floors}
            onKeyDown={(e) => {
              const key = e.key;
              if (key === '.' || key === '0') {
                e.preventDefault();
              }
            }}
            onChange={(e) => {
              let value = e.target.value;
              if (value.match(/\./)) {
                value = value.replace(/./g, '');
              }
              let number = 1;
              if (value) {
                number = Number(value);
                if (!number) {
                  if (floorPlanInfo.floors > 0) {
                    number = -1;
                  } else {
                    number = 1;
                  }
                }
              }

              setFloorPlanInfo({
                ...floorPlanInfo,
                floors: number,
              });
            }}
          />
        </div>
      </FormRow>
      {showFileUpload && (
        <FormGroup>
          <FormLabel textKey={'text_file_upload'} htmlFor={'formFile'} />
          <input
            className="form-control pr-5"
            type="file"
            accept={'image/*'}
            ref={fileInput}
            onChange={(e) => {
              let file = null;
              const drawSquare = draw.square;
              if (e.target.files && e.target.files.length) {
                file = e.target.files[0];
                if (drawSquare) {
                  map?.addInteraction(drawSquare);
                  drawSquare.once('drawend', (e) => {
                    const feature = e.feature;
                    squareFeature = feature;
                    setDrawSquare(true);
                    setShowInvalidMessage(false);
                    map?.removeInteraction(drawSquare);
                  });
                }
              } else {
                if (drawSquare) {
                  drawSource?.clear();
                  squareFeature = null;
                  map?.removeInteraction(drawSquare);
                }
              }
              setFloorPlanInfo({
                ...floorPlanInfo,
                file,
              });
            }}
          />
          {showFileDel && (
            <a
              className="file-del"
              onClick={() => {
                const drawSquare = draw.square;
                if (drawSquare) {
                  drawSource?.clear();
                  squareFeature = null;
                  map?.removeInteraction(drawSquare);
                }
                if (fileInput.current) {
                  fileInput.current.value = '';
                }
                setFloorPlanInfo({
                  ...floorPlanInfo,
                  file: null,
                });
              }}
            >
              <MaterialIcon name={'close'} />
            </a>
          )}
        </FormGroup>
      )}
      {!showFileUpload && (
        <FormRow className="align-items-center">
          <div className="col-auto">
            <div className="avatar">
              <img
                src={`${Config.space_api.uri}/bprint/${info.floors.fileName}`}
                className="avatar-img rounded"
              />
            </div>
          </div>
          <div className="col">
            <div className="font-weight-bold">{info.floors.oriFileName}</div>
          </div>
          <div className="col-auto">
            <a
              className="text-muted-light"
              onClick={() => {
                setFileDeleteFlag(true);
                setShowFileUpload(true);
              }}
            >
              <MaterialIcon name={'close'} />
            </a>
          </div>
        </FormRow>
      )}
      {showInvalidMessage && (
        <InvalidAlert
          messageKey={
            isDrawSquare
              ? 'msg_valid_enter_required_value'
              : 'msg_draw_square_to_place_the_floor_plan'
          }
        />
      )}
      <div className="my-16pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn-grad-col btn-grad-small ml-3"
            onClick={handleSubmit}
          >
            {mode === SPACE_FILE_EDIT_MODE && t('text_edit')}
            {mode === SPACE_FILE_REGISTER_MODE && t('text_add')}
          </a>
        </div>
      </div>
    </div>
  );
}

type ListProps = {
  floorsList: ResponseFloorsInfo[];
  onRefreshFloorsList: () => void;
};

function List({ floorsList, onRefreshFloorsList }: ListProps) {
  const { t } = useTranslation();
  const [selectedFloorsId, setSelectFloorsId] = useState('');

  return (
    <div className="container-fluid maps-item-list">
      {floorsList.length === 0 && (
        <>
          <h5>{t('text_list_of_registered_floor_plan')}</h5>
          <em className="none-list">{t('msg_floor_plan_suggest_to_add')}</em>
        </>
      )}
      {floorsList.length > 0 &&
        floorsList.map((floors) => (
          <ListItem
            key={floors.map_id}
            {...floors}
            selectedFloorsId={selectedFloorsId}
            setSelectFloorsId={setSelectFloorsId}
            onRefreshFloorsList={onRefreshFloorsList}
          />
        ))}
    </div>
  );
}

type ListItemProps = ResponseFloorsInfo & {
  selectedFloorsId: string;
  setSelectFloorsId: (floorsId: string) => void;
  onRefreshFloorsList: () => void;
};

function ListItem({
  map_name,
  map_id,
  map_floor,
  filename,
  orifilename,
  cx,
  cy,
  scalex,
  scaley,
  rotation,
  selectedFloorsId,
  setSelectFloorsId,
  onRefreshFloorsList,
}: ListItemProps) {
  const { t } = useTranslation();
  const { user } = useUser();
  const { info, handleSetFloorsInfo } = useSpace();
  const { handleChangeFileMode } = useSpaceRegister();
  const { geoImage, handleClearDrawSource } = useOpenLayers();

  const handleDeleteFloorsPlan = async () => {
    const result = await postDeleteFloorPlan({
      userid: user.userId,
      del: true,
      mapid: map_id,
    });

    if (result) {
      if (info.floors.id === map_id) {
        handleSetFloorsInfo(FloorsInfoInitialState);
        handleChangeFileMode(SPACE_FILE_REGISTER_MODE);
      }
      if (selectedFloorsId === map_id) {
        geoImage.remove();
      }

      onRefreshFloorsList();
    }
  };

  const handleOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        handleSetFloorsInfo({
          id: map_id,
          name: map_name,
          value: map_floor,
          fileName: filename,
          oriFileName: orifilename,
          cx: cx,
          cy: cy,
          scalex: scalex,
          scaley: scaley,
          rotation: rotation,
        });
        geoImage.remove();
        handleChangeFileMode(SPACE_FILE_EDIT_MODE);
        break;
      case FUNCTION_DELETE:
        handleDeleteFloorsPlan();
        break;
    }
  };

  const handleLoadFloorPlan = () => {
    setSelectFloorsId(map_id);
    handleClearDrawSource();

    handleSetFloorsInfo({
      id: map_id,
      name: map_name,
      value: map_floor,
      fileName: filename,
      oriFileName: orifilename,
      cx: cx,
      cy: cy,
      scalex: scalex,
      scaley: scaley,
      rotation: rotation,
    });

    if (filename) {
      if (squareFeature) {
        geoImage.drawFromSquare(
          {
            url: `${Config.space_api.uri}/bprint/${filename}`,
            imageCenter: [Number(cx), Number(cy)],
            imageScale: [Number(scalex), Number(scaley)],
            imageRotate: Number(rotation),
          },
          squareFeature
        );

        setTimeout(() => {
          const layer = geoImage.layer;
          if (layer) {
            const source = layer.getSource();
            const center = source.getCenter();
            const rotation = source.getRotation();
            const scale = source.getScale();

            postUpdateFloorPlan({
              userid: user.userId,
              mapid: map_id,
              mapname: map_name,
              floor: map_floor,
              cx: center[0],
              cy: center[1],
              imgRotate: rotation,
              imgScalex: scale[0],
              imgScaley: scale[1],
            }).then(() => {
              onRefreshFloorsList();
            });

            handleSetFloorsInfo({
              id: map_id,
              name: map_name,
              value: map_floor,
              fileName: filename,
              oriFileName: orifilename,
              cx: center[0],
              cy: center[1],
              scalex: scale[0],
              scaley: scale[1],
              rotation: rotation,
            });
          }
        }, 300);

        squareFeature = null;
      } else {
        geoImage.draw({
          url: `${Config.space_api.uri}/bprint/${filename}`,
          imageCenter: [Number(cx), Number(cy)],
          imageScale: [Number(scalex), Number(scaley)],
          imageRotate: Number(rotation),
        });
      }
    } else {
      geoImage.remove();
    }
  };

  return (
    <div
      className={classNames('maps-list', {
        active: selectedFloorsId === map_id,
      })}
      onClick={handleLoadFloorPlan}
    >
      <div className="card-body d-flex align-items-center">
        <div className="flex mr-12pt">
          <a className="card-title">{map_name}</a>
          <dl className="space-id">
            <dt>{t('text_floors_id')} :</dt> <dd>{map_id}</dd>
          </dl>
        </div>
        <div className="d-flex flex-column align-items-center">
          <span className="lead text-headings">
            {CommonUtils.convertFloorsFormat(map_floor)}
          </span>
        </div>
      </div>
      <div className="card-footer">
        <div className="d-flex align-items-center">
          <div className="flex mr-2 file-att">
            {filename && (
              <>
                <MaterialIcon
                  name={'attachment'}
                  className={'font-size-16pt'}
                />{' '}
                {orifilename}
              </>
            )}
          </div>
          <ListDropdown onSelect={handleOptions} />
        </div>
      </div>
    </div>
  );
}

type FooterProps = {
  onSubmit: () => void;
  floorsLength: number;
};

function Footer({ onSubmit, floorsLength }: FooterProps) {
  const { info } = useSpace();
  const { t } = useTranslation();
  const { handleAllClear } = useOpenLayers();
  const {
    projectInfo,
    projectProduceStep,
    handleChangeRegisterStep,
  } = useProjectRegister();
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (floorsLength) {
      setShowInvalidMessage(false);
    }
  }, [floorsLength]);

  const handleRouteIndoorEdit = async () => {
    if (floorsLength) {
      const projectId = projectInfo.projectId;
      if (projectId) {
        await putProjectStepData(projectId, {
          produceStepData: JSON.stringify({
            ...projectProduceStep,
            step: 5,
          }),
        });
      }
      handleChangeRegisterStep(5);
    } else {
      setShowInvalidMessage(true);
    }
  };

  return (
    <>
      {showInvalidMessage && (
        <InvalidAlert
          messageKey={'msg_valid_register_at_least_one_floor_plan'}
        />
      )}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className={classNames('btn btn-outline-secondary ml-0')}
            onClick={() => {
              handleAllClear();
              handleChangeRegisterStep(2);
            }}
          >
            {t('text_to_the_prev')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-2', {
              disabled: !info.floors.fileName,
            })}
            onClick={() => {
              if (info.floors.fileName) {
                onSubmit();
              }
            }}
          >
            {t('text_save')}
          </a>
          <a
            className={classNames('btn btn-outline-secondary ml-2')}
            onClick={handleRouteIndoorEdit}
          >
            {t('text_to_the_next')}
          </a>
        </div>
      </div>
    </>
  );
}

export default ProjectSpaceFileRegister;
