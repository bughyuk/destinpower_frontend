import React from 'react';
import moment from 'moment';
import { Badge } from 'react-bootstrap';

type DateBadgeProps = {
  date: string;
};

function DateBadge({ date }: DateBadgeProps) {
  return (
    <>
      {moment.duration(moment().diff(moment(date))).asDays() < 1 && (
        <Badge className={'badge-accent'}>N</Badge>
      )}
      {` ${moment(date).format('YYYY.MM.DD')}`}
    </>
  );
}

export default DateBadge;
