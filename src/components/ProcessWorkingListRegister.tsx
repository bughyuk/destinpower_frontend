import React, { useState, useEffect } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import { Korean } from 'flatpickr/dist/l10n/ko';
import Flatpickr from 'react-flatpickr';
import { postProcessWorkingAdd } from '@/api/process';
import classNames from 'classnames';
import { fetchProcessWorkingData } from '@/api/process';
import { Process } from '@/modules/physical_distribution/types';

type ProcessWorkingListRegisterProps = PaneProps & {
  process: Process;
};

function ProcessWorkingListRegister({
  onChangeStatus,
  process,
}: ProcessWorkingListRegisterProps) {
  const { t } = useTranslation();
  const [inputs, setInputs] = useState<{
    name: string;
    pname: string;
    sn: string;
    wtime: string;
    etime: string;
    fid: number;
    pcid: number;
    ptid: number; // 생산제품
    bid: number; // 거래처 아이디
  }>({
    name: '',
    pname: '',
    sn: '',
    wtime: '',
    etime: '',
    fid: -1,
    pcid: -1,
    ptid: -1,
    bid: -1,
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  const [factoryList, setFactoryList] = useState<
    { name: string; id: string }[]
  >([]);
  const [processCheckList, setProcessCheckList] = useState<
    { name: string; id: string }[]
  >([]);
  const [productTypeList, setProductTypeList] = useState<
    { name: string; id: string }[]
  >([]);
  const [buyerList, setBuyerList] = useState<{ name: string; id: string }[]>(
    []
  );

  useEffect(() => {
    // TODO: backend 요청
    setInputs({
      name: process.processName,
      pname: process.processPname,
      sn: process.serialNumber,
      wtime: process.workingTime,
      etime: process.endTime,
      fid: -1,
      pcid: -1,
      ptid: -1,
      bid: -1,
    });
  }, [process]);

  useEffect(() => {
    (async () => {
      const data = await fetchProcessWorkingData();
      setFactoryList(data.factory);
      setProcessCheckList(data.process_checklist);
      setProductTypeList(data.product_type);
      setBuyerList(data.buyer);
    })();
  }, []);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }

    console.log(inputs);

    setShowInvalidMessage(false);
    const isValid =
      inputs.name &&
      inputs.pname &&
      inputs.sn &&
      inputs.wtime &&
      inputs.etime &&
      inputs.fid &&
      inputs.pcid &&
      inputs.ptid &&
      inputs.bid;

    if (isValid) {
      setLoading(true);

      const result = await postProcessWorkingAdd(inputs);

      if (result) {
        onChangeStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel
          textKey={'text_project_name'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="name"
          value={inputs.name}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'공장리스트'} essential={true} />
        <select
          className="form-line"
          onChange={(e) => {
            console.log('fid ', Number(e.target.value));
            setInputs((prev) => ({
              ...prev,
              fid: Number(e.target.value),
            }));
          }}
        >
          <option value="">공장을 지정해 주세요.</option>
          {factoryList?.length &&
            factoryList.map((list, i) => (
              <option key={i} value={list.id}>
                {list.name}
              </option>
            ))}
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_product_title'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="pname"
          value={inputs.pname}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={t('text_serial_number')}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="sn"
          value={inputs.sn}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'생산제품'} essential={true} />
        <select
          className="form-line"
          onChange={(e) => {
            console.log('ptid ', Number(e.target.value));
            setInputs((prev) => ({
              ...prev,
              ptid: Number(e.target.value),
            }));
          }}
        >
          <option value="">생산제품을 선택해 주세요.</option>
          {productTypeList?.length &&
            productTypeList.map((list, i) => (
              <option key={i} value={list.id}>
                {list.name}
              </option>
            ))}
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'공정리스트 선택'} essential={true} />
        <select
          className="form-line"
          onChange={(e) => {
            console.log('pcid ', Number(e.target.value));
            setInputs((prev) => ({
              ...prev,
              pcid: Number(e.target.value),
            }));
          }}
        >
          <option value="">공정리스트를 선택해 주세요.</option>
          {processCheckList?.length &&
            processCheckList.map((list, i) => (
              <option key={i} value={list.id}>
                {list.name}
              </option>
            ))}
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'공정시작일'} essential={true} />
        <Flatpickr
          onChange={(selectedDates, dateStr) => {
            setInputs((prev) => ({
              ...prev,
              wtime: dateStr,
            }));
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'기한'} essential={true} />
        <Flatpickr
          onChange={(selectedDates, dateStr) => {
            setInputs((prev) => ({
              ...prev,
              etime: dateStr,
            }));
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'거래처'} essential={true} />
        <select
          className="form-line"
          onChange={(e) => {
            console.log('bid ', Number(e.target.value));
            setInputs((prev) => ({
              ...prev,
              bid: Number(e.target.value),
            }));
          }}
        >
          <option value="">거래처를 선택해 주세요.</option>
          {buyerList?.length &&
            buyerList.map((list, i) => (
              <option key={i} value={list.id}>
                {list.name}
              </option>
            ))}
        </select>
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => onChangeStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_registration')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default ProcessWorkingListRegister;
