import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import React, { useEffect, useState } from 'react';
import {
  useControlAccident,
  useControlArea,
  useControlProject,
  useRealtimeUser,
} from '@/modules/map/hook';
import * as turf from '@turf/turf';
import { Polygon } from 'ol/geom';

function FilterRealtime() {
  const { t } = useTranslation();
  const { summary } = useControlProject();
  const { users } = useRealtimeUser();
  const { selectedAreaFeature } = useControlArea();
  const [areaUserCount, setAreaUserCount] = useState(0);

  useEffect(() => {
    setAreaUserCount(0);
    const geometry = selectedAreaFeature?.getGeometry();
    if (geometry) {
      const polygonGeometry = geometry as Polygon;
      const polygon = turf.polygon(polygonGeometry.getCoordinates());

      let userCount = 0;

      users.forEach((user) => {
        const userPoint = turf.point([user.lng, user.lat]);
        if (turf.inside(userPoint, polygon)) {
          userCount++;
        }
      });

      setAreaUserCount(userCount);
    }
  }, [users, selectedAreaFeature]);

  return (
    <div className="tabBox mapStauts02 status-con active">
      <span className="d-flex align-items-center mr-4 pl-3">
        <span className="avatar mr-12pt">
          <span className="avatar-title navbar-avatar">
            <MaterialIcon name={'trending_up'} />
          </span>
        </span>
        <small className="flex d-flex flex-column">
          <span className="navbar-text-50">
            {t('text_total_number_of_person')}
          </span>
          <strong className="navbar-text-100">
            {summary.userCount + t('text_unit_of_person')}
            {/*<VariationArrow value={0} unit={t('text_unit_of_person')} />*/}
          </strong>
        </small>
      </span>
      {selectedAreaFeature && (
        <span className="d-flex align-items-center mr-4">
          <span className="avatar mr-12pt">
            <span className="avatar-title navbar-avatar">
              <MaterialIcon name={'trending_up'} />
            </span>
          </span>
          <small className="flex d-flex flex-column">
            <span className="navbar-text-50">
              {t('text_number_of_person_by_area')}
            </span>
            <strong className="navbar-text-100">
              {areaUserCount} {t('text_unit_of_person')}
              {/*<VariationArrow value={0} unit={t('text_unit_of_person')} />*/}
            </strong>
          </small>
        </span>
      )}
      <span className="d-flex align-items-center mr-4">
        <span className="avatar mr-12pt">
          <span className="avatar-title navbar-avatar">
            <MaterialIcon name={'notification_important'} />
          </span>
        </span>
        <small className="flex d-flex flex-column">
          <span className="navbar-text-50">{t('text_number_of_accident')}</span>
          <strong className="navbar-text-100">
            {summary.accidentCount + t('text_case')}
          </strong>
        </small>
      </span>
      {/*<div className="filter-btn-cell d-flex align-items-center">*/}
      {/*  <a className="">*/}
      {/*    <MaterialIcon name={'autorenew'} className={'mr-1'} />*/}
      {/*    {t('text_refresh')}*/}
      {/*  </a>*/}
      {/*</div>*/}
    </div>
  );
}

export default FilterRealtime;
