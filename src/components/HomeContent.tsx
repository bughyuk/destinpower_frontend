import React from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useHomeMenu } from '@/modules/home/hook';
import {
  MENU_ACCOUNT_PROFILE,
  MENU_ACCOUNT_SECURITY,
  MENU_GROUP_SETTINGS_GROUP,
  MENU_GROUP_SETTINGS_USER,
  MENU_PROJECT,
  MENU_SPACE_CUSTOM,
  MENU_SPACE_PUBLIC,
} from '@/modules/home/types';
import HomeSpace from '@/components/HomeSpace';
import HomeProject from '@/components/HomeProject';
import HomeGroupSettings from '@/components/HomeGroupSettings';
import HomeLocationbar from '@/components/HomeLocationbar';
import HomeAccount from '@/components/HomeAccount';

function HomeContent() {
  const { activeMenuIdx } = useHomeMenu();

  let isSpace = false;
  if (
    activeMenuIdx === MENU_SPACE_CUSTOM ||
    activeMenuIdx === MENU_SPACE_PUBLIC
  ) {
    isSpace = true;
  }

  let isGroupSettings = false;
  if (
    activeMenuIdx === MENU_GROUP_SETTINGS_USER ||
    activeMenuIdx === MENU_GROUP_SETTINGS_GROUP
  ) {
    isGroupSettings = true;
  }

  let isAccount = false;
  if (
    activeMenuIdx === MENU_ACCOUNT_PROFILE ||
    activeMenuIdx === MENU_ACCOUNT_SECURITY
  ) {
    isAccount = true;
  }

  return (
    <PerfectScrollbar className={'contents'}>
      <HomeLocationbar />
      <>
        {activeMenuIdx === MENU_PROJECT && <HomeProject />}
        {isSpace && <HomeSpace />}
        {isAccount && <HomeAccount />}
        {isGroupSettings && <HomeGroupSettings />}
      </>
    </PerfectScrollbar>
  );
}

export default HomeContent;
