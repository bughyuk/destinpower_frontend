import React, { useEffect, useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import { Warehouse } from '@/modules/physical_distribution/types';
import classNames from 'classnames';
import { putWarehouse } from '@/api/physical_distribution';
import InputNumber from '@/components/InputNumber';

type PhysicalDistributionWarehouseEditProps = PaneProps & {
  warehouse: Warehouse;
};

function PhysicalDistributionWarehouseEdit({
  warehouse,
  onChangeStatus,
}: PhysicalDistributionWarehouseEditProps) {
  const { t } = useTranslation();
  const [inputs, setInputs] = useState<{
    warehouseTitle: string;
    acceptableQuantity: number;
    memo: string;
  }>({
    warehouseTitle: warehouse.warehouseName,
    acceptableQuantity: warehouse.maxQuantity,
    memo: warehouse.note,
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!warehouse.warehouseId) {
      onChangeStatus(PANE_STATUS_LIST);
    }
  }, [warehouse]);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid = inputs.warehouseTitle;

    if (isValid) {
      setLoading(true);
      const result = await putWarehouse(warehouse.warehouseId, {
        warehouseName: inputs.warehouseTitle,
        maxQuantity: inputs.acceptableQuantity,
        note: inputs.memo,
      });

      if (result) {
        onChangeStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel
          textKey={'text_warehouse_title'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="warehouseTitle"
          placeholder={t('place_holder_warehouse_title')}
          value={inputs.warehouseTitle}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_acceptable_quantity'} className={'mb-0'} />
        <InputNumber
          className={'form-line'}
          value={inputs.acceptableQuantity}
          onChange={(value) =>
            setInputs({
              ...inputs,
              acceptableQuantity: value,
            })
          }
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_memo'} />
        <input
          type="text"
          className="form-line"
          id="memo"
          placeholder={t('place_holder_memo')}
          value={inputs.memo}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => onChangeStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_do_edit')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default PhysicalDistributionWarehouseEdit;
