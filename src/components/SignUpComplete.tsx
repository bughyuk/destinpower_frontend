import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { SignUpMemberProps } from '@/components/SignUpMember';

function SignUpComplete() {
  const { t } = useTranslation();
  const history = useHistory();

  return (
    <>
      <div className="msg-board">
        <h2>{t('text_sign_up_completed')}</h2>
        <em>{t('msg_sign_up_completed')}</em>
      </div>
      <div className="form-group text-center mb-32pt">
        <button
          className="btn btn-block btn-lg btn-accent"
          type="button"
          onClick={() => history.replace('/signin')}
        >
          {t('text_to_sign_in')}
        </button>
      </div>
    </>
  );
}

export default SignUpComplete;
