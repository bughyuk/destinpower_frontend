import React, { useCallback, useEffect, useRef, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import ReactQuill from 'react-quill';
import MaterialIcon from '@/components/MaterialIcon';
import {
  GEOMETRY_TYPE_CIRCLE,
  GEOMETRY_TYPE_POLYGON,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import CompletePane from '@/components/CompletePane';
import { useLeftPaneContainer } from '@/modules/setup/hook';
import { CommonUtils } from '@/utils';
import FormLabel from '@/components/FormLabel';
import { useTranslation } from 'react-i18next';
import FormGroup from '@/components/FormGroup';
import classNames from 'classnames';
import {
  Accident,
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
  Message,
  MessageHistory,
} from '@/modules/accident/types';
import { fetchAccident, putAccidentInactive } from '@/api/accident';
import moment from 'moment';
import { Feature } from 'ol';
import { Circle, Point, Polygon } from 'ol/geom';
import { WKT } from 'ol/format';
import {
  useControlProject,
  useOpenLayers,
  useRealtimeUser,
} from '@/modules/map/hook';
import { fetchMessages, postMessages } from '@/api/message';
import GeometryType from 'ol/geom/GeometryType';
import * as turf from '@turf/turf';
import { RealtimeUser } from '@/modules/map/types';
import { Icon, Style } from 'ol/style';
import { showWarningMessage } from '@/utils/alertifyjs-bridge';
import { htmlToText } from 'html-to-text';
import { useAccidentMessage } from '@/modules/accident/hook';
import { postImage } from '@/api/common';
import InvalidAlert from '@/components/InvalidAlert';

type AccidentDetailProps = {
  eventId: string;
  onStatusChange: (status: PaneStatus) => void;
};

const wkt = new WKT();
const checkMap = new Map<string, boolean>();
function AccidentDetail({ eventId, onStatusChange }: AccidentDetailProps) {
  const { t } = useTranslation();
  const [done, setDone] = useState(false);
  const [isValid, setValid] = useState(false);
  const { draw } = useOpenLayers();
  const { users } = useRealtimeUser();
  const { project } = useControlProject();
  const { updateScroll } = useLeftPaneContainer();
  const [message, setMessage] = useState<Message>({
    title: '',
    content: '',
    linkUrl: '',
    imgId: '',
  });
  const [accident, setAccident] = useState({} as Accident);
  const { handleGetAccidentMessage } = useAccidentMessage();
  const [occupantUserList, setOccupantUserList] = useState<RealtimeUser[]>([]);
  const [feature, setFeature] = useState<Feature>();

  useEffect(() => {
    return () => {
      draw.source?.clear();
      checkMap.clear();
    };
  }, []);

  const handleSendMessage = async () => {
    const targetIds = occupantUserList.map(
      (occupantUser) => occupantUser.accessKey
    );
    const data = await postMessages({
      projectId: project.id,
      parentId: eventId,
      parentType: 'message',
      messageTitle: message.title,
      messageContent: htmlToText(message.content, {
        wordwrap: 130,
      }),
      linkUrl: message.linkUrl,
      imgId: message.imgId,
      targetIds,
    });
    if (data) {
      setDone(true);
    }
  };

  useEffect(() => {
    if (message.title && message.content && occupantUserList.length > 0) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [message]);

  useEffect(() => {
    updateScroll();
  }, [done]);

  const handleFetchAccident = async () => {
    const data = await fetchAccident(eventId);

    if (data) {
      setAccident(data);

      const { title, content } = handleGetAccidentMessage(
        data.eventDetailCategory
      );

      setMessage({
        ...message,
        title,
      });

      setMessage({
        ...message,
        content,
      });

      let iconCenter: number[] = [];
      let geometryFeature: Feature | null = null;
      if (data.geomType === GEOMETRY_TYPE_POLYGON) {
        if (data.geom) {
          geometryFeature = wkt.readFeature(data.geom);
          const polygonGeometry = geometryFeature.getGeometry() as Polygon;
          const centroid = turf.centroid(
            turf.polygon(polygonGeometry.getCoordinates())
          );
          iconCenter = centroid.geometry.coordinates;
        }
      } else if (data.geomType === GEOMETRY_TYPE_CIRCLE) {
        if (data.lng && data.lat && data.radius) {
          iconCenter = [data.lng, data.lat];
          geometryFeature = new Feature(new Circle(iconCenter, data.radius));
        }
      } else if (data.areaId) {
        if (data.areaGeom) {
          geometryFeature = wkt.readFeature(data.areaGeom);
          const polygonGeometry = geometryFeature.getGeometry() as Polygon;
          const centroid = turf.centroid(
            turf.polygon(polygonGeometry.getCoordinates())
          );
          iconCenter = centroid.geometry.coordinates;
        }
      }

      if (geometryFeature) {
        setFeature(geometryFeature);
        draw.source?.addFeature(geometryFeature);
      }

      const accidentIconName = CommonUtils.getAccidentIconName(
        data.eventDetailCategory
      );
      const icon = new Feature(new Point(iconCenter));
      const iconStyle = new Style({
        image: new Icon({
          src: `/static/images/${accidentIconName}`,
          scale: 2,
        }),
      });
      icon.setStyle(iconStyle);
      draw.source?.addFeature(icon);
    }
  };

  useEffect(() => {
    if (feature && accident.activeFlag) {
      const geometry = feature.getGeometry();
      if (geometry) {
        const list: RealtimeUser[] = [];
        switch (geometry.getType()) {
          case GeometryType.POLYGON:
            const polygonGeometry = geometry as Polygon;
            const polygon = turf.polygon(polygonGeometry.getCoordinates());

            users.forEach((user) => {
              const userPoint = turf.point([user.lng, user.lat]);
              if (turf.inside(userPoint, polygon)) {
                list.push(user);
                if (!checkMap.get(user.userId)) {
                  showWarningMessage(
                    `${user.userId} ${t('msg_user_detected_accident_area')}`
                  );
                  checkMap.set(user.userId, true);
                }
              }
            });

            break;
          case GeometryType.CIRCLE:
            const circleGeometry = geometry as Circle;
            const center = circleGeometry.getCenter();
            const radius = circleGeometry.getRadius();

            const accidentPoint = turf.toWgs84(turf.point(center));

            users.forEach((user) => {
              const userPoint = turf.toWgs84(turf.point([user.lng, user.lat]));

              const buffer = turf.buffer(accidentPoint, radius, {
                units: 'meters',
              });
              const contain = turf.booleanContains(buffer, userPoint);

              if (contain) {
                list.push(user);
                if (!checkMap.get(user.userId)) {
                  showWarningMessage(
                    `${user.userId} ${t('msg_user_detected_accident_area')}`
                  );
                  checkMap.set(user.userId, true);
                }
              }
            });
            break;
        }

        setOccupantUserList(list);

        if (list.length > 0) {
          setValid(true);
        }
      }
    }
  }, [users, feature]);

  useEffect(() => {
    draw.source?.clear();
    handleFetchAccident();
  }, [eventId]);

  if (done) {
    return (
      <CompletePane
        completeTextKey={'msg_send_message_complete'}
        backBtnTextKey={'msg_redirect_list'}
        onBackBtnClick={() => {
          onStatusChange(PANE_STATUS_LIST);
        }}
      />
    );
  }

  if (CommonUtils.isEmptyObject(accident)) {
    return <></>;
  }

  return (
    <>
      <Header {...accident} onStatusChange={onStatusChange} />
      <Body
        {...accident}
        occupantUserList={occupantUserList}
        message={message}
        onMessageChange={setMessage}
      />
      {accident.activeFlag && (
        <Footer
          eventId={accident.eventId}
          onStatusChange={onStatusChange}
          onSendClick={handleSendMessage}
          valid={isValid}
          occupantUserLength={occupantUserList.length}
        />
      )}
    </>
  );
}

type HeaderProps = Accident & {
  onStatusChange: (status: PaneStatus) => void;
};

function Header({
  activeFlag,
  registDate,
  updateDate,
  onStatusChange,
}: HeaderProps) {
  const { t } = useTranslation();
  const [elapsedTime, setElapsedTime] = useState('00:00:00');

  const calcElapsedTime = useCallback(() => {
    let time = '';
    if (activeFlag) {
      time = CommonUtils.calcElapsedTime(registDate);
    } else {
      time = CommonUtils.calcElapsedTime(registDate, updateDate);
    }
    setElapsedTime(time);
  }, [registDate]);

  useEffect(() => {
    calcElapsedTime();

    let interval: ReturnType<typeof setInterval>;
    if (activeFlag) {
      interval = setInterval(() => {
        calcElapsedTime();
      }, 1000);
    }

    return () => {
      clearInterval(interval);
    };
  }, [registDate]);

  return (
    <div
      className={classNames('emg-stat p-16pt', {
        'emg-disabled': !activeFlag,
      })}
    >
      <div className="emg-top d-flex align-items-center pb-12pt mb-16pt">
        <a
          className="go-back"
          onClick={() => {
            onStatusChange(PANE_STATUS_LIST);
          }}
        >
          <MaterialIcon name={'arrow_back'} className={'text-white'} />
        </a>
        <div className="d-flex col-auto">
          <p className="text-white mb-0">{t('text_first_detection_time')}</p>
        </div>
        <div className="col p-0 text-right">
          <strong className="mb-0 text-white">
            {moment(registDate).format('YYYY.MM.DD HH:mm:ss')}
          </strong>
        </div>
      </div>
      <div className="emg-con py-8pt text-center">
        <em className="time mb-2">{elapsedTime}</em>
        <p className="tit m-0">
          {activeFlag && t('text_elapsed_time')}
          {!activeFlag && t('text_end_of_situation')}
        </p>
      </div>
    </div>
  );
}

type BodyProps = Accident & {
  occupantUserList: RealtimeUser[];
  message: Message;
  onMessageChange: (message: Message) => void;
};

function Body({
  eventId,
  eventDetailCategory,
  occupantUserList,
  areaName,
  activeFlag,
  message,
  onMessageChange,
}: BodyProps) {
  const { t } = useTranslation();
  const fileInput = useRef<HTMLInputElement>(null);
  const { project } = useControlProject();
  const [messageHistory, setMessageHistory] = useState<MessageHistory[]>([]);

  let eventTypeText = '';
  let eventContentPlaceholder = '';
  switch (eventDetailCategory) {
    case ACCIDENT_FIRE:
      eventTypeText = t('text_fire');
      eventContentPlaceholder = t('place_holder_emergency_accident_message');
      break;
    case ACCIDENT_EMERGENCY:
      eventTypeText = t('text_emergency');
      eventContentPlaceholder = t('place_holder_fire_accident_message');
      break;
    case ACCIDENT_RESCUE:
      eventTypeText = t('text_rescue');
      eventContentPlaceholder = t('place_holder_rescue_accident_message');
      break;
    case ACCIDENT_INFECTIOUS_DISEASE:
      eventTypeText = t('text_infectious_disease');
      eventContentPlaceholder = t(
        'place_holder_infectious_disease_accident_message'
      );
      break;
  }

  const handleFetchMessages = async () => {
    const data = await fetchMessages({
      projectId: project.id,
      parentId: eventId,
    });
    if (data) {
      setMessageHistory(data);
    }
  };

  const handlePostImage = async (file: File) => {
    const imgId = await postImage(file);

    onMessageChange({
      ...message,
      imgId,
    });
  };

  const handleCancelImage = () => {
    const current = fileInput.current;
    if (current) {
      current.value = '';
    }

    onMessageChange({
      ...message,
      imgId: '',
    });
  };

  useEffect(() => {
    handleFetchMessages();
  }, [eventId]);

  return (
    <>
      <div className="emg-info">
        <div className="info-col info-col-line d-flex align-items-center">
          <p className="flex m-0">{t('text_kind_of_accident')}</p>
          <strong>{eventTypeText}</strong>
        </div>
        {areaName && (
          <div className="info-col info-col-line d-flex align-items-center">
            <p className="flex m-0">{t('text_location_of_accident')}</p>
            <strong>{areaName}</strong>
          </div>
        )}
        {activeFlag && (
          <div className="info-col info-col-line">
            <div className="d-flex align-items-center">
              <p className="flex m-0">{t('text_number_of_occupants')}</p>
              <strong className="text-accent font-size-24pt">
                {occupantUserList.length + t('text_unit_of_person')}
              </strong>
            </div>
            <PerfectScrollbar className="card-body mt-2">
              <div className="card-inner">
                <ul>
                  {occupantUserList.map((occupantUser) => {
                    let genderText = '';
                    if (occupantUser.userGender === 'M') {
                      genderText = t('text_male');
                    } else {
                      genderText = t('text_female');
                    }

                    return (
                      <li key={occupantUser.userId}>
                        <a>{`${occupantUser.userId} / ${genderText} / ${occupantUser.userAge}`}</a>
                      </li>
                    );
                  })}
                </ul>
              </div>
            </PerfectScrollbar>
          </div>
        )}
        <div className="d-flex align-items-center">
          <p className="flex m-0">{t('text_shipping_history')}</p>
        </div>
        <PerfectScrollbar className="card-body mt-2">
          <div className="card-inner">
            <ul>
              {messageHistory.length === 0 && (
                <>
                  <li>{t('msg_no_shipping_history')}</li>
                </>
              )}
              {messageHistory.map((history) => (
                <li key={history.messageId}>
                  <a>
                    <span>{history.messageTitle}</span>
                    <span
                      style={{ color: 'rgba(39, 44, 51, .7)' }}
                      dangerouslySetInnerHTML={{
                        __html: history.messageContent,
                      }}
                    ></span>{' '}
                    <small>
                      {moment(history.registDate).format('YY.MM.DD HH:mm:ss')}
                    </small>
                  </a>
                </li>
              ))}
            </ul>
          </div>
        </PerfectScrollbar>
      </div>
      {activeFlag && (
        <div className="container-fluid">
          <div className="form-group">
            <FormLabel
              textKey={'text_enter_message_content'}
              className={'mb-0'}
              essential={true}
              htmlFor={'title'}
            />
            <input
              type="text"
              className="form-line"
              id="title"
              placeholder={t('place_holder_title')}
              value={message.title}
              onChange={(e) => {
                onMessageChange({
                  ...message,
                  title: e.target.value,
                });
              }}
            />
          </div>
          <div className="form-group mb-4" style={{ height: '172px' }}>
            <ReactQuill
              style={{ height: '130px' }}
              placeholder={eventContentPlaceholder}
              modules={{
                toolbar: [
                  ['bold', 'italic'],
                  ['link', 'blockquote', 'code', 'image'],
                  [{ list: 'ordered' }, { list: 'bullet' }],
                ],
              }}
              value={message.content}
              onChange={(content, delta, source, editor) => {
                if (!editor.getText().trim().length) {
                  content = '';
                }
                onMessageChange({
                  ...message,
                  content,
                });
              }}
            />
          </div>
          <FormGroup>
            <FormLabel textKey={'text_link_url'} />
            <input
              type="text"
              className="form-line"
              placeholder={t('place_holder_url')}
              value={message.linkUrl}
              onChange={(e) => {
                onMessageChange({
                  ...message,
                  linkUrl: e.target.value,
                });
              }}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel textKey={'text_file_registration'} />
            <input
              className="form-control"
              type="file"
              accept={'image/*'}
              onChange={(e) => {
                if (e.target.files && e.target.files.length) {
                  handlePostImage(e.target.files[0]);
                } else {
                  onMessageChange({
                    ...message,
                    imgId: '',
                  });
                }
              }}
              ref={fileInput}
            />
            {message.imgId && (
              <a className="file-del" onClick={handleCancelImage}>
                <MaterialIcon name={'close'} />
              </a>
            )}
          </FormGroup>
        </div>
      )}
    </>
  );
}

type FooterProps = {
  eventId: string;
  onStatusChange: (status: PaneStatus) => void;
  onSendClick: () => void;
  valid: boolean;
  occupantUserLength: number;
};

function Footer({
  eventId,
  onStatusChange,
  valid,
  occupantUserLength,
  onSendClick,
}: FooterProps) {
  const { t } = useTranslation();
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (valid) {
      setShowInvalidMessage(false);
    }
  }, [valid]);

  const handleEndSituation = async () => {
    const data = await putAccidentInactive(eventId);
    if (data) {
      onStatusChange(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      {showInvalidMessage && (
        <InvalidAlert
          messageKey={
            occupantUserLength === 0
              ? 'msg_no_occupants'
              : 'msg_valid_enter_required_value'
          }
        />
      )}
      <div className="mt-48pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary"
            onClick={() => handleEndSituation()}
          >
            {t('text_end_of_situation')}
          </a>
          <a
            className={'btn btn-outline-accent ml-3'}
            onClick={() => {
              if (valid) {
                setShowInvalidMessage(false);
                onSendClick();
              } else {
                setShowInvalidMessage(true);
              }
            }}
          >
            {t('text_send_message')}
          </a>
        </div>
      </div>
    </>
  );
}

export default AccidentDetail;
