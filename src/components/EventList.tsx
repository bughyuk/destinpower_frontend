import React, { useEffect, useState } from 'react';
import {
  FUNCTION_COPY,
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
  PANE_STATUS_REGISTER,
  PaneStatus,
} from '@/utils/constants/common';

import DateBadge from '@/components/DateBadge';
import ListDropdown from '@/components/ListDropdown';
import { deleteEvent, fetchEvents } from '@/api/event';
import {
  useControlSpace,
  useControlEvent,
  useControlProject,
} from '@/modules/map/hook';
import EmptyPane from '@/components/EmptyPane';
import ListHeader from '@/components/ListHeader';
import Pagination from '@/components/Pagination';
import { ControlEvent } from '@/modules/event/types';
import classNames from 'classnames';
import moment from 'moment';

type EventListProps = {
  onChangeEventId: (eventId: string) => void;
  onStatusChange: (status: PaneStatus) => void;
};

function EventList({ onChangeEventId, onStatusChange }: EventListProps) {
  const { space } = useControlSpace();
  const { project } = useControlProject();
  const [load, setLoad] = useState(false);
  const [totalCount, setTotalCount] = useState(0);
  const [eventList, setEventList] = useState<ControlEvent[]>([]);
  const [page, setPage] = useState(1);

  const handleFetchEvent = async () => {
    const data = await fetchEvents({
      page,
      projectId: project.id,
      mappingId: space.spaceMappingId,
      mapId: space.floorsMapId,
    });

    setLoad(true);
    setEventList(data.content);
    setTotalCount(data.totalElements);
  };

  useEffect(() => {
    if (space.spaceMappingId && space.floorsMapId) {
      setLoad(false);
      setEventList([]);
      setPage(1);
      handleFetchEvent();
    }
  }, [space]);

  useEffect(() => {
    handleFetchEvent();
  }, [page]);

  const handleReload = () => {
    handleFetchEvent();
  };

  if (!space.floorsMapId) {
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
  }

  return (
    <>
      {!load && <></>}
      {load && eventList.length === 0 && (
        <EmptyPane
          textKey={'msg_event_empty'}
          descriptionKey={'msg_event_suggest_to_register'}
          btnTextKey={'text_event_registration'}
          onClick={() => {
            onStatusChange(PANE_STATUS_REGISTER);
          }}
        />
      )}
      {load && eventList.length > 0 && (
        <>
          <ListHeader
            titleKey={'text_event'}
            descriptionKey={'msg_event_list'}
            createBtnTextKey={'text_new_registration'}
            onCreateBtnClick={() => {
              onStatusChange(PANE_STATUS_REGISTER);
            }}
          />
          <div className="list-group row-list list-group-flush mb-4">
            {eventList.map((event) => (
              <EventItem
                key={event.eventId}
                {...event}
                onClick={() => {
                  onChangeEventId(event.eventId);
                  onStatusChange(PANE_STATUS_DETAIL);
                }}
                onEditClick={(eventId: string) => {
                  onChangeEventId(eventId);
                  onStatusChange(PANE_STATUS_EDIT);
                }}
                onReload={handleReload}
              />
            ))}
          </div>
          <Pagination
            curPage={page}
            totalCount={totalCount}
            onPageChange={(page) => {
              setPage(page);
            }}
          />
        </>
      )}
    </>
  );
}

type EventItemProps = ControlEvent & {
  onClick: () => void;
  onEditClick: (eventId: string) => void;
  onReload: () => void;
};

function EventItem({
  eventId,
  eventTitle,
  registDate,
  startDate,
  endDate,
  activeFlag,
  onClick,
  onEditClick,
  onReload,
}: EventItemProps) {
  const { handleReloadActivationEvents } = useControlEvent();

  const handleDeleteEvent = async () => {
    const data = await deleteEvent(eventId);
    if (data) {
      handleReloadActivationEvents();
      onReload();
    }
  };

  const handleOptions = (eventKey: string | null) => {
    // TODO: API
    switch (eventKey) {
      case FUNCTION_EDIT:
        onEditClick(eventId);
        break;
      case FUNCTION_COPY:
        break;
      case FUNCTION_DELETE:
        handleDeleteEvent();
        break;
    }
  };

  return (
    <div className="list-group-item d-flex" onClick={onClick}>
      <div
        className={classNames('flex d-flex align-items-center mr-16pt', {
          deactivate:
            !activeFlag ||
            !moment().isBetween(
              moment(startDate),
              moment(endDate),
              'days',
              '[]'
            ),
        })}
      >
        <div className="flex list-els">
          <a className="card-title">{eventTitle}</a>
          <div className="card-subtitle text-50">
            <DateBadge date={registDate} />
          </div>
        </div>
      </div>
      <ListDropdown onSelect={handleOptions} />
    </div>
  );
}

export default EventList;
