import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import {
  usePhysicalDistributionStatistics,
  usePhysicalDistributionWarehouse,
} from '@/modules/physical_distribution/hook';
import classNames from 'classnames';
import Preloader from '@/components/Preloader';

function StatisticsSmartFactory() {
  const { t } = useTranslation();
  const {
    load: loadWarehouseList,
    warehouseList,
  } = usePhysicalDistributionWarehouse();
  const {
    selectedWarehouseId,
    handleSetWarehouseId,
  } = usePhysicalDistributionStatistics();

  useEffect(() => {
    return () => {
      handleSetWarehouseId('');
    };
  }, []);

  const handleClickWarehouseId = (warehouseId: string) => {
    handleSetWarehouseId(warehouseId);
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="d-flex align-items-center mb-2">
          <div className="flex d-flex flex-sm-row align-items-center mb-0">
            <div className="mr-24pt">
              <h3 className="mb-0">{t('text_statistics')}</h3>
            </div>
          </div>
        </div>
        <div className="board-title"></div>
      </div>
      <div className="container-fluid">
        <ul className="dot-list">
          <li>
            <a
              className={classNames({
                active: selectedWarehouseId === '',
              })}
              onClick={() => handleClickWarehouseId('')}
            >
              <span></span> {t('text_the_whole')}
            </a>
          </li>
          {!loadWarehouseList && <Preloader />}
          {warehouseList.map(({ warehouseId, warehouseName }) => (
            <li key={warehouseId}>
              <a
                className={classNames({
                  active: selectedWarehouseId === warehouseId,
                })}
                onClick={() => handleClickWarehouseId(warehouseId)}
              >
                <span></span> {warehouseName}
              </a>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
}

export default StatisticsSmartFactory;
