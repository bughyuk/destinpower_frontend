import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, ButtonGroup } from 'react-bootstrap';
import {
  PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
} from '@/modules/physical_distribution/types';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import RightPhysicalDistributionStockHistoryAccordion from '@/components/RightPhysicalDistributionStockHistoryAccordion';

type HistoryButtonIdx =
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT
  | typeof PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT;

function RightPhysicalDistributionHistory() {
  const { t } = useTranslation();
  const { handleSetCategoryIdx } = usePhysicalDistributionCategory();

  const buttonList: {
    idx: HistoryButtonIdx;
    name: string;
  }[] = [
    {
      idx: PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
      name: t('text_warehousing'),
    },
    {
      idx: PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE,
      name: t('text_release'),
    },
    {
      idx: PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT,
      name: t('text_adjustment'),
    },
    {
      idx: PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT,
      name: t('text_movement'),
    },
  ];

  return (
    <>
      <ButtonGroup className={'mb-3 w-100'}>
        {buttonList.map(({ idx, name }) => (
          <Button
            key={idx}
            as={'a'}
            variant={'outline-secondary'}
            onClick={() => {
              handleSetCategoryIdx(idx);
            }}
          >
            {name}
          </Button>
        ))}
      </ButtonGroup>
      <div className="accordion js-accordion accordion--boxed list-group-flush">
        <RightPhysicalDistributionStockHistoryAccordion />
      </div>
    </>
  );
}

export default RightPhysicalDistributionHistory;
