import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ASSETS_INTERVAL,
  PANE_STATUS_DETAIL,
  PANE_STATUS_STATUS_BOARD,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { Collapse } from 'react-bootstrap';
import classNames from 'classnames';
import {
  ASSET_SIGNAL_STATUS_ERROR,
  ASSET_SIGNAL_STATUS_GOOD,
  ASSET_SIGNAL_STATUS_NO_SIGNAL,
  ASSET_SIGNAL_STATUS_STOP,
  ASSET_SIGNAL_STATUS_USUALLY,
  Assets,
  ASSETS_DIV_SENSOR,
  AssetsDiv,
} from '@/modules/assets/types';
import { fetchAssets } from '@/api/assets';
import { useControlSpace, useOpenLayers } from '@/modules/map/hook';

type AssetsListProps = {
  assetsDiv: AssetsDiv;
  onChangeStatus: (status: PaneStatus) => void;
  onChangeAssets: (assets: Assets) => void;
};

function AssetsList({
  assetsDiv,
  onChangeStatus,
  onChangeAssets,
}: AssetsListProps) {
  return (
    <>
      <Header assetsDiv={assetsDiv} onChangeStatus={onChangeStatus} />
      <Body
        assetsDiv={assetsDiv}
        onChangeStatus={onChangeStatus}
        onChangeAssets={onChangeAssets}
      />
    </>
  );
}

type HeaderProps = {
  assetsDiv: AssetsDiv;
  onChangeStatus: (status: PaneStatus) => void;
};

function Header({ assetsDiv, onChangeStatus }: HeaderProps) {
  const { t } = useTranslation();

  let title = '';
  if (assetsDiv === ASSETS_DIV_SENSOR) {
    title = t('text_assets_sensor_list');
  } else {
    title = t('text_assets_total_list');
  }

  return (
    <div className="container-fluid  py-4">
      <div className="flex d-flex">
        <a
          className="pr-2"
          onClick={() => onChangeStatus(PANE_STATUS_STATUS_BOARD)}
        >
          <MaterialIcon name={'arrow_back'} />
        </a>
        <h3 className="mb-0">{title}</h3>
      </div>
    </div>
  );
}

let assetsTimer: ReturnType<typeof setInterval> | null = null;

type BodyProps = {
  assetsDiv: AssetsDiv;
  onChangeStatus: (status: PaneStatus) => void;
  onChangeAssets: (assets: Assets) => void;
};

function Body({ assetsDiv, onChangeStatus, onChangeAssets }: BodyProps) {
  const { t } = useTranslation();
  const { space } = useControlSpace();
  const { map } = useOpenLayers();
  const [load, setLoad] = useState(false);
  const [assetsList, setAssetsList] = useState<Assets[]>([]);
  const [displayAssetsId, setDisplayAssetsId] = useState('');

  useEffect(() => {
    handleFetchAssetsList();
    assetsTimer = setInterval(() => {
      handleFetchAssetsList();
    }, ASSETS_INTERVAL);

    return () => {
      if (assetsTimer) {
        clearInterval(assetsTimer);
      }
    };
  }, []);

  const handleFetchAssetsList = async () => {
    setLoad(false);
    const data = await fetchAssets({
      metaid: space.spaceMetaId,
      mapid: space.floorsMapId,
      assetdiv: assetsDiv,
    });
    setAssetsList(data);
    setLoad(true);
  };

  const handleRouteAssetsDetail = (assets: Assets) => {
    onChangeAssets(assets);
    onChangeStatus(PANE_STATUS_DETAIL);
  };

  return (
    <div className="container-fluid">
      <div className="mb-4">
        <div className="d-flex align-items-center mb-2">
          <h5 className="d-flex flex m-0">{t('text_detailed_assets')}</h5>
          <span className="v">
            {t('text_total')}{' '}
            <strong className="text-iden">{assetsList.length}</strong>
            {t('text_count')}
          </span>
        </div>
        <ul className="sidebar-menu">
          {!load && <></>}
          {load && assetsList.length === 0 && (
            <em
              className="none-list"
              style={{
                borderTop: 'none',
              }}
            >
              {t('msg_assets_empty')}
            </em>
          )}
          {load &&
            assetsList.length > 0 &&
            assetsList.map((assets) => (
              <AssetsItem
                key={assets.asset_id}
                {...assets}
                displayAssetsId={displayAssetsId}
                onChangeDisplayAssetsId={setDisplayAssetsId}
                onClickDetail={() => handleRouteAssetsDetail(assets)}
              />
            ))}
        </ul>
      </div>
    </div>
  );
}

type AssetsItem = Assets & {
  displayAssetsId: string;
  onChangeDisplayAssetsId: (assetsId: string) => void;
  onClickDetail: () => void;
};

function AssetsItem({
  asset_id,
  asset_name,
  util_serial_no,
  asset_sigdate,
  asset_status,
  displayAssetsId,
  onChangeDisplayAssetsId,
  onClickDetail,
}: AssetsItem) {
  const { t } = useTranslation();

  let signalStatus = '';
  switch (asset_status) {
    case ASSET_SIGNAL_STATUS_GOOD:
      signalStatus = t('text_good_signal');
      break;
    case ASSET_SIGNAL_STATUS_USUALLY:
      signalStatus = t('text_usually_signal');
      break;
    case ASSET_SIGNAL_STATUS_ERROR:
      signalStatus = t('text_error_signal');
      break;
    case ASSET_SIGNAL_STATUS_NO_SIGNAL:
      signalStatus = t('text_no_signal');
      break;
    case ASSET_SIGNAL_STATUS_STOP:
      signalStatus = t('text_disable_signal');
      break;
  }

  return (
    <li
      className={classNames('sidebar-menu-item', {
        open: displayAssetsId === asset_id,
      })}
    >
      <a
        className={`sidebar-menu-button dot-level0${asset_status + 1}`}
        onClick={() => {
          let id = '';
          if (displayAssetsId !== asset_id) {
            id = asset_id;
          }
          onChangeDisplayAssetsId(id);
        }}
      >
        <span className={`dot dot-level0${asset_status + 1}`}></span>
        {asset_name}
        <span className="sidebar-menu-badge ml-auto mr-2">{signalStatus}</span>
        <span className="sidebar-menu-toggle-icon"></span>
      </a>
      <Collapse in={displayAssetsId === asset_id}>
        <div className="sidebar-submenu sm-indent">
          <div className="cover">
            <div className="col-6 p-2">
              <span className="text-50">{t('text_model_number')}</span>
              <p className="m-0">{util_serial_no}</p>
            </div>
            <div className="col-6 p-2">
              <span className="text-50">{t('text_last_reply_date')}</span>
              <p className="m-0">{asset_sigdate}</p>
            </div>
            <div className="btn-cover">
              <a className="btn" onClick={() => onClickDetail()}>
                {t('text_detail_view')}
              </a>
            </div>
          </div>
        </div>
      </Collapse>
    </li>
  );
}

export default AssetsList;
