import {
  useControlMode,
  useControlUserPane,
  useSetupData,
} from '@/modules/setup/hook';
import {
  useControlProject,
  useControlUser,
  useOpenLayers,
} from '@/modules/map/hook';
import React, { useEffect, useState } from 'react';
import {
  CONTROL_MODE_TENSE_HISTORY,
  ControlUserTab,
} from '@/modules/setup/types';
import { useTranslation } from 'react-i18next';
import { Nav, TabContainer, TabContent, TabPane } from 'react-bootstrap';
import { Feature } from 'ol';
import { Circle, Fill, Icon, Stroke, Style } from 'ol/style';
import PerfectScrollbar from 'react-perfect-scrollbar';
import classNames from 'classnames';
import MaterialIcon from '@/components/MaterialIcon';
import { HistoryUser, RealtimeUser, UserTrace } from '@/modules/map/types';
import { fetchEndPointUser, fetchEndPointUserTrace } from '@/api/endpoint';
import { postMessages } from '@/api/message';
import { htmlToText } from 'html-to-text';
import { Point } from 'ol/geom';
import moment from 'moment';
import RealtimeUserMenuGuideModal from '@/components/RealtimeUserMenuGuideModal';
import {
  LOCAL_STORAGE_KEY_USER_MENU_GUIDE_NOT_VIEW,
  USER_TYPE,
} from '@/utils/constants/common';

type UserContentProps = {
  accessKey: string;
};

function RealtimeUserContent() {
  const {
    controlUserTab,
    controlUserFlag,
    handleResetControlUserTab,
  } = useControlUserPane();
  const { users } = useControlUser();
  const [accessKey, setAccessKey] = useState('');
  const [activeKey, setActiveKey] = useState<ControlUserTab>('info');

  const { draw } = useOpenLayers();

  useEffect(() => {
    return () => {
      handleResetControlUserTab();
      draw.source?.clear();
    };
  }, []);

  useEffect(() => {
    setActiveKey(controlUserTab);
  }, [controlUserTab, controlUserFlag]);

  useEffect(() => {
    if (users.length) {
      let exist = false;
      if (accessKey) {
        exist = users.some((user) => user.accessKey === accessKey);
      }

      if (!exist) {
        const user = users[0];
        setAccessKey(user.accessKey);
      }
    } else {
      setAccessKey('');
      setActiveKey('info');
      draw.source?.clear();
    }
  }, [users]);

  useEffect(() => {
    if (!accessKey) {
      draw.source?.clear();
    }
  }, [accessKey]);

  return (
    <>
      {users.length > 0 && <Header accessKey={accessKey} />}
      <Body
        activeKey={activeKey}
        accessKey={accessKey}
        onChangeActiveKey={setActiveKey}
        onChangeAccessKey={setAccessKey}
      />
    </>
  );
}

type HeaderProps = {
  accessKey: string;
};

function Header({ accessKey }: HeaderProps) {
  const { t } = useTranslation();
  const { realtimeSource } = useOpenLayers();
  const { users, handleCancelControlUser } = useControlUser();

  const handleCancelUser = () => {
    handleCancelControlUser(accessKey);
    const feature = realtimeSource?.getFeatureById(accessKey);
    if (feature && feature.getStyle()) {
      const style = feature.getStyle() as Style;
      const realtimeUserFeatureData = feature.get('realtime_user');
      if (realtimeUserFeatureData) {
        const realtimeUser = realtimeUserFeatureData as RealtimeUser;
        let iconImageUrl = 'person_pin.svg';
        if (realtimeUser.userType === USER_TYPE.MANAGER) {
          iconImageUrl = 'manager_pin.svg';
        }
        style.setImage(
          new Icon({
            src: `/static/images/${iconImageUrl}`,
            scale: 2,
          })
        );
        feature.setStyle(style);
      }
    }
  };

  return (
    <>
      <div className="container-fluid d-flex align-items-center py-4">
        <div className="flex d-flex">
          <div className="mr-24pt">
            <h3 className="mb-0">{t('text_selection_user')}</h3>
          </div>
        </div>
      </div>
      {users.length === 1 && (
        <div className="container-fluid d-flex align-items-center py-2 justify-content-end">
          <div className="d-flex">
            <a className="btn-link text-accent" onClick={handleCancelUser}>
              {t('text_selection_cancel_user')}
            </a>
          </div>
        </div>
      )}
    </>
  );
}

type BodyProps = UserContentProps & {
  activeKey: ControlUserTab;
  onChangeActiveKey: (activeKey: ControlUserTab) => void;
  onChangeAccessKey: (accessKey: string) => void;
};

function Body({
  activeKey,
  accessKey,
  onChangeActiveKey,
  onChangeAccessKey,
}: BodyProps) {
  const { t } = useTranslation();
  const { users } = useControlUser();
  const [modalShow, setModalShow] = useState(false);

  useEffect(() => {
    if (!localStorage.getItem(LOCAL_STORAGE_KEY_USER_MENU_GUIDE_NOT_VIEW)) {
      setModalShow(true);
    }
  }, []);

  if (users.length === 0) {
    return (
      <>
        <div className="tit-intro">
          <em>{t('msg_no_selection_user')}</em>
          <p>{t('msg_user_suggest_to_selection')}</p>
          <a
            className="btn text-accent text-und"
            onClick={() => setModalShow(true)}
          >
            {t('text_user_menu_guide')}
          </a>
        </div>
        <RealtimeUserMenuGuideModal
          show={modalShow}
          onChangeShow={setModalShow}
        />
      </>
    );
  }

  return (
    <div className="container-fluid">
      <TabContainer
        defaultActiveKey={activeKey}
        activeKey={activeKey}
        onSelect={(eventKey) => {
          if (eventKey) {
            onChangeActiveKey(eventKey as ControlUserTab);
          }
        }}
        transition={false}
      >
        <Nav className="contents-tab the-num03 mb-4">
          <Nav.Link eventKey={'info'} bsPrefix={''}>
            <span className="menu-cover">
              <span>{t('text_info')}</span>
            </span>
          </Nav.Link>
          <Nav.Link eventKey={'message'} bsPrefix={''}>
            <span className="menu-cover">
              <span>{t('text_send_message')}</span>
            </span>
          </Nav.Link>
          <Nav.Link eventKey={'trace'} bsPrefix={''}>
            <span className="menu-cover">
              <span>{t('text_traffic_tracking')}</span>
            </span>
          </Nav.Link>
        </Nav>
        {users.length > 1 && (
          <ControlUserList
            accessKey={accessKey}
            onChangeAccessKey={onChangeAccessKey}
          />
        )}
        <TabContent>
          <TabPane eventKey={'info'}>
            <Info accessKey={accessKey} />
          </TabPane>
          <TabPane eventKey={'message'}>
            <Message accessKey={accessKey} />
          </TabPane>
          <TabPane eventKey={'trace'}>
            <Trace activeKey={activeKey} accessKey={accessKey} />
          </TabPane>
        </TabContent>
      </TabContainer>
    </div>
  );
}

type ControlUserListProps = UserContentProps & {
  onChangeAccessKey: (accessKey: string) => void;
};

function ControlUserList({
  accessKey,
  onChangeAccessKey,
}: ControlUserListProps) {
  const { t } = useTranslation();
  const { realtimeSource } = useOpenLayers();
  const {
    users,
    handleCancelControlUser,
    handleResetControlUser,
  } = useControlUser();

  const handleCancel = (accessKey: string) => {
    handleChangeStyleCancelUser(realtimeSource?.getFeatureById(accessKey));
    handleCancelControlUser(accessKey);
  };

  const handleAllCancel = (e: React.MouseEvent) => {
    e.stopPropagation();
    handleResetControlUser();
    realtimeSource?.getFeatures().forEach((feature) => {
      handleChangeStyleCancelUser(feature);
    });
  };

  const handleChangeStyleCancelUser = (feature: Feature | undefined) => {
    if (feature && feature.getStyle()) {
      const realtimeUserFeatureData = feature.get('realtime_user');
      if (realtimeUserFeatureData) {
        const realtimeUser = realtimeUserFeatureData as RealtimeUser;
        const style = feature.getStyle() as Style;
        let iconImageUrl = 'person_pin.svg';
        if (realtimeUser.userType === USER_TYPE.MANAGER) {
          iconImageUrl = 'manager_pin.svg';
        }
        style.setImage(
          new Icon({
            src: `/static/images/${iconImageUrl}`,
            scale: 2,
          })
        );
        feature.setStyle(style);
      }
    }
  };

  return (
    <>
      <div className="d-flex mb-2">
        <span className="d-flex flex m-0">
          <strong className="text-iden">{users.length}</strong>
          {t('msg_number_of_user_have_been_selected')}
        </span>
        <a className="btn-link text-accent" onClick={handleAllCancel}>
          {t('text_all_selection_cancel_user')}
        </a>
      </div>
      <PerfectScrollbar className={'selected-user mt-2'}>
        <div className="card-inner">
          <ul>
            {users.map((user) => {
              let genderText = '';
              if (user.userGender === 'M') {
                genderText = t('text_male');
              } else {
                genderText = t('text_female');
              }

              return (
                <li key={user.accessKey}>
                  <a
                    className={classNames({
                      active: user.accessKey === accessKey,
                    })}
                    onClick={(e) => {
                      e.stopPropagation();
                      onChangeAccessKey(user.accessKey);
                    }}
                  >{`${user.userId} / ${genderText} / ${
                    user.userAge + t('text_years_old')
                  }`}</a>
                  <a
                    className="btn-del"
                    onClick={(e) => {
                      e.stopPropagation();
                      handleCancel(user.accessKey);
                    }}
                  >
                    <MaterialIcon name={'close'} />
                  </a>
                </li>
              );
            })}
          </ul>
        </div>
      </PerfectScrollbar>
    </>
  );
}

type InfoProps = UserContentProps;

function Info({ accessKey }: InfoProps) {
  const { t } = useTranslation();
  const [userInfo, setUserInfo] = useState<RealtimeUser>({
    accessKey: '',
    userId: '',
    deviceType: '',
    lng: 0,
    lat: 0,
    userAge: 0,
    userType: '',
    userGender: '',
    residenceTime: 0,
  });

  const handleFetchControlUser = async (accessKey: string) => {
    const data = await fetchEndPointUser(accessKey);
    setUserInfo(data);
  };

  useEffect(() => {
    if (accessKey) {
      handleFetchControlUser(accessKey);
    }
  }, [accessKey]);

  let genderText = '';
  if (userInfo.userGender === 'M') {
    genderText = t('text_male');
  } else {
    genderText = t('text_female');
  }

  return (
    <div className="user-info">
      <div className="p-16pt">
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_user')}</dt>
          <dd className="m-0">{userInfo.userId}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_identification_id')}</dt>
          <dd className="m-0">{accessKey}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_gender')}</dt>
          <dd className="m-0">{genderText}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_age')}</dt>
          <dd className="m-0">{userInfo.userAge}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_residence_time')}</dt>
          <dd className="m-0">
            {userInfo.residenceTime + t('text_unit_of_minute')}
          </dd>
        </dl>
      </div>
    </div>
  );
}

type MessageProps = UserContentProps;

function Message({ accessKey }: MessageProps) {
  const { t } = useTranslation();
  const { users } = useControlUser();
  const { handleSetHistoryUserFilter } = useSetupData();
  const { setControlModeTense } = useControlMode();
  const [targetAccessKeyList, setTargetAccessKeyList] = useState<string[]>([]);
  const { project } = useControlProject();
  const [inputs, setInputs] = useState({
    title: '',
    content: '',
  });

  useEffect(() => {
    if (users.length > 0) {
      setTargetAccessKeyList(users.map((user) => user.accessKey));
    }
  }, [users]);

  const handleSubmit = async () => {
    if (inputs.title && inputs.content) {
      const result = await postMessages({
        projectId: project.id,
        parentId: '',
        parentType: 'message',
        messageTitle: inputs.title,
        messageContent: htmlToText(inputs.content, {
          wordwrap: 130,
        }),
        imgId: '',
        targetIds: targetAccessKeyList,
      });

      if (result) {
        setInputs({
          title: '',
          content: '',
        });
      }
    }
  };

  const handlePastHistory = () => {
    const historyUsers: HistoryUser[] = [];
    users.forEach((user) => {
      const historyUser: HistoryUser = {
        access_key: user.accessKey,
        user_id: user.userId,
        user_age: user.userAge,
        user_gender: user.userGender,
        device_type: user.deviceType,
      };

      historyUsers.push(historyUser);
    });

    handleSetHistoryUserFilter({
      accessKey,
      users: historyUsers,
      tab: 'message',
    });
    setControlModeTense(CONTROL_MODE_TENSE_HISTORY, 'user');
  };

  return (
    <div className="p-16pt">
      <div className="form-group mb-4">
        <label className="form-label mb-0 text-50 essential">
          {t('text_title')}
        </label>
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_title')}
          value={inputs.title}
          onChange={(e) => setInputs({ ...inputs, title: e.target.value })}
        />
      </div>
      <div className="form-group mb-4">
        <label className="form-label mb-0 text-50 essential mb-2">
          {t('text_content')}
        </label>
        <div className="msg-box">
          <textarea
            className="form-control"
            rows={3}
            placeholder={t('place_holder_content')}
            style={{
              marginTop: '0px',
              marginBottom: '0px',
              height: '148px',
              resize: 'none',
            }}
            value={inputs.content}
            maxLength={1000}
            onChange={(e) => setInputs({ ...inputs, content: e.target.value })}
          ></textarea>
          <div className="d-flex p-2">
            <span className="">{inputs.content.length}/1000</span>
            <a
              className={classNames({
                disabled: !inputs.title || !inputs.content,
              })}
              onClick={handleSubmit}
            >
              {t('text_send_message')}
            </a>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <a className="btn btn-link text-accent" onClick={handlePastHistory}>
          <strong>{t('text_past_shipping_history')}</strong>
        </a>
      </div>
    </div>
  );
}

type TraceProps = UserContentProps & {
  activeKey: ControlUserTab;
};

function Trace({ activeKey, accessKey }: TraceProps) {
  const { t } = useTranslation();
  const { map, draw } = useOpenLayers();
  const [traceList, setTraceList] = useState<UserTrace[]>([]);
  const [sectionPathLogDate, setSectionPathLogDate] = useState('');

  const { users } = useControlUser();
  const { handleSetHistoryUserFilter } = useSetupData();
  const { setControlModeTense } = useControlMode();

  const handleFetchControlUserTrace = async (accessKey: string) => {
    const data = await fetchEndPointUserTrace(accessKey);
    if (data) {
      setTraceList(data);
    }
  };

  useEffect(() => {
    if (accessKey) {
      handleFetchControlUserTrace(accessKey);
      handleDeleteSectionPath();
    }
  }, [accessKey]);

  const handleDrawTrace = () => {
    draw.source?.clear();
    users.forEach(async (user) => {
      const userAccessKey = user.accessKey;
      const data = await fetchEndPointUserTrace(userAccessKey);
      if (data) {
        if (data.length > 0) {
          data.forEach((trace, index) => {
            const point = new Point([Number(trace.lng), Number(trace.lat)]);

            const feature = new Feature({
              geometry: point,
            });
            let fillColor = '#AEAEAE';
            if (userAccessKey === accessKey) {
              fillColor = '#FF6363';
            }

            feature.setStyle(
              new Style({
                image: new Circle({
                  radius: 8,
                  fill: new Fill({ color: fillColor }),
                  stroke: new Stroke({
                    color: '#000000',
                  }),
                }),
                zIndex: 4,
              })
            );
            draw.source?.addFeature(feature);

            /*
            if (index !== data.length - 1) {
              const coordinates: number[][] = [];
              const nextTrace = data[index + 1];
              coordinates.push([trace.lng, trace.lat]);
              coordinates.push([nextTrace.lng, nextTrace.lat]);
              const lineString = turf.lineString(coordinates);
              const geoJSON = new GeoJSON();
              const feature = geoJSON.readFeature(lineString);

              let strokeColor = '#AEAEAE';
              if (userAccessKey === accessKey) {
                strokeColor = '#FF6363';
              }

              feature.setStyle(
                new Style({
                  stroke: new Stroke({
                    color: strokeColor,
                    width: 5,
                  }),
                  zIndex: 4,
                })
              );

              draw.source?.addFeature(feature);
            }
            */
          });
        }
      }
    });
  };

  useEffect(() => {
    if (activeKey === 'trace') {
      handleDrawTrace();
    } else {
      draw.source?.clear();
      setSectionPathLogDate('');
    }
  }, [activeKey]);

  const handleDeleteSectionPath = () => {
    const sectionPathFeature = draw.source?.getFeatureById('section-path');
    if (sectionPathFeature) {
      draw.source?.removeFeature(sectionPathFeature);
    }
  };

  const handleDrawSectionPath = (trace: UserTrace) => {
    handleDeleteSectionPath();
    if (sectionPathLogDate !== trace.logDate) {
      /*
      const sectionPath = trace.sectionPath;
      if (sectionPath) {
        const feature = new Feature(new LineString(sectionPath));
        feature.setId('section-path');
        feature.setStyle(
          new Style({
            stroke: new Stroke({
              color: '#59FFEA',
              width: 5,
            }),
            zIndex: 5,
          })
        );
      
        draw.source?.addFeature(feature);
      }
      */

      const point = new Point([Number(trace.lng), Number(trace.lat)]);
      map?.getView().setCenter([Number(trace.lng), Number(trace.lat)]);
      map?.getView().setZoom(15);

      const feature = new Feature(point);
      feature.setId('section-path');
      feature.setStyle(
        new Style({
          image: new Circle({
            radius: 8,
            fill: new Fill({ color: '#59FFEA' }),
            stroke: new Stroke({
              color: '#000000',
            }),
          }),
          zIndex: 4,
        })
      );
      draw.source?.addFeature(feature);

      setSectionPathLogDate(trace.logDate);
    } else {
      setSectionPathLogDate('');
    }
  };

  const handlePastHistory = () => {
    const historyUsers: HistoryUser[] = [];
    users.forEach((user) => {
      const historyUser: HistoryUser = {
        access_key: user.accessKey,
        user_id: user.userId,
        user_age: user.userAge,
        user_gender: user.userGender,
        device_type: user.deviceType,
      };

      historyUsers.push(historyUser);
    });

    handleSetHistoryUserFilter({
      accessKey,
      users: historyUsers,
      tab: 'trace',
    });
    setControlModeTense(CONTROL_MODE_TENSE_HISTORY, 'user');
  };

  return (
    <>
      <div className="route-list">
        <ul className="route-user01">
          {traceList.map((trace, idx) => (
            <li
              key={trace.areaName + trace.logDate}
              className={classNames('line-blue', {
                current: idx === 0,
              })}
            >
              <div className="route-inner">
                <div className="route-title">
                  <span className="time">
                    {moment(trace.logDate).format('HH:mm')}
                  </span>
                  <h6 className="title">{`${trace.areaName} (${
                    trace.residenceTime + t('text_unit_of_minute')
                  })`}</h6>
                </div>
                <a
                  className="btn-location"
                  onClick={() => {
                    handleDrawSectionPath(trace);
                  }}
                >
                  <MaterialIcon name={'place'} />
                </a>
              </div>
            </li>
          ))}
        </ul>
      </div>
      <div className="d-flex justify-content-center align-items-center">
        <a className="btn btn-link text-accent" onClick={handlePastHistory}>
          <strong>{t('text_past_traffic_tracking')}</strong>
        </a>
      </div>
    </>
  );
}

export default RealtimeUserContent;
