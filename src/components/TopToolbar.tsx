import React from 'react';
import TopMidToolbar from '@/components/TopMidToolbar';
import TopRightToolbar from '@/components/TopRIghtToolbar';

function TopToolbar() {
  return (
    <>
      <TopMidToolbar />
      <TopRightToolbar />
    </>
  );
}

export default TopToolbar;
