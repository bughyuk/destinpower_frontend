import { Alert } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import React from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import NewlineText from '@/components/NewlineText';

type InvalidAlertProps = {
  messageKey?: string;
  alertContainerPadding?: boolean;
};

function InvalidAlert({
  messageKey = 'msg_valid_enter_required_value',
  alertContainerPadding = true,
}: InvalidAlertProps) {
  const { t } = useTranslation();

  return (
    <Alert
      className={classNames('alert-soft-accent', {
        'invalid-alert': alertContainerPadding,
      })}
    >
      <div className="d-flex flex-wrap align-items-center">
        <div className="mr-8pt">
          <MaterialIcon name={'error_outline'} />
        </div>
        <div className="flex">
          <small className="text-black-100">
            <NewlineText text={t(messageKey)} />
          </small>
        </div>
      </div>
    </Alert>
  );
}

export default InvalidAlert;
