import React, { useEffect, useState } from 'react';
import {
  Collapse,
  Nav,
  OverlayTrigger,
  TabContainer,
  TabContent,
  TabPane,
  Tooltip,
} from 'react-bootstrap';
import { Korean } from 'flatpickr/dist/l10n/ko';
import Flatpickr from 'react-flatpickr';
import moment from 'moment';
import classNames from 'classnames';
import { fetchProcessCompleted } from '@/api/process';

function FloatProcessCompleted() {
  const [isAllSpread, setAllSpread] = useState(false);
  const [isHideCompleted, setHideCompleted] = useState(false);

  useEffect(() => {
    async () => {
      const testid = '1';
      try {
        const result = await fetchProcessCompleted(testid);
        console.log(result);
      } catch (err) {
        console.log(err);
      }
    };
  }, []);

  return (
    <>
      <TabContainer defaultActiveKey={'factory_1'} transition={false}>
        <div className="tab-cover mb-4">
          <Nav className="tab-menu" as={'ul'}>
            <Nav.Item as={'li'}>
              <Nav.Link eventKey={'factory_1'} bsPrefix={' '}>
                제 1공장
              </Nav.Link>
            </Nav.Item>
            <Nav.Item as={'li'}>
              <Nav.Link eventKey={'factory_2'} bsPrefix={' '}>
                제 2공장
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <div className="sort-cover">
            <div className="custom-control custom-checkbox d-flex align-items-center mr-4">
              <input
                type="checkbox"
                className="custom-control-input"
                id="sort-item03"
                checked={isAllSpread}
                onChange={(e) => setAllSpread(e.target.checked)}
              />
              <label className="custom-control-label" htmlFor="sort-item03">
                전체 펼침
              </label>
            </div>
            <select name="category" className="form-control custom-select">
              <option value="">내림차순</option>
              <option value="">오름차순</option>
              <option value="">정렬선택</option>
            </select>
          </div>
        </div>
        <TabContent>
          <TabPane eventKey={'factory_1'}>
            <FactoryInfo
              isAllSpread={isAllSpread}
              isHideCompleted={isHideCompleted}
            />
          </TabPane>
        </TabContent>
      </TabContainer>
    </>
  );
}

type FactoryInfoProps = {
  isAllSpread: boolean;
  isHideCompleted: boolean;
};

function FactoryInfo({ isAllSpread, isHideCompleted }: FactoryInfoProps) {
  const [isOpenProductLine, setOpenProductLine] = useState(false);
  const [isOpenProductType, setOpenProductType] = useState(false);
  const [isOpenProcess, setOpenProcess] = useState(false);
  const [isOpenProcessStep1, setOpenProcessStep1] = useState(false);
  const [isOpenProcessStep2, setOpenProcessStep2] = useState(false);
  const [isOpenProcessStep3, setOpenProcessStep3] = useState(false);
  const [isOpenProcessStep4, setOpenProcessStep4] = useState(false);
  const [isOpenProcessStep5, setOpenProcessStep5] = useState(false);
  const [isOpenProcessStep6, setOpenProcessStep6] = useState(false);
  const [isOpenProcessStep7, setOpenProcessStep7] = useState(false);

  const [productLineCheckList, setProductLineCheckList] = useState<number[]>(
    []
  );
  const [productTypeCheckList, setProductTypeCheckList] = useState<number[]>(
    []
  );
  const [processCheckList, setProcessCheckList] = useState<number[]>([]);

  useEffect(() => {
    setOpenProcess(isAllSpread);
    setOpenProcessStep1(isAllSpread);
    setOpenProcessStep2(isAllSpread);
    setOpenProcessStep3(isAllSpread);
    setOpenProcessStep4(isAllSpread);
    setOpenProcessStep5(isAllSpread);
    setOpenProcessStep6(isAllSpread);
    setOpenProcessStep7(isAllSpread);
  }, [isAllSpread]);

  return (
    <>
      <div className="accordion js-accordion list-group-flush accordion-sort">
        <div className="accordion__item">
          <div className="accordion__toggle">
            <h5 className="m-0 text-nowrap">기간</h5>
            <div className="ml-4">
              <div className="data-rage-cover">
                <span className="material-icons-outlined">date_range</span>
                <Flatpickr
                  options={{
                    mode: 'range',
                    altInput: true,
                    altInputClass: 'form-control flatpickr-input',
                    altFormat: 'Y-m-d',
                    locale: Korean,
                  }}
                  value={[moment().format(), moment().format()]}
                />
              </div>
            </div>
          </div>
        </div>
        <div
          className={classNames('accordion__item', {
            open: isOpenProductLine,
          })}
        >
          <div className="accordion__toggle">
            <h5 className="m-0 text-nowrap">제품군</h5>
            <div className="sort-btn-group ml-4">
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.length === 0,
                })}
                onClick={() => {
                  setProductLineCheckList([]);
                }}
              >
                전체
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(1),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(1)) {
                    setProductLineCheckList([...productLineCheckList, 1]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 1)
                    );
                  }
                }}
              >
                ESS ALL in ONE
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(2),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(2)) {
                    setProductLineCheckList([...productLineCheckList, 2]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 2)
                    );
                  }
                }}
              >
                ESS PCS
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(3),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(3)) {
                    setProductLineCheckList([...productLineCheckList, 3]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 3)
                    );
                  }
                }}
              >
                Multi Function PCS
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(4),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(4)) {
                    setProductLineCheckList([...productLineCheckList, 4]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 4)
                    );
                  }
                }}
              >
                Solar Inverter
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(5),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(5)) {
                    setProductLineCheckList([...productLineCheckList, 5]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 5)
                    );
                  }
                }}
              >
                Hybrid inverter
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(6),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(6)) {
                    setProductLineCheckList([...productLineCheckList, 6]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 6)
                    );
                  }
                }}
              >
                Fuel Cell Inverter
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productLineCheckList.includes(7),
                })}
                onClick={() => {
                  if (!productLineCheckList.includes(7)) {
                    setProductLineCheckList([...productLineCheckList, 7]);
                  } else {
                    setProductLineCheckList(
                      productLineCheckList.filter((value) => value !== 7)
                    );
                  }
                }}
              >
                Industrial UPS
              </a>
            </div>
            <a
              className="d-flex h-100 align-items-center ml-auto px-3 circle-pin"
              onClick={() => setOpenProductLine(!isOpenProductLine)}
            >
              <span className="accordion__toggle-icon material-icons">
                keyboard_arrow_down
              </span>
            </a>
          </div>
          <Collapse in={isOpenProductLine}>
            <div
              className={classNames('accordion__menu bg-light border-top-1', {
                open: isOpenProductLine,
              })}
            >
              <div className="row p-3">
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">생산건수</h5>
                        <div className="cell ml-auto">
                          <em>13</em>
                          <span className="text-dark font-size-16pt font-weight-bold">
                            건
                          </span>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 7건</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 6건</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">평균 L/T</h5>
                        <div className="cell ml-auto">
                          <em>1d 09:24:15</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 09:24:15</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 06:24:15</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">L/T 증감율</h5>
                        <div className="cell ml-auto">
                          <em>+ 00:12:00</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 +00:16:00</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 -00:12:00</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">공정률</h5>
                        <div className="cell ml-auto">
                          <em>78.2%</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-12">
                          <div className="cell blue-board">진행 45.2%</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Collapse>
        </div>
        <div
          className={classNames('accordion__item', {
            open: isOpenProductType,
          })}
        >
          <div className="accordion__toggle">
            <h5 className="m-0 text-nowrap">제품타입</h5>
            <div className="sort-btn-group ml-4">
              <a
                className={classNames('sort-btn', {
                  active: productTypeCheckList.length === 0,
                })}
                onClick={() => {
                  setProductTypeCheckList([]);
                }}
              >
                전체
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productTypeCheckList.includes(1),
                })}
                onClick={() => {
                  if (!productTypeCheckList.includes(1)) {
                    setProductTypeCheckList([...productTypeCheckList, 1]);
                  } else {
                    setProductTypeCheckList(
                      productTypeCheckList.filter((value) => value !== 1)
                    );
                  }
                }}
              >
                SAVEEN EVE Hybrid 60
              </a>
              <a
                className={classNames('sort-btn', {
                  active: productTypeCheckList.includes(2),
                })}
                onClick={() => {
                  if (!productTypeCheckList.includes(2)) {
                    setProductTypeCheckList([...productTypeCheckList, 2]);
                  } else {
                    setProductTypeCheckList(
                      productTypeCheckList.filter((value) => value !== 2)
                    );
                  }
                }}
              >
                SANEEN EVE Hybrid 200
              </a>
            </div>
            <a
              className="d-flex h-100 align-items-center ml-auto px-3 circle-pin"
              onClick={() => setOpenProductType(!isOpenProductType)}
            >
              <span className="accordion__toggle-icon material-icons">
                keyboard_arrow_down
              </span>
            </a>
          </div>
          <Collapse in={isOpenProductType}>
            <div
              className={classNames('accordion__menu bg-light border-top-1', {
                open: isOpenProductType,
              })}
            >
              <div className="row p-3">
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">생산건수</h5>
                        <div className="cell ml-auto">
                          <em>13</em>
                          <span className="text-dark font-size-16pt font-weight-bold">
                            건
                          </span>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 7건</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 6건</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">평균 L/T</h5>
                        <div className="cell ml-auto">
                          <em>1d 09:24:15</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 09:24:15</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 06:24:15</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">L/T 증감율</h5>
                        <div className="cell ml-auto">
                          <em>+ 00:12:00</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 +00:16:00</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 -00:12:00</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">공정률</h5>
                        <div className="cell ml-auto">
                          <em>78.2%</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-12">
                          <div className="cell blue-board">진행 45.2%</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Collapse>
        </div>
        <div
          className={classNames('accordion__item', {
            open: isOpenProcess,
          })}
        >
          <div className="accordion__toggle">
            <h5 className="m-0 text-nowrap">공정</h5>
            <div className="sort-btn-group ml-4">
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.length === 0,
                })}
                onClick={() => {
                  setProcessCheckList([]);
                }}
              >
                전체
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(1),
                })}
                onClick={() => {
                  if (!processCheckList.includes(1)) {
                    setProcessCheckList([...processCheckList, 1]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 1
                      )
                    );
                  }
                }}
              >
                조립1공정
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(2),
                })}
                onClick={() => {
                  if (!processCheckList.includes(2)) {
                    setProcessCheckList([...processCheckList, 2]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 2
                      )
                    );
                  }
                }}
              >
                조립2공정
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(3),
                })}
                onClick={() => {
                  if (!processCheckList.includes(3)) {
                    setProcessCheckList([...processCheckList, 3]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 3
                      )
                    );
                  }
                }}
              >
                조립3공정
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(4),
                })}
                onClick={() => {
                  if (!processCheckList.includes(4)) {
                    setProcessCheckList([...processCheckList, 4]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 4
                      )
                    );
                  }
                }}
              >
                조립4공정
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(5),
                })}
                onClick={() => {
                  if (!processCheckList.includes(5)) {
                    setProcessCheckList([...processCheckList, 5]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 5
                      )
                    );
                  }
                }}
              >
                조립5공정
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(6),
                })}
                onClick={() => {
                  if (!processCheckList.includes(6)) {
                    setProcessCheckList([...processCheckList, 6]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 6
                      )
                    );
                  }
                }}
              >
                조립6공정
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(7),
                })}
                onClick={() => {
                  if (!processCheckList.includes(7)) {
                    setProcessCheckList([...processCheckList, 7]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 7
                      )
                    );
                  }
                }}
              >
                조립 검사
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(8),
                })}
                onClick={() => {
                  if (!processCheckList.includes(8)) {
                    setProcessCheckList([...processCheckList, 8]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 8
                      )
                    );
                  }
                }}
              >
                공정풀짐 검사
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(9),
                })}
                onClick={() => {
                  if (!processCheckList.includes(9)) {
                    setProcessCheckList([...processCheckList, 9]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 9
                      )
                    );
                  }
                }}
              >
                양산 검사
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(10),
                })}
                onClick={() => {
                  if (!processCheckList.includes(10)) {
                    setProcessCheckList([...processCheckList, 10]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 10
                      )
                    );
                  }
                }}
              >
                출하 검사
              </a>
              <a
                className={classNames('sort-btn', {
                  active: processCheckList.includes(11),
                })}
                onClick={() => {
                  if (!processCheckList.includes(11)) {
                    setProcessCheckList([...processCheckList, 11]);
                  } else {
                    setProcessCheckList(
                      processCheckList.filter(
                        (processCheck) => processCheck !== 11
                      )
                    );
                  }
                }}
              >
                출하
              </a>
            </div>
            <a
              className="d-flex h-100 align-items-center ml-auto px-3 circle-pin"
              onClick={() => setOpenProcess(!isOpenProcess)}
            >
              <span className="accordion__toggle-icon material-icons">
                keyboard_arrow_down
              </span>
            </a>
          </div>
          <Collapse in={isOpenProcess}>
            <div
              className={classNames('accordion__menu bg-light border-top-1', {
                open: isOpenProcess,
              })}
            >
              <div className="row p-3">
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">생산건수</h5>
                        <div className="cell ml-auto">
                          <em>13</em>
                          <span className="text-dark font-size-16pt font-weight-bold">
                            건
                          </span>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 7건</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 6건</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">평균 L/T</h5>
                        <div className="cell ml-auto">
                          <em>1d 09:24:15</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 09:24:15</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 06:24:15</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">L/T 증감율</h5>
                        <div className="cell ml-auto">
                          <em>+ 00:12:00</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-6">
                          <div className="cell green-board">완료 +00:16:00</div>
                        </div>
                        <div className="col-6">
                          <div className="cell blue-board">진행 -00:12:00</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-3 d-flex">
                  <div className="card">
                    <div className="card-body">
                      <div className="d-flex flex-row align-items-center p-3 mb-3">
                        <h5 className="d-flex mb-0">공정률</h5>
                        <div className="cell ml-auto">
                          <em>78.2%</em>
                        </div>
                      </div>
                      <div className="row state-board d-flex">
                        <div className="col-12">
                          <div className="cell blue-board">진행 45.2%</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Collapse>
        </div>
        <div className="accordion__item">
          <div className="accordion__toggle border-bottom-1">
            <h5 className="m-0 text-nowrap">제품 S/N</h5>
            <div className="d-flex flex-row ml-4">
              <input
                type="text"
                className="form-control"
                style={{
                  width: '300px',
                }}
                placeholder="제품 S/N"
              />
              <a className="btn btn-dark ml-2">검색</a>
            </div>
          </div>
        </div>
      </div>
      <div className="d-flex justify-content-end">
        <div className="ml-3">
          <span className="material-icons text-success font-size-16pt mr-1">
            circle
          </span>
          공정 완료
        </div>
      </div>
      <div className="page-separator">
        <div className="page-separator__text">전체</div>
      </div>
      <div className="accordion js-accordion list-group-flush accordion-state">
        {!isHideCompleted && (
          <>
            <div
              className={classNames('accordion__item mb-4 state-cmp', {
                open: isOpenProcessStep1,
              })}
            >
              <div className="accordion__toggle">
                <div className="row w-100 p-3">
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>S/N</span>
                      <em>5N2218-00027VP</em>
                    </div>
                  </div>
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>프로젝트명</span>
                      <em>DOOSAN ESM LPG</em>
                    </div>
                  </div>
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>L/T</span>
                      <em>
                        {processCheckList.length > 0 &&
                          moment
                            .utc(
                              (moment.duration('09:24:15').asSeconds() /
                                (11 - processCheckList.length === 0
                                  ? 1
                                  : 11 - processCheckList.length)) *
                                1000
                            )
                            .format('HH:mm:ss')}
                        {processCheckList.length === 0 && '09:24:15'}
                      </em>
                    </div>
                  </div>
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>공정률</span>
                      <em>100%</em>
                      <div
                        className="progress"
                        style={{
                          height: '18px',
                        }}
                      >
                        <div
                          className="progress-bar pg-bar-success"
                          role="progressbar"
                          style={{
                            width: '100%',
                          }}
                          aria-valuenow={100}
                          aria-valuemin={0}
                          aria-valuemax={100}
                        >
                          100%
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="d-flex h-100 align-items-center ml-auto px-3 circle-pin"
                  onClick={() => setOpenProcessStep1(!isOpenProcessStep1)}
                >
                  <span className="accordion__toggle-icon material-icons">
                    keyboard_arrow_down
                  </span>
                </a>
              </div>
              <Collapse in={isOpenProcessStep1}>
                <div className="accordion__menu bg-light border-top-1">
                  <div className="process-board">
                    <div className="board-top">
                      <div className="row w-100 p-3">
                        <div className="col-3 d-flex">
                          <div className="cell">
                            <span>프로젝트명</span>
                            <h5>DOOSAN ESM</h5>
                          </div>
                        </div>
                        <div className="col-3 d-flex">
                          <div className="cell">
                            <span>제품군</span>
                            <h5>Fuel Cell Inverter</h5>
                          </div>
                        </div>
                        <div className="col-2 d-flex">
                          <div className="cell">
                            <span>거래처</span>
                            <h5>DOOSAN</h5>
                          </div>
                        </div>
                        <div className="col-2 d-flex">
                          <div className="cell">
                            <span>작업시작일</span>
                            <h5>2021-10-01</h5>
                          </div>
                        </div>
                        <div className="col-2 d-flex">
                          <div className="cell">
                            <span>작업종료일</span>
                            <h5>-</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="board-bottom">
                      <ul className="process-list">
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:12:14</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(1),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립1공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:34:51</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(2),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립2공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:05:32</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(3),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립3공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:43:11</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(4),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립4공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:25:56</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(5),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립5공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:45:06</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(6),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립6공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:30:26</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(7),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:18:35</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(8),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>공정품질 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:10:46</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(9),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>양산 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:05:23</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(10),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>출하 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:14:44</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(11),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>출하</span>
                          </li>
                        </OverlayTrigger>
                      </ul>
                    </div>
                  </div>
                </div>
              </Collapse>
            </div>
            <div
              className={classNames('accordion__item mb-4 state-cmp', {
                open: isOpenProcessStep2,
              })}
            >
              <div className="accordion__toggle">
                <div className="row w-100 p-3">
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>S/N</span>
                      <em>5N2218-00028VP</em>
                    </div>
                  </div>
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>프로젝트명</span>
                      <em>DOOSAN ESM LPG</em>
                    </div>
                  </div>
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>L/T</span>
                      <em>
                        {processCheckList.length > 0 &&
                          moment
                            .utc(
                              (moment.duration('09:24:15').asSeconds() /
                                (11 - processCheckList.length === 0
                                  ? 1
                                  : 11 - processCheckList.length)) *
                                1000
                            )
                            .format('HH:mm:ss')}
                        {processCheckList.length === 0 && '09:24:15'}
                      </em>
                    </div>
                  </div>
                  <div className="col-3 d-flex">
                    <div className="front-info">
                      <span>공정률</span>
                      <em>100%</em>
                      <div
                        className="progress"
                        style={{
                          height: '18px',
                        }}
                      >
                        <div
                          className="progress-bar pg-bar-success"
                          role="progressbar"
                          style={{
                            width: '100%',
                          }}
                          aria-valuenow={100}
                          aria-valuemin={0}
                          aria-valuemax={100}
                        >
                          100%
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="d-flex h-100 align-items-center ml-auto px-3 circle-pin"
                  onClick={() => setOpenProcessStep2(!isOpenProcessStep2)}
                >
                  <span className="accordion__toggle-icon material-icons">
                    keyboard_arrow_down
                  </span>
                </a>
              </div>
              <Collapse in={isOpenProcessStep2}>
                <div className="accordion__menu bg-light border-top-1">
                  <div className="process-board">
                    <div className="board-top">
                      <div className="row w-100 p-3">
                        <div className="col-3 d-flex">
                          <div className="cell">
                            <span>프로젝트명</span>
                            <h5>DOOSAN ESM</h5>
                          </div>
                        </div>
                        <div className="col-3 d-flex">
                          <div className="cell">
                            <span>제품군</span>
                            <h5>Fuel Cell Inverter</h5>
                          </div>
                        </div>
                        <div className="col-2 d-flex">
                          <div className="cell">
                            <span>거래처</span>
                            <h5>DOOSAN</h5>
                          </div>
                        </div>
                        <div className="col-2 d-flex">
                          <div className="cell">
                            <span>작업시작일</span>
                            <h5>2021-10-01</h5>
                          </div>
                        </div>
                        <div className="col-2 d-flex">
                          <div className="cell">
                            <span>작업종료일</span>
                            <h5>-</h5>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="board-bottom">
                      <ul className="process-list">
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:12:14</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(1),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립1공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:34:51</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(2),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립2공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:05:32</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(3),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립3공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:43:11</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(4),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립4공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:25:56</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(5),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립5공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:45:06</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(6),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립6공정</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:30:26</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(7),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>조립 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:18:35</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(8),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>공정품질 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:10:46</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(9),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>양산 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:05:23</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(10),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>출하 검사</span>
                          </li>
                        </OverlayTrigger>
                        <OverlayTrigger
                          placement={'top'}
                          overlay={<Tooltip id={'list1'}>L/T 00:14:44</Tooltip>}
                        >
                          <li
                            className={classNames({
                              opacity:
                                processCheckList.length > 0 &&
                                !processCheckList.includes(11),
                            })}
                          >
                            <div className="pc-circle state-cmp"></div>
                            <span>출하</span>
                          </li>
                        </OverlayTrigger>
                      </ul>
                    </div>
                  </div>
                </div>
              </Collapse>
            </div>
          </>
        )}
      </div>
    </>
  );
}

export default FloatProcessCompleted;
