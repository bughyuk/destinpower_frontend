import React, { useEffect, useState } from 'react';
import {
  PANE_STATUS_CATEGORY,
  PANE_STATUS_LIST,
  PaneStatus,
  SESSION_STORAGE_KEY_DIRECT_PROCESS_MANAGEMENT_CATEGORY,
} from '@/utils/constants/common';
import {
  useActiveMenu,
  useFloatPane,
  useLeftPaneContainer,
  useRightPane,
} from '@/modules/setup/hook';
import { useControlProject, useControlSpace } from '@/modules/map/hook';
import {
  FLOAT_PANE_COMPLETED_PROCESS,
  FLOAT_PANE_PROCESS_CHECK,
  FLOAT_PANE_PROCESS_CHECK_LIST_MANAGEMENT,
  RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS,
} from '@/modules/setup/types';
import { fetchDistributionStatus } from '@/api/physical_distribution';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionStatus,
} from '@/modules/physical_distribution/hook';
import ProcessManagementCategory from '@/components/ProcessManagementCategory';
import ProcessManagementList from '@/components/ProcessManagementList';
import {
  Category,
  PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT,
  PROCESS_CATEGORY_COMPLETED_PROCESS,
  PROCESS_CATEGORY_PROCESS_CHECK,
} from '@/modules/physical_distribution/types';

function ProcessManagementPane() {
  return (
    <div className="tab-pane sm-distribution active">
      <div className="distribution-inner">
        <ProcessManagementContent />
      </div>
    </div>
  );
}

function ProcessManagementContent() {
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_CATEGORY);
  const { project } = useControlProject();
  const { space } = useControlSpace();
  const { updateScroll } = useLeftPaneContainer();
  useEffect(() => {
    setStatus(PANE_STATUS_CATEGORY);
  }, [space]);

  const { handleChangeShow: handleChangeFloatPaneShow } = useFloatPane();
  const { handleChangeShow: handleChangeRightPaneShow } = useRightPane();
  const { menuIdx } = useActiveMenu();
  const {
    categoryIdx,
    handleSetCategoryIdx,
  } = usePhysicalDistributionCategory();
  useEffect(() => {
    updateScroll();
    if (status !== PANE_STATUS_CATEGORY) {
      // handleChangeFloatPaneShow(true, FLOAT_PANE_PHYSICAL_DISTRIBUTION);
    } else {
      handleChangeFloatPaneShow(false);
    }
  }, [status, categoryIdx]);
  const {
    handleSetDistributionStatus,
    reloadFlag,
  } = usePhysicalDistributionStatus();

  useEffect(() => {
    handleChangeRightPaneShow(
      true,
      RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS
    );

    const item = sessionStorage.getItem(
      SESSION_STORAGE_KEY_DIRECT_PROCESS_MANAGEMENT_CATEGORY
    );
    if (item) {
      handleSetCategoryIdx(item as Category);
      sessionStorage.removeItem(
        SESSION_STORAGE_KEY_DIRECT_PROCESS_MANAGEMENT_CATEGORY
      );
    }

    return () => {
      handleChangeFloatPaneShow(false);
      handleChangeRightPaneShow(false);
      handleSetCategoryIdx(undefined);
    };
  }, []);

  useEffect(() => {
    if (typeof categoryIdx !== 'undefined') {
      setStatus(PANE_STATUS_LIST);
      if (categoryIdx === PROCESS_CATEGORY_PROCESS_CHECK) {
        handleChangeFloatPaneShow(true, FLOAT_PANE_PROCESS_CHECK);
      } else if (categoryIdx === PROCESS_CATEGORY_COMPLETED_PROCESS) {
        handleChangeFloatPaneShow(true, FLOAT_PANE_COMPLETED_PROCESS);
      } else if (categoryIdx === PROCESS_CATEGORY_CHECK_LIST_MANAGEMENT) {
        handleChangeFloatPaneShow(
          true,
          FLOAT_PANE_PROCESS_CHECK_LIST_MANAGEMENT
        );
      } else {
        handleChangeFloatPaneShow(false);
      }
    }
  }, [categoryIdx]);

  useEffect(() => {
    setStatus(PANE_STATUS_CATEGORY);
  }, [menuIdx]);

  useEffect(() => {
    handleFetchDistributionStatus();
  }, [reloadFlag]);

  const handleFetchDistributionStatus = async () => {
    const data = await fetchDistributionStatus(project.id);
    if (data) {
      handleSetDistributionStatus(data);
    }
  };

  return (
    <>
      {status === PANE_STATUS_CATEGORY && <ProcessManagementCategory />}
      {status === PANE_STATUS_LIST && (
        <ProcessManagementList onChangeStatus={setStatus} />
      )}
    </>
  );
}

export default ProcessManagementPane;
