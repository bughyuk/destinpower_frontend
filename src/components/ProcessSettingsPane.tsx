import React, { useEffect, useState } from 'react';
import {
  PANE_STATUS_CATEGORY,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import {
  useActiveMenu,
  useFloatPane,
  useLeftPaneContainer,
  useRightPane,
} from '@/modules/setup/hook';
import PhysicalDistributionList from '@/components/PhysicalDistributionList';
import { useControlProject, useControlSpace } from '@/modules/map/hook';
import {
  FLOAT_PANE_PHYSICAL_DISTRIBUTION,
  RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS,
} from '@/modules/setup/types';
import { fetchDistributionStatus } from '@/api/physical_distribution';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionStatus,
} from '@/modules/physical_distribution/hook';
import ProcessSettingsCategory from '@/components/ProcessSettingsCategory';
import ProcessSettingsList from '@/components/ProcessSettingsList';

function ProcessSettingsPane() {
  return (
    <div className="tab-pane sm-distribution active">
      <div className="distribution-inner">
        <ProcessSettingsContent />
      </div>
    </div>
  );
}

function ProcessSettingsContent() {
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_CATEGORY);
  const { project } = useControlProject();
  const { space } = useControlSpace();
  const { updateScroll } = useLeftPaneContainer();
  useEffect(() => {
    setStatus(PANE_STATUS_CATEGORY);
  }, [space]);

  const { handleChangeShow: handleChangeFloatPaneShow } = useFloatPane();
  const { handleChangeShow: handleChangeRightPaneShow } = useRightPane();
  const { menuIdx } = useActiveMenu();
  const {
    categoryIdx,
    handleSetCategoryIdx,
  } = usePhysicalDistributionCategory();
  useEffect(() => {
    updateScroll();
    if (status !== PANE_STATUS_CATEGORY) {
      // handleChangeFloatPaneShow(true, FLOAT_PANE_PHYSICAL_DISTRIBUTION);
    } else {
      handleChangeFloatPaneShow(false);
    }
  }, [status, categoryIdx]);
  const {
    handleSetDistributionStatus,
    reloadFlag,
  } = usePhysicalDistributionStatus();

  useEffect(() => {
    handleChangeRightPaneShow(
      true,
      RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS
    );

    return () => {
      handleChangeFloatPaneShow(false);
      handleChangeRightPaneShow(false);
      handleSetCategoryIdx(undefined);
    };
  }, []);

  useEffect(() => {
    if (typeof categoryIdx !== 'undefined') {
      setStatus(PANE_STATUS_LIST);
    }
  }, [categoryIdx]);

  useEffect(() => {
    setStatus(PANE_STATUS_CATEGORY);
  }, [menuIdx]);

  useEffect(() => {
    handleFetchDistributionStatus();
  }, [reloadFlag]);

  const handleFetchDistributionStatus = async () => {
    const data = await fetchDistributionStatus(project.id);
    if (data) {
      handleSetDistributionStatus(data);
    }
  };

  return (
    <>
      {status === PANE_STATUS_CATEGORY && <ProcessSettingsCategory />}
      {status === PANE_STATUS_LIST && (
        <ProcessSettingsList onChangeStatus={setStatus} />
      )}
    </>
  );
}

export default ProcessSettingsPane;
