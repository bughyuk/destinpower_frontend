import React, { ReactElement, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDropdown } from '@/modules/common';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import MaterialIcon from '@/components/MaterialIcon';
import { Dropdown } from 'react-bootstrap';
import {
  FUNCTION_SCOPE_AREA,
  FUNCTION_SCOPE_CIRCLE,
  FUNCTION_SCOPE_POLYGON,
  GEOMETRY_TARGET_TYPE_INNER,
  GEOMETRY_TARGET_TYPE_OUTER,
  GEOMETRY_TYPE_CIRCLE,
  GEOMETRY_TYPE_POLYGON,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import {
  useAdditionalLocation,
  useControlEvent,
  useControlProject,
  useControlSpace,
  useOpenLayers,
} from '@/modules/map/hook';
import { CommonUtils } from '@/utils';
import FormLabel from '@/components/FormLabel';
import FormGroup from '@/components/FormGroup';
import Flatpickr from 'react-flatpickr';
import ReactQuill from 'react-quill';
import ToggleButton from '@/components/ToggleButton';
import moment from 'moment';
import CompletePane from '@/components/CompletePane';
import { useLeftPaneContainer, useSetupEventsFlag } from '@/modules/setup/hook';
import { postEvent, RequestPostEvent } from '@/api/event';
import { postImage } from '@/api/common';
import InvalidAlert from '@/components/InvalidAlert';

type EventRegisterProps = {
  onStatusChange: (status: PaneStatus) => void;
};

type EventRegisterData = {
  title: string;
  period: string[];
  subject: string;
  content: string;
  imgId: string;
  activation: boolean;
};

const today = moment().format('YYYY-MM-DD');
const specificDay = moment().add(7, 'days').format('YYYY-MM-DD');
function EventRegister({ onStatusChange }: EventRegisterProps) {
  const { project } = useControlProject();
  const { space } = useControlSpace();
  const { handleReloadActivationEvents } = useControlEvent();
  const { map, draw } = useOpenLayers();
  const { locationData, handleResetLocation } = useAdditionalLocation();
  const { updateScroll } = useLeftPaneContainer();
  const { handleChangeClick } = useSetupEventsFlag();
  const [done, setDone] = useState(false);
  const [isValid, setValid] = useState(false);
  const [isRegisterPossible, setRegisterPossible] = useState(false);
  const [inputs, setInputs] = useState<EventRegisterData>({
    title: '',
    period: [today, specificDay],
    subject: '',
    content: '',
    imgId: '',
    activation: true,
  });

  useEffect(() => {
    if (!CommonUtils.isEmptyObject(locationData)) {
      setRegisterPossible(true);
    } else {
      setRegisterPossible(false);
    }
  }, [locationData]);

  useEffect(() => {
    handleChangeClick(false);
    return () => {
      handleChangeClick(true);
      draw.source?.clear();
      handleResetLocation();
    };
  }, []);

  useEffect(() => {
    return () => {
      if (draw.interaction) {
        map?.removeInteraction(draw.interaction);
      }
    };
  }, [draw.interaction]);

  const handleRegisterDone = async () => {
    const requestPostEvent: RequestPostEvent = {
      startDate: moment(inputs.period[0]).format('yyyy-MM-DDTHH:mm:ss'),
      endDate: moment(inputs.period[1]).format('yyyy-MM-DDTHH:mm:ss'),
      activeFlag: inputs.activation,
      eventTitle: inputs.title,
      eventSubject: inputs.subject,
      eventContent: inputs.content,
      projectId: project.id,
      targetMappingId: space.spaceMappingId,
      targetMapId: space.floorsMapId,
      imgId: inputs.imgId,
    };

    if (locationData.geom) {
      requestPostEvent.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPostEvent.geomType = GEOMETRY_TYPE_POLYGON;
      requestPostEvent.geom = locationData.geom;
    }

    if (locationData.lng && locationData.lat && locationData.radius) {
      requestPostEvent.targetGeomType = GEOMETRY_TARGET_TYPE_INNER;
      requestPostEvent.geomType = GEOMETRY_TYPE_CIRCLE;
      requestPostEvent.lng = locationData.lng;
      requestPostEvent.lat = locationData.lat;
      requestPostEvent.radius = locationData.radius;
    }

    if (locationData.areaId) {
      requestPostEvent.targetGeomType = GEOMETRY_TARGET_TYPE_OUTER;
      requestPostEvent.outerKey = locationData.areaId;
    }

    const data = await postEvent(requestPostEvent);
    if (data) {
      draw.source?.clear();
      if (inputs.activation) {
        handleReloadActivationEvents();
      }
      setDone(true);
    }
  };

  useEffect(() => {
    if (
      inputs.title &&
      inputs.period.length > 1 &&
      inputs.subject &&
      inputs.content
    ) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [inputs]);

  useEffect(() => {
    updateScroll();
  }, [done]);

  if (done) {
    return (
      <CompletePane
        backBtnTextKey={'text_event_list'}
        onBackBtnClick={() => onStatusChange(PANE_STATUS_LIST)}
      />
    );
  }

  let content: ReactElement;
  if (!CommonUtils.isEmptyObject(locationData)) {
    content = <Body data={inputs} onChangeData={setInputs} />;
  } else {
    content = <Guide />;
  }

  return (
    <>
      <Header />
      <div className="container-fluid">{content}</div>
      <Footer
        registerPossible={isRegisterPossible}
        onConfirmBtnClick={handleRegisterDone}
        onCancelBtnClick={onStatusChange}
        valid={isValid}
      />
    </>
  );
}

function Header() {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const { handleResetLocation } = useAdditionalLocation();
  const {
    handleSelectArea,
    handleDrawCircle,
    handleDrawPolygon,
  } = useOpenLayers();

  const handleFunctionSelect = (eventKey: string | null) => {
    handleResetLocation();
    switch (eventKey) {
      case FUNCTION_SCOPE_AREA:
        handleSelectArea();
        break;
      case FUNCTION_SCOPE_CIRCLE:
        handleDrawCircle();
        break;
      case FUNCTION_SCOPE_POLYGON:
        handleDrawPolygon();
        break;
    }
  };

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{t('text_event_registration')}</h3>
        </div>
      </div>
      <div className="btn-group">
        <Dropdown onToggle={handleToggle} onSelect={handleFunctionSelect}>
          <DropdownToggle as={'a'} className="btn btn-outline-accent">
            {t('text_selection_function')}
          </DropdownToggle>
          <DropdownMenu align={'right'} ref={dropdown}>
            <Dropdown.Header>
              <strong>{t('text_specify_range')}</strong>
            </Dropdown.Header>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_AREA}>
              <MaterialIcon name={'highlight_alt'} />{' '}
              {t('text_selection_structure')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_CIRCLE}>
              <MaterialIcon name={'adjust'} /> {t('text_selection_range')}
            </Dropdown.Item>
            <Dropdown.Item eventKey={FUNCTION_SCOPE_POLYGON}>
              <MaterialIcon name={'timeline'} /> {t('text_draw_line')}
            </Dropdown.Item>
          </DropdownMenu>
        </Dropdown>
      </div>
    </div>
  );
}

function Guide() {
  const { t } = useTranslation();
  return <em className="none-list">{t('msg_selection_function')}</em>;
}

type BodyProps = {
  data: EventRegisterData;
  onChangeData: (data: EventRegisterData) => void;
};

function Body({ data, onChangeData }: BodyProps) {
  const { t } = useTranslation();
  const fileInput = useRef<HTMLInputElement>(null);

  useEffect(() => {
    return () =>
      onChangeData({
        ...data,
        imgId: '',
      });
  }, []);

  const handlePostImage = async (file: File) => {
    const imgId = await postImage(file);

    onChangeData({
      ...data,
      imgId,
    });
  };

  const handleCancelImage = () => {
    const current = fileInput.current;
    if (current) {
      current.value = '';
    }

    onChangeData({
      ...data,
      imgId: '',
    });
  };

  return (
    <>
      <FormGroup>
        <FormLabel
          textKey={'text_event_name'}
          className={'mb-0'}
          htmlFor={'title'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="title"
          value={data.title}
          onChange={(e) =>
            onChangeData({
              ...data,
              [e.target.id]: e.target.value,
            })
          }
          placeholder={t('place_holder_event_name')}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_period'}
          className={'mb-0'}
          htmlFor={'period'}
          essential={true}
        />
        <Flatpickr
          className="form-control"
          options={{
            mode: 'range',
          }}
          value={data.period}
          onChange={(dates) =>
            onChangeData({
              ...data,
              period: dates.map((date) => moment(date).format('YYYY-MM-DD')),
            })
          }
          id="period"
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_title'}
          className={'mb-0'}
          essential={true}
          htmlFor={'subject'}
        />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_title')}
          id={'subject'}
          value={data.subject}
          onChange={(e) =>
            onChangeData({
              ...data,
              [e.target.id]: e.target.value,
            })
          }
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_content'}
          essential={true}
          htmlFor={'content'}
        />
        <div style={{ height: '192px' }}>
          <ReactQuill
            id="content"
            style={{ height: '150px' }}
            placeholder={t('place_holder_content')}
            modules={{
              toolbar: [
                ['bold', 'italic'],
                ['link', 'blockquote', 'code', 'image'],
                [{ list: 'ordered' }, { list: 'bullet' }],
              ],
            }}
            value={data.content}
            onChange={(content, delta, source, editor) => {
              if (!editor.getText().trim().length) {
                content = '';
              }

              onChangeData({
                ...data,
                content,
              });
            }}
          />
        </div>
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_file_registration'} />
        <input
          className="form-control"
          type="file"
          accept={'image/*'}
          onChange={(e) => {
            if (e.target.files && e.target.files.length) {
              handlePostImage(e.target.files[0]);
            } else {
              onChangeData({
                ...data,
                imgId: '',
              });
            }
          }}
          ref={fileInput}
        />
        {data.imgId && (
          <a className="file-del" onClick={handleCancelImage}>
            <MaterialIcon name={'close'} />
          </a>
        )}
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_set_up'} className={'mb-0'} />
        <div className="set-list">
          <FormLabel
            textKey={'text_whether_of_activation'}
            className={'mb-0'}
            fontSizeApply={false}
          />
          <div className="control-cover">
            <ToggleButton
              id="activation"
              className={'ml-8pt'}
              checked={data.activation}
              onChange={(id, checked) =>
                onChangeData({
                  ...data,
                  [id]: checked,
                })
              }
            />
          </div>
        </div>
      </FormGroup>
    </>
  );
}

type FooterProps = {
  onCancelBtnClick?: (status: PaneStatus) => void;
  onConfirmBtnClick?: () => void;
  registerPossible?: boolean;
  valid: boolean;
};

function Footer({
  onConfirmBtnClick,
  onCancelBtnClick,
  registerPossible = false,
  valid,
}: FooterProps) {
  const { t } = useTranslation();
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (!registerPossible || valid) {
      setShowInvalidMessage(false);
    }
  }, [registerPossible, valid]);

  return (
    <>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            onClick={() => {
              onCancelBtnClick?.call(null, PANE_STATUS_LIST);
            }}
            className="btn btn-outline-secondary mr-8pt"
          >
            {t('text_to_cancel')}
          </a>
          {registerPossible && (
            <a
              className={'btn btn-outline-accent ml-0'}
              onClick={() => {
                if (valid) {
                  setShowInvalidMessage(false);
                  onConfirmBtnClick?.call(null);
                } else {
                  setShowInvalidMessage(true);
                }
              }}
            >
              {t('text_registration')}
            </a>
          )}
        </div>
      </div>
    </>
  );
}

export default EventRegister;
