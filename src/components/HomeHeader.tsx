import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import ListSearch from '@/components/ListSearch';
import { useUser } from '@/modules/user/hook';

type HomeHeaderProps = {
  titleKey: string;
  registerButtonTextKey: string;
  count: number;
  descriptionTextKey: string;
  onSubmitSearch: (searchKeyword: string) => void;
  onClickRegisterButton: () => void;
};

function HomeHeader({
  titleKey,
  registerButtonTextKey,
  count,
  descriptionTextKey,
  onSubmitSearch,
  onClickRegisterButton,
}: HomeHeaderProps) {
  const { t } = useTranslation();
  const { user } = useUser();

  const handleSubmitSearch = (text: string) => {
    onSubmitSearch(text);
  };

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="mb-0">{t(titleKey)}</h3>
          <small className="text-muted text-headings text-uppercase">
            <strong className="text-accent">{count + t('text_count')}</strong>
            {t(descriptionTextKey)}
          </small>
        </div>
        <ListSearch onSubmit={handleSubmitSearch} />
        {user.userType === 'OWNER' && (
          <a
            className="btn btn-accent btn-rounded ml-4"
            onClick={onClickRegisterButton}
          >
            {t(registerButtonTextKey)}
          </a>
        )}
      </div>
    </>
  );
}

export default HomeHeader;
