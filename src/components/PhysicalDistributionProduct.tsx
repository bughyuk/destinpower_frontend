import React, { ReactElement, useEffect } from 'react';
import PhysicalDistributionProductList from '@/components/PhysicalDistributionProductList';
import { PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT } from '@/modules/physical_distribution/types';
import PhysicalDistributionProductRegister from '@/components/PhysicalDistributionProductRegister';
import PhysicalDistributionProductEdit from '@/components/PhysicalDistributionProductEdit';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER,
  PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import { useTranslation } from 'react-i18next';
import { usePhysicalDistributionProduct } from '@/modules/physical_distribution/hook';
import { useLeftPaneContainer } from '@/modules/setup/hook';

function PhysicalDistributionProduct({
  categoryIdx,
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  const { t } = useTranslation();
  const {
    paneStatus,
    handleChangePaneStatus,
    handleSetProduct,
  } = usePhysicalDistributionProduct();
  const { updateScroll } = useLeftPaneContainer();

  useEffect(() => {
    return () => {
      handleChangePaneStatus(PANE_STATUS_LIST);
      handleSetProduct(null);
    };
  }, []);

  useEffect(() => {
    updateScroll();
  }, [paneStatus]);

  let content: ReactElement = <></>;
  if (paneStatus === PANE_STATUS_LIST) {
    content = <PhysicalDistributionProductList />;
  } else if (
    paneStatus === PANE_STATUS_REGISTER ||
    paneStatus ===
      PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER
  ) {
    content = <PhysicalDistributionProductRegister />;
  } else if (
    paneStatus === PANE_STATUS_EDIT ||
    paneStatus ===
      PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER
  ) {
    content = <PhysicalDistributionProductEdit />;
  }

  let displayTitle = title;
  if (paneStatus === PANE_STATUS_REGISTER) {
    displayTitle = t('text_add_product');
  } else if (paneStatus === PANE_STATUS_EDIT) {
    displayTitle = t('text_edit_product');
  } else if (
    paneStatus ===
      PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER ||
    paneStatus ===
      PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER
  ) {
    displayTitle = t('text_add_item');
  }

  const handleClickBack = () => {
    if (paneStatus === PANE_STATUS_LIST) {
      onClickBack?.call(null);
    } else if (
      paneStatus ===
      PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER
    ) {
      handleChangePaneStatus(PANE_STATUS_REGISTER);
    } else if (
      paneStatus ===
      PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER
    ) {
      handleChangePaneStatus(PANE_STATUS_EDIT);
    } else {
      handleChangePaneStatus(PANE_STATUS_LIST);
    }
  };

  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={handleClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{displayTitle}</h3>
          </div>
          {categoryIdx === PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT &&
            paneStatus === PANE_STATUS_LIST && (
              <a
                className="btn btn-outline-dark ml-auto"
                onClick={() => handleChangePaneStatus(PANE_STATUS_REGISTER)}
              >
                {t('text_add_product')}
              </a>
            )}
        </div>
      </div>
      {content}
    </>
  );
}

export default PhysicalDistributionProduct;
