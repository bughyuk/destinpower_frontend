import React from 'react';
import { Nav, TabContainer, TabContent, TabPane } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import ProjectPublicSpaceTab from '@/components/ProjectPublicSpaceTab';
import ProjectCustomSpaceTab from '@/components/ProjectCustomSpaceTab';

function ProjectSpaceTab() {
  return (
    <>
      <TabContainer defaultActiveKey={'custom'} transition={false}>
        <SpaceNav />
        <TabContent className="mt-4 text-70">
          <TabPane eventKey={'custom'}>
            <ProjectCustomSpaceTab />
          </TabPane>
          <TabPane eventKey={'public'}>
            <ProjectPublicSpaceTab />
          </TabPane>
        </TabContent>
      </TabContainer>
    </>
  );
}

function SpaceNav() {
  const { t } = useTranslation();

  return (
    <div className="tab-cover">
      <Nav as={'ul'}>
        <Nav.Item as={'li'}>
          <Nav.Link eventKey={'custom'}>
            {t('text_buildings_user_registration')}
          </Nav.Link>
        </Nav.Item>
        {/*
        <Nav.Item as={'li'}>
          <Nav.Link eventKey={'public'}>{t('text_public_map')}</Nav.Link>
        </Nav.Item>
        */}
      </Nav>
    </div>
  );
}

export default ProjectSpaceTab;
