import React, { useEffect, useState } from 'react';
import { PaneProps } from '@/modules/common';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_EDIT } from '@/utils/constants/common';
import { Collapse } from 'react-bootstrap';
import classNames from 'classnames';
import { deleteItem, fetchItems } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import { Item } from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';

type PhysicalDistributionItemListProps = PaneProps & {
  onChangeItem: (item: Item) => void;
};

function PhysicalDistributionItemList({
  onChangeItem,
  onChangeStatus,
}: PhysicalDistributionItemListProps) {
  const { t } = useTranslation();
  const [searchKeyword, setSearchKeyword] = useState('');
  const { project } = useControlProject();
  const [load, setLoad] = useState(false);
  const [itemList, setItemList] = useState<Item[]>([]);
  const [filterList, setFilterList] = useState<Item[]>([]);
  const [activeId, setActiveId] = useState('');

  useEffect(() => {
    handleFetchItemList();
  }, []);

  useEffect(() => {
    setActiveId('');
    setFilterList(
      itemList.filter(
        (item) =>
          item.productCategoryName.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, itemList]);

  const handleFetchItemList = async () => {
    setLoad(false);
    const data = await fetchItems(project.id);
    setItemList(data);
    setLoad(true);
  };

  const handleClickEdit = (item: Item) => {
    onChangeItem(item);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteItem = async (itemId: string) => {
    await deleteItem(itemId);
    handleFetchItemList();
  };

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'place_holder_search_item'}
          onSubmit={setSearchKeyword}
        />
      </div>
      <div className="list-wide-cover">
        <div className="mb-4">
          {!load && <></>}
          {load && filterList.length === 0 && (
            <em className="none-list mb-4">{t('msg_item_empty')}</em>
          )}
          {load && filterList.length > 0 && (
            <ul className="sidebar-menu">
              {filterList.map((item) => (
                <ItemRow
                  key={item.productCategoryId}
                  {...item}
                  activeId={activeId}
                  onClick={(itemId) => {
                    let value = '';
                    if (itemId !== activeId) {
                      value = itemId;
                    }
                    setActiveId(value);
                  }}
                  onClickEdit={() => handleClickEdit(item)}
                  onClickDelete={handleDeleteItem}
                />
              ))}
            </ul>
          )}
        </div>
      </div>
    </>
  );
}

type ItemRowProps = Item & {
  activeId: string;
  onClick: (productCategoryId: string) => void;
  onClickEdit: () => void;
  onClickDelete: (itemId: string) => void;
};

function ItemRow({
  productCategoryId,
  productCategoryName,
  maker,
  productCodePrefix,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
  deleteFlag,
}: ItemRowProps) {
  const { t } = useTranslation();
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  return (
    <>
      <li
        className={classNames('sidebar-menu-item', {
          open: productCategoryId === activeId,
        })}
      >
        <div className="d-flex flex-row">
          <a
            className={classNames('sidebar-menu-button pl-3', {
              'text-30': deleteFlag,
            })}
            onClick={() => onClick(productCategoryId)}
          >
            <div className="title">
              {deleteFlag && `[${t('text_delete')}] `} {productCategoryName}
            </div>
            <div className="d-flex ml-auto">
              <span className="sidebar-menu-toggle-icon"></span>
            </div>
          </a>
        </div>
        <Collapse in={productCategoryId === activeId}>
          <div className="sidebar-submenu sm-indent">
            <div className="cover">
              <div className="col-6 p-2">
                <span className="text-50">{t('text_item_title')}</span>
                <p className="m-0">{productCategoryName}</p>
              </div>
              <div className="col-6 p-2">
                <span className="text-50">{t('text_identifier')}</span>
                <p className="m-0">{productCodePrefix}</p>
              </div>
              <div className="col-6 p-2">
                <span className="text-50">{t('text_item_maker')}</span>
                <p className="m-0">{maker}</p>
              </div>
              {!deleteFlag && (
                <div className="btn-cover mt-2">
                  <a className="btn" onClick={() => setShowConfirmModal(true)}>
                    {t('text_delete')}
                  </a>
                  <a className="btn" onClick={() => onClickEdit()}>
                    {t('text_edit')}
                  </a>
                </div>
              )}
            </div>
          </div>
        </Collapse>
      </li>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(productCategoryId)}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

export default PhysicalDistributionItemList;
