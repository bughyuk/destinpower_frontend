import React from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { PeriodIndicator } from '@/components/HistoryUserContent';
import moment from 'moment';
import { useSetupData } from '@/modules/setup/hook';

type HistoryUserPeriodProps = {
  period: PeriodIndicator;
  onChangePeriod: (period: PeriodIndicator) => void;
};

function HistoryUserPeriod({ period, onChangePeriod }: HistoryUserPeriodProps) {
  const { t } = useTranslation();
  const { handleSetPeriodFilter } = useSetupData();

  const handleChangePeriod = (period: PeriodIndicator) => {
    const format = 'YYYY-MM-DD';

    const today = moment().format(format);
    let agoMoment: moment.Moment;

    switch (period) {
      case '1D':
        agoMoment = moment().subtract('1', 'days');
        break;
      case '1W':
        agoMoment = moment().subtract('1', 'weeks');
        break;
      case '1M':
        agoMoment = moment().subtract('1', 'months');
        break;
      case '3M':
        agoMoment = moment().subtract('3', 'months');
        break;
      case '6M':
        agoMoment = moment().subtract('6', 'months');
        break;
    }

    const ago = agoMoment.format(format);
    handleSetPeriodFilter([ago, today]);
    onChangePeriod(period);
  };

  return (
    <div className="item-sort">
      <a
        className={classNames('items', {
          active: period === '1D',
        })}
        onClick={() => handleChangePeriod('1D')}
      >
        {t('text_1_day_ago')}
      </a>
      <a
        className={classNames('items', {
          active: period === '1W',
        })}
        onClick={() => handleChangePeriod('1W')}
      >
        {t('text_1_week_ago')}
      </a>
      <a
        className={classNames('items', {
          active: period === '1M',
        })}
        onClick={() => handleChangePeriod('1M')}
      >
        {t('text_1_month_ago')}
      </a>
      <a
        className={classNames('items', {
          active: period === '3M',
        })}
        onClick={() => handleChangePeriod('3M')}
      >
        {t('text_3_month_ago')}
      </a>
      <a
        className={classNames('items', {
          active: period === '6M',
        })}
        onClick={() => handleChangePeriod('6M')}
      >
        {t('text_6_month_ago')}
      </a>
    </div>
  );
}

export default HistoryUserPeriod;
