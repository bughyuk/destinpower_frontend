import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import InputNumber from '@/components/InputNumber';
import { useControlProject } from '@/modules/map/hook';
import {
  usePhysicalDistributionProduct,
  usePhysicalDistributionStatus,
  usePhysicalDistributionWarehouse,
} from '@/modules/physical_distribution/hook';
import {
  MovementProduct,
  ReleaseProduct,
  Warehouse,
} from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';
import { CommonUtils } from '@/utils';
import AlertModal from '@/components/AlertModal';
import NewlineText from '@/components/NewlineText';
import { postMovement } from '@/api/physical_distribution';
import classNames from 'classnames';

function FloatPhysicalDistributionMovement() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const {
    load: loadWarehouseList,
    warehouseList,
  } = usePhysicalDistributionWarehouse();
  const {
    selectedProduct,
    handleSetProduct,
    handleChangeReloadFlag: handleChangeProductReloadFlag,
  } = usePhysicalDistributionProduct();
  const {
    handleChangeReloadFlag: handleChangeStatusReloadFlag,
  } = usePhysicalDistributionStatus();
  const [inputs, setInputs] = useState<{
    boxCategoryId: string;
    warehouseName: string;
    warehouseId: string;
    usableQuantity: number;
  }>({
    boxCategoryId: '',
    warehouseName: '',
    warehouseId: '',
    usableQuantity: 0,
  });
  const [productList, setProductList] = useState<MovementProduct[]>([]);
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [showAlertModal, setShowAlertModal] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  useEffect(() => {
    if (selectedProduct && warehouseList.length) {
      const selectedReleaseProduct = selectedProduct as ReleaseProduct;
      const filterWarehouseList = warehouseList.filter(
        (warehouse) =>
          warehouse.warehouseId !== selectedReleaseProduct.warehouseId
      );
      handleSetProduct(null);
      let message = '';
      if (
        productList.find(
          (product) =>
            product.warehouseId === selectedReleaseProduct.warehouseId &&
            product.boxCategoryId === selectedReleaseProduct.boxCategoryId
        )
      ) {
        message = t('msg_already_been_added_product');
      } else if (productList.length) {
        message = t('msg_can_be_one_product_movement');
      }

      if (message) {
        handleShowAlertModal(message);
        return;
      }

      setInputs({
        ...inputs,
        boxCategoryId: selectedReleaseProduct.boxCategoryId,
        warehouseId: selectedReleaseProduct.warehouseId,
        warehouseName: selectedReleaseProduct.warehouseName,
        usableQuantity: selectedReleaseProduct.usableQuantity,
      });

      let targetWarehouseId = '';
      let targetWarehouseName = '';

      if (filterWarehouseList.length) {
        const firstWarehouse = filterWarehouseList[0];
        targetWarehouseId = firstWarehouse.warehouseId;
        targetWarehouseName = firstWarehouse.warehouseName;
      }

      setProductList([
        ...productList,
        {
          ...selectedReleaseProduct,
          targetWarehouseName,
          targetWarehouseId,
        },
      ]);
    }
  }, [selectedProduct, warehouseList]);

  const handleShowAlertModal = (message: string) => {
    setAlertMessage(message);
    setShowAlertModal(true);
  };

  const handleChangeValue = (
    changeIndex: number,
    field: 'logisticsQuantity' | 'targetWarehouseId' | 'targetWarehouseName',
    value: string | number
  ) => {
    setProductList(
      productList.map((product, index) => {
        if (changeIndex === index) {
          switch (field) {
            case 'logisticsQuantity':
              product[field] = Number(value);
              break;
            case 'targetWarehouseId':
              product[field] = String(value);
              break;
            case 'targetWarehouseName':
              product[field] = String(value);
              break;
          }
        }

        return product;
      })
    );
  };

  const handleClickRemove = (clickedIndex: number) => {
    setProductList(
      productList.filter((product, index) => index !== clickedIndex)
    );
  };

  const handleClickInstruction = () => {
    if (productList.length) {
      let totalLogisticsQuantity = 0;
      let totalTargetWarehouseId = '';
      productList.forEach(({ targetWarehouseId, logisticsQuantity }) => {
        totalLogisticsQuantity += logisticsQuantity;
        totalTargetWarehouseId += targetWarehouseId;
      });
      if (!totalLogisticsQuantity) {
        handleShowAlertModal(
          t('msg_movement_instruction_with_more_than_one_box')
        );
      } else if (!totalTargetWarehouseId) {
        handleShowAlertModal(t('msg_not_exist_warehouse_to_movement'));
      } else {
        setShowConfirmModal(true);
      }
    } else {
      handleShowAlertModal(t('msg_add_product_before_proceeding'));
    }
  };

  const handleSubmit = async () => {
    let totalLogisticsQuantity = 0;
    let totalTargetWarehouseId = '';
    productList.forEach(({ targetWarehouseId, logisticsQuantity }) => {
      totalLogisticsQuantity += logisticsQuantity;
      totalTargetWarehouseId += targetWarehouseId;
    });

    const result = await postMovement({
      projectId: project.id,
      logisticsQuantity: totalLogisticsQuantity * -1,
      warehouseId: inputs.warehouseId,
      boxCategoryId: inputs.boxCategoryId,
      targetWarehouseId: totalTargetWarehouseId,
    });

    if (result) {
      handleChangeStatusReloadFlag();
      handleChangeProductReloadFlag();
      handleInitialInputs();
    }
  };

  const handleInitialInputs = () => {
    setInputs({
      ...inputs,
      boxCategoryId: '',
      warehouseId: '',
      usableQuantity: 0,
    });

    setProductList([]);
  };

  if (!loadWarehouseList) {
    return <></>;
  }

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_movement_document')}
          </h3>
        </div>
        <a
          className="btn btn-rounded btn-outline-dark ml-2"
          onClick={handleClickInstruction}
        >
          <MaterialIcon name={'arrow_downward'} align={'left'} />
          {t('text_movement')}
        </a>
      </div>
      <div className="contents-view">
        <div className="page-separator mt-5">
          <div className="page-separator__text">
            {t('text_movement_information')}
          </div>
        </div>
        {productList.length === 0 && (
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>
            <em className="empty-txt m-0">
              {t('msg_selection_product_from_the_list_on_the_left')}
            </em>
          </div>
        )}
        {productList.length > 0 && (
          <table className="table mb-4 thead-bg-light">
            <colgroup>
              <col width="*" />
              <col width="15%" />
              <col width="15%" />
              <col width="5%" />
            </colgroup>
            <thead>
              <tr>
                <th>{t('text_product_information')}</th>
                <th>{t('text_box_quantity')}</th>
                <th>{t('text_warehouse_to_move')}</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {productList.map((product, i) => (
                <MovementProductItem
                  key={i}
                  {...product}
                  warehouseList={warehouseList}
                  onChangeValue={(field, value) => {
                    handleChangeValue(i, field, value);
                  }}
                  onClickRemove={() => handleClickRemove(i)}
                />
              ))}
            </tbody>
          </table>
        )}
      </div>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={handleSubmit}
      >
        <div className="py-4">
          <div className="text-center">
            <h3>
              <NewlineText
                text={t(
                  'msg_sure_want_to_movement_instruction_in_quantity_boxes',
                  {
                    oldWarehouseName: inputs.warehouseName,
                    oldQuantity: inputs.usableQuantity,
                    newWarehouseName: productList.map(
                      (product) => product.targetWarehouseName
                    ),
                    newQuantity: productList.reduce(
                      (a, b) => +a + b.logisticsQuantity,
                      0
                    ),
                  }
                )}
              />
            </h3>
          </div>
          <div className="d-flex justify-content-center">
            {productList.map(
              ({ productName, imgUrl, productWeight, boxQuantity }, i) => (
                <div
                  key={i}
                  className="media flex-nowrap align-items-center border-1 p-3"
                >
                  <span className="avatar avatar-sm mr-2 blank-img">
                    <img
                      src={imgUrl}
                      className={classNames('avatar-img', {
                        'rounded-circle': imgUrl,
                      })}
                    />
                  </span>
                  <div className="media-body">
                    <strong className="text-dark">{productName}</strong>
                    <div className="text-muted small">
                      {CommonUtils.getProductStandard(
                        productWeight,
                        boxQuantity
                      )}
                    </div>
                  </div>
                </div>
              )
            )}
          </div>
        </div>
      </ConfirmModal>
      <AlertModal show={showAlertModal} onHide={() => setShowAlertModal(false)}>
        <NewlineText text={alertMessage} />
      </AlertModal>
    </>
  );
}

type MovementProductItemProps = ReleaseProduct & {
  warehouseList: Warehouse[];
  onChangeValue: (
    field: 'logisticsQuantity' | 'targetWarehouseId' | 'targetWarehouseName',
    value: string | number
  ) => void;
  onClickRemove: () => void;
};

function MovementProductItem({
  productName,
  imgUrl,
  productSizeName,
  productWeight,
  boxQuantity,
  usableQuantity,
  logisticsQuantity,
  warehouseId,
  warehouseName,
  warehouseList,
  onChangeValue,
  onClickRemove,
}: MovementProductItemProps) {
  const { t } = useTranslation();
  const anotherWarehouseListRef = useRef<Warehouse[]>(
    warehouseList.filter((warehouse) => warehouse.warehouseId !== warehouseId)
  );

  return (
    <tr>
      <td>
        <div className="flex d-flex align-items-center">
          <a className="avatar mr-12pt blank-img">
            <img src={imgUrl} className="avatar-img rounded" />
          </a>
          <div className="flex list-els">
            <h6 className="m-0">{productName}</h6>
            <div className="card-subtitle text-50">
              <small className="mr-2">{productSizeName}</small>
              <small className="mr-2">
                {CommonUtils.getProductStandard(productWeight, boxQuantity)}
              </small>
            </div>
          </div>
        </div>
      </td>
      <td>
        <div className="d-flex align-items-center">
          <span className="d-flex font-weight-bolder">{usableQuantity}</span>
          <span className="material-icons text-30 px-2 font-size-16pt">
            arrow_forward
          </span>
          <span className="d-flex align-items-center">
            <InputNumber
              className="form-control font-weight-bolder"
              style={{
                width: '6rem',
              }}
              value={logisticsQuantity}
              max={usableQuantity}
              onChange={(value) => onChangeValue('logisticsQuantity', value)}
            />
            <span className="ml-2 text-50">{t('text_box_en')}</span>
          </span>
        </div>
      </td>
      <td>
        {anotherWarehouseListRef.current.length === 0 && <>{warehouseName}</>}
        {anotherWarehouseListRef.current.length > 0 && (
          <select
            className="form-control custom-select font-weight-bolder w-auto"
            onChange={(e) => {
              const findWarehouse = warehouseList.find(
                (warehouse) => warehouse.warehouseId === e.target.value
              );

              if (findWarehouse) {
                onChangeValue('targetWarehouseId', findWarehouse.warehouseId);
                onChangeValue(
                  'targetWarehouseName',
                  findWarehouse.warehouseName
                );
              }
            }}
          >
            {warehouseList.map(
              ({
                warehouseId: targetWarehouseId,
                warehouseName: targetWarehouseName,
              }) => (
                <option
                  key={targetWarehouseId}
                  value={targetWarehouseId}
                  disabled={warehouseId === targetWarehouseId}
                >
                  {targetWarehouseName}
                </option>
              )
            )}
          </select>
        )}
      </td>
      <td>
        <a className="circle-pin-sm p-2" onClick={onClickRemove}>
          <span className="material-icons font-size-16pt">close</span>
        </a>
      </td>
    </tr>
  );
}

export default FloatPhysicalDistributionMovement;
