import React, { ReactNode } from 'react';
import classNames from 'classnames';

type FormGroupProps = {
  children?: ReactNode;
  className?: string;
};

function FormGroup({ className, children }: FormGroupProps) {
  return (
    <div className={classNames('form-group mb-32pt', className)}>
      {children}
    </div>
  );
}

export default FormGroup;
