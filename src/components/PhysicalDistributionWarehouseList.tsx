import React, { useEffect, useRef, useState } from 'react';
import { PaneProps, useDropdown } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_EDIT,
} from '@/utils/constants/common';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import { Dropdown } from 'react-bootstrap';
import { deleteWarehouse, fetchWarehouses } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import { Product, Warehouse } from '@/modules/physical_distribution/types';
import ConfirmModal from '@/components/ConfirmModal';
import { CommonUtils } from '@/utils';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import MaterialIcon from '@/components/MaterialIcon';
import { usePhysicalDistributionWarehouse } from '@/modules/physical_distribution/hook';
import classNames from 'classnames';

type PhysicalDistributionWarehouseListProps = PaneProps & {
  onChangeWarehouse: (warehouse: Warehouse) => void;
};

function PhysicalDistributionWarehouseList({
  onChangeStatus,
  onChangeWarehouse,
}: PhysicalDistributionWarehouseListProps) {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const {
    selectedWarehouse,
    handleSetWarehouse,
  } = usePhysicalDistributionWarehouse();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [load, setLoad] = useState(false);
  const [warehouseList, setWarehouseList] = useState<Warehouse[]>([]);
  const [filterList, setFilterList] = useState<Warehouse[]>([]);

  useEffect(() => {
    handleFetchWarehouseList();
    return () => {
      handleSetWarehouse(null);
    };
  }, []);

  useEffect(() => {
    handleSetWarehouse(null);
    setFilterList(
      warehouseList.filter(
        (warehouse) =>
          warehouse.warehouseName.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, warehouseList]);

  const handleFetchWarehouseList = async () => {
    setLoad(false);
    const data = await fetchWarehouses(project.id);
    setWarehouseList(data);
    setLoad(true);
  };

  const handleClickWarehouse = (clickWarehouse: Warehouse) => {
    let warehouse: Warehouse | null = null;
    if (selectedWarehouse?.warehouseId !== clickWarehouse.warehouseId) {
      warehouse = clickWarehouse;
    }
    handleSetWarehouse(warehouse);
  };

  const handleClickEdit = (warehouse: Warehouse) => {
    onChangeWarehouse(warehouse);
    onChangeStatus(PANE_STATUS_EDIT);
  };

  const handleDeleteWarehouse = async (warehouseId: string) => {
    const result = await deleteWarehouse(warehouseId);

    if (result) {
      handleFetchWarehouseList();
    }
  };

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'place_holder_search_warehouse'}
          onSubmit={setSearchKeyword}
        />
      </div>
      {!load && <></>}
      {load && filterList.length === 0 && (
        <em className="none-list">{t('msg_warehouse_empty')}</em>
      )}
      {load && filterList.length > 0 && (
        <div className="list-group row-list list-group-flush mb-4">
          {filterList.map((warehouse) => (
            <WarehouseItem
              key={warehouse.warehouseId}
              {...warehouse}
              activeId={selectedWarehouse?.warehouseId || ''}
              onClick={() => handleClickWarehouse(warehouse)}
              onClickEdit={() => {
                handleClickEdit(warehouse);
              }}
              onClickDelete={handleDeleteWarehouse}
            />
          ))}
        </div>
      )}
    </>
  );
}

type WarehouseItemProps = Warehouse & {
  activeId: string;
  onClick: () => void;
  onClickEdit: () => void;
  onClickDelete: (warehouseId: string) => void;
};

function WarehouseItem({
  warehouseId,
  warehouseName,
  totalQuantity,
  note,
  activeId,
  onClick,
  onClickEdit,
  onClickDelete,
  deleteFlag,
}: WarehouseItemProps) {
  const { t } = useTranslation();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const [showConfirmModal, setShowConfirmModal] = useState(false);

  const handleSelectFunction = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        onClickEdit();
        break;
      case FUNCTION_DELETE:
        setShowConfirmModal(true);
        break;
    }
  };

  return (
    <>
      <div
        className={classNames('list-group-item d-flex', {
          active: warehouseId === activeId,
          'text-30': deleteFlag,
        })}
        onClick={() => onClick()}
      >
        <div className="flex d-flex align-items-center mr-16pt">
          <div className="flex list-els">
            <div
              className={classNames('card-title', {
                'text-30': deleteFlag,
              })}
            >
              {deleteFlag && `[${t('text_delete')}] `} {warehouseName}
            </div>
            <div
              className="card-subtitle text-50"
              dangerouslySetInnerHTML={{
                __html: note || '-',
              }}
            ></div>
          </div>
          <div className="pd-count">
            {CommonUtils.numberWithCommas(totalQuantity)}
          </div>
        </div>
        {!deleteFlag && (
          <Dropdown onToggle={handleToggle} onSelect={handleSelectFunction}>
            <DropdownToggle
              as={'a'}
              data-caret="false"
              className="text-muted py-3 px-2"
            >
              <MaterialIcon name={'more_vert'} />
            </DropdownToggle>
            <DropdownMenu align={'right'} ref={dropdown}>
              <Dropdown.Item className="text-primary" eventKey={FUNCTION_EDIT}>
                {t('text_edit')}
              </Dropdown.Item>
              <Dropdown.Item className="text-danger" eventKey={FUNCTION_DELETE}>
                {t('text_delete')}
              </Dropdown.Item>
            </DropdownMenu>
          </Dropdown>
        )}
      </div>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={() => onClickDelete(warehouseId)}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>
    </>
  );
}

export default PhysicalDistributionWarehouseList;
