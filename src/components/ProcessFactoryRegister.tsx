import React, { useState, useEffect } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import { Factory } from '@/modules/physical_distribution/types';
// import { useControlProject } from '@/modules/map/hook';
import classNames from 'classnames';
import { dp_postFactory } from '@/api/physical_distribution';
// import InputNumber from '@/components/InputNumber';
// import { postWarehouse } from '@/api/physical_distribution';

type ProcessProductLineRegisterProps = PaneProps & {
  factory: Factory | undefined;
};

function ProcessFactoryRegister({
  factory,
  onChangeStatus,
}: ProcessProductLineRegisterProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [inputs, setInputs] = useState<{
    name: string;
    memo: string;
  }>({
    name: factory !== undefined ? factory.name : '',
    memo: factory !== undefined ? factory.memo : '',
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (factory !== undefined && !factory.id) {
      onChangeStatus(PANE_STATUS_LIST);
    }
  }, [factory]);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid = !!inputs.name;

    if (isValid) {
      setLoading(true);
      const result = await dp_postFactory({
        id: factory !== undefined ? factory.id : undefined, //id 값존재하면 수정처리
        name: inputs.name,
        memo: inputs.memo,
      });

      if (result) {
        onChangeStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'공장명'} className={'mb-0'} essential={true} />
        <input
          type="text"
          className="form-line"
          id="name"
          value={inputs.name}
          placeholder={t('공장명을 입력해 주세요.')}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_memo'} />
        <input
          type="text"
          className="form-line"
          id="memo"
          value={inputs.memo}
          placeholder={t('place_holder_memo')}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => onChangeStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t(factory === undefined ? 'text_do_add' : 'text_do_edit')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default ProcessFactoryRegister;
