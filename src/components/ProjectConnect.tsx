import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useControlProject } from '@/modules/map/hook';
import { Space, Floors } from '@/modules/map/types';
import { Collapse, Dropdown, OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useDropdown } from '@/modules/common';
import { useProjectPane, useProjectRegister } from '@/modules/project/hook';
import {
  FUNCTION_DISCONNECT_SPACE,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory } from 'react-router-dom';
import { fetchSpaceListInfo } from '@/api/space';
import {
  deleteProjectDisconnectSpace,
  fetchProject,
  postProjectConnectSpace,
} from '@/api/project';
import { useSpacePane, useSpaceRegister } from '@/modules/space/hook';

function ProjectConnect() {
  const history = useHistory();
  const { t } = useTranslation();
  const { handleChangeDepthSidebarShow } = useProjectPane();
  const { project, handleSetProject } = useControlProject();
  const {
    projectInfo,
    handleSetProjectInfo,
    handleSetRegisterInitialState,
    handleSetConnectedMetaIds,
  } = useProjectRegister();
  const [spaceList, setSpaceList] = useState<Space[]>([]);

  useEffect(() => {
    setSpaceList(project.spaceList);
    handleSetConnectedMetaIds(project.spaceList.map((space) => space.id));

    return () => {
      handleSetRegisterInitialState();
      handleChangeDepthSidebarShow(false);
    };
  }, []);

  useEffect(() => {
    if (projectInfo.metaIds) {
      handleFetchSpaceInfo();
    }
  }, [projectInfo]);

  const handleFetchSpaceInfo = async () => {
    const processSpaceInfos = await fetchSpaceListInfo(projectInfo.metaIds!);

    const addSpaceList: Space[] = [];

    if (processSpaceInfos.length) {
      processSpaceInfos.forEach((processSpaceInfo) => {
        const addFloors: Floors[] = [];

        if (!spaceList.find((space) => space.id === processSpaceInfo.spaceId)) {
          processSpaceInfo.floorsList.forEach((floors) => {
            addFloors.push({
              id: '',
              name: floors.floorsName,
              value: floors.floorsValue,
            });
          });

          addSpaceList.push({
            id: processSpaceInfo.spaceId,
            name: processSpaceInfo.spaceName,
            longitude: processSpaceInfo.lng,
            latitude: processSpaceInfo.lat,
            mappingId: '',
            floorsList: addFloors,
            registDate: '',
          });
        }
      });

      setSpaceList(spaceList.concat(addSpaceList));
    }
  };

  const handleSubmit = async () => {
    const curSpaceIdList = project.spaceList.map((space) => space.id);

    const resultSpaceIdList = spaceList.map((space) => space.id);

    const disconnectSpace = project.spaceList.filter(
      (space) => !resultSpaceIdList.includes(space.id)
    );

    const newConnectionSpace = spaceList.filter(
      (space) => !curSpaceIdList.includes(space.id)
    );

    const promise: Promise<any>[] = [];

    disconnectSpace.forEach((space) => {
      promise.push(deleteProjectDisconnectSpace(project.id, space.mappingId));
    });

    newConnectionSpace.forEach((space) => {
      promise.push(postProjectConnectSpace(project.id, space.id));
    });

    await Promise.all(promise);

    handleFetchProject();
  };

  const handleFetchProject = async () => {
    const projectDetail = await fetchProject(project.id);

    if (projectDetail) {
      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((projectSpace) => {
        const projectFloors: Floors[] = [];
        projectSpace.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: projectSpace.mappingId,
          id: projectSpace.metaId,
          name: projectSpace.metaName,
          longitude: projectSpace.lng,
          latitude: projectSpace.lat,
          floorsList: projectFloors,
          registDate: projectSpace.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.push('/control');
    }
  };

  const handleDisconnect = (spaceId: string) => {
    setSpaceList(spaceList.filter((space) => space.id !== spaceId));

    const metaIds = projectInfo.metaIds;
    const index = metaIds?.indexOf(spaceId);
    if (typeof metaIds !== 'undefined' && typeof index !== 'undefined') {
      if (index > -1) {
        metaIds?.splice(index, 1);
      }

      handleSetProjectInfo({
        ...projectInfo,
        metaIds,
      });
    } else {
      handleSetProjectInfo({
        ...projectInfo,
        metaIds: [],
      });
    }
  };

  return (
    <>
      <div className="container-fluid d-flex align-items-center py-4">
        <div className="flex d-flex">
          <div className="mr-24pt">
            <h3 className="mb-0">{t('text_space_connect_to_project')}</h3>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <div className="accordion js-accordion accordion--boxed mb-4">
          {spaceList.length === 0 && (
            <em className="none-list mb-4">{t('msg_connect_space')}</em>
          )}
          {spaceList.map((space) => (
            <SpaceItem
              key={space.id}
              {...space}
              onDisconnect={handleDisconnect}
            />
          ))}
        </div>
        <RegisterButton />
      </div>
      <Footer onSubmit={handleSubmit} />
    </>
  );
}

type SpaceItemProps = Space & {
  onDisconnect: (id: string) => void;
};

function SpaceItem({ id, name, floorsList, onDisconnect }: SpaceItemProps) {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  const handleOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_DISCONNECT_SPACE:
        onDisconnect(id);
        break;
    }
  };

  return (
    <div className="accordion__item">
      <a className="accordion__toggle" onClick={() => setOpen(!isOpen)}>
        <span className="flex">
          {name} <small className="text-danger">({floorsList.length})</small>
        </span>
        {floorsList.length > 0 && (
          <MaterialIcon
            name={'keyboard_arrow_down'}
            className={'accordion__toggle-icon'}
          />
        )}
      </a>
      <Dropdown
        onSelect={handleOptions}
        onToggle={handleToggle}
        className="acc-del"
      >
        <Dropdown.Toggle as={'a'} data-caret="false" className="text-100">
          <MaterialIcon name={'more_vert'} />
        </Dropdown.Toggle>
        <Dropdown.Menu align={'right'} ref={dropdown}>
          <Dropdown.Item
            className="text-primary"
            eventKey={FUNCTION_DISCONNECT_SPACE}
          >
            {t('text_space_disconnect_to_project')}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
      {floorsList.length > 0 && (
        <Collapse in={isOpen}>
          <div className="accordion__menu">
            {floorsList.map((floors) => (
              <FloorItem key={`${floors.value}/${floors.name}`} {...floors} />
            ))}
          </div>
        </Collapse>
      )}
    </div>
  );
}

type FloorItemProps = {
  name: string;
};

function FloorItem({ name }: FloorItemProps) {
  return (
    <div className="accordion__menu-link">
      <span className="icon-holder icon-holder--small icon-holder--accent rounded-circle d-inline-flex icon--left">
        <FontAwesomeIcon icon={['far', 'map']} />
      </span>
      <p className="flex m-0">{name}</p>
    </div>
  );
}

function RegisterButton() {
  const history = useHistory();
  const { handleChangePaneStatus } = useSpacePane();
  const { t } = useTranslation();
  const { handleSetConnectProjectId } = useSpaceRegister();
  const { project } = useControlProject();
  const { depthSidebarShow, handleChangeDepthSidebarShow } = useProjectPane();

  return (
    <div className="d-flex align-items-center justify-content-center">
      {
        <OverlayTrigger
          placement={'top'}
          overlay={
            <Tooltip id={'doNotHaveSpace'}>
              {t('msg_do_not_have_space')}
            </Tooltip>
          }
        >
          <a
            className="btn-row-add"
            onClick={() => {
              handleSetConnectProjectId(project.id);
              handleChangePaneStatus(PANE_STATUS_REGISTER);
              history.push('/space');
            }}
          >
            {' '}
            <MaterialIcon name={'add'} /> {t('text_space_new_registration')}
          </a>
        </OverlayTrigger>
      }
      <OverlayTrigger
        placement={'top'}
        overlay={
          <Tooltip id={'connectExistingSpace'}>
            {t('msg_connect_existing_space')}
          </Tooltip>
        }
      >
        <a
          className="btn-row-add ml-2"
          onClick={() => handleChangeDepthSidebarShow(!depthSidebarShow)}
        >
          {' '}
          <MaterialIcon name={'insert_link'} /> {t('text_space_list')}
        </a>
      </OverlayTrigger>
    </div>
  );
}

type Footer = {
  onSubmit: () => void;
};

function Footer({ onSubmit }: Footer) {
  const { t } = useTranslation();
  const history = useHistory();

  return (
    <div className="my-32pt">
      <div className="d-flex align-items-center justify-content-center">
        <a
          className="btn btn-outline-secondary mr-8pt"
          onClick={() => history.push('/control')}
        >
          {t('text_to_cancel')}
        </a>
        <a className="btn btn-outline-accent ml-0" onClick={onSubmit}>
          {t('text_save')}
        </a>
      </div>
    </div>
  );
}

export default ProjectConnect;
