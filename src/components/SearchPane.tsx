import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useActiveMenu, useSetupData } from '@/modules/setup/hook';
import { fetchSearchPoi } from '@/api/space';
import { useControlSpace, useOpenLayers } from '@/modules/map/hook';
import { PoiSearchInfo } from '@/modules/space/types';
import EmptyPane from '@/components/EmptyPane';
import { SearchDivision } from '@/modules/setup/types';
import { WKT } from 'ol/format';
import { Feature } from 'ol';
import { Stroke, Style } from 'ol/style';
import { getCenter } from 'ol/extent';

const wkt = new WKT();
function SearchPane() {
  const { t } = useTranslation();
  const { draw } = useOpenLayers();
  const { handleMenuResetActive } = useActiveMenu();
  const [load, setLoad] = useState(false);
  const [poiSearchInfoList, setPoiSearchInfoList] = useState<PoiSearchInfo[]>(
    []
  );

  const { search } = useSetupData();
  const { space } = useControlSpace();

  const handleFetchSearchPoi = async () => {
    const searchData = await fetchSearchPoi({
      mapid: space.floorsMapId,
      poinm: search.keyword,
      searchDv: search.division,
    });
    setLoad(true);
    if (searchData) {
      setPoiSearchInfoList(searchData);
    }
  };

  useEffect(() => {
    return () => draw.source?.clear();
  }, []);

  useEffect(() => {
    setPoiSearchInfoList([]);
    if (space.floorsMapId) {
      setLoad(false);
      handleFetchSearchPoi();
    }
  }, [search.flag, space]);

  return (
    <div className="tab-pane sm-search active">
      {load && poiSearchInfoList.length === 0 && (
        <EmptyPane
          textKey={'msg_no_result_search'}
          descriptionKey={'msg_search_suggest_to_keyword'}
        />
      )}
      {load && poiSearchInfoList.length > 0 && (
        <>
          <div className="search-value d-flex align-items-center">
            <span className="d-flex align-items-center text-muted">
              {t('text_search_result')}
              <strong className="text-danger">
                {poiSearchInfoList.length}
              </strong>
              {t('text_case')}
            </span>
            <a className="ml-auto" onClick={handleMenuResetActive}>
              <span className="material-icons">close</span>
            </a>
          </div>
          <div className="list-group list-group-flush mb-4">
            {poiSearchInfoList.map((poiSearchInfo) => (
              <SearchItem
                key={poiSearchInfo.id}
                division={search.division}
                {...poiSearchInfo}
              />
            ))}
          </div>
        </>
      )}
    </div>
  );
}

type SearchItemProps = PoiSearchInfo & {
  division: SearchDivision;
};

function SearchItem({ division, poi_name, area_geom }: SearchItemProps) {
  const { map, draw } = useOpenLayers();

  let searchName = '';
  let feature: Feature | null = null;
  if (division === 0) {
    if (poi_name) {
      searchName = poi_name;
    }
    if (area_geom) {
      feature = wkt.readFeature(area_geom);
    }
  } else if (division === 1) {
    if (poi_name) {
      searchName = poi_name;
    }
    if (area_geom) {
      feature = wkt.readFeature(area_geom);
    }
  } else if (division === 2) {
    if (poi_name) {
      searchName = poi_name;
    }

    if (area_geom) {
      feature = wkt.readFeature(area_geom);
    }
  }

  if (feature) {
    feature.setStyle(
      new Style({
        stroke: new Stroke({
          color: '#FF0000',
          width: 3,
        }),
      })
    );
  }

  const handleShowGeofencing = () => {
    draw.source?.clear();

    if (feature) {
      const view = map?.getView();
      if (division === 2) {
        const extent = feature.getGeometry()?.getExtent();
        if (extent) {
          view?.fit(extent, {
            size: map?.getSize(),
            maxZoom: 16,
          });
        }
      } else {
        draw.source?.addFeature(feature);
        const extent = feature.getGeometry()?.getExtent();
        if (extent) {
          view?.fit(extent, {
            size: map?.getSize(),
          });
        }
      }
    }
  };

  return (
    <div
      className="list-group-item d-flex flex-column flex-sm-row align-items-sm-center"
      onClick={handleShowGeofencing}
    >
      <div className="flex d-flex align-items-center">
        <div className="flex list-cover">
          <a className="card-title">{searchName}</a>
          {/*<div className="card-subtitle text-70">{poi_name}</div>*/}
        </div>
      </div>
    </div>
  );
}

export default SearchPane;
