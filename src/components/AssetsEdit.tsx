import React, { useEffect, useRef, useState } from 'react';
import { PANE_STATUS_DETAIL, PaneStatus } from '@/utils/constants/common';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import CompletePane from '@/components/CompletePane';
import { useLeftPaneContainer } from '@/modules/setup/hook';
import { Assets } from '@/modules/assets/types';

type AssetsEditProps = {
  assets: Assets;
  onChangeStatus: (status: PaneStatus) => void;
};

function AssetsEdit({ assets, onChangeStatus }: AssetsEditProps) {
  const [done, setDone] = useState(false);
  const { updateScroll } = useLeftPaneContainer();

  const handleSubmit = () => {
    setDone(true);
  };

  useEffect(() => {
    updateScroll();
  }, [done]);

  if (done) {
    return (
      <CompletePane
        completeTextKey={'msg_edit_complete'}
        backBtnTextKey={'text_confirm'}
        onBackBtnClick={() => onChangeStatus(PANE_STATUS_DETAIL)}
      />
    );
  }

  return (
    <>
      <Header onChangeStatus={onChangeStatus} />
      <Body />
      <Footer onSubmit={handleSubmit} onChangeStatus={onChangeStatus} />
    </>
  );
}

type HeaderProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function Header({ onChangeStatus }: HeaderProps) {
  const { t } = useTranslation();

  return (
    <div className="container-fluid  py-4">
      <div className="flex d-flex">
        <a className="pr-2" onClick={() => onChangeStatus(PANE_STATUS_DETAIL)}>
          <MaterialIcon name={'arrow_back'} />
        </a>
        <h3 className="mb-0">{t('text_assets_edit')}</h3>
      </div>
    </div>
  );
}

function Body() {
  const { t } = useTranslation();
  const uuidInput = useRef<HTMLInputElement>(null);

  const handleCopyUuid = () => {
    const current = uuidInput.current;
    if (current) {
      current.select();
      document.execCommand('copy');
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'text_assets_name'} essential={true} />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_assets_name')}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_classification'} essential={true} />
        <select className="form-line">
          <option value="">분류1</option>
          <option value="">분류2</option>
          <option value="">분류3</option>
          <option value="">분류4</option>
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_location'} essential={true} />
        <select className="form-line">
          <option value="">위치1</option>
          <option value="">위치2</option>
          <option value="">위치3</option>
          <option value="">위치4</option>
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_tag_connection'} essential={true} />
        <select className="form-line">
          <option value="">태그1</option>
          <option value="">태그2</option>
          <option value="">태그3</option>
        </select>
      </FormGroup>
      <div className="uwb-form">
        <FormGroup>
          <FormLabel textKey={'text_type'} essential={true} />
          <select className="form-line" disabled>
            <option value="">태그</option>
            <option value="">앵커</option>
            <option value="">브릿지</option>
            <option value="">와이파이</option>
          </select>
        </FormGroup>
        <FormGroup className={'over-txt'}>
          <FormLabel textKey={'text_uuid'} essential={true} />
          <input
            type="text"
            className="form-line pr-6"
            value="KIR2394KFHF991"
            readOnly={true}
            ref={uuidInput}
          />
          <OverlayTrigger
            placement={'right'}
            overlay={<Tooltip id={'uuidCopy'}>{t('text_do_copy')}</Tooltip>}
          >
            <a className="btn btn-secondary btn-sm" onClick={handleCopyUuid}>
              <MaterialIcon name={'content_copy'} />
            </a>
          </OverlayTrigger>
        </FormGroup>
      </div>
    </div>
  );
}

type FooterProps = {
  onSubmit: () => void;
  onChangeStatus: (status: PaneStatus) => void;
};

function Footer({ onSubmit, onChangeStatus }: FooterProps) {
  const { t } = useTranslation();

  return (
    <div className="mt-64pt">
      <div className="d-flex align-items-center justify-content-center">
        <a
          className="btn btn-outline-secondary mr-8pt"
          onClick={() => onChangeStatus(PANE_STATUS_DETAIL)}
        >
          {t('text_to_cancel')}
        </a>
        <a className="btn btn-outline-accent ml-0" onClick={onSubmit}>
          {t('text_do_edit')}
        </a>
      </div>
    </div>
  );
}

export default AssetsEdit;
