import React from 'react';
import Flatpickr from 'react-flatpickr';
import classNames from 'classnames';
import moment from 'moment';
import { Korean } from 'flatpickr/dist/l10n/ko';

type PhysicalDistributionFlatpickrProps = {
  disabled?: boolean;
  value?: string;
  onChange?: (value: string) => void;
};

function PhysicalDistributionFlatpickr({
  value,
  disabled = false,
  onChange,
}: PhysicalDistributionFlatpickrProps) {
  if (disabled) {
    return (
      <Flatpickr
        options={{
          mode: 'single',
          altFormat: 'Y-m-d',
          dateFormat: 'Y-m-d',
          altInput: true,
          altInputClass: classNames(
            'font-weight-bolder d-inline-flex physical-distribution-logistics-flatpickr',
            {
              disabled,
            }
          ),
          locale: Korean,
        }}
        disabled={disabled}
        value={moment(value).format('YYYY-MM-DD')}
      />
    );
  } else {
    return (
      <Flatpickr
        options={{
          mode: 'single',
          altFormat: 'Y-m-d',
          dateFormat: 'Y-m-d',
          altInput: true,
          altInputClass: classNames(
            'font-weight-bolder d-inline-flex physical-distribution-logistics-flatpickr'
          ),
          locale: Korean,
        }}
        value={moment(value).format('YYYY-MM-DD')}
        onChange={(dates) => {
          if (dates.length > 0) {
            onChange?.call(null, moment(dates[0]).format('YYYY-MM-DD'));
          }
        }}
      />
    );
  }
}

export default PhysicalDistributionFlatpickr;
