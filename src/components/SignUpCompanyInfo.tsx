import React, { useState } from 'react';
import { SignUpMemberProps } from '@/components/SignUpMember';
import { useTranslation } from 'react-i18next';
import FormLabel from '@/components/FormLabel';
import { useHistory } from 'react-router-dom';

type SignUpCompanyInfoProps = SignUpMemberProps;

function SignUpCompanyInfo({ onChangeMemberStep }: SignUpCompanyInfoProps) {
  const { t } = useTranslation();
  const history = useHistory();

  const [inputs, setInputs] = useState<{
    companyName: string;
    department: string;
    type: string;
    scale: string;
    field: string;
  }>({
    companyName: '',
    department: '',
    type: '',
    scale: '',
    field: '',
  });

  const [validInputs, setValidInputs] = useState<{
    companyName: boolean;
    department: boolean;
    type: boolean;
    scale: boolean;
    field: boolean;
  }>({
    companyName: true,
    department: true,
    type: true,
    scale: true,
    field: true,
  });

  const validForm = () => {
    let companyName = true;
    let department = true;
    let type = true;
    let scale = true;
    let field = true;

    if (!inputs.companyName) {
      companyName = false;
    }
    if (!inputs.department) {
      department = false;
    }
    if (!inputs.type) {
      type = false;
    }
    if (!inputs.scale) {
      scale = false;
    }
    if (!inputs.field) {
      field = false;
    }

    setValidInputs({
      companyName,
      department,
      type,
      scale,
      field,
    });

    return companyName && department && type && scale && field;
  };

  const handleClickComplete = () => {
    const isValid = validForm();

    if (isValid) {
      // TODO: API
      onChangeMemberStep(3);
    }
  };

  return (
    <>
      <div className="title-group mb-5">
        <h3 className="m-0">{t('text_enter_company_information')}</h3>
      </div>
      <div className="form-group mb-4">
        <FormLabel
          textKey={'text_company_name'}
          essential={true}
          htmlFor={'companyName'}
        />
        <input
          id="companyName"
          type="text"
          className="form-line pr-6"
          placeholder={t('place_holder_company_name')}
          autoComplete={'off'}
          value={inputs.companyName}
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        />
        {!validInputs.companyName && (
          <div className="invalid-feedback">
            {t('msg_valid_enter_company_name')}
          </div>
        )}
      </div>
      <div className="form-group mb-4">
        <FormLabel
          textKey={'text_department'}
          essential={true}
          htmlFor={'department'}
        />
        <input
          id="department"
          type="text"
          className="form-line"
          placeholder={t('place_holder_department')}
          autoComplete={'off'}
          value={inputs.department}
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        />
        {!validInputs.department && (
          <div className="invalid-feedback">
            {t('msg_valid_enter_department')}
          </div>
        )}
      </div>
      <div className="form-group mb-4">
        <FormLabel
          textKey={'text_types_of_company'}
          essential={true}
          htmlFor={'type'}
        />
        <input
          id="type"
          type="text"
          className="form-line pr-6"
          placeholder={t('place_holder_types_of_company')}
          autoComplete={'off'}
          value={inputs.type}
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        />
        {!validInputs.type && (
          <div className="invalid-feedback">
            {t('msg_valid_enter_types_of_company')}
          </div>
        )}
      </div>
      <div className="form-group mb-4">
        <FormLabel textKey={'text_scale'} essential={true} htmlFor={'scale'} />
        <input
          id="scale"
          type="text"
          className="form-line"
          placeholder={t('place_holder_scale')}
          autoComplete={'off'}
          value={inputs.scale}
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        />
        {!validInputs.scale && (
          <div className="invalid-feedback">{t('msg_valid_enter_scale')}</div>
        )}
      </div>
      <div className="form-group mb-5">
        <FormLabel
          textKey={'text_field_of_company'}
          essential={true}
          htmlFor={'field'}
        />
        <input
          id="field"
          type="text"
          className="form-line"
          placeholder={t('place_holder_field_of_company')}
          autoComplete={'off'}
          value={inputs.field}
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        />
        {!validInputs.field && (
          <div className="invalid-feedback">
            {t('msg_valid_enter_field_of_company')}
          </div>
        )}
      </div>
      <div className="form-group text-center mb-32pt">
        <button
          className="btn btn-block btn-lg btn-accent"
          type="button"
          onClick={handleClickComplete}
        >
          {t('text_complete')}
        </button>
      </div>
      <div className="text-50">
        {t('msg_gen_company_possible_at_any_time_after')}{' '}
        <a
          className="text-body text-underline"
          onClick={() => history.replace('/signin')}
        >
          {t('text_skip')}
        </a>
      </div>
    </>
  );
}

export default SignUpCompanyInfo;
