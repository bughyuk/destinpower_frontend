import React, { ReactElement, useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { Heatmap, Image, Tile, Vector as VectorLayer } from 'ol/layer';
import { Collection, Feature, Map, Overlay, View } from 'ol';
import {
  useControlAccident,
  useControlArea,
  useControlEvent,
  useControlProject,
  useControlSpace,
  useControlUser,
  useHistoryUser,
  useOpenLayers,
  useRealtimeUser,
} from '@/modules/map/hook';
import { ImageWMS, TileJSON, Vector as VectorSource } from 'ol/source';
import { Circle, Fill, Icon, Stroke, Style, Text } from 'ol/style';
import {
  defaults as defaultInteractions,
  DragBox,
  Draw,
  Select,
} from 'ol/interaction';
import { defaults as defaultControls, MousePosition } from 'ol/control';
import { fetchProjectRealtime } from '@/api/map';
import {
  ASSETS_INTERVAL,
  GEOMETRY_TYPE_CIRCLE,
  GEOMETRY_TYPE_POLYGON,
  MAP_TILE_KEY,
  OPEN_LAYERS_RESOLUTIONS,
  REALTIME_INTERVAL,
  USER_TYPE,
} from '@/utils/constants/common';
import { Circle as CircleGeometry, Geometry, Point, Polygon } from 'ol/geom';
import { RealtimeUser } from '@/modules/map/types';
import {
  useControlMode,
  useControlUserPane,
  useSetupAssets,
  useSetupEventsFlag,
} from '@/modules/setup/hook';
import ControlUserMenu from '@/components/ControlUserMenu';
import GeometryType from 'ol/geom/GeometryType';
import { fetchActivationEvents } from '@/api/event';
import { WKT } from 'ol/format';
import { fetchActivationAccidents } from '@/api/accident';
import * as turf from '@turf/turf';
import { CommonUtils } from '@/utils';
import { fetchArea, fetchPoi } from '@/api/space';
import { Config } from '@/config';
import {
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
} from '@/modules/accident/types';
import {
  CONTROL_MODE_TENSE_HISTORY,
  CONTROL_MODE_TENSE_REALTIME,
} from '@/modules/setup/types';
import { platformModifierKeyOnly, shiftKeyOnly } from 'ol/events/condition';
import { DrawGeoImage } from '@/modules/space/types';
import { getGeoImageLayer, getGeoImageSource } from '@/utils/ol-ext-bridge';
import { OpenLayersUtils } from '@/utils/OpenLayersUtils';
import { createStringXY } from 'ol/coordinate';
import {
  fetchAssets,
  fetchAssetsTemperatureAndHumidityRealtime,
} from '@/api/assets';
import {
  ASSET_SIGNAL_STATUS_STOP,
  Assets,
  ASSETS_CATEGORY_HUMIDITY,
  ASSETS_CATEGORY_TEMPERATURE,
  ASSETS_CATEGORY_TEMPERATURE_AND_HUMIDITY,
  ASSETS_DIV_SENSOR,
} from '@/modules/assets/types';
import _ from 'lodash';
import { fromLonLat } from 'ol/proj';
import classNames from 'classnames';

let historySimulationSource: VectorSource;
let realtimeTimer: ReturnType<typeof setInterval> | null;
let map: Map;
let floorsSource: ImageWMS;
let poiIconSource: VectorSource;
let poiTextSource: VectorSource;
let areaSource: VectorSource;
let eventSource: VectorSource;
let accidentEmergencySource: VectorSource;
let accidentFireSource: VectorSource;
let accidentRescueSource: VectorSource;
let accidentInfectiousDiseaseSource: VectorSource;
let realtimeSource: VectorSource;
let heatMapSource: ImageWMS;
let distributionSource: ImageWMS;
let apSource: ImageWMS;
let zoneSource: ImageWMS;
let realtimeHeatMapSource: VectorSource;
let controlUserList: RealtimeUser[] = [];

let assetsTimer: ReturnType<typeof setInterval> | null = null;

const wkt = new WKT();
function MapContainer() {
  const { handleSetOpenLayers } = useOpenLayers();
  const { project, handleSetProjectSummary } = useControlProject();
  const { space: controlSpace } = useControlSpace();
  const { handleSetAreaList } = useControlArea();
  const {
    users,
    handleSetControlUser,
    handleCancelControlUser,
    handleResetControlUser,
  } = useControlUser();
  const { handleDirectShowControlUserPane } = useControlUserPane();
  const {
    reloadFlag: eventReloadFlag,
    handleSetDirectEventId,
  } = useControlEvent();
  const {
    reloadFlag: accidentReloadFlag,
    handleSetDirectAccidentId,
  } = useControlAccident();
  const { handleSetRealtimeUser } = useRealtimeUser();
  const { handleSetHistoryUser } = useHistoryUser();
  const { tense: controlModeTense } = useControlMode();
  const { click: clickFlag } = useSetupEventsFlag();
  const clickFlagRef = useRef(true);
  const container = useRef<HTMLDivElement>(null);
  const [assetsList, setAssetsList] = useState<Assets[]>([]);
  const { assetsReloadFlag } = useSetupAssets();

  useEffect(() => {
    controlUserList = users;
  }, [users]);

  useEffect(() => {
    clickFlagRef.current = clickFlag;
  }, [clickFlag]);

  useEffect(() => {
    const brightMap = new Tile({
      source: new TileJSON({
        url: `https://api.maptiler.com/maps/streets/tiles.json?key=${MAP_TILE_KEY}`,
        tileSize: 512,
      }),
    });

    floorsSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        LAYERS: 'watta:lms_view_area',
        VERSION: '1.1.1',
        FORMAT: 'image/png',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const floorsLayer = new Image({
      source: floorsSource,
      visible: true,
    });

    heatMapSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        FORMAT: 'image/PNG',
        VERSION: '1.1.1',
        LAYERS: 'watta:lms_heatmap_sqlv',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const heatMapLayer = new Image({
      visible: false,
      source: heatMapSource,
    });

    distributionSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        FORMAT: 'image/PNG',
        VERSION: '1.1.1',
        LAYERS: 'watta:lms_visitor_sqlv',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const distributionLayer = new Image({
      visible: false,
      source: distributionSource,
    });

    areaSource = new VectorSource({ wrapX: false });
    const areaLayer = new VectorLayer({
      source: areaSource,
      style: new Style({
        stroke: new Stroke({
          color: '#FF0000',
          width: 3,
        }),
      }),
    });

    poiIconSource = new VectorSource({ wrapX: false });
    const poiIconLayer = new VectorLayer({
      source: poiIconSource,
      zIndex: 100,
    });

    poiTextSource = new VectorSource({ wrapX: false });
    const poiTextLayer = new VectorLayer({
      source: poiTextSource,
      zIndex: 100,
    });

    const drawSource = new VectorSource({ wrapX: false });
    const drawLayer = new VectorLayer({
      source: drawSource,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#3388ff',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#3388ff',
          }),
        }),
      }),
    });

    eventSource = new VectorSource({ wrapX: false });
    const eventLayer = new VectorLayer({
      source: eventSource,
      visible: false,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#3388ff',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#3388ff',
          }),
        }),
      }),
    });

    accidentEmergencySource = new VectorSource({ wrapX: false });
    const accidentEmergencyLayer = new VectorLayer({
      source: accidentEmergencySource,
      visible: false,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#3388ff',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#3388ff',
          }),
        }),
      }),
    });

    accidentFireSource = new VectorSource({ wrapX: false });
    const accidentFireLayer = new VectorLayer({
      source: accidentFireSource,
      visible: false,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#3388ff',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#3388ff',
          }),
        }),
      }),
    });

    accidentRescueSource = new VectorSource({ wrapX: false });
    const accidentRescueLayer = new VectorLayer({
      source: accidentRescueSource,
      visible: false,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#3388ff',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#3388ff',
          }),
        }),
      }),
    });

    accidentInfectiousDiseaseSource = new VectorSource({ wrapX: false });
    const accidentInfectiousDiseaseLayer = new VectorLayer({
      source: accidentInfectiousDiseaseSource,
      visible: false,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#3388ff',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#3388ff',
          }),
        }),
      }),
    });

    realtimeSource = new VectorSource({ wrapX: false });
    const realtimeLayer = new VectorLayer({
      source: realtimeSource,
      visible: true,
      zIndex: 99,
    });

    historySimulationSource = new VectorSource({ wrapX: false });
    const historySimulationLayer = new VectorLayer({
      source: historySimulationSource,
      visible: true,
    });

    const historyTraceDistributionSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        FORMAT: 'image/PNG',
        VERSION: '1.1.1',
        LAYERS: 'watta:lms_visitor_sqlv',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const historyTraceDistributionLayer = new Image({
      visible: false,
      source: historyTraceDistributionSource,
    });

    apSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        FORMAT: 'image/PNG',
        VERSION: '1.1.1',
        LAYERS: 'watta:util_sqlv',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const apLayer = new Image({
      visible: false,
      source: apSource,
    });

    zoneSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        LAYERS: 'watta:mng_zone_sqlv',
        VERSION: '1.1.1',
        FORMAT: 'image/png',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const zoneLayer = new Image({
      visible: false,
      source: zoneSource,
    });

    realtimeHeatMapSource = new VectorSource({
      wrapX: false,
    });

    const realtimeHeatMapLayer = new Heatmap({
      visible: false,
      source: realtimeHeatMapSource,
      blur: 10,
      radius: 5,
      weight: () => {
        return 2;
      },
    });

    let mapCenter: number[] = [14149654.863648022, 4495176.025755065];
    const findSpace = project.spaceList.find(
      (space) => space.mappingId === controlSpace.spaceMappingId
    );

    if (findSpace) {
      mapCenter = [findSpace.longitude, findSpace.latitude];
    }

    map = new Map({
      layers: [
        brightMap,
        floorsLayer,
        poiIconLayer,
        poiTextLayer,
        drawLayer,
        eventLayer,
        accidentEmergencyLayer,
        accidentFireLayer,
        accidentRescueLayer,
        accidentInfectiousDiseaseLayer,
        realtimeLayer,
        areaLayer,
        heatMapLayer,
        distributionLayer,
        historyTraceDistributionLayer,
        historySimulationLayer,
        apLayer,
        zoneLayer,
        realtimeHeatMapLayer,
      ],
      target: container.current!,
      interactions: defaultInteractions({
        doubleClickZoom: false,
      }),
      controls: defaultControls().extend([
        new MousePosition({
          coordinateFormat: createStringXY(5),
          projection: 'EPSG:4326',
          target: document.getElementById('mousePositionInfoBar')!,
        }),
      ]),
      view: new View({
        resolutions: OPEN_LAYERS_RESOLUTIONS,
        maxResolution: OPEN_LAYERS_RESOLUTIONS[0],
        center: mapCenter,
        zoom: 10,
        projection: 'EPSG:3857',
        zoomFactor: 1,
        minZoom: 9,
      }),
    });

    const overlay = new Overlay({});
    map.addOverlay(overlay);

    map.on('contextmenu', (e) => {
      const pixel = e.pixel;
      overlay.setElement(undefined);
      map.forEachFeatureAtPixel(pixel, (feature, layer) => {
        const realtimeUser = feature.get('realtime_user');
        if (realtimeUser) {
          const accessKey = feature.getId() as string;
          const container = document.createElement('div');
          container.classList.add('context-wrap', 'row');
          container.style.opacity = '0.7';
          container.style.top = '10px';
          container.style.left = '20px';

          const menu = (
            <ControlUserMenu
              selected={controlUserList.some(
                (controlUser) => controlUser.accessKey === accessKey
              )}
              onClickMenu={(eventKey) => {
                overlay.setElement(undefined);
                handleDirectShowControlUserPane(eventKey);
                handleControlRealtimeUser([realtimeUser as RealtimeUser]);

                const featureById = realtimeSource.getFeatureById(accessKey);
                if (featureById && featureById.getStyle()) {
                  const style = featureById.getStyle() as Style;
                  let iconImageUrl = 'person_pin_active.svg';
                  if (realtimeUser.userType === USER_TYPE.MANAGER) {
                    iconImageUrl = 'manager_pin_active.svg';
                  }
                  style.setImage(
                    new Icon({
                      src: `/static/images/${iconImageUrl}`,
                      scale: 2,
                    })
                  );
                  featureById.setStyle(style);
                }
              }}
              onCancel={() => {
                overlay.setElement(undefined);
                handleCancelControlRealtimeUser(accessKey);

                const featureById = realtimeSource.getFeatureById(accessKey);
                if (featureById && featureById.getStyle()) {
                  const style = featureById.getStyle() as Style;
                  let iconImageUrl = 'person_pin.svg';
                  if (realtimeUser.userType === USER_TYPE.MANAGER) {
                    iconImageUrl = 'manager_pin.svg';
                  }
                  style.setImage(
                    new Icon({
                      src: `/static/images/${iconImageUrl}`,
                      scale: 2,
                    })
                  );
                  featureById.setStyle(style);
                }
              }}
            />
          );

          ReactDOM.render(menu, container);

          overlay.setElement(container);
          const geometry = feature.getGeometry() as Point;
          overlay.setPosition(geometry.getCoordinates());
        }
      });
    });

    map.on('click', (e) => {
      if (clickFlagRef.current) {
        const pixel = e.pixel;
        overlay.setElement(undefined);
        map.forEachFeatureAtPixel(pixel, (feature, layer) => {
          const eventInfo = feature.get('event_info');
          const accidentInfo = feature.get('accident_info');

          if (eventInfo) {
            const id = feature.getId() as string;
            handleSetDirectEventId(id);
            return true;
          }

          if (accidentInfo) {
            const id = feature.getId() as string;
            handleSetDirectAccidentId(id);
            return true;
          }
        });
      }
    });

    const drawPolygon = new Draw({
      source: drawSource,
      type: GeometryType.POLYGON,
    });

    const drawCircle = new Draw({
      source: drawSource,
      type: GeometryType.CIRCLE,
    });

    const drawAccident = new Draw({
      source: drawSource,
      type: GeometryType.CIRCLE,
    });

    const floorPlanLayer = getGeoImageLayer();
    const drawFloorPlan = (options: DrawGeoImage) => {
      map.removeLayer(floorPlanLayer);
      map.addLayer(floorPlanLayer);
      const cx = options.imageCenter[0];
      if (cx === 0) {
        const center = map.getView().getCenter();
        if (center) {
          options.imageCenter = [center[0], center[1]];
          options.imageScale = [1, 1];
          options.imageRotate = 0;
        }
      }

      floorPlanLayer.setSource(getGeoImageSource(options));
    };

    const removeFloorPlan = () => {
      map.removeLayer(floorPlanLayer);
    };

    handleSetOpenLayers({
      map,
      floorsLayer,
      realtimeLayer,
      areaSource,
      eventLayer,
      accidentEmergencyLayer,
      accidentFireLayer,
      accidentRescueLayer,
      accidentInfectiousDiseaseLayer,
      heatMapLayer,
      distributionLayer,
      apLayer,
      historySimulationLayer,
      historyTraceDistributionLayer,
      poiLayer: {
        icon: poiIconLayer,
        text: poiTextLayer,
      },
      zoneLayer,
      realtimeHeatMapLayer,
      draw: {
        source: drawSource,
        layer: drawLayer,
        circle: drawCircle,
        polygon: drawPolygon,
        accident: drawAccident,
      },
      floorPlan: {
        draw: drawFloorPlan,
        remove: removeFloorPlan,
      },
    });

    const select = new Select({
      layers: [realtimeLayer],
    });
    const controlUserSelectedFeatures = select.getFeatures();
    const controlUserSelectionDragBox = new DragBox({
      condition: platformModifierKeyOnly,
    });
    map.addInteraction(controlUserSelectionDragBox);
    controlUserSelectionDragBox.on('boxstart', () => {
      controlUserSelectedFeatures.clear();
    });
    controlUserSelectionDragBox.on('boxend', () => {
      handleDragBoxEnd(
        controlUserSelectionDragBox,
        controlUserSelectedFeatures
      );
    });
    controlUserSelectedFeatures.on(['add'], () => {
      const selectedUserList: RealtimeUser[] = controlUserSelectedFeatures
        .getArray()
        .map((selectedFeature) => {
          if (selectedFeature.getStyle()) {
            const realtimeUserFeatureData = selectedFeature.get(
              'realtime_user'
            );
            if (realtimeUserFeatureData) {
              const realtimeUser = realtimeUserFeatureData as RealtimeUser;
              const style = selectedFeature.getStyle() as Style;
              let iconImageUrl = 'person_pin_active.svg';
              if (realtimeUser.userType === USER_TYPE.MANAGER) {
                iconImageUrl = 'manager_pin_active.svg';
              }
              style.setImage(
                new Icon({
                  src: `/static/images/${iconImageUrl}`,
                  scale: 2,
                })
              );
              selectedFeature.setStyle(style);
            }
          }
          return selectedFeature.get('realtime_user');
        }) as RealtimeUser[];

      handleControlRealtimeUser(selectedUserList);
      handleDirectShowControlUserPane('info');
    });

    const cancelSelect = new Select({
      layers: [realtimeLayer],
    });
    const controlUserCancelFeatures = cancelSelect.getFeatures();
    const controlUserCancelDragBox = new DragBox({
      condition: shiftKeyOnly,
    });
    map.addInteraction(controlUserCancelDragBox);
    controlUserCancelDragBox.on('boxstart', () => {
      controlUserCancelFeatures.clear();
    });
    controlUserCancelDragBox.on('boxend', () => {
      handleDragBoxEnd(controlUserCancelDragBox, controlUserCancelFeatures);
    });
    controlUserCancelFeatures.on(['add'], () => {
      controlUserCancelFeatures.getArray().map((selectedFeature) => {
        const id = selectedFeature.getId() as string;
        if (id) {
          if (selectedFeature.getStyle()) {
            const realtimeUserFeatureData = selectedFeature.get(
              'realtime_user'
            );
            if (realtimeUserFeatureData) {
              const realtimeUser = realtimeUserFeatureData as RealtimeUser;
              const style = selectedFeature.getStyle() as Style;
              let iconImageUrl = 'person_pin.svg';
              if (realtimeUser.userType === USER_TYPE.MANAGER) {
                iconImageUrl = 'manager_pin.svg';
              }
              style.setImage(
                new Icon({
                  src: `/static/images/${iconImageUrl}`,
                  scale: 2,
                })
              );
              selectedFeature.setStyle(style);
            }
          }
          handleCancelControlRealtimeUser(id);
        }
      });
    });

    return () => {
      handleStopInterval();
    };
  }, []);

  const handleFetchAssetsList = async () => {
    const data = await fetchAssets({
      metaid: controlSpace.spaceMetaId,
      mapid: controlSpace.floorsMapId,
      assetdiv: ASSETS_DIV_SENSOR,
    });
    setAssetsList(data);
  };

  const handleRemoveAssetsOverlay = () => {
    let overlays = map?.getOverlays().getArray();
    overlays = overlays?.filter((overlay) => {
      const overlayType = overlay.get('overlayType');
      if (overlayType && overlayType === 'assets') {
        return true;
      }

      return false;
    });

    overlays?.forEach((overlay) => {
      map?.removeOverlay(overlay);
    });
  };

  const handleFetchRealtimeData = async () => {
    handleRemoveAssetsOverlay();
    let uniqAssetsList = _.uniqBy(assetsList, 'asset_uuid');
    uniqAssetsList = uniqAssetsList.filter((assets) => assets.active_flag);
    for (const assets of uniqAssetsList) {
      const assetStatus = assets.asset_status;
      if (assetStatus === ASSET_SIGNAL_STATUS_STOP) {
        continue;
      }
      const assetCategory = Number(assets.asset_category);
      const isTempAndHumid =
        assetCategory === ASSETS_CATEGORY_TEMPERATURE ||
        assetCategory === ASSETS_CATEGORY_HUMIDITY ||
        assetCategory === ASSETS_CATEGORY_TEMPERATURE_AND_HUMIDITY;

      const coordinate = fromLonLat([assets.lng, assets.lat]);
      const container = document.createElement('div');
      container.classList.add('mouse-point', 'flexible-poi');
      let assetsTag: ReactElement = <></>;

      if (isTempAndHumid) {
        const data = await fetchAssetsTemperatureAndHumidityRealtime(
          assets.asset_uuid
        );

        if (data) {
          assetsTag = (
            <div className={classNames('level-cover level-item-compound')}>
              <span
                className={classNames('status mb-1', {
                  'bg-pin-blue': !data.humidity_ok,
                  'bg-pin-red': data.humidity_ok,
                })}
              >
                {Number(data.humidity)}%/
                {Number(data.humidity_max2) !== 0
                  ? Number(data.humidity_max2)
                  : Number(data.humidity_min2)}
                %
              </span>
              <span
                className={classNames('status', {
                  'bg-pin-blue': !data.celsius_ok,
                  'bg-pin-red': data.celsius_ok,
                })}
              >
                {Number(data.celsius)}&deg;/
                {Number(data.cellcius_max1) !== 0
                  ? Number(data.cellcius_max1)
                  : Number(data.cellcius_min1)}
                &deg;
              </span>
              <span className="poi-bg"></span>
            </div>
          );
        }
      } else {
        assetsTag = (
          <div
            className={classNames(
              'level-cover',
              `level-item0${assetStatus + 1}`
            )}
          >
            <span className="poi-bg"></span>
          </div>
        );
      }

      ReactDOM.render(assetsTag, container);
      const overlay = new Overlay({
        position: coordinate,
        element: container,
      });
      overlay.set('overlayType', 'assets');
      map?.addOverlay(overlay);
    }
  };

  useEffect(() => {
    handleFetchRealtimeData();
  }, [assetsList]);

  /*
    Select
   */

  const handleDragBoxEnd = (
    dragBox: DragBox,
    selectedFeatures: Collection<Feature<Geometry>>
  ) => {
    const rotation = map.getView().getRotation();
    const oblique = rotation % (Math.PI / 2) !== 0;
    const featureArray: Feature[] = [];
    const candidateFeatures = oblique ? featureArray : selectedFeatures;
    const extent = dragBox.getGeometry().getExtent();
    realtimeSource.forEachFeatureIntersectingExtent(extent, function (feature) {
      candidateFeatures.push(feature);
    });

    if (oblique) {
      const anchor = [0, 0];
      const geometry = dragBox.getGeometry().clone();
      geometry.rotate(-rotation, anchor);
      const extent$1 = geometry.getExtent();
      candidateFeatures.forEach((feature) => {
        const featureGeometry = feature.getGeometry();
        if (featureGeometry) {
          const geometry = featureGeometry.clone();
          geometry.rotate(-rotation, anchor);
          if (geometry.intersectsExtent(extent$1)) {
            selectedFeatures.push(feature);
          }
        }
      });
    }
  };

  /*
    Select End
   */

  /*
    Realtime
   */
  const handleControlRealtimeUser = (newControlUserList: RealtimeUser[]) => {
    handleSetControlUser(newControlUserList);
  };

  const handleCancelControlRealtimeUser = (accessKey: string) => {
    handleCancelControlUser(accessKey);
  };

  const handleChangeSpace = () => {
    realtimeSource.clear();
    realtimeHeatMapSource.clear();
    areaSource.clear();
    poiIconSource.clear();
    poiTextSource.clear();
    handleStopInterval();
    handleResetControlUser();
    handleSetRealtimeUser([]);
    handleSetHistoryUser([]);
    handleSetAreaList([]);

    const floorsParams = floorsSource.getParams();
    floorsParams['viewparams'] = '';
    const heatMapParams = heatMapSource.getParams();
    heatMapParams['CQL_FILTER'] = '';
    const distributionParams = distributionSource.getParams();
    distributionParams['CQL_FILTER'] = '';
    const apParams = apSource.getParams();
    apParams['CQL_FILTER'] = '';
    const zoneParams = zoneSource.getParams();
    zoneParams['viewparams'] = '';
  };

  const handleChangeFloors = () => {
    const floorsMapId = controlSpace.floorsMapId;
    const floorsParams = floorsSource.getParams();
    floorsParams['viewparams'] = `mapid:'${floorsMapId}'`;
    floorsSource.refresh();
    const heatMapParams = heatMapSource.getParams();
    heatMapParams['CQL_FILTER'] = `map_id = '${floorsMapId}'`;
    heatMapSource.refresh();
    const distributionParams = distributionSource.getParams();
    distributionParams['CQL_FILTER'] = `map_id = '${floorsMapId}'`;
    distributionSource.refresh();
    const apParams = apSource.getParams();
    apParams['CQL_FILTER'] = `map_id = '${floorsMapId}'`;
    apSource.refresh();
    const zoneParams = zoneSource.getParams();
    zoneParams['viewparams'] = `mapid:'${floorsMapId}'`;
    zoneSource.refresh();

    handleFetchPoi();
    handleFetchArea();
  };

  useEffect(() => {
    handleChangeSpace();

    if (controlSpace.spaceMappingId) {
      const findSpace = project.spaceList.find(
        (space) => space.mappingId === controlSpace.spaceMappingId
      );
      if (findSpace) {
        map.getView().animate({
          center: [findSpace.longitude, findSpace.latitude],
          zoom: 10,
        });
      }
    }

    if (controlSpace.spaceMappingId && controlSpace.floorsMapId) {
      handleChangeFloors();
    }
  }, [controlSpace]);

  useEffect(() => {
    handleSetRealtimeUser([]);
    handleSetHistoryUser([]);
    handleResetControlUser();
    realtimeSource.clear();
    realtimeHeatMapSource.clear();
    if (realtimeTimer) {
      clearInterval(realtimeTimer);
    }
    if (assetsTimer) {
      clearInterval(assetsTimer);
    }
    historySimulationSource.clear();

    if (controlModeTense === CONTROL_MODE_TENSE_REALTIME) {
      heatMapSource.updateParams({
        viewparams: '',
      });
      heatMapSource.refresh();
      distributionSource.updateParams({
        viewparams: '',
      });
      distributionSource.refresh();
    } else if (controlModeTense === CONTROL_MODE_TENSE_HISTORY) {
      handleSetProjectSummary({
        userCount: 0,
        accidentCount: 0,
      });
    }

    if (controlSpace.spaceMappingId && controlSpace.floorsMapId) {
      if (controlModeTense === CONTROL_MODE_TENSE_REALTIME) {
        // handleFetchRealTime();
      }

      handleFetchAssetsList();
      assetsTimer = setInterval(() => {
        handleFetchAssetsList();
      }, ASSETS_INTERVAL);
    }
  }, [controlModeTense, controlSpace, assetsReloadFlag]);

  useEffect(() => {
    eventSource.clear();
    if (controlSpace.spaceMappingId && controlSpace.floorsMapId) {
      // handleFetchEvent();
    }
  }, [controlSpace, eventReloadFlag]);

  useEffect(() => {
    accidentEmergencySource.clear();
    accidentFireSource.clear();
    accidentRescueSource.clear();
    accidentInfectiousDiseaseSource.clear();
    if (controlSpace.spaceMappingId && controlSpace.floorsMapId) {
      // handleFetchAccident();
    }
  }, [controlSpace, accidentReloadFlag]);

  const handleStopInterval = () => {
    if (realtimeTimer) {
      clearInterval(realtimeTimer);
    }
    realtimeTimer = null;
    if (assetsTimer) {
      clearInterval(assetsTimer);
    }
    assetsTimer = null;
  };

  const handleFetchRealTime = () => {
    realtimeTimer = setInterval(async () => {
      realtimeSource.clear();
      realtimeHeatMapSource.clear();

      const projectRealtimeData = await fetchProjectRealtime({
        mappingId: controlSpace.spaceMappingId,
        mapId: controlSpace.floorsMapId,
        projectId: project.id,
      });

      if (!CommonUtils.isEmptyObject(projectRealtimeData)) {
        const realtimeUserData = projectRealtimeData.activeUsersByFloor.data;

        if (map) {
          const realtimeUserList: RealtimeUser[] = [];
          for (const accessKey in realtimeUserData) {
            const realtimeUser = realtimeUserData[accessKey];
            const convertLngLat = OpenLayersUtils.convertLngLat([
              realtimeUser.lng,
              realtimeUser.lat,
            ]);
            realtimeUser.lng = convertLngLat[0];
            realtimeUser.lat = convertLngLat[1];
            realtimeUserList.push(realtimeUser);
            handleDrawFeatureRealtimeUser(accessKey, realtimeUser);
          }

          handleSetRealtimeUser(realtimeUserList);
          handleSetProjectSummary({
            userCount: projectRealtimeData.activeUsersByProject.size,
            accidentCount: projectRealtimeData.accidentsByProject.size,
          });
        }
      }
    }, REALTIME_INTERVAL);
  };

  const handleDrawFeatureRealtimeUser = (
    accessKey: string,
    realtimeUser: RealtimeUser
  ) => {
    const coordinate = [realtimeUser.lng, realtimeUser.lat];

    const iconFeature = new Feature(new Point(coordinate));
    realtimeHeatMapSource.addFeature(iconFeature);
    iconFeature.setId(accessKey);
    iconFeature.set('realtime_user', realtimeUser);

    let iconImageUrl = '';
    if (realtimeUser.userType === USER_TYPE.PERSON) {
      iconImageUrl = 'person_pin.svg';

      if (
        controlUserList.some(
          (controlUser) => controlUser.accessKey === accessKey
        )
      ) {
        iconImageUrl = 'person_pin_active.svg';
      }
    } else {
      iconImageUrl = 'manager_pin.svg';

      if (
        controlUserList.some(
          (controlUser) => controlUser.accessKey === accessKey
        )
      ) {
        iconImageUrl = 'manager_pin_active.svg';
      }
    }

    const iconStyle = new Style({
      image: new Icon({
        src: `/static/images/${iconImageUrl}`,
        scale: 2,
      }),
    });

    iconFeature.setStyle(iconStyle);
    realtimeSource?.addFeature(iconFeature);
  };

  /*
    Realtime End
   */

  /*
    Event
   */

  const handleFetchEvent = async () => {
    const events = await fetchActivationEvents({
      projectId: project.id,
      mappingId: controlSpace.spaceMappingId,
      mapId: controlSpace.floorsMapId,
    });

    if (events) {
      let feature: Feature | null;

      events.forEach((event) => {
        if (event.geomType === GEOMETRY_TYPE_POLYGON) {
          if (event.geom) {
            feature = wkt.readFeature(event.geom);
          }
        } else if (event.geomType === GEOMETRY_TYPE_CIRCLE) {
          if (event.lng && event.lat && event.radius) {
            feature = new Feature(
              new CircleGeometry([event.lng, event.lat], event.radius)
            );
          }
        } else if (event.areaId) {
          if (event.areaGeom) {
            feature = wkt.readFeature(event.areaGeom);
          }
        }

        if (feature) {
          feature.set('event_info', event);
          feature.setId(event.eventId);
          eventSource.addFeature(feature);
          feature = null;
        }
      });
    }
  };

  /*
    Event End
   */

  /*
    Accident
   */

  const handleFetchAccident = async () => {
    const accidents = await fetchActivationAccidents({
      projectId: project.id,
      mappingId: controlSpace.spaceMappingId,
      mapId: controlSpace.floorsMapId,
    });

    if (accidents) {
      let feature: Feature | null;
      let iconCenter: number[] = [];
      accidents.forEach((accident) => {
        if (accident.geomType === GEOMETRY_TYPE_POLYGON) {
          if (accident.geom) {
            feature = wkt.readFeature(accident.geom);
            const polygonGeometry = feature.getGeometry() as Polygon;
            const centroid = turf.centroid(
              turf.polygon(polygonGeometry.getCoordinates())
            );
            iconCenter = centroid.geometry.coordinates;
          }
        } else if (accident.geomType === GEOMETRY_TYPE_CIRCLE) {
          if (accident.lng && accident.lat && accident.radius) {
            iconCenter = [accident.lng, accident.lat];
            feature = new Feature(
              new CircleGeometry([accident.lng, accident.lat], accident.radius)
            );
          }
        } else if (accident.areaId) {
          if (accident.areaGeom) {
            feature = wkt.readFeature(accident.areaGeom);
          }
        }

        if (feature) {
          feature.set('accident_info', accident);
          feature.setId(accident.eventId);

          const accidentIconName = CommonUtils.getAccidentIconName(
            accident.eventDetailCategory
          );
          const icon = new Feature(new Point(iconCenter));
          const iconStyle = new Style({
            image: new Icon({
              src: `/static/images/${accidentIconName}`,
              scale: 2,
            }),
          });
          icon.setStyle(iconStyle);

          switch (accident.eventDetailCategory) {
            case ACCIDENT_EMERGENCY:
              accidentEmergencySource.addFeature(feature);
              accidentEmergencySource.addFeature(icon);
              break;
            case ACCIDENT_FIRE:
              accidentFireSource.addFeature(feature);
              accidentFireSource.addFeature(icon);
              break;
            case ACCIDENT_RESCUE:
              accidentRescueSource.addFeature(feature);
              accidentRescueSource.addFeature(icon);
              break;
            case ACCIDENT_INFECTIOUS_DISEASE:
              accidentInfectiousDiseaseSource.addFeature(feature);
              accidentInfectiousDiseaseSource.addFeature(icon);
              break;
          }

          feature = null;
          iconCenter = [];
        }
      });
    }
  };

  /*
    Accident End
  */

  /*
    Poi
   */

  const handleFetchPoi = async () => {
    const poiList = await fetchPoi({
      metaid: controlSpace.spaceMetaId,
      mapid: controlSpace.floorsMapId,
    });

    if (poiList) {
      poiList.forEach((poi) => {
        const point = new Point([poi.xcoord, poi.ycoord]);
        const iconFeature = new Feature(point);
        iconFeature.setStyle(
          new Style({
            image: new Icon({
              src: `${Config.map_server.indoor_edit_uri}/data/${poi.filenm}`,
              scale: 0.5,
            }),
          })
        );
        poiIconSource.addFeature(iconFeature);
        const textFeature = new Feature(point);
        textFeature.setStyle(
          new Style({
            text: new Text({
              text: poi.poi_name,
              scale: 1,
              textAlign: 'center',
              offsetY: 20,
              fill: new Fill({
                color: '#000000',
              }),
              stroke: new Stroke({
                color: '#FFFFFF',
                width: 2,
              }),
              font: 'bold 12px roboto',
            }),
          })
        );
        poiTextSource.addFeature(textFeature);
      });
    }
  };

  /*
    Poi End
   */

  /*
    Area
   */

  const handleFetchArea = async () => {
    const data = await fetchArea({
      metaid: controlSpace.spaceMetaId,
      mapid: controlSpace.floorsMapId,
    });

    if (data) {
      handleSetAreaList(data);
    }
  };

  /*
    Area End
   */

  return (
    <>
      <div
        ref={container}
        className="map-container"
        onContextMenu={(e) => {
          e.preventDefault();
        }}
      />
    </>
  );
}

export default MapContainer;
