import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  usePhysicalDistributionClient,
  usePhysicalDistributionHistory,
  usePhysicalDistributionStatus,
  usePhysicalDistributionWarehouse,
} from '@/modules/physical_distribution/hook';
import { useControlProject } from '@/modules/map/hook';
import {
  DistributionHistory,
  DistributionRevisedHistory,
  QRCodePrintConfirmData,
} from '@/modules/physical_distribution/types';
import FloatPhysicalDistributionHistoryWarehousing from '@/components/FloatPhysicalDistributionHistoryWarehousing';
import { ButtonGroup, Dropdown } from 'react-bootstrap';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import { FUNCTION_DELETE, FUNCTION_EDIT } from '@/utils/constants/common';
import { useDropdown } from '@/modules/common';
import ProfileAvatar from '@/components/ProfileAvatar';
import MaterialIcon from '@/components/MaterialIcon';
import FloatPhysicalDistributionHistoryRelease from '@/components/FloatPhysicalDistributionHistoryRelease';
import FloatPhysicalDistributionHistoryAdjustment from '@/components/FloatPhysicalDistributionHistoryAdjustment';
import FloatPhysicalDistributionHistoryMovement from '@/components/FloatPhysicalDistributionHistoryMovement';
import FloatPhysicalDistributionRevisedHistoryItem from '@/components/FloatPhysicalDistributionRevisedHistoryItem';
import ConfirmModal from '@/components/ConfirmModal';
import {
  deleteDistributionHistory,
  fetchDistributionHistory,
  fetchDistributionRevisedHistory,
  fetchQRCode,
  postManualComplete,
  putDistributionHistory,
} from '@/api/physical_distribution';
import QRCodePrintConfirmModal from '@/components/QRCodePrintConfirmModal';
import moment from 'moment';
import AlertModal from '@/components/AlertModal';
import NewlineText from '@/components/NewlineText';
import { useUser } from '@/modules/user/hook';

export type FloatPhysicalDistributionHistoryCommonProps = {
  distributionHistory: DistributionHistory;
};

function FloatPhysicalDistributionHistory() {
  const { t } = useTranslation();
  const { user } = useUser();
  const { project } = useControlProject();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const {
    selectedDistributionHistory,
    reloadFlag: historyReloadFlag,
    handleSetDistributionHistory,
    handleChangeReloadFlag,
  } = usePhysicalDistributionHistory();
  const [
    distributionHistory,
    setDistributionHistory,
  ] = useState<DistributionHistory | null>(null);
  const [
    originalDistributionHistory,
    setOriginalDistributionHistory,
  ] = useState<DistributionHistory | null>(null);
  const [
    loadDistributionRevisedHistoryList,
    setLoadDistributionRevisedHistoryList,
  ] = useState(false);
  const [
    distributionRevisedHistoryList,
    setDistributionRevisedHistoryList,
  ] = useState<DistributionRevisedHistory[]>([]);
  const [editMode, setEditMode] = useState(false);
  const {
    handleChangeReloadFlag: handleChangePhysicalDistributionStatusReloadFlag,
  } = usePhysicalDistributionStatus();
  const [
    showManualCompleteConfirmModal,
    setShowManualCompleteConfirmModal,
  ] = useState(false);
  const [
    showHistoryDeleteConfirmModal,
    setShowHistoryDeleteConfirmModal,
  ] = useState(false);
  const [
    showHistoryChangeConfirmModal,
    setShowHistoryChangeConfirmModal,
  ] = useState(false);
  const [
    showQRCodePrintConfirmModal,
    setShowQRCodePrintConfirmModal,
  ] = useState(false);
  const [showAlertModal, setShowAlertModal] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [
    qrCodePrintConfirmData,
    setQrCodePrintConfirmData,
  ] = useState<QRCodePrintConfirmData>({
    image: '',
    warehousingDate: '',
    totalLogisticsQuantity: 0,
    productName: '',
    productStandard: '',
  });
  const {
    load: loadWarehouseList,
    warehouseList,
  } = usePhysicalDistributionWarehouse();
  const {
    load: loadReleaseClientList,
    releaseClientList,
  } = usePhysicalDistributionClient();

  useEffect(() => {
    setEditMode(false);
    if (selectedDistributionHistory) {
      setDistributionHistory(selectedDistributionHistory);
      setOriginalDistributionHistory(selectedDistributionHistory);
    } else {
      setDistributionHistory(null);
      setOriginalDistributionHistory(null);
    }
  }, [selectedDistributionHistory]);

  useEffect(() => {
    handleFetchDistributionRevisedHistory();
  }, [originalDistributionHistory]);

  useEffect(() => {
    if (distributionHistory) {
      handleFetchDistributionHistory();
    }
  }, [historyReloadFlag]);

  const handleFetchDistributionHistory = async () => {
    if (distributionHistory) {
      const data = await fetchDistributionHistory(
        distributionHistory.logisticsId,
        project.id
      );
      if (data) {
        handleCloseAllModal();
        handleSetDistributionHistory(data);
      }
    }
  };

  const handleCloseAllModal = () => {
    setShowAlertModal(false);
    setShowQRCodePrintConfirmModal(false);
    setShowHistoryChangeConfirmModal(false);
    setShowHistoryDeleteConfirmModal(false);
    setShowManualCompleteConfirmModal(false);
  };

  const handleFetchDistributionRevisedHistory = async () => {
    if (originalDistributionHistory) {
      setLoadDistributionRevisedHistoryList(false);
      const data = await fetchDistributionRevisedHistory({
        projectId: project.id,
        logisticsId: originalDistributionHistory.logisticsId,
      });
      setDistributionRevisedHistoryList(data);
      setLoadDistributionRevisedHistoryList(true);
    }
  };

  const handleSelectFunction = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        if (editMode) {
          if (originalDistributionHistory) {
            setDistributionHistory({
              ...originalDistributionHistory,
            });
          }
        }
        setEditMode(!editMode);
        break;
      case FUNCTION_DELETE:
        setShowHistoryDeleteConfirmModal(true);
        break;
    }
  };

  const handleShowAlertModal = (message: string) => {
    setAlertMessage(message);
    setShowAlertModal(true);
  };

  const handleClickQRPrinting = async () => {
    if (distributionHistory) {
      const {
        logisticsId,
        logisticsQuantity,
        dueDate,
        productName,
        productSizeName,
        boxQuantity,
      } = distributionHistory;
      const base64QRCodeImage = await fetchQRCode(logisticsId);
      if (base64QRCodeImage) {
        setQrCodePrintConfirmData({
          image: base64QRCodeImage,
          totalLogisticsQuantity: logisticsQuantity,
          warehousingDate: dueDate,
          productName,
          productStandard: `${productSizeName} / ${t(
            'text_standard_each_number_of_product',
            {
              number: boxQuantity,
            }
          )}`,
        });
        setShowQRCodePrintConfirmModal(true);
      }
    }
  };

  const handleChangeHistorySubmit = async () => {
    if (distributionHistory && originalDistributionHistory) {
      const {
        logisticsId,
        customerId,
        dueDate,
        warehouseId,
        logisticsQuantity,
      } = distributionHistory;
      const {
        logisticsQuantity: originalLogisticsQuantity,
      } = originalDistributionHistory;

      if (!logisticsQuantity) {
        handleShowAlertModal(t('msg_number_of_boxes_cant_not_changed_to_zero'));
        return;
      }

      const result = await putDistributionHistory(logisticsId, {
        projectId: project.id,
        customerId: customerId || '',
        warehouseId: warehouseId || '',
        dueDate: `${moment(dueDate).format('YYYY-MM-DD')}T00:00:00`,
        cnt: logisticsQuantity - originalLogisticsQuantity,
      });

      if (result) {
        handleChangePhysicalDistributionStatusReloadFlag();
      }
      handleChangeReloadFlag();
    }
  };

  const handleChangeManualCompleteSubmit = async () => {
    if (distributionHistory) {
      const dueDate = moment(distributionHistory.dueDate).format('YYYY-MM-DD');
      const today = moment().format('YYYY-MM-DD');
      const daysDiff = moment.duration(moment(today).diff(dueDate)).days();
      if (daysDiff < 0) {
        let meesage = '';
        if (distributionHistory.logisticsCategory === 'WAREHOUSING') {
          meesage = t('msg_can_not_logistics_completed_check_warehousing_date');
        } else if (distributionHistory.logisticsCategory === 'RELEASING') {
          meesage = t('msg_can_not_logistics_completed_check_release_date');
        }
        handleShowAlertModal(meesage);
        return;
      }

      const result = await postManualComplete({
        projectId: project.id,
        logisticsId: distributionHistory.logisticsId,
      });

      if (result) {
        handleChangePhysicalDistributionStatusReloadFlag();
      }
      handleChangeReloadFlag();
    }
  };

  const handleDeleteHistorySubmit = async () => {
    if (distributionHistory) {
      const result = await deleteDistributionHistory(
        distributionHistory.logisticsId,
        {
          projectId: project.id,
          logisticsId: distributionHistory.logisticsId,
        }
      );

      if (result) {
        handleChangePhysicalDistributionStatusReloadFlag();
      }
      handleChangeReloadFlag();
    }
  };

  let showMoreButton = false;
  let isWarehousingOrRelease = false;
  if (distributionHistory) {
    const {
      logisticsCategory,
      status,
      logisticsExecutionQuantity,
      deleteFlag,
    } = distributionHistory;
    if (
      logisticsCategory === 'WAREHOUSING' ||
      logisticsCategory === 'RELEASING'
    ) {
      isWarehousingOrRelease = true;
      if (status === 'READY' && !logisticsExecutionQuantity && !deleteFlag) {
        showMoreButton = true;
      }
    }
  }

  if (!loadWarehouseList || !loadReleaseClientList) {
    return <></>;
  }

  return (
    <>
      <div className="d-flex align-items-center mb-24pt">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_detailed_history')}
          </h3>
        </div>
        {distributionHistory && (
          <>
            <div className="media flex-nowrap align-items-center">
              <span className="avatar avatar-sm mr-2">
                <ProfileAvatar
                  profileImgUrl={''}
                  userName={
                    distributionHistory.updateId || distributionHistory.registId
                  }
                />
              </span>
              <div className="media-body mr-4">
                <strong className="text-dark">
                  {distributionHistory.updateId || distributionHistory.registId}
                </strong>
              </div>
            </div>
            {distributionHistory.logisticsCategory === 'WAREHOUSING' &&
              distributionHistory.status === 'READY' &&
              !distributionHistory.deleteFlag &&
              !editMode && (
                <>
                  {user.userType === 'OWNER' && (
                    <a
                      className="btn btn-rounded btn-outline-accent mr-2"
                      onClick={() => setShowManualCompleteConfirmModal(true)}
                    >
                      <MaterialIcon name={'arrow_downward'} align={'left'} />
                      {t('text_warehousing_completed')}
                    </a>
                  )}
                  <a
                    className="btn btn-rounded btn-outline-dark mr-2"
                    onClick={handleClickQRPrinting}
                  >
                    <MaterialIcon name={'qr_code'} align={'left'} />
                    {t('text_printing_qr_code')}
                  </a>
                </>
              )}
            {distributionHistory.logisticsCategory === 'RELEASING' &&
              distributionHistory.status === 'READY' &&
              !distributionHistory.deleteFlag &&
              !editMode && (
                <>
                  {user.userType === 'OWNER' && (
                    <a
                      className="btn btn-rounded btn-outline-accent mr-2"
                      onClick={() => setShowManualCompleteConfirmModal(true)}
                    >
                      <MaterialIcon name={'arrow_downward'} align={'left'} />
                      {t('text_release_completed')}
                    </a>
                  )}
                </>
              )}
            {showMoreButton && (
              <ButtonGroup>
                <Dropdown
                  onToggle={handleToggle}
                  onSelect={handleSelectFunction}
                >
                  <DropdownToggle
                    as={'a'}
                    className="btn btn-rounded btn-outline-dark"
                  >
                    {t('text_see_more_details')}
                  </DropdownToggle>
                  <DropdownMenu align={'right'} ref={dropdown}>
                    <Dropdown.Item
                      className="text-primary"
                      eventKey={FUNCTION_EDIT}
                    >
                      {!editMode && t('text_edit_history_detail')}
                      {editMode && t('text_cancel_edit_history_detail')}
                    </Dropdown.Item>
                    {distributionHistory.logisticsCategory === 'WAREHOUSING' &&
                      distributionHistory.status === 'READY' &&
                      distributionHistory.logisticsExecutionQuantity === 0 && (
                        <Dropdown.Item
                          className="text-danger"
                          eventKey={FUNCTION_DELETE}
                        >
                          {t('text_delete')}
                        </Dropdown.Item>
                      )}
                  </DropdownMenu>
                </Dropdown>
              </ButtonGroup>
            )}
          </>
        )}
      </div>
      <div className="contents-view">
        {!distributionHistory && (
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>
            <em className="empty-txt m-0">
              {t('msg_selection_history_from_the_list_on_the_left')}
            </em>
          </div>
        )}
        {distributionHistory && (
          <>
            {distributionHistory.logisticsCategory === 'WAREHOUSING' && (
              <FloatPhysicalDistributionHistoryWarehousing
                distributionHistory={distributionHistory}
                editMode={editMode}
                warehouseList={warehouseList}
                onChangeDistributionHistory={setDistributionHistory}
              />
            )}
            {distributionHistory.logisticsCategory === 'RELEASING' && (
              <FloatPhysicalDistributionHistoryRelease
                distributionHistory={distributionHistory}
                editMode={editMode}
                releaseClientList={releaseClientList}
                onChangeDistributionHistory={setDistributionHistory}
              />
            )}
            {distributionHistory.logisticsCategory === 'ADJUSTMENT' && (
              <FloatPhysicalDistributionHistoryAdjustment
                distributionHistory={distributionHistory}
              />
            )}
            {distributionHistory.logisticsCategory === 'MOVE' && (
              <FloatPhysicalDistributionHistoryMovement
                distributionHistory={distributionHistory}
              />
            )}
            {editMode && (
              <div className="my-32pt">
                <div className="d-flex align-items-center justify-content-center">
                  <a
                    className="btn btn-outline-accent btn-rounded ml-0"
                    onClick={() => setShowHistoryChangeConfirmModal(true)}
                  >
                    {t('text_do_edit')}
                  </a>
                </div>
              </div>
            )}
            {isWarehousingOrRelease && (
              <>
                <div className="page-separator mt-5">
                  <div className="page-separator__text">
                    {t('text_detailed_history_before_edit')}
                  </div>
                </div>
                {!loadWarehouseList && <></>}
                {loadWarehouseList &&
                  distributionRevisedHistoryList.length === 0 && (
                    <div className="empty-state border-top-0">
                      <p className="empty-icon m-0">
                        <span className="material-icons-outlined">
                          error_outline
                        </span>
                      </p>
                      <em className="empty-txt m-0">
                        {t('msg_not_exist_histories')}
                      </em>
                    </div>
                  )}
                {loadWarehouseList &&
                  distributionRevisedHistoryList.length > 0 &&
                  distributionRevisedHistoryList.map(
                    (distributionRevisedHistory) => (
                      <div
                        key={distributionRevisedHistory.logDate}
                        className="accordion js-accordion accordion--boxed list-group-flush"
                      >
                        <FloatPhysicalDistributionRevisedHistoryItem
                          distributionHistory={distributionHistory}
                          distributionRevisedHistory={
                            distributionRevisedHistory
                          }
                        />
                      </div>
                    )
                  )}
              </>
            )}
          </>
        )}
      </div>
      <ConfirmModal
        show={showManualCompleteConfirmModal}
        onHide={() => setShowManualCompleteConfirmModal(false)}
        onClickConfirm={handleChangeManualCompleteSubmit}
      >
        {distributionHistory?.logisticsCategory === 'WAREHOUSING' &&
          t('msg_sure_want_to_completed_warehousing')}
        {distributionHistory?.logisticsCategory === 'RELEASING' &&
          t('msg_sure_want_to_completed_release')}
      </ConfirmModal>
      <ConfirmModal
        show={showHistoryDeleteConfirmModal}
        onHide={() => setShowHistoryDeleteConfirmModal(false)}
        onClickConfirm={handleDeleteHistorySubmit}
      >
        {t('msg_really_sure_want_to_delete')}
      </ConfirmModal>{' '}
      <ConfirmModal
        show={showHistoryChangeConfirmModal}
        onHide={() => setShowHistoryChangeConfirmModal(false)}
        onClickConfirm={handleChangeHistorySubmit}
      >
        {t('msg_sure_want_to_change_detailed_history')}
      </ConfirmModal>
      <QRCodePrintConfirmModal
        show={showQRCodePrintConfirmModal}
        onHide={() => setShowQRCodePrintConfirmModal(false)}
        qrCodePrintConfirmData={qrCodePrintConfirmData}
      />
      <AlertModal show={showAlertModal} onHide={() => setShowAlertModal(false)}>
        <NewlineText text={alertMessage} />
      </AlertModal>
    </>
  );
}

export default FloatPhysicalDistributionHistory;
