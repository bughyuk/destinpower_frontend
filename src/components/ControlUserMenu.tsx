import { ControlUserTab } from '@/modules/setup/types';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import React from 'react';

type ControlUserMenuProps = {
  selected: boolean;
  onClickMenu: (eventKey: ControlUserTab) => void;
  onCancel: () => void;
};

function ControlUserMenu({
  selected,
  onClickMenu,
  onCancel,
}: ControlUserMenuProps) {
  return (
    <>
      <div className={'context-menu type-row'}>
        <div className="nav">
          {selected && <SelectedMenu onCancel={onCancel} />}
          {!selected && <NonSelectedMenu onClickMenu={onClickMenu} />}
        </div>
      </div>
    </>
  );
}

type SelectedMenuProps = {
  onCancel: () => void;
};

function SelectedMenu({ onCancel }: SelectedMenuProps) {
  const { t } = useTranslation();
  return (
    <>
      <a className="d-flex align-items-center" onClick={onCancel}>
        <MaterialIcon name={'person_search'} align={'left'} />
        {t('text_selection_cancel_user')}
      </a>
    </>
  );
}

type NonSelectedMenuProps = {
  onClickMenu: (eventKey: ControlUserTab) => void;
};

function NonSelectedMenu({ onClickMenu }: NonSelectedMenuProps) {
  const { t } = useTranslation();
  return (
    <>
      <a
        className="d-flex align-items-center"
        onClick={() => {
          onClickMenu('info');
        }}
      >
        <MaterialIcon name={'person_search'} align={'left'} />
        {t('text_user_info')}
      </a>
      <a
        className="d-flex align-items-center"
        onClick={() => {
          onClickMenu('message');
        }}
      >
        <MaterialIcon name={'mail_outline'} align={'left'} />
        {t('text_send_message')}
      </a>
      <a
        className="d-flex align-items-center"
        onClick={() => {
          onClickMenu('trace');
        }}
      >
        <MaterialIcon name={'timeline'} align={'left'} />
        {t('text_traffic_tracking')}
      </a>
    </>
  );
}

export default ControlUserMenu;
