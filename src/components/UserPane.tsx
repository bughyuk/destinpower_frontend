import React from 'react';
import { useControlMode } from '@/modules/setup/hook';
import RealtimeUserContent from '@/components/RealtimeUserContent';
import {
  CONTROL_MODE_TENSE_HISTORY,
  CONTROL_MODE_TENSE_REALTIME,
} from '@/modules/setup/types';
import HistoryUserContent from '@/components/HistoryUserContent';

function UserPane() {
  const { tense } = useControlMode();

  return (
    <div className="tab-pane sm-user active">
      <div className="user-inner">
        {tense === CONTROL_MODE_TENSE_REALTIME && <RealtimeUserContent />}
        {tense === CONTROL_MODE_TENSE_HISTORY && <HistoryUserContent />}
      </div>
    </div>
  );
}

export default UserPane;
