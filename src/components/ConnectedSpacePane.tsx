import React from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { useControlSpace, useControlProject } from '@/modules/map/hook';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import {
  SOLUTION_TYPE_SMART_CONTROL,
  SOLUTION_TYPE_SMART_FACTORY,
  SOLUTION_TYPE_SMART_HOSPITAL,
  SOLUTION_TYPE_SMART_OFFICE,
  SOLUTION_TYPE_SMART_RETAIL,
} from '@/modules/project/types';
import { Space } from '@/modules/map/types';
import classNames from 'classnames';
import { Badge } from 'react-bootstrap';
import moment from 'moment';
import { useProjectPane } from '@/modules/project/hook';
import { PANE_STATUS_PROJECT_CONNECT_TO_SPACE } from '@/utils/constants/common';
import { useUser } from '@/modules/user/hook';

function ConnectedSpacePane() {
  return (
    <div className="tab-pane sm-projects active">
      <div className="project-inner">
        <Header />
        <Body />
      </div>
    </div>
  );
}

function Header() {
  const { t } = useTranslation();
  const history = useHistory();
  const { project } = useControlProject();

  let solutionText = '';

  switch (project.solutionType) {
    case SOLUTION_TYPE_SMART_CONTROL:
      solutionText = t('text_smart_control');
      break;
    case SOLUTION_TYPE_SMART_OFFICE:
      solutionText = t('text_smart_office');
      break;
    case SOLUTION_TYPE_SMART_HOSPITAL:
      solutionText = t('text_smart_hospital');
      break;
    case SOLUTION_TYPE_SMART_FACTORY:
      solutionText = t('text_smart_factory');
      break;
    case SOLUTION_TYPE_SMART_RETAIL:
      solutionText = t('text_smart_retail');
      break;
  }

  return (
    <>
      <div className="container-fluid py-4">
        <div className="d-flex align-items-center mb-2">
          <div className="flex d-flex flex-sm-row align-items-center mb-0">
            <a
              className="circle-pin pr-2"
              onClick={() => history.replace('/home')}
            >
              <MaterialIcon name={'arrow_back'} />
            </a>
            <div className="mr-24pt">
              <h3 className="mb-0">{project.name}</h3>
            </div>
          </div>
        </div>
        <div className="board-title">
          <p>{project.note}</p>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row align-items-center mb-4">
          <div className="col text-right">
            <select className="form-none" disabled>
              <option value="">{solutionText}</option>
            </select>
          </div>
        </div>
      </div>
    </>
  );
}

function Body() {
  const { t } = useTranslation();
  const { user } = useUser();
  const { project } = useControlProject();
  const history = useHistory();
  const { handleChangePaneStatus } = useProjectPane();

  const handleSpaceConnectToProject = () => {
    handleChangePaneStatus(PANE_STATUS_PROJECT_CONNECT_TO_SPACE);
    history.replace('/project');
  };

  return (
    <div className="container-fluid">
      <div className="tab-content">
        <div className="tab-pane active">
          <div className="list-group block-list dragula-list mb-4">
            {project.spaceList.length === 0 && (
              <em className="none-list mb-4">{t('msg_no_space_connected')}</em>
            )}
            {project.spaceList.map((space) => (
              <SpaceItem key={space.id} {...space} />
            ))}
          </div>
          {user.userType === 'OWNER' && (
            <div className="d-flex align-items-center justify-content-center">
              <a className="btn-row-add" onClick={handleSpaceConnectToProject}>
                <MaterialIcon name={'insert_link'} />{' '}
                {t('text_space_connect_to_project')}
              </a>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

function SpaceItem({ id, mappingId, name, registDate }: Space) {
  const { t } = useTranslation();
  const { space, handleSetSpace } = useControlSpace();

  const handleChangeControlSpace = () => {
    handleSetSpace({
      spaceMetaId: id,
      spaceMappingId: mappingId,
      floorsMapId: '',
    });
  };

  return (
    <div
      className={classNames('list-group-item d-flex', {
        active: space.spaceMappingId === mappingId,
      })}
    >
      <div className="flex d-flex align-items-center mr-16pt">
        <div className="flex" onClick={handleChangeControlSpace}>
          <a className="card-title">{name}</a>
          <div className="card-subtitle text-50">
            <Badge variant={'accent'}>{t('text_distribution_status')}</Badge>
            {` ${moment(registDate).format('YYYY.MM.DD')}`}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ConnectedSpacePane;
