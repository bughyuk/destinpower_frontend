import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

type PhysicalDistributionListSearchProps = {
  placeholderTextKey: string;
  onSubmit: (searchKeyword: string) => void;
};

function PhysicalDistributionListSearch({
  placeholderTextKey,
  onSubmit,
}: PhysicalDistributionListSearchProps) {
  const { t } = useTranslation();
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    handleSubmit();
  }, [searchKeyword]);

  const handleSubmit = () => {
    onSubmit(searchKeyword.toLowerCase());
  };

  return (
    <div className="cell serach-box">
      <input
        type="text"
        placeholder={t(placeholderTextKey)}
        value={searchKeyword}
        onChange={(e) => setSearchKeyword(e.target.value)}
      />
      <a className="btn btn-secondary btn-sm ml-auto" onClick={handleSubmit}>
        {t('text_search')}
      </a>
    </div>
  );
}

export default PhysicalDistributionListSearch;
