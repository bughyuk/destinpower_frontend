import React, { useEffect, useRef, useState } from 'react';
import Flatpickr from 'react-flatpickr';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import { useTranslation } from 'react-i18next';
import { useControlProject } from '@/modules/map/hook';
import { usePhysicalDistributionStatistics } from '@/modules/physical_distribution/hook';
import {
  fetchLogisticsStockByProducts,
  fetchLogisticsStockChange,
  fetchLogisticsSummary,
  fetchLogisticsTrend,
} from '@/api/statistics';
import {
  StatLogisticsStockByProduct,
  StatLogisticsStockChange,
  StatLogisticsSummary,
  StatLogisticsTrend,
} from '@/modules/statistics/types';
import { CommonUtils } from '@/utils';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import { Korean } from 'flatpickr/dist/l10n/ko';

const moment = extendMoment(Moment);

function FloatStatisticsSmartFactory() {
  return (
    <>
      <LogisticsInfo />
    </>
  );
}

function LogisticsInfo() {
  const { t } = useTranslation();
  const flatpickrRef = useRef<Flatpickr>(null);
  const [period, setPeriod] = useState<string[]>([
    moment().add(-6, 'days').format('YYYY-MM-DD'),
    moment().format('YYYY-MM-DD'),
  ]);
  const { project } = useControlProject();
  const { selectedWarehouseId } = usePhysicalDistributionStatistics();
  const [
    logisticsSummary,
    setLogisticsSummary,
  ] = useState<StatLogisticsSummary>({
    warehouseStockTotal: 0,
    logisticsCounts: {
      warehousingCompleteCnt: 0,
      releasingCompleteCnt: 0,
      logisticsIncompleteCnt: 0,
    },
  });
  const [warehousingTrendList, setWarehousingTrendList] = useState<
    StatLogisticsTrend[]
  >([]);
  const [releaseTrendList, setReleaseTrendList] = useState<
    StatLogisticsTrend[]
  >([]);
  const [stockChangeList, setStockChangeList] = useState<
    StatLogisticsStockChange[]
  >([]);
  const [
    stockByProductList,
    setStockByProductList,
  ] = useState<StatLogisticsStockByProduct>({
    totalQuantitySum: 0,
    data: [],
  });

  const handleFetchLogisticsSummary = async () => {
    const data = await fetchLogisticsSummary({
      projectId: project.id,
      warehouseId: selectedWarehouseId,
    });
    if (data) {
      setLogisticsSummary(data);
    }
  };

  const handleFetchLogisticsTrend = async () => {
    setWarehousingTrendList(
      await fetchLogisticsTrend({
        projectId: project.id,
        warehouseId: selectedWarehouseId,
        searchStartDates: period[0],
        searchEndDates: period[1],
        logisticsCategory: 'WAREHOUSING',
        status: 'COMPLETE',
      })
    );
    setReleaseTrendList(
      await fetchLogisticsTrend({
        projectId: project.id,
        warehouseId: selectedWarehouseId,
        searchStartDates: period[0],
        searchEndDates: period[1],
        logisticsCategory: 'RELEASING',
        status: 'COMPLETE',
      })
    );
  };

  const handleFetchLogisticsStockChange = async () => {
    setStockChangeList(
      await fetchLogisticsStockChange({
        projectId: project.id,
        warehouseId: selectedWarehouseId,
        searchStartDates: period[0],
        searchEndDates: period[1],
      })
    );
  };

  const handleFetchLogisticsStockByProducts = async () => {
    const data = await fetchLogisticsStockByProducts({
      projectId: project.id,
      warehouseId: selectedWarehouseId,
      searchStartDates: period[0],
      searchEndDates: period[1],
    });

    if (data) {
      setStockByProductList(data);
    }
  };

  useEffect(() => {
    handleFetchLogisticsSummary();
    handleFetchLogisticsTrend();
    handleFetchLogisticsStockChange();
    handleFetchLogisticsStockByProducts();
  }, [period, selectedWarehouseId]);

  useEffect(() => {
    for (const chart of Highcharts.charts) {
      if (chart) {
        chart.reflow();
      }
    }
  }, []);

  return (
    <div className="ch-group">
      <div className="row mb-5">
        <div className="col-md-6 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_total_number_of_stock')}</h6>
              <div className="cell">
                <em>
                  {CommonUtils.numberWithCommas(
                    logisticsSummary.warehouseStockTotal
                  )}
                </em>
                <span className="text-muted">{t('text_count')}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_warehousing')}</h6>
              <div className="cell">
                <em>
                  {CommonUtils.numberWithCommas(
                    logisticsSummary.logisticsCounts.warehousingCompleteCnt
                  )}
                </em>
                <span className="text-muted">{t('text_case')}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_release')}</h6>
              <div className="cell">
                <em>
                  {CommonUtils.numberWithCommas(
                    logisticsSummary.logisticsCounts.releasingCompleteCnt
                  )}
                </em>
                <span className="text-muted">{t('text_case')}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_incomplete_processing')}</h6>
              <div className="cell">
                <em>
                  {CommonUtils.numberWithCommas(
                    logisticsSummary.logisticsCounts.logisticsIncompleteCnt
                  )}
                </em>
                <span className="text-muted">{t('text_case')}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="sort-cover mb-3">
        <div className="set-date">
          <span className="material-icons-outlined font-size-16pt">
            date_range
          </span>
          <label className="">{t('text_period')}</label>
          <Flatpickr
            options={{
              mode: 'range',
              altInput: true,
              altInputClass: 'btn-link statistics-flatpickr',
              altFormat: 'Y-m-d',
              locale: Korean,
            }}
            ref={flatpickrRef}
            value={period}
            onChange={(dates) => {
              if (dates.length === 2) {
                setPeriod(
                  dates.map((date) => moment(date).format('YYYY-MM-DD'))
                );
              }
            }}
            onClose={(dates) => {
              if (dates.length < 2) {
                flatpickrRef.current?.flatpickr.setDate(period);
              }
            }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body">
              <h6>{t('text_number_of_warehousing_trend')}</h6>
              <div className="graph-box">
                <HighchartsReact
                  highcharts={Highcharts}
                  options={{
                    chart: {
                      type: 'line',
                      events: {
                        render: function (e: any) {
                          e.target.reflow();
                        },
                      },
                    },
                    title: null,
                    credits: {
                      enabled: false,
                    },
                    plotOptions: {
                      series: {
                        animation: false,
                      },
                      line: {
                        marker: {
                          enabled: true,
                          radius: 4,
                        },
                      },
                    },
                    xAxis: {
                      categories: warehousingTrendList.map((trend) =>
                        moment(trend.date).format('MM-DD')
                      ),
                    },
                    yAxis: {
                      minRange: 1,
                      min: 0,
                      title: null,
                    },
                    tooltip: {
                      pointFormat: '{series.name}: <b>{point.y}</b>',
                    },
                    legend: {
                      enabled: false,
                    },
                    series: [
                      {
                        name: t('text_number_of_warehousing'),
                        data: warehousingTrendList.map(
                          (trend) => trend.totalQuantity
                        ),
                        color: '#2E80DF',
                      },
                    ],
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body">
              <h6>{t('text_number_of_release_case_trend')}</h6>
              <div className="graph-box">
                <HighchartsReact
                  highcharts={Highcharts}
                  options={{
                    chart: {
                      type: 'line',
                      events: {
                        render: function (e: any) {
                          e.target.reflow();
                        },
                      },
                    },
                    title: null,
                    credits: {
                      enabled: false,
                    },
                    plotOptions: {
                      series: {
                        animation: false,
                      },
                      line: {
                        marker: {
                          marker: {
                            enabled: true,
                            radius: 4,
                          },
                        },
                      },
                    },
                    xAxis: {
                      categories: releaseTrendList.map((trend) =>
                        moment(trend.date).format('MM-DD')
                      ),
                    },
                    yAxis: {
                      minRange: 1,
                      min: 0,
                      title: null,
                    },
                    tooltip: {
                      pointFormat: '{series.name}: <b>{point.y}</b>',
                    },
                    legend: {
                      enabled: false,
                    },
                    series: [
                      {
                        name: t('text_number_of_release_case'),
                        data: releaseTrendList.map(
                          (trend) => trend.totalQuantity
                        ),
                        color: '#2E80DF',
                      },
                    ],
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body">
              <h6>{t('text_stock_by_product')}</h6>
              <div className="graph-box">
                <HighchartsReact
                  highcharts={Highcharts}
                  options={{
                    credits: {
                      enabled: false,
                    },
                    chart: {
                      type: 'pie',
                      events: {
                        render: function (e: any) {
                          e.target.reflow();
                        },
                      },
                    },
                    title: null,
                    plotOptions: {
                      series: {
                        animation: false,
                      },
                      pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                          enabled: false,
                        },
                        showInLegend: true,
                      },
                    },
                    tooltip: {
                      pointFormat: '<b>{point.percentage:.0f}%</b>',
                    },
                    legend: {
                      enabled: false,
                    },
                    series: [
                      {
                        name: 'Percent',
                        data: stockByProductList.data.map(
                          ({ productName, percent }) => {
                            return {
                              name: productName,
                              y: percent,
                            };
                          }
                        ),
                      },
                    ],
                  }}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body">
              <h6>{t('text_stock_change')}</h6>
              <div className="graph-box">
                <HighchartsReact
                  highcharts={Highcharts}
                  options={{
                    chart: {
                      type: 'line',
                      events: {
                        render: function (e: any) {
                          e.target.reflow();
                        },
                      },
                    },
                    title: null,
                    credits: {
                      enabled: false,
                    },
                    plotOptions: {
                      series: {
                        animation: false,
                      },
                      line: {
                        marker: {
                          enabled: true,
                          radius: 4,
                        },
                      },
                    },
                    xAxis: {
                      categories: stockChangeList.map((stockChange) =>
                        moment(stockChange.date).format('MM-DD')
                      ),
                    },
                    yAxis: {
                      minRange: 1,
                      min: 0,
                      title: null,
                    },
                    tooltip: {
                      pointFormat: '{series.name}: <b>{point.y}</b>',
                    },
                    legend: {
                      enabled: false,
                    },
                    series: [
                      {
                        name: t('text_number_of_stock'),
                        data: stockChangeList.map(
                          (stockChange) => stockChange.totalQuantity
                        ),
                        color: '#2E80DF',
                      },
                    ],
                  }}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FloatStatisticsSmartFactory;
