import React, { useState } from 'react';
import { PANE_STATUS_STATUS_BOARD, PaneStatus } from '@/utils/constants/common';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import moment from 'moment';
import { Collapse } from 'react-bootstrap';
import classNames from 'classnames';

type EntryDetailProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function EntryDetail({ onChangeStatus }: EntryDetailProps) {
  return (
    <>
      <Header onChangeStatus={onChangeStatus} />
      <Body />
    </>
  );
}

type HeaderProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function Header({ onChangeStatus }: HeaderProps) {
  const { t } = useTranslation();

  return (
    <div className="container-fluid py-4">
      <div className="d-flex align-items-center mb-2">
        <div className="flex d-flex flex-sm-row align-items-center mb-0">
          <a
            className="circle-pin pr-2"
            onClick={() => onChangeStatus(PANE_STATUS_STATUS_BOARD)}
          >
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{t('text_total_number_of_people')}</h3>
          </div>
        </div>
      </div>
      <div className="board-title">
        <p>
          {moment().format('yyyy.MM.DD')}{' '}
          {t('text_current_total_number_of_people')}{' '}
          <strong className="text-iden ml-1">0</strong>
          {t('text_unit_of_person')}
        </p>
      </div>
    </div>
  );
}

function Body() {
  const { t } = useTranslation();
  return (
    <div className="container-fluid">
      <div className="form-group mb-4">
        <div className="d-flex align-items-center mb-2">
          <h5 className="d-flex flex m-0">{t('text_branch_office')}</h5>
          <span className="v">
            {t('text_total')} <strong className="text-iden">0</strong>
            {t('msg_there_are_number_branch_office')}
          </span>
        </div>
        <select name="category" className="form-control custom-select">
          <option value="">{t('text_the_whole')}</option>
          <option value="">지점 A</option>
          <option value="">지점 B</option>
        </select>
      </div>
      <div className="mb-4">
        <div className="d-flex align-items-center mb-2">
          <h5 className="d-flex flex m-0">지점 A</h5>
          <span className="v">
            {t('text_total')} <strong className="text-iden">0</strong>
            {t('text_unit_of_person')}
          </span>
        </div>

        <ul className="sidebar-menu">
          <EntryItem />
        </ul>
      </div>
    </div>
  );
}

function EntryItem() {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);

  return (
    <li
      className={classNames('sidebar-menu-item', {
        open: isOpen,
      })}
    >
      <a className="sidebar-menu-button" onClick={() => setOpen(!isOpen)}>
        <span className="dot dot-on"></span>
        A-1241
        <span className="sidebar-menu-badge text-success ml-auto mr-2">
          출근
        </span>
        <span className="sidebar-menu-toggle-icon"></span>
      </a>
      <Collapse in={isOpen}>
        <div className="sidebar-submenu sm-indent">
          <div className="cover">
            <div className="col-6 p-2">
              <span className="text-50">{t('text_go_to_office_time')}</span>
              <p className="m-0">08:47</p>
            </div>
            <div className="col-6 p-2">
              <span className="text-50">{t('text_leave_the_office_time')}</span>
              <p className="m-0">-</p>
            </div>
          </div>
        </div>
      </Collapse>
    </li>
  );
}

export default EntryDetail;
