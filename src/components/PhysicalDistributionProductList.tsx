import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionItem,
  usePhysicalDistributionProduct,
} from '@/modules/physical_distribution/hook';
import {
  PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
  Product,
} from '@/modules/physical_distribution/types';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import { fetchProducts } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import Preloader from '@/components/Preloader';
import PhysicalDistributionListSelectBox from '@/components/PhysicalDistributionListSelectBox';

function PhysicalDistributionProductList() {
  const { t } = useTranslation();
  const { load: loadItemList, itemList } = usePhysicalDistributionItem();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [itemId, setItemId] = useState('');
  const { project } = useControlProject();
  const { categoryIdx } = usePhysicalDistributionCategory();
  const {
    selectedProduct,
    directSelectedProduct,
    reloadFlag,
    handleSetProduct,
    handleSetDirectProduct,
  } = usePhysicalDistributionProduct();
  const [load, setLoad] = useState(false);
  const [productList, setProductList] = useState<Product[]>([]);
  const [filterList, setFilterList] = useState<Product[]>([]);

  useEffect(() => {
    return () => {
      handleSetProduct(null);
      handleSetDirectProduct(null);
    };
  }, []);

  useEffect(() => {
    handleSetProduct(null);
    setFilterList(
      productList.filter((product) => {
        return (
          product.productName.toLowerCase().indexOf(searchKeyword) > -1 &&
          (!itemId || product.productCategoryId === itemId) &&
          (categoryIdx === PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING
            ? !product.deleteFlag
            : true)
        );
      })
    );
  }, [searchKeyword, itemId, productList]);

  useEffect(() => {
    handleFetchProductList();
  }, [reloadFlag]);

  useEffect(() => {
    if (directSelectedProduct) {
      handleSetProduct(directSelectedProduct);
    }
  }, [filterList, directSelectedProduct]);

  const handleFetchProductList = async () => {
    setLoad(false);
    const data = await fetchProducts(project.id);
    setProductList(data);
    setLoad(true);
  };

  const handleClickProduct = (clickProduct: Product) => {
    let product: Product | null = null;
    if (selectedProduct?.productId !== clickProduct.productId) {
      product = clickProduct;
    }
    handleSetProduct(product);
  };

  if (!loadItemList) {
    return <></>;
  }

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'place_holder_search_product'}
          onSubmit={setSearchKeyword}
        />
        <PhysicalDistributionListSelectBox
          all={true}
          options={itemList.map((item) => {
            return {
              value: item.productCategoryId,
              text: item.productCategoryName,
            };
          })}
          onChange={setItemId}
        />
      </div>
      {!load && <Preloader />}
      {load && filterList.length === 0 && (
        <em className="none-list mb-4">{t('msg_product_empty')}</em>
      )}
      {load && filterList.length > 0 && (
        <div className="list-group row-list list-group-flush mb-4">
          {filterList.map((product) => (
            <ProductItem
              key={product.productId}
              {...product}
              activeId={
                categoryIdx === PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT
                  ? selectedProduct?.productId || ''
                  : ''
              }
              onClick={() => handleClickProduct(product)}
            />
          ))}
        </div>
      )}
    </>
  );
}

type ProductItemProps = Product & {
  activeId: string;
  onClick: () => void;
};

function ProductItem({
  productId,
  productName,
  boxQuantity,
  minQuantity,
  totalQuantity,
  productWeight,
  productSizeName,
  imgUrl,
  activeId,
  deleteFlag,
  onClick,
}: ProductItemProps) {
  const { t } = useTranslation();

  return (
    <div
      className={classNames('list-group-item d-flex', {
        active: productId === activeId,
      })}
      onClick={() => onClick()}
    >
      <a className="flex d-flex align-items-center">
        <div className="avatar mr-12pt blank-img">
          <img src={imgUrl} className="avatar-img rounded" />
        </div>
        <div className="flex list-els">
          <h4
            className={classNames('card-title', {
              'text-30': deleteFlag,
            })}
          >
            {deleteFlag && `[${t('text_delete')}] `}
            {productName}
          </h4>
          <div className="card-subtitle text-50 d-flex align-items-center">
            <div className="card-subtitle text-50">
              <small className="mr-2">{productSizeName}</small>
              <small className="mr-2">{`${productWeight}g x ${boxQuantity}`}</small>
            </div>
          </div>
        </div>
        <div
          className={classNames('pd-count', {
            'plus-item': minQuantity < totalQuantity,
            'minus-item': minQuantity > totalQuantity,
          })}
        >
          {totalQuantity}
        </div>
      </a>
    </div>
  );
}

export default PhysicalDistributionProductList;
