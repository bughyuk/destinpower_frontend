import React, { CSSProperties, useEffect, useState } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { useActiveMenu } from '@/modules/setup/hook';
import { MENU_IDX_STATISTICS } from '@/utils/constants/common';
import FloatStatisticsSmartOffice from '@/components/FloatStatisticsSmartOffice';

function LeftWidePane() {
  const { showWide, menuIdx, handleMenuResetActive } = useActiveMenu();
  const [sidebarWideStyle, setSidebarWideStyle] = useState<CSSProperties>({});

  useEffect(() => {
    return () => {
      handleMenuResetActive();
    };
  }, []);

  useEffect(() => {
    if (showWide) {
      setSidebarWideStyle({
        transform: 'translateZ(0)',
      });
    } else {
      setSidebarWideStyle({});
    }
  }, [showWide]);

  return (
    <div className={'sidebar-depth sidebar-wide'} style={sidebarWideStyle}>
      <PerfectScrollbar>
        <div className="sidebar-depth-wrap">
          {menuIdx === MENU_IDX_STATISTICS && <FloatStatisticsSmartOffice />}
        </div>
      </PerfectScrollbar>
    </div>
  );
}

export default LeftWidePane;
