import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { Collapse } from 'react-bootstrap';
import {
  DistributionHistory,
  DistributionRevisedHistory,
} from '@/modules/physical_distribution/types';
import moment from 'moment';
import FloatPhysicalDistributionRevisedHistoryWarehousingItem from '@/components/FloatPhysicalDistributionRevisedHistoryWarehousingItem';
import FloatPhysicalDistributionRevisedHistoryReleaseItem from '@/components/FloatPhysicalDistributionRevisedHistoryReleaseItem';

type FloatPhysicalDistributionHistoryEditItemProps = {
  distributionHistory: DistributionHistory;
  distributionRevisedHistory: DistributionRevisedHistory;
};

function FloatPhysicalDistributionRevisedHistoryItem({
  distributionHistory,
  distributionRevisedHistory,
}: FloatPhysicalDistributionHistoryEditItemProps) {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);
  const { logisticsCategory } = distributionHistory;
  const { logDate, workerId } = distributionRevisedHistory;

  return (
    <div
      className={classNames('accordion__item mb-4', {
        open: isOpen,
      })}
    >
      <a
        className="accordion__toggle collapsed"
        onClick={() => setOpen(!isOpen)}
      >
        <span className="d-flex">
          {moment(logDate).format('YYYY-MM-DD')}{' '}
          <span className="text-50 ml-2">
            {moment(logDate).format('HH:mm')}
          </span>
        </span>
        <span className="badge-pich ml-2 font-weight-bolder">{workerId}</span>
        <span className="accordion__toggle-icon material-icons ml-auto">
          keyboard_arrow_down
        </span>
      </a>
      <Collapse in={isOpen}>
        <div className="accordion__menu">
          <div className="p-5 border-top-1">
            {logisticsCategory === 'WAREHOUSING' && (
              <FloatPhysicalDistributionRevisedHistoryWarehousingItem
                distributionHistory={distributionHistory}
                distributionRevisedHistory={distributionRevisedHistory}
              />
            )}
            {logisticsCategory === 'RELEASING' && (
              <FloatPhysicalDistributionRevisedHistoryReleaseItem
                distributionHistory={distributionHistory}
                distributionRevisedHistory={distributionRevisedHistory}
              />
            )}
          </div>
        </div>
      </Collapse>
    </div>
  );
}

export default FloatPhysicalDistributionRevisedHistoryItem;
