import React, { useEffect } from 'react';
import { useProjectPane } from '@/modules/project/hook';
import {
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_PROJECT_CONNECT_TO_SPACE,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import ProjectList from '@/components/ProjectList';
import ProjectRegister from '@/components/ProjectRegister';
import ProjectEdit from '@/components/ProjectEdit';
import ProjectConnect from '@/components/ProjectConnect';

function ProjectPane() {
  return (
    <div className="tab-pane sm-projects active">
      <div className="project-inner">
        <ProjectContent />
      </div>
    </div>
  );
}

function ProjectContent() {
  const { paneStatus, handleChangePaneStatus } = useProjectPane();

  useEffect(() => {
    return () => {
      handleChangePaneStatus(PANE_STATUS_LIST);
    };
  }, []);

  return (
    <>
      {paneStatus === PANE_STATUS_LIST && <ProjectList />}
      {paneStatus === PANE_STATUS_REGISTER && <ProjectRegister />}
      {paneStatus === PANE_STATUS_EDIT && <ProjectEdit />}
      {paneStatus === PANE_STATUS_PROJECT_CONNECT_TO_SPACE && (
        <ProjectConnect />
      )}
    </>
  );
}

export default ProjectPane;
