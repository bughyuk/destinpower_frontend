import React, { useEffect } from 'react';
import { useSpace } from '@/modules/space/hook';
import { useUser } from '@/modules/user/hook';
import { useHistory } from 'react-router-dom';
import { ValidUtils } from '@/utils';
import { useLogout } from '@/modules/access/logout/hook';
import { useProjectRegister } from '@/modules/project/hook';
import { postProjectConnectSpace, putProjectStepData } from '@/api/project';
import { Config } from '@/config';

type IframeMessage = {
  type: 'back' | 'nexton' | 'logout' | 'home';
};

function ProjectSpaceIndoorEditContainer() {
  const { user } = useUser();
  const { info } = useSpace();
  const {
    handleChangeRegisterStep,
    projectInfo,
    projectProduceStep,
    handleSetProjectInfo,
    handleSetConnectedMetaIds,
  } = useProjectRegister();
  const history = useHistory();
  const { handleLogout, logoutStatusCode } = useLogout();

  useEffect(() => {
    if (logoutStatusCode) {
      history.replace('/signin');
    }
  }, [logoutStatusCode]);

  const statusName = ValidUtils.validateHangul(user?.userName || '')
    ? user?.userName.length > 0
      ? user?.userName.substring(0, 1)
      : ''
    : user?.userName.length > 1
    ? user?.userName.substring(0, 1)
    : '';

  const lang = localStorage.getItem('language') || 'ko';

  const handleCommunicationMessage = (e: MessageEvent<IframeMessage>) => {
    e.stopPropagation();
    if (e.origin.indexOf(Config.map_server.indoor_edit_origin) > 0) {
      const data = e.data;
      if (data) {
        switch (data.type) {
          case 'back':
            handleChangeRegisterStep(4);
            break;
          case 'nexton':
            handleSaveProduceStepData();
            break;
          case 'logout':
            handleLogout();
            break;
          case 'home':
            history.replace('/home');
            break;
        }
      }
    }
  };

  const handleSaveProduceStepData = async () => {
    const projectId = projectInfo.projectId;
    if (projectId) {
      await putProjectStepData(projectId, {
        produceStepData: JSON.stringify({
          ...projectProduceStep,
          step: 6,
        }),
      });

      const metaId = info.space.id;

      if (!projectInfo.metaIds?.includes(metaId)) {
        await postProjectConnectSpace(projectId, metaId);
        const connectedMetaIds = [...(projectInfo.metaIds || []), metaId];
        handleSetConnectedMetaIds(connectedMetaIds);
        handleSetProjectInfo({
          ...projectInfo,
          metaIds: connectedMetaIds,
        });
      }

      handleChangeRegisterStep(6);
    }
  };

  useEffect(() => {
    window.removeEventListener('message', handleCommunicationMessage, true);
    window.addEventListener('message', handleCommunicationMessage, {
      once: true,
    });

    return () =>
      window.removeEventListener('message', handleCommunicationMessage);
  }, []);

  return (
    <>
      <iframe
        style={{
          height: '100vh',
        }}
        frameBorder={0}
        src={`${Config.map_server.indoor_edit_uri}/drawMap.jsp?userid=${user.userId}&metaid=${info.space.id}&lang=${lang}&surname=${statusName}&div=1`}
      />
    </>
  );
}

export default ProjectSpaceIndoorEditContainer;
