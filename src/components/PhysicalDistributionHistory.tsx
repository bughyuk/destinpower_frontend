import React from 'react';
import MaterialIcon from '@/components/MaterialIcon';
import { PhysicalDistributionListContentProps } from '@/components/PhysicalDistributionList';
import PhysicalDistributionHistoryList from '@/components/PhysicalDistributionHistoryList';

function PhysicalDistributionHistory({
  title,
  onClickBack,
}: PhysicalDistributionListContentProps) {
  return (
    <>
      <div className="container-fluid py-4">
        <div className="flex d-flex align-items-center">
          <a className="circle-pin pr-2" onClick={onClickBack}>
            <MaterialIcon name={'arrow_back'} />
          </a>
          <div className="mr-24pt">
            <h3 className="mb-0">{title}</h3>
          </div>
          {/*
          <OverlayTrigger
            placement={'left'}
            overlay={
              <Tooltip id={'filterTooltip'}>
                {t('text_filter_settings')}
              </Tooltip>
            }
          >
            <a className="btn btn-outline-dark ml-auto btn_layerpopup px-2 py-1">
              <span className="material-icons font-size-16pt">tune</span>
            </a>
          </OverlayTrigger>
          */}
        </div>
      </div>
      <PhysicalDistributionHistoryList />
    </>
  );
}

export default PhysicalDistributionHistory;
