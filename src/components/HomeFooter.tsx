import React from 'react';
import { useTranslation } from 'react-i18next';

function HomeFooter() {
  const { t } = useTranslation();

  return (
    <div className="footer">
      <div className="container page__container page-section d-flex flex-column">
        <div className="row">
          <div className="col-auto">
            <p className="text-70 brand mb-24pt py-2">
              <img
                className="brand-icon"
                src="/static/images/logo.svg"
                width="100px"
              />
              {t('text_company_name_ko')}
            </p>
          </div>
          <div className="col justify-content-end">
            <div className="d-flex justify-content-end info-cus">
              <div className="text-right">
                <a className="btn btn-link">Help Center</a>
              </div>
              <div className="text-right">
                <a className="btn btn-link">Developer Center</a>
              </div>
            </div>
          </div>
        </div>
        <p className="measure-lead-max text-50 small mr-8pt">
          {t('text_company_address_ko')}
        </p>
        <p className="mb-8pt d-flex">
          <a className="text-70 btn-link mr-8pt small">Terms</a>
          <a className="text-70 btn-link small">Privacy policy</a>
        </p>
        <p className="text-50 small mt-n1 mb-0">
          Copyright 2021 &copy; All rights reserved.
        </p>
      </div>
    </div>
  );
}

export default HomeFooter;
