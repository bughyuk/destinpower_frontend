import React, { useEffect } from 'react';
import ProjectInfoRegister from '@/components/ProjectInfoRegister';
import ProjectSpaceRegister from '@/components/ProjectSpaceRegister';
import { useProjectRegister } from '@/modules/project/hook';
import ProjectSolutionRegister from '@/components/ProjectSolutionRegister';
import ProjectSpaceAddressRegister from '@/components/ProjectSpaceAddressRegister';
import ProjectSpaceFileRegister from '@/components/ProjectSpaceFileRegister';

function ProjectRegister() {
  const { registerStep, handleSetRegisterInitialState } = useProjectRegister();

  useEffect(() => {
    return () => {
      handleSetRegisterInitialState();
    };
  }, []);

  let isSolutionRegister = false;
  if (registerStep === 6 || registerStep === 7) {
    isSolutionRegister = true;
  }

  return (
    <>
      {registerStep === 1 && <ProjectInfoRegister />}
      {registerStep === 2 && <ProjectSpaceRegister />}
      {registerStep === 3 && <ProjectSpaceAddressRegister />}
      {registerStep === 4 && <ProjectSpaceFileRegister />}
      {isSolutionRegister && <ProjectSolutionRegister />}
    </>
  );
}

export default ProjectRegister;
