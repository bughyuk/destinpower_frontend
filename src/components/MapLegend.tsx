import React, { ReactNode, useEffect, useState } from 'react';
import { useControlMode, useSetupLayer } from '@/modules/setup/hook';
import { CONTROL_MODE_TENSE_HISTORY } from '@/modules/setup/types';
import { LAYER_HEAT_MAP } from '@/utils/constants/common';
import { useTranslation } from 'react-i18next';
import { Alert } from 'react-bootstrap';

function MapLegend() {
  const { tense } = useControlMode();
  const { visibleLayer } = useSetupLayer();
  const [legend, setLegend] = useState<ReactNode>(<></>);

  useEffect(() => {
    if (
      tense === CONTROL_MODE_TENSE_HISTORY &&
      visibleLayer.includes(LAYER_HEAT_MAP)
    ) {
      setLegend(<HistoryHeatMapLegend />);
    } else {
      setLegend(<></>);
    }
  }, [tense, visibleLayer]);

  return <div className="map-alert-container">{legend}</div>;
}

function HistoryHeatMapLegend() {
  const { t } = useTranslation();

  return (
    <Alert className="p-4 mb-0 alert-white" show={true}>
      <div className="d-flex align-items-center mb-3">
        <h6 className="m-0">{t('text_legend')}</h6>
        <small className="text-50 pl-2">{t('msg_heat_map_legend')}</small>
      </div>
      <div className="d-flex flex-wrap align-items-start">
        <div className="level-list">
          <ul className="step4">
            <li className="level-item01">
              {t('text_smoothly')}
              <span className="item-bar"></span>
            </li>
            <li className="level-item02">
              {t('text_usually')}
              <span className="item-bar"></span>
            </li>
            <li className="level-item03">
              {t('text_congestion')}
              <span className="item-bar"></span>
            </li>
            <li className="level-item04">
              {t('text_confusion')}
              <span className="item-bar"></span>
            </li>
          </ul>
        </div>
      </div>
    </Alert>
  );
}

export default MapLegend;
