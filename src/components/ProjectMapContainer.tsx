import React, { useEffect, useRef } from 'react';
import ReactDOM from 'react-dom';
import { Image, Tile, Vector as VectorLayer } from 'ol/layer';
import { ImageWMS, TileJSON, Vector as VectorSource } from 'ol/source';
import {
  COOKIE_KEY_VIEWER_COUNTRY,
  MAP_TILE_KEY,
  OPEN_LAYERS_RESOLUTIONS,
  PANE_STATUS_LIST,
} from '@/utils/constants/common';
import { Feature, Map, Overlay, View } from 'ol';
import { defaults, Draw } from 'ol/interaction';
import { fetchProject, fetchProjectsAllSpace } from '@/api/project';
import ProjectSpaceLocationOverlay from '@/components/ProjectSpaceLocationOverlay';
import { boundingExtent, getWidth } from 'ol/extent';
import { useOpenLayers, useProjectPane } from '@/modules/project/hook';
import { Floors, Space } from '@/modules/map/types';
import { useControlProject } from '@/modules/map/hook';
import { useHistory } from 'react-router-dom';
import { Config } from '@/config';
import { Circle, Fill, Stroke, Style } from 'ol/style';
import { createBox } from 'ol/interaction/Draw';
import GeometryType from 'ol/geom/GeometryType';
import {
  calcGeoImage,
  getGeoImageLayer,
  getGeoImageSource,
  getInteractionTransform,
  transformFromPolygon,
} from '@/utils/ol-ext-bridge';
import { DrawGeoImage } from '@/modules/space/types';
import { useCookies } from 'react-cookie';

let map: Map;
function ProjectMapContainer() {
  const history = useHistory();
  const container = useRef<HTMLDivElement>(null);
  const { locationOverlayReload, handleSetOpenLayers } = useOpenLayers();
  const { handleSetProject } = useControlProject();
  const { paneStatus } = useProjectPane();
  const [cookies, setCookie, removeCookie] = useCookies();

  useEffect(() => {
    const brightMap = new Tile({
      source: new TileJSON({
        url: `https://api.maptiler.com/maps/streets/tiles.json?key=${MAP_TILE_KEY}`,
        tileSize: 512,
      }),
    });

    const drawSource = new VectorSource({ wrapX: false });
    const drawLayer = new VectorLayer({
      source: drawSource,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#ff0000',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ff0000',
          }),
        }),
      }),
    });

    const geofencingSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        VERSION: '1.1.1',
        FORMAT: 'image/png',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const geofencingLayer = new Image({
      visible: false,
      source: geofencingSource,
    });

    map = new Map({
      layers: [brightMap, drawLayer, geofencingLayer],
      target: container.current!,
      interactions: defaults({
        doubleClickZoom: false,
      }),
      view: new View({
        resolutions: OPEN_LAYERS_RESOLUTIONS,
        maxResolution: OPEN_LAYERS_RESOLUTIONS[0],
        center: [15558454.49842193, 4257326.124240982],
        zoom: 5,
        projection: 'EPSG:3857',
        zoomFactor: 1,
      }),
    });

    const drawSquare = new Draw({
      source: drawSource,
      geometryFunction: createBox(),
      type: GeometryType.CIRCLE,
    });

    const geoImageLayer = getGeoImageLayer();
    const [transformLayer, transformInteraction] = getInteractionTransform();
    map.addLayer(transformLayer);

    transformInteraction.on('rotating', function (e: any) {
      transformFromPolygon(geoImageLayer, e.feature);
    });
    transformInteraction.on('translating', (e: any) => {
      transformFromPolygon(geoImageLayer, e.feature);
    });
    transformInteraction.on('scaling', function (e: any) {
      transformFromPolygon(geoImageLayer, e.feature);
    });
    transformInteraction.on(
      ['rotateend', 'translateend', 'scaleend'],
      (e: any) => {
        transformFromPolygon(geoImageLayer, e.feature);
      }
    );

    const drawGeoImageFromSquare = (
      options: DrawGeoImage,
      squareFeature: Feature
    ) => {
      map.removeLayer(geoImageLayer);
      map.addLayer(geoImageLayer);
      map.removeInteraction(transformInteraction);
      map.addInteraction(transformInteraction);
      const cx = options.imageCenter[0];
      if (cx === 0) {
        const center = map.getView().getCenter();
        if (center) {
          options.imageCenter = [center[0], center[1]];
          options.imageScale = [1, 1];
          options.imageRotate = 0;
        }
      }
      geoImageLayer.setSource(getGeoImageSource(options));
      setTimeout(() => {
        transformFromPolygon(geoImageLayer, squareFeature);
        const calcResult = calcGeoImage(geoImageLayer);
        const transformFeature = new Feature(calcResult);
        if (transformLayer.getSource().getFeatures().length === 0) {
          transformLayer.getSource().addFeature(transformFeature);
        } else {
          transformLayer.getSource().clear();
          transformLayer.getSource().addFeature(transformFeature);
        }
      }, 100);
    };

    const drawGeoImage = (options: DrawGeoImage, editing = true) => {
      map.removeLayer(geoImageLayer);
      map.addLayer(geoImageLayer);
      map.removeInteraction(transformInteraction);
      map.addInteraction(transformInteraction);
      const cx = options.imageCenter[0];
      if (cx === 0) {
        const center = map.getView().getCenter();
        if (center) {
          options.imageCenter = [center[0], center[1]];
          options.imageScale = [1, 1];
          options.imageRotate = 0;
        }
      }

      geoImageLayer.setSource(getGeoImageSource(options));

      if (editing) {
        setTimeout(() => {
          const calcResult = calcGeoImage(geoImageLayer);
          const transformFeature = new Feature(calcResult);
          if (transformLayer.getSource().getFeatures().length === 0) {
            transformLayer.getSource().addFeature(transformFeature);
          } else {
            transformLayer.getSource().clear();
            transformLayer.getSource().addFeature(transformFeature);
          }

          const geometry = transformFeature.getGeometry();
          if (geometry) {
            const extent = geometry.getExtent();
            map.getView().fit(extent, {
              size: map.getSize(),
            });
            let zoom = map.getView().getZoom();
            if (typeof zoom === 'number') {
              zoom -= 1;
              zoom > 19 && (zoom = 19), map.getView().setZoom(zoom);
            }
          }
        }, 100);
      }
    };

    const removeGeoImage = () => {
      map.removeInteraction(transformInteraction);
      transformLayer.getSource().clear();
      map.removeLayer(geoImageLayer);
    };

    handleSetOpenLayers({
      map,
      drawSource,
      geofencingLayer,
      draw: {
        square: drawSquare,
      },
      geoImage: {
        layer: geoImageLayer,
        draw: drawGeoImage,
        drawFromSquare: drawGeoImageFromSquare,
        remove: removeGeoImage,
      },
    });
  }, []);

  useEffect(() => {
    handleFetchProjectsAllSpace();
  }, [locationOverlayReload]);

  useEffect(() => {
    if (paneStatus === PANE_STATUS_LIST) {
      map.getOverlays().forEach((overlay) => {
        const element = overlay.getElement();
        if (element) {
          element.style.cssText = '';
        }
      });
    } else {
      map.getOverlays().forEach((overlay) => {
        const element = overlay.getElement();
        if (element) {
          element.style.cssText = 'display:none';
        }
      });
    }
  }, [paneStatus]);

  const handleFetchProjectsAllSpace = async () => {
    map.getOverlays().clear();
    const coordinates: number[][] = [];
    const projectSpaceLocationList = await fetchProjectsAllSpace();
    const view = map.getView();
    projectSpaceLocationList.forEach((projectSpaceLocation) => {
      const container = document.createElement('div');
      container.classList.add(
        'mouse-point',
        'space-marker',
        'location-correction'
      );
      if (paneStatus !== PANE_STATUS_LIST) {
        container.style.cssText = 'display:none';
      }
      ReactDOM.render(
        <ProjectSpaceLocationOverlay
          projectName={projectSpaceLocation.projectName}
        />,
        container
      );
      const overlay = new Overlay({
        element: container,
      });

      const worldWidth = getWidth(view.getProjection().getExtent());
      const world = Math.floor(
        (projectSpaceLocation.lng + worldWidth / 2) / worldWidth
      );

      overlay.setPosition([
        projectSpaceLocation.lng + world * worldWidth,
        projectSpaceLocation.lat,
      ]);

      map.addOverlay(overlay);

      coordinates.push([projectSpaceLocation.lng, projectSpaceLocation.lat]);

      container.addEventListener('click', (e) => {
        e.stopPropagation();
        handleFetchProject(projectSpaceLocation.projectId);
      });
    });

    if (coordinates.length) {
      const extent = boundingExtent(coordinates);
      let size = map.getSize();
      if (size) {
        size = [size[0] - 220, size[1] - 220];
        view.fit(extent, {
          size,
          maxZoom: 10,
        });
      }
    } else {
      const viewerCountry = cookies[COOKIE_KEY_VIEWER_COUNTRY];
      if (viewerCountry) {
        let center: number[] = [];
        switch (viewerCountry) {
          case 'JP':
            center = [15558454.49842193, 4257326.124240982];
            break;
          case 'KR':
            center = [14134326.456645714, 4516772.805272909];
            break;
        }

        if (center.length === 2) {
          view.setCenter(center);
          view.setZoom(7);
        }
      }
    }
  };

  const handleFetchProject = async (projectId: string) => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((projectSpace) => {
        const projectFloors: Floors[] = [];
        projectSpace.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: projectSpace.mappingId,
          id: projectSpace.metaId,
          name: projectSpace.metaName,
          longitude: projectSpace.lng,
          latitude: projectSpace.lat,
          floorsList: projectFloors,
          registDate: projectSpace.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.replace('/control');
    }
  };

  return (
    <>
      <div
        ref={container}
        className="map-container"
        style={{
          position: 'absolute',
        }}
        onContextMenu={(e) => {
          e.preventDefault();
        }}
      />
    </>
  );
}

export default ProjectMapContainer;
