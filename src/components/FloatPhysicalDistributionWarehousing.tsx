import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import {
  WarehousingProduct,
  WarehousingProductBox,
  WarehousingQRCodePrintingData,
} from '@/modules/physical_distribution/types';
import {
  usePhysicalDistributionProduct,
  usePhysicalDistributionStatus,
  usePhysicalDistributionWarehouse,
} from '@/modules/physical_distribution/hook';
import InputNumber from '@/components/InputNumber';
import { fetchQRCode, postWarehousing } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import moment from 'moment';
import { printQRCode } from '@/utils/bixolon-bridge';
import { CommonUtils } from '@/utils';
import AlertModal from '@/components/AlertModal';
import {
  BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER,
  BIXOLON_ERROR_CODE_ETC_ERROR,
  ResponsePrint,
} from '@/utils/constants/common';
import NewlineText from '@/components/NewlineText';
import ConfirmModal from '@/components/ConfirmModal';
import QRCodePrintingModal from '@/components/QRCodePrintingModal';
import PhysicalDistributionFlatpickr from '@/components/PhysicalDistributionFlatpickr';
import classNames from 'classnames';

let printingFlag = false;
const today = moment().format('YYYY-MM-DD');
function FloatPhysicalDistributionWarehousing() {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const {
    load: loadWarehouseList,
    warehouseList,
  } = usePhysicalDistributionWarehouse();
  const {
    selectedProduct,
    handleSetProduct,
  } = usePhysicalDistributionProduct();
  const { handleChangeReloadFlag } = usePhysicalDistributionStatus();
  const [inputs, setInputs] = useState<{
    printingQRCode: boolean;
    date: string;
    warehouseId: string;
  }>({
    printingQRCode: true,
    date: today,
    warehouseId: '',
  });
  const [productList, setProductList] = useState<WarehousingProduct[]>([]);
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [showAlertModal, setShowAlertModal] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');
  const [showQRPrintingModal, setShowQRPrintingModal] = useState(false);
  const [
    qrCodePrintingData,
    setQRCodePrintingData,
  ] = useState<WarehousingQRCodePrintingData>({
    image: '',
    totalLogisticsQuantity: 0,
    warehousingDate: '',
    productList: [],
  });
  const [qrCodePrintingCompleteFlag, setQRCodePrintingCompleteFlag] = useState(
    false
  );

  useEffect(() => {
    if (selectedProduct) {
      handleSetProduct(null);
      let message = '';
      if (
        productList.find(
          (product) => product.productId === selectedProduct.productId
        )
      ) {
        message = t('msg_already_been_added_product');
      }

      if (
        productList.filter(
          (product) => product.productId !== selectedProduct.productId
        ).length
      ) {
        message = t('msg_can_not_warehousing_multiple_kinds_of_products');
      }

      if (message) {
        handleShowAlertModal(message);
        return;
      }

      setProductList([
        ...productList,
        {
          ...selectedProduct,
          logisticsQuantity: 0,
        },
      ]);
    }
  }, [selectedProduct]);

  useEffect(() => {
    if (warehouseList.length) {
      const firstWarehouse = warehouseList[0];
      setInputs({
        ...inputs,
        warehouseId: firstWarehouse.warehouseId,
      });
    }
  }, [warehouseList]);

  const handleShowAlertModal = (message: string) => {
    setAlertMessage(message);
    setShowAlertModal(true);
  };

  const handleShowQRCodePrintingModal = (
    qrCodePrintingData: WarehousingQRCodePrintingData
  ) => {
    setQRCodePrintingData(qrCodePrintingData);
    setShowQRPrintingModal(true);
  };

  const handleChangeValue = (
    changeIndex: number,
    field: 'logisticsQuantity',
    value: string | number
  ) => {
    setProductList(
      productList.map((product, index) => {
        if (changeIndex === index) {
          switch (field) {
            case 'logisticsQuantity':
              product[field] = Number(value);
              break;
          }
        }

        return product;
      })
    );
  };

  const handleClickRemove = (clickedIndex: number) => {
    setProductList(
      productList.filter((product, index) => index !== clickedIndex)
    );
  };

  const handleClickInstruction = () => {
    if (productList.length) {
      let totalLogisticsQuantity = 0;
      productList.forEach(({ productId, logisticsQuantity }) => {
        totalLogisticsQuantity += logisticsQuantity;
      });
      if (!totalLogisticsQuantity) {
        handleShowAlertModal(
          t('msg_warehousing_instruction_with_more_than_one_box')
        );
      } else {
        setShowConfirmModal(true);
      }
    } else {
      handleShowAlertModal(t('msg_add_product_before_proceeding'));
    }
  };

  const handleSubmit = async () => {
    let totalLogisticsQuantity = 0;
    const boxList: WarehousingProductBox[] = [];
    let printingProductName = '';
    let printingProductStandard = '';
    productList.forEach(
      ({
        productId,
        productName,
        productSizeName,
        boxQuantity,
        logisticsQuantity,
      }) => {
        totalLogisticsQuantity += logisticsQuantity;
        printingProductName += productName + ',';
        printingProductStandard = `${productSizeName} / ${t(
          'text_standard_each_number_of_product',
          {
            number: boxQuantity,
          }
        )}`;
        boxList.push({
          productId,
        });
      }
    );

    if (printingProductName) {
      printingProductName = printingProductName.substring(
        0,
        printingProductName.length - 1
      );
    }

    if (printingProductStandard) {
      printingProductStandard = printingProductStandard.substring(
        0,
        printingProductStandard.length - 1
      );
    }

    if (!totalLogisticsQuantity) {
      handleShowAlertModal(
        t('msg_warehousing_instruction_with_more_than_one_box')
      );
      return;
    }

    const result = await postWarehousing({
      projectId: project.id,
      logisticsQuantity: totalLogisticsQuantity,
      warehouseId: inputs.warehouseId,
      boxList,
      dueDate: `${moment(inputs.date).format('YYYY-MM-DD')}T00:00:00`,
    });

    if (result) {
      handleChangeReloadFlag();
      handleInitialInputs();
      if (inputs.printingQRCode) {
        const base64QRCodeImage = await fetchQRCode(result);
        if (base64QRCodeImage) {
          printQRCode(
            {
              image: base64QRCodeImage,
              date: `${t('text_warehousing_date')} : ${inputs.date}`,
              quantity: `${t(
                'text_box_quantity'
              )} :  ${totalLogisticsQuantity}`,
              productName: `${t(
                'text_product_title'
              )} : ${printingProductName}`,
              standard: `${t('text_standard')} : ${printingProductStandard}`,
            },
            totalLogisticsQuantity,
            (responsePrint: ResponsePrint) => {
              let resultMessage = '';
              switch (responsePrint.errorCode) {
                case BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER:
                  resultMessage = t(
                    'msg_printer_server_can_not_connect_to_server'
                  );
                  break;
                case BIXOLON_ERROR_CODE_ETC_ERROR:
                  resultMessage = t('msg_printer_impossible_due_to_error');
                  break;
              }

              if (resultMessage) {
                handleShowAlertModal(resultMessage);
              } else {
                printingFlag = !printingFlag;
                setQRCodePrintingCompleteFlag(!printingFlag);
                if (!showQRPrintingModal) {
                  handleShowQRCodePrintingModal({
                    image: base64QRCodeImage,
                    totalLogisticsQuantity,
                    warehousingDate: inputs.date,
                    productList,
                  });
                }
              }
            }
          );
        }
      }
    }
  };

  const handleInitialInputs = () => {
    let warehouseId = '';
    if (warehouseList.length) {
      const firstWarehouse = warehouseList[0];
      warehouseId = firstWarehouse.warehouseId;
    }

    setInputs({
      ...inputs,
      date: today,
      warehouseId,
    });

    setProductList([]);
  };

  if (!loadWarehouseList) {
    return <></>;
  }

  if (loadWarehouseList && warehouseList.length === 0) {
    return (
      <>
        <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
          <div className="flex title-row">
            <h3 className="d-flex align-items-center mb-0">
              {t('text_warehousing_instruction')}
            </h3>
          </div>
        </div>
        <div className="contents-view">
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>
            <em className="empty-txt m-0">
              {t('msg_add_warehouse_before_proceeding')}
            </em>
          </div>
        </div>
      </>
    );
  }

  return (
    <>
      <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
        <div className="flex title-row">
          <h3 className="d-flex align-items-center mb-0">
            {t('text_warehousing_instruction')}
          </h3>
          <span className="text-muted text-headings text-uppercase">
            {t('msg_warehousing_description')}
          </span>
        </div>
        <div className="d-flex align-items-center options">
          <div className="d-flex align-items-center mr-2">
            <div className="custom-control custom-checkbox checkbox-rounded">
              <input
                id="printingQRCode"
                type="checkbox"
                className="custom-control-input"
                checked={inputs.printingQRCode}
                onChange={(e) => {
                  setInputs({
                    ...inputs,
                    [e.target.id]: e.target.checked,
                  });
                }}
              />
              <label htmlFor="printingQRCode" className="custom-control-label">
                {t('text_printing_qr_code')}
              </label>
            </div>
          </div>
        </div>
        <a
          className="btn btn-rounded btn-outline-dark ml-2"
          onClick={handleClickInstruction}
        >
          <MaterialIcon name={'arrow_downward'} align={'left'} />
          {t('text_warehousing_instruction')}
        </a>
      </div>
      <div className="contents-view">
        <div className="page-separator mt-5">
          <div className="page-separator__text">
            {t('text_warehousing_information')}
          </div>
        </div>
        <table className="table mb-4 thead-bg-light">
          <thead>
            <tr>
              <th>{t('text_warehousing_date')}</th>
              <th>{t('text_warehouse')}</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <PhysicalDistributionFlatpickr
                  value={inputs.date}
                  onChange={(value) =>
                    setInputs({
                      ...inputs,
                      date: value,
                    })
                  }
                />
              </td>
              <td>
                <select
                  className="form-control custom-select font-weight-bolder w-auto"
                  onChange={(e) =>
                    setInputs({
                      ...inputs,
                      warehouseId: e.target.value,
                    })
                  }
                  value={inputs.warehouseId}
                >
                  {warehouseList.map(({ warehouseId, warehouseName }) => (
                    <option key={warehouseId} value={warehouseId}>
                      {warehouseName}
                    </option>
                  ))}
                </select>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="page-separator mt-5">
          <div className="page-separator__text">
            {t('text_product_information')}
          </div>
        </div>
        {productList.length === 0 && (
          <div className="empty-state border-top-0">
            <p className="empty-icon m-0">
              <span className="material-icons-outlined">error_outline</span>
            </p>

            <em className="empty-txt m-0">
              {t('msg_selection_product_from_the_list_on_the_left')}
            </em>
          </div>
        )}
        {productList.length > 0 && (
          <table className="table mb-4 thead-bg-light">
            <colgroup>
              <col width="*" />
              <col width="15%" />
              <col width="5%" />
            </colgroup>
            <thead>
              <tr>
                <th>{t('text_product_information')}</th>
                <th>{t('text_box_quantity')}</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {productList.map((product, i) => (
                <WarehousingProductItem
                  key={i}
                  {...product}
                  onChangeValue={(field, value) => {
                    handleChangeValue(i, field, value);
                  }}
                  onClickRemove={() => handleClickRemove(i)}
                />
              ))}
            </tbody>
          </table>
        )}
      </div>
      <ConfirmModal
        show={showConfirmModal}
        onHide={() => setShowConfirmModal(false)}
        onClickConfirm={handleSubmit}
      >
        <div className="py-4">
          <div className="text-center">
            <h3>
              {t('msg_sure_want_to_warehousing_instruction_in_quantity_boxes', {
                quantity: productList.reduce(
                  (a, b) => +a + b.logisticsQuantity,
                  0
                ),
              })}
            </h3>
          </div>
          <div className="d-flex justify-content-center">
            {productList.map(
              ({ productName, imgUrl, productWeight, boxQuantity }, i) => (
                <div
                  key={i}
                  className="media flex-nowrap align-items-center border-1 p-3"
                >
                  <span className="avatar avatar-sm mr-2 blank-img">
                    <img
                      src={imgUrl}
                      className={classNames('avatar-img', {
                        'rounded-circle': imgUrl,
                      })}
                    />
                  </span>
                  <div className="media-body">
                    <strong className="text-dark">{productName}</strong>
                    <div className="text-muted small">
                      {CommonUtils.getProductStandard(
                        productWeight,
                        boxQuantity
                      )}
                    </div>
                  </div>
                </div>
              )
            )}
          </div>
        </div>
      </ConfirmModal>
      <AlertModal show={showAlertModal} onHide={() => setShowAlertModal(false)}>
        <NewlineText text={alertMessage} />
      </AlertModal>
      <QRCodePrintingModal
        show={showQRPrintingModal}
        onHide={() => setShowQRPrintingModal(false)}
        qrCodePrintingData={qrCodePrintingData}
        qrCodePrintingCompleteFlag={qrCodePrintingCompleteFlag}
      />
    </>
  );
}

type WarehousingProductItemProps = WarehousingProduct & {
  onChangeValue: (field: 'logisticsQuantity', value: string | number) => void;
  onClickRemove: () => void;
};

function WarehousingProductItem({
  productName,
  imgUrl,
  productSizeName,
  productWeight,
  boxQuantity,
  logisticsQuantity,
  onChangeValue,
  onClickRemove,
}: WarehousingProductItemProps) {
  const { t } = useTranslation();

  return (
    <tr>
      <td>
        <div className="flex d-flex align-items-center">
          <a className="avatar mr-12pt blank-img">
            <img src={imgUrl} className="avatar-img rounded" />
          </a>
          <div className="flex list-els">
            <h6 className="m-0">{productName}</h6>
            <div className="card-subtitle text-50">
              <small className="mr-2">{productSizeName}</small>
              <small className="mr-2">
                {CommonUtils.getProductStandard(productWeight, boxQuantity)}
              </small>
            </div>
          </div>
        </div>
      </td>
      <td>
        <div className="d-inline-flex align-items-center">
          <InputNumber
            className="form-control font-weight-bolder"
            style={{
              width: '6rem',
            }}
            value={logisticsQuantity}
            onChange={(value) => onChangeValue('logisticsQuantity', value)}
          />
          <span className="ml-2 text-50">{t('text_box_en')}</span>
        </div>
      </td>
      <td>
        <a className="circle-pin-sm p-2" onClick={onClickRemove}>
          <span className="material-icons font-size-16pt">close</span>
        </a>
      </td>
    </tr>
  );
}

export default FloatPhysicalDistributionWarehousing;
