import React, { useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import { ProductLine } from '@/modules/physical_distribution/types';
// import { useControlProject } from '@/modules/map/hook';
import classNames from 'classnames';
// import InputNumber from '@/components/InputNumber';
import { dp_postProductLine } from '@/api/physical_distribution';

type PhysicalDistributionWarehouseRegisterProps = PaneProps & {
  productLine: ProductLine | undefined;
};

function ProcessProductLineRegister({
  productLine,
  onChangeStatus,
}: PhysicalDistributionWarehouseRegisterProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [inputs, setInputs] = useState<{
    name: string;
    memo: string;
  }>({
    name: productLine !== undefined ? productLine.name : '',
    memo: productLine !== undefined ? productLine.memo : '',
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid = true;

    if (isValid) {
      setLoading(true);
      const result = await dp_postProductLine({
        id: productLine !== undefined ? productLine.id : undefined, //id 값존재하면 수정처리
        name: inputs.name,
        memo: inputs.memo,
      });

      if (result) {
        onChangeStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'제품군명'} className={'mb-0'} essential={true} />
        <input
          type="text"
          className="form-line"
          id="name"
          value={inputs.name}
          placeholder={t('제품군명을 입력해 주세요.')}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_memo'} />
        <input
          type="text"
          className="form-line"
          id="memo"
          value={inputs.memo}
          placeholder={t('place_holder_memo')}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => onChangeStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t(productLine === undefined ? 'text_do_add' : 'text_do_edit')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default ProcessProductLineRegister;
