import {
  useControlProject,
  useControlSpace,
  useHistoryUser,
  useOpenLayers,
} from '@/modules/map/hook';
import React, { useEffect, useState } from 'react';
import { ControlUserTab } from '@/modules/setup/types';
import { useTranslation } from 'react-i18next';
import EmptyPane from '@/components/EmptyPane';
import {
  Collapse,
  Nav,
  TabContainer,
  TabContent,
  TabPane,
} from 'react-bootstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import classNames from 'classnames';
import MaterialIcon from '@/components/MaterialIcon';
import { HistoryTrace, HistoryUser } from '@/modules/map/types';
import { fetchMessages, postMessages } from '@/api/message';
import { htmlToText } from 'html-to-text';
import { fetchHistoryUserTrace } from '@/api/map';
import { useControlMode, useSetupData } from '@/modules/setup/hook';
import { MessageHistory } from '@/modules/accident/types';
import moment from 'moment';
import FormLabel from '@/components/FormLabel';
import HistoryUserPeriod from '@/components/HistoryUserPeriod';
import { Icon, Style } from 'ol/style';
import { Feature } from 'ol';
import { Point } from 'ol/geom';
import { ImageWMS } from 'ol/source';

type UserContentProps = {
  accessKey: string;
};

function HistoryUserContent() {
  const { users, handleSetHistoryUser } = useHistoryUser();
  const { historyStandard } = useControlMode();
  const { historyUserFilter } = useSetupData();
  const [accessKey, setAccessKey] = useState('');
  const [userId, setUserId] = useState('');
  const [activeKey, setActiveKey] = useState<ControlUserTab>('info');
  const { historySimulationLayer } = useOpenLayers();

  useEffect(() => {
    if (historyStandard === 'user') {
      handleSetHistoryUser(historyUserFilter.users);
    }

    return () => {
      historySimulationLayer?.getSource().clear();
    };
  }, []);

  useEffect(() => {
    if (users.length) {
      let exist = false;
      if (accessKey) {
        exist = users.some((user) => user.access_key === accessKey);
      }

      if (!exist) {
        const user = users[0];
        setAccessKey(user.access_key);
        setUserId(user.user_id);
      }

      if (historyStandard === 'user') {
        setActiveKey(historyUserFilter.tab);
      }
    } else {
      setAccessKey('');
      setActiveKey('info');
      setUserId('');
    }
  }, [users]);

  return (
    <>
      <Header />
      <Body
        activeKey={activeKey}
        accessKey={accessKey}
        userId={userId}
        onChangeActiveKey={setActiveKey}
        onChangeAccessKey={setAccessKey}
        onChangeUserId={setUserId}
      />
    </>
  );
}

function Header() {
  const { t } = useTranslation();
  const { users, handleCancelHistoryUser } = useHistoryUser();

  const handleCancelInquiry = () => {
    users.forEach((user) => {
      handleCancelHistoryUser(user.access_key);
    });
  };

  return (
    <>
      <div className="container-fluid d-flex align-items-center py-4">
        <div className="flex d-flex">
          <div className="mr-24pt">
            <h3 className="mb-0">{t('text_inquiry_user')}</h3>
          </div>
        </div>
      </div>
      {users.length > 0 && (
        <div className="container-fluid d-flex align-items-center py-2 justify-content-end">
          <div className="d-flex">
            <a className="btn-link text-accent" onClick={handleCancelInquiry}>
              {t('text_cancel_inquiry')}
            </a>
          </div>
        </div>
      )}
    </>
  );
}

type BodyProps = UserContentProps & {
  activeKey: ControlUserTab;
  userId: string;
  onChangeActiveKey: (activeKey: ControlUserTab) => void;
  onChangeAccessKey: (accessKey: string) => void;
  onChangeUserId: (userId: string) => void;
};

export type PeriodIndicator = '1D' | '1W' | '1M' | '3M' | '6M';

function Body({
  activeKey,
  accessKey,
  userId,
  onChangeActiveKey,
  onChangeAccessKey,
  onChangeUserId,
}: BodyProps) {
  const { t } = useTranslation();
  const { users } = useHistoryUser();
  const [periodIndicator, setPeriodIndicator] = useState<PeriodIndicator>('1D');

  if (users.length === 0) {
    return (
      <>
        <EmptyPane
          textKey={'msg_no_inquiry_user'}
          descriptionKey={'msg_user_suggest_to_inquiry_condition'}
        />
      </>
    );
  }

  return (
    <div className="container-fluid">
      <TabContainer
        defaultActiveKey={activeKey}
        activeKey={activeKey}
        onSelect={(eventKey) => {
          if (eventKey) {
            onChangeActiveKey(eventKey as ControlUserTab);
          }
        }}
        transition={false}
      >
        <Nav className="contents-tab the-num03 mb-4">
          <Nav.Link eventKey={'info'} bsPrefix={''}>
            <span className="menu-cover">
              <span>{t('text_info')}</span>
            </span>
          </Nav.Link>
          <Nav.Link eventKey={'message'} bsPrefix={''}>
            <span className="menu-cover">
              <span>{t('text_send_message')}</span>
            </span>
          </Nav.Link>
          <Nav.Link eventKey={'trace'} bsPrefix={''}>
            <span className="menu-cover">
              <span>{t('text_traffic_tracking')}</span>
            </span>
          </Nav.Link>
        </Nav>
        {users.length > 1 && (
          <HistoryUserList
            accessKey={accessKey}
            onChangeAccessKey={onChangeAccessKey}
            onChangeUserId={onChangeUserId}
          />
        )}
        <TabContent>
          <TabPane eventKey={'info'}>
            <Info accessKey={accessKey} />
          </TabPane>
          <TabPane eventKey={'message'}>
            <Message
              accessKey={accessKey}
              periodIndicator={periodIndicator}
              onChangePeriodIndicator={setPeriodIndicator}
            />
          </TabPane>
          <TabPane eventKey={'trace'}>
            <Trace
              accessKey={accessKey}
              activeKey={activeKey}
              userId={userId}
              periodIndicator={periodIndicator}
              onChangePeriodIndicator={setPeriodIndicator}
            />
          </TabPane>
        </TabContent>
      </TabContainer>
    </div>
  );
}

type HistoryUserListProps = UserContentProps & {
  onChangeAccessKey: (accessKey: string) => void;
  onChangeUserId: (userId: string) => void;
};

function HistoryUserList({
  accessKey,
  onChangeAccessKey,
  onChangeUserId,
}: HistoryUserListProps) {
  const { t } = useTranslation();
  const { users, handleCancelHistoryUser } = useHistoryUser();

  return (
    <>
      <div className="d-flex mb-2">
        <span className="d-flex flex m-0">
          <strong className="text-iden">{users.length}</strong>
          {t('msg_number_of_user_have_been_inquiry')}
        </span>
      </div>
      <PerfectScrollbar className={'selected-user mt-2'}>
        <div className="card-inner">
          <ul>
            {users.map((user) => {
              let genderText = '';
              if (user.user_gender === 'M') {
                genderText = t('text_male');
              } else {
                genderText = t('text_female');
              }

              return (
                <li key={user.access_key}>
                  <a
                    className={classNames({
                      active: user.access_key === accessKey,
                    })}
                    onClick={(e) => {
                      e.stopPropagation();
                      onChangeAccessKey(user.access_key);
                      onChangeUserId(user.user_id);
                    }}
                  >{`${user.user_id} / ${genderText} / ${
                    user.user_age + t('text_years_old')
                  }`}</a>
                  <a
                    className="btn-del"
                    onClick={(e) => {
                      e.stopPropagation();
                      handleCancelHistoryUser(user.access_key);
                    }}
                  >
                    <MaterialIcon name={'close'} />
                  </a>
                </li>
              );
            })}
          </ul>
        </div>
      </PerfectScrollbar>
    </>
  );
}

function Info({ accessKey }: UserContentProps) {
  const { t } = useTranslation();
  const { users } = useHistoryUser();
  const [userInfo, setUserInfo] = useState<HistoryUser>({
    access_key: '',
    user_id: '',
    user_age: 0,
    user_gender: '',
    device_type: '',
  });

  useEffect(() => {
    if (accessKey) {
      const user = users.find((user) => user.access_key === accessKey);
      if (user) {
        setUserInfo(user);
      }
    }
  }, [accessKey]);

  let genderText = '';
  if (userInfo.user_gender === 'M') {
    genderText = t('text_male');
  } else {
    genderText = t('text_female');
  }

  return (
    <div className="user-info">
      <div className="p-16pt">
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_user')}</dt>
          <dd className="m-0">{userInfo.user_id}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_identification_id')}</dt>
          <dd className="m-0">{accessKey}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_gender')}</dt>
          <dd className="m-0">{genderText}</dd>
        </dl>
        <dl className="d-flex align-items-center">
          <dt className="flex mr-2">{t('text_age')}</dt>
          <dd className="m-0">{userInfo.user_gender}</dd>
        </dl>
      </div>
    </div>
  );
}

type MessageProps = UserContentProps & {
  periodIndicator: PeriodIndicator;
  onChangePeriodIndicator: (periodIndicator: PeriodIndicator) => void;
};

function Message({
  accessKey,
  periodIndicator,
  onChangePeriodIndicator,
}: MessageProps) {
  const { t } = useTranslation();
  const { users } = useHistoryUser();
  const { historyStandard } = useControlMode();
  const { historyFilter } = useSetupData();
  const [targetAccessKeyList, setTargetAccessKeyList] = useState<string[]>([]);
  const { project } = useControlProject();
  const [inputs, setInputs] = useState({
    title: '',
    content: '',
  });
  const [displayMessageId, setDisplayMessageId] = useState('');
  const [messageHistory, setMessageHistory] = useState<MessageHistory[]>([]);

  const handleFetchMessages = async () => {
    const period = historyFilter.period;
    const data = await fetchMessages({
      projectId: project.id,
      targetId: accessKey,
      searchStartDates: period[0],
      searchEndDates: period[1],
    });

    setMessageHistory(data);
  };

  useEffect(() => {
    if (accessKey) {
      setDisplayMessageId('');
      setMessageHistory([]);
      handleFetchMessages();
    }
  }, [accessKey, historyFilter.period]);

  useEffect(() => {
    if (users.length > 0) {
      setTargetAccessKeyList(users.map((user) => user.access_key));
    }
  }, [users]);

  const handleSubmit = async () => {
    if (inputs.title && inputs.content) {
      const result = await postMessages({
        projectId: project.id,
        parentId: '',
        parentType: 'message',
        messageTitle: inputs.title,
        messageContent: htmlToText(inputs.content, {
          wordwrap: 130,
        }),
        imgId: '',
        targetIds: targetAccessKeyList,
      });

      if (result) {
        setInputs({
          title: '',
          content: '',
        });
        handleFetchMessages();
      }
    }
  };

  return (
    <>
      <div className="p-16pt">
        <div className="form-group mb-4">
          <label className="form-label mb-0 text-50 essential">
            {t('text_title')}
          </label>
          <input
            type="text"
            className="form-line"
            placeholder={t('place_holder_title')}
            value={inputs.title}
            onChange={(e) => setInputs({ ...inputs, title: e.target.value })}
          />
        </div>
        <div className="form-group mb-4">
          <label className="form-label mb-0 text-50 essential mb-2">
            {t('text_content')}
          </label>
          <div className="msg-box">
            <textarea
              className="form-control"
              rows={3}
              placeholder={t('place_holder_content')}
              style={{
                marginTop: '0px',
                marginBottom: '0px',
                height: '148px',
                resize: 'none',
              }}
              value={inputs.content}
              maxLength={1000}
              onChange={(e) =>
                setInputs({ ...inputs, content: e.target.value })
              }
            ></textarea>
            <div className="d-flex p-2">
              <span className="">{inputs.content.length}/1000</span>
              <a
                className={classNames({
                  disabled: !inputs.title || !inputs.content,
                })}
                onClick={handleSubmit}
              >
                {t('text_send_message')}
              </a>
            </div>
          </div>
        </div>
      </div>
      {historyStandard === 'user' && (
        <HistoryUserPeriod
          period={periodIndicator}
          onChangePeriod={onChangePeriodIndicator}
        />
      )}
      {messageHistory.length > 0 && (
        <div className="sidebar">
          <ul className="sidebar-menu">
            {messageHistory.map((history) => (
              <MessageHistoryItem
                key={history.messageId}
                {...history}
                displayMessageId={displayMessageId}
                onChangeDisplayMessageId={setDisplayMessageId}
              />
            ))}
          </ul>
        </div>
      )}
    </>
  );
}

type MessageHistoryItemProps = MessageHistory & {
  displayMessageId: string;
  onChangeDisplayMessageId: (messageId: string) => void;
};

function MessageHistoryItem({
  displayMessageId,
  onChangeDisplayMessageId,
  messageId,
  registDate,
  messageTitle,
  messageContent,
}: MessageHistoryItemProps) {
  const handleDisplay = () => {
    let id = '';
    if (displayMessageId !== messageId) {
      id = messageId;
    }

    onChangeDisplayMessageId(id);
  };

  return (
    <li
      className={classNames('sidebar-menu-item', {
        open: displayMessageId === messageId,
      })}
    >
      <a className="sidebar-menu-button" onClick={handleDisplay}>
        <MaterialIcon name={'date_range'} />
        {moment(registDate).format('YYYY.MM.DD')}
        <span className="ml-auto sidebar-menu-toggle-icon"></span>
      </a>
      <Collapse in={displayMessageId === messageId}>
        <div className="msg-list">
          <div className="msg-history">
            <div className="form-group mb-4">
              <FormLabel textKey={'text_title'} className={'mb-0'} />
              <p className="py-2 ml-2">{messageTitle}</p>
            </div>
            <div className="form-group mb-4">
              <FormLabel textKey={'text_content'} className={'mb-0'} />
              <p className="py-2 ml-2">{messageContent}</p>
            </div>
          </div>
        </div>
      </Collapse>
    </li>
  );
}

type TraceProps = UserContentProps & {
  activeKey: ControlUserTab;
  userId: string;
  periodIndicator: PeriodIndicator;
  onChangePeriodIndicator: (periodIndicator: PeriodIndicator) => void;
};

function Trace({
  accessKey,
  activeKey,
  userId,
  periodIndicator,
  onChangePeriodIndicator,
}: TraceProps) {
  const { space } = useControlSpace();
  const { historyStandard } = useControlMode();
  const { historyFilter } = useSetupData();
  const [traceList, setTraceList] = useState<HistoryTrace[]>([]);
  const [displayTraceDate, setDisplayTraceDate] = useState('');
  const {
    historySimulationLayer,
    historyTraceDistributionLayer,
  } = useOpenLayers();

  useEffect(() => {
    return () => {
      historySimulationLayer?.getSource().clear();
      handleInitialTraceDistribution();
      historyTraceDistributionLayer?.setVisible(false);
    };
  }, []);

  useEffect(() => {
    if (activeKey !== 'trace') {
      setDisplayTraceDate('');
    }
  }, [activeKey]);

  useEffect(() => {
    if (!displayTraceDate) {
      handleInitialTraceDistribution();
      historySimulationLayer?.getSource().clear();
    }
  }, [displayTraceDate]);

  const handleInitialTraceDistribution = () => {
    const historyTraceDistributionSource = historyTraceDistributionLayer?.getSource() as ImageWMS;
    historyTraceDistributionSource.updateParams({});
    historyTraceDistributionSource.refresh();
    historyTraceDistributionLayer?.setVisible(false);
  };
  const handleFetchHistoryTrace = async () => {
    const data = await fetchHistoryUserTrace({
      mapid: space.floorsMapId,
      user_id: userId,
      startDate: historyFilter.period[0],
      endDate: historyFilter.period[1],
      stime: historyFilter.startTime,
      etime: historyFilter.endTime,
      device_type: historyFilter.cellphone,
      user_gender: historyFilter.gender,
      ageTo: historyFilter.age.to,
      ageFrom: historyFilter.age.from,
      visitTo: historyFilter.residenceTime.to,
      visitFrom: historyFilter.residenceTime.from,
    });

    setTraceList(data);
  };

  const handleDrawDateTrace = (date: string, dateLocationList: number[][]) => {
    if (!historyTraceDistributionLayer?.getVisible()) {
      historyTraceDistributionLayer?.setVisible(true);
    }
    const historyTraceDistributionSource = historyTraceDistributionLayer?.getSource() as ImageWMS;
    let params = `map_id:'${space.floorsMapId}';`;
    params += `starttime:'${date} 00:00:00';endtime:'${date} 23:59:59';`;
    const updateParams = {
      CQL_FILTER: `map_id = '${space.floorsMapId}' and user_id = '${userId}'`,
      viewparams: params,
    };
    historyTraceDistributionSource.updateParams(updateParams);
    historyTraceDistributionSource.refresh();

    const source = historySimulationLayer?.getSource();
    const iconFeature = new Feature(new Point([0, 0]));
    const iconStyle = new Style({
      image: new Icon({
        src: `/static/images/person_pin.svg`,
        scale: 2,
      }),
    });
    iconFeature.setStyle(iconStyle);
    if (source) {
      source.clear();
      source.addFeature(iconFeature);
      for (let i = 0; i < dateLocationList.length; i++) {
        setTimeout(() => {
          const coordinates = dateLocationList[i];
          iconFeature.setGeometry(new Point(coordinates));
        }, i * 150);
      }
    }
  };

  useEffect(() => {
    if (accessKey) {
      setDisplayTraceDate('');
      setTraceList([]);
      handleFetchHistoryTrace();
    }
  }, [accessKey, historyFilter.period]);

  return (
    <>
      {historyStandard === 'user' && (
        <HistoryUserPeriod
          period={periodIndicator}
          onChangePeriod={onChangePeriodIndicator}
        />
      )}
      <div className="sidebar">
        <ul className="sidebar-menu">
          {traceList.map((trace) => (
            <TraceItem
              key={trace.date}
              displayTraceDate={displayTraceDate}
              onChangeShowTraceDate={setDisplayTraceDate}
              drawDateTrace={handleDrawDateTrace}
              {...trace}
            />
          ))}
        </ul>
      </div>
    </>
  );
}

type TraceItemProps = HistoryTrace & {
  displayTraceDate: string;
  onChangeShowTraceDate: (traceDate: string) => void;
  drawDateTrace: (date: string, dateLocationList: number[][]) => void;
};

function TraceItem({
  displayTraceDate,
  onChangeShowTraceDate,
  drawDateTrace,
  date,
  trace,
  dateLocationList,
}: TraceItemProps) {
  const { historySimulationLayer } = useOpenLayers();
  const [sectionPathHour, setSectionPathHour] = useState('');

  const handleDeleteSectionPath = () => {
    historySimulationLayer?.getSource().clear();
  };

  const handleDrawSectionPath = (hour: string, locationList: number[][]) => {
    handleDeleteSectionPath();
    if (sectionPathHour !== hour) {
      const source = historySimulationLayer?.getSource();
      const iconFeature = new Feature(new Point([0, 0]));
      const iconStyle = new Style({
        image: new Icon({
          src: `/static/images/person_pin.svg`,
          scale: 2,
        }),
      });
      iconFeature.setStyle(iconStyle);
      if (source) {
        source.addFeature(iconFeature);
        for (let i = 0; i < locationList.length; i++) {
          const coordinates = locationList[i];
          setTimeout(() => {
            iconFeature.setGeometry(new Point(coordinates));
          }, i * 150);
        }
      }

      setSectionPathHour(hour);
    } else {
      setSectionPathHour('');
      drawDateTrace(date, dateLocationList);
    }
  };

  return (
    <li
      className={classNames('sidebar-menu-item', {
        open: displayTraceDate === date,
      })}
    >
      <a
        className="sidebar-menu-button"
        onClick={() => {
          let traceDate = date;
          if (displayTraceDate === date) {
            traceDate = '';
          }
          drawDateTrace(traceDate, dateLocationList);
          onChangeShowTraceDate(traceDate);
        }}
      >
        <MaterialIcon name={'date_range'} />
        {date}
        <span className="ml-auto sidebar-menu-toggle-icon"></span>
      </a>
      <Collapse in={displayTraceDate === date}>
        <div className="route-list collapse" id="user-location-list01">
          <ul className="route-user01">
            {trace.map((traceArea) => (
              <li key={traceArea.hour} className="line-blue">
                <div className="route-inner">
                  <div className="route-title">
                    <span className="time">{traceArea.hour}</span>
                    <h6 className="title">{traceArea.area}</h6>
                  </div>
                  <a
                    className="btn-location"
                    onClick={() =>
                      handleDrawSectionPath(
                        traceArea.hour,
                        traceArea.sectionLocationList
                      )
                    }
                  >
                    <MaterialIcon name={'place'} />
                  </a>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </Collapse>
    </li>
  );
}

export default HistoryUserContent;
