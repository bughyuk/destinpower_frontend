import React from 'react';
import ListHeader from '@/components/ListHeader';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_DETAIL, PaneStatus } from '@/utils/constants/common';

type EntryStatusProps = {
  onChangeStatus: (status: PaneStatus) => void;
};

function EntryStatus({ onChangeStatus }: EntryStatusProps) {
  const { t } = useTranslation();

  const handleRouteDetail = () => {
    onChangeStatus(PANE_STATUS_DETAIL);
  };

  return (
    <>
      <ListHeader
        titleKey={'text_entry_management'}
        descriptionKey={'msg_entry_management'}
      />
      <div className="container-fluid">
        <ul className="info-panel info-link">
          <li>
            <a onClick={handleRouteDetail}>
              <p>{t('text_authorization')}</p>
              <div className="cell">
                <span className="material-icons">people</span>
                <em>
                  <strong>0</strong>
                  {t('text_unit_of_person')}
                </em>
              </div>
            </a>
          </li>
          <li>
            <a onClick={handleRouteDetail}>
              <p>{t('text_go_to_the_office')}</p>
              <div className="cell">
                <span className="material-icons">how_to_reg</span>
                <em>
                  <strong>0</strong>
                  {t('text_unit_of_person')}
                </em>
              </div>
            </a>
          </li>
          <li>
            <a onClick={handleRouteDetail}>
              <p>{t('text_lateness')}</p>
              <div className="cell">
                <span className="material-icons">schedule</span>
                <em>
                  <strong>0</strong>
                  {t('text_unit_of_person')}
                </em>
              </div>
            </a>
          </li>
          <li>
            <a onClick={handleRouteDetail}>
              <p>{t('text_absence')}</p>
              <div className="cell">
                <span className="material-icons">person_off</span>
                <em>
                  <strong>0</strong>
                  {t('text_unit_of_person')}
                </em>
              </div>
            </a>
          </li>
        </ul>
        <ul className="info-panel none-link">
          <li>
            <p>{t('text_entry_status')}</p>
            <div className="cell">
              <span className="material-icons text-accent">expand_less</span>
              <em>
                <strong>0</strong>%
              </em>
            </div>
          </li>
          <li>
            <p>{t('text_average_work_time')}</p>
            <div className="cell">
              <span className="material-icons text-primary">expand_more</span>
              <em>
                <strong>0</strong>h <strong>0</strong>m
              </em>
            </div>
          </li>
        </ul>
      </div>
    </>
  );
}

export default EntryStatus;
