import React, { useEffect, useRef, useState } from 'react';
import {
  Dropdown,
  Nav,
  OverlayTrigger,
  Popover,
  PopoverContent,
  TabContainer,
  TabContent,
  TabPane,
} from 'react-bootstrap';
import Pagination from '@/components/Pagination';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Flatpickr from 'react-flatpickr';
import { useDropdown } from '@/modules/common';
import DropdownToggle from 'react-bootstrap/DropdownToggle';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import * as Moment from 'moment';
import { extendMoment } from 'moment-range';
import * as Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import HighchartsMore from 'highcharts/highcharts-more';
import HighchartsNetworkGraph from 'highcharts/modules/networkgraph';
import { Image, Tile, Vector as VectorLayer } from 'ol/layer';
import { ImageWMS, TileJSON } from 'ol/source';
import {
  MAP_TILE_KEY,
  OPEN_LAYERS_RESOLUTIONS,
} from '@/utils/constants/common';
import { Feature, Map as OpenLayersMap, View } from 'ol';
import { defaults } from 'ol/interaction';
import { useControlProject, useControlSpace } from '@/modules/map/hook';
import { fetchStatAccessInfo, fetchStatZone } from '@/api/statistics';
import { StatAccessInfo, StatZone } from '@/modules/statistics/types';
import { useTranslation } from 'react-i18next';
import VariationArrow from '@/components/VariationArrow';
import { Config } from '@/config';
import { fetchPoi } from '@/api/space';
import { Point } from 'ol/geom';
import { Fill, Icon, Stroke, Style, Text } from 'ol/style';
import VectorSource from 'ol/source/Vector';
import EmptyPane from '@/components/EmptyPane';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import MaterialIcon from '@/components/MaterialIcon';

const moment = extendMoment(Moment);
HighchartsMore(Highcharts);
HighchartsNetworkGraph(Highcharts);

type StatisticsProps = {
  mappingId: string;
  metaId: string;
  mapId: string;
  period: string[];
  selectedZone: StatZone;
};

type StatisticsTab =
  | 'ACCESS_INFO'
  | 'VISITOR_LIST'
  | 'WORKSPACE_INFO'
  | 'EMPLOYEE_INFO';

function FloatStatisticsSmartOffice() {
  const { t } = useTranslation();
  const { project: controlProject } = useControlProject();
  const { space: controlSpace } = useControlSpace();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);
  const [activeKey, setActiveKey] = useState<StatisticsTab>('ACCESS_INFO');
  const flatpickrRef = useRef<Flatpickr>(null);
  const [period, setPeriod] = useState<string[]>([
    moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD'),
    moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD'),
  ]);

  const [selectedZone, setSelectZone] = useState<StatZone>({
    zoneId: '',
    zoneName: t('text_the_whole_of_area'),
    geomStr: '',
  });
  const handleSelectFloors = (eventKey: string | null) => {
    const findZone = zoneList.find((zone) => zone.zoneId === eventKey);
    if (findZone) {
      setSelectZone(findZone);
    } else {
      setSelectZone({
        zoneId: '',
        zoneName: t('text_the_whole_of_area'),
        geomStr: '',
      });
    }
  };
  const [mapPosition, setMapPosition] = useState<number[]>([]);
  const [zoneList, setZoneList] = useState<StatZone[]>([]);
  const [showVisitorListTab, setShowVisitorListTab] = useState(false);

  useEffect(() => {
    if (!showVisitorListTab) {
      setActiveKey('ACCESS_INFO');
    }
  }, [showVisitorListTab]);

  useEffect(() => {
    const findSpace = controlProject.spaceList.find(
      (space) => space.id === controlSpace.spaceMetaId
    );

    if (findSpace) {
      setMapPosition([findSpace.longitude, findSpace.latitude]);
      const mappingId = findSpace.mappingId;

      if (findSpace.floorsList.length > 0) {
        const findFloors = findSpace.floorsList.find(
          (floors) => floors.id === controlSpace.floorsMapId
        );
        if (findFloors) {
          handleFetchZone(mappingId, findFloors.id);
        }
      }
    }
  }, [controlProject]);

  const handleFetchZone = async (mappingId: string, mapId: string) => {
    setZoneList(await fetchStatZone(mappingId, mapId));
  };

  if (!controlSpace.floorsMapId) {
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
  }

  return (
    <>
      <TabContainer>
        <div className="tab-cover mb-4">
          <Nav
            as={'ul'}
            onSelect={(eventKey) => {
              if (eventKey) {
                setActiveKey(eventKey as StatisticsTab);
              }
            }}
          >
            <Nav.Item as={'li'}>
              <Nav.Link
                active={activeKey === 'ACCESS_INFO'}
                eventKey={'ACCESS_INFO'}
              >
                {t('text_common_access_information')}
              </Nav.Link>
            </Nav.Item>
            {showVisitorListTab && (
              <Nav.Item as={'li'}>
                <Nav.Link
                  active={activeKey === 'VISITOR_LIST'}
                  eventKey={'VISITOR_LIST'}
                >
                  {t('text_detailed_visitor_list')}
                  <button
                    type="button"
                    className="tab-close"
                    onClick={() => setShowVisitorListTab(false)}
                  >
                    <span className="material-icons">close</span>
                  </button>
                </Nav.Link>
              </Nav.Item>
            )}
            <Nav.Item as={'li'}>
              <Nav.Link
                active={activeKey === 'WORKSPACE_INFO'}
                eventKey={'WORKSPACE_INFO'}
              >
                {t('text_workspace_information')}
              </Nav.Link>
            </Nav.Item>
            <Nav.Item as={'li'}>
              <Nav.Link
                active={activeKey === 'EMPLOYEE_INFO'}
                eventKey={'EMPLOYEE_INFO'}
              >
                {t('text_detailed_employee_information')}
              </Nav.Link>
            </Nav.Item>
          </Nav>
          <div className="sort-cover">
            <div className="set-date">
              <span className="material-icons-outlined font-size-16pt">
                date_range
              </span>
              <label className="">{t('text_period')}</label>
              <Flatpickr
                options={{
                  mode: 'range',
                  altInput: true,
                  altInputClass: 'btn-link statistics-flatpickr',
                  altFormat: 'Y-m-d',
                }}
                ref={flatpickrRef}
                value={period}
                onChange={(dates) => {
                  if (dates.length === 2) {
                    setPeriod(
                      dates.map((date) => moment(date).format('YYYY-MM-DD'))
                    );
                  }
                }}
                onClose={(dates) => {
                  if (dates.length < 2) {
                    flatpickrRef.current?.flatpickr.setDate(period);
                  }
                }}
              />
            </div>
            <div className="set-area">
              <span className="material-icons-outlined font-size-16pt">
                place
              </span>
              <label className="">{t('text_area')}</label>
              <TabContainer>
                <Dropdown onToggle={handleToggle} onSelect={handleSelectFloors}>
                  <DropdownToggle
                    as={'a'}
                    data-caret="false"
                    className={'btn-link'}
                  >
                    {selectedZone.zoneName}
                  </DropdownToggle>
                  <DropdownMenu align={'right'} ref={dropdown}>
                    <Dropdown.Item eventKey={''}>
                      {t('text_the_whole_of_area')}
                    </Dropdown.Item>
                    {zoneList.map((zone) => (
                      <Dropdown.Item key={zone.zoneId} eventKey={zone.zoneId}>
                        {zone.zoneName}
                      </Dropdown.Item>
                    ))}
                  </DropdownMenu>
                </Dropdown>
              </TabContainer>
            </div>
          </div>
        </div>
        <TabContent>
          <TabPane
            eventKey={'ACCESS_INFO'}
            active={activeKey === 'ACCESS_INFO'}
          >
            <AccessInfo
              mappingId={controlSpace.spaceMappingId}
              metaId={controlSpace.spaceMetaId}
              mapId={controlSpace.floorsMapId}
              period={period}
              mapPosition={mapPosition}
              selectedZone={selectedZone}
              onClickNumberOfVisitorsPerDay={() => {
                setShowVisitorListTab(true);
              }}
            />
          </TabPane>
          <TabPane
            eventKey={'VISITOR_LIST'}
            active={activeKey === 'VISITOR_LIST'}
          >
            <VisitorList />
          </TabPane>
          <TabPane
            eventKey={'WORKSPACE_INFO'}
            active={activeKey === 'WORKSPACE_INFO'}
          >
            <WorkspaceInfo activeKey={activeKey} mapPosition={mapPosition} />
          </TabPane>
          <TabPane
            eventKey={'EMPLOYEE_INFO'}
            active={activeKey === 'EMPLOYEE_INFO'}
          >
            <EmployeeInfo activeKey={activeKey} mapPosition={mapPosition} />
          </TabPane>
        </TabContent>
      </TabContainer>
    </>
  );
}

type AccessInfoProps = StatisticsProps & {
  mapPosition: number[];
  onClickNumberOfVisitorsPerDay: () => void;
};

function AccessInfo({
  mapPosition,
  mappingId,
  metaId,
  mapId,
  period,
  selectedZone,
  onClickNumberOfVisitorsPerDay,
}: AccessInfoProps) {
  const { t } = useTranslation();
  const mapContainerRef = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<OpenLayersMap | null>(null);
  const [heatMapImageWMS, setHeatMapImageWMS] = useState<ImageWMS | null>(null);
  const [zoneImage, setZoneImage] = useState<Image | null>(null);
  const [poiVectorSource, setPoiVectorSource] = useState<VectorSource | null>(
    null
  );
  const [statAccessInfo, setStatAccessInfo] = useState<StatAccessInfo>({
    averageForPeriod: 0,
    averageForPeriodComparison: 0,
    totalEnterCountForPeriod: 0,
    totalEnterCountForPeriodComparison: 0,
    frequentAreaForPeriod: null,
    rushHourForPeriod: null,
    rushHourForPeriodByDaily: [],
    enterCountForPeriodByDaily: {
      manager: [],
      user: [],
    },
  });
  const [visitorCountChartXAxis, setVisitorCountChartXAxis] = useState<
    string[]
  >([]);
  const [
    visitorManagerCountChartData,
    setVisitorManagerCountChartData,
  ] = useState<number[]>([]);
  const [visitorUserCountChartData, setVisitorUserCountChartData] = useState<
    number[]
  >([]);
  const [congestionTimeChartData, setCongestionTimeChartData] = useState<
    number[]
  >(Array.from({ length: 16 }, (_) => 0));

  useEffect(() => {
    if (period.length > 1) {
      const daysArray = calcPeriodDays();
      const initVisitorCountChartData = Array.from(
        { length: daysArray.length },
        () => 0
      );

      let prevMonth = '';
      const xAxis = daysArray.map((m) => {
        const month = m.format('MM');
        if (!prevMonth) {
          prevMonth = month;
          return m.format('MM-DD');
        } else {
          if (prevMonth !== month) {
            prevMonth = month;
            return m.format('MM-DD');
          } else {
            return m.format('DD');
          }
        }
      });

      setVisitorManagerCountChartData(initVisitorCountChartData);
      setVisitorUserCountChartData(initVisitorCountChartData);

      setVisitorCountChartXAxis(xAxis);
    }
  }, [period]);

  useEffect(() => {
    const brightMap = new Tile({
      source: new TileJSON({
        url: `https://api.maptiler.com/maps/streets/tiles.json?key=${MAP_TILE_KEY}`,
        tileSize: 512,
      }),
    });

    const floorsSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        LAYERS: 'watta:lms_view_area',
        VERSION: '1.1.1',
        FORMAT: 'image/png',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
        viewparams: `mapid:'${mapId}'`,
      },
    });

    const floorsLayer = new Image({
      source: floorsSource,
      visible: true,
    });

    const poiSource = new VectorSource({ wrapX: false });
    const poiLayer = new VectorLayer({
      source: poiSource,
      zIndex: 100,
    });

    if (!poiVectorSource) {
      setPoiVectorSource(poiSource);
    }

    const heatMapSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        FORMAT: 'image/PNG',
        VERSION: '1.1.1',
        LAYERS: 'watta:lms_heatmap_sqlv',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const heatMapLayer = new Image({
      visible: true,
      source: heatMapSource,
    });

    if (!heatMapImageWMS) {
      setHeatMapImageWMS(heatMapSource);
    }

    const zoneSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        LAYERS: 'watta:mng_zone_sqlv',
        VERSION: '1.1.1',
        FORMAT: 'image/png',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
        viewparams: `mapid:'${mapId}'`,
        ['CQL_FILTER']: ` zone_name = ''`,
      },
    });

    const zoneLayer = new Image({
      source: zoneSource,
      visible: false,
    });

    if (!zoneImage) {
      setZoneImage(zoneLayer);
    }

    if (!map) {
      const map = new OpenLayersMap({
        layers: [brightMap, floorsLayer, poiLayer, heatMapLayer, zoneLayer],
        target: mapContainerRef.current!,
        controls: [],
        interactions: defaults({
          doubleClickZoom: false,
        }),
        view: new View({
          resolutions: OPEN_LAYERS_RESOLUTIONS,
          maxResolution: OPEN_LAYERS_RESOLUTIONS[0],
          center: [14149654.863648022, 4495176.025755065],
          zoom: 10,
          projection: 'EPSG:3857',
          zoomFactor: 1,
        }),
      });
      setMap(map);
    }
  }, []);

  useEffect(() => {
    if (map) {
      map.getView().setCenter(mapPosition);
    }
  }, [map, mapPosition]);

  useEffect(() => {
    handleFetchPoi();
  }, [poiVectorSource]);

  useEffect(() => {
    const newCongestionTimeChartData = Array.from({ length: 16 }, () => 0);
    statAccessInfo.rushHourForPeriodByDaily.forEach((data) => {
      const logHour = Number(data.logHour);
      newCongestionTimeChartData.splice(logHour - 7, 1, data.cnt);
    });
    setCongestionTimeChartData(newCongestionTimeChartData);

    const daysArray = calcPeriodDays();
    const newVisitorManagerCountChartData = new Map<string, number>();
    const newVisitorUserCountChartData = new Map<string, number>();
    daysArray.forEach((m) => {
      const date = m.format('YYYY-MM-DD');
      newVisitorManagerCountChartData.set(date, 0);
      newVisitorUserCountChartData.set(date, 0);
    });

    statAccessInfo.enterCountForPeriodByDaily.user.forEach((data) => {
      const logDate = data.logDate;
      if (newVisitorUserCountChartData.has(logDate)) {
        newVisitorUserCountChartData.set(logDate, data.cnt);
      }
    });

    statAccessInfo.enterCountForPeriodByDaily.manager.forEach((data) => {
      const logDate = data.logDate;
      if (newVisitorManagerCountChartData.has(logDate)) {
        newVisitorManagerCountChartData.set(logDate, data.cnt);
      }
    });

    setVisitorManagerCountChartData(
      Array.from(newVisitorManagerCountChartData.values())
    );
    setVisitorUserCountChartData(
      Array.from(newVisitorUserCountChartData.values())
    );
  }, [statAccessInfo]);

  const calcPeriodDays = () => {
    const startDate = moment(period[0]);
    const endDate = moment(period[1]);
    const dateRange = moment.range(startDate, endDate);
    return Array.from(dateRange.by('days'));
  };

  const handleFetchStatAccessInfo = async () => {
    const data = await fetchStatAccessInfo({
      mappingId,
      mapId,
      fromDate: period[0],
      toDate: period[1],
      searchOption: selectedZone.zoneId,
    });

    if (data) {
      setStatAccessInfo(data);
    }
  };

  const handleFetchStatStatCongestionArea = async () => {
    if (heatMapImageWMS) {
      let params = `map_id:'${mapId}';`;
      params += `starttime:'${period[0]} 00:00:00';endtime:'${period[1]} 23:59:59';`;
      heatMapImageWMS.refresh();
      let cqlFilter = ` map_id = '${mapId}' `;
      if (selectedZone.geomStr) {
        cqlFilter += ` and INTERSECTS(geom, ${selectedZone.geomStr}) `;
      }
      const updateParams = {
        CQL_FILTER: cqlFilter,
        viewparams: params,
      };

      heatMapImageWMS.updateParams(updateParams);
      heatMapImageWMS.refresh();
    }
  };

  const handleChangeZone = () => {
    if (zoneImage) {
      const zoneImageWMS = zoneImage.getSource() as ImageWMS;
      const params = zoneImageWMS.getParams();
      params['viewparams'] = `mapid:'${mapId}'`;
      let visible = false;
      if (selectedZone.zoneId) {
        params['CQL_FILTER'] = ` zone_name = '${selectedZone.zoneName}'`;
        visible = true;
      } else {
        params['CQL_FILTER'] = ` zone_name = ''`;
      }
      zoneImage.setVisible(visible);
      zoneImageWMS.refresh();
    }
  };

  const handleFetchPoi = async () => {
    if (poiVectorSource) {
      const poiList = await fetchPoi({
        metaid: metaId,
        mapid: mapId,
      });

      if (poiList) {
        poiList.forEach((poi) => {
          const point = new Point([poi.xcoord, poi.ycoord]);
          const iconFeature = new Feature(point);
          iconFeature.setStyle(
            new Style({
              image: new Icon({
                src: `${Config.map_server.indoor_edit_uri}/data/${poi.filenm}`,
                scale: 0.5,
              }),
            })
          );
          poiVectorSource.addFeature(iconFeature);
          const textFeature = new Feature(point);
          textFeature.setStyle(
            new Style({
              text: new Text({
                text: poi.poi_name,
                scale: 1,
                textAlign: 'center',
                offsetY: 20,
                fill: new Fill({
                  color: '#000000',
                }),
                stroke: new Stroke({
                  color: '#FFFFFF',
                  width: 2,
                }),
                font: 'bold 12px roboto',
              }),
            })
          );
          poiVectorSource.addFeature(textFeature);
        });
      }
    }
  };

  useEffect(() => {
    if (heatMapImageWMS && mappingId && mapId && period.length > 1) {
      handleChangeZone();
      handleFetchStatAccessInfo();
      handleFetchStatStatCongestionArea();
    }
  }, [mappingId, mapId, period, selectedZone, heatMapImageWMS]);

  return (
    <div className="ch-group">
      <div className="row mb-4">
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_average_number_of_visitors_per_day')}</h6>
              <div className="cell keyword">
                <h5>{`${statAccessInfo.averageForPeriod} ${t(
                  'text_unit_of_person'
                )}`}</h5>
                {statAccessInfo.averageForPeriod -
                  statAccessInfo.averageForPeriodComparison !==
                  0 && (
                  <span>
                    {t('text_compared_to_the_previous_month')}{' '}
                    <VariationArrow
                      value={
                        statAccessInfo.averageForPeriod -
                        statAccessInfo.averageForPeriodComparison
                      }
                      unit={t('text_unit_of_person')}
                    />
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_cumulative_entrance_count')}</h6>
              <div className="cell keyword">
                <h5>{`${statAccessInfo.totalEnterCountForPeriod} ${t(
                  'text_unit_of_person'
                )}`}</h5>
                {statAccessInfo.totalEnterCountForPeriod -
                  statAccessInfo.totalEnterCountForPeriodComparison !==
                  0 && (
                  <span>
                    {t('text_compared_to_the_previous_month')}{' '}
                    <VariationArrow
                      value={
                        statAccessInfo.totalEnterCountForPeriod -
                        statAccessInfo.totalEnterCountForPeriodComparison
                      }
                      unit={t('text_unit_of_person')}
                    />
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_the_largest_number_of_entrances')}</h6>
              <div className="cell keyword">
                <h5>{statAccessInfo.frequentAreaForPeriod?.areaName || '-'}</h5>
                {statAccessInfo.frequentAreaForPeriod && (
                  <span className="text-accent">
                    {t('text_percent_of_the_entire_area', {
                      percent:
                        statAccessInfo.frequentAreaForPeriod?.totalPercent,
                    })}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_access_rush_hour')}</h6>
              <div className="cell keyword">
                <h5>
                  {statAccessInfo.rushHourForPeriod &&
                    `${moment(
                      statAccessInfo.rushHourForPeriod.logHour,
                      'hh'
                    ).format('LT')} ~ ${moment(
                      Number(statAccessInfo.rushHourForPeriod.logHour) + 1,
                      'hh'
                    ).format('LT')}`}
                  {!statAccessInfo.rushHourForPeriod && '-'}
                </h5>
                {statAccessInfo.rushHourForPeriod && (
                  <span className="text-accent">
                    {t('text_percent_of_the_time', {
                      percent: statAccessInfo.rushHourForPeriod?.totalPercent,
                    })}
                  </span>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body">
              <h6>{t('text_congestion_by_area')}</h6>
              <div
                style={{
                  width: '100%',
                  height: '97%',
                  borderRadius: '.25rem',
                }}
                onContextMenu={(e) => {
                  e.preventDefault();
                }}
                ref={mapContainerRef}
              ></div>
              <div className="btn-widget-holder z-index-0 mr-2">
                <ul>
                  <li>
                    <div className="btn-group-vertical">
                      <button
                        type="button"
                        className="btn btn-white"
                        onClick={() =>
                          map?.getView().animate({
                            zoom: (map?.getView().getZoom() || 0) + 0.5,
                            duration: 350,
                          })
                        }
                      >
                        <FontAwesomeIcon icon={['fas', 'plus']} />
                      </button>
                      <button
                        type="button"
                        className="btn btn-white"
                        onClick={() =>
                          map?.getView().animate({
                            zoom: (map?.getView().getZoom() || 0) + -0.5,
                            duration: 350,
                          })
                        }
                      >
                        <FontAwesomeIcon icon={['fas', 'minus']} />
                      </button>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6">
          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body">
                  <h6>{t('text_number_of_visitors_per_day')}</h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        chart: {
                          type: 'column',
                        },
                        title: null,
                        credits: {
                          enabled: false,
                        },
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          column: {
                            events: {
                              click: () => {
                                onClickNumberOfVisitorsPerDay();
                              },
                            },
                          },
                        },
                        xAxis: {
                          categories: visitorCountChartXAxis,
                        },
                        yAxis: {
                          minRange: 1,
                          min: 0,
                          title: null,
                        },
                        tooltip: {
                          pointFormat: '<b>{point.y}</b>',
                        },
                        legend: {
                          enabled: true,
                          layout: 'vertical',
                          align: 'right',
                          verticalAlign: 'top',
                          itemMarginTop: 5,
                          itemMarginBottom: 5,
                          symbolRadius: 0,
                        },
                        series: [
                          {
                            name: t('text_manager'),
                            data: visitorManagerCountChartData,
                            color: '#FEC57E',
                          },
                          {
                            name: t('text_user'),
                            data: visitorUserCountChartData,
                            color: '#DA732B',
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div className="card">
                <div className="card-body gray-scale">
                  <h6>{t('text_purpose_of_entry')}</h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        credits: {
                          enabled: false,
                        },
                        chart: {
                          type: 'pie',
                        },
                        title: null,
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                              enabled: false,
                            },
                            showInLegend: true,
                          },
                        },
                        tooltip: {
                          pointFormat: '<b>{point.percentage:.2f}%</b>',
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: 'Percent',
                            data: [
                              {
                                name: '근무',
                                y: 91.41,
                                color: '#E57030',
                              },
                              {
                                name: '비즈니스 회의',
                                y: 3.04,
                                color: '#3DC2DE',
                              },
                              {
                                name: '직원 방문',
                                y: 2.54,
                                color: '#2E80DF',
                              },
                              {
                                name: '기타',
                                y: 3.02,
                                color: '#B2DD8F',
                              },
                            ],
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="card">
                <div className="card-body">
                  <h6>{t('text_access_congestion_hours')}</h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        chart: {
                          type: 'line',
                        },
                        title: null,
                        credits: {
                          enabled: false,
                        },
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          line: {
                            marker: {
                              enabled: false,
                            },
                          },
                        },
                        xAxis: {
                          categories: Array.from(
                            { length: 16 },
                            (_, i) => i + 7
                          ),
                        },
                        yAxis: {
                          minRange: 1,
                          min: 0,
                          title: null,
                        },
                        tooltip: {
                          pointFormat: '{series.name}: <b>{point.y}</b>',
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: t('text_number_of_access_personnel'),
                            data: congestionTimeChartData,
                            color: '#2E80DF',
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function VisitorList() {
  const { t } = useTranslation();
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(5);

  return (
    <div className="ch-group">
      <div className="row">
        <div className="col d-flex">
          <div className="card">
            <div className="card-body">
              <h6>{t('text_entrant_list')}</h6>
              <div className="graph-box">
                <table className="table mb-4 table-nowrap">
                  <thead>
                    <tr>
                      <th>{t('text_month')}</th>
                      <th>{t('text_day')}</th>
                      <th>{t('text_employee_number')}</th>
                      <th>{t('text_purpose_of_entry')}</th>
                      <th>{t('text_visiting_department')}</th>
                      <th>{t('text_residence_time')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td rowSpan={2}>1</td>
                      <td rowSpan={2}>8</td>
                      <td>employee_0</td>
                      <td>{t('text_be_at_work')}</td>
                      <td>{t('text_development_department')}</td>
                      <td>10.02</td>
                    </tr>
                    <tr>
                      <td>employee_1</td>
                      <td>{t('text_be_at_work')}</td>
                      <td>{t('text_design_department')}</td>
                      <td>10.07</td>
                    </tr>
                  </tbody>
                </table>
                <Pagination curPage={page} totalCount={totalCount} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

type WorkspaceInfoProps = {
  activeKey: StatisticsTab;
  mapPosition: number[];
};

function WorkspaceInfo({ activeKey, mapPosition }: WorkspaceInfoProps) {
  const { t } = useTranslation();
  const [loadMap, setLoadMap] = useState(false);
  const mapContainerRef = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<OpenLayersMap | null>(null);

  useEffect(() => {
    if (activeKey === 'WORKSPACE_INFO' && !loadMap) {
      const brightMap = new Tile({
        source: new TileJSON({
          url: `https://api.maptiler.com/maps/streets/tiles.json?key=${MAP_TILE_KEY}`,
          tileSize: 512,
        }),
      });

      if (!map) {
        const map = new OpenLayersMap({
          layers: [brightMap],
          target: mapContainerRef.current!,
          controls: [],
          interactions: defaults({
            doubleClickZoom: false,
          }),
          view: new View({
            resolutions: OPEN_LAYERS_RESOLUTIONS,
            maxResolution: OPEN_LAYERS_RESOLUTIONS[0],
            center: [14149654.863648022, 4495176.025755065],
            zoom: 10,
            projection: 'EPSG:3857',
            zoomFactor: 1,
          }),
        });
        map.once('rendercomplete', (e) => {
          setLoadMap(true);
        });
        setMap(map);
      }
    }
  }, [activeKey]);

  useEffect(() => {
    if (map) {
      map.getView().setCenter(mapPosition);
    }
  }, [map, mapPosition]);

  return (
    <div className="ch-group">
      <div className="row mb-4">
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_average_daily_stay_time')}</h6>
              <div className="cell keyword">
                <em>11.23 {t('text_time')}</em>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_average_daily_desktop_hours_of_use')}</h6>
              <div className="cell keyword">
                <em>9.23 {t('text_time')}</em>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>
                {t('text_working_hours_compared_to_average_residence_time')}
              </h6>
              <div className="cell keyword">
                <em>82%</em>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-4 col-lg-3 d-flex">
          <div className="card m-0">
            <div className="card-body">
              <h6>{t('text_average_residence_time')}</h6>
              <div className="cell keyword">
                <em>p046 {t('text_area')}</em>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-4 d-flex">
          <div className="card">
            <div className="card-body gray-scale">
              <h6>{t('text_main_residence_area_by_department')}</h6>
              <div
                style={{
                  width: '100%',
                  height: '97%',
                  borderRadius: '.25rem',
                }}
                onContextMenu={(e) => {
                  e.preventDefault();
                }}
                ref={mapContainerRef}
              ></div>
            </div>
          </div>
        </div>
        <div className="col-lg-8">
          <div className="row">
            <div className="col-lg-12">
              <div className="card">
                <div className="card-body gray-scale">
                  <h6>
                    {t(
                      'text_average_daily_desktop_usage_time_and_stay_time_per_employee'
                    )}
                  </h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        chart: {
                          type: 'bar',
                        },
                        title: null,
                        credits: {
                          enabled: false,
                        },
                        plotOptions: {
                          series: {
                            animation: false,
                            stacking: 'normal',
                          },
                        },
                        xAxis: [
                          {
                            categories: [
                              'employee_145',
                              'employee_191',
                              'employee_221',
                              'employee_52',
                              'employee_108',
                              'employee_213',
                              'employee_234',
                              'employee_60',
                              'employee_125',
                            ],
                            reversed: false,
                            labels: {
                              step: 1,
                            },
                          },
                          {
                            opposite: true,
                            reversed: false,
                            linkedTo: 0,
                            labels: {
                              enabled: false,
                            },
                            tickLength: 0,
                          },
                        ],
                        yAxis: {
                          title: null,
                          labels: {
                            formatter: function () {
                              const self = this as any;
                              return Math.abs(self.value);
                            },
                          },
                        },
                        tooltip: {
                          formatter: function () {
                            const self = this as any;
                            return `${self.point.category} : ${Math.abs(
                              self.point.y
                            )}`;
                          },
                        },
                        legend: {
                          enabled: true,
                          layout: 'vertical',
                          align: 'right',
                          verticalAlign: 'top',
                          itemMarginTop: 5,
                          itemMarginBottom: 5,
                          symbolRadius: 0,
                        },
                        series: [
                          {
                            name: t('text_average_daily_desktop_hours_of_use'),
                            data: [-5, -6, -4, -4, -2, -1, -8, -1, -2],
                            color: '#2E80DF',
                          },
                          {
                            name: t('text_average_daily_stay_time'),
                            data: [1, 4, 2, 1, 7, 3, 4, 2, 8],
                            color: '#3DC2DE',
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-4">
              <div className="card">
                <div className="card-body gray-scale">
                  <h6>{t('text_main_residence_area_by_department')}</h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        credits: {
                          enabled: false,
                        },
                        chart: {
                          type: 'pie',
                        },
                        title: null,
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                              enabled: false,
                            },
                            showInLegend: true,
                          },
                        },
                        tooltip: {
                          pointFormat: '<b>{point.percentage:.2f}%</b>',
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: 'Percent',
                            data: [
                              {
                                name: 'p020',
                                y: 5.49,
                                color: '#E57030',
                              },
                              {
                                name: 'p028',
                                y: 1.1,
                                color: '#3DC2DE',
                              },
                              {
                                name: 'p037',
                                y: 15.38,
                                color: '#2E80DF',
                              },
                              {
                                name: 'p046',
                                y: 36.26,
                                color: '#B2DD8F',
                              },
                              {
                                name: 'p047',
                                y: 24.18,
                                color: '#385165',
                              },
                            ],
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="card">
                <div className="card-body gray-scale">
                  <h6>{t('text_average_go_to_work_time_by_department')}</h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        chart: {
                          type: 'line',
                        },
                        title: null,
                        credits: {
                          enabled: false,
                        },
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          line: {
                            marker: {
                              enabled: false,
                            },
                          },
                        },
                        xAxis: {
                          categories: [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16],
                        },
                        yAxis: {
                          min: 0,
                          title: null,
                        },
                        tooltip: {
                          pointFormat: '{series.name}: <b>{point.y}</b>',
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: '출근 직원 수',
                            data: [
                              0,
                              0,
                              4443,
                              4685,
                              114,
                              122,
                              111,
                              154,
                              123,
                              94,
                              0,
                            ],
                            color: '#2E80DF',
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="card">
                <div className="card-body gray-scale">
                  <h6>
                    {t('text_average_leave_the_office_time_by_department')}
                  </h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        chart: {
                          type: 'line',
                        },
                        title: null,
                        credits: {
                          enabled: false,
                        },
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          line: {
                            marker: {
                              enabled: false,
                            },
                          },
                        },
                        xAxis: {
                          categories: [
                            12,
                            13,
                            14,
                            15,
                            16,
                            17,
                            18,
                            19,
                            20,
                            21,
                            22,
                          ],
                        },
                        yAxis: {
                          min: 0,
                          title: null,
                        },
                        tooltip: {
                          pointFormat: '{series.name}: <b>{point.y}</b>',
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: '퇴근 직원 수',
                            data: [
                              116,
                              124,
                              135,
                              119,
                              98,
                              1148,
                              3361,
                              3405,
                              1113,
                              0,
                              0,
                            ],
                            color: '#2E80DF',
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

type EmployeeInfoProps = {
  activeKey: StatisticsTab;
  mapPosition: number[];
};

function EmployeeInfo({ activeKey, mapPosition }: EmployeeInfoProps) {
  const { t } = useTranslation();
  const [loadMap, setLoadMap] = useState(false);
  const mapContainerRef = useRef<HTMLDivElement>(null);
  const [map, setMap] = useState<OpenLayersMap | null>(null);

  useEffect(() => {
    if (activeKey === 'EMPLOYEE_INFO' && !loadMap) {
      const brightMap = new Tile({
        source: new TileJSON({
          url: `https://api.maptiler.com/maps/streets/tiles.json?key=${MAP_TILE_KEY}`,
          tileSize: 512,
        }),
      });

      if (!map) {
        const map = new OpenLayersMap({
          layers: [brightMap],
          target: mapContainerRef.current!,
          controls: [],
          interactions: defaults({
            doubleClickZoom: false,
          }),
          view: new View({
            resolutions: OPEN_LAYERS_RESOLUTIONS,
            maxResolution: OPEN_LAYERS_RESOLUTIONS[0],
            center: [14149654.863648022, 4495176.025755065],
            zoom: 10,
            projection: 'EPSG:3857',
            zoomFactor: 1,
          }),
        });
        map.once('rendercomplete', (e) => {
          setLoadMap(true);
        });
        setMap(map);
      }
    }
  }, [activeKey]);

  useEffect(() => {
    if (map) {
      map.getView().setCenter(mapPosition);
    }
  }, [map, mapPosition]);

  return (
    <div className="ch-group">
      <div className="row">
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body gray-scale">
              <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-2 sort-wrap">
                <div className="flex title-row">
                  <h6 className="d-flex align-items-center mb-0 ">
                    {t('text_employee_list')}
                  </h6>
                </div>
                <form className="form-inline ml-2">
                  <select className="form-control custom-select border-0">
                    <option value="">{t('text_whole_department')}</option>
                    <option value="">개발1팀</option>
                    <option value="">개발2팀</option>
                    <option value="">디자인팀</option>
                  </select>
                </form>
                <form className="form-inline ml-2">
                  <input
                    type="text"
                    className="form-control search"
                    placeholder={t('place_holder_search_employee')}
                  />
                  <a className="btn btn-sm btn-light">
                    <MaterialIcon name={'search'} />
                  </a>
                </form>
              </div>
              <PerfectScrollbar
                style={{ position: 'relative', maxHeight: '506px' }}
              >
                <table className="table mb-0 table-nowrap sm-table">
                  <thead>
                    <tr>
                      <th>{t('text_employee_name')}</th>
                      <th>{t('text_department')}</th>
                      <th>{t('text_year_of_employment')}</th>
                      <th>{t('text_average_daily_stay_time')}</th>
                      <th>{t('text_number_of_working_days_per_month')}</th>
                      <th>{t('text_interact')}</th>
                      <th>{t('text_frequency_of_movement')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <OverlayTrigger
                          trigger={'click'}
                          placement={'right'}
                          rootClose={true}
                          overlay={
                            <Popover id={'popover'} className={'popover-lg'}>
                              <PopoverContent>
                                <h6 className="mb-2">상세정보</h6>
                                <div className="text-center">
                                  <div className="avatar avatar-xxl mb-4">
                                    <span className="avatar-title rounded-circle">
                                      오
                                    </span>
                                  </div>
                                </div>
                                <div className="list-group list-group-flush list-popover">
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">성명</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        오영희
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">사원번호</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        employee_76
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">부서</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        개발1팀
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">직위</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        과장
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">휴대폰</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        010-1234-5678
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">직통전화</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        02-202-1049
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </PopoverContent>
                            </Popover>
                          }
                        >
                          <a className="d-flex text-black">
                            <div
                              className="media flex-nowrap align-items-center"
                              style={{
                                whiteSpace: 'nowrap',
                              }}
                            >
                              <div className="avatar avatar-xs mr-8pt">
                                <span className="avatar-title rounded-circle font-size-12pt">
                                  오
                                </span>
                              </div>
                              <div className="media-body">
                                <div className="d-flex flex-column">
                                  <p className="mb-0 text-underline">
                                    <strong className="js-lists-values-name">
                                      오영희
                                    </strong>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </a>
                        </OverlayTrigger>
                      </td>
                      <td>{t('text_development_department')}</td>
                      <td>2005</td>
                      <td>9.18</td>
                      <td>22</td>
                      <td>3</td>
                      <td>3</td>
                    </tr>
                  </tbody>
                </table>
              </PerfectScrollbar>
            </div>
          </div>
        </div>
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="card-body gray-scale">
              <h6>{t('text_in_house_network')}</h6>
              <div className="graph-box mb-4">
                <HighchartsReact
                  highcharts={Highcharts}
                  options={{
                    credits: {
                      enabled: false,
                    },
                    chart: {
                      type: 'networkgraph',
                    },
                    title: null,
                    plotOptions: {
                      series: {
                        animation: false,
                      },
                      networkgraph: {
                        layoutAlgorithm: {
                          enableSimulation: false,
                        },
                      },
                    },
                    series: [
                      {
                        marker: {
                          radius: 10,
                        },
                        color: '#C2CAE6',
                        dataLabels: {
                          enabled: true,
                          linkFormat: '',
                        },
                        nodes: [
                          {
                            id: 'Employee 1',
                            marker: {
                              radius: 20,
                            },
                            color: '#FF9898',
                          },
                          {
                            id: 'Employee 2',
                            marker: {
                              radius: 15,
                            },
                            color: '#E6C2CA',
                          },
                        ],
                        data: [
                          {
                            from: 'Employee 1',
                            to: 'Employee 2',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 3',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 4',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 5',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 6',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 7',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 8',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 9',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 10',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 11',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 12',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 13',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 14',
                          },
                          {
                            from: 'Employee 1',
                            to: 'Employee 15',
                          },
                          {
                            from: 'Employee 3',
                            to: 'Employee 16',
                          },
                          {
                            from: 'Employee 2',
                            to: 'Employee 17',
                          },
                          {
                            from: 'Employee 2',
                            to: 'Employee 18',
                          },
                          {
                            from: 'Employee 2',
                            to: 'Employee 19',
                          },
                          {
                            from: 'Employee 8',
                            to: 'Employee 20',
                          },
                        ],
                      },
                    ],
                  }}
                />
              </div>
              <PerfectScrollbar
                style={{ position: 'relative', maxHeight: '247px' }}
              >
                <table className="table mb-0 table-nowrap sm-table">
                  <thead>
                    <tr>
                      <th>{t('text_employee_name')}</th>
                      <th>{t('text_employee_number')}</th>
                      <th>{t('text_department')}</th>
                      <th>{t('text_interact')}</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <OverlayTrigger
                          trigger={'click'}
                          placement={'left'}
                          rootClose={true}
                          overlay={
                            <Popover id={'popover'} className={'popover-lg'}>
                              <PopoverContent>
                                <h6 className="mb-2">상세정보</h6>
                                <div className="text-center">
                                  <div className="avatar avatar-xxl mb-4">
                                    <span className="avatar-title rounded-circle">
                                      오
                                    </span>
                                  </div>
                                </div>
                                <div className="list-group list-group-flush list-popover">
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">성명</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        오영희
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">사원번호</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        employee_76
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">부서</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        개발1팀
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">직위</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        과장
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">휴대폰</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        010-1234-5678
                                      </span>
                                    </div>
                                  </div>
                                  <div className="list-group-item">
                                    <div className="d-flex popover-title">
                                      <span className="text-50">직통전화</span>
                                    </div>
                                    <div className="d-flex popover-content">
                                      <span className="text-headings">
                                        02-202-1049
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </PopoverContent>
                            </Popover>
                          }
                        >
                          <a className="d-flex text-black">
                            <div
                              className="media flex-nowrap align-items-center"
                              style={{ whiteSpace: 'nowrap' }}
                            >
                              <div className="avatar avatar-xs mr-8pt">
                                <span className="avatar-title rounded-circle font-size-12pt">
                                  오
                                </span>
                              </div>
                              <div className="media-body">
                                <div className="d-flex flex-column">
                                  <p className="mb-0 text-underline">
                                    <strong className="js-lists-values-name">
                                      오영희
                                    </strong>
                                  </p>
                                </div>
                              </div>
                            </div>
                          </a>
                        </OverlayTrigger>
                      </td>
                      <td>employee_1</td>
                      <td>{t('text_development_department')}</td>
                      <td>5</td>
                    </tr>
                  </tbody>
                </table>
              </PerfectScrollbar>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="half-container half-row">
              <div className="half-60">
                <div
                  className="card-body gray-scale"
                  style={{
                    height: '100%',
                  }}
                >
                  <h6>{t('text_main_residence_area')}</h6>
                  <div
                    style={{
                      width: '100%',
                      height: '97%',
                      borderRadius: '.25rem',
                    }}
                    onContextMenu={(e) => {
                      e.preventDefault();
                    }}
                    ref={mapContainerRef}
                  ></div>
                </div>
              </div>
              <div className="half-40">
                <div className="card-body gray-scale">
                  <h6>{t('text_residence_information')}</h6>
                  <div className="graph-box mb-4">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        credits: {
                          enabled: false,
                        },
                        chart: {
                          type: 'pie',
                        },
                        title: null,
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                              enabled: false,
                            },
                            showInLegend: true,
                          },
                        },
                        tooltip: {
                          pointFormat: '<b>{point.percentage:.2f}%</b>',
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: 'Percent',
                            data: [
                              {
                                name: 'p022',
                                y: 6.59,
                                color: '#E57030',
                              },
                              {
                                name: 'p028',
                                y: 1.1,
                                color: '#3DC2DE',
                              },
                              {
                                name: 'p037',
                                y: 15.38,
                                color: '#2E80DF',
                              },
                              {
                                name: 'p045',
                                y: 1.1,
                                color: '#B2DD8F',
                              },
                              {
                                name: 'p046',
                                y: 36.26,
                                color: '#D9D9DB',
                              },
                              {
                                name: 'p047',
                                y: 24.18,
                                color: '#385165',
                              },
                            ],
                          },
                        ],
                      }}
                    />
                  </div>
                  <ul className="list-unstyled bg-light p-3 m-0 rounded">
                    <li className="d-flex align-items-center mb-2">
                      <span className="material-icons icon-16pt text-50 mr-8pt">
                        check
                      </span>
                      <p
                        className="m-0"
                        dangerouslySetInnerHTML={{
                          __html: t(
                            'text_percent_of_residence_in_the_department_area',
                            {
                              percent: 36.26,
                            }
                          ),
                        }}
                      ></p>
                    </li>
                    <li className="d-flex align-items-center">
                      <span className="material-icons icon-16pt text-50 mr-8pt">
                        check
                      </span>
                      <p
                        className="m-0"
                        dangerouslySetInnerHTML={{
                          __html: t(
                            'text_percent_of_residence_in_the_etc_area',
                            {
                              percent: 63.74,
                            }
                          ),
                        }}
                      ></p>
                    </li>
                  </ul>
                  <ul className="list-unstyled">
                    <li className="d-flex align-items-center p-2 border-bottom-1">
                      <strong className="text-accent mr-3">1st.</strong>
                      <p className="flex m-0">1회의실</p>
                      <span className="text-muted">24.18%</span>
                    </li>
                    <li className="d-flex align-items-center p-2 border-bottom-1">
                      <strong className="text-dark mr-3">2st.</strong>
                      <p className="flex m-0">2회의실</p>
                      <span className="text-muted">15.38%</span>
                    </li>
                    <li className="d-flex align-items-center p-2 border-bottom-1">
                      <strong className="text-dark mr-3">3st.</strong>
                      <p className="flex m-0">탕비실</p>
                      <span className="text-muted">6.59%</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-lg-6 d-flex">
          <div className="card">
            <div className="half-container half-row">
              <div className="half-60">
                <div className="card-body gray-scale">
                  <h6>
                    {t(
                      'text_office_residence_time_and_desktop_usage_time_by_day'
                    )}
                  </h6>
                  <div className="graph-box">
                    <HighchartsReact
                      highcharts={Highcharts}
                      options={{
                        chart: {
                          type: 'columnrange',
                          inverted: true,
                          height: '690',
                        },

                        title: null,
                        credits: {
                          enabled: false,
                        },
                        plotOptions: {
                          series: {
                            animation: false,
                          },
                          columnrange: {
                            grouping: false,
                          },
                        },
                        xAxis: {
                          categories: Array.from(
                            { length: 30 },
                            (_, i) => i + 1
                          ),
                        },
                        yAxis: {
                          title: null,
                        },
                        tooltip: {
                          formatter: function () {
                            const self = this as any;
                            return `${self.point.low} ~ ${self.point.high}`;
                          },
                        },
                        legend: {
                          enabled: false,
                        },
                        series: [
                          {
                            name: '출근',
                            data: [
                              [8, 18],
                              [8, 17],
                              [8, 18],
                              [9, 19],
                              [8, 20],
                              [10, 17],
                              [7, 16],
                              [9, 19],
                              [10, 20],
                              [11, 19],
                              [9, 18],
                              [8, 16],
                              [8, 18],
                              [8, 17],
                              [8, 18],
                              [9, 19],
                              [8, 20],
                              [10, 17],
                              [7, 16],
                              [9, 19],
                              [9, 18],
                              [8, 16],
                              [8, 18],
                              [8, 17],
                              [8, 18],
                              [9, 19],
                              [8, 20],
                              [10, 17],
                              [7, 16],
                              [9, 19],
                            ],
                            color: '#FEC57E',
                          },
                          {
                            name: '업무',
                            data: [
                              [9, 17],
                              [9, 17],
                              [9, 17],
                              [10, 18],
                              [10, 18],
                              [10, 17],
                              [9, 14],
                              [12, 16],
                              [11, 18],
                              [13, 17],
                              [9, 16],
                              [11, 14],
                              [9, 17],
                              [9, 17],
                              [9, 17],
                              [10, 18],
                              [10, 18],
                              [10, 17],
                              [9, 14],
                              [12, 16],
                              [9, 16],
                              [11, 14],
                              [9, 17],
                              [9, 17],
                              [9, 17],
                              [10, 18],
                              [10, 18],
                              [10, 17],
                              [9, 14],
                              [12, 16],
                            ],
                            color: '#AE3450',
                          },
                        ],
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="half-40">
                <div className="card-body gray-scale">
                  <h6>{t('text_attendance_information')}</h6>
                  <ul className="list-unstyled">
                    <li className="p-2 border-bottom-1 border-top-1">
                      <p className="m-0">
                        {t('text_average_hours_of_residence', {
                          hour: 11.23,
                        })}
                      </p>
                      <span className="text-muted font-size-12pt">
                        {t('text_compared_to_the_previous_month')}{' '}
                        <strong className="text-accent state up">
                          1.1{t('text_time')}
                        </strong>
                      </span>
                    </li>
                    <li className="p-2 border-bottom-1">
                      <p className="flex m-0">
                        {t('text_average_hour_desktop_usage', {
                          hour: 8.21,
                        })}
                      </p>
                      <span className="text-muted">
                        {`${t('text_compared_to_the_previous_month')} ${t(
                          'text_no_change'
                        )}`}
                      </span>
                    </li>
                    <li className="p-2 border-bottom-1">
                      <p className="flex m-0">
                        {t('text_count_of_annual_leave_usage', {
                          count: 0,
                        })}
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FloatStatisticsSmartOffice;
