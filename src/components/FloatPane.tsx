import React from 'react';
import LeftSidebarDepth from '@/components/LeftSidebarDepth';
import { useFloatPane, useRightPane } from '@/modules/setup/hook';
import classNames from 'classnames';
import {
  FLOAT_PANE_PHYSICAL_DISTRIBUTION,
  FLOAT_PANE_STATISTICS,
} from '@/modules/setup/types';
import FloatPhysicalDistributionPane from '@/components/FloatPhysicalDistributionPane';
import FloatStatisticsPane from '@/components/FloatStatisticsPane';

function FloatPane() {
  const { show: floatPaneShow, view } = useFloatPane();
  const { show: rightPaneShow } = useRightPane();

  return (
    <>
      <LeftSidebarDepth
        show={floatPaneShow}
        className={classNames({
          'sidebar-float': rightPaneShow,
        })}
      >
        {typeof view === 'undefined' && <></>}
        {view === FLOAT_PANE_PHYSICAL_DISTRIBUTION && (
          <FloatPhysicalDistributionPane />
        )}
        {view === FLOAT_PANE_STATISTICS && <FloatStatisticsPane />}
      </LeftSidebarDepth>
    </>
  );
}

export default FloatPane;
