import { useTranslation } from 'react-i18next';
import React, { useRef, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useActiveMenu } from '@/modules/setup/hook';
import { SearchDivision } from '@/modules/setup/types';

function SearchNavbar() {
  const { t } = useTranslation();
  const { handleSearchActive } = useActiveMenu();

  const [searchDivision, setSearchDivision] = useState<SearchDivision>(0);
  const searchInput = useRef<HTMLInputElement>(null);

  const handleSearchSubmit = () => {
    const keyword = searchInput.current?.value || '';
    if (keyword) {
      handleSearchActive({
        keyword,
        division: searchDivision,
        flag: false,
      });
    }
  };

  return (
    <div className="nav-search">
      {/*
      <select
        value={searchDivision}
        onChange={(e) => {
          setSearchDivision(Number(e.target.value) as SearchDivision);
        }}
      >
        <option value="0">{t('text_poi')}</option>
        <option value="2">{t('text_sensor')}</option>
        <option value="1">{t('text_place')}</option>
      </select>
      <input
        type="text"
        placeholder={t('place_holder_search')}
        ref={searchInput}
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            handleSearchSubmit();
          }
        }}
      />
      <button type="button" onClick={handleSearchSubmit}>
        <FontAwesomeIcon
          icon={['fas', 'search']}
          className={'font-size-16pt'}
        />
      </button>
      */}
    </div>
  );
}

export default SearchNavbar;
