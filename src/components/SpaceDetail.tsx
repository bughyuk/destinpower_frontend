import React, { useEffect, useRef } from 'react';
import { Dropdown } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import { FUNCTION_DELETE } from '@/utils/constants/common';
import { useDropdown } from '@/modules/common';
import SpaceFloorsDetail from '@/components/SpaceFloorsDetail';
import { useSpace, useOpenLayers } from '@/modules/space/hook';
import { fetchSpaceInfo, postUpdateSpace } from '@/api/space';
import { useHistory } from 'react-router-dom';
import { useUser } from '@/modules/user/hook';

function SpaceDetail() {
  const { info, handleSetSpaceInfo } = useSpace();
  const { map } = useOpenLayers();

  const handleFetchSpaceInfo = async () => {
    const data = await fetchSpaceInfo(info.space.id);
    if (data) {
      handleSetSpaceInfo({
        id: data.spaceId,
        name: data.spaceName,
        city: data.city,
        country: data.country,
        address: data.address,
        location: {
          lng: data.lng,
          lat: data.lat,
        },
      });
      map?.getView().setCenter([data.lng, data.lat]);
    }
  };

  useEffect(() => {
    handleFetchSpaceInfo();
  }, []);

  return (
    <>
      <Header />
      <Body />
    </>
  );
}

function Header() {
  const { t } = useTranslation();
  const { user } = useUser();
  const { info } = useSpace();
  const history = useHistory();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  const handleDeleteSpace = async () => {
    const result = await postUpdateSpace({
      userid: user.userId,
      del: true,
      metaid: info.space.id,
    });

    if (result) {
      history.replace('/home');
    }
  };

  const handleOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_DELETE:
        handleDeleteSpace();
        break;
    }
  };

  return (
    <div className="container-fluid py-4">
      <div className="d-flex flex-md-row align-items-center mb-2">
        <div className="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">
          <div className="mb-24pt mb-sm-0 mr-sm-24pt">
            <h3 className="mb-0">{info.space.name}</h3>
          </div>
        </div>
        <div className="row">
          <div className="col-auto">
            <Dropdown onToggle={handleToggle} onSelect={handleOptions}>
              <Dropdown.Toggle
                as={'a'}
                data-caret="false"
                className="text-muted"
              >
                <MaterialIcon name={'more_vert'} />
              </Dropdown.Toggle>
              <Dropdown.Menu align={'right'} ref={dropdown}>
                {/*
                <Dropdown.Item
                  className="text-primary"
                  eventKey={FUNCTION_CHANGE_NAME}
                >
                  {t('text_change_buildings_name')}
                </Dropdown.Item>
                */}
                <Dropdown.Item
                  className="text-danger"
                  eventKey={FUNCTION_DELETE}
                >
                  {t('text_delete')}
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        </div>
      </div>
      <div className="board-title">
        <p>
          {`${info.space.country || ''}${info.space.city || ''}${
            info.space.address
          }`}
        </p>
      </div>
    </div>
  );
}

function Body() {
  return (
    <div className="container-fluid">
      <SpaceFloorsDetail />
    </div>
  );
}

export default SpaceDetail;
