import React, { useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import FormGroup from '@/components/FormGroup';
import InvalidAlert from '@/components/InvalidAlert';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormLabel from '@/components/FormLabel';
import { fetchItemCode, postItem } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import classNames from 'classnames';

type PhysicalDistributionItemRegisterProps = {
  onFinish: () => void;
  onClickCancel: () => void;
};

function PhysicalDistributionItemRegister({
  onFinish,
  onClickCancel,
}: PhysicalDistributionItemRegisterProps) {
  const { t } = useTranslation();
  const { project } = useControlProject();
  const [inputs, setInputs] = useState<{
    itemTitle: string;
    productCodePrefix: string;
    maker: string;
  }>({
    itemTitle: '',
    productCodePrefix: '',
    maker: '',
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [invalidMessage, setInvalidMessage] = useState(
    'msg_valid_enter_required_value'
  );
  const [loading, setLoading] = useState(false);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid =
      inputs.itemTitle && inputs.productCodePrefix && inputs.maker;

    if (isValid) {
      setLoading(true);
      const isDuplicateItemCode = await fetchItemCode(
        project.id,
        inputs.productCodePrefix
      );
      if (isDuplicateItemCode) {
        setInvalidMessage('msg_valid_duplicate_identifier');
        setShowInvalidMessage(true);
      } else {
        const result = await postItem({
          projectId: project.id,
          productCategoryName: inputs.itemTitle,
          productCodePrefix: inputs.productCodePrefix,
          maker: inputs.maker,
        });

        if (result) {
          onFinish();
        }
      }

      setLoading(false);
    } else {
      setInvalidMessage('msg_valid_enter_required_value');
      setShowInvalidMessage(true);
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel
          textKey={'text_item_title'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="itemTitle"
          placeholder={t('place_holder_item_title')}
          autoComplete={'off'}
          onChange={handleChangeInputsValue}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_identifier'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="productCodePrefix"
          placeholder={t('place_holder_identifier')}
          autoComplete={'off'}
          onChange={handleChangeInputsValue}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_item_maker'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="maker"
          placeholder={t('place_holder_item_maker')}
          autoComplete={'off'}
          onChange={handleChangeInputsValue}
        />
      </FormGroup>
      {showInvalidMessage && <InvalidAlert messageKey={invalidMessage} />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={onClickCancel}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_do_add')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default PhysicalDistributionItemRegister;
