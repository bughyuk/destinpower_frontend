import React, { useEffect, useState } from 'react';
import ProjectRegisterHeader from '@/components/ProjectRegisterHeader';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import { useProjectPane, useProjectRegister } from '@/modules/project/hook';
import ProjectInfoInput from '@/components/ProjectInfoInput';
import { postProject, putProject, putProjectStepData } from '@/api/project';
import { ProjectProduceStep } from '@/modules/project/types';
import InvalidAlert from '@/components/InvalidAlert';
import { useHistory } from 'react-router-dom';

function ProjectInfoRegister() {
  const [projectName, setProjectName] = useState('');
  const [note, setNote] = useState('');
  const [isValid, setValid] = useState(false);
  const {
    projectInfo,
    projectProduceStep,
    handleChangeRegisterStep,
    handleSetProjectInfo,
    handleSetProjectProduceStep,
  } = useProjectRegister();

  useEffect(() => {
    setProjectName(projectInfo.projectName || '');
    setNote(projectInfo.note || '');
  }, []);

  useEffect(() => {
    if (projectName && note) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [projectName, note]);

  const handleSubmit = async () => {
    const projectId = projectInfo.projectId;
    let isPass = false;
    if (projectId) {
      const result = await putProject(projectId, {
        projectName,
        note,
      });

      if (result) {
        handleSetProjectInfo({
          ...projectInfo,
          projectName,
          note,
        });
        const saveProjectProduceStep: ProjectProduceStep = {
          ...projectProduceStep,
          step: 2,
        };
        handleSetProjectProduceStep(saveProjectProduceStep);
        await putProjectStepData(projectId, {
          produceStepData: JSON.stringify(saveProjectProduceStep),
        });
        isPass = true;
      }
    } else {
      const registerProjectId = await postProject({
        projectName,
        note,
      });

      if (registerProjectId) {
        handleSetProjectInfo({
          projectId: registerProjectId,
          projectName,
          note,
        });
        const saveProjectProduceStep: ProjectProduceStep = {
          step: 2,
        };
        handleSetProjectProduceStep(saveProjectProduceStep);
        await putProjectStepData(registerProjectId, {
          produceStepData: JSON.stringify(saveProjectProduceStep),
        });
        isPass = true;
      }
    }

    if (isPass) {
      handleChangeRegisterStep(2);
    }
  };

  return (
    <>
      <ProjectRegisterHeader />
      <ProjectInfoInput
        projectName={projectName}
        note={note}
        onChangeProjectName={setProjectName}
        onChangeNote={setNote}
      />
      <Footer onSubmit={handleSubmit} valid={isValid} />
    </>
  );
}

type FooterProps = {
  valid: boolean;
  onSubmit: () => void;
};

function Footer({ valid, onSubmit }: FooterProps) {
  const history = useHistory();
  const { t } = useTranslation();
  const { handleChangePaneStatus } = useProjectPane();
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (valid) {
      setShowInvalidMessage(false);
    }
  }, [valid]);

  return (
    <>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => history.replace('/home')}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0')}
            onClick={() => {
              if (valid) {
                setShowInvalidMessage(false);
                onSubmit();
              } else {
                setShowInvalidMessage(true);
              }
            }}
          >
            {t('text_save_and_to_the_next')}
          </a>
        </div>
      </div>
    </>
  );
}

export default ProjectInfoRegister;
