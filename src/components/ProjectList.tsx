import React, { useEffect, useState } from 'react';
import ListHeader from '@/components/ListHeader';
import {
  useOpenLayers,
  useProjectEdit,
  useProjectPane,
  useProjectRegister,
} from '@/modules/project/hook';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import { Project, Projects } from '@/modules/project/types';
import ProjectListDropdown from '@/components/ProjectListDropdown';
import DateBadge from '@/components/DateBadge';
import { ListResult, useAsync } from '@/modules/common';
import {
  deleteProject,
  fetchProject,
  fetchProjects,
  putProjectStepData,
} from '@/api/project';
import EmptyPane from '@/components/EmptyPane';
import Pagination from '@/components/Pagination';
import { useHistory } from 'react-router-dom';
import { useControlProject } from '@/modules/map/hook';
import { Space, Floors } from '@/modules/map/types';
import { Badge } from 'react-bootstrap';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { useSpace } from '@/modules/space/hook';
import { fetchSpaceInfo } from '@/api/space';

function ProjectList() {
  const { handleChangePaneStatus } = useProjectPane();
  const [state, api] = useAsync(fetchProjects);
  const { loading, data } = state;
  const [projects, setProjects] = useState<Projects>([]);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);

  useEffect(() => {
    handleFetchProjects();
  }, [page]);

  const handleFetchProjects = (fetchPage: number = page) => {
    setPage(fetchPage);
    api(fetchPage, '');
  };

  useEffect(() => {
    if (data) {
      const listResult = data as ListResult<Projects>;
      setProjects(listResult.content);
      setTotalCount(listResult.totalElements);
    }
  }, [data]);

  return (
    <>
      {data && projects.length === 0 && (
        <EmptyPane
          textKey={'msg_project_empty'}
          descriptionKey={'msg_project_suggest_to_register'}
          btnTextKey={'text_project_registration'}
          onClick={() => {
            handleChangePaneStatus(PANE_STATUS_REGISTER);
          }}
        />
      )}
      {projects.length > 0 && (
        <>
          <ProjectItemList
            projects={projects}
            onRefresh={handleFetchProjects}
          />
          <Pagination
            curPage={page}
            totalCount={totalCount}
            onPageChange={setPage}
          />
        </>
      )}
    </>
  );
}

type ProjectItemListProps = {
  projects: Projects;
  onRefresh: () => void;
};

function ProjectItemList({ projects, onRefresh }: ProjectItemListProps) {
  const { handleChangePaneStatus } = useProjectPane();

  return (
    <>
      <ListHeader
        titleKey={'text_project'}
        descriptionKey={'msg_project_list'}
        createBtnTextKey={'text_new_registration'}
        onCreateBtnClick={() => handleChangePaneStatus(PANE_STATUS_REGISTER)}
      />
      <div className="container-fluid">
        <div className="list-group row-list list-group-flush mb-4">
          {projects.map((project) => (
            <ProjectItem
              key={project.projectId}
              {...project}
              onRefresh={onRefresh}
            />
          ))}
        </div>
      </div>
    </>
  );
}

type ProjectItemProps = Project & {
  onRefresh: () => void;
};

function ProjectItem({
  projectId,
  projectName,
  registDate,
  produceEndFlag,
  onRefresh,
}: ProjectItemProps) {
  const { t } = useTranslation();
  const { handleReloadSpaceLocationOverlay } = useOpenLayers();
  const [deleteState, deleteApi] = useAsync(deleteProject);
  const { handleRouteEdit } = useProjectEdit();
  const history = useHistory();
  const { handleChangePaneStatus } = useProjectPane();
  const {
    handleChangeRegisterStep,
    handleSetConnectedMetaIds,
    handleSetProjectProduceStep,
    handleSetProjectInfo,
    handleSetRegisterInitialState,
  } = useProjectRegister();
  const { handleSetSpaceInfo } = useSpace();
  const { handleSetProject } = useControlProject();
  const { data: deleteData } = deleteState;

  const handleOptions = (textKey: string | null) => {
    switch (textKey) {
      case FUNCTION_EDIT:
        handleRouteEdit(projectId);
        break;
      case FUNCTION_DELETE:
        deleteApi(projectId);
        break;
    }
  };

  useEffect(() => {
    if (deleteData) {
      onRefresh();
      handleReloadSpaceLocationOverlay();
    }
  }, [deleteData]);

  const handleFetchProject = async () => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      if (!produceEndFlag) {
        handleSetRegisterInitialState();
        const produceStep = projectDetail.produceStep;
        const step = produceStep.step;

        handleSetProjectInfo({
          projectId: projectDetail.projectId,
          projectName: projectDetail.projectName,
          note: projectDetail.note,
          metaIds: projectDetail.buildings.map(
            (connectSpace) => connectSpace.metaId
          ),
        });

        handleSetProjectProduceStep(projectDetail.produceStep);
        handleSetConnectedMetaIds(
          projectDetail.buildings.map((space) => space.metaId)
        );

        if (step) {
          handleChangeRegisterStep(step);

          if (step === 4 || step === 5) {
            const metaId = produceStep.metaId;

            if (metaId) {
              const spaceMetaInfo = await fetchSpaceInfo(metaId);

              if (spaceMetaInfo.spaceId) {
                handleSetSpaceInfo({
                  id: metaId,
                  location: {
                    lng: spaceMetaInfo.lng,
                    lat: spaceMetaInfo.lat,
                  },
                });
              } else {
                await putProjectStepData(projectId, {
                  produceStepData: JSON.stringify({
                    metaId: '',
                    step: 2,
                  }),
                });
                handleChangeRegisterStep(2);
              }
            }
          }

          handleChangePaneStatus(PANE_STATUS_REGISTER);
        }
        return;
      }

      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((space) => {
        const projectFloors: Floors[] = [];
        space.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: space.mappingId,
          id: space.metaId,
          name: space.metaName,
          longitude: space.lng,
          latitude: space.lat,
          floorsList: projectFloors,
          registDate: space.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.push('/control');
    }
  };

  return (
    <div className="list-group-item d-flex">
      <div
        className="flex d-flex align-items-center mr-16pt"
        onClick={handleFetchProject}
      >
        <a className="avatar mr-12pt blank-img"></a>
        <div className="flex list-els">
          <a className="card-title">{projectName}</a>
          <div className="card-subtitle text-50">
            {produceEndFlag && <DateBadge date={registDate} />}
            {!produceEndFlag && (
              <>
                <Badge variant={'accent'}>
                  {t('text_project_editing_status')}
                </Badge>
                {moment(registDate).format('YYYY.MM.DD')}
              </>
            )}
          </div>
        </div>
      </div>
      <ProjectListDropdown onSelect={handleOptions} showEdit={produceEndFlag} />
    </div>
  );
}

export default ProjectList;
