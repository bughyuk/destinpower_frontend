import React, { useEffect, useState } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import ReactQuill from 'react-quill';
import {
  Client,
  ClientKind,
  PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING,
  PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING,
} from '@/modules/physical_distribution/types';
// import { useControlProject } from '@/modules/map/hook';
import classNames from 'classnames';
import { dp_postClient } from '@/api/physical_distribution';

type PhysicalDistributionClientEditProps = PaneProps & {
  client: Client;
};

function PhysicalDistributionClientEdit({
  client,
  onChangeStatus,
}: PhysicalDistributionClientEditProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [inputs, setInputs] = useState<{
    kind: ClientKind;
    clientTitle: string;
    phoneNumber: string;
    email: string;
    memo: string;
  }>({
    kind: client.logisticsCategory,
    clientTitle: client.customerName,
    phoneNumber: client.phoneNumber,
    email: client.mailId,
    memo: client.note,
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!client.customerId) {
      onChangeStatus(PANE_STATUS_LIST);
    }
  }, [client]);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid = inputs.clientTitle && inputs.phoneNumber && inputs.email;

    if (isValid) {
      setLoading(true);
      const result = await dp_postClient({
        id: client.customerId, //거래처 id 값이 있으면 수정처리
        type:
          inputs.kind === PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING
            ? 'o'
            : 'i',
        name: inputs.clientTitle,
        phone: inputs.phoneNumber,
        email: inputs.email,
        comment: inputs.memo,
      });

      if (result) {
        onChangeStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'text_kind'} className={'mb-0'} essential={true} />
        <div className="d-flex pt-3 pb-2">
          <div className="custom-control custom-radio mr-4">
            <input
              id="warehousingClient"
              name="kind"
              type="radio"
              className="custom-control-input"
              value={PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING}
              checked={inputs.kind === PHYSICAL_DISTRIBUTION_CLIENT_WAREHOUSING}
              onChange={(e) => {
                setInputs({
                  ...inputs,
                  kind: e.target.value as ClientKind,
                });
              }}
            />
            <label htmlFor="warehousingClient" className="custom-control-label">
              {t('text_warehousing_client')}
            </label>
          </div>
          <div className="custom-control custom-radio">
            <input
              id="releaseClient"
              name="kind"
              type="radio"
              className="custom-control-input"
              value={PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING}
              checked={
                inputs.kind === PHYSICAL_DISTRIBUTION_CLIENT_RELEASE_WAREHOUSING
              }
              onChange={(e) => {
                setInputs({
                  ...inputs,
                  kind: e.target.value as ClientKind,
                });
              }}
            />
            <label htmlFor="releaseClient" className="custom-control-label">
              {t('text_release_client')}
            </label>
          </div>
        </div>
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_client_title'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="clientTitle"
          placeholder={t('place_holder_client_title')}
          value={inputs.clientTitle}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_phone_number'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          id="phoneNumber"
          placeholder={t('place_holder_phone_number')}
          value={inputs.phoneNumber}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_email'} className={'mb-0'} essential={true} />
        <input
          type="text"
          className="form-line"
          id="email"
          placeholder={t('place_holder_contact_email')}
          value={inputs.email}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_memo'} />
        <div style={{ height: '192px' }}>
          <ReactQuill
            id="memo"
            style={{ height: '150px' }}
            placeholder={t('place_holder_memo')}
            modules={{
              toolbar: [
                ['bold', 'italic'],
                ['link', 'blockquote', 'code', 'image'],
                [{ list: 'ordered' }, { list: 'bullet' }],
              ],
            }}
            value={inputs.memo}
            onChange={(content, delta, source, editor) => {
              if (!editor.getText().trim().length) {
                content = '';
              }

              setInputs({
                ...inputs,
                memo: content,
              });
            }}
          />
        </div>
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => onChangeStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_do_edit')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default PhysicalDistributionClientEdit;
