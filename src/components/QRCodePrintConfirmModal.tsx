import React, { useEffect, useState } from 'react';
import { ModalProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { Modal, ModalBody, ModalFooter, ModalTitle } from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/ModalHeader';
import {
  QRCodePrintConfirmData,
  WarehousingQRCodePrintingData,
} from '@/modules/physical_distribution/types';
import classNames from 'classnames';
import InputNumber from '@/components/InputNumber';
import { printQRCode } from '@/utils/bixolon-bridge';
import {
  BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER,
  BIXOLON_ERROR_CODE_ETC_ERROR,
  ResponsePrint,
} from '@/utils/constants/common';
import NewlineText from '@/components/NewlineText';
import AlertModal from '@/components/AlertModal';

type QRCodePrintConfirmModalProps = ModalProps & {
  qrCodePrintConfirmData: QRCodePrintConfirmData;
};

function QRCodePrintConfirmModal({
  show,
  qrCodePrintConfirmData,
  onHide,
}: QRCodePrintConfirmModalProps) {
  const { t } = useTranslation();
  const {
    image,
    totalLogisticsQuantity,
    warehousingDate,
    productName,
    productStandard,
  } = qrCodePrintConfirmData;
  const [printing, setPrinting] = useState(false);
  const [printCount, setPrintCount] = useState(0);
  const [printingCompleteCount, setPrintingCompleteCount] = useState(0);
  const [showAlertModal, setShowAlertModal] = useState(false);
  const [alertMessage, setAlertMessage] = useState('');

  const handleShowAlertModal = (message: string) => {
    setAlertMessage(message);
    setShowAlertModal(true);
  };

  const handleClickQRRePrinting = () => {
    if (printing) {
      return;
    }
    if (printCount === 0) {
      handleShowAlertModal(t('msg_printing_with_more_than_one_box'));
      return;
    }
    setPrintingCompleteCount(0);
    setPrinting(true);
    let competeCount = 0;
    printQRCode(
      {
        image,
        date: `${t('text_warehousing_date')} : ${warehousingDate}`,
        quantity: `${t('text_box_quantity')} :  ${totalLogisticsQuantity}`,
        productName: `${t('text_product_title')} : ${productName}`,
        standard: `${t('text_standard')} : ${productStandard}`,
      },
      printCount,
      (responsePrint: ResponsePrint) => {
        let resultMessage = '';
        switch (responsePrint.errorCode) {
          case BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER:
            resultMessage = t('msg_printer_server_can_not_connect_to_server');
            break;
          case BIXOLON_ERROR_CODE_ETC_ERROR:
            resultMessage = t('msg_printer_impossible_due_to_error');
            break;
        }

        if (resultMessage) {
          setPrinting(false);
          handleShowAlertModal(resultMessage);
        } else {
          competeCount += 1;
          setPrintingCompleteCount(competeCount);
        }
      }
    );
  };

  const handleClickConfirm = () => {
    if (!printing) {
      onHide();
    }
  };

  useEffect(() => {
    if (printingCompleteCount >= printCount) {
      setPrinting(false);
    }
  }, [printingCompleteCount]);

  return (
    <>
      <Modal
        show={show}
        onHide={() => {
          //
        }}
        onExited={() => {
          setPrintingCompleteCount(0);
          setPrintCount(0);
        }}
        centered={true}
      >
        <ModalHeader>
          <ModalTitle as={'h5'}>{t('text_printing_qr_code')}</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="qr-cover">
            <img src={image} className="img-qr-code" />
            <div className="d-flex flex-row align-items-center justify-content-center py-2 font-size-24pt font-weight-bolder">
              <InputNumber
                className="form-control font-size-24pt text-accent font-weight-bolder mr-2"
                style={{
                  width: '7rem',
                }}
                value={printCount}
                max={totalLogisticsQuantity}
                onChange={(value) => {
                  if (!printing) {
                    setPrintCount(value);
                  }
                }}
              />{' '}
              / {totalLogisticsQuantity}
            </div>
          </div>
        </ModalBody>
        <ModalFooter>
          <button
            type="button"
            className="btn btn-link"
            onClick={handleClickQRRePrinting}
          >
            {t('text_reprinting_qr_code')}
          </button>
          <button
            type="button"
            className={classNames('btn btn-accent', {
              disabled: printing,
            })}
            onClick={handleClickConfirm}
          >
            {t('text_confirm')}
          </button>
        </ModalFooter>
      </Modal>
      <AlertModal show={showAlertModal} onHide={() => setShowAlertModal(false)}>
        <NewlineText text={alertMessage} />
      </AlertModal>
    </>
  );
}

export default QRCodePrintConfirmModal;
