import React from 'react';

type ProjectSpaceLocationOverlayProps = {
  projectName: string;
};

function ProjectSpaceLocationOverlay({
  projectName,
}: ProjectSpaceLocationOverlayProps) {
  return (
    <div className="marker-inner">
      <div className="marker-icon">
        <span className="material-icons">location_on</span>
      </div>
      <div
        className="marker-name"
        style={{
          minWidth: 'max-content',
        }}
      >
        {projectName}
      </div>
    </div>
  );
}

export default ProjectSpaceLocationOverlay;
