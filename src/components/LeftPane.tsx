import React, { useEffect, useRef } from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import ProjectPane from '@/components/ProjectPane';
import {
  MENU_IDX_ACCIDENT,
  MENU_IDX_EVENT,
  MENU_IDX_SPACE,
  MENU_IDX_PROJECT,
  MENU_IDX_SEARCH,
  MENU_IDX_CONNECTED_SPACE,
  MENU_IDX_USER,
  PANE_STATUS_SPACE_INDOOR_EDIT,
  MENU_IDX_ASSETS,
  MENU_IDX_ENTRY,
  MENU_IDX_PHYSICAL_DISTRIBUTION,
  MENU_IDX_STATISTICS,
  MENU_IDX_PROCESS_SETTINGS,
  MENU_IDX_PROCESS_MANAGEMENT,
  MENU_IDX_PROCESS_DASHBOARD,
} from '@/utils/constants/common';
import EventPane from '@/components/EventPane';
import AccidentPane from '@/components/AccidentPane';
import {
  useActiveMenu,
  useFloatPane,
  useLeftPaneContainer,
} from '@/modules/setup/hook';
import SearchPane from '@/components/SearchPane';
import SpacePane from '@/components/SpacePane';
import ConnectedSpacePane from '@/components/ConnectedSpacePane';
import UserPane from '@/components/UserPane';
import { useSpacePane } from '@/modules/space/hook';
import AssetsPane from '@/components/AssetsPane';
import EntryPane from '@/components/EntryPane';
import PhysicalDistributionPane from '@/components/PhysicalDistributionPane';
import StatisticsPane from '@/components/StatisticsPane';
import ProcessSettingsPane from '@/components/ProcessSettingsPane';
import ProcessManagementPane from '@/components/ProcessManagementPane';
import { FLOAT_PANE_PROCESS_DASHBOARD } from '@/modules/setup/types';

function LeftPane() {
  const scrollbar = useRef<PerfectScrollbar>(null);
  const { show, menuIdx, handleMenuResetActive } = useActiveMenu();
  const { setContainer } = useLeftPaneContainer();
  const { paneStatus: spacePaneStatus } = useSpacePane();
  const { handleChangeShow } = useFloatPane();

  useEffect(() => {
    const current = scrollbar.current;

    if (current) {
      setContainer(current);
    }

    return () => {
      handleMenuResetActive();
    };
  }, []);

  useEffect(() => {
    const current = scrollbar.current;

    if (current) {
      const container = (current as any)._container;
      if (show) {
        container.style.visibility = 'visible';
        container.style.transform = 'translateZ(0)';
        container.style.boxShadow =
          '0 3px 3px -2px rgba(39, 44, 51, .1), 0 3px 4px 0 rgba(39, 44, 51, .04), 0 1px 8px 0 rgba(39, 44, 51, .02)';
      } else {
        container.style.transform = null;
        container.style.boxShadow = null;
      }
    }
  }, [show]);

  // TODO: 지워야함
  useEffect(() => {
    if (menuIdx === MENU_IDX_PROCESS_DASHBOARD) {
      handleChangeShow(true, FLOAT_PANE_PROCESS_DASHBOARD);
    } else {
      handleChangeShow(false);
    }
  }, [menuIdx]);

  if (menuIdx === MENU_IDX_PROCESS_DASHBOARD) {
    return <></>;
  }

  return (
    <PerfectScrollbar
      className="sidebar sidebar-left flex sidebar-secondary"
      ref={scrollbar}
    >
      <div className="tab-content">
        {menuIdx === MENU_IDX_SEARCH && <SearchPane />}
        {menuIdx === MENU_IDX_PROJECT && <ProjectPane />}
        {menuIdx === MENU_IDX_EVENT && <EventPane />}
        {menuIdx === MENU_IDX_ACCIDENT && <AccidentPane />}
        {menuIdx === MENU_IDX_SPACE && <SpacePane />}
        {menuIdx === MENU_IDX_CONNECTED_SPACE && <ConnectedSpacePane />}
        {menuIdx === MENU_IDX_USER && <UserPane />}
        {menuIdx === MENU_IDX_ASSETS && <AssetsPane />}
        {menuIdx === MENU_IDX_ENTRY && <EntryPane />}
        {menuIdx === MENU_IDX_PHYSICAL_DISTRIBUTION && (
          <PhysicalDistributionPane />
        )}
        {menuIdx === MENU_IDX_PROCESS_MANAGEMENT && <ProcessManagementPane />}
        {menuIdx === MENU_IDX_PROCESS_SETTINGS && <ProcessSettingsPane />}
        {menuIdx === MENU_IDX_STATISTICS && <StatisticsPane />}
      </div>
    </PerfectScrollbar>
  );
}

export default LeftPane;
