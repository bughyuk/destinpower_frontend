import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import SpaceListHeader from '@/components/SpaceListHeader';
import SpaceListTable from '@/components/SpaceListTable';
import { useHomeMenu } from '@/modules/home/hook';

function SpaceList() {
  const { activeMenuIdx } = useHomeMenu();
  const [count, setCount] = useState(0);
  const [searchKeyword, setSearchKeyword] = useState('');

  useEffect(() => {
    setSearchKeyword('');
  }, [activeMenuIdx]);

  return (
    <>
      <SpaceListHeader count={count} onSubmit={setSearchKeyword} />
      <SpaceListTable onChangeCount={setCount} searchKeyword={searchKeyword} />
    </>
  );
}

function Footer() {
  const { t } = useTranslation();

  return (
    <div className="d-flex justify-content-center mt-5">
      <a className="btn-grad-col">{t('text_project_registration')}</a>
    </div>
  );
}

export default SpaceList;
