import React, {
  CSSProperties,
  ReactNode,
  useLayoutEffect,
  useState,
} from 'react';
import PerfectScrollbar from 'react-perfect-scrollbar';
import LeftPane from '@/components/LeftPane';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useActiveMenu } from '@/modules/setup/hook';
import { MENU_IDX_PROCESS_DASHBOARD } from '@/utils/constants/common';

type LeftSidebarProps = {
  customPane?: ReactNode;
  children?: ReactNode;
  style?: CSSProperties;
};

function LeftSidebar({ customPane, children, style }: LeftSidebarProps) {
  const { t } = useTranslation();
  const history = useHistory();
  const [isMini, setMini] = useState(window.innerWidth <= 1441 ? true : false);
  const { menuIdx } = useActiveMenu();

  const handleResizeWindow = () => {
    if (window.innerWidth <= 1441) {
      setMini(true);
    } else {
      setMini(false);
    }
  };

  useLayoutEffect(() => {
    window.addEventListener('resize', handleResizeWindow);

    return () => window.removeEventListener('resize', handleResizeWindow);
  }, []);

  return (
    <div
      id={'left-sidebar'}
      className="mdk-drawer js-mdk-drawer layout-mini-secondary__drawer data-closing"
      data-align={'start'}
      data-position={'left'}
      style={{
        ...style,
        width: menuIdx === MENU_IDX_PROCESS_DASHBOARD ? '0px' : '',
      }}
      {...(!isMini ? { 'data-persistent': true } : {})}
      {...(!isMini ? { 'data-opened': true } : {})}
    >
      <div
        onClick={() => {
          const leftSidebarElement = document.getElementById('left-sidebar');
          if (leftSidebarElement) {
            delete leftSidebarElement.dataset['opened'];
          }
          const scrimElement = document.getElementById('scrim');
          if (scrimElement) {
            scrimElement.classList.remove('mdk-drawer__scrim');
          }
        }}
        id={'scrim'}
      ></div>
      <div
        className="mdk-drawer__content js-sidebar-mini"
        data-responsive-width="1365px"
        data-layout="mini-secondary"
        style={
          menuIdx === MENU_IDX_PROCESS_DASHBOARD
            ? {
                width: '64px',
              }
            : {}
        }
      >
        <div className="sidebar sidebar-mini sidebar-left d-flex flex-column">
          <OverlayTrigger
            placement={'right'}
            overlay={<Tooltip id={'goHome'}>{t('msg_go_to_home')}</Tooltip>}
          >
            <a
              className="sidebar-brand p-0 navbar-height d-flex justify-content-center"
              onClick={() => history.replace('/home')}
            >
              <span className="avatar avatar-sm">
                <img
                  src="/static/images/symbol.svg"
                  className="img-fluid"
                  alt="logo"
                />
              </span>
            </a>
          </OverlayTrigger>
          <PerfectScrollbar className="flex d-flex flex-column justify-content-start">
            <ul className="nav flex-shrink-0 flex-nowrap flex-column sidebar-menu mb-0 js-sidebar-mini-tabs">
              {children}
            </ul>
          </PerfectScrollbar>
        </div>
        <LeftPane />
        {customPane}
      </div>
    </div>
  );
}

export default LeftSidebar;
