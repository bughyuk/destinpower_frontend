import React, { ReactNode, useLayoutEffect, useState } from 'react';
import classNames from 'classnames';
import { useFloatPane } from '@/modules/setup/hook';

type MainContainerProps = {
  children?: ReactNode;
};

function MainContainer({ children }: MainContainerProps) {
  const { show } = useFloatPane();
  const [isMini, setMini] = useState(window.innerWidth <= 1441 ? true : false);

  const handleResizeWindow = () => {
    if (window.innerWidth <= 1441) {
      setMini(true);
    } else {
      setMini(false);
    }
  };

  useLayoutEffect(() => {
    handleResizeWindow();
    window.addEventListener('resize', handleResizeWindow);

    return () => window.removeEventListener('resize', handleResizeWindow);
  }, []);

  return (
    <div
      style={{ height: '100vh' }}
      className={classNames('layout-mini-secondary', {
        'has-drawer-opened': !isMini,
      })}
    >
      <div
        className={classNames('mdk-drawer-layout js-mdk-drawer-layout', {
          'float-contents': false,
        })}
        data-push
        data-responsive-width="1365px"
      >
        {children}
      </div>
    </div>
  );
}

export default MainContainer;
