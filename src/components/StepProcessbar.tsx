import React from 'react';
import classNames from 'classnames';

type StepProcessBarProps = {
  step: number;
  totalStep: number;
};

function StepProcessBar({ step, totalStep }: StepProcessBarProps) {
  return (
    <div className="top-process">
      <div className={classNames('process-bar', `ps-${step}`)}></div>
      <div className="process-txt">
        <em>{`${step}/${totalStep}`}</em>
      </div>
    </div>
  );
}

export default StepProcessBar;
