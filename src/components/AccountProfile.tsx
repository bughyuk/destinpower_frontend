import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useUser } from '@/modules/user/hook';
import ProfileAvatar from '@/components/ProfileAvatar';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import { postImage } from '@/api/common';
import { Alert } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';
import { fetchUserInfoMySelf, putMyInfo } from '@/api/user';

function AccountProfile() {
  const [profileView, setProfileView] = useState<'view' | 'edit'>('view');

  return (
    <div className="contents-section account-wrap">
      <div className="cell">
        {profileView === 'view' && (
          <ProfileView onClickEdit={() => setProfileView('edit')} />
        )}
        {profileView === 'edit' && (
          <ProfileEdit onChangeView={() => setProfileView('view')} />
        )}
      </div>
    </div>
  );
}

type ProfileViewProps = {
  onClickEdit: () => void;
};

function ProfileView({ onClickEdit }: ProfileViewProps) {
  const { t } = useTranslation();
  const { user } = useUser();

  return (
    <div className="row align-items-start">
      <div className="col-md-5">
        <div className="avatar-box">
          <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-5 sort-wrap">
            <div className="flex title-row">
              <h3 className="mb-0">{t('text_my_profile')}</h3>
            </div>
          </div>
          <div className="text-center">
            <div className="avatar avatar-xxxl mb-4">
              <ProfileAvatar
                profileImgUrl={user.profileImgUrl || ''}
                userName={user.userName}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-7">
        <div className="mb-5">
          <h5 className="mb-4">{t('text_basic_information')}</h5>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_email'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.userId}
            </p>
          </FormGroup>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_name'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.userName}
            </p>
          </FormGroup>
        </div>
        <div className="mb-5">
          <h5 className="mb-4">{t('text_affiliation_information')}</h5>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_company_name'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.companyName}
            </p>
          </FormGroup>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_department'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.department}
            </p>
          </FormGroup>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_types_of_company'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.businessType}
            </p>
          </FormGroup>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_scale'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.companyScale}
            </p>
          </FormGroup>
          <FormGroup className={'border-bottom-1'}>
            <FormLabel textKey={'text_field_of_company'} />
            <p className="py-2 m-0 font-size-16pt font-weight-bold">
              {user.businessField}
            </p>
          </FormGroup>
        </div>
        <div className="my-32pt">
          <div className="d-flex align-items-center justify-content-start">
            <a className="btn btn-accent ml-0" onClick={onClickEdit}>
              {t('text_edit_profile')}
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

type ProfileEditProps = {
  onChangeView: () => void;
};

function ProfileEdit({ onChangeView }: ProfileEditProps) {
  const { t } = useTranslation();
  const { user, handleUserSet } = useUser();
  const fileInput = useRef<HTMLInputElement>(null);
  const fileReaderRef = useRef<FileReader>(new FileReader());
  const imgIdRef = useRef(user.profileImgId);
  const [success, setSuccess] = useState(false);

  const [inputs, setInputs] = useState<{
    previewUrl: string;
    name: string;
  }>({
    previewUrl: user.profileImgUrl || '',
    name: user.userName,
  });

  const [valid, setValid] = useState<{
    name: boolean;
  }>({
    name: true,
  });

  const [companyInputs, setCompanyInputs] = useState<{
    companyName: string;
    department: string;
    type: string;
    scale: string;
    field: string;
  }>({
    companyName: user.companyName,
    department: user.department,
    type: user.businessType,
    scale: user.companyScale,
    field: user.businessField,
  });

  useEffect(() => {
    const fileReader = fileReaderRef.current;
    if (fileReader) {
      fileReader.onload = (e) => {
        const target = e.target;
        if (target && target.result) {
          const result = target.result as string;
          setInputs({
            ...inputs,
            previewUrl: result,
          });
        }
      };
    }
  }, []);

  const handlePostImage = async (file: File) => {
    const imgId = await postImage(file);
    if (imgId) {
      imgIdRef.current = imgId;

      if (fileReaderRef.current) {
        fileReaderRef.current.readAsDataURL(file);
      }
    }
  };

  const validForm = () => {
    let name = true;

    if (!inputs.name) {
      name = false;
    }

    setValid({
      name,
    });

    return name;
  };

  const handleSubmit = async () => {
    setSuccess(false);
    const isValid = validForm();

    if (isValid) {
      const result = await putMyInfo({
        profileImgId: imgIdRef.current,
        userName: inputs.name,
        companyName: companyInputs.companyName,
        department: companyInputs.department,
        businessType: companyInputs.type,
        companyScale: companyInputs.scale,
        businessField: companyInputs.field,
      });

      if (result) {
        const userInfo = await fetchUserInfoMySelf();
        if (userInfo) {
          handleUserSet({
            userName: userInfo.username,
            ...userInfo,
          });
        }

        setSuccess(true);
        setTimeout(() => {
          onChangeView();
        }, 1000);
      }
    }
  };

  return (
    <div className="row align-items-start">
      <div className="col-md-5">
        <div className="avatar-box">
          <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-5 sort-wrap">
            <div className="flex title-row">
              <h3 className="mb-0">{t('text_my_profile')}</h3>
            </div>
          </div>
          <div className="text-center">
            <div className="avatar avatar-xxxl mb-4">
              <ProfileAvatar
                profileImgUrl={inputs.previewUrl}
                userName={user?.userName}
              />
            </div>
            <p className="text-50 font-size-12pt text-center">
              {t('msg_profile_image_size')}
            </p>
            <div className="button-wrapper">
              <label
                htmlFor="profileFileUpload"
                className="btn btn-outline-secondary btn-rounded m-0"
              >
                {t('text_file_upload_en')}
              </label>
              <input
                type="file"
                id="profileFileUpload"
                className="upload-box"
                accept={'image/*'}
                ref={fileInput}
                onChange={(e) => {
                  if (e.target.files && e.target.files.length) {
                    handlePostImage(e.target.files[0]);
                  } else {
                    setInputs({
                      ...inputs,
                      previewUrl: '',
                    });
                    imgIdRef.current = '';
                  }
                }}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="col-md-7">
        <div className="mb-5">
          <h5 className="mb-4">{t('text_basic_information')}</h5>
          <FormGroup>
            <FormLabel textKey={'text_email'} />
            <input
              type="text"
              className="form-line"
              placeholder={user.userId}
              disabled={true}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel textKey={'text_name'} />
            <input
              type="text"
              className="form-line"
              placeholder={t('place_holder_name')}
              value={inputs.name}
              id={'name'}
              onChange={(e) => {
                setInputs({
                  ...inputs,
                  [e.target.id]: e.target.value,
                });
              }}
              autoComplete={'off'}
            />
            {!valid.name && (
              <div className="invalid-feedback">
                {t('msg_valid_empty_name')}
              </div>
            )}
          </FormGroup>
        </div>
        <div className="mb-5">
          <h5 className="mb-4">{t('text_affiliation_information')}</h5>
          <FormGroup>
            <FormLabel textKey={'text_company_name'} />
            <input
              id={'companyName'}
              type="text"
              className="form-line"
              placeholder={t('msg_valid_enter_company_name')}
              value={companyInputs.companyName}
              onChange={(e) => {
                setCompanyInputs({
                  ...companyInputs,
                  [e.target.id]: e.target.value,
                });
              }}
              autoComplete={'off'}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel textKey={'text_department'} />
            <input
              id={'department'}
              type="text"
              className="form-line"
              placeholder={t('msg_valid_enter_department')}
              value={companyInputs.department}
              onChange={(e) => {
                setCompanyInputs({
                  ...companyInputs,
                  [e.target.id]: e.target.value,
                });
              }}
              autoComplete={'off'}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel textKey={'text_types_of_company'} />
            <input
              id={'type'}
              type="text"
              className="form-line"
              placeholder={t('msg_valid_enter_types_of_company')}
              value={companyInputs.type}
              onChange={(e) => {
                setCompanyInputs({
                  ...companyInputs,
                  [e.target.id]: e.target.value,
                });
              }}
              autoComplete={'off'}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel textKey={'text_scale'} />
            <input
              id={'scale'}
              type="text"
              className="form-line"
              placeholder={t('msg_valid_enter_scale')}
              value={companyInputs.scale}
              onChange={(e) => {
                setCompanyInputs({
                  ...companyInputs,
                  [e.target.id]: e.target.value,
                });
              }}
              autoComplete={'off'}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel textKey={'text_field_of_company'} />
            <input
              id={'field'}
              type="text"
              className="form-line"
              placeholder={t('msg_valid_enter_field_of_company')}
              value={companyInputs.field}
              onChange={(e) => {
                setCompanyInputs({
                  ...companyInputs,
                  [e.target.id]: e.target.value,
                });
              }}
              autoComplete={'off'}
            />
          </FormGroup>
        </div>
        {success && (
          <Alert className="alert-soft-success">
            <div className="d-flex flex-wrap align-items-center">
              <div className="mr-8pt">
                <MaterialIcon name={'done'} />
              </div>
              <div className="flex" style={{ minWidth: '180px' }}>
                <small className="text-black-100">
                  {t('msg_completed_info_change')}
                </small>
              </div>
            </div>
          </Alert>
        )}
        {!success && (
          <div className="my-32pt">
            <div className="d-flex align-items-center justify-content-start">
              <a className="btn btn-light mr-2" onClick={onChangeView}>
                {t('text_to_cancel')}
              </a>
              <a className="btn btn-accent" onClick={handleSubmit}>
                {t('text_to_save')}
              </a>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default AccountProfile;
