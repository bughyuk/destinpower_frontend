import React, { useState } from 'react';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalTitle,
  Nav,
  TabContainer,
  TabContent,
  TabPane,
} from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/ModalHeader';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import { ControlUserTab } from '@/modules/setup/types';
import classNames from 'classnames';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Navigation, Pagination } from 'swiper';
import { LOCAL_STORAGE_KEY_USER_MENU_GUIDE_NOT_VIEW } from '@/utils/constants/common';

SwiperCore.use([Navigation, Pagination]);

type RealtimeUserMenuGuideModalProps = {
  show: boolean;
  onChangeShow: (show: boolean) => void;
};

function RealtimeUserMenuGuideModal({
  show,
  onChangeShow,
}: RealtimeUserMenuGuideModalProps) {
  const { t } = useTranslation();
  const [isNotView, setNotView] = useState(false);

  return (
    <Modal
      show={show}
      dialogClassName={'modal-guide'}
      onExited={() => {
        if (isNotView) {
          localStorage.setItem(
            LOCAL_STORAGE_KEY_USER_MENU_GUIDE_NOT_VIEW,
            'NOT_VIEW'
          );
        }
      }}
      onHide={() => {
        //
      }}
    >
      <ModalHeader>
        <ModalTitle as={'h5'}>{t('text_user_menu_guide')}</ModalTitle>
        <button
          type="button"
          className="close"
          onClick={() => onChangeShow(false)}
        >
          <span>
            <MaterialIcon name={'clear'} />
          </span>
        </button>
      </ModalHeader>
      <ModalBody>
        <Body />
      </ModalBody>
      {!localStorage.getItem(LOCAL_STORAGE_KEY_USER_MENU_GUIDE_NOT_VIEW) && (
        <ModalFooter>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="not-seeing"
              checked={isNotView}
              onChange={(e) => setNotView(e.target.checked)}
            />
            <label htmlFor="not-seeing" className="custom-control-label">
              {t('text_not_view_any_more')}
            </label>
          </div>
        </ModalFooter>
      )}
    </Modal>
  );
}

function Body() {
  const { t } = useTranslation();
  const [activeKey, setActiveKey] = useState<ControlUserTab>('info');

  return (
    <div className="position-relative">
      <div className="guide-cover">
        <TabContainer defaultActiveKey={'info'}>
          <div className="col-left">
            <Nav
              className="flex-column"
              as={'ul'}
              onSelect={(eventKey) => {
                if (eventKey) {
                  setActiveKey(eventKey as ControlUserTab);
                }
              }}
            >
              <Nav.Item as={'li'}>
                <Nav.Link
                  className={classNames({
                    active: activeKey === 'info',
                  })}
                  eventKey={'info'}
                >
                  <span className="material-icons-round">person_search</span>
                  <div className="boxing">
                    <h5>{t('text_user_info')}</h5>
                    <p>{t('msg_user_menu_guide_info')}</p>
                  </div>
                </Nav.Link>
              </Nav.Item>
              <Nav.Item as={'li'}>
                <Nav.Link
                  className={classNames({
                    active: activeKey === 'message',
                  })}
                  eventKey={'message'}
                >
                  <span className="material-icons-round">email</span>
                  <div className="boxing">
                    <h5>{t('text_send_message')}</h5>
                    <p>{t('msg_user_menu_guide_message')}</p>
                  </div>
                </Nav.Link>
              </Nav.Item>
              <Nav.Item as={'li'}>
                <Nav.Link
                  className={classNames({
                    active: activeKey === 'trace',
                  })}
                  eventKey={'trace'}
                >
                  <span className="material-icons-round">timeline</span>
                  <div className="boxing">
                    <h5>{t('text_traffic_tracking')}</h5>
                    <p>{t('msg_user_menu_guide_trace')}</p>
                  </div>
                </Nav.Link>
              </Nav.Item>
            </Nav>
          </div>
          <div className="col-right">
            <TabContent>
              <TabPane eventKey={'info'}>
                <ImageSwiper tab={'info'} />
                <ol>
                  <li
                    dangerouslySetInnerHTML={{
                      __html: t('msg_user_menu_guide_detail'),
                    }}
                  ></li>
                  <li>{t('msg_user_menu_guide_detail_info')}</li>
                </ol>
              </TabPane>
              <TabPane eventKey={'message'}>
                <ImageSwiper tab={'message'} />
                <ol>
                  <li
                    dangerouslySetInnerHTML={{
                      __html: t('msg_user_menu_guide_detail'),
                    }}
                  ></li>
                  <li>{t('msg_user_menu_guide_detail_message')}</li>
                </ol>
              </TabPane>
              <TabPane eventKey={'trace'}>
                <ImageSwiper tab={'trace'} />
                <ol>
                  <li
                    dangerouslySetInnerHTML={{
                      __html: t('msg_user_menu_guide_detail'),
                    }}
                  ></li>
                  <li>{t('msg_user_menu_guide_detail_trace')}</li>
                </ol>
              </TabPane>
            </TabContent>
          </div>
        </TabContainer>
      </div>
    </div>
  );
}

type ImageSwiperProps = {
  tab: ControlUserTab;
};

function ImageSwiper({ tab }: ImageSwiperProps) {
  return (
    <Swiper
      className="guide-container"
      spaceBetween={30}
      observer={true}
      observeParents={true}
      pagination={{
        el: '.swiper-pagination',
        type: 'fraction',
      }}
      navigation={{
        nextEl: '.guide-next',
        prevEl: '.guide-prev',
      }}
    >
      <SwiperSlide>
        <img src={`/static/images/guide_${tab}_1.jpg`} />
      </SwiperSlide>
      <SwiperSlide>
        <img src={`/static/images/guide_${tab}_2.jpg`} />
      </SwiperSlide>
      <div className="swiper-pagination"></div>
      <div className="guide-arrow guide-next"></div>
      <div className="guide-arrow guide-prev"></div>
    </Swiper>
  );
}

export default RealtimeUserMenuGuideModal;
