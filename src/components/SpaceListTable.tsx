import React, { CSSProperties, useEffect, useState } from 'react';
import Pagination from '@/components/Pagination';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useSpace, useSpacePane } from '@/modules/space/hook';
import {
  FUNCTION_DELETE,
  FUNCTION_EDIT,
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
} from '@/utils/constants/common';
import ListDropdown from '@/components/ListDropdown';
import {
  fetchSpace,
  postUpdateSpace,
  ResponseSpaceMetaInfo,
} from '@/api/space';
import { useUser } from '@/modules/user/hook';
import { CommonUtils } from '@/utils';
import EmptyList from '@/components/EmptyList';
import { useHomeMenu } from '@/modules/home/hook';
import { MENU_SPACE } from '@/modules/home/types';

type SpaceListTableProps = {
  showCheckbox?: boolean;
  showOptions?: boolean;
  showPagination?: boolean;
  showSelectionDelete?: boolean;
  recentSpace?: boolean;
  routeDetail?: boolean;
  searchKeyword?: string;
  onChangeCount?: (count: number) => void;
  initCheckList?: string[];
  onChangeCheck?: (checked: boolean, spaceId: string) => void;
  onChangeCheckAll?: (checked: boolean, spaceIds: string[]) => void;
};

function SpaceListTable({
  showCheckbox = false,
  showOptions = true,
  showPagination = false,
  showSelectionDelete = false,
  recentSpace = false,
  routeDetail = true,
  searchKeyword = '',
  onChangeCount,
  initCheckList,
  onChangeCheck,
  onChangeCheckAll,
}: SpaceListTableProps) {
  const { t } = useTranslation();
  const { activeMenuIdx } = useHomeMenu();
  const [spaceList, setSpaceList] = useState<ResponseSpaceMetaInfo[]>([]);
  const [load, setLoad] = useState(false);
  let checkListInitialState: string[] = [];
  if (initCheckList) {
    checkListInitialState = [...initCheckList];
  }
  const [checkList, setCheckList] = useState<string[]>(checkListInitialState);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const { user } = useUser();

  useEffect(() => {
    if (initCheckList) {
      setCheckList(initCheckList);
    }
  }, [initCheckList]);

  useEffect(() => {
    if (activeMenuIdx === MENU_SPACE) {
      setPage(1);
    }
  }, [activeMenuIdx, searchKeyword]);

  useEffect(() => {
    if (initCheckList) {
      setCheckList([...initCheckList]);
    } else {
      setCheckList([]);
    }
    handleFetchSpace();
  }, [activeMenuIdx, searchKeyword, page]);

  const handleFetchSpace = async () => {
    setLoad(false);
    setSpaceList([]);
    setTotalCount(0);
    onChangeCount?.call(null, 0);
    const { list, listCount } = await fetchSpace(
      recentSpace,
      user.userId,
      page,
      searchKeyword
    );
    setSpaceList(list);
    setTotalCount(listCount);
    onChangeCount?.call(null, listCount);
    setLoad(true);
  };

  const handleReloadSpace = () => {
    handleFetchSpace();
  };

  const handleCheckChange = (checked: boolean, checkedMetaId: string) => {
    onChangeCheck?.call(null, checked, checkedMetaId);
    if (checked) {
      setCheckList([...checkList, checkedMetaId]);
    } else {
      setCheckList(checkList.filter((metaId) => metaId !== checkedMetaId));
    }
  };

  const handleCheckAllChange = (checked: boolean) => {
    const idList: string[] = [];
    spaceList.forEach((space) => idList.push(space.meta_id));
    onChangeCheckAll?.call(null, checked, idList);
    if (checked) {
      setCheckList(idList);
    } else {
      setCheckList([]);
    }
  };

  const handleClickSelectionDelete = async () => {
    if (checkList.length) {
      const promise: Promise<any>[] = [];
      checkList.forEach((checkedSpaceId) => {
        promise.push(
          postUpdateSpace({
            userid: user.userId,
            del: true,
            metaid: checkedSpaceId,
          })
        );
      });
      await Promise.all(promise);
      handleReloadSpace();
    }
  };

  let allCheck = false;
  spaceList.some((space) => {
    if (!checkList.includes(space.meta_id)) {
      allCheck = false;
      return true;
    } else {
      allCheck = true;
    }
  });

  if (load && spaceList.length === 0) {
    return <EmptyList listType={'space'} />;
  }

  return (
    <>
      <table className="table mb-4 table-nowrap">
        <colgroup>
          {showCheckbox && <col width="5%" />}
          <col width="*" />
          <col width="13%" />
          <col width="13%" />
          <col width="12%" />
          {showOptions && <col width="3%" />}
        </colgroup>
        <thead>
          <TableHeader
            check={spaceList.length !== 0 && allCheck}
            onChange={handleCheckAllChange}
            showCheckbox={showCheckbox}
            showOptions={showOptions}
          />
        </thead>
        <tbody className="list">
          {spaceList.map((space) => (
            <TableItem
              key={space.meta_id}
              {...space}
              checkList={checkList}
              onChange={handleCheckChange}
              showCheckbox={showCheckbox}
              showOptions={showOptions}
              routeDetail={routeDetail}
              onReload={handleReloadSpace}
            />
          ))}
        </tbody>
      </table>
      {showSelectionDelete && (
        <div className="flex mb-4">
          <a
            className="btn btn-outline-secondary btn-rounded"
            onClick={handleClickSelectionDelete}
          >
            {t('text_selection_delete')}
          </a>
        </div>
      )}
      {showPagination && (
        <Pagination
          curPage={page}
          totalCount={totalCount}
          onPageChange={setPage}
        />
      )}
    </>
  );
}

type TableHeaderProps = {
  check: boolean;
  onChange: (check: boolean) => void;
  showCheckbox: boolean;
  showOptions: boolean;
};

function TableHeader({
  check,
  onChange,
  showCheckbox,
  showOptions,
}: TableHeaderProps) {
  const { t } = useTranslation();

  return (
    <tr>
      {showCheckbox && (
        <th>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input js-toggle-check-all"
              id="checkAll"
              checked={check}
              onChange={(e) => onChange(e.target.checked)}
            />
            <label className="custom-control-label" htmlFor="checkAll">
              <span className="text-hide">Check All</span>
            </label>
          </div>
        </th>
      )}
      <th>{t('text_space_name')}</th>
      <th>{t('text_last_update_date')}</th>
      <th>{t('text_distribution_date')}</th>
      <th>{t('text_status')}</th>
      {showOptions && <th></th>}
    </tr>
  );
}

type TableItemProps = ResponseSpaceMetaInfo & {
  checkList: string[];
  showCheckbox: boolean;
  showOptions: boolean;
  routeDetail: boolean;
  onChange: (checked: boolean, metaId: string) => void;
  onReload: () => void;
};

function TableItem({
  meta_id,
  meta_name,
  country_code,
  country_txt,
  city_txt,
  address_txt1,
  lng,
  lat,
  update_date,
  regist_date,
  thumbnail,
  checkList,
  onChange,
  showCheckbox,
  showOptions,
  routeDetail,
  onReload,
}: TableItemProps) {
  let cursorStyle: CSSProperties = {};
  if (!routeDetail) {
    cursorStyle = { cursor: 'default' };
  }

  const { t } = useTranslation();
  const history = useHistory();
  const { user } = useUser();
  const { handleChangePaneStatus } = useSpacePane();
  const { handleSetSpaceInfo } = useSpace();

  const setSpaceInfo = () => {
    handleSetSpaceInfo({
      id: meta_id,
      name: meta_name,
      countryCode: country_code,
      country: country_txt,
      city: city_txt,
      address: address_txt1,
      location: {
        lng,
        lat,
      },
    });
  };

  const handleRouteSpaceDetail = () => {
    if (routeDetail) {
      setSpaceInfo();
      handleChangePaneStatus(PANE_STATUS_DETAIL);
      history.push('/space');
    }
  };

  const handleRouteSpaceEdit = () => {
    setSpaceInfo();
    handleChangePaneStatus(PANE_STATUS_EDIT);
    history.push('/space');
  };

  const handleDeleteSpace = async () => {
    const result = await postUpdateSpace({
      userid: user.userId,
      del: true,
      metaid: meta_id,
    });

    if (result) {
      onReload();
    }
  };

  const handleSelectOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        handleRouteSpaceEdit();
        break;
      case FUNCTION_DELETE:
        handleDeleteSpace();
        break;
    }
  };

  return (
    <tr>
      {showCheckbox && (
        <td>
          <div className="custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input js-check-selected-row"
              id={`check_${meta_id}`}
              checked={checkList.includes(meta_id)}
              onChange={(e) => onChange(e.target.checked, meta_id)}
            />
            <label
              className="custom-control-label"
              htmlFor={`check_${meta_id}`}
            >
              <span className="text-hide">Check</span>
            </label>
          </div>
        </td>
      )}
      <td>
        <div className="flex d-flex align-items-center mr-16pt">
          <a
            style={cursorStyle}
            className="avatar mr-12pt"
            onClick={handleRouteSpaceDetail}
          >
            <img src={thumbnail} className="avatar-img rounded" />
          </a>
          <div className="flex list-els">
            <a
              style={cursorStyle}
              className="card-title"
              onClick={handleRouteSpaceDetail}
            >
              {meta_name}
            </a>
          </div>
        </div>
      </td>
      <td>{CommonUtils.convertDateFormat(update_date, 'YYYY.MM.DD')}</td>
      <td>{CommonUtils.convertDateFormat(regist_date, 'YYYY.MM.DD')}</td>
      <td>
        <span className="text-accent">{t('text_distribution_status')}</span>
      </td>
      {showOptions && (
        <td className="text-right">
          <ListDropdown onSelect={handleSelectOptions} />
        </td>
      )}
    </tr>
  );
}

export default SpaceListTable;
