import React, { useEffect, useRef } from 'react';
import { Image, Tile, Vector as VectorLayer } from 'ol/layer';
import { Feature, Map, View } from 'ol';
import { useOpenLayers } from '@/modules/space/hook';
import { ImageWMS, TileJSON, Vector as VectorSource } from 'ol/source';
import { Circle, Fill, Stroke, Style } from 'ol/style';
import { defaults, Draw } from 'ol/interaction';
import GeometryType from 'ol/geom/GeometryType';
import {
  calcGeoImage,
  getGeoImageLayer,
  getGeoImageSource,
  getInteractionTransform,
  getSnapGuides,
  getTooltip,
  transformFromPolygon,
} from '@/utils/ol-ext-bridge';
import { DrawGeoImage } from '@/modules/space/types';
import {
  MAP_TILE_KEY,
  OPEN_LAYERS_RESOLUTIONS,
} from '@/utils/constants/common';
import { Config } from '@/config';
import { createBox } from 'ol/interaction/Draw';

function SpaceMapContainer() {
  const { handleSetOpenLayers } = useOpenLayers();
  const container = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const brightMap = new Tile({
      source: new TileJSON({
        url: `https://api.maptiler.com/maps/streets/tiles.json?key=${MAP_TILE_KEY}`,
        tileSize: 512,
      }),
    });

    const drawSource = new VectorSource({ wrapX: false });
    const drawLayer = new VectorLayer({
      source: drawSource,
      style: new Style({
        fill: new Fill({
          color: 'rgba(255, 255, 255, 0.2)',
        }),
        stroke: new Stroke({
          color: '#ff0000',
          width: 2,
        }),
        image: new Circle({
          radius: 7,
          fill: new Fill({
            color: '#ff0000',
          }),
        }),
      }),
    });

    const geofencingSource = new ImageWMS({
      url: `${Config.map_server.geo_server_uri}/geoserver/watta/wms`,
      params: {
        VERSION: '1.1.1',
        FORMAT: 'image/png',
        EXCEPTIONS: 'application/vnd.ogc.se_inimage',
      },
    });

    const geofencingLayer = new Image({
      visible: false,
      source: geofencingSource,
    });

    const map = new Map({
      layers: [brightMap, drawLayer, geofencingLayer],
      target: container.current!,
      interactions: defaults({
        doubleClickZoom: false,
      }),
      view: new View({
        resolutions: OPEN_LAYERS_RESOLUTIONS,
        maxResolution: OPEN_LAYERS_RESOLUTIONS[0],
        center: [15557973.31564726, 4257073.412067225],
        zoom: 11,
        projection: 'EPSG:3857',
        zoomFactor: 1,
      }),
    });

    const drawPolygon = new Draw({
      source: drawSource,
      type: GeometryType.POLYGON,
    });

    const drawLine = new Draw({
      source: drawSource,
      type: GeometryType.LINE_STRING,
    });

    const drawPoint = new Draw({
      source: drawSource,
      type: GeometryType.POINT,
    });

    const drawSquare = new Draw({
      source: drawSource,
      geometryFunction: createBox(),
      type: GeometryType.CIRCLE,
    });

    const snapGuides = getSnapGuides();
    snapGuides.setDrawInteraction(drawLine);

    const tooltip = getTooltip();
    map.addControl(tooltip);

    drawPolygon.on('drawstart', (e) => {
      tooltip.setFeature(e.feature);
    });

    drawPolygon.on('drawend', (e) => {
      tooltip.setFeature(e.feature);
    });

    const geoImageLayer = getGeoImageLayer();

    const [transformLayer, transformInteraction] = getInteractionTransform();
    map.addLayer(transformLayer);

    transformInteraction.on('rotating', function (e: any) {
      transformFromPolygon(geoImageLayer, e.feature);
    });
    transformInteraction.on('translating', (e: any) => {
      transformFromPolygon(geoImageLayer, e.feature);
    });
    transformInteraction.on('scaling', function (e: any) {
      transformFromPolygon(geoImageLayer, e.feature);
    });
    transformInteraction.on(
      ['rotateend', 'translateend', 'scaleend'],
      (e: any) => {
        transformFromPolygon(geoImageLayer, e.feature);
      }
    );

    const drawGeoImageFromSquare = (
      options: DrawGeoImage,
      squareFeature: Feature
    ) => {
      map.removeLayer(geoImageLayer);
      map.addLayer(geoImageLayer);
      map.removeInteraction(transformInteraction);
      map.addInteraction(transformInteraction);
      const cx = options.imageCenter[0];
      if (cx === 0) {
        const center = map.getView().getCenter();
        if (center) {
          options.imageCenter = [center[0], center[1]];
          options.imageScale = [1, 1];
          options.imageRotate = 0;
        }
      }
      geoImageLayer.setSource(getGeoImageSource(options));
      setTimeout(() => {
        transformFromPolygon(geoImageLayer, squareFeature);
        const calcResult = calcGeoImage(geoImageLayer);
        const transformFeature = new Feature(calcResult);
        if (transformLayer.getSource().getFeatures().length === 0) {
          transformLayer.getSource().addFeature(transformFeature);
        } else {
          transformLayer.getSource().clear();
          transformLayer.getSource().addFeature(transformFeature);
        }
      }, 100);
    };

    const drawGeoImage = (options: DrawGeoImage, editing = true) => {
      map.removeLayer(geoImageLayer);
      map.addLayer(geoImageLayer);
      map.removeInteraction(transformInteraction);
      map.addInteraction(transformInteraction);
      const cx = options.imageCenter[0];
      if (cx === 0) {
        const center = map.getView().getCenter();
        if (center) {
          options.imageCenter = [center[0], center[1]];
          options.imageScale = [1, 1];
          options.imageRotate = 0;
        }
      }

      geoImageLayer.setSource(getGeoImageSource(options));

      if (editing) {
        setTimeout(() => {
          const calcResult = calcGeoImage(geoImageLayer);
          const transformFeature = new Feature(calcResult);
          if (transformLayer.getSource().getFeatures().length === 0) {
            transformLayer.getSource().addFeature(transformFeature);
          } else {
            transformLayer.getSource().clear();
            transformLayer.getSource().addFeature(transformFeature);
          }

          const geometry = transformFeature.getGeometry();
          if (geometry) {
            const extent = geometry.getExtent();
            map.getView().fit(extent, {
              size: map.getSize(),
            });
            let zoom = map.getView().getZoom();
            if (typeof zoom === 'number') {
              zoom -= 1;
              zoom > 19 && (zoom = 19), map.getView().setZoom(zoom);
            }
          }
        }, 100);
      }
    };

    const removeGeoImage = () => {
      map.removeInteraction(transformInteraction);
      transformLayer.getSource().clear();
      map.removeLayer(geoImageLayer);
    };

    handleSetOpenLayers({
      map,
      drawSource,
      drawPolygon,
      drawLine,
      drawPoint,
      drawSquare,
      geoImageLayer,
      drawGeoImage,
      drawGeoImageFromSquare,
      removeGeoImage,
      snapGuides,
      geofencingLayer,
    });
  }, []);

  return (
    <>
      <div
        ref={container}
        className="map-container"
        onContextMenu={(e) => {
          e.preventDefault();
        }}
      />
    </>
  );
}

export default SpaceMapContainer;
