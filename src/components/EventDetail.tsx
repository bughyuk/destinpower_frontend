import React, { useEffect, useState } from 'react';
import {
  GEOMETRY_TYPE_CIRCLE,
  GEOMETRY_TYPE_POLYGON,
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PaneStatus,
} from '@/utils/constants/common';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import FormLabel from '@/components/FormLabel';
import ToggleButton from '@/components/ToggleButton';
import { fetchEvent, putEventActive } from '@/api/event';
import { ControlEvent } from '@/modules/event/types';
import moment from 'moment';
import { CommonUtils } from '@/utils';
import {
  useControlEvent,
  useOpenLayers,
  useRealtimeUser,
} from '@/modules/map/hook';
import { Circle, Polygon } from 'ol/geom';
import { Feature } from 'ol';
import { WKT } from 'ol/format';
import GeometryType from 'ol/geom/GeometryType';
import * as turf from '@turf/turf';
import { showSuccessMessage } from '@/utils/alertifyjs-bridge';
import { fetchImageDownload } from '@/api/common';

type EventDetailProps = {
  eventId: string;
  onStatusChange?: (status: PaneStatus) => void;
};

const wkt = new WKT();
const checkMap = new Map<string, boolean>();
function EventDetail({ eventId, onStatusChange }: EventDetailProps) {
  const { t } = useTranslation();
  const { draw } = useOpenLayers();
  const { users } = useRealtimeUser();
  const [event, setEvent] = useState<ControlEvent>({} as ControlEvent);
  const [feature, setFeature] = useState<Feature>();

  useEffect(() => {
    return () => {
      draw.source?.clear();
      checkMap.clear();
    };
  }, []);

  const handleFetchEvent = async () => {
    const data = await fetchEvent(eventId);
    if (data) {
      setEvent(data);

      if (data.geomType === GEOMETRY_TYPE_POLYGON) {
        if (data.geom) {
          const geometryFeature = wkt.readFeature(data.geom);
          draw.source?.addFeature(geometryFeature);
          setFeature(geometryFeature);
        }
      } else if (data.geomType === GEOMETRY_TYPE_CIRCLE) {
        if (data.lng && data.lat && data.radius) {
          const geometryFeature = new Feature(
            new Circle([data.lng, data.lat], data.radius)
          );
          draw.source?.addFeature(geometryFeature);
          setFeature(geometryFeature);
        }
      } else if (data.areaId) {
        if (data.areaGeom) {
          const geometryFeature = wkt.readFeature(data.areaGeom);
          draw.source?.addFeature(geometryFeature);
          setFeature(geometryFeature);
        }
      }
    }
  };

  useEffect(() => {
    handleFetchEvent();
  }, [eventId]);

  useEffect(() => {
    if (event && feature) {
      const geometry = feature.getGeometry();
      if (geometry) {
        switch (geometry.getType()) {
          case GeometryType.POLYGON:
            const polygonGeometry = geometry as Polygon;
            const polygon = turf.polygon(polygonGeometry.getCoordinates());

            users.forEach((user) => {
              const userPoint = turf.point([user.lng, user.lat]);
              if (turf.inside(userPoint, polygon)) {
                if (!checkMap.get(user.userId)) {
                  showSuccessMessage(
                    `${user.userId} ${t('msg_user_entered_event_area')}`
                  );
                  checkMap.set(user.userId, true);
                }
              }
            });

            break;
          case GeometryType.CIRCLE:
            const circleGeometry = geometry as Circle;
            const center = circleGeometry.getCenter();
            const radius = circleGeometry.getRadius();

            const accidentPoint = turf.toWgs84(turf.point(center));
            users.forEach((user) => {
              const userPoint = turf.toWgs84(turf.point([user.lng, user.lat]));
              const buffer = turf.buffer(accidentPoint, radius, {
                units: 'meters',
              });
              const contain = turf.booleanContains(buffer, userPoint);

              if (contain) {
                if (!checkMap.get(user.userId)) {
                  showSuccessMessage(
                    `${user.userId} ${t('msg_user_entered_event_area')}`
                  );
                  checkMap.set(user.userId, true);
                }
              }
            });
            break;
        }
      }
    }
  }, [users, feature]);

  if (CommonUtils.isEmptyObject(event)) {
    return <></>;
  }

  return (
    <>
      <Header onStatusChange={onStatusChange} />
      <Body {...event} />
      <Footer onStatusChange={onStatusChange} />
    </>
  );
}

type HeaderProps = {
  onStatusChange?: (status: PaneStatus) => void;
};

function Header({ onStatusChange }: HeaderProps) {
  const { t } = useTranslation();

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <a
          className="pr-2"
          onClick={() => onStatusChange?.call(null, PANE_STATUS_LIST)}
        >
          <MaterialIcon name={'arrow_back'} />
        </a>
        <div className="mr-24pt">
          <h3 className="mb-0">{t('text_event_view')}</h3>
        </div>
      </div>
    </div>
  );
}

type BodyProps = ControlEvent;

function Body({
  eventId,
  eventTitle,
  eventSubject,
  eventContent,
  startDate,
  endDate,
  activeFlag,
  areaName,
  originalFileName,
  imgId,
  fileUrl,
}: BodyProps) {
  const { handleReloadActivationEvents } = useControlEvent();

  const [activation, setActivation] = useState(activeFlag);

  const handleActivation = async (id: string, activation: boolean) => {
    const data = await putEventActive(eventId, activation);
    if (data) {
      setActivation(activation);
      handleReloadActivationEvents();
    }
  };

  return (
    <div className="container-fluid contents-view">
      <div className="form-group">
        <em className="form-title">{eventTitle}</em>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_period'} className={'mb-0'} />
        <p className="form-text">
          {moment(startDate).format('YYYY-MM-DD')} ~{' '}
          {moment(endDate).format('YYYY-MM-DD')}
        </p>
      </div>
      {areaName && (
        <div className="form-group">
          <FormLabel textKey={'text_area'} className={'mb-0'} />
          <p className="form-text">{areaName}</p>
        </div>
      )}
      <div className="form-group">
        <FormLabel textKey={'text_title'} className={'mb-0'} />
        <p className="form-text">{eventSubject}</p>
      </div>
      <div className="form-group">
        <FormLabel textKey={'text_content'} />
        <div
          className="form-text-content"
          dangerouslySetInnerHTML={{ __html: eventContent }}
        ></div>
      </div>
      {fileUrl && (
        <div className="form-group">
          <FormLabel textKey={'text_attached_file'} />
          <div className="form-row align-items-center">
            <div className="col-auto">
              <div className="avatar">
                <img src={fileUrl} className="avatar-img rounded" />
              </div>
            </div>
            <div className="col">
              <div className="font-weight-bold">{originalFileName}</div>
            </div>
            <div className="col-auto">
              <a
                className="text-muted-light"
                onClick={async () => {
                  await fetchImageDownload(imgId, originalFileName);
                }}
              >
                <MaterialIcon name={'file_download'} />
              </a>
            </div>
          </div>
        </div>
      )}
      <div className="form-group">
        <div className="set-list">
          <FormLabel
            textKey={'text_whether_of_activation'}
            className={'mb-0'}
            fontSizeApply={false}
          />
          <div className="control-cover">
            <ToggleButton
              id="activation"
              className={'ml-8pt'}
              checked={activation}
              onChange={handleActivation}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

type FooterProps = {
  onStatusChange?: (status: PaneStatus) => void;
};

function Footer(props: FooterProps) {
  const { t } = useTranslation();

  const handleEdit = () => {
    // TODO: API
    props.onStatusChange?.call(null, PANE_STATUS_EDIT);
  };

  return (
    <div className="my-32pt">
      <div className="d-flex align-items-center justify-content-center">
        <a
          className="btn btn-outline-secondary mr-8pt"
          onClick={() => handleEdit()}
        >
          {t('text_do_edit')}
        </a>
      </div>
    </div>
  );
}

export default EventDetail;
