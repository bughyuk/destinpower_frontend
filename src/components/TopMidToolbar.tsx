import {
  CONTROL_MODE_TENSE_HISTORY,
  CONTROL_MODE_TENSE_REALTIME,
  ControlModeTense,
} from '@/modules/setup/types';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useControlMode } from '@/modules/setup/hook';
import classNames from 'classnames';

function TopMidToolbar() {
  return (
    <div className="btn-mid-holder">
      <div className="btn-group btn-group-toggle">
        <ControlModeLabel controlModeTense={CONTROL_MODE_TENSE_HISTORY} />
        <ControlModeLabel controlModeTense={CONTROL_MODE_TENSE_REALTIME} />
      </div>
    </div>
  );
}

function ControlModeLabel({
  controlModeTense,
}: {
  controlModeTense: ControlModeTense;
}) {
  const { t } = useTranslation();
  const { tense, setControlModeTense } = useControlMode();

  let labelText = '';
  if (controlModeTense === CONTROL_MODE_TENSE_HISTORY) {
    labelText = t('text_history');
  } else {
    labelText = t('text_realtime');
  }

  useEffect(() => {
    return () => {
      setControlModeTense(CONTROL_MODE_TENSE_REALTIME);
    };
  }, []);

  return (
    <label
      onClick={() => setControlModeTense(controlModeTense)}
      className={classNames('btn btn-white', {
        active: tense === controlModeTense,
      })}
    >
      <input type="radio" /> {labelText}
    </label>
  );
}

export default TopMidToolbar;
