import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import React from 'react';

type VariationArrowProps = { value: number; unit?: string };

function VariationArrow({ value, unit }: VariationArrowProps) {
  const { t } = useTranslation();

  let text = '';

  if (value > 0) {
    text = t('text_variation_increase');
  } else if (value < 0) {
    text = t('text_variation_decrease');
  }

  if (!value) {
    return <></>;
  }

  return (
    <span
      className={classNames('state', {
        'text-accent up': value > 0,
        'text-primary down': value < 0,
      })}
    >
      {`${Math.abs(value)}${unit} ${text}`}
    </span>
  );
}

export default VariationArrow;
