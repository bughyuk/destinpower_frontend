import React, { useEffect, useState } from 'react';
import ListHeader from '@/components/ListHeader';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST, PaneStatus } from '@/utils/constants/common';
import { fetchAssetsSummary } from '@/api/assets';
import { useControlSpace } from '@/modules/map/hook';
import {
  ASSETS_DIV_SENSOR,
  ASSETS_DIV_TOTAL,
  AssetsDiv,
  AssetsSummary,
} from '@/modules/assets/types';
import EmptyPane from '@/components/EmptyPane';

type AssetsStatusProps = {
  onChangeStatus: (status: PaneStatus) => void;
  onChangeAssetsDiv: (assetsDiv: AssetsDiv) => void;
};

function AssetsStatus({
  onChangeStatus,
  onChangeAssetsDiv,
}: AssetsStatusProps) {
  const { t } = useTranslation();
  const { space } = useControlSpace();
  const [load, setLoad] = useState(false);
  const [assetsSummary, setAssetsSummary] = useState<AssetsSummary>({
    asset_cnt: '0',
    sensor_cnt: '0',
  });

  useEffect(() => {
    handleFetchAssetsSummary();
  }, [space]);

  const handleFetchAssetsSummary = async () => {
    const data = await fetchAssetsSummary({
      metaid: space.spaceMetaId,
      mapid: space.floorsMapId,
    });
    setAssetsSummary(data);
    setLoad(true);
  };

  const handleRouteAssetsList = (assetsDiv: AssetsDiv) => {
    onChangeAssetsDiv(assetsDiv);
    onChangeStatus(PANE_STATUS_LIST);
  };

  if (!space.floorsMapId) {
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
  }

  if (!load) {
    return <></>;
  }

  return (
    <>
      <ListHeader
        titleKey={'text_assets_management'}
        descriptionKey={'msg_assets_management'}
      />
      <div className="container-fluid">
        {/*<em className="none-list mb-4">{t('msg_assets_empty')}</em>*/}
        <ul className="info-panel info-link">
          <li>
            <a onClick={() => handleRouteAssetsList(ASSETS_DIV_TOTAL)}>
              <p>{t('text_total_assets')}</p>
              <div className="cell">
                <span className="material-icons">category</span>
                <em>
                  <strong>
                    {Number(assetsSummary.asset_cnt) +
                      Number(assetsSummary.sensor_cnt)}
                  </strong>
                  {t('text_count')}
                </em>
              </div>
            </a>
          </li>
          <li>
            <a onClick={() => handleRouteAssetsList(ASSETS_DIV_SENSOR)}>
              <p>{t('text_sensor')}</p>
              <div className="cell">
                <span className="material-icons">sensors</span>
                <em>
                  <strong>{Number(assetsSummary.sensor_cnt)}</strong>
                  {t('text_count')}
                </em>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </>
  );
}

export default AssetsStatus;
