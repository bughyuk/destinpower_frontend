import React, { ReactElement, useLayoutEffect, useState } from 'react';
import FilterArea from '@/components/FilterArea';
import MaterialIcon from '@/components/MaterialIcon';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import ProcessFilterNavbar from '@/components/ProcessFilterNavbar';

function FilterNavbar() {
  const { t } = useTranslation();
  const [filterMenuOpen, setFilterMenuOpen] = useState(false);

  const filterTime = <ProcessFilterNavbar />;

  const handleClickFilterTrigger = (
    e: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    e.stopPropagation();
    setFilterMenuOpen(!filterMenuOpen);
  };

  const handleResizeWindow = () => {
    if (window.innerWidth > 1441) {
      setFilterMenuOpen(false);
    }
  };

  useLayoutEffect(() => {
    window.addEventListener('resize', handleResizeWindow);

    return () => window.removeEventListener('resize', handleResizeWindow);
  }, []);

  return (
    <>
      <div id="filter-bar">
        <a
          className={classNames('cd-filter-trigger', {
            'menu-is-open': filterMenuOpen,
          })}
          onClick={handleClickFilterTrigger}
        >
          <MaterialIcon name={'tune'} />
          {t('text_filter')}
        </a>
        <div
          className={classNames(
            'status-map align-items-center justify-content-center',
            {
              'is-visible': filterMenuOpen,
            }
          )}
        >
          {filterTime}
        </div>
      </div>
    </>
  );
}

export default FilterNavbar;
