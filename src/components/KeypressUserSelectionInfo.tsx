import React, { ReactNode } from 'react';
import { useTranslation } from 'react-i18next';

type KeypressUserSelectionInfoProps = {
  eventKey: 'selection' | 'cancel';
};

function KeypressUserSelectionInfo({
  eventKey,
}: KeypressUserSelectionInfoProps) {
  const { t } = useTranslation();
  let infoText: ReactNode;
  if (eventKey === 'selection') {
    let modifierKey = 'Ctrl';
    if (navigator.appVersion.indexOf('Mac') > -1) {
      modifierKey = 'Command';
    }
    infoText = (
      <>
        <strong>{modifierKey}</strong> + {t('text_multiple_selection_possible')}
      </>
    );
  } else if (eventKey === 'cancel') {
    infoText = (
      <>
        <strong>Shift</strong> + {t('text_multiple_cancel_possible')}
      </>
    );
  }

  return <div className="inner bg-accent">{infoText}</div>;
}

export default KeypressUserSelectionInfo;
