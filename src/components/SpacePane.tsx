import React, { useEffect } from 'react';
import {
  PANE_STATUS_SPACE_FILE_REGISTER,
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import SpaceRegister from '@/components/SpaceRegister';
import { useSpacePane, useSpaceRegister } from '@/modules/space/hook';
import SpaceDetail from '@/components/SpaceDetail';
import SpaceFileRegister from '@/components/SpaceFileRegister';
import SpaceEdit from '@/components/SpaceEdit';

function SpacePane() {
  return (
    <div className="tab-pane sm-maps active">
      <div className="maps-inner">
        <SpaceContent />
      </div>
    </div>
  );
}

function SpaceContent() {
  const { paneStatus } = useSpacePane();
  const { handleChangeShowAddressPane } = useSpaceRegister();

  useEffect(() => {
    return () => {
      if (
        paneStatus === PANE_STATUS_REGISTER ||
        paneStatus === PANE_STATUS_EDIT
      ) {
        handleChangeShowAddressPane(false);
      }
    };
  }, []);

  return (
    <>
      {paneStatus === PANE_STATUS_DETAIL && <SpaceDetail />}
      {paneStatus === PANE_STATUS_REGISTER && <SpaceRegister />}
      {paneStatus === PANE_STATUS_EDIT && <SpaceEdit />}
      {paneStatus === PANE_STATUS_SPACE_FILE_REGISTER && <SpaceFileRegister />}
    </>
  );
}

export default SpacePane;
