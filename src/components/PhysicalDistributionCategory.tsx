import React, { useEffect, useRef } from 'react';
import { PANE_STATUS_LIST, PaneStatus } from '@/utils/constants/common';
import {
  Category,
  PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_CLIENT_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY,
  PHYSICAL_DISTRIBUTION_CATEGORY_ITEM_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
  PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
} from '@/modules/physical_distribution/types';
import { useTranslation } from 'react-i18next';
import MaterialIcon from '@/components/MaterialIcon';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import { useControlSpace } from '@/modules/map/hook';
import { useRightPane } from '@/modules/setup/hook';
import { RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS } from '@/modules/setup/types';
import { UserType } from '@/modules/user/types';
import { useUser } from '@/modules/user/hook';

function PhysicalDistributionCategory() {
  const { space } = useControlSpace();
  const { user } = useUser();
  const { t } = useTranslation();
  const { handleSetCategoryIdx } = usePhysicalDistributionCategory();
  const { handleChangeShow: handleChangeRightPaneShow } = useRightPane();

  useEffect(() => {
    handleSetCategoryIdx(undefined);
    handleChangeRightPaneShow(
      true,
      RIGHT_PANE_PHYSICAL_DISTRIBUTION_STATISTICS
    );
  }, []);

  const categoryListRef = useRef<
    {
      categoryIdx: Category;
      nameKey: string;
      icon: string;
      allowableUserType: UserType[];
    }[]
  >([
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
      icon: 'notes',
      nameKey: 'text_product_list',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
      icon: 'arrow_downward',
      nameKey: 'text_warehousing',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE,
      icon: 'arrow_upward',
      nameKey: 'text_release',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT,
      icon: 'swap_vert',
      nameKey: 'text_adjustment',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT,
      icon: 'east',
      nameKey: 'text_movement',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY,
      icon: 'history',
      nameKey: 'text_history',
      allowableUserType: ['OWNER', 'NORMAL'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT,
      icon: 'house_siding',
      nameKey: 'text_warehouse_management',
      allowableUserType: ['OWNER'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_ITEM_MANAGEMENT,
      icon: 'interests',
      nameKey: 'text_item_management',
      allowableUserType: ['OWNER'],
    },
    {
      categoryIdx: PHYSICAL_DISTRIBUTION_CATEGORY_CLIENT_MANAGEMENT,
      icon: 'person_search',
      nameKey: 'text_client_management',
      allowableUserType: ['OWNER'],
    },
  ]);

  const handleClickCategory = (categoryIdx: Category) => {
    handleSetCategoryIdx(categoryIdx);
  };

  if (!space.floorsMapId) {
    /*
    return (
      <EmptyPane
        textKey={'msg_no_selection_floors'}
        descriptionKey={'msg_floors_suggest_to_selection'}
      />
    );
     */
  }

  return (
    <>
      <div className="container-fluid d-flex flex-md-row align-items-center py-4">
        <div className="flex d-flex flex-column flex-sm-row align-items-center mb-24pt mb-md-0">
          <div className="mb-24pt mb-sm-0 mr-sm-24pt">
            <h3 className="mb-0">{t('text_physical_distribution')}</h3>
            <p className="text-70 mb-0">
              {t('msg_physical_distribution_management')}
            </p>
          </div>
        </div>
      </div>
      <div className="container-fluid">
        <ul className="info-panel accident-panel info-link">
          {categoryListRef.current.map(
            ({ categoryIdx, nameKey, icon, allowableUserType }) => {
              if (allowableUserType.includes(user.userType)) {
                return (
                  <li key={categoryIdx}>
                    <a onClick={() => handleClickCategory(categoryIdx)}>
                      <p>{t(nameKey)}</p>
                      <div className="cell">
                        <em>
                          <MaterialIcon name={icon} />
                        </em>
                      </div>
                    </a>
                  </li>
                );
              }
            }
          )}
        </ul>
      </div>
    </>
  );
}

export default PhysicalDistributionCategory;
