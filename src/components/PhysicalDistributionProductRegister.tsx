import React, { useEffect, useRef, useState } from 'react';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import { useTranslation } from 'react-i18next';
import {
  usePhysicalDistributionItem,
  usePhysicalDistributionProduct,
  usePhysicalDistributionProductCode,
} from '@/modules/physical_distribution/hook';
import { postImage } from '@/api/common';
import MaterialIcon from '@/components/MaterialIcon';
import InvalidAlert from '@/components/InvalidAlert';
import {
  PANE_STATUS_LIST,
  PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import InputNumber from '@/components/InputNumber';

import { postProduct } from '@/api/physical_distribution';
import classNames from 'classnames';
import { useControlProject } from '@/modules/map/hook';
import PhysicalDistributionItemRegister from '@/components/PhysicalDistributionItemRegister';

function PhysicalDistributionProductRegister() {
  const { t } = useTranslation();
  const fileInput = useRef<HTMLInputElement>(null);
  const { project } = useControlProject();
  const {
    paneStatus,
    handleChangePaneStatus,
  } = usePhysicalDistributionProduct();
  const {
    load: loadSizeCode,
    productCodeSizeList,
  } = usePhysicalDistributionProductCode();
  const {
    load: loadItemList,
    itemList,
    handleReloadItemList,
  } = usePhysicalDistributionItem();
  const [inputs, setInputs] = useState<{
    productCode: string;
    productTitle: string;
    size: string;
    weight: number;
    eachNumberOfProduct: number;
    itemId: string;
    minimumQuantity: number;
    imgId: string;
  }>({
    productCode: '',
    productTitle: '',
    size: '1',
    weight: 0,
    eachNumberOfProduct: 0,
    itemId: '',
    minimumQuantity: 0,
    imgId: '',
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (itemList.length) {
      const firstItem = itemList[0];

      setInputs({
        ...inputs,
        productCode: firstItem.productCodePrefix,
        itemId: firstItem.productCategoryId,
      });
    }
  }, [itemList]);

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handlePostImage = async (file: File) => {
    const imgId = await postImage(file);

    setInputs({
      ...inputs,
      imgId,
    });
  };

  const handleCancelImage = () => {
    const current = fileInput.current;
    if (current) {
      current.value = '';
    }

    setInputs({
      ...inputs,
      imgId: '',
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid = inputs.productTitle && inputs.itemId;

    if (isValid) {
      setLoading(true);
      const result = await postProduct(project.id, {
        productCode: inputs.productCode,
        productName: inputs.productTitle,
        productCategoryId: inputs.itemId,
        productSize: inputs.size,
        productWeight: inputs.weight,
        boxQuantity: inputs.eachNumberOfProduct,
        minQuantity: inputs.minimumQuantity,
        imgId: inputs.imgId,
      });

      if (result) {
        handleChangePaneStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  const handleChangePaneStatusProductRegister = () => {
    handleChangePaneStatus(PANE_STATUS_REGISTER);
  };

  if (!loadItemList || !loadSizeCode) {
    return <></>;
  }

  if (
    paneStatus ===
    PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER
  ) {
    return (
      <PhysicalDistributionItemRegister
        onFinish={() => {
          handleReloadItemList();
          handleChangePaneStatusProductRegister();
        }}
        onClickCancel={handleChangePaneStatusProductRegister}
      />
    );
  }

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'text_item'} essential={true} />
        {itemList.length === 0 && (
          <a
            className="btn-add-cat"
            onClick={() =>
              handleChangePaneStatus(
                PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER
              )
            }
          >
            <span className="material-icons-outlined font-size-24pt mr-2">
              add
            </span>
            {t('text_add_item')}
          </a>
        )}
        {itemList.length > 0 && (
          <select
            className="form-line"
            id="itemId"
            onChange={(e) => {
              const value = e.target.value;
              const item = itemList.find(
                (item) => item.productCategoryId === value
              );
              if (item) {
                setInputs({
                  ...inputs,
                  productCode: item.productCodePrefix,
                  [e.target.id]: item.productCategoryId,
                });
              }
            }}
          >
            {itemList.map((item) => (
              <option
                key={item.productCategoryId}
                value={item.productCategoryId}
              >
                {item.productCategoryName}
              </option>
            ))}
          </select>
        )}
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_product_code'} className={'mb-0'} />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_product_code')}
          autoComplete={'off'}
          id="productCode"
          value={inputs.productCode}
          onChange={handleChangeInputsValue}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_product_title'}
          className={'mb-0'}
          essential={true}
        />
        <input
          type="text"
          className="form-line"
          placeholder={t('place_holder_product_title')}
          id="productTitle"
          value={inputs.productTitle}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_size'} className={'mb-0'} essential={true} />
        <select
          className="form-line"
          id="size"
          onChange={(e) => {
            setInputs({
              ...inputs,
              [e.target.id]: e.target.value,
            });
          }}
        >
          {productCodeSizeList.map(({ code, text }) => (
            <option key={code} value={code}>
              {text}
            </option>
          ))}
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_weight_unit_gram'}
          className={'mb-0'}
          essential={true}
        />
        <InputNumber
          className="form-line"
          value={inputs.weight}
          onChange={(value) => {
            setInputs({
              ...inputs,
              weight: value,
            });
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_each_number_of_product'}
          className={'mb-0'}
          essential={true}
        />
        <InputNumber
          className="form-line"
          value={inputs.eachNumberOfProduct}
          onChange={(value) => {
            setInputs({
              ...inputs,
              eachNumberOfProduct: value,
            });
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel
          textKey={'text_stock_minimum_quantity'}
          className={'mb-0'}
          essential={true}
        />
        <InputNumber
          className="form-line"
          value={inputs.minimumQuantity}
          onChange={(value) => {
            setInputs({
              ...inputs,
              minimumQuantity: value,
            });
          }}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'text_product_image'} />
        <small className="text-50 ml-1">
          {t('text_product_image_recommended_size')}
        </small>
        <input
          className="form-control"
          type="file"
          accept={'image/*'}
          onChange={(e) => {
            if (e.target.files && e.target.files.length) {
              handlePostImage(e.target.files[0]);
            } else {
              setInputs({
                ...inputs,
                imgId: '',
              });
            }
          }}
          ref={fileInput}
        />
        {inputs.imgId && (
          <a className="file-del" onClick={handleCancelImage}>
            <MaterialIcon name={'close'} />
          </a>
        )}
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => handleChangePaneStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_do_add')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default PhysicalDistributionProductRegister;
