import React, { ReactNode } from 'react';
import classNames from 'classnames';

type FormRowProps = {
  children?: ReactNode;
  className?: string;
};

function FormRow({ className, children }: FormRowProps) {
  return (
    <div className={classNames('form-row mb-32pt', className)}>{children}</div>
  );
}

export default FormRow;
