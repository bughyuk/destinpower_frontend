import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { getAuthNumber, postAuthNumberMail } from '@/api/user';

type SignUpNumberAuthenticationProps = {
  signUpEmail: string;
  onMatchNumber: () => void;
};

function SignUpNumberAuthentication({
  signUpEmail,
  onMatchNumber,
}: SignUpNumberAuthenticationProps) {
  const { i18n, t } = useTranslation();

  const [feedbackMessage, setFeedbackMessage] = useState('');
  const [authNumber, setAuthNumber] = useState('');
  const [isCheck, setCheck] = useState(false);

  useEffect(() => {
    setCheck(false);
  }, [authNumber]);

  const handleReSendMail = async () => {
    const result = await postAuthNumberMail(
      signUpEmail,
      i18n.language.length > 2 ? i18n.language.slice(0, 2) : i18n.language
    );
    if (result) {
      setCheck(false);
    }
  };

  const handleClickCheck = async () => {
    if (!authNumber) {
      setFeedbackMessage(t('msg_valid_enter_authentication_number'));
      return;
    }

    // TODO: API
    const result = await getAuthNumber(signUpEmail, authNumber);

    if (result) {
      setFeedbackMessage(t('msg_match_authentication_number'));
      setCheck(true);
    } else {
      setFeedbackMessage(t('msg_not_match_authentication_number'));
    }
  };

  const handleClickNext = () => {
    if (!authNumber) {
      setFeedbackMessage(t('msg_valid_enter_authentication_number'));
      return;
    }
    if (!isCheck) {
      setFeedbackMessage(t('msg_check_authentication_number'));
    } else {
      onMatchNumber();
    }
  };

  return (
    <>
      <div className="title-group mb-5">
        <h3 className="m-0">{t('text_email_authentication')}</h3>
      </div>
      <div className="form-group mb-5">
        <div className="over-txt">
          <label className="form-label text-50 essential" htmlFor="authNumber">
            {t('text_authentication_number_confirm')}
          </label>
          <input
            id="authNumber"
            type="text"
            className="form-line"
            style={{ paddingRight: '6rem' }}
            placeholder={t('place_holder_authentication_number')}
            value={authNumber}
            onChange={(e) => {
              setAuthNumber(e.target.value);
            }}
            autoComplete={'off'}
          />
          <a
            className="btn btn-outline-secondary btn-sm btn-rounded"
            onClick={handleClickCheck}
          >
            {t('text_authentication_number_confirm')}
          </a>
        </div>
        {feedbackMessage && (
          <div className="invalid-feedback">{feedbackMessage}</div>
        )}
        <div className="support-feedback">
          {t('msg_do_not_receive_mail')}{' '}
          <a className="text-body text-underline" onClick={handleReSendMail}>
            {t('text_resend')}
          </a>
        </div>
      </div>
      <div className="form-group text-center mb-32pt">
        <button
          className="btn btn-block btn-lg btn-accent"
          type="button"
          onClick={handleClickNext}
        >
          {t('text_next')}
        </button>
      </div>
    </>
  );
}

export default SignUpNumberAuthentication;
