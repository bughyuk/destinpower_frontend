import React, { useState, useEffect } from 'react';
import { PaneProps } from '@/modules/common';
import { useTranslation } from 'react-i18next';
import { PANE_STATUS_LIST } from '@/utils/constants/common';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import { ProductType } from '@/modules/physical_distribution/types';
// import { useControlProject } from '@/modules/map/hook';
import classNames from 'classnames';
// import InputNumber from '@/components/InputNumber';
import {
  dp_postProductType,
  dp_fetchProductTypeRelData,
} from '@/api/physical_distribution';
// import MaterialIcon from '@/components/MaterialIcon';

type ProcessProductTypeRegisterProps = PaneProps & {
  productType: ProductType | undefined;
};

function ProcessProductTypeRegister({
  productType,
  onChangeStatus,
}: ProcessProductTypeRegisterProps) {
  const { t } = useTranslation();
  // const { project } = useControlProject();
  const [inputs, setInputs] = useState<{
    name: string;
    pfid: string; //not support number type
    i_file: any;
  }>({
    name: productType !== undefined ? productType.name : '',
    pfid: productType !== undefined ? productType.product_family?.id : '',
    i_file: null,
  });
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [loading, setLoading] = useState(false);
  const [prdFamilyList, setPrdFamilyList] = useState<
    {
      name: string;
      id: string;
    }[]
  >([]); //제품군 데이타

  const handleChangeInputsValue = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.id]: e.target.value,
    });
  };

  const handleSubmit = async () => {
    if (loading) {
      return;
    }
    setShowInvalidMessage(false);
    const isValid = inputs.pfid && inputs.name;

    if (isValid) {
      setLoading(true);

      const result = await dp_postProductType({
        id: productType !== undefined ? productType.id : undefined, //id 값존재하면 수정처리
        name: inputs.name,
        pfid: inputs.pfid,
        i_file: inputs.i_file,
      });
      if (result) {
        onChangeStatus(PANE_STATUS_LIST);
      }

      setLoading(false);
    } else {
      setShowInvalidMessage(true);
    }
  };

  useEffect(() => {
    (async () => {
      if (prdFamilyList?.length) return; //maybe
      const respData = await dp_fetchProductTypeRelData();
      setPrdFamilyList(respData);
    })();
  }, [productType]);

  return (
    <div className="container-fluid">
      <FormGroup>
        <FormLabel textKey={'제품군'} essential={true} />
        <select
          className="form-line"
          onChange={(e) => {
            setInputs({
              ...inputs,
              pfid: e.target.value,
            });
          }}
        >
          {/* <option value="1">ESS ALL in ONE</option> */}
          {/* <option value="2">ESS PCS</option> */}
          <option value="">제품군을 선택해 주세요.</option>
          {prdFamilyList?.length &&
            prdFamilyList.map((list, i) => (
              <option
                key={i}
                value={list.id}
                selected={
                  productType !== undefined &&
                  productType.product_family?.id === list.id
                }
              >
                {list.name}
              </option>
            ))}
        </select>
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'제품타입'} className={'mb-0'} essential={true} />
        <input
          type="text"
          className="form-line"
          id="name"
          value={inputs.name}
          placeholder={t('제품타입을 입력해 주세요.')}
          onChange={handleChangeInputsValue}
          autoComplete={'off'}
        />
      </FormGroup>
      <FormGroup>
        <FormLabel textKey={'제품 이미지'} />
        <input
          className="form-control"
          type="file"
          accept={'image/*'}
          onChange={(e) => {
            setInputs({
              ...inputs,
              i_file:
                e.target.files && e.target.files.length
                  ? e.target.files[0]
                  : null,
            });
          }}
        />
      </FormGroup>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => onChangeStatus(PANE_STATUS_LIST)}
          >
            {t('text_to_cancel')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0', {
              disabled: loading,
              'is-loading': loading,
            })}
            onClick={handleSubmit}
          >
            {t('text_do_add')}
          </a>
        </div>
      </div>
    </div>
  );
}

export default ProcessProductTypeRegister;
