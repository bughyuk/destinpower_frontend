import { useTranslation } from 'react-i18next';
import React, { useEffect, useRef, useState } from 'react';
import { useDropdown } from '@/modules/common/hook';
import { Dropdown } from 'react-bootstrap';
import classNames from 'classnames';
import DropdownMenu from 'react-bootstrap/DropdownMenu';
import {
  useControlArea,
  useControlProject,
  useControlSpace,
  useOpenLayers,
} from '@/modules/map/hook';

let selectedWholeOfArea = true;
function FilterArea() {
  const { t, i18n } = useTranslation();
  const { project } = useControlProject();
  const { space: controlSpace } = useControlSpace();
  const { map, areaSource } = useOpenLayers();
  const [isActive, setActive] = useState(false);
  const [selectedArea, setSelectedArea] = useState<string>(
    t('text_selection_area')
  );
  const { areaList, handleSetSelectArea } = useControlArea();
  const dropdown = useRef<HTMLDivElement>(null);
  const { handleToggle } = useDropdown(dropdown);

  const handleAreaSelect = (eventKey: string | null) => {
    areaSource?.clear();
    if (eventKey === 'the_whole_of_area') {
      selectedWholeOfArea = true;
      setSelectedArea(t('text_the_whole_of_area'));
      handleSetSelectArea(null);

      const findSpace = project.spaceList.find(
        (space) => space.mappingId === controlSpace.spaceMappingId
      );

      if (findSpace) {
        const view = map?.getView();
        view?.setCenter([findSpace.longitude, findSpace.latitude]);
        view?.setZoom(13);
      }
    } else {
      selectedWholeOfArea = false;
      const findArea = areaList.find((area) => area.area_id === eventKey);
      if (findArea) {
        setSelectedArea(findArea.area_name);
        const feature = findArea.feature;
        handleSetSelectArea(feature);
        areaSource?.addFeature(feature);
        const extent = feature.getGeometry()?.getExtent();
        if (extent) {
          map?.getView().fit(extent, {
            size: map?.getSize(),
          });
        }
      }
    }
  };

  useEffect(() => {
    setSelectedArea(t('text_the_whole_of_area'));
    handleSetSelectArea(null);
  }, [controlSpace]);

  useEffect(() => {
    if (selectedWholeOfArea) {
      setSelectedArea(t('text_the_whole_of_area'));
    }
  }, [i18n.language]);

  return (
    <div className="filter-cell filter-area d-flex align-items-center">
      <Dropdown
        onSelect={handleAreaSelect}
        onToggle={(isOpen, event, metadata) => {
          setActive(isOpen);
          handleToggle(isOpen, event, metadata);
        }}
        className={'h-100'}
      >
        <Dropdown.Toggle
          as={'a'}
          data-caret="false"
          className={classNames('filter-dropdown-toggle filter-select', {
            active: isActive,
          })}
        >
          <strong>{selectedArea}</strong>
        </Dropdown.Toggle>
        <DropdownMenu
          className="filter-item-dropdown no-arrow near"
          style={{
            maxHeight: '700px',
            overflowY: 'auto',
          }}
          ref={dropdown}
        >
          <Dropdown.Header>
            <strong>{t('text_buildings_classifications')}</strong>
          </Dropdown.Header>
          <Dropdown.Item eventKey={'the_whole_of_area'}>
            {t('text_the_whole_of_area')}
          </Dropdown.Item>
          {areaList.length > 0 && <Dropdown.Divider />}
          {areaList.map((area) => (
            <Dropdown.Item key={area.area_id} eventKey={area.area_id}>
              {area.area_name}
            </Dropdown.Item>
          ))}
        </DropdownMenu>
      </Dropdown>
    </div>
  );
}

export default FilterArea;
