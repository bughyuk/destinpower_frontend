import React, { ReactNode, useEffect } from 'react';
import { usePhysicalDistributionCategory } from '@/modules/physical_distribution/hook';
import {
  PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY,
  PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
  PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT,
  PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING,
} from '@/modules/physical_distribution/types';
import FloatPhysicalDistributionProduct from '@/components/FloatPhysicalDistributionProduct';
import FloatPhysicalDistributionWarehousing from '@/components/FloatPhysicalDistributionWarehousing';
import FloatPhysicalDistributionRelease from '@/components/FloatPhysicalDistributionRelease';
import FloatPhysicalDistributionAdjustment from '@/components/FloatPhysicalDistributionAdjustment';
import FloatPhysicalDistributionMovement from '@/components/FloatPhysicalDistributionMovement';
import { useFloatPane } from '@/modules/setup/hook';
import FloatPhysicalDistributionWarehouse from '@/components/FloatPhysicalDistributionWarehouse';
import FloatPhysicalDistributionHistory from '@/components/FloatPhysicalDistributionHistory';

function FloatPhysicalDistributionPane() {
  const { categoryIdx } = usePhysicalDistributionCategory();
  const { handleContainerUpdateScroll } = useFloatPane();

  let content: ReactNode = (
    <div
      className="water-mark-wrap"
      style={{
        position: 'fixed',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
      }}
    >
      <div className="grayscale">
        <img src="/static/images/symbol_col.svg" />
        <h2>WATA Inc.</h2>
      </div>
    </div>
  );
  switch (categoryIdx) {
    case PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT:
      content = <FloatPhysicalDistributionProduct />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSING:
      content = <FloatPhysicalDistributionWarehousing />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_RELEASE:
      content = <FloatPhysicalDistributionRelease />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_ADJUSTMENT:
      content = <FloatPhysicalDistributionAdjustment />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_MOVEMENT:
      content = <FloatPhysicalDistributionMovement />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_HISTORY:
      content = <FloatPhysicalDistributionHistory />;
      break;
    case PHYSICAL_DISTRIBUTION_CATEGORY_WAREHOUSE_MANAGEMENT:
      content = <FloatPhysicalDistributionWarehouse />;
      break;
  }

  useEffect(() => {
    handleContainerUpdateScroll();
  }, [categoryIdx]);

  return <>{content}</>;
}

export default FloatPhysicalDistributionPane;
