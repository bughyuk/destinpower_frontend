import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';
import {
  usePhysicalDistributionCategory,
  usePhysicalDistributionProduct,
  usePhysicalDistributionWarehouse,
} from '@/modules/physical_distribution/hook';
import {
  PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT,
  Product,
  ReleaseProduct,
} from '@/modules/physical_distribution/types';
import PhysicalDistributionListSearch from '@/components/PhysicalDistributionListSearch';
import { fetchReleaseProducts } from '@/api/physical_distribution';
import { useControlProject } from '@/modules/map/hook';
import Preloader from '@/components/Preloader';
import { CommonUtils } from '@/utils';
import PhysicalDistributionListSelectBox from '@/components/PhysicalDistributionListSelectBox';

function PhysicalDistributionReleaseProductList() {
  const { t } = useTranslation();
  const {
    load: loadWarehouseList,
    warehouseList,
  } = usePhysicalDistributionWarehouse();
  const [searchKeyword, setSearchKeyword] = useState('');
  const [warehouseId, setWarehouseId] = useState('');
  const { project } = useControlProject();
  const { categoryIdx } = usePhysicalDistributionCategory();
  const {
    selectedProduct,
    reloadFlag,
    handleSetProduct,
  } = usePhysicalDistributionProduct();
  const [load, setLoad] = useState(false);
  const [productList, setProductList] = useState<ReleaseProduct[]>([]);
  const [filterList, setFilterList] = useState<ReleaseProduct[]>([]);

  useEffect(() => {
    return () => {
      handleSetProduct(null);
    };
  }, []);

  useEffect(() => {
    if (loadWarehouseList && warehouseList.length) {
      const firstWarehouse = warehouseList[0];
      setWarehouseId(firstWarehouse.warehouseId);
    }
  }, [loadWarehouseList, warehouseList]);

  useEffect(() => {
    handleSetProduct(null);
    setFilterList(
      productList.filter(
        (product) =>
          product.productName.toLowerCase().indexOf(searchKeyword) > -1
      )
    );
  }, [searchKeyword, productList]);

  useEffect(() => {
    handleFetchReleaseProductList();
  }, [reloadFlag, warehouseId]);

  const handleFetchReleaseProductList = async () => {
    setLoad(false);
    if (warehouseId) {
      const data = await fetchReleaseProducts(project.id, warehouseId);
      setProductList(data);
    }
    setLoad(true);
  };

  const handleClickProduct = (clickProduct: Product) => {
    let product: Product | null = null;
    if (selectedProduct?.productId !== clickProduct.productId) {
      product = clickProduct;
    }
    handleSetProduct(product);
  };

  if (!loadWarehouseList) {
    return <></>;
  }

  return (
    <>
      <div className="list-opt-box">
        <PhysicalDistributionListSearch
          placeholderTextKey={'place_holder_search_product'}
          onSubmit={setSearchKeyword}
        />
        <PhysicalDistributionListSelectBox
          options={warehouseList.map((warehouse) => {
            return {
              value: warehouse.warehouseId,
              text: warehouse.warehouseName,
            };
          })}
          onChange={setWarehouseId}
          emptyTextKey={'text_warehouse_empty'}
        />
      </div>
      {!load && <Preloader />}
      {load && filterList.length === 0 && (
        <em className="none-list mb-4">{t('msg_product_empty')}</em>
      )}
      {load && filterList.length > 0 && (
        <div className="list-group row-list list-group-flush mb-4">
          {filterList.map((product) => (
            <ProductItem
              key={`${product.boxCategoryId}/${product.warehouseId}`}
              {...product}
              activeId={
                categoryIdx === PHYSICAL_DISTRIBUTION_CATEGORY_PRODUCT
                  ? selectedProduct?.productId || ''
                  : ''
              }
              onClick={() => handleClickProduct(product)}
            />
          ))}
        </div>
      )}
    </>
  );
}

type ProductItemProps = ReleaseProduct & {
  activeId: string;
  onClick: () => void;
};

function ProductItem({
  productId,
  productName,
  boxQuantity,
  usableQuantity,
  productWeight,
  productSizeName,
  warehouseName,
  imgUrl,
  activeId,
  onClick,
}: ProductItemProps) {
  return (
    <div
      className={classNames('list-group-item d-flex', {
        active: productId === activeId,
      })}
      onClick={() => onClick()}
    >
      <a className="flex d-flex align-items-center">
        <div className="avatar mr-12pt blank-img">
          <img src={imgUrl} className="avatar-img rounded" />
        </div>
        <div className="flex list-els">
          <h4 className="card-title">{productName}</h4>
          <div className="card-subtitle text-50 d-flex align-items-center">
            <div className="card-subtitle text-50">
              <small className="mr-2">{productSizeName}</small>
              <small className="mr-2">
                {CommonUtils.getProductStandard(productWeight, boxQuantity)}
              </small>
              <small className="mr-2">{warehouseName}</small>
            </div>
          </div>
        </div>
        <div className={'pd-count'}>{usableQuantity}</div>
      </a>
    </div>
  );
}

export default PhysicalDistributionReleaseProductList;
