import React, { useEffect, useState } from 'react';
import {
  PANE_STATUS_DETAIL,
  PANE_STATUS_EDIT,
  PANE_STATUS_LIST,
  PANE_STATUS_REGISTER,
  PANE_STATUS_STATUS_BOARD,
  PaneStatus,
} from '@/utils/constants/common';
import { useLeftPaneContainer } from '@/modules/setup/hook';
import AssetsList from '@/components/AssetsList';
import AssetsRegister from '@/components/AssetsRegister';
import AssetsStatus from '@/components/AssetsStatus';
import AssetsDetail from '@/components/AssetsDetail';
import AssetsEdit from '@/components/AssetsEdit';
import { Assets, ASSETS_DIV_TOTAL, AssetsDiv } from '@/modules/assets/types';
import { useControlSpace } from '@/modules/map/hook';

function AssetsPane() {
  return (
    <div className="tab-pane sm-assets active">
      <div className="assets-inner">
        <AssetsContent />
      </div>
    </div>
  );
}

function AssetsContent() {
  const { space } = useControlSpace();
  const [status, setStatus] = useState<PaneStatus>(PANE_STATUS_STATUS_BOARD);
  const [assets, setAssets] = useState<Assets>({} as Assets);
  const [assetsDiv, setAssetsDiv] = useState<AssetsDiv>(ASSETS_DIV_TOTAL);
  const { updateScroll } = useLeftPaneContainer();
  useEffect(() => {
    updateScroll();
  }, [status]);

  useEffect(() => {
    setStatus(PANE_STATUS_STATUS_BOARD);
  }, [space]);

  return (
    <>
      {status === PANE_STATUS_STATUS_BOARD && (
        <AssetsStatus
          onChangeStatus={setStatus}
          onChangeAssetsDiv={setAssetsDiv}
        />
      )}
      {status === PANE_STATUS_LIST && (
        <AssetsList
          assetsDiv={assetsDiv}
          onChangeStatus={setStatus}
          onChangeAssets={setAssets}
        />
      )}
      {status === PANE_STATUS_REGISTER && (
        <AssetsRegister onChangeStatus={setStatus} />
      )}
      {status === PANE_STATUS_DETAIL && (
        <AssetsDetail assets={assets} onChangeStatus={setStatus} />
      )}
      {status === PANE_STATUS_EDIT && (
        <AssetsEdit assets={assets} onChangeStatus={setStatus} />
      )}
    </>
  );
}

export default AssetsPane;
