import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import classNames from 'classnames';

function HomeHelp() {
  const { t } = useTranslation();
  const [isShow, setShow] = useState(false);

  const handleShowHelp = () => {
    setShow(true);
  };

  return (
    <>
      <div className="cd-nugget-info">
        <a className="cd-btn" onClick={handleShowHelp}>
          {t('text_help_en')}
        </a>
      </div>
      {isShow && <HomeHelpContainer onChangeShow={setShow} />}
    </>
  );
}

type HomeHelpStep = 1 | 2 | 3 | 4;

type HomeHelpContainerProps = {
  onChangeShow: (show: boolean) => void;
};

function HomeHelpContainer({ onChangeShow }: HomeHelpContainerProps) {
  const { t } = useTranslation();
  const layerElement = useRef<HTMLDivElement>(null);
  const [step, setStep] = useState<HomeHelpStep>(1);

  useEffect(() => {
    setTimeout(() => {
      const current = layerElement.current;
      if (current) {
        current.classList.add('is-visible');
      }
    });
  }, []);

  const handleClose = () => {
    onChangeShow(false);
  };

  return (
    <>
      <div className="cd-tour-wrapper tutorial-active">
        {step === 1 && (
          <div className="cd-single-step step01 is-selected">
            <span></span>
            <div className="cd-more-info bottom">
              <h5>{t('text_project_en')}</h5>
              <div
                dangerouslySetInnerHTML={{ __html: t('msg_help_project') }}
              ></div>
              <HomeHelpNav
                step={step}
                onChangeStep={setStep}
                onClose={handleClose}
              />
            </div>
          </div>
        )}
        {step === 2 && (
          <div className="cd-single-step step02 is-selected">
            <span></span>
            <div className="cd-more-info bottom">
              <h5>{t('text_space_en')}</h5>
              <div
                dangerouslySetInnerHTML={{ __html: t('msg_help_space') }}
              ></div>
              <HomeHelpNav
                step={step}
                onChangeStep={setStep}
                onClose={handleClose}
              />
            </div>
          </div>
        )}
        {step === 3 && (
          <div className="cd-single-step step03 is-selected">
            <span></span>
            <div className="cd-more-info right">
              <h5>{t('text_list_of_recent_projects')}</h5>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('msg_help_list_of_recent_projects'),
                }}
              ></p>
              <HomeHelpNav
                step={step}
                onChangeStep={setStep}
                onClose={handleClose}
              />
            </div>
          </div>
        )}
        {step === 4 && (
          <div className="cd-single-step step04 is-selected">
            <span></span>
            <div className="cd-more-info right">
              <h5>{t('text_space_list')}</h5>
              <p
                dangerouslySetInnerHTML={{
                  __html: t('msg_help_list_of_space'),
                }}
              ></p>
              <HomeHelpNav
                step={step}
                onChangeStep={setStep}
                onClose={handleClose}
              />
            </div>
          </div>
        )}
      </div>
      <div className="cd-cover-layer" ref={layerElement}></div>
    </>
  );
}

type HomeHelpNavProps = {
  step: HomeHelpStep;
  onChangeStep: (step: HomeHelpStep) => void;
  onClose: () => void;
};

function HomeHelpNav({ step, onChangeStep, onClose }: HomeHelpNavProps) {
  const totalStep = 4;
  return (
    <>
      <div className="cd-nav">
        <span>
          <b className="cd-actual-step">{step}</b> of {totalStep}
        </span>
        <ul className="cd-tour-nav">
          <li>
            <a
              className={classNames('cd-prev', {
                inactive: step === 1,
              })}
              onClick={() => {
                if (step !== 1) {
                  onChangeStep((step - 1) as HomeHelpStep);
                }
              }}
            >
              &#171; Previous
            </a>
          </li>
          <li>
            <a
              className={classNames('cd-next', {
                inactive: step === totalStep,
              })}
              onClick={() => {
                if (step !== totalStep) {
                  onChangeStep((step + 1) as HomeHelpStep);
                }
              }}
            >
              Next &#187;
            </a>
          </li>
        </ul>
      </div>
      <a className="cd-close" onClick={onClose}>
        Close
      </a>
    </>
  );
}

export default HomeHelp;
