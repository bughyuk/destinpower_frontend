import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import { useSpace, useSpaceRegister } from '@/modules/space/hook';
import { useAsync } from '@/modules/common';
import { fetchJapanAddress } from '@/api/address';
import { LngLat } from '@/modules/map/types';
import classNames from 'classnames';
import { Collection, Feature } from 'ol';
import { Icon, Style } from 'ol/style';
import { Point } from 'ol/geom';
import {
  postSpaceMetaInfo,
  RequestSpaceMetaInfo,
  ResponseSpaceMetaInfo,
} from '@/api/space';
import { useUser } from '@/modules/user/hook';
import { Translate } from 'ol/interaction';
import {
  JapanLngLat,
  SPACE_COUNTRY_CODE_JP,
  SPACE_COUNTRY_CODE_KR,
  SpaceCountryCode,
} from '@/modules/space/types';
import { OpenLayersUtils } from '@/utils/OpenLayersUtils';
import { useOpenLayers, useProjectRegister } from '@/modules/project/hook';
import { putProjectStepData } from '@/api/project';
import InvalidAlert from '@/components/InvalidAlert';
import DaumPostcodeModal from '@/components/DaumPostcodeModal';
import { fetchGoogleLocation } from '@/api/common';

/*
  TODO: 프로젝트, 공간 둘다 같은 화면 인데 파일 두개 생성하여 작업
  TODO: 시간이 없어 우선 작업 진행 추후 리팩토링 필요
 */
const iconFeature = new Feature();
iconFeature.setId('address_marker');
const iconStyle = new Style({
  image: new Icon({
    src: '/static/images/ic-marker-24-px.svg',
  }),
});
iconFeature.setStyle(iconStyle);
const collection = new Collection<Feature>();
collection.push(iconFeature);
const translate = new Translate({
  features: collection,
});
function ProjectSpaceAddressRegister() {
  const { user } = useUser();
  const [spaceName, setSpaceName] = useState('');
  const [countryCode, setCountryCode] = useState<SpaceCountryCode>(
    SPACE_COUNTRY_CODE_JP
  );
  const [address, setAddress] = useState('');
  const [lngLat, setLngLat] = useState<LngLat | null>(null);
  const [isValid, setValid] = useState(false);
  const {
    selectedDo,
    selectedSubDistrict,
    selectedLngLat,
    detailedAddress,
    handleSetAddress,
  } = useSpaceRegister();
  const [fetchJapanAddressState, fetchJapanAddressApi] = useAsync(
    fetchJapanAddress
  );

  const { data: fetchJapanAddressData } = fetchJapanAddressState;
  const { map, drawSource } = useOpenLayers();
  const {
    projectInfo,
    projectProduceStep,
    handleChangeRegisterStep,
    handleSetProjectProduceStep,
  } = useProjectRegister();
  const { handleSetSpaceInfo } = useSpace();

  const [postSpaceMetaInfoState, postSpaceMetaInfoApi] = useAsync(
    postSpaceMetaInfo
  );
  const { data: postSpaceMetaInfoData } = postSpaceMetaInfoState;

  const handleFetchAddress = async () => {
    const lngLat = await fetchGoogleLocation(
      selectedDo + selectedSubDistrict + address
    );
    if (lngLat) {
      setLngLat({
        lng: lngLat.lng,
        lat: lngLat.lat,
      });

      const lngLatArray = Object.values(lngLat);

      iconFeature.setGeometry(new Point(lngLatArray));
      drawSource?.addFeature(iconFeature);
      map?.getView().setZoom(13);
      map?.getView().setCenter(lngLatArray);
    }
  };

  useEffect(() => {
    if (fetchJapanAddressData) {
      if ((fetchJapanAddressData as any)['地理座標']) {
        const featureById = drawSource?.getFeatureById('address_marker');
        if (featureById) {
          drawSource?.removeFeature(featureById);
        }

        const japanLatLng: JapanLngLat = (fetchJapanAddressData as any)[
          '地理座標'
        ];

        const latLng: LngLat = {
          lng: japanLatLng.経度,
          lat: japanLatLng.緯度,
        };

        const convertLngLat = OpenLayersUtils.convertLngLat([
          latLng.lng,
          latLng.lat,
        ]);

        setLngLat({
          lng: convertLngLat[0],
          lat: convertLngLat[1],
        });

        iconFeature.setGeometry(new Point(convertLngLat));
        drawSource?.addFeature(iconFeature);

        map?.getView().setZoom(13);
        map?.getView().setCenter(convertLngLat);
      }
    }
  }, [fetchJapanAddressData]);

  useEffect(() => {
    if (spaceName && address && lngLat) {
      setValid(true);
    } else {
      setValid(false);
    }
  }, [spaceName, address, lngLat]);

  const handleSubmit = () => {
    if (lngLat) {
      const submitData: RequestSpaceMetaInfo = {
        metaname: spaceName,
        imgId: '',
        countrycode: countryCode,
        addres1: address,
        addres2: '',
        country: selectedDo,
        city: selectedSubDistrict,
        zipcode: '',
        lng: lngLat.lng,
        lat: lngLat.lat,
        userid: user.userId,
      };

      postSpaceMetaInfoApi(submitData);
    }
  };

  useEffect(() => {
    if (postSpaceMetaInfoData) {
      const featureById = drawSource?.getFeatureById('address_marker');
      if (featureById) {
        map?.removeInteraction(translate);
        drawSource?.removeFeature(featureById);
      }

      const spaceMetaInfoDataArray = postSpaceMetaInfoData as ResponseSpaceMetaInfo[];
      if (spaceMetaInfoDataArray.length > 0) {
        const spaceMetaInfo = spaceMetaInfoDataArray[0] as ResponseSpaceMetaInfo;
        const metaId = spaceMetaInfo.meta_id;
        handleSetSpaceInfo({
          id: metaId,
          location: {
            lng: spaceMetaInfo.lng,
            lat: spaceMetaInfo.lat,
          },
        });
        handleSaveProduceStepData(metaId);
        handleChangeRegisterStep(4);
      }
    }
  }, [postSpaceMetaInfoData]);

  useEffect(() => {
    translate.on('translateend', (e) => {
      const coordinate = e.coordinate;
      setLngLat({
        lng: coordinate[0],
        lat: coordinate[1],
      });
    });
    map?.addInteraction(translate);

    return () => {
      handleSetAddress({
        selectedDo: '',
        selectedSubDistrict: '',
        selectedLngLat: null,
        detailedAddress: '',
      });
      drawSource?.clear();
    };
  }, []);

  useEffect(() => {
    drawSource?.clear();
    setAddress('');
    if (detailedAddress) {
      setAddress(detailedAddress);
    }

    if (selectedLngLat) {
      const coordinate = [selectedLngLat.lng, selectedLngLat.lat];
      const featureById = drawSource?.getFeatureById('address_marker');
      if (featureById) {
        drawSource?.removeFeature(featureById);
      }

      iconFeature.setGeometry(new Point(coordinate));
      drawSource?.addFeature(iconFeature);
      map?.getView().setCenter(coordinate);
      map?.getView().setZoom(10);
      setLngLat(selectedLngLat);
    }
  }, [selectedDo, selectedSubDistrict, selectedLngLat, detailedAddress]);

  useEffect(() => {
    handleSetAddress({
      selectedDo: '',
      selectedSubDistrict: '',
      selectedLngLat: null,
      detailedAddress: '',
    });
    const view = map?.getView();
    if (view) {
      let center: number[] = [];
      if (countryCode === SPACE_COUNTRY_CODE_JP) {
        center = [15558454.49842193, 4257326.124240982];
      } else if (countryCode === SPACE_COUNTRY_CODE_KR) {
        center = [14134326.456645714, 4516772.805272909];
      }
      view.setZoom(7);
      view.setCenter(center);
    }
  }, [countryCode]);

  const handleChangeAddress = (address: string) => {
    setAddress(address);
  };

  const handleSaveProduceStepData = async (metaId: string) => {
    const projectId = projectInfo.projectId;
    if (projectId) {
      handleSetProjectProduceStep({
        step: 4,
        metaId,
      });
      await putProjectStepData(projectId, {
        produceStepData: JSON.stringify({
          ...projectProduceStep,
          metaId,
          step: 4,
        }),
      });
    }
  };

  return (
    <>
      <Header />
      <Body
        spaceName={spaceName}
        countryCode={countryCode}
        address={address}
        onChangeSpaceName={setSpaceName}
        onChangeCountryCode={setCountryCode}
        onChangeAddress={handleChangeAddress}
        onSubmitAddress={handleFetchAddress}
      />
      <Footer valid={isValid} onSubmit={handleSubmit} />
    </>
  );
}

function Header() {
  const { t } = useTranslation();

  return (
    <div className="container-fluid d-flex align-items-center py-4">
      <div className="flex d-flex">
        <div className="mr-24pt">
          <h3 className="mb-0">{t('text_space_registration')}</h3>
        </div>
      </div>
    </div>
  );
}

type BodyProps = {
  spaceName: string;
  countryCode: SpaceCountryCode;
  address: string;
  onChangeSpaceName: (spaceName: string) => void;
  onChangeCountryCode: (countryCode: SpaceCountryCode) => void;
  onChangeAddress: (address: string) => void;
  onSubmitAddress: () => void;
};

function Body({
  spaceName,
  countryCode,
  address,
  onChangeSpaceName,
  onChangeCountryCode,
  onChangeAddress,
  onSubmitAddress,
}: BodyProps) {
  const { t } = useTranslation();
  const [showKRPostCodeModal, setShowKRPostCodeModal] = useState(false);
  const {
    selectedDo,
    selectedSubDistrict,
    handleSetAddress,
    handleChangeShowAddressPane,
  } = useSpaceRegister();

  const handleClickAddressSearch = () => {
    if (countryCode === SPACE_COUNTRY_CODE_JP) {
      handleChangeShowAddressPane(true);
    } else if (countryCode === SPACE_COUNTRY_CODE_KR) {
      setShowKRPostCodeModal(true);
    }
  };

  return (
    <>
      <DaumPostcodeModal
        show={showKRPostCodeModal}
        onHide={() => setShowKRPostCodeModal(false)}
        onComplete={(data) => {
          handleSetAddress({
            selectedDo: data.address,
            selectedSubDistrict: data.extra,
            selectedLngLat: data.lngLat,
            detailedAddress: '',
          });
        }}
      />
      <div className="container-fluid">
        <FormGroup>
          <FormLabel textKey={'text_space_name'} essential={true} />
          <input
            type="text"
            className="form-line"
            placeholder={t('place_holder_space_name')}
            value={spaceName}
            onChange={(e) => {
              onChangeSpaceName(e.target.value);
            }}
          />
        </FormGroup>
        <FormGroup>
          <FormLabel textKey={'text_country'} />
          <select
            className="form-line"
            value={countryCode}
            onChange={(e) => {
              onChangeCountryCode(Number(e.target.value) as SpaceCountryCode);
            }}
          >
            <option value={SPACE_COUNTRY_CODE_KR}>{t('country_kr')}</option>
            <option value={SPACE_COUNTRY_CODE_JP}>{t('country_jp')}</option>
          </select>
        </FormGroup>
        <FormGroup className={'over-txt'}>
          <FormLabel textKey={'text_address'} essential={true} />
          <input
            type="text"
            className="form-line"
            style={{
              paddingRight: '75px',
            }}
            readOnly={true}
            value={selectedDo + selectedSubDistrict}
          />
          <a
            className="btn btn-secondary btn-sm"
            onClick={handleClickAddressSearch}
          >
            {t('text_address_search')}
          </a>
        </FormGroup>
        <FormGroup>
          <FormLabel textKey={'text_detailed_address'} essential={true} />
          <input
            type="text"
            className="form-line"
            style={{
              paddingRight: '100px',
            }}
            value={address}
            disabled={!selectedDo && !selectedSubDistrict}
            onChange={(e) => {
              onChangeAddress(e.target.value);
            }}
            onKeyPress={(e) => {
              if (e.key === 'Enter') {
                if (address) {
                  onSubmitAddress();
                }
              }
            }}
          />
          <a
            className={classNames('btn btn-secondary btn-sm', {
              disabled: !address,
            })}
            onClick={() => {
              if (address) {
                onSubmitAddress();
              }
            }}
          >
            {t('text_detailed_location_search')}
          </a>
        </FormGroup>
      </div>
    </>
  );
}

type FooterProps = {
  valid: boolean;
  onSubmit: () => void;
};

function Footer({ valid, onSubmit }: FooterProps) {
  const { t } = useTranslation();
  const { handleChangeRegisterStep } = useProjectRegister();

  const [showInvalidMessage, setShowInvalidMessage] = useState(false);

  useEffect(() => {
    if (valid) {
      setShowInvalidMessage(false);
    }
  }, [valid]);

  return (
    <>
      {showInvalidMessage && <InvalidAlert />}
      <div className="my-32pt">
        <div className="d-flex align-items-center justify-content-center">
          <a
            className="btn btn-outline-secondary mr-8pt"
            onClick={() => handleChangeRegisterStep(2)}
          >
            {t('text_to_the_prev')}
          </a>
          <a
            className={classNames('btn btn-outline-accent ml-0')}
            onClick={() => {
              if (valid) {
                setShowInvalidMessage(false);
                onSubmit();
              } else {
                setShowInvalidMessage(true);
              }
            }}
          >
            {t('text_after_space_register_and_to_the_next')}
          </a>
        </div>
      </div>
    </>
  );
}

export default ProjectSpaceAddressRegister;
