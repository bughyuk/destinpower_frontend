import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import FormGroup from '@/components/FormGroup';
import FormLabel from '@/components/FormLabel';
import { ValidUtils } from '@/utils';
import { putMyInfoPassword } from '@/api/user';
import { Alert } from 'react-bootstrap';
import MaterialIcon from '@/components/MaterialIcon';

function AccountSecurity() {
  const { t } = useTranslation();
  const [inputs, setInputs] = useState<{
    password: string;
    newPassword: string;
    rePassword: string;
  }>({
    password: '',
    newPassword: '',
    rePassword: '',
  });

  const [valid, setValid] = useState<{
    password: boolean;
    newPassword: boolean;
    rePassword: boolean;
  }>({
    password: true,
    newPassword: true,
    rePassword: true,
  });

  const [passwordInvalidMessage, setPasswordInvalidMessage] = useState(
    'msg_valid_enter_all_info_changing_password'
  );
  const [success, setSuccess] = useState(false);

  const validForm = () => {
    let password = true;
    let newPassword = true;
    let rePassword = true;

    if (inputs.password && inputs.newPassword && inputs.rePassword) {
      if (!ValidUtils.validatePassword(inputs.newPassword)) {
        newPassword = false;
      }

      if (inputs.newPassword !== inputs.rePassword) {
        rePassword = false;
      }
    } else {
      password = false;
    }

    setValid({
      password,
      newPassword,
      rePassword,
    });

    return password && newPassword && rePassword;
  };

  const handleSubmit = async () => {
    setSuccess(false);
    const isValid = validForm();

    if (isValid) {
      const result = await putMyInfoPassword({
        password: inputs.password,
        newPassword: inputs.newPassword,
      });

      if (!result) {
        setValid({
          ...valid,
          password: false,
          newPassword: true,
          rePassword: true,
        });
        setPasswordInvalidMessage('msg_valid_incorrectly_old_password');
      } else {
        setSuccess(true);
        setTimeout(() => {
          setSuccess(false);
          setInputs({
            password: '',
            newPassword: '',
            rePassword: '',
          });
          setPasswordInvalidMessage(
            'msg_valid_enter_all_info_changing_password'
          );
        }, 1000);
      }
    }
  };

  return (
    <div className="contents-section account-wrap">
      <div className="cell">
        <div className="row align-items-start">
          <div className="col-md-5">
            <span className="material-icons font-size-32pt mb-3">password</span>
            <div className="flex title-row">
              <h3 className="mb-2">{t('text_change_password')}</h3>
              {/*
              <small className="text-muted text-headings">
                {t('text_last_update_date')} :{' '}
                <strong className="text-dark">
                  <mark>2021-01-06</mark>
                </strong>
              </small>
              */}
            </div>
          </div>
          <div className="col-md-7">
            <div className="mb-5">
              <FormGroup>
                <FormLabel textKey={'text_current_password'} />
                <input
                  type="password"
                  className="form-line"
                  placeholder={t('place_holder_password')}
                  value={inputs.password}
                  id={'password'}
                  onChange={(e) => {
                    setInputs({
                      ...inputs,
                      [e.target.id]: e.target.value,
                    });
                  }}
                />
                {!valid.password && (
                  <div className="invalid-feedback">
                    {t(passwordInvalidMessage)}
                  </div>
                )}
              </FormGroup>
              <FormGroup>
                <FormLabel textKey={'text_new_password'} />
                <input
                  type="password"
                  className="form-line"
                  placeholder={t('place_holder_password')}
                  value={inputs.newPassword}
                  id={'newPassword'}
                  onChange={(e) => {
                    setInputs({
                      ...inputs,
                      [e.target.id]: e.target.value,
                    });
                  }}
                />
                {!valid.newPassword && (
                  <div className="invalid-feedback">
                    {t('msg_valid_format_password')}
                  </div>
                )}
              </FormGroup>
              <FormGroup>
                <FormLabel textKey={'text_re_password'} />
                <input
                  type="password"
                  className="form-line"
                  placeholder={t('place_holder_password')}
                  value={inputs.rePassword}
                  id={'rePassword'}
                  onChange={(e) => {
                    setInputs({
                      ...inputs,
                      [e.target.id]: e.target.value,
                    });
                  }}
                />
                {!valid.rePassword && (
                  <div className="invalid-feedback">
                    {t('msg_valid_re_password_not_match')}
                  </div>
                )}
              </FormGroup>
            </div>
            {success && (
              <Alert className="alert-soft-success">
                <div className="d-flex flex-wrap align-items-center">
                  <div className="mr-8pt">
                    <MaterialIcon name={'done'} />
                  </div>
                  <div className="flex" style={{ minWidth: '180px' }}>
                    <small className="text-black-100">
                      {t('msg_completed_password_change')}
                    </small>
                  </div>
                </div>
              </Alert>
            )}
            {!success && (
              <div className="my-32pt">
                <div className="d-flex align-items-center justify-content-start">
                  <a className="btn btn-accent ml-0" onClick={handleSubmit}>
                    {t('text_to_change')}
                  </a>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default AccountSecurity;
