import React, { FormEvent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useSpacePane } from '@/modules/space/hook';
import MaterialIcon from '@/components/MaterialIcon';
import { PANE_STATUS_REGISTER } from '@/utils/constants/common';
import { useHomeMenu } from '@/modules/home/hook';
import { MENU_SPACE } from '@/modules/home/types';

type SpaceListHeaderProps = {
  count?: number;
  possibleRegister?: boolean;
  onSubmit?: (searchKeyword: string) => void;
};

function SpaceListHeader({
  count = 0,
  possibleRegister = true,
  onSubmit,
}: SpaceListHeaderProps) {
  const { t } = useTranslation();
  const history = useHistory();
  const { activeMenuIdx } = useHomeMenu();
  const { handleChangePaneStatus } = useSpacePane();
  const [searchKeyword, setSearchKeyword] = useState('');
  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    onSubmit?.call(null, searchKeyword);
  };

  useEffect(() => {
    setSearchKeyword('');
  }, [activeMenuIdx]);

  return (
    <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
      <div className="flex title-row">
        <h3 className="d-flex align-items-center mb-0">
          <MaterialIcon name={'map'} outlined={true} align={'left'} />
          {t('text_space_list')}
        </h3>
        {activeMenuIdx === MENU_SPACE && (
          <small className="text-muted text-headings text-uppercase">
            <strong className="text-accent">{count + t('text_count')}</strong>
            {t('msg_there_are_number_space_registered')}
          </small>
        )}
      </div>
      {activeMenuIdx === MENU_SPACE && (
        <form className="form-inline ml-4" onSubmit={handleSubmit}>
          <input
            type="text"
            className="form-control search"
            placeholder="Search"
            value={searchKeyword}
            onChange={(e) => setSearchKeyword(e.target.value)}
          />
          <a
            className="btn btn-sm btn-light"
            onClick={() => onSubmit?.call(null, searchKeyword)}
          >
            <MaterialIcon name={'search'} />
          </a>
        </form>
      )}
      {possibleRegister && (
        <a
          className="btn btn-accent ml-4"
          onClick={(e) => {
            handleChangePaneStatus(PANE_STATUS_REGISTER);
            history.replace('/space');
          }}
        >
          {t('text_space_new_registration')}
        </a>
      )}
    </div>
  );
}

export default SpaceListHeader;
