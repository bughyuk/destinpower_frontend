import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { CommonUtils } from '@/utils';
import { fetchGroups, postGroup } from '@/api/group';
import FormLabel from '@/components/FormLabel';
import InvalidAlert from '@/components/InvalidAlert';
import ListSearch from '@/components/ListSearch';
import Pagination from '@/components/Pagination';
import { FUNCTION_DELETE, FUNCTION_EDIT } from '@/utils/constants/common';
import ListDropdown from '@/components/ListDropdown';
import classNames from 'classnames';
import { Group } from '@/modules/group/types';
import GroupManagementEditModal from '@/components/GroupManagementEditModal';

type GroupManagementListProps = {
  reloadFlag: boolean;
  onClickItem: (groupSeq: number) => void;
  onClickDelete: (group: Group) => void;
};

function GroupManagementList({
  reloadFlag,
  onClickItem,
  onClickDelete,
}: GroupManagementListProps) {
  const { t } = useTranslation();
  const [load, setLoad] = useState(false);
  const [groupList, setGroupList] = useState<Group[]>([]);
  const [loading, setLoading] = useState(false);
  const [inputs, setInputs] = useState<{
    groupName: string;
    groupUserMaxCount: number;
    groupDescription: string;
  }>({
    groupName: '',
    groupUserMaxCount: 1,
    groupDescription: '',
  });
  const [searchKeyword, setSearchKeyword] = useState('');
  const [showInvalidMessage, setShowInvalidMessage] = useState(false);
  const [page, setPage] = useState(1);
  const [totalCount, setTotalCount] = useState(0);
  const [showGroupEditModal, setShowGroupEditModal] = useState(false);
  const [groupEditData, setGroupEditData] = useState<Group | null>(null);

  useEffect(() => {
    setPage(1);
  }, [searchKeyword]);

  useEffect(() => {
    handleFetchGroupList();
  }, [page, searchKeyword, reloadFlag]);

  useEffect(() => {
    if (CommonUtils.isValidationObject(inputs)) {
      setShowInvalidMessage(false);
    }
  }, [inputs]);

  const handleClickCreateGroup = async () => {
    if (!CommonUtils.isValidationObject(inputs)) {
      setShowInvalidMessage(true);
      return;
    }

    setLoading(true);

    const result = await postGroup({
      groupName: inputs.groupName,
      groupDesc: inputs.groupDescription,
      maxCount: inputs.groupUserMaxCount,
    });

    setLoading(false);

    if (result) {
      setInputs({
        groupName: '',
        groupUserMaxCount: 1,
        groupDescription: '',
      });

      handleFetchGroupList();
    }
  };

  const handleFetchGroupList = async () => {
    setLoad(false);
    const result = await fetchGroups(page, searchKeyword);
    setGroupList(result.content);
    setTotalCount(result.totalElements);
    setLoad(true);
  };

  const handleSubmitSearch = (text: string) => {
    setSearchKeyword(text);
  };

  const handleShowGroupEditModal = (group: Group) => {
    setGroupEditData(group);
    setShowGroupEditModal(true);
  };

  return (
    <>
      <div className="col-md-4">
        <div className="cell">
          <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-5 sort-wrap">
            <div className="flex title-row">
              <h3 className="mb-0">{t('text_create_a_group')}</h3>
              <small className="text-muted text-headings text-uppercase">
                {t('msg_group_add_description')}
              </small>
            </div>
          </div>
          <div className="form-group mb-4">
            <FormLabel
              textKey={'text_group_name'}
              essential={true}
              htmlFor={'groupName'}
            />
            <input
              id="groupName"
              type="text"
              className="form-line pr-6"
              placeholder={t('msg_enter_group_name')}
              value={inputs.groupName}
              onChange={(e) =>
                setInputs({
                  ...inputs,
                  [e.target.id]: e.target.value,
                })
              }
              autoComplete={'off'}
            />
          </div>
          <div className="form-group mb-5">
            <FormLabel
              textKey={'text_number_of_group_user'}
              essential={true}
              htmlFor={'groupDescription'}
            />
            <input
              id="groupUserMaxCount"
              type="number"
              min={1}
              className="form-line"
              value={inputs.groupUserMaxCount}
              onKeyDown={(e) => {
                const key = e.key;
                if (key === '.') {
                  e.preventDefault();
                }
              }}
              onChange={(e) => {
                let value = e.target.value;
                if (value.match(/\./)) {
                  value = value.replace(/./g, '');
                }

                let number = 1;
                if (value) {
                  number = Number(value);
                  if (!number) {
                    number = 1;
                  } else {
                    if (number < 1) {
                      number = 1;
                    }
                  }
                }

                setInputs({
                  ...inputs,
                  [e.target.id]: number,
                });
              }}
            />
          </div>
          <div className="form-group mb-5">
            <FormLabel
              textKey={'text_group_description'}
              essential={true}
              htmlFor={'groupDescription'}
            />
            <input
              id="groupDescription"
              type="text"
              className="form-line"
              placeholder={t('msg_enter_description')}
              value={inputs.groupDescription}
              onChange={(e) =>
                setInputs({
                  ...inputs,
                  [e.target.id]: e.target.value,
                })
              }
              autoComplete={'off'}
            />
          </div>
          {showInvalidMessage && <InvalidAlert alertContainerPadding={false} />}
          <div className="form-group text-center mb-32pt">
            <button
              className={classNames('btn btn-block btn-lg btn-accent', {
                'is-loading': loading,
              })}
              type="button"
              onClick={handleClickCreateGroup}
            >
              {t('text_create_a_group')}
            </button>
          </div>
        </div>
      </div>
      <div className="col-md-8">
        <div className="cell">
          <div className="d-flex flex-column flex-sm-row align-items-sm-center mb-24pt sort-wrap">
            <div className="flex title-row">
              <h3 className="mb-0">{t('text_group')}</h3>
              <small className="text-muted text-headings text-uppercase">
                {t('msg_group_grant_description')}
              </small>
            </div>
            <ListSearch onSubmit={handleSubmitSearch} />
          </div>
          <table className="table mb-4 table-nowrap">
            <colgroup>
              <col width="*" />
              <col width="15%" />
              <col width="15%" />
              <col width="15%" />
              <col width="5%" />
            </colgroup>
            <thead>
              <tr>
                <th>
                  <a>{t('text_group')}</a>
                </th>
                <th>
                  <a>{t('text_group_user')}</a>
                </th>
                <th>
                  <a>{t('text_access')}</a>
                </th>
                <th>
                  <a>{t('text_work')}</a>
                </th>
                <th></th>
              </tr>
            </thead>
            <tbody className="list">
              {!load && <></>}
              {load && groupList.length === 0 && (
                <tr>
                  <td colSpan={5}>
                    <div className="text-center py-4">
                      <span className="material-icons-outlined text-50 font-size-32pt">
                        info
                      </span>
                      <p className="m-0 text-50">{t('msg_group_empty')}</p>
                    </div>
                  </td>
                </tr>
              )}
              {groupList.map((group) => (
                <GroupItem
                  key={group.groupSeq}
                  {...group}
                  onClick={() => onClickItem(group.groupSeq)}
                  onClickEdit={() => handleShowGroupEditModal(group)}
                  onClickDelete={() => onClickDelete(group)}
                />
              ))}
            </tbody>
          </table>
          {groupList.length > 0 && (
            <Pagination
              curPage={page}
              totalCount={totalCount}
              onPageChange={setPage}
            />
          )}
        </div>
      </div>
      <GroupManagementEditModal
        group={groupEditData}
        show={showGroupEditModal}
        onHide={() => {
          setGroupEditData(null);
          setShowGroupEditModal(false);
        }}
        onReload={() => handleFetchGroupList()}
      />
    </>
  );
}

type GroupItemProps = Group & {
  onClick: () => void;
  onClickEdit: () => void;
  onClickDelete: () => void;
};

function GroupItem({
  groupName,
  groupDesc,
  maxCount,
  userCnt,
  projectCnt,
  onClick,
  onClickEdit,
  onClickDelete,
}: GroupItemProps) {
  const { t } = useTranslation();

  const handleSelectOptions = (eventKey: string | null) => {
    switch (eventKey) {
      case FUNCTION_EDIT:
        onClickEdit();
        break;
      case FUNCTION_DELETE:
        onClickDelete();
        break;
    }
  };

  return (
    <tr>
      <td>
        <a className="media-body" onClick={onClick}>
          <strong className="text-dark">{groupName}</strong>
          <div className="text-muted small">{groupDesc}</div>
        </a>
      </td>
      <td>{`${userCnt}/${maxCount}`}</td>
      <td>
        <strong className="text-accent">{projectCnt}</strong>
        {t('msg_there_are_number_projects')}
      </td>
      <td>
        <a className="text-underline text-primary" onClick={onClick}>
          {t('text_facts_and_figures_view')}
        </a>
      </td>
      <td className="text-right">
        <ListDropdown onSelect={handleSelectOptions} />
      </td>
    </tr>
  );
}

export default GroupManagementList;
