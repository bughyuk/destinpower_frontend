const Config = {
  platform_api: {
    uri: 'https://factory.watalbs.com/monitoring',
  },
  space_api: {
    uri: 'https://factory.watalbs.com/wtm',
  },
  address_api: {
    uri: 'https://factory.watalbs.com/wtm',
    zip_code_uri: 'https://factory.watalbs.com/wtm',
  },
  map_server: {
    geo_server_uri: 'https://factory.watalbs.com',
    indoor_edit_uri: 'https://factory.watalbs.com/watta_map',
    indoor_edit_origin: 'factory.watalbs.com',
  },
  // platform_api: {
  //   uri: 'https://lms-api.watalbs.com/monitoring',
  // },
  // space_api: {
  //   uri: 'https://lms-api.watalbs.com/wtm',
  // },
  // address_api: {
  //   uri: 'https://lms-api.watalbs.com/wtm',
  //   zip_code_uri: 'https://lms-api.watalbs.com/wtm',
  // },
  // map_server: {
  //   geo_server_uri: 'https://lms-api.watalbs.com',
  //   indoor_edit_uri: 'https://lms-api.watalbs.com/watta_map',
  //   indoor_edit_origin: 'lms-api.watalbs.com',
  // },
  backend_api: {
    uri: 'http://121.163.41.61:48080/dpserv',
  },
};

type DeepPartial<T> = {
  [P in keyof T]?: T[P] extends Array<infer U>
    ? Array<DeepPartial<U>>
    : // tslint:disable-next-line:no-shadowed-variable
    T[P] extends ReadonlyArray<infer U>
    ? ReadonlyArray<DeepPartial<U>>
    : DeepPartial<T[P]>;
};

export type BaseConfigType = DeepPartial<typeof Config>;
export default Config;
