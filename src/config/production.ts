import { BaseConfigType } from '@/config/default';

const Config: BaseConfigType = {
  // platform_api: {
  //   uri: 'https://lms-api.watalbs.com/monitoring',
  // },
  // space_api: {
  //   uri: 'https://lms-api.watalbs.com/wtm',
  // },
  // address_api: {
  //   uri: 'https://lms-api.watalbs.com/wtm',
  //   zip_code_uri: 'https://lms-api.watalbs.com/wtm',
  // },
  // map_server: {
  //   geo_server_uri: 'https://lms-api.watalbs.com',
  //   indoor_edit_uri: 'https://lms-api.watalbs.com/watta_map',
  //   indoor_edit_origin: 'lms-api.watalbs.com',
  // },
};

export default Config;
