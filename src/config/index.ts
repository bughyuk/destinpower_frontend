import _ from 'lodash';
import BaseConfig from './default';

const Config = _.cloneDeep(BaseConfig);

if (process.env.NODE_ENV) {
  try {
    (async () => {
      const EnvConfig = await import(`./${process.env.NODE_ENV}`);
      _.merge(Config, EnvConfig.default);
    })();
  } catch (e) {
    console.warn(`Cannot find configs for env=${process.env.NODE_ENV}`);
  }
}

export { Config };
