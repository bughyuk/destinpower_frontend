import {
  checkLabelStatus,
  clearBuffer,
  drawBitmap,
  getLabelData,
  printBuffer,
  setLabelId,
  drawDeviceFont,
} from '@/static/js/bxllabel';
import { requestPrint } from '@/static/js/bxlcommon';
import {
  BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER,
  BIXOLON_ERROR_CODE_ETC_ERROR,
  BIXOLON_ERROR_CODE_NO_ERROR,
  BIXOLON_RESULT_MESSAGE_CAN_NOT_CONNECT_TO_SERVER,
} from '@/utils/constants/common';

export function printQRCode(data, count, callback) {
  const { image, date, quantity, productName, standard } = data;
  if (!image) {
    return;
  }
  let isError = false;
  for (let i = 0; i < count; i++) {
    if (isError) {
      break;
    }
    setLabelId(i);
    checkLabelStatus();
    clearBuffer();
    drawDeviceFont(productName, 50, 50, 'a', 2, 2, 0, 0, 0, 0);
    drawDeviceFont(standard, 50, 100, 'a', 2, 2, 0, 0, 0, 0);
    drawDeviceFont(date, 50, 150, 'a', 2, 2, 0, 0, 0, 0);
    drawDeviceFont(quantity, 50, 200, 'a', 2, 2, 0, 0, 0, 0);
    drawBitmap(image, 212, 225, 415, 1);
    printBuffer();
    const labelData = getLabelData();
    requestPrint('Printer1', labelData, function (result) {
      let success = false;
      let errorCode = BIXOLON_ERROR_CODE_NO_ERROR;
      if (result === BIXOLON_RESULT_MESSAGE_CAN_NOT_CONNECT_TO_SERVER) {
        errorCode = BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER;
      } else if (result.indexOf('error') > -1) {
        errorCode = BIXOLON_ERROR_CODE_ETC_ERROR;
      } else if (result.indexOf('success') > -1) {
        success = true;
      }

      if (callback) {
        callback({
          success,
          errorCode,
        });
      }

      if (errorCode > -1) {
        isError = true;
      }
    });
  }
}
