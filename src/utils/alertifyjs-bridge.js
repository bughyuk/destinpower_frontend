import alertify from 'alertifyjs';

/**
 *
 * @param {string} message
 */
export function showSuccessMessage(message) {
  alertify.success(message);
}

/**
 *
 * @param {string} message
 */
export function showWarningMessage(message) {
  alertify.warning(message);
}
