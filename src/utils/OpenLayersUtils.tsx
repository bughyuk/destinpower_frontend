import { transform } from 'ol/proj';

export class OpenLayersUtils {
  static convertLngLat(coordinate: number[]) {
    if (coordinate.length < 2) {
      return coordinate;
    }

    return transform([coordinate[0], coordinate[1]], 'EPSG:4326', 'EPSG:3857');
  }
}
