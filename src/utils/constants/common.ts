import { AsyncState } from '@/modules/common';

export const COOKIE_KEY_REMEMBER_EMAIL = 'CONTROL_PLATFORM_ID';
export const COOKIE_MAX_AGE_REMEMBER_EMAIL = 60 * 60 * 24 * 30;
export const SESSION_STORAGE_KEY_DIRECT_PROCESS_MANAGEMENT_CATEGORY =
  'CONTROL_PLATFORM_DIRECT_PROCESS_MANAGEMENT_CATEGORY';
export const SESSION_STORAGE_KEY_USER_INFO = 'CONTROL_PLATFORM_USER_INFO';
export const LOCAL_STORAGE_KEY_PROJECT_ID = 'CONTROL_PLATFORM_PROJECT_ID';
export const LOCAL_STORAGE_KEY_USER_MENU_GUIDE_NOT_VIEW =
  'CONTROL_PLATFORM_USER_MENU_GUIDE_NOT_VIEW';

export const COOKIE_KEY_VIEWER_COUNTRY = 'CONTROL_PLATFORM_VIEWER_COUNTRY';
export const COOKIE_KEY_WHETHER_TO_SIGN_IN = 'CONTROL_PLATFORM_SIGN_IN';
export const COOKIE_MAX_AGE_WHETHER_TO_SIGN_IN = 60 * 10;

export const REALTIME_INTERVAL = 3000;
export const ASSETS_INTERVAL = 30000;

export const MENU_IDX_SEARCH = 0;
export const MENU_IDX_PROJECT = 1;
export const MENU_IDX_EVENT = 2;
export const MENU_IDX_ACCIDENT = 3;
export const MENU_IDX_SPACE = 4;
export const MENU_IDX_CONNECTED_SPACE = 5;
export const MENU_IDX_USER = 6;
export const MENU_IDX_ASSETS = 7;
export const MENU_IDX_ENTRY = 8;
export const MENU_IDX_STATISTICS = 9;
export const MENU_IDX_PHYSICAL_DISTRIBUTION = 10;
export const MENU_IDX_PROCESS_DASHBOARD = 60;
export const MENU_IDX_PROCESS_MANAGEMENT = 61;
export const MENU_IDX_PROCESS_SETTINGS = 62;

export const PAGING_MAX_DISPLAY = 5;
export const PAGING_SIZE = 8;

export const PANE_STATUS_LIST = 1 as const;
export const PANE_STATUS_DETAIL = 2 as const;
export const PANE_STATUS_REGISTER = 3 as const;
export const PANE_STATUS_EDIT = 4 as const;
export const PANE_STATUS_STATUS_BOARD = 5 as const;
export const PANE_STATUS_CATEGORY = 6 as const;
export const PANE_STATUS_PROJECT_CONNECT_TO_SPACE = 400;
export const PANE_STATUS_ACCIDENT_DIRECT_REGISTER = 600 as const;
export const PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER = 700 as const;
export const PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER = 701 as const;
export const PANE_STATUS_SPACE_FILE_REGISTER = 900 as const;
export const PANE_STATUS_SPACE_INDOOR_EDIT = 901 as const;

export type PaneStatus =
  | typeof PANE_STATUS_LIST
  | typeof PANE_STATUS_DETAIL
  | typeof PANE_STATUS_REGISTER
  | typeof PANE_STATUS_EDIT
  | typeof PANE_STATUS_PROJECT_CONNECT_TO_SPACE
  | typeof PANE_STATUS_ACCIDENT_DIRECT_REGISTER
  | typeof PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_REGISTER_ITEM_DIRECT_REGISTER
  | typeof PANE_STATUS_PHYSICAL_DISTRIBUTION_PRODUCT_EDIT_ITEM_DIRECT_REGISTER
  | typeof PANE_STATUS_SPACE_FILE_REGISTER
  | typeof PANE_STATUS_SPACE_INDOOR_EDIT
  | typeof PANE_STATUS_STATUS_BOARD
  | typeof PANE_STATUS_CATEGORY;

export const LAYER_INDIVIDUAL_LOCATION = 0;
export const LAYER_LOCATION_GUIDE = 1;
export const LAYER_EMERGENCY = 2;
export const LAYER_FIRE = 3;
export const LAYER_RESCUE = 4;
export const LAYER_INFECTIOUS_DISEASE = 5;
export const LAYER_HEAT_MAP = 6;
export const LAYER_DISTRIBUTION_MAP = 7;
export const LAYER_AP = 8;
export const LAYER_POI = 9;
export const LAYER_ZONE = 10;
export const LAYER_REALTIME = 11;

export const FUNCTION_SCOPE_AREA = 'SCOPE_AREA';
export const FUNCTION_SCOPE_CIRCLE = 'SCOPE_CIRCLE';
export const FUNCTION_SCOPE_POLYGON = 'SCOPE_POLYGON';

export const FUNCTION_LOCATION_EMERGENCY = 'LOCATION_EMERGENCY';
export const FUNCTION_LOCATION_FIRE = 'LOCATION_FIRE';
export const FUNCTION_LOCATION_RESCUE = 'LOCATION_RESCUE';
export const FUNCTION_LOCATION_INFECTIOUS_DISEASE =
  'LOCATION_INFECTIOUS_DISEASE';

export type AccidentFunction =
  | typeof FUNCTION_SCOPE_AREA
  | typeof FUNCTION_SCOPE_CIRCLE
  | typeof FUNCTION_SCOPE_POLYGON
  | typeof FUNCTION_LOCATION_EMERGENCY
  | typeof FUNCTION_LOCATION_FIRE
  | typeof FUNCTION_LOCATION_RESCUE
  | typeof FUNCTION_LOCATION_INFECTIOUS_DISEASE;

export const FUNCTION_EDIT = 'EDIT';
export const FUNCTION_COPY = 'COPY';
export const FUNCTION_DELETE = 'DELETE';
export const FUNCTION_CHANGE_NAME = 'CHANGE_NAME';
export const FUNCTION_DISCONNECT_SPACE = 'DISCONNECT_SPACE';
export const FUNCTION_DETAIL = 'DETAIL';
export const FUNCTION_ADD_USER_TO_GROUP = 'ADD_USER_TO_GROUP';
export const FUNCTION_CONTROL_ACCESS = 'CONTROL_ACCESS';

export const EDITING_INDOOR_MAP = 1;
export const EDITING_PATH = 2;
export const EDITING_POI = 3;

export const SPACE_FILE_REGISTER_MODE = 1;
export const SPACE_FILE_EDIT_MODE = 2;

export const GEOMETRY_TARGET_TYPE_INNER = 'INNER';
export const GEOMETRY_TARGET_TYPE_OUTER = 'OUTER';
export const GEOMETRY_TYPE_POLYGON = 'POLYGON';
export const GEOMETRY_TYPE_CIRCLE = 'CIRCLE';

export type SpaceFileMode =
  | typeof SPACE_FILE_REGISTER_MODE
  | typeof SPACE_FILE_EDIT_MODE;

export enum USER_TYPE {
  PERSON = 'USER',
  MANAGER = 'MANAGER',
}

export const MAP_TILE_KEY = 'cZ7VKzCSU2EFWJEaYgkn';
// export const MAP_TILE_KEY = '0xhNYsuSH6EDmzdFwzdS';

export const GOOGLE_API_KEY = 'AIzaSyCsF1QOEHTioegjao_zVawS0SFuiNoeEV4';

export const OPEN_LAYERS_RESOLUTIONS = [
  2048,
  1024,
  512,
  256,
  128,
  64,
  32,
  16,
  8,
  4,
  2,
  1,
  0.5,
  0.25,
  0.125,
  0.0625,
  0.03625,
  0.018125,
  0.0090625,
  0.00453125,
  0.002265625,
  0.0011328125,
  0.00056640625,
];

export type ResponsePrint = {
  success: boolean;
  errorCode:
    | typeof BIXOLON_ERROR_CODE_NO_ERROR
    | typeof BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER
    | typeof BIXOLON_ERROR_CODE_ETC_ERROR;
};

export const BIXOLON_ERROR_CODE_NO_ERROR = -1;
export const BIXOLON_ERROR_CODE_CAN_NOT_CONNECT_TO_SERVER = 0;
export const BIXOLON_ERROR_CODE_ETC_ERROR = 1;

export const BIXOLON_RESULT_MESSAGE_CAN_NOT_CONNECT_TO_SERVER =
  'Cannot connect to server';

export const ASYNC_DEFAULT_STATE: AsyncState = {
  loading: false,
  data: null,
  error: null,
};

export const ASYNC_LOADING_STATE: AsyncState = {
  loading: true,
  data: null,
  error: null,
};

export const ASYNC_SUCCESS_STATE = (data: any): AsyncState => ({
  loading: false,
  data,
  error: null,
});

export const ASYNC_ERROR_STATE = (error: any): AsyncState => ({
  loading: false,
  data: null,
  error: error,
});

// prefix front-end
export const PREFIX_FRONTEND_PATH = '/dppage' as string;
