import { PAGING_MAX_DISPLAY, PAGING_SIZE } from '@/utils/constants/common';
import moment from 'moment';
import {
  ACCIDENT_EMERGENCY,
  ACCIDENT_FIRE,
  ACCIDENT_INFECTIOUS_DISEASE,
  ACCIDENT_RESCUE,
  AccidentCategory,
} from '@/modules/accident/types';
import {
  ASSET_SIGNAL_STATUS_ERROR,
  ASSET_SIGNAL_STATUS_GOOD,
  ASSET_SIGNAL_STATUS_NO_SIGNAL,
  ASSET_SIGNAL_STATUS_USUALLY,
  AssetsSignalStatus,
} from '@/modules/assets/types';
import {
  DistributionHistoryLogisticsCategory,
  DistributionHistoryStatus,
} from '@/modules/physical_distribution/types';

export class CommonUtils {
  /**
   * @description 페이징 계산
   * @param curPage
   * @param totalCount
   */
  static calcPages(curPage: number, totalCount: number) {
    let startPage: number, endPage: number;
    const totalPage = Math.ceil(totalCount / PAGING_SIZE);
    if (totalPage <= PAGING_MAX_DISPLAY) {
      startPage = 1;
      endPage = totalPage;
    } else {
      const halfwayPoint = Math.ceil(PAGING_MAX_DISPLAY / 2);
      const pastHalfwayPoint = Math.floor(PAGING_MAX_DISPLAY / 2) + 1;
      const beforeHalfwayPoint = halfwayPoint - 1;
      if (curPage <= pastHalfwayPoint) {
        startPage = 1;
        endPage = PAGING_MAX_DISPLAY;
      } else if (curPage + beforeHalfwayPoint >= totalPage) {
        startPage = totalPage - (PAGING_MAX_DISPLAY - 1);
        endPage = totalPage;
      } else {
        startPage = curPage - halfwayPoint;
        endPage = curPage + beforeHalfwayPoint;
      }
    }

    const pages = [...Array<number>(endPage + 1 - startPage).keys()].map(
      (i) => startPage + i
    );

    return {
      pages,
      totalPage,
    };
  }

  /**
   * @description 경과 시간 계산
   * @param inputDate
   */
  static calcElapsedTime(inputDate: string, standardDate?: string) {
    const startDate = moment(inputDate);
    let standardMoment: moment.Moment = moment();
    if (standardDate) {
      standardMoment = moment(standardDate);
    }
    const calcMilliseconds = moment
      .duration(standardMoment.diff(startDate))
      .asMilliseconds();

    if (calcMilliseconds < 0) {
      return `00:00:00`;
    }

    let seconds = calcMilliseconds / 1000;
    let minutes = seconds / 60;
    seconds %= 60;
    let hours = minutes / 60;
    minutes %= 60;

    hours = Math.floor(hours);
    minutes = Math.floor(minutes);
    seconds = Math.floor(seconds);

    return `${CommonUtils.padZeroTime(hours)}:${CommonUtils.padZeroTime(
      minutes
    )}:${CommonUtils.padZeroTime(seconds)}`;
  }

  /**
   * @description 숫자 영 메워넣기
   * @param input
   */
  static padZeroTime(input: number) {
    let output = input.toString();
    if (input < 10) {
      output = `0${input}`;
    }

    return output;
  }

  static isEmptyObject(input: Record<string, any>) {
    return !Object.keys(input).length;
  }

  static isValidationObject(input: Record<string, any>) {
    let result = true;
    Object.values(input).some((x) => {
      if (!x) {
        result = false;
        return true;
      }
    });
    return result;
  }

  static convertDateFormat(input: string, format: string) {
    if (!input) {
      return '';
    }

    return moment(input).format(format);
  }

  static convertFloorsFormat(input: number) {
    if (input === null) {
      return '';
    }

    if (input >= 0) {
      return `${input}F`;
    } else {
      return `B${Math.abs(input)}`;
    }
  }

  static getAccidentIconName(category: AccidentCategory) {
    let icon = '';
    switch (category) {
      case ACCIDENT_EMERGENCY:
        icon = 'warning_cau.svg';
        break;
      case ACCIDENT_FIRE:
        icon = 'warning_fire.svg';
        break;
      case ACCIDENT_RESCUE:
        icon = 'warning_emg.svg';
        break;
      case ACCIDENT_INFECTIOUS_DISEASE:
        icon = 'warning_virus.svg';
        break;
    }

    return icon;
  }

  static getRandomIntInclusive(min: number, max: number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  static numberWithCommas(value: number) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  static getAssetsColorFromStatus(signalStatus: AssetsSignalStatus) {
    let color = '';
    switch (signalStatus) {
      case ASSET_SIGNAL_STATUS_GOOD:
        color = 'bg-pin-blue';
        break;
      case ASSET_SIGNAL_STATUS_USUALLY:
        color = 'bg-pin-green';
        break;
      case ASSET_SIGNAL_STATUS_ERROR:
        color = 'bg-pin-yellow';
        break;
      case ASSET_SIGNAL_STATUS_NO_SIGNAL:
        color = 'bg-pin-red';
        break;
    }

    return color;
  }

  static getProductStandard(weight: number, boxQuantity: number) {
    return `${weight}g x ${boxQuantity}`;
  }

  static getDistributionHistoryTextKey(
    logisticsCategory: DistributionHistoryLogisticsCategory,
    distributionHistoryStatus: DistributionHistoryStatus
  ) {
    let icon = '';
    let textKey = '';

    if (logisticsCategory === 'WAREHOUSING') {
      icon = 'arrow_downward';
      if (distributionHistoryStatus === 'READY') {
        textKey = 'text_warehousing_ready';
      } else if (distributionHistoryStatus === 'COMPLETE') {
        textKey = 'text_warehousing_complete';
      }
    } else if (logisticsCategory === 'RELEASING') {
      icon = 'arrow_upward';
      if (distributionHistoryStatus === 'READY') {
        textKey = 'text_release_ready';
      } else if (distributionHistoryStatus === 'COMPLETE') {
        textKey = 'text_release_complete';
      }
    } else if (logisticsCategory === 'ADJUSTMENT') {
      icon = 'swap_vert';
      if (distributionHistoryStatus === 'READY') {
        textKey = 'text_adjustment_ready';
      } else if (distributionHistoryStatus === 'COMPLETE') {
        textKey = 'text_adjustment_complete';
      }
    } else if (logisticsCategory === 'MOVE') {
      icon = 'east';
      if (distributionHistoryStatus === 'READY') {
        textKey = 'text_movement_ready';
      } else if (distributionHistoryStatus === 'COMPLETE') {
        textKey = 'text_movement_complete';
      }
    }

    return {
      icon,
      textKey,
    };
  }
}
