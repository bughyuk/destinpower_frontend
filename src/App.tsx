import React, { useEffect } from 'react';
import {
  BrowserRouter as Router,
  matchPath,
  Redirect,
  Route,
  Switch,
  useHistory,
} from 'react-router-dom';
import Access from '@/page/Access';
import { UserContextProvider } from '@/modules/user/context';
import Map from '@/page/Map';
import { MapContextProvider } from '@/modules/map/context';
import Home from '@/page/Home';
import Space from '@/page/Space';
import { SetupContextProvider } from '@/modules/setup/context';
import {
  SESSION_STORAGE_KEY_USER_INFO,
  PREFIX_FRONTEND_PATH,
} from '@/utils/constants/common';
import { useUser } from '@/modules/user/hook';
import { SpaceContextProvider } from '@/modules/space/context';
import Project from '@/page/Project';
import { ProjectContextProvider } from '@/modules/project/context';
import axios from 'axios';
import { StatusCodes } from 'http-status-codes';
import { fetchUserInfoMySelf, refreshAccessToken } from '@/api/user';
import { useLogout } from '@/modules/access/logout/hook';
import { HomeContextProvider } from '@/modules/home/context';
import { PhysicalDistributionContextProvider } from '@/modules/physical_distribution/context';
import Init from '@/page/Init';

function AuthenticationRoute() {
  const { handleRefresh, handleUserSet } = useUser();
  const { handleClearUser } = useLogout();
  const history = useHistory();

  const handleInitUserInfo = async () => {
    const item = sessionStorage.getItem(SESSION_STORAGE_KEY_USER_INFO);
    if (item) {
      handleRefresh();
      const userInfo = await fetchUserInfoMySelf();
      if (userInfo) {
        handleUserSet({
          userName: userInfo.username,
          ...userInfo,
        });
      }
    }
  };

  useEffect(() => {
    axios.defaults.withCredentials = true;
    handleInitUserInfo();
  }, []);

  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    async function (error) {
      const originalRequest = error.config;
      if (error.response) {
        if (
          error.response.status === StatusCodes.UNAUTHORIZED &&
          !originalRequest._retry
        ) {
          const sessionObj = sessionStorage.getItem(
            SESSION_STORAGE_KEY_USER_INFO
          );
          const userInfo = sessionObj ? JSON.parse(sessionObj) : null;
          if (userInfo) {
            try {
              const data = await refreshAccessToken();
              if (data) {
                const accessToken = data.accessToken;
                originalRequest.headers[
                  'Authorization'
                ] = `Bearer ${accessToken}`;
                axios.defaults.headers.common[
                  'Authorization'
                ] = `Bearer ${accessToken}`;
                userInfo.accessToken = accessToken;
                sessionStorage.setItem(
                  SESSION_STORAGE_KEY_USER_INFO,
                  JSON.stringify(userInfo)
                );
              } else {
                handleClearUser();
                history.replace('/signin');
                return;
              }
            } catch (e) {
              handleClearUser();
              history.replace('/signin');
              return;
            }
          }
          return axios(originalRequest);
        }
        return Promise.reject(error);
      }
    }
  );

  if (
    matchPath(location.pathname, {
      path: [
        `${PREFIX_FRONTEND_PATH}/reset/password/:key`,
        `${PREFIX_FRONTEND_PATH}/invite/:key`,
      ],
      exact: true,
    })
  ) {
    return <></>;
  }

  let redirectTo = '/signin';
  const userInfo = sessionStorage.getItem(SESSION_STORAGE_KEY_USER_INFO);
  if (userInfo) {
    redirectTo = '/init';
  }
  return <Redirect to={redirectTo} />;
}

function App(): React.ReactElement {
  return (
    <UserContextProvider>
      <HomeContextProvider>
        <SetupContextProvider>
          <ProjectContextProvider>
            <SpaceContextProvider>
              <MapContextProvider>
                <PhysicalDistributionContextProvider>
                  <Router basename={PREFIX_FRONTEND_PATH}>
                    <AuthenticationRoute />
                    <Switch>
                      <Route
                        path={[
                          '/signin',
                          '/signup',
                          '/find/password',
                          '/reset/password/:key',
                          '/invite/:key',
                        ]}
                        component={Access}
                      />
                      <Route path={'/home'} component={Home} />
                      <Route path={'/project'} component={Project} />
                      <Route path={'/space'} component={Space} />
                      <Route path={'/init'} component={Init} />
                      <Route path={'/control'} component={Map} />
                    </Switch>
                  </Router>
                  <Global />
                </PhysicalDistributionContextProvider>
              </MapContextProvider>
            </SpaceContextProvider>
          </ProjectContextProvider>
        </SetupContextProvider>
      </HomeContextProvider>
    </UserContextProvider>
  );
}

function Global() {
  return <></>;
}

export default App;
