import React, { useEffect } from 'react';
import MainContainer from '@/components/MainContainer';
import TopNavbar from '@/components/TopNavbar';
import ContentContainer from '@/components/ContentContainer';
import LeftSidebar from '@/components/LeftSidebar';
import SearchNavbar from '@/components/SearchNavbar';
import StatusNavbar from '@/components/StatusNavbar';
import LeftMenu from '@/components/LeftMenu';
import { useActiveMenu, useMapMenu, useSetupInit } from '@/modules/setup/hook';
import {
  MENU_IDX_PHYSICAL_DISTRIBUTION,
  MENU_IDX_PROCESS_DASHBOARD,
} from '@/utils/constants/common';
import { useControlProject } from '@/modules/map/hook';
import { postProjectPushToken } from '@/api/project';
import firebase from 'firebase/app';
import '@firebase/messaging';
import PhysicalDistributionFilterNavbar from '@/components/PhysicalDistributionFilterNavbar';
import ProcessFilterNavbar from '@/components/ProcessFilterNavbar';

let firebaseApp: firebase.app.App;
function Map() {
  const { project } = useControlProject();
  const { handleMenuActive } = useActiveMenu();
  const { handleSetupInit } = useSetupInit();
  const { menus } = useMapMenu();

  const handlePostPushToken = async () => {
    if (project.id) {
      if (!firebase.apps.length) {
        firebaseApp = firebase.initializeApp(
          {
            apiKey: 'AIzaSyBVXG4UfDmB_YPDu8qAnjgyYJuhvgM2VPg',
            authDomain: 'inplemap-4b394.firebaseapp.com',
            databaseURL: 'https://project-id.firebaseio.com',
            projectId: 'inplemap-4b394',
            storageBucket: 'inplemap-4b394.appspot.com',
            messagingSenderId: '246253828554',
            appId: '1:246253828554:web:f79a8d91a9ef042df93733',
            measurementId: 'G-SLGEE28FCJ',
          },
          'WATA_CONTROL_PLATFORM_APP'
        );
      }

      if (firebaseApp && firebase.messaging.isSupported()) {
        const messaging = firebaseApp.messaging();

        if (messaging) {
          try {
            const token = await messaging.getToken();

            if (token) {
              postProjectPushToken(project.id, token);
            }
          } catch (e) {
            console.error(e);
          }
        }
      }
    }
  };

  const handleDeletePushToken = async () => {
    if (firebase.apps.length) {
      const firebaseApp = firebase.apps.find(
        (app) => app.name === 'WATA_CONTROL_PLATFORM_APP'
      );

      if (firebaseApp && firebase.messaging.isSupported()) {
        const messaging = firebaseApp.messaging();

        if (messaging) {
          try {
            await messaging.deleteToken();
          } catch (e) {
            console.error(e);
          }
        }
      }
    }
  };

  useEffect(() => {
    handleSetupInit();
    handlePostPushToken();
    // handleMenuActive(MENU_IDX_PHYSICAL_DISTRIBUTION);
    handleMenuActive(MENU_IDX_PROCESS_DASHBOARD);

    return () => {
      handleDeletePushToken();
    };
  }, []);

  return (
    <MainContainer>
      <ContentContainer />
      <LeftSidebar>
        {menus.map(
          (menu) =>
            menu.display && (
              <LeftMenu
                key={menu.menuIdx}
                menuIdx={menu.menuIdx}
                titleKey={menu.titleKey}
                iconName={menu.iconName}
                active={menu.active}
              />
            )
        )}
      </LeftSidebar>
    </MainContainer>
  );
}

export default Map;
