import React, { useEffect } from 'react';
import { useUser } from '@/modules/user/hook';
import HomeNavbar from '@/components/HomeNavbar';
import HomeSidebar from '@/components/HomeSidebar';
import HomeContent from '@/components/HomeContent';
import { fetchProject } from '@/api/project';
import { Floors, Space } from '@/modules/map/types';
import { LOCAL_STORAGE_KEY_PROJECT_ID } from '@/utils/constants/common';
import { useControlProject } from '@/modules/map/hook';
import { useHistory } from 'react-router-dom';
import { useHomeMenu } from '@/modules/home/hook';

function Home() {
  const history = useHistory();
  const { user } = useUser();
  const { handleSetProject } = useControlProject();
  const { handleSetMenu } = useHomeMenu();

  const handleRouteProject = async (projectId: string) => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((projectSpace) => {
        const projectFloors: Floors[] = [];
        projectSpace.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: projectSpace.mappingId,
          id: projectSpace.metaId,
          name: projectSpace.metaName,
          longitude: projectSpace.lng,
          latitude: projectSpace.lat,
          floorsList: projectFloors,
          registDate: projectSpace.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.replace('/control');
    }
  };

  useEffect(() => {
    const projectId = localStorage.getItem(LOCAL_STORAGE_KEY_PROJECT_ID);
    if (projectId) {
      handleRouteProject(projectId);
      localStorage.removeItem(LOCAL_STORAGE_KEY_PROJECT_ID);
    }
  }, [user.userId]);

  useEffect(() => {
    return () => {
      handleSetMenu(-1);
    };
  }, []);

  if (user.userId && !localStorage.getItem(LOCAL_STORAGE_KEY_PROJECT_ID)) {
    return (
      <div className="layout-sticky-subnav layout-default">
        <main className="dashboard-main">
          <HomeNavbar />
          <div className="dashboard-body">
            <HomeSidebar />
            <HomeContent />
          </div>
        </main>
      </div>
    );
  }

  return <></>;
}

export default Home;
