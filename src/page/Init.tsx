import React, { useEffect } from 'react';
import { useUser } from '@/modules/user/hook';
import { fetchProject, fetchProjects } from '@/api/project';
import { ListResult } from '@/modules/common';
import { Projects } from '@/modules/project/types';
import { Floors, Space } from '@/modules/map/types';
import { useControlProject } from '@/modules/map/hook';
import { useHistory } from 'react-router-dom';

function Init() {
  const history = useHistory();
  const { user } = useUser();
  const { handleSetProject } = useControlProject();

  useEffect(() => {
    if (user.userId) {
      handleFetchProject();
    }
  }, [user.userId]);

  if (!user.userId) {
    return <></>;
  }

  const handleFetchProject = async () => {
    const result = await fetchProjects(1, '');
    const listResult = result as ListResult<Projects>;
    if (listResult.content.length) {
      handleFetchProjectDetail(listResult.content[0].projectId);
    } else {
      history.replace('/home');
    }
  };

  const handleFetchProjectDetail = async (projectId: string) => {
    const projectDetail = await fetchProject(projectId);

    if (projectDetail) {
      const projectSpaceList: Space[] = [];
      projectDetail.buildings.forEach((space) => {
        const projectFloors: Floors[] = [];
        space.floors.forEach((floorsData) => {
          projectFloors.push({
            id: floorsData.mapId,
            name: floorsData.mapName,
            value: floorsData.mapFloor,
            cx: floorsData.cx,
            cy: floorsData.cy,
            scalex: floorsData.scalex,
            scaley: floorsData.scaley,
            filename: floorsData.filename,
            rotation: floorsData.rotation,
          });
        });

        projectSpaceList.push({
          mappingId: space.mappingId,
          id: space.metaId,
          name: space.metaName,
          longitude: space.lng,
          latitude: space.lat,
          floorsList: projectFloors,
          registDate: space.registDate,
        });
      });

      handleSetProject({
        id: projectDetail.projectId,
        name: projectDetail.projectName,
        note: projectDetail.note,
        solutionType: projectDetail.solutionType,
        spaceList: projectSpaceList,
      });

      history.replace('/control');
    }
  };

  return <></>;
}

export default Init;
