import React, { useEffect, useState } from 'react';
import MainContainer from '@/components/MainContainer';
import StatusNavbar from '@/components/StatusNavbar';
import TopNavbar from '@/components/TopNavbar';
import LeftSidebar from '@/components/LeftSidebar';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { useActiveMenu } from '@/modules/setup/hook';
import {
  MENU_IDX_SPACE,
  PANE_STATUS_EDIT,
  PANE_STATUS_REGISTER,
  PANE_STATUS_SPACE_FILE_REGISTER,
  PANE_STATUS_SPACE_INDOOR_EDIT,
} from '@/utils/constants/common';
import {
  useOpenLayers,
  useSpacePane,
  useSpaceRegister,
} from '@/modules/space/hook';
import MaterialIcon from '@/components/MaterialIcon';
import JapanAddressPane from '@/components/JapanAddressPane';
import JapanAddressSearch from '@/components/JapanAddressSearch';
import SpaceMapContainer from '@/components/SpaceMapContainer';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { OpenLayersUtils } from '@/utils/OpenLayersUtils';
import SpaceIndoorEditContainer from '@/components/SpaceIndoorEditContainer';
import StepProcessBar from '@/components/StepProcessbar';

function Space() {
  const { paneStatus } = useSpacePane();
  const { showPane } = useSpaceRegister();
  const { map, geofencingLayer } = useOpenLayers();

  let isRenderJapanAddressPane = false;
  if (paneStatus === PANE_STATUS_REGISTER || paneStatus === PANE_STATUS_EDIT) {
    isRenderJapanAddressPane = true;
  }

  return (
    <MainContainer>
      {paneStatus !== PANE_STATUS_SPACE_INDOOR_EDIT && (
        <>
          <Top />
          <LeftSidebar
            customPane={
              isRenderJapanAddressPane && (
                <JapanAddressPane show={showPane}>
                  <JapanAddressSearch
                    map={map}
                    geofencingLayer={geofencingLayer}
                    show={showPane}
                  />
                </JapanAddressPane>
              )
            }
          >
            <LeftMenu />
          </LeftSidebar>
        </>
      )}
      <ContentContainer />
    </MainContainer>
  );
}

function Top() {
  const { paneStatus } = useSpacePane();
  const [step, setStep] = useState(1);
  let isShowStepProcessBar = false;
  if (
    paneStatus === PANE_STATUS_REGISTER ||
    paneStatus === PANE_STATUS_SPACE_FILE_REGISTER
  ) {
    isShowStepProcessBar = true;
  }

  useEffect(() => {
    if (paneStatus === PANE_STATUS_REGISTER) {
      setStep(1);
    } else if (paneStatus === PANE_STATUS_SPACE_FILE_REGISTER) {
      setStep(2);
    }
  }, [paneStatus]);

  return (
    <>
      <TopNavbar>
        {isShowStepProcessBar && <StepProcessBar step={step} totalStep={4} />}
        <StatusNavbar />
      </TopNavbar>
    </>
  );
}

function ContentContainer() {
  const { paneStatus } = useSpacePane();

  return (
    <div className="mdk-drawer-layout__content page-content">
      {paneStatus === PANE_STATUS_SPACE_FILE_REGISTER && <TopToolbar />}
      {paneStatus !== PANE_STATUS_SPACE_INDOOR_EDIT && <SpaceMapContainer />}
      {paneStatus === PANE_STATUS_SPACE_INDOOR_EDIT && (
        <SpaceIndoorEditContainer />
      )}
      {paneStatus === PANE_STATUS_REGISTER && navigator.geolocation && (
        <BottomGPS />
      )}
    </div>
  );
}

function TopToolbar() {
  return (
    <>
      <FloorToolbar />
    </>
  );
}

function FloorToolbar() {
  const floorList: string[] = [];

  return (
    <div className="btn-mid-holder">
      <div className="btn-group">
        {floorList.map((floor) => (
          <a key={floor} className="btn btn-white">
            {floor}
          </a>
        ))}
      </div>
    </div>
  );
}

function LeftMenu() {
  const { t } = useTranslation();
  const { menuIdx, handleMenuActive } = useActiveMenu();

  useEffect(() => {
    handleMenuActive(MENU_IDX_SPACE);
  }, []);

  return (
    <li className="sidebar-menu-item">
      <a
        className={classNames({
          'sidebar-menu-button': true,
          active: menuIdx === MENU_IDX_SPACE,
        })}
        onClick={() => handleMenuActive(MENU_IDX_SPACE)}
      >
        <MaterialIcon
          name={'map'}
          outlined={true}
          className={'sidebar-menu-icon sidebar-menu-icon--left'}
        />
        <span className="sidebar-menu-text">{t('text_space')}</span>
      </a>
    </li>
  );
}

function BottomGPS() {
  const { t } = useTranslation();
  const { map } = useOpenLayers();

  const [gpsError, setGpsError] = useState<{
    show: boolean;
    description: string;
  }>({
    show: false,
    description: '',
  });
  return (
    <>
      <div className="btn-widget-holder">
        <ul>
          <li>
            <button
              type="button"
              className="btn btn-white mb-3"
              onClick={() => {
                navigator.geolocation.getCurrentPosition(
                  (position) => {
                    map
                      ?.getView()
                      .setCenter(
                        OpenLayersUtils.convertLngLat([
                          position.coords.longitude,
                          position.coords.latitude,
                        ])
                      );
                  },
                  (error) => {
                    let description = '';
                    if (error.code === error.PERMISSION_DENIED) {
                      description = t('msg_user_denied_location');
                    } else {
                      description = t(
                        'msg_occur_internal_error_found_location'
                      );
                    }
                    setGpsError({
                      show: true,
                      description,
                    });

                    setTimeout(() => {
                      setGpsError({
                        show: false,
                        description: '',
                      });
                    }, 3000);
                  },
                  {
                    enableHighAccuracy: false,
                    maximumAge: 0,
                    timeout: 5000,
                  }
                );
              }}
            >
              <FontAwesomeIcon icon={['fas', 'crosshairs']} />
            </button>
          </li>
        </ul>
      </div>
      {gpsError.show && (
        <div className="info-holder">
          <div className="inner bg-accent">
            <span className="wrong-location">{gpsError.description}</span>
          </div>
        </div>
      )}
    </>
  );
}

export default Space;
