import React, { CSSProperties, useEffect, useState } from 'react';
import MainContainer from '@/components/MainContainer';
import StatusNavbar from '@/components/StatusNavbar';
import TopNavbar from '@/components/TopNavbar';
import LeftSidebar from '@/components/LeftSidebar';
import { useTranslation } from 'react-i18next';
import { useActiveMenu } from '@/modules/setup/hook';
import {
  MENU_IDX_PROJECT,
  PANE_STATUS_PROJECT_CONNECT_TO_SPACE,
  PANE_STATUS_REGISTER,
} from '@/utils/constants/common';
import classNames from 'classnames';
import MaterialIcon from '@/components/MaterialIcon';
import LeftSidebarDepth from '@/components/LeftSidebarDepth';
import ProjectSpaceTab from '@/components/ProjectSpaceTab';
import {
  useOpenLayers,
  useProjectPane,
  useProjectRegister,
} from '@/modules/project/hook';
import ProjectMapContainer from '@/components/ProjectMapContainer';
import StepProcessBar from '@/components/StepProcessbar';
import JapanAddressPane from '@/components/JapanAddressPane';
import JapanAddressSearch from '@/components/JapanAddressSearch';
import { useSpace, useSpaceRegister } from '@/modules/space/hook';
import ProjectSpaceIndoorEditContainer from '@/components/ProjectSpaceIndoorEditContainer';
import BottomToolbar from '@/components/BottomToolbar';

function Project() {
  const { paneStatus, depthSidebarShow } = useProjectPane();
  const { registerStep } = useProjectRegister();
  const { showPane } = useSpaceRegister();
  const { map, geofencingLayer } = useOpenLayers();

  let isShowCustomPane = false;
  if (paneStatus === PANE_STATUS_REGISTER && registerStep === 3) {
    isShowCustomPane = true;
  }

  let leftSidebarCSSProperties: CSSProperties = {};
  let isShowMapContainer = true;
  if (paneStatus === PANE_STATUS_REGISTER && registerStep === 5) {
    isShowMapContainer = false;
    leftSidebarCSSProperties = {
      display: 'none',
    };
  }

  let isShowLeftSidebarDepth = false;
  if (
    (paneStatus === PANE_STATUS_REGISTER && registerStep === 2) ||
    paneStatus === PANE_STATUS_PROJECT_CONNECT_TO_SPACE
  ) {
    isShowLeftSidebarDepth = true;
  }

  return (
    <MainContainer>
      <ContentContainer />
      <TopNavbar>
        {paneStatus === PANE_STATUS_REGISTER && (
          <StepProcessBar step={registerStep} totalStep={7} />
        )}
        <StatusNavbar />
      </TopNavbar>
      <LeftSidebar
        style={leftSidebarCSSProperties}
        customPane={
          isShowCustomPane && (
            <JapanAddressPane show={showPane}>
              <JapanAddressSearch
                map={map}
                geofencingLayer={geofencingLayer}
                show={showPane}
              />
            </JapanAddressPane>
          )
        }
      >
        <LeftMenu />
      </LeftSidebar>
      {isShowLeftSidebarDepth && (
        <LeftSidebarDepth show={depthSidebarShow} className={'z-index-0'}>
          <ProjectSpaceTab />
        </LeftSidebarDepth>
      )}
      {!isShowMapContainer && (
        <div className="mdk-drawer-layout__content page-content">
          <ProjectSpaceIndoorEditContainer />
        </div>
      )}
    </MainContainer>
  );
}

function LeftMenu() {
  const { t } = useTranslation();
  const { menuIdx, handleMenuActive } = useActiveMenu();

  useEffect(() => {
    handleMenuActive(MENU_IDX_PROJECT);
  }, []);

  return (
    <li className="sidebar-menu-item">
      <a
        className={classNames({
          'sidebar-menu-button': true,
          active: menuIdx === MENU_IDX_PROJECT,
        })}
      >
        <MaterialIcon
          name={'topic'}
          className="sidebar-menu-icon sidebar-menu-icon--left"
          outlined={true}
        />
        <span className="sidebar-menu-text">{t('text_project')}</span>
      </a>
    </li>
  );
}

function ContentContainer() {
  const { paneStatus } = useProjectPane();
  const { registerStep } = useProjectRegister();
  const { map } = useOpenLayers();
  const { info } = useSpace();

  let isShowMapContainer = true;
  if (paneStatus === PANE_STATUS_REGISTER && registerStep === 5) {
    isShowMapContainer = false;
  }

  const [isVisibleLocationButton, setVisibleLocationButton] = useState(false);
  useEffect(() => {
    if (registerStep === 4) {
      setVisibleLocationButton(true);
    } else {
      setVisibleLocationButton(false);
    }
  }, [registerStep]);

  const handleClickLocation = () => {
    const location = info.space.location;
    if (location) {
      map?.getView().animate({
        center: [Number(location.lng), Number(location.lat)],
        zoom: 13,
      });
    }
  };

  return (
    <>
      {isShowMapContainer && (
        <>
          <ProjectMapContainer />
          <BottomToolbar
            map={map}
            visibleLocationButton={isVisibleLocationButton}
            onClickLocationButton={handleClickLocation}
          />
        </>
      )}
    </>
  );
}

export default Project;
