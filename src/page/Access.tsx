import React from 'react';
import { matchPath, Route, Switch } from 'react-router-dom';
import SignInForm from '@/components/SignInForm';
import SignUpForm from '@/components/SignUpForm';
import FindPasswordForm from '@/components/FindPasswordForm';
import ResetPasswordForm from '@/components/ResetPasswordForm';
import InviteForm from '@/components/InviteForm';
import { PREFIX_FRONTEND_PATH } from '@/utils/constants/common';

function Access() {
  const isSign = matchPath(location.pathname, {
    path: [
      `${PREFIX_FRONTEND_PATH}/signin`,
      `${PREFIX_FRONTEND_PATH}/signup`,
      `${PREFIX_FRONTEND_PATH}/invite/:key`,
    ],
    exact: true,
  });

  return (
    <div className="layout-sticky-subnav layout-default">
      <div className="mdk-header-layout js-mdk-header-layout sign-up">
        <div className="flex-parent">
          <div className="flex-child">
            <div className="layout-login__form">
              {isSign && <SignContent />}
              {!isSign && <FindContent />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function SignContent() {
  return (
    <>
      <Switch>
        <Route path={'/signin'} component={SignInForm} />
        <Route path={'/signup'} component={SignUpForm} />
        <Route path={'/invite/:key'} component={InviteForm} />
      </Switch>
    </>
  );
}

function FindContent() {
  return (
    <>
      <Switch>
        <Route path={'/find/password'} component={FindPasswordForm} />
        <Route path={'/reset/password/:key'} component={ResetPasswordForm} />
      </Switch>
    </>
  );
}

export default Access;
