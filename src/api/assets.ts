import axios from 'axios';
import { Config } from '@/config';
import { StatusCodes } from 'http-status-codes';
import {
  Assets,
  AssetsDiv,
  AssetsSummary,
  TemperatureAndHumidity,
  TemperatureAndHumidityRealtime,
  TemperatureAndHumidityStatus,
} from '@/modules/assets/types';
import { NodeResult } from '@/api/type';

export async function fetchAssetsSummary(params: {
  mapid: string;
  metaid: string;
}) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/assetInfoSummary`,
    {
      params,
    }
  );

  let result: AssetsSummary = {
    asset_cnt: '0',
    sensor_cnt: '0',
  };

  if (response.status === StatusCodes.OK) {
    result = response.data as AssetsSummary;
  }

  return result;
}

export async function fetchAssets(params: {
  mapid: string;
  metaid: string;
  assetdiv: AssetsDiv;
}) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/assetInfos`,
    {
      params,
    }
  );

  let result: Assets[] = [];
  if (response.status === StatusCodes.OK) {
    const data = response.data as NodeResult<Assets[]>;

    if (data.status === StatusCodes.OK) {
      result = data.result;
    }
  }

  return result;
}

export async function fetchAssetsTemperatureAndHumidity(params: {
  projectid: string;
  sensorid: string;
  dateinfo: string;
}) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/assetTempHum`,
    {
      params,
    }
  );

  let list: TemperatureAndHumidity[] = [];
  if (response.status === StatusCodes.OK) {
    const nodeResult = response.data as NodeResult<TemperatureAndHumidity[]>;
    if (nodeResult.status === StatusCodes.OK) {
      list = nodeResult.result;
    }
  }

  return list;
}

export async function fetchAssetsTemperatureAndHumidityRealtime(
  sensorId: string
) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/assetTempHumNow`,
    {
      params: {
        sensorid: sensorId,
      },
    }
  );

  if (response.status === StatusCodes.OK) {
    const nodeResult = response.data as NodeResult<
      TemperatureAndHumidityRealtime[]
    >;
    if (nodeResult.status === StatusCodes.OK) {
      return nodeResult.result[0];
    }
  }

  return null;
}

export async function fetchTemperatureAndHumidityStatuses(mapId: string) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/assetTempHumStatus`,
    {
      params: {
        mapid: mapId,
      },
    }
  );

  let temperatureAndHumidityStatuses: TemperatureAndHumidityStatus[] = [];
  if (response.status === StatusCodes.OK) {
    const nodeResult = response.data as NodeResult<
      TemperatureAndHumidityStatus[]
    >;
    if (nodeResult.status === StatusCodes.OK) {
      temperatureAndHumidityStatuses = nodeResult.result;
    }
  }

  return temperatureAndHumidityStatuses;
}

export async function postUpdateAssetsActiveFlag(data: {
  asset_uuid: string;
  onoff: boolean;
}) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/toggleSensor`,
    data
  );
  if (response.status === StatusCodes.OK) {
    return true;
  }

  return false;
}
