import { StatusCodes } from 'http-status-codes';

export type Pagination = {
  totcount: string;
};

export type SpaceResult = {
  success: 'ok' | 'fail';
};

export type DataResultMessage<T = any> = {
  result: string | boolean;
  detail: string;
  data?: T;
  origin?: string;
};

export type NodeResult<T> = {
  result: T;
  status: StatusCodes;
};

export type DetailResult<T> = {
  detail: T;
};
