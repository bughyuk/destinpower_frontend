import axios from 'axios';
import { Config } from '@/config';
import { StatusCodes } from 'http-status-codes';
import { SpaceResult, Pagination } from '@/api/type';
import {
  PoiInfo,
  PoiSearchInfo,
  UploadFloorsInfo,
} from '@/modules/space/types';
import { Area } from '@/modules/map/types';
import { WKT } from 'ol/format';
import { SearchDivision } from '@/modules/setup/types';

export type ResponseSpaceMetaInfo = {
  meta_id: string;
  meta_name: string;
  regist_date: string;
  update_date: string;
  country_code: string;
  country_txt: string;
  city_txt: string;
  address_txt1: string;
  lng: number;
  lat: number;
  thumbnail: string;
};

export type RequestSpaceMetaInfo = {
  metaname: string;
  imgId: string;
  countrycode: number;
  addres1: string;
  addres2: string;
  city: string;
  zipcode: string;
  country: string;
  lng: number;
  lat: number;
  userid: string;
};

export type ResponseFloorsInfo = {
  cx: number;
  cy: number;
  filename: string;
  orifilename: string;
  map_floor: number;
  map_id: string;
  map_image_id: string;
  map_name: string;
  meta_id: string;
  regist_date: string;
  rotation: number;
  scalex: number;
  scaley: number;
};

export type RequestFloorsInfo = {
  metaid: string;
  mapname: string;
  userid: string;
  floor: number;
};

export type RequestFloorsFile = RequestFloorsInfo & {
  file: File | null;
  mapid?: string;
  cx: number;
  cy: number;
  imgRotate: number;
  imgScalex: number;
  imgScaley: number;
  div: 'insert' | 'update';
};

export type RequestUpdateFloorsInfo = RequestFloorsInfo & {
  mapid: string;
  filename: string;
  originalname: string;
  cx: number;
  cy: number;
  imgRotate: number;
  imgScalex: number;
  imgScaley: number;
};

export type RequestUpdateFloorPlan = {
  mapid: string;
  mapname: string;
  userid: string;
  floor: number;
  cx: number;
  cy: number;
  imgRotate: number;
  imgScalex: number;
  imgScaley: number;
};

type RequestFetchSpaceParams = {
  userid: string;
  pageno?: number;
  metaname?: string;
};

export async function fetchSpace(
  recent: boolean,
  userId: string,
  page: number,
  searchKeyword: string
) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const params: RequestFetchSpaceParams = {
    userid: userId,
  };

  if (!recent) {
    params.pageno = page;
    params.metaname = searchKeyword;
  } else {
    params.pageno = -1;
  }

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/mapmetaInfos`,
    {
      params,
    }
  );

  if (response.status === StatusCodes.OK) {
    const data = response.data;
    const list = data[0] as ResponseSpaceMetaInfo[];
    const { totcount } = data[1][0] as Pagination;

    list.forEach((metaInfo) => {
      metaInfo.thumbnail = `/static/images/slider0${Number(
        Math.floor(Math.random() * (4 - 1) + 1)
      )}.jpg`;
    });

    return {
      list,
      listCount: Number(totcount),
    };
  }

  throw new Error('Server Error');
}

type ResponseSpaceInfo = {
  meta_id: string;
  meta_name: string;
  map_floor: number;
  map_name: string;
  lng: number;
  lat: number;
  country_txt: string;
  city_txt: string;
  address_txt1: string;
};

export type ProcessSpaceInfo = {
  spaceId: string;
  spaceName: string;
  lng: number;
  lat: number;
  country: string;
  city: string;
  address: string;
  floorsList: {
    floorsValue: number;
    floorsName: string;
  }[];
};

export async function fetchSpaceInfo(spaceId: string) {
  const axiosInstance = axios.create({
    withCredentials: false,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/mapMetaInfosList`,
    {
      params: {
        metaid: spaceId,
      },
    }
  );

  if (response.status === StatusCodes.OK) {
    const processData: ProcessSpaceInfo = {
      spaceId: '',
      spaceName: '',
      lng: 0,
      lat: 0,
      country: '',
      city: '',
      address: '',
      floorsList: [],
    };

    const data = response.data as ResponseSpaceInfo[];
    data.forEach((datum, i) => {
      if (i === 0) {
        processData.spaceId = datum.meta_id;
        processData.spaceName = datum.meta_name;
        processData.lng = Number(datum.lng);
        processData.lat = Number(datum.lat);
        processData.country = datum.country_txt;
        processData.city = datum.city_txt;
        processData.address = datum.address_txt1;
      }

      if (datum.map_floor != null && datum.map_name) {
        processData.floorsList.push({
          floorsValue: datum.map_floor,
          floorsName: datum.map_name,
        });
      }
    });

    return processData;
  }

  throw new Error('Server Error');
}

export async function fetchSpaceListInfo(spaceIds: string[]) {
  const result: ProcessSpaceInfo[] = [];

  for (const spaceId of spaceIds) {
    result.push(await fetchSpaceInfo(spaceId));
  }
  return result;

  throw new Error('Server Error');
}

export async function postSpaceMetaInfo(data: RequestSpaceMetaInfo) {
  const axiosInstance = axios.create({
    withCredentials: false,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/mapmetaInfo`,
    {
      params: data,
    }
  );
  return response.data;
}

export type RequestUpdateSpace = {
  metaid: string;
  userid: string;
  del?: boolean;
  countrycode?: number;
  addres1?: string;
  city?: string;
  country?: string;
  lng?: number;
  lat?: number;
  metaname?: string;
};

export async function postUpdateSpace(data: RequestUpdateSpace) {
  const axiosInstance = axios.create({
    withCredentials: false,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8',
    },
  });

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/updateMapmetaInfo`,
    {
      params: data,
    }
  );
  if (response.status === StatusCodes.OK) {
    const result = response.data as SpaceResult;
    if (result.success === 'ok') {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

export async function fetchFloors(userId: string, metaId: string) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(`${Config.space_api.uri}/mapInfos`, {
    params: {
      userid: userId,
      metaid: metaId,
    },
  });
  if (response.status === StatusCodes.OK && response.data) {
    return response.data as ResponseFloorsInfo[];
  }

  new Error('Server Error');
}

export async function postFloorsInfo(data: RequestFloorsInfo) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.post(`${Config.space_api.uri}/mapInfo`, {
    params: data,
  });
  return response.data;
}

export async function postFloorsFile(data: RequestFloorsFile) {
  const axiosInstance = axios.create({
    withCredentials: false,
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });

  const formData = new FormData();
  if (data.file) {
    formData.append('file', data.file);
  }
  formData.append('metaid', data.metaid);
  formData.append('userid', data.userid);
  formData.append('mapname', data.mapname);
  formData.append('floor', data.floor.toString());
  formData.append('cx', data.cx.toString());
  formData.append('cy', data.cy.toString());
  formData.append('imgRotate', data.imgRotate.toString());
  formData.append('imgScalex', data.imgScalex.toString());
  formData.append('imgScaley', data.imgScaley.toString());
  formData.append('div', data.div);
  if (data.mapid) {
    formData.append('mapid', data.mapid);
  }

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/upload`,
    formData
  );

  if (response.status === StatusCodes.OK) {
    const data = response.data as UploadFloorsInfo[];
    if (data.length > 0) {
      return data[0];
    }
  }

  return null;
}

export async function postUpdateFloors(data: RequestUpdateFloorsInfo) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/updatemapimageInfo`,
    {
      params: data,
    }
  );
  return response.data;
}

export async function postUpdateFloorPlan(data: RequestUpdateFloorPlan) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/updateMapInfo`,
    {
      params: data,
    }
  );
  return response.data;
}

export type RequestDeleteFloorPlan = {
  mapid: string;
  userid: string;
  del: boolean;
};

export async function postDeleteFloorPlan(data: RequestDeleteFloorPlan) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.post(
    `${Config.space_api.uri}/updateMapInfo`,
    {
      params: data,
    }
  );

  if (response.status === StatusCodes.OK) {
    const result = response.data as SpaceResult;
    if (result.success === 'ok') {
      return true;
    } else {
      return false;
    }
  }
  return false;
}

type requestFetchExtra = {
  metaid: string;
  mapid: string;
};

export async function fetchArea(data: requestFetchExtra) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });
  const response = await axiosInstance.get(
    `${Config.space_api.uri}/areaInfos`,
    {
      params: data,
    }
  );

  if (response.status === StatusCodes.OK && response.data) {
    const wkt = new WKT();
    const data = response.data as Area[];
    data.forEach((datum) => {
      const feature = wkt.readFeature(datum.geomstr);
      feature.set('area_info', datum);
      datum.feature = feature;
    });

    return data;
  }

  new Error('Server Error');
}

export async function fetchPoi(data: requestFetchExtra) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });
  const response = await axiosInstance.get(`${Config.space_api.uri}/poiInfos`, {
    params: data,
  });

  if (response.status === StatusCodes.OK && response.data) {
    return response.data as PoiInfo[];
  }

  new Error('Server Error');
}

type RequestSearchPoi = {
  searchDv: SearchDivision;
  mapid: string;
  poinm: string;
};

export async function fetchSearchPoi(params: RequestSearchPoi) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });
  const response = await axiosInstance.get(
    `${Config.space_api.uri}/poiInfoByName`,
    {
      params,
    }
  );

  if (response && response.status === StatusCodes.OK) {
    return response.data as PoiSearchInfo[];
  }

  new Error('Server Error');
}
