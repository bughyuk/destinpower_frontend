import axios from 'axios';
import { Config } from '@/config';
import { ApiResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import { RealtimeUser, UserTrace } from '@/modules/map/types';

export async function fetchEndPointUser(accessKey: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/endpoint-users/${accessKey}`
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as RealtimeUser;
  }

  throw new Error('Server Error');
}

export async function fetchEndPointUserTrace(accessKey: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/endpoint-users/${accessKey}/trace`
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    const data = result.data as UserTrace[];

    if (data.length > 1) {
      data.forEach((datum, i) => {
        let tail: UserTrace;
        if (i === data.length - 1) {
          tail = data[i - 1];
        } else {
          tail = data[i + 1];
        }
        datum.sectionPath = [
          [datum.lng, datum.lat],
          [tail.lng, tail.lat],
        ];
      });
    }

    return data;
  }

  throw new Error('Server Error');
}
