import axios from 'axios';
import { Config } from '@/config';
import { ApiResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import { MessageHistory } from '@/modules/accident/types';

type RequestFetchMessages = {
  projectId: string;
  parentType?: 'message' | 'accident' | 'logistics';
  parentId?: string;
  searchStartDates?: string;
  searchEndDates?: string;
  targetId?: string;
};

export async function fetchMessages(params: RequestFetchMessages) {
  const response = await axios.get(`${Config.platform_api.uri}/messages`, {
    params: {
      ...params,
      parentType: params.parentType || 'message',
    },
  });
  let messageHistories: MessageHistory[] = [];
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    messageHistories = result.data as MessageHistory[];
  }

  return messageHistories;
}

type RequestPostMessages = {
  projectId: string;
  parentId: string;
  parentType: string;
  messageTitle: string;
  messageContent: string;
  linkUrl?: string;
  imgId: string;
  targetIds: string[];
};

export async function postMessages(data: RequestPostMessages) {
  const response = await axios.post(
    `${Config.platform_api.uri}/messages`,
    data
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data;
  }

  throw new Error('Server Error');
}
