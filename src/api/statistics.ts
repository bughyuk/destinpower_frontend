import axios from 'axios';
import { Config } from '@/config';
import { ApiResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import {
  StatAccessInfo,
  StatCongestionArea,
  StatLogisticsStockByProduct,
  StatLogisticsStockChange,
  StatLogisticsSummary,
  StatLogisticsTrend,
  StatZone,
} from '@/modules/statistics/types';
import moment from 'moment';

export async function fetchStatZone(mappingId: string, mapId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/statistics/options/zone/${mappingId}/${mapId}`
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as StatZone[];
  }

  return [] as StatZone[];
}

export async function fetchStatAccessInfo({
  mappingId,
  mapId,
  fromDate,
  toDate,
  searchOption,
}: {
  mappingId: string;
  mapId: string;
  fromDate: string;
  toDate: string;
  searchOption: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/statistics/dashboard/${mappingId}/${mapId}`,
    {
      params: {
        fromDate,
        toDate,
        searchOption,
      },
    }
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as StatAccessInfo;
  }

  return null;
}

export async function fetchStatCongestionArea({
  mappingId,
  mapId,
  fromDate,
  toDate,
}: {
  mappingId: string;
  mapId: string;
  fromDate: string;
  toDate: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/statistics/dashboard/density/${mappingId}/${mapId}`,
    {
      params: {
        fromDate,
        toDate,
      },
    }
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as StatCongestionArea[];
  }

  return [] as StatCongestionArea[];
}

export async function fetchLogisticsSummary(params: {
  projectId: string;
  warehouseId: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/summary`,
    {
      params,
    }
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as StatLogisticsSummary;
  }

  return null;
}

export async function fetchLogisticsTrend(params: {
  projectId: string;
  warehouseId: string;
  searchStartDates: string;
  searchEndDates: string;
  logisticsCategory: 'WAREHOUSING' | 'RELEASING';
  status: 'READY' | 'COMPLETE';
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/serial/logistics`,
    {
      params,
    }
  );

  let logisticsTrends: StatLogisticsTrend[] = [];
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    logisticsTrends = result.data as StatLogisticsTrend[];
    logisticsTrends = logisticsTrends.sort((a, b) =>
      moment(a.date).diff(moment(b.date))
    );
  }

  return logisticsTrends;
}

export async function fetchLogisticsStockByProducts(params: {
  projectId: string;
  warehouseId: string;
  searchStartDates: string;
  searchEndDates: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/products/stocks`,
    {
      params,
    }
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as StatLogisticsStockByProduct;
  }

  return null;
}

export async function fetchLogisticsStockChange(params: {
  projectId: string;
  warehouseId: string;
  searchStartDates: string;
  searchEndDates: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/serial/stocks`,
    {
      params,
    }
  );

  let logisticsStockChanges: StatLogisticsStockChange[] = [];
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    logisticsStockChanges = result.data as StatLogisticsStockChange[];
    logisticsStockChanges = logisticsStockChanges.sort((a, b) =>
      moment(a.date).diff(moment(b.date))
    );
  }

  return logisticsStockChanges;
}
