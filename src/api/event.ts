import axios from 'axios';
import { Config } from '@/config';
import { PAGING_SIZE } from '@/utils/constants/common';
import { ApiResult, ListResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import { ControlEvent } from '@/modules/event/types';

type RequestFetchActivation = {
  projectId: string;
  mappingId: string;
  mapId: string;
};

export async function fetchActivationEvents({
  projectId,
  mappingId,
  mapId,
}: RequestFetchActivation) {
  const response = await axios.get(
    `${Config.platform_api.uri}/realtime/events`,
    {
      params: {
        projectId,
        mappingId,
        mapId,
      },
    }
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as ControlEvent[];
  }

  throw new Error('Server Error');
}

type RequestFetchEvents = {
  page: number;
  projectId: string;
  mappingId: string;
  mapId: string;
};

export async function fetchEvents({
  page,
  projectId,
  mappingId,
  mapId,
}: RequestFetchEvents) {
  const response = await axios.get(`${Config.platform_api.uri}/events`, {
    params: {
      pageSize: PAGING_SIZE,
      page,
      projectId,
      mappingId,
      mapId,
    },
  });
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    const process: ListResult<ControlEvent[]> = {
      content: result.data.content,
      totalElements: result.data.totalElements,
    };

    return process;
  }

  throw new Error('Server Error');
}

export async function fetchEvent(eventId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/events/${eventId}`
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as ControlEvent;
  }

  throw new Error('Server Error');
}

export type RequestPostEvent = {
  projectId: string;
  targetMappingId: string;
  targetMapId: string;
  targetAreaId?: string;
  targetGeomType?: 'OUTER' | 'INNER';
  imgId?: string;
  eventTitle: string;
  eventSubject: string;
  eventContent: string;
  startDate: string;
  endDate: string;
  activeFlag: boolean;
  geomType?: 'CIRCLE' | 'POLYGON';
  geom?: string;
  lat?: number;
  lng?: number;
  radius?: number;
  outerKey?: string;
};

export async function postEvent(data: RequestPostEvent) {
  const response = await axios.post(`${Config.platform_api.uri}/events`, data);
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data;
  }

  throw new Error('Server Error');
}

export type RequestPutEvent = {
  targetAreaId?: string;
  targetGeomType?: 'OUTER' | 'INNER';
  imgId?: string;
  eventTitle: string;
  eventSubject: string;
  eventContent: string;
  startDate: string;
  endDate: string;
  geomType?: 'CIRCLE' | 'POLYGON';
  geom?: string;
  lat?: number;
  lng?: number;
  radius?: number;
  outerKey?: string;
};

export async function putEvent(eventId: string, data: RequestPutEvent) {
  const response = await axios.put(
    `${Config.platform_api.uri}/events/${eventId}`,
    data
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data;
  }

  throw new Error('Server Error');
}

export async function putEventActive(eventId: string, checked: boolean) {
  let flag = 'off';

  if (checked) {
    flag = 'on';
  }

  const response = await axios.put(
    `${Config.platform_api.uri}/events/${flag}/${eventId}`
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data;
  }

  throw new Error('Server Error');
}

export async function deleteEvent(eventId: string) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/events/${eventId}`
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data;
  }

  throw new Error('Server Error');
}
