import axios from 'axios';
import { Config } from '@/config';
import { ApiResult, ListResult } from '@/modules/common';
import {
  ProjectDetail,
  ProjectInfo,
  ProjectProduceStep,
  Projects,
  ProjectSpaceLocation,
  ProjectStepData,
} from '@/modules/project/types';
import { StatusCodes } from 'http-status-codes';
import { PAGING_SIZE } from '@/utils/constants/common';

export async function fetchRecentProjects() {
  const response = await axios.get(
    `${Config.platform_api.uri}/projects/latest/4`
  );

  if (response && response.data) {
    const result: ApiResult = response.data;
    if (result.status === StatusCodes.OK) {
      const projects = result.data as Projects;
      return projects.filter((project) => project.produceEndFlag);
    }
  } else {
    return undefined;
  }
}

export async function fetchProjects(page: number, searchKey: string) {
  const response = await axios.get(`${Config.platform_api.uri}/projects`, {
    params: {
      pageSize: PAGING_SIZE,
      page,
      usePaging: true,
      searchKey,
    },
  });
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    const process: ListResult<Projects> = {
      content: result.data.content,
      totalElements: result.data.totalElements,
    };

    return process;
  }

  throw new Error('Server Error');
}

export async function fetchProject(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/projects/${projectId}`
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as ProjectDetail;
  }

  return null;
}

export async function fetchProjectsAllSpace() {
  const response = await axios.get(
    `${Config.platform_api.uri}/projects/all-buildings`
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as ProjectSpaceLocation[];
  }

  return [] as ProjectSpaceLocation[];
}

export async function postProject(data: ProjectInfo) {
  const produceStepData: ProjectProduceStep = {
    step: 2,
  };
  data.produceStepData = JSON.stringify(produceStepData);
  const response = await axios.post(
    `${Config.platform_api.uri}/projects`,
    data
  );

  let projectId = '';
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    projectId = result.data as string;
  }

  return projectId;
}

export async function postProjectConnectSpace(
  projectId: string,
  metaId: string
) {
  const response = await axios.post(
    `${Config.platform_api.uri}/projects/${projectId}/buildings/${metaId}`
  );
  const result: ApiResult = response.data;
  return result;
}

export async function putProject(projectId: string, data: ProjectInfo) {
  const response = await axios.put(
    `${Config.platform_api.uri}/projects/${projectId}`,
    data
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function putProjectStepData(
  projectId: string,
  data: ProjectStepData
) {
  const response = await axios.put(
    `${Config.platform_api.uri}/projects/${projectId}/step-data`,
    data
  );
  const result: ApiResult = response.data;
  return result;
}

export async function deleteProject(projectId: string) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/projects/${projectId}`
  );

  if (response.status === StatusCodes.OK) {
    const result: ApiResult = response.data;
    return result.data;
  }

  return false;
}

export async function deleteProjectDisconnectSpace(
  projectId: string,
  metaId: string
) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/projects/${projectId}/mapping/${metaId}`
  );
  const result: ApiResult = response.data;
  return result;
}

export async function postProjectPushToken(
  projectId: string,
  pushTokenId: string
) {
  axios.post(`${Config.platform_api.uri}/projects/${projectId}/leave-trace`, {
    pushTokenId,
  });
}
