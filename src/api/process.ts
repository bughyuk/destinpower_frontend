import axios from 'axios';
import { Config } from '@/config';
import { ApiResult, dp_ApiResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import { NodeResult } from '@/api/type';
import { fetchImage } from '@/api/common';
import { CommonUtils } from '@/utils';
import {
  Process,
  CheckList,
  CheckListStep,
  CheckListItem,
  StepGroup,
} from '@/modules/physical_distribution/types';

// TODO: 검색어
export async function fetchProcessCheckList(searchQuery: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-checklist/list`,
    {
      params: searchQuery !== '' && {
        q: searchQuery,
      },
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  let checklist: CheckList[] = [];

  if (result.status === StatusCodes.OK) {
    checklist = response.data.list.map((item: any) => {
      return {
        checkListId: item.id,
        checkListName: item.name,
        registerTime: item.reg_time,
        updateTime: item.update_time,
        status: item.status,
        checkListStep: item.steps.map((step: any) => {
          return {
            stepId: step.id,
            stepName: step.name,
            stepOrder: step.ord,
            group: {
              groupId: step.grp.id,
              groupName: step.grp.name,
            } as StepGroup,
            stepItems: step.items.map((i: any) => {
              return {
                itemId: i.id,
                itemName: i.name,
                itemOrder: i.ord,
              } as CheckListItem;
            }),
          } as CheckListStep;
        }),
      } as CheckList;
    });
  }

  return checklist;
}

// TODO: 검색어
export async function fetchProcessWorkingList(searchQuery: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-working/list`,
    {
      params: searchQuery !== '' && {
        q: searchQuery,
      },
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  let processes: Process[] = [];

  if (result.status === StatusCodes.OK) {
    processes = response.data.list.map((item: any) => {
      return {
        processId: item.id,
        processName: item.name,
        processPname: item.pname,
        registerTime: item.reg_time,
        updateTime: item.update_time,
        serialNumber: item.sn,
        deleteFlag: item.status,
      };
    });
  }

  return processes;
}

// 공정 체크리스트 추가 참고 데이터
export async function fetchProcessWorkingData() {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-working/data`,
    {
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  let data;
  if (result.status === StatusCodes.OK) {
    data = response.data;
  }

  return data;
}

export async function postProcessWorkingAdd(data: {
  name: string;
  pname: string;
  sn: string;
  wtime: string;
  etime: string;
  pcid: number;
  ptid: number;
  fid: number;
  bid: number;
}) {
  const response = await axios.post(
    `${Config.backend_api.uri}/process-working/add`,
    data,
    {
      withCredentials: false,
    }
  );

  const result: dp_ApiResult = response.data;

  if (result.res === 'succ') {
    return true;
  } else {
    return dp_alert(result.message);
  }

  return false;
}

export async function postProcessWorkingDelete(processId: string) {
  const response = await axios.post(
    `${Config.backend_api.uri}/process-working/${processId}/remove`,
    {},
    {
      withCredentials: false,
    }
  );

  const result: dp_ApiResult = response.data;

  if (result.res === 'succ') {
    return true;
  } else {
    return dp_alert(result.message);
  }

  return false;
}

function dp_alert(msg: string) {
  window.alert(msg);
  return false;
}

export async function postProcessCheckListDelete(checkListId: string) {
  const response = await axios.post(
    `${Config.backend_api.uri}/process-checklist/${checkListId}/remove`,
    {},
    {
      withCredentials: false,
    }
  );

  const result: dp_ApiResult = response.data;

  if (result.res === 'succ') {
    return true;
  } else {
    return dp_alert(result.message);
  }

  return false;
}

export async function fetchProcessWorkingInfo(processId: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-working/${processId}/info`,
    {
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  if (result.status === StatusCodes.OK) {
    return result.data;
  }
}

export async function fetchProcessCheckListInfo(checkListId: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-checklist/${checkListId}/info`,
    {
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  if (result.status === StatusCodes.OK) {
    return result.data;
  }
}

export async function postProcessWorkingStepProgress(
  stepId: number,
  progress: number
) {
  const response = await axios.post(
    `${Config.backend_api.uri}/process-working/step-progress/${stepId}`,
    {
      progress,
    },
    {
      withCredentials: false,
    }
  );

  const result: dp_ApiResult = response.data;

  if (result.res === 'succ') {
    return true;
  } else {
    return dp_alert(result.message);
  }

  return false;
}

export async function postProcessWorkingItemProgress(
  stepId: string,
  itemId: number,
  progress: number
) {
  const response = await axios.post(
    `${Config.backend_api.uri}/process-working/item-progress/${stepId}/${itemId}`,
    {
      progress,
    },
    {
      withCredentials: false,
    }
  );

  const result: dp_ApiResult = response.data;

  if (result.res === 'succ') {
    return true;
  } else {
    return dp_alert(result.message);
  }

  return false;
}

export async function fetchProcessWorkingHistory(processId: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-working/history/${processId}`,
    {
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  if (result.status === StatusCodes.OK) {
    return result.data;
  }
}

export async function fetchProcessCompleted(factoryId: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/process-completed/${factoryId}/list`,
    {
      withCredentials: false,
    }
  );

  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });

  if (result.status === StatusCodes.OK) {
    return result.data;
  }
}
