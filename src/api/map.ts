import axios from 'axios';
import { Config } from '@/config';
import { ApiResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import {
  HistorySimulationDate,
  HistorySimulationUser,
  HistoryTrace,
  HistoryTraceArea,
  HistoryUser,
  ProjectRealtimeData,
} from '@/modules/map/types';
import moment from 'moment';

type RequestFetchRealtimeAllData = {
  projectId: string;
  mappingId: string;
  mapId: string;
};

export async function fetchProjectRealtime({
  projectId,
  mappingId,
  mapId,
}: RequestFetchRealtimeAllData) {
  const response = await axios.get(
    `${Config.platform_api.uri}/realtime/projects/${projectId}/buildings/${mappingId}/floors/${mapId}/data`
  );

  if (response) {
    const result: ApiResult = response.data;
    if (result.status === StatusCodes.OK) {
      return result.data as ProjectRealtimeData;
    }

    throw new Error('Server Error');
  }

  return {} as ProjectRealtimeData;
}

type RequestFetchHistoryUser = {
  mapid: string;
  startDate: string;
  endDate: string;
  stime: string;
  etime: string;
  user_gender: string;
  device_type: string;
  ageFrom: number;
  ageTo: number;
  user_id?: string;
  visitFrom: number;
  visitTo: number;
};

export async function fetchHistoryUser({
  mapid,
  startDate,
  endDate,
  stime,
  etime,
  user_gender,
  device_type,
  ageFrom,
  ageTo,
  user_id,
  visitFrom,
  visitTo,
}: RequestFetchHistoryUser) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/lmsVisitorSelSummary`,
    {
      params: {
        mapid,
        startDate,
        endDate,
        stime,
        etime,
        user_gender,
        device_type,
        ageFrom,
        ageTo,
        user_id,
        visitFrom,
        visitTo,
      },
    }
  );
  return response.data as HistoryUser[];
}

export async function fetchHistoryUserTrace({
  mapid,
  startDate,
  endDate,
  stime,
  etime,
  user_gender,
  device_type,
  ageFrom,
  ageTo,
  user_id,
  visitFrom,
  visitTo,
}: RequestFetchHistoryUser) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/lmsVisitorPosHistoSel`,
    {
      params: {
        mapid,
        startDate,
        endDate,
        stime,
        etime,
        user_gender,
        device_type,
        ageFrom,
        ageTo,
        user_id,
        visitFrom,
        visitTo,
      },
    }
  );

  if (response && response.status === StatusCodes.OK) {
    const data = response.data;
    const dateData = data[user_id!];
    const detailData = data['detail'];

    const historyTraceList: HistoryTrace[] = [];

    let dateKeys = Object.keys(dateData);
    dateKeys = dateKeys.sort(
      (a, b) => moment(b).valueOf() - moment(a).valueOf()
    );

    for (const date of dateKeys) {
      let dateLocationList: number[][] = [];
      const timeData = dateData[date];
      const detailTimeData = detailData[date];

      const historyTraceAreaList: HistoryTraceArea[] = [];
      let timeKeys = Object.keys(timeData);
      timeKeys = timeKeys.sort();

      for (const dateTime of timeKeys) {
        const hour = moment(dateTime, 'YYYY-MM-DD HH').format('HH');
        const areaData = timeData[dateTime];
        const area = areaData.area_name.join(', ');

        const detailTimeDatum = detailTimeData[dateTime];
        const sectionLocationList: number[][] = [];
        if (detailTimeDatum) {
          const coordinates = detailTimeDatum[user_id!];
          const xtm = coordinates.xtm;
          const ytm = coordinates.ytm;
          if (xtm.length === ytm.length) {
            for (let i = 0; i < xtm.length; i++) {
              sectionLocationList.push([Number(xtm[i]), Number(ytm[i])]);
            }
          }
        }

        historyTraceAreaList.push({
          hour: `${hour}:00`,
          area,
          sectionLocationList,
        });

        dateLocationList = dateLocationList.concat(sectionLocationList);
      }

      historyTraceAreaList.sort((a, b) => -1);
      historyTraceList.push({
        date,
        trace: historyTraceAreaList,
        dateLocationList,
      });
    }

    return historyTraceList;
  } else {
    throw new Error('Server Error');
  }

  return [];
}

export async function fetchHistoryAllUserTrace({
  mapid,
  startDate,
  endDate,
  stime,
  etime,
  user_gender,
  device_type,
  ageFrom,
  ageTo,
  visitFrom,
  visitTo,
}: RequestFetchHistoryUser) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });

  const response = await axiosInstance.get(
    `${Config.space_api.uri}/lmsVisitorPosHistoAll`,
    {
      params: {
        mapid,
        startDate,
        endDate,
        stime,
        etime,
        user_gender,
        device_type,
        ageFrom,
        ageTo,
        visitFrom,
        visitTo,
      },
    }
  );

  if (response && response.status === StatusCodes.OK) {
    const data = response.data;
    if (data) {
      const historySimulationUser: HistorySimulationUser = {};
      const detail = data.detail;
      for (const userId in detail) {
        const historySimulationDate: HistorySimulationDate = {};
        const historyDateData = detail[userId];
        for (const date in historyDateData) {
          const coordinates = historyDateData[date];
          const xtm = coordinates.xtm;
          const ytm = coordinates.ytm;
          if (xtm.length === ytm.length) {
            const locationList: number[][] = [];
            for (let i = 0; i < xtm.length; i++) {
              locationList.push([Number(xtm[i]), Number(ytm[i])]);
            }
            historySimulationDate[date] = locationList;
          }
        }
        historySimulationUser[userId] = historySimulationDate;
      }
      return historySimulationUser;
    }
  } else {
    throw new Error('Server Error');
  }

  return {} as HistorySimulationUser;
}
