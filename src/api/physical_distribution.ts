import axios from 'axios';
import { Config } from '@/config';
import { ApiResult, dp_ApiResult } from '@/modules/common';
import { StatusCodes } from 'http-status-codes';
import {
  Client,
  ClientKind,
  DistributionHistory,
  DistributionHistoryLogisticsCategory,
  DistributionRevisedHistory,
  DistributionStatus,
  Item,
  Product,
  ProductCodeSize,
  ProductDetail,
  ReleaseProduct,
  Warehouse,
  WarehouseDetail,
  WarehousingProductBox,
  Factory,
  ProductLine,
  ProductType,
} from '@/modules/physical_distribution/types';
import { NodeResult } from '@/api/type';
import { fetchImage } from '@/api/common';
import { CommonUtils } from '@/utils';

export async function fetchProducts(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/${projectId}/products`,
    {
      params: {
        usePaginig: false,
      },
    }
  );
  const result: ApiResult = response.data;
  let products: Product[] = [];
  if (result.status === StatusCodes.OK) {
    products = result.data as Product[];

    for (const product of products) {
      const imgId = product.imgId;
      if (imgId) {
        const imageUrl = await fetchImage(imgId);
        if (imageUrl) {
          product.imgUrl = imageUrl;
        }
      }
    }
  }

  return products;
}

export async function fetchProduct(projectId: string, productId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/${projectId}/products/${productId}`
  );
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const data = result.data as ProductDetail;
    const productDetail = data.detail;
    if (productDetail) {
      const imgId = productDetail.imgId;
      if (imgId) {
        const imageUrl = await fetchImage(imgId);
        if (imageUrl) {
          productDetail.imgUrl = imageUrl;
        }
      }
    }

    return data;
  }

  return null;
}

type ProductCodeSizeResponse = {
  [sizeKey: number]: string;
};

export async function fetchProductCodeSize() {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/products/code/size`
  );

  const result: ApiResult = response.data;
  const productCodeSizes: ProductCodeSize[] = [];
  if (result.status === StatusCodes.OK) {
    const data = result.data as ProductCodeSizeResponse;
    if (!CommonUtils.isEmptyObject(data)) {
      for (const sizeKey in data) {
        const datum = data[sizeKey];
        if (datum) {
          productCodeSizes.push({
            code: Number(sizeKey),
            text: datum,
          });
        }
      }
    }
  }

  return productCodeSizes;
}

export async function postProduct(
  projectId: string,
  data: {
    productCode: string;
    productName: string;
    imgId: string;
    boxQuantity: number;
    productSize: string;
    minQuantity: number;
    productCategoryId: string;
    productWeight: number;
  }
) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/${projectId}/products`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const productId = result.data as string;
    if (productId) {
      return true;
    }
  }

  return false;
}

export async function putProduct(
  productId: string,
  data: {
    productCode: string;
    productName: string;
    imgId: string;
    boxQuantity: number;
    productSize: string;
    minQuantity: number;
    productCategoryId: string;
    productWeight: number;
  }
) {
  const response = await axios.put(
    `${Config.platform_api.uri}/factory/products/${productId}`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function deleteProduct(projectId: string, productId: string) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/factory/products/${productId}`,
    {
      params: {
        projectId,
      },
    }
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function postWarehousing(data: {
  projectId: string;
  warehouseId: string;
  boxList: WarehousingProductBox[];
  logisticsQuantity: number;
  dueDate: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/logistics/warehousing-directions`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as string;
  }

  return null;
}

export async function postManualComplete(data: {
  projectId: string;
  logisticsId: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/logistics/manuel`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return (result.data as string) === 'success' ? true : false;
  }

  return false;
}

export async function fetchQRCode(data: string) {
  const axiosInstance = axios.create({
    withCredentials: false,
  });
  const response = await axiosInstance.get(`${Config.space_api.uri}/qrcode`, {
    params: {
      url: data,
    },
  });

  if (response.status === StatusCodes.OK) {
    const resultQRCode = response.data as NodeResult<string>;
    return resultQRCode.result;
  }

  return null;
}

export async function fetchReleaseProducts(
  projectId: string,
  warehouseId: string
) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/logistics/releasing/init`,
    {
      params: {
        projectId,
        warehouseId,
      },
    }
  );
  const result: ApiResult = response.data;
  let products: ReleaseProduct[] = [];
  if (result.status === StatusCodes.OK) {
    products = result.data as ReleaseProduct[];

    for (const product of products) {
      const imgId = product.imgId;
      if (imgId) {
        const imageUrl = await fetchImage(imgId);
        if (imageUrl) {
          product.imgUrl = imageUrl;
        }
      }
    }
  }

  return products;
}

export async function postRelease(data: {
  projectId: string;
  warehouseId: string;
  boxCategoryId: string;
  customerId: string;
  logisticsQuantity: number;
  dueDate: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/logistics/releasing-directions`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as string;
  }

  return null;
}

export async function postAdjustment(data: {
  projectId: string;
  warehouseId: string;
  boxCategoryId: string;
  logisticsQuantity: number;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/logistics/adjustment`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as string;
  }

  return null;
}

export async function postMovement(data: {
  projectId: string;
  warehouseId: string;
  boxCategoryId: string;
  logisticsQuantity: number;
  targetWarehouseId: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/logistics/move`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as string;
  }

  return null;
}

export async function fetchItems(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/product-category`,
    {
      params: {
        projectId,
        usePaginig: false,
      },
    }
  );
  const result: ApiResult = response.data;
  let items: Item[] = [];
  if (result.status === StatusCodes.OK) {
    items = result.data as Item[];
  }

  return items;
}

export async function fetchItemCode(
  productId: string,
  productCodePrefix: string
) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/product-category/prefix-duplicate`,
    {
      params: {
        productId,
        productCodePrefix,
      },
    }
  );
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return true;
}

export async function postItem(data: {
  productCategoryName: string;
  productCodePrefix: string;
  maker: string;
  projectId: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/product-category`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const itemId = result.data as string;
    if (itemId) {
      return true;
    }
  }

  return false;
}

export async function putItem(
  itemId: string,
  data: {
    productCategoryName: string;
    productCodePrefix: string;
    maker: string;
    projectId: string;
  }
) {
  const response = await axios.put(
    `${Config.platform_api.uri}/factory/product-category/${itemId}`,
    data
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function deleteItem(itemId: string) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/factory/product-category/${itemId}`
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function fetchClients(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/manage/customer`,
    {
      params: {
        projectId,
        usePaginig: false,
      },
    }
  );
  const result: ApiResult = response.data;
  let clients: Client[] = [];
  if (result.status === StatusCodes.OK) {
    clients = result.data as Client[];
  }

  return clients;
}

export async function postClient(data: {
  customerName: string;
  logisticsCategory: ClientKind;
  phoneNumber: string;
  mailId: string;
  note: string;
  projectId: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/manage/customer`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const clientId = result.data as string;
    if (clientId) {
      return true;
    }
  }

  return false;
}

export async function putClient(
  clientId: string,
  data: {
    customerName: string;
    logisticsCategory: ClientKind;
    phoneNumber: string;
    mailId: string;
    note: string;
  }
) {
  const response = await axios.put(
    `${Config.platform_api.uri}/factory/manage/customer/${clientId}`,
    data
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function deleteClient(clientId: string) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/factory/manage/customer/${clientId}`
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function fetchWarehouses(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/manage/warehouse`,
    {
      params: {
        projectId,
        usePaginig: false,
      },
    }
  );
  const result: ApiResult = response.data;
  let warehouses: Warehouse[] = [];
  if (result.status === StatusCodes.OK) {
    warehouses = result.data as Warehouse[];
  }

  return warehouses;
}

export async function fetchWarehouse(projectId: string, warehouseId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/manage/warehouse/${warehouseId}`,
    {
      params: {
        projectId,
        usePaginig: false,
      },
    }
  );
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const data = result.data as WarehouseDetail;
    const warehouseStocks = data.stock;
    if (warehouseStocks.length) {
      for (const warehouseStock of warehouseStocks) {
        const imgId = warehouseStock.imgId;
        if (imgId) {
          const imageUrl = await fetchImage(imgId);
          if (imageUrl) {
            warehouseStock.imgUrl = imageUrl;
          }
        }
      }
    }

    return data;
  }

  return null;
}

export async function postWarehouse(data: {
  warehouseName: string;
  maxQuantity: number;
  note: string;
  projectId: string;
}) {
  const response = await axios.post(
    `${Config.platform_api.uri}/factory/manage/warehouse`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return true;
  }

  return false;
}

export async function putWarehouse(
  warehouseId: string,
  data: {
    warehouseName: string;
    maxQuantity: number;
    note: string;
  }
) {
  const response = await axios.put(
    `${Config.platform_api.uri}/factory/manage/warehouse/${warehouseId}`,
    data
  );
  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function deleteWarehouse(warehouseId: string) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/factory/manage/warehouse/${warehouseId}`
  );

  const result: ApiResult = response.data;

  if (result.status === StatusCodes.OK) {
    return result.data as boolean;
  }

  return false;
}

export async function fetchDistributionStatus(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/realtime/count`,
    {
      params: {
        projectId,
        usePaginig: false,
      },
    }
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return result.data as DistributionStatus;
  }

  return null;
}

export async function fetchDashboardDistributionHistory(projectId: string) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/history/logistics`,
    {
      params: {
        projectId,
        usePaginig: true,
      },
    }
  );

  const result: ApiResult = response.data;
  let distributionHistories: DistributionHistory[] = [];
  if (result.status === StatusCodes.OK) {
    distributionHistories = result.data.content as DistributionHistory[];
    for (const distributionHistory of distributionHistories) {
      const imgId = distributionHistory.imgId;
      if (imgId) {
        const imageUrl = await fetchImage(imgId);
        if (imageUrl) {
          distributionHistory.imgUrl = imageUrl;
        }
      }
    }
  }

  return distributionHistories;
}

export async function fetchDistributionHistories(params: {
  page: number;
  projectId: string;
  warehouseId?: string;
  logisticsCategory?: DistributionHistoryLogisticsCategory;
  searchStartDates?: string;
  searchEndDates?: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/history/logistics`,
    {
      params: {
        ...params,
        usePaginig: true,
      },
    }
  );

  let distributionHistories: {
    content: DistributionHistory[];
    totalPage: number;
  } = {
    content: [],
    totalPage: 0,
  };

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const data = result.data.content as DistributionHistory[];
    for (const datum of data) {
      const imgId = datum.imgId;
      if (imgId) {
        const imageUrl = await fetchImage(imgId);
        if (imageUrl) {
          datum.imgUrl = imageUrl;
        }
      }
    }
    distributionHistories = {
      content: data,
      totalPage: result.data.totalPages,
    };
  }

  return distributionHistories;
}

export async function fetchDistributionHistory(
  logisticsId: string,
  projectId: string
) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/statistics/history/logistics/${logisticsId}`,
    {
      params: {
        projectId,
      },
    }
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    const distributionHistory = result.data as DistributionHistory;
    const imgId = distributionHistory.imgId;
    if (imgId) {
      const imageUrl = await fetchImage(imgId);
      if (imageUrl) {
        distributionHistory.imgUrl = imageUrl;
      }
    }

    return distributionHistory;
  }

  return null;
}

export async function fetchDistributionRevisedHistory(params: {
  projectId: string;
  logisticsId: string;
}) {
  const response = await axios.get(
    `${Config.platform_api.uri}/factory/logistics/update-history`,
    {
      params: {
        ...params,
        usePaginig: true,
      },
    }
  );

  let distributionRevisedHistories: DistributionRevisedHistory[] = [];
  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    distributionRevisedHistories = result.data as DistributionRevisedHistory[];
  }

  return distributionRevisedHistories;
}

export async function putDistributionHistory(
  logisticsId: string,
  data: {
    projectId: string;
    cnt: number;
    warehouseId: string;
    customerId: string;
    dueDate: string;
  }
) {
  const response = await axios.put(
    `${Config.platform_api.uri}/factory/logistics/${logisticsId}`,
    data
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return true;
  }

  return false;
}

export async function deleteDistributionHistory(
  logisticsId: string,
  data: {
    projectId: string;
    logisticsId: string;
  }
) {
  const response = await axios.delete(
    `${Config.platform_api.uri}/factory/logistics/${logisticsId}`,
    {
      data,
    }
  );

  const result: ApiResult = response.data;
  if (result.status === StatusCodes.OK) {
    return true;
  }

  return false;
}

// 거래처 목록(검색어)
export async function dp_fetchClients(searchQuery: string) {
  const response = await axios.get(`${Config.backend_api.uri}/buyer/list`, {
    params: searchQuery !== '' && {
      q: searchQuery,
    },
    withCredentials: false, //개발백엔드 필수!!
  });
  // message 위치가 개발백엔드에서는 data 내부에 있음
  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });
  let clients: Client[] = [];
  if (result.status === StatusCodes.OK) {
    clients = result.data.frontend as Client[];
  }

  return clients;
}

// 거래처 등록,수정
export async function dp_postClient(data: {
  id?: string | undefined; //있을때 수정처리
  type: string;
  name: string;
  phone: string;
  email: string;
  comment: string;
}) {
  let response = {
    data: {
      res: 'fail',
      message: '[ERROR] post client',
      id: 0,
    },
  };
  if (data.id === undefined) {
    response = await axios.post(`${Config.backend_api.uri}/buyer/add`, data, {
      withCredentials: false, //개발백엔드 필수!!
    });
  } else {
    const clientId: string = data.id;
    delete data.id; //필요없는
    response = await axios.post(
      `${Config.backend_api.uri}/buyer/${clientId}/update`,
      data,
      {
        withCredentials: false, //개발백엔드 필수!!
      }
    );
  }

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    const clientId = result.id as number;
    if (clientId) {
      return true;
    }
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 거래처 삭제
export async function dp_deleteClient(clientId: string) {
  const response = await axios.post(
    `${Config.backend_api.uri}/buyer/${clientId}/remove`,
    {},
    {
      withCredentials: false, //개발백엔드 필수!!
    }
  );

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    const clientId = result.id as number;
    if (clientId) {
      return true;
    }
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 공장 목록
export async function dp_fetchFactorys(searchQuery: string) {
  const response = await axios.get(`${Config.backend_api.uri}/factory/list`, {
    params: searchQuery !== '' && {
      q: searchQuery,
    },
    withCredentials: false, //개발백엔드 필수!!
  });

  // message 위치가 개발백엔드에서는 data 내부에 있음
  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });
  let factorys: Factory[] = [];
  if (result.status === StatusCodes.OK) {
    factorys = result.data.list as Factory[];
  }

  return factorys;
}

// 공장 등록,수정
export async function dp_postFactory(data: {
  id?: string | undefined;
  name: string;
  memo: string;
}) {
  let response = {
    data: {
      res: 'fail',
      message: '[ERROR] post factory',
      id: 0,
    },
  };
  if (data.id === undefined) {
    response = await axios.post(`${Config.backend_api.uri}/factory/add`, data, {
      withCredentials: false, //개발백엔드 필수!!
    });
  } else {
    const factoryId: string = data.id;
    delete data.id; //필요없는
    response = await axios.post(
      `${Config.backend_api.uri}/factory/${factoryId}/update`,
      data,
      {
        withCredentials: false, //개발백엔드 필수!!
      }
    );
  }

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    // const factoryId = result.id as number;
    // if (factoryId) {
    //   return true;
    // }
    return true; //우선 성공적
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 공장 삭제
export async function dp_deleteFactory(factoryId: string) {
  const response = await axios.post(
    `${Config.backend_api.uri}/factory/${factoryId}/remove`,
    {},
    {
      withCredentials: false, //개발백엔드 필수!!
    }
  );

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    return true;
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 제품군
export async function dp_fetchProductLines(searchQuery: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/product-family/list`,
    {
      params: searchQuery !== '' && {
        q: searchQuery,
      },
      withCredentials: false, //개발백엔드 필수!!
    }
  );

  // message 위치가 개발백엔드에서는 data 내부에 있음
  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });
  let productLines: ProductLine[] = [];
  if (result.status === StatusCodes.OK) {
    productLines = result.data.list as ProductLine[];
  }

  return productLines;
}

// 제품군 등록,수정
export async function dp_postProductLine(data: {
  id?: string | undefined;
  name: string;
  memo: string;
}) {
  let response = {
    data: {
      res: 'fail',
      message: '[ERROR] post product line',
      id: 0,
    },
  };
  if (data.id === undefined) {
    response = await axios.post(
      `${Config.backend_api.uri}/product-family/add`,
      data,
      {
        withCredentials: false, //개발백엔드 필수!!
      }
    );
  } else {
    const lineId: string = data.id;
    delete data.id; //필요없는
    response = await axios.post(
      `${Config.backend_api.uri}/product-family/${lineId}/update`,
      data,
      {
        withCredentials: false, //개발백엔드 필수!!
      }
    );
  }

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    return true;
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 제품군 삭제
export async function dp_deleteProductLine(lineId: string) {
  const response = await axios.post(
    `${Config.backend_api.uri}/product-family/${lineId}/remove`,
    {},
    {
      withCredentials: false, //개발백엔드 필수!!
    }
  );

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    return true;
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 제품타입 목록
export async function dp_fetchProductTypes(searchQuery: string) {
  const response = await axios.get(
    `${Config.backend_api.uri}/product-type/list`,
    {
      params: searchQuery !== '' && {
        q: searchQuery,
      },
      withCredentials: false, //개발백엔드 필수!!
    }
  );

  // message 위치가 개발백엔드에서는 data 내부에 있음
  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });
  let productTypes: ProductType[] = [];
  if (result.status === StatusCodes.OK) {
    productTypes = result.data.list as ProductType[];
  }

  return productTypes;
}

// 제품타입 참고데이타
export async function dp_fetchProductTypeRelData() {
  const response = await axios.get(
    `${Config.backend_api.uri}/product-type/data`,
    {
      withCredentials: false, //개발백엔드 필수!!
    }
  );
  console.log(`[참고데이타]`, response);
  const result: ApiResult = Object.assign(response, {
    message: response.data?.message || ``,
  });
  if (result.status === StatusCodes.OK) {
    return result.data.product_family; //제품군 배열
  } else if (result.data.res === 'fail') {
    dp_alert(result.message);
  }
  return [];
}

// 제품타입 등록,수정
export async function dp_postProductType(data: {
  id?: string | undefined;
  name: string;
  pfid: string;
  i_file: any;
}) {
  let response = {
    data: {
      res: 'fail',
      message: '[ERROR] post product type',
      id: 0,
    },
  };

  // form data 형식
  const formData = new FormData();
  formData.append('name', data.name);
  formData.append('pfid', data.pfid);
  if (data.i_file) {
    formData.append('i_file', data.i_file);
  }

  // post option
  const postOption = {
    withCredentials: false, //개발백엔드 필수!!
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  };

  if (data.id === undefined) {
    response = await axios.post(
      `${Config.backend_api.uri}/product-type/add`,
      formData,
      postOption
    );
  } else {
    const typeId: string = data.id;
    formData.append('id', typeId);
    delete data.id; //필요없는
    response = await axios.post(
      `${Config.backend_api.uri}/product-type/${typeId}/update`,
      formData,
      postOption
    );
  }

  const result: dp_ApiResult = response.data;
  console.log(`[result]`, result, data);
  if (result.res === 'succ') {
    return true;
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// 제품타입 삭제
export async function dp_deleteProductType(typeId: string) {
  const response = await axios.post(
    `${Config.backend_api.uri}/product-type/${typeId}/remove`,
    {},
    {
      withCredentials: false, //개발백엔드 필수!!
    }
  );

  const result: dp_ApiResult = response.data;
  if (result.res === 'succ') {
    return true;
  } else if (result.res === 'fail') {
    return dp_alert(result.message);
  }

  return false;
}

// window alert
function dp_alert(msg: string) {
  window.alert(msg);
  return false;
}
